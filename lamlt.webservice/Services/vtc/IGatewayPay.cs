﻿using HttpContext = Microsoft.AspNetCore.Http.HttpContext;

namespace Golaco.bank
{
    public interface IGatewayPay
    {
        //GatewayResponse ExcuteResultNotifyFromVTCPay(HttpContext context);
        GatewayResponse ExcuteResultRedirectUrl(HttpContext context);
        string getURLToBankNetToCharge(VTCHolder holder);
    }
}