﻿using lamlt.data;
using lamlt.web.Models;
using Newtonsoft.Json;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace lamlt.web.Services
{
    public class livestreamingService : LamltService
    {

        public livestreamingService()
        {

        }
        public List<livestreaming> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.video.Contains(page.search)));
                /*
  if(!(video == "-1") ||  string.IsNullOrEmpty(video)) query=query.Where(e=>e.video == page.video);
*/

                query = query.Skip(offset).Take(limit);
                List<livestreaming> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_livestreaming> GetViewAllItem(PagingModel page)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                return db.Select(db.From<vw_livestreaming>().OrderByDescending(x => x.datecreated)).ToList();
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(video == "-1") ||  string.IsNullOrEmpty(video)) query=query.Where(e=>e.video == page.video);
*/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_livestreaming>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(video == "-1") ||  string.IsNullOrEmpty(video)) query=query.Where(e=>e.video == page.video);
*/

                return db.Count(query);
            }
        }
        public livestreaming GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_livestreaming GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_livestreaming>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(livestreaming obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<livestreaming>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.processid = obj.processid;
                        objUpdate.cmd = obj.cmd;
                        objUpdate.video = obj.video;
                        objUpdate.status = obj.status;
                        objUpdate.type = obj.type;
                        objUpdate.userid = obj.userid;
                        objUpdate.title = obj.title;
                        objUpdate.note = obj.note;
                        objUpdate.resolution = obj.resolution;
                        objUpdate.live_catalog_id = obj.live_catalog_id <= 0 ? obj.live_catalog_id : 1;
                        objUpdate.datecreated = obj.datecreated != null ? obj.datecreated : DateTime.Now;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    //var queryCount = db.From<livestreaming>()
                    //    .Where(e => e.video == obj.video).Select(e => e.Id);
                    //var objCount = db.Count(queryCount);
                    //if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.processid = obj.processid;
                    objUpdate.cmd = obj.cmd;
                    objUpdate.video = obj.video;
                    objUpdate.status = obj.status;
                    objUpdate.note = obj.note;
                    objUpdate.type = obj.type;
                    objUpdate.userid = obj.userid;
                    objUpdate.title = obj.title;
                    objUpdate.live_catalog_id = obj.live_catalog_id <= 0 ? obj.live_catalog_id : 1;
                    objUpdate.resolution = obj.resolution;
                    objUpdate.datecreated = obj.datecreated != null ? obj.datecreated : DateTime.Now;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        internal object GetVideoLive(int limit = 4)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query.Where(e => e.status == 2 && e.catalog_id == 16 && (e.copyright_expired >= DateTime.Now || e.copyright_expired == null));
                query.OrderByDescending(x => x.datecreated);
                return db.Select(query).Take(limit);
            }
        }
        internal object GetVideoByCate(int cateId, int limit = 5)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query.Where(e => e.status == 2);
                if (cateId > 0)
                {
                    query.Where(e => e.catalog_id == cateId && (e.catalog_id != 16 || e.catalog.Contains("Live")));
                    query.Where(e => e.copyright_expired >= DateTime.Now || e.copyright_expired == null);
                }
                return db.Select(query).Take(limit);
            }
        }
        internal object GetVideoByCateOrderBy(int cateId = 1, int limit = 5, int offset = 10, string orderby = "random", string type = "canhac", string filter = "no")
        {
            lstTempTitle = new List<string>(); ;
            List<film_video> lists_result = new List<film_video>();
            List<film_video> lists = new List<film_video>();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query.Where(e => e.status == 2);
                // Chỉ lấy phim mới nhất thì không cho hiển thị video thuộc danh mục live, hoạt hình, giáo dục, ca nhạc ....
                if (cateId == -1)
                {
                    //if (cateId == 1 || cateId == 7 || cateId == 8 || cateId == 9 || cateId == 11 || cateId == 14){}
                    switch (type)
                    {
                        case "canhac":
                            int[] danhmuc_khac_canhac = { 1, 4, 5, 7, 8, 9, 10, 13, 14, 15, 16, 17 };// các id này là danh mục phim
                            foreach (var item in danhmuc_khac_canhac)
                            {
                                query.Where(e => e.catalog_id != item);
                            }
                            break;
                        default:
                            int[] danhmuc_khac_phim = { 4, 5, 6, 13, 15, 16, 17, 21, 22, 23, 24 };// các id này là danh mục ca nhạc, giáo dục,.....
                            foreach (var item in danhmuc_khac_phim)
                            {
                                query.Where(e => e.catalog_id != item);
                            }
                            break;
                    }
                }

                if (cateId > 0)
                {
                    query.Where(e => e.catalog_id == cateId && (e.catalog_id != 16 || e.catalog.Contains("Live")));
                    query.Where(e => e.copyright_expired >= DateTime.Now || e.copyright_expired == null);
                }
                if (!string.IsNullOrEmpty(orderby))
                {
                    switch (orderby)
                    {
                        case "random":
                            query.OrderByRandom();
                            break;
                        case "datecreated":
                            query.OrderByDescending(e => e.datecreated);
                            break;
                        case "title":
                            query.OrderByDescending(e => e.title);
                            break;
                        case "id":
                            query.OrderByDescending(e => e.Id);
                            break;
                        default:
                            query.OrderByDescending(e => e.Id);
                            break;
                    }
                }
                query = query.Skip(offset).Take(limit);
                lists = db.Select(query);
                if (filter == "duplicate")
                {
                    for (int i = 0; i < lists.Count; i++)
                    {
                        if (i == 0) lists_result.Add(lists[0]);
                        if (i > 0)
                        {
                            if (lists[i].title.Contains(lists[i - 1].title.Substring(0, 5)))
                            {
                                var obj_search = Film_Regex_Duplicate(lists[i].title, cateId);
                                if (obj_search != null)
                                {
                                    lists_result.Add(obj_search);
                                }
                            }
                        }
                    }
                    return lists_result;
                }
                return lists;
            }
        }
        public static List<string> lstTempTitle = new List<string>();
        public film_video Film_Regex_Duplicate(string title, int cateId)
        {
            film_video obj = new film_video();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query.Where(e => e.status == 2 && e.catalog_id == cateId && !e.title.Contains(title.Substring(0, 5)));
                if (lstTempTitle.Count > 0)
                {
                    for (int i = 0; i < lstTempTitle.Count; i++)
                    {
                        query.Where(e => !e.title.Contains(lstTempTitle[i]));
                    }
                }
                query.OrderByDescending(e => e.datecreated);
                obj = db.Select(query).FirstOrDefault();
                if (obj != null) lstTempTitle.Add(obj.title.Substring(0, 10));
            }
            return obj;
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public livestreaming InitEmpty()
        {
            var obj = new livestreaming();
            obj.Id = 0;
            return obj;
        }

        internal int KillProcessLive(string id, string process_id)
        {
            int rs = -1;
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string path = "/home/amnhacsaigon/livestream/";
            if (isDev)
            {
                path = "C://";
            }
            using (StreamWriter sw = new StreamWriter(path + process_id + ".process", false, Encoding.UTF8))
            {
                sw.Write(JsonConvert.SerializeObject(GetByID(id)));
                sw.Close();
                rs = 1;
            }
            return rs;
        }
        /*
genvie here
create view vw_livestreaming
as
   select livestreaming.Id,livestreaming.processid,livestreaming.cmd,livestreaming.video,livestreaming.status,livestreaming.note,livestreaming.datecreated  from livestreaming
*/
    }
}