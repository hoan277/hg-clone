﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_user_logService : LamltService
    {
        #region ===== LIST VIEW =====
        //list-view
        public List<vw_film_video_user_log> GetViewAllItem(PagingModel page, string token)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query_user = db.From<film_user>().Where(e => (e.token == token));
                film_user user = db.Select(query_user).FirstOrDefault();
                if (user != null)
                {
                    var query = db.From<vw_film_video_user_log>();
                    query.OrderByDescending(x => x.Id);

                    int offset = 0; try { offset = page.offset; }
                    catch { }

                    int limit = 10;
                    try { limit = page.limit; }
                    catch { }
                    query = query.Where(e => (e.user_id == user.Id && e.link_userid == page.link_id));
                    query = query.Skip(offset).Take(limit);
                    List<vw_film_video_user_log> rows = db.Select(query).ToList();
                    return rows;
                }
                return null;
            }
        }
        #endregion ===== LIST VIEW =====

        #region ===== COUNT ALL =====
        public long CountAll(PagingModel page, string token)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query_user = db.From<film_user>().Where(e => (e.token == token));
                film_user user = db.Select(query_user).FirstOrDefault();
                if (user != null)
                {
                    var query = db.From<film_video_user_log>();
                    int offset = 0; try { offset = page.offset; }
                    catch { }

                    int limit = 10;
                    try { limit = page.limit; }
                    catch { }

                    query = query.Where(e => (e.user_id == user.Id && e.link_userid == page.link_id));
                    return db.Count(query);
                }
                return -1;
            }
        }
        #endregion ===== COUNT ALL =====

        #region ===== UPDATE OR INSERT =====
        public int UpdateOrInsert(int video_id, int link_id, string token, string time)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query_user = db.From<film_user>().Where(e => e.token == token);
                film_user user = db.Select(query_user).FirstOrDefault();

                if (user != null)
                {

                    // update user log
                    var query_log = db.From<film_video_user_log>().Where(e => e.film_id == video_id && e.user_id == user.Id && e.link_userid == link_id);
                    var obj_log = db.Select(query_log).FirstOrDefault();

                    var query_film = db.From<film_video>().Where(e => e.Id == video_id);
                    var obj_film = db.Select(query_film).SingleOrDefault();
                    if (obj_film != null)
                    {
                        // update view
                        film_video_viewService filmVideoViewSV = new film_video_viewService();
                        film_video_view filmView = new film_video_view()
                        {
                            userid = user.Id,
                            videoid = video_id,
                            status = 1,
                            view = 1,
                            time_end = int.Parse(time)
                        };
                        int intV = filmVideoViewSV.UpdateOrInsertApi(filmView);

                        if (obj_log != null)
                        {
                            intV = 1;
                            obj_log.user_id = user.Id;
                            obj_log.film_id = video_id;
                            obj_log.link_userid = link_id;
                            obj_log.date_updated = DateTime.Now;
                            obj_log.status = 1;
                            int a = db.Update(obj_log);
                            if (a > 0)
                            {
                                return obj_log.Id;
                            }
                            return 0;
                        }
                        else
                        {
                            intV = 2;
                            var obj_log_init = InitEmpty();
                            obj_log_init.user_id = user.Id;
                            obj_log_init.link_userid = link_id;
                            obj_log_init.film_id = video_id;

                            obj_log_init.date_created = DateTime.Now;
                            obj_log_init.date_updated = DateTime.Now;
                            obj_log_init.status = 1;
                            return (int)db.Insert(obj_log_init, selectIdentity: true);
                        }
                    }
                    return -5; // sai video
                }
                return -4; // sai token
            }
        }
        public int UpdateViewToken(int video_id, string token)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query_user = db.From<film_user>().Where(e => e.token == token);
                film_user user = db.Select(query_user).FirstOrDefault();

                if (user != null)
                {
                    var query_log = db.From<film_video_user_log>().Where(e => e.film_id == video_id && e.user_id == user.Id && e.link_userid == user.link_id);
                    var obj_log = db.Select(query_log).FirstOrDefault();

                    var query_film = db.From<film_video>().Where(e => e.Id == video_id);
                    var obj_film = db.Select(query_film).SingleOrDefault();
                    if (obj_film != null)
                    {
                        if (obj_log != null)
                        {
                            obj_log.date_updated = DateTime.Now;
                            obj_log.status = 1;
                            int a = db.Update(obj_log);
                            if (a > 0) return obj_log.Id;
                            return 0;
                        }
                        else
                        {
                            var obj_log_init = InitEmpty();
                            obj_log_init.user_id = user.Id;
                            obj_log_init.link_userid = user.link_id;
                            obj_log_init.film_id = video_id;

                            obj_log_init.date_created = DateTime.Now;
                            obj_log_init.date_updated = DateTime.Now;
                            obj_log_init.status = 1;
                            return (int)db.Insert(obj_log_init, selectIdentity: true);
                        }
                    }
                    return -5; // sai video
                }
                return -4; // sai token
            }
        }
        public film_video_user_log InitEmpty()
        {
            var obj = new film_video_user_log();
            obj.Id = 0;
            return obj;
        }
        #endregion ===== UPDATE OR INSERT =====

        #region ===== DETAIL =====
        //get-by-id
        public film_users_log GetByID(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_users_log>().Where(e => e.Id == Id);
                return db.Select(query).SingleOrDefault();
            }
        }
        //get-view-by-id
        public vw_film_video_user_log GetViewByID(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_user_log>().Where(e => e.Id == Id);
                return db.Select(query).FirstOrDefault();
            }
        }
        // get-view-by-video_id-token
        //public vw_film_video_user_log GetViewByVideoIdAndToken(int video_id, string token)
        //{
        //    using (var db = _connectionData.OpenDbConnection())
        //    {
        //        var query = db.From<vw_film_video_user_log>().Where(e => e.film_id==video_id && e.);
        //        return db.Select(query).FirstOrDefault();
        //    }
        //}

        #endregion ===== DETAIL =====

        #region ===== DELETE =====
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_user_log>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        #endregion ===== DELETE =====
    }
}