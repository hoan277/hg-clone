﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace lamlt.webservice.Services
{
    public class HelperUtil
    {
        public static string EncryptString(string text, string keyString)
        {
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                        return Convert.ToBase64String(result);
                    }
                }
            }
        }

        public static string DecryptString(string cipherText, string keyString)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[16];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }

        public static bool isViettel(string phone)
        {
            //string[] vietel_number = { "098", "032", "033", "034", "035", "036", "037", "038", "086", "96", "97", "98", "32", "33", "34", "35", "36", "37", "38", "86", "+8496", "+8497", "+8498", "+8432", "+8433", "+8434", "+8435", "+8436", "+8437", "+8438", "+8486" };
            return phone.StartsWith("096") || phone.StartsWith("097") || phone.StartsWith("098") || phone.StartsWith("032") || phone.StartsWith("033")
                || phone.StartsWith("034") || phone.StartsWith("035") || phone.StartsWith("036") || phone.StartsWith("037") || phone.StartsWith("038")
                || phone.StartsWith("086") || phone.StartsWith("96") || phone.StartsWith("97") || phone.StartsWith("98") || phone.StartsWith("32")
                || phone.StartsWith("33") || phone.StartsWith("34") || phone.StartsWith("35") || phone.StartsWith("36") || phone.StartsWith("37")
                || phone.StartsWith("38") || phone.StartsWith("86") || phone.StartsWith("+8496") || phone.StartsWith("+8497") || phone.StartsWith("+8498")
                || phone.StartsWith("+8432") || phone.StartsWith("+8433") || phone.StartsWith("+8434") || phone.StartsWith("+8435") || phone.StartsWith("+8436")
                || phone.StartsWith("+8437") || phone.StartsWith("+8438") || phone.StartsWith("+8486") || phone.StartsWith("039");
        }

        public static bool isVina(string s)
        {
            if (s.StartsWith("+84")) s = s.Replace("+84", "0");
            return s.StartsWith("088")
               || s.StartsWith("091")
               || s.StartsWith("094")
               || s.StartsWith("081")
               || s.StartsWith("082")
               || s.StartsWith("083")
               || s.StartsWith("084")
               || s.StartsWith("085");
        }

        public static bool SendEmail(string email, string title, string body, string fileName)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("internationalbusiness2710@gmail.com");
                mail.To.Add(email);
                mail.Subject = title;
                mail.Body = body;
                mail.IsBodyHtml = true;
                if (fileName != null)
                {
                    mail.Attachments.Add(new Attachment(fileName));
                }
                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    //smtp.Credentials = new NetworkCredential("admin@lalatv.com.vn", "Asd123!@#");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = new NetworkCredential("internationalbusiness2710@gmail.com", "Intlamlt123!");
                    smtp.EnableSsl = true;

                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(mail);
                    return true;
                }
            }
        }

        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
    }
}