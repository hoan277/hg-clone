﻿using Lala.Services.Utility;
using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_videoService : LamltService
    {

        public film_videoService()
        {

        }
        public List<film_video_image> GetAll_Images(int videoid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>();
                query.Where(e => e.filmid == videoid);
                List<film_video_image> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<film_video> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == 0) page.cpid = -1;
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));

                //query = query.Where(e => (e.cpid == page.cpid));
                if (page.cpid > 0) query = query.Where(e => e.cpid == page.cpid);
                if (page.categoryid > 0) query = query.Where(e => e.catalog_id == page.categoryid);


                query = query.Skip(offset).Take(limit);
                List<film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_video> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == 0) ;
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                if (page.categoryid > 0) query = query.Where(e => e.catalog_id == page.categoryid);

                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search) || e.tags.Contains(page.search)));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);


                query = query.Skip(offset).Take(limit);
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_video> GetViewAllItem_Episode(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            int offset = 0;
            try { offset = page.offset; }
            catch { }

            int limit = 10;//int.Parse(Request.Params["limit"]);
            try { limit = page.limit; }
            catch { }
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            List<vw_film_video> lstVideo = new List<vw_film_video>();
            using (var db = _connectionData.OpenDbConnection())
            {
                //lấy các tập
                var queryEpisode = db.From<vw_film_episode>().Where(e => e.status == 1 && e.title.Contains(page.search));
                //var queryEpisode = db.From<vw_film_episode>().Where(e => e.status == 1 && e.title.Contains(page.search));

                if (page.categoryid > 0) queryEpisode.Where(e => e.catalog_id == page.categoryid);

                List<vw_film_episode> listEpisode = db.Select(queryEpisode.Skip(offset).Take(100)).ToList();

                //lặp qua các tập
                foreach (var episode in listEpisode)
                {
                    if (page.categoryid > 0)
                    {
                        var queryVideo = db.From<vw_film_video>();
                        queryVideo.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                        && (x.catalog_id == page.categoryid || y.catalogid == page.categoryid)
                        && x.episode_id == episode.Id && x.status == 2);
                        queryVideo = queryVideo.Where(e => e.isview > 0);
                        //queryVideo = queryVideo.Where(e => e.filter == 1 && e.isview > 0);
                        //var video = db.Select(queryVideo).FirstOrDefault();
                        var video = db.Select(queryVideo).Where(b => b.episode_current == 1).FirstOrDefault();
                        if (video != null)
                        {
                            lstVideo.Add(video);
                        }
                    }
                    else
                    {
                        var queryVideo = db.From<vw_film_video>().Where(x => x.episode_id == episode.Id && x.status == 2);
                        if (queryVideo != null)
                        {
                            var video = db.Select(queryVideo).FirstOrDefault();
                            if (video != null) lstVideo.Add(video);
                        }
                    }
                }
                List<vw_film_video> videos = new List<vw_film_video>();
                if (!string.IsNullOrEmpty(page.limit.ToString()))
                {
                    if (page.limit >= lstVideo.Count)
                    {
                        page.limit = lstVideo.Count;
                    }
                    for (int i = 0; i < page.limit; i++)
                    {
                        videos.Add(lstVideo[i]);
                    }
                }
                return videos;
            }
        }


        #region ===== CATEGORY =====

        public List<vw_film_video> GetViewAllItem_mobile(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 20 };
            if (page.search == null) page.search = "";
            if (page.cpid == 0) { };
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dtStart = DateTime.Now;
                //var query = db.From<vw_film_video>();
                var query = db.From<film_video>();
                //if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                if (page.categoryid > 0)
                {
                    film_catalog catalog = new film_catalogService().GetByID(page.categoryid.ToString());
                    if (catalog != null)
                    {
                        //query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                        //&& (x.catalog_id == catalog.Id || y.catalogid == catalog.Id));
                        query.Join<film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                        && (x.catalog_id == catalog.Id || y.catalogid == catalog.Id));
                    }

                }
                //query.Where(e => e.filter == 1);
                query.Where(e => e.title.ToLower().Contains(page.search.ToLower()));
                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));
                query.Where(e => e.status == 2);
                query.Where(e => e.film_type == 0);//chỉ lấy phim lẻ
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => ((e.code != null) && e.title.Contains(page.search) || e.tags.Contains(page.search)));
                //if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                ////query = query.Where(e => e.status == 2).GroupBy(x => x.Id);
                //query = query.Where(e => e.status == 2);


                //List<film_video> rows = db.Select(query.Skip(offset).Take(limit)).ToList();
                var rows = db.Select(query.Select(e => e.Id).Skip(offset).Take(limit)).ToList();
                //List<vw_film_video> distinct = rows.Distinct().ToList();
                logItem("Step 3.1.0.1===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
                dtStart = DateTime.Now;

                List<vw_film_video> rows1 = new List<vw_film_video>();
                List<int> list = new List<int>();
                foreach (var item in rows)
                {
                    //list.Add(item.Id);
                    list.Add(item.Id);
                }
                List<int> distinct = list.Distinct().ToList();
                foreach (var item_dt in distinct)
                {
                    vw_film_video film = GetViewById(item_dt);
                    rows1.Add(film);
                }
                logItem("Step 3.1.0.2===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");

                return rows1;
            }
        }
        private void logItem(string s)
        {
            using (StreamWriter w = new StreamWriter("logDebugLong.txt", true))
            {
                w.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ":" + s);
            }
        }

        public List<vw_film_video_processing> SearchByTitle(string[] list_title)
        {
            List<vw_film_video_processing> lst = new List<vw_film_video_processing>();
            using (var db = _connectionData.OpenDbConnection())
            {
                for (int i = 0; i < list_title.Length; i++)
                {
                    var query = db.From<vw_film_video_processing>();
                    string title = CommService.TrimVietnameseMark(list_title[i]);
                    query.Where(e => e.title.Contains(list_title[i]) || e.title.Contains(title) || e.upload_file.Contains(title) || e.upload_file.Contains(list_title[i]));
                    List<vw_film_video_processing> rows = db.Select(query).ToList();
                    if (rows.Count > 0)
                    {
                        foreach (var item in rows)
                        {
                            lst.Add(item);
                        }
                    }
                }
                return lst;
            }
        }

        internal Dictionary<object, object> update_duration(int id, string title, string duration)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "", message = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                if (id > 0) query.Where(e => e.Id == id);
                if (!string.IsNullOrEmpty(title)) query.Where(e => e.title == title || e.upload_file.Contains(title));
                film_video obj = db.Select(query).SingleOrDefault();
                if (obj != null)
                {
                    var update_obj = obj;
                    update_obj.duration = duration + "";
                    LogService.logItem("update_duration");
                    LogService.logItem("duration:" + duration);
                    LogService.logItem(update_obj);
                    UpdateOrInsert(update_obj);
                    code = "success"; message = "Cập nhật duration thành công";
                }
                else
                {
                    code = "error"; message = "Không có film_id này : " + id;
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public long CountViewAllItem_mobile(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == 0) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                if (page.categoryid > 0)
                {
                    film_catalog catalog = new film_catalogService().GetByID(page.categoryid.ToString());
                    if (catalog != null)
                    {
                        query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                        && (x.catalog_id == catalog.Id || y.catalogid == catalog.Id));
                    }
                }
                //query.Where(e => e.filter == 1); 
                query.Where(e => e.title.Contains(page.search));
                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));
                query.Where(e => e.status == 2);
                long count = db.Count(query);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                if (count > limit) { count = limit; }
                //List<vw_film_video> rows = db.Select(query)
                //    .Skip(offset).Take(limit).ToList();
                return count;
            }
        }
        public List<vw_film_video> GetViewByLive(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == 0) { };
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                if (page.categoryid > 0)
                {
                    film_catalog catalog = new film_catalogService().GetByID(page.categoryid.ToString());
                    if (catalog != null)
                    {
                        query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                        && (x.catalog_id == catalog.Id || y.catalogid == catalog.Id));
                    }
                }
                query.Where(e => e.isview <= page.typekid);
                //if (page.typekid == 1)
                //{
                //    query.Where(e => e.isview == page.typekid); // seeding
                //}
                //query.Where(e => e.filter == 1);
                query.Where(e => e.title.Contains(page.search));
                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));
                query.Where(e => e.status == 2);
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                List<vw_film_video> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                //List<vw_film_video> distinct = rows.Distinct().ToList();
                List<vw_film_video> rows1 = new List<vw_film_video>();
                List<int> list = new List<int>();
                foreach (var item in rows)
                {
                    list.Add(item.Id);
                }
                List<int> distinct = list.Distinct().ToList();
                foreach (var item_dt in distinct)
                {
                    vw_film_video film = GetViewById(item_dt);
                    rows1.Add(film);
                }
                return rows1;
            }
        }

        public long CountViewAllItem_Episode(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            int offset = 0;
            try { offset = page.offset; }
            catch { }

            int limit = 10;//int.Parse(Request.Params["limit"]);
            try { limit = page.limit; }
            catch { }
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            List<vw_film_video> lstVideo = new List<vw_film_video>();
            using (var db = _connectionData.OpenDbConnection())
            {
                //lấy các tập
                var queryEpisode = db.From<vw_film_episode>().Where(e => e.status == 1);

                if (page.categoryid > 0) queryEpisode.Where(e => e.catalog_id == page.categoryid);

                List<vw_film_episode> listEpisode = db.Select(queryEpisode.Skip(offset).Take(limit)).ToList();

                //lặp qua các tập
                foreach (var episode in listEpisode)
                {
                    if (page.categoryid > 0)
                    {
                        var queryVideo = db.From<vw_film_video>();
                        queryVideo.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                        && (x.catalog_id == page.categoryid || y.catalogid == page.categoryid)
                        && x.episode_id == episode.Id && x.status == 2);
                        queryVideo = queryVideo.Where(e => e.isview > 0);
                        //queryVideo = queryVideo.Where(e => e.filter == 1 && e.isview > 0);
                        var video = db.Select(queryVideo).FirstOrDefault();
                        //var video = db.Select(queryVideo).FirstOrDefault();
                        if (video != null)
                        {
                            lstVideo.Add(video);
                        }
                    }
                    else
                    {
                        var queryVideo = db.From<vw_film_video>().Where(x => x.episode_id == episode.Id && x.status == 2);
                        var video = db.Select(queryVideo).FirstOrDefault();
                        if (video != null) lstVideo.Add(video);
                    }
                }
                return lstVideo.Count();
            }
        }
        public long CountViewByLive(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == 0) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                if (page.categoryid > 0)
                {
                    film_catalog catalog = new film_catalogService().GetByID(page.categoryid.ToString());
                    if (catalog != null)
                    {
                        query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                   && (x.catalog_id == catalog.Id || y.catalogid == catalog.Id));
                    }
                }
                query.Where(e => e.isview <= page.typekid); // seeding
                //query.Where(e => e.filter == 1); // seeding
                query.Where(e => e.title.Contains(page.search));
                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));
                query.Where(e => e.status == 2);
                query.Where(e => e.film_type == 0);//chỉ lấy phim lẻ
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                List<vw_film_video> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                List<vw_film_video> rows1 = new List<vw_film_video>();
                List<int> list = new List<int>();
                foreach (var item in rows)
                {
                    list.Add(item.Id);
                }
                List<int> distinct = list.Distinct().ToList();
                foreach (var item_dt in distinct)
                {
                    vw_film_video film = GetViewById(item_dt);
                    rows1.Add(film);
                }
                return rows1.Count();
            }
        }
        #endregion ===== CATEGORY =====
        public List<vw_film_video> GetViewHistory(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            //if (page.cpid == 0) ;
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                //var query = db.From<vw_film_video>().Join<vw_film_video_user_log>((x,y) => x.Id == y.film_id && y.user_id == page.userid && y.link_userid == page.link_id);



                var query = db.From<vw_film_video>()
                    .Join<film_video_user_log>((x, y) => x.Id == y.film_id && y.user_id == page.userid && y.link_userid == page.link_id)
                    .OrderByDescending<film_video_user_log>(x => x.date_updated);
                query.Where(e => e.status == 2);
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => ((e.code != null) && e.title.Contains(page.search) || e.tags.Contains(page.search)));
                //if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                ////query = query.Where(e => e.status == 2).GroupBy(x => x.Id);
                //query = query.Where(e => e.status == 2);
                query = query.Skip(offset).Take(limit);
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_video> GetRelatedVideo(string videos_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                var current_query = db.From<vw_film_video>().Where(e => e.Id == Int32.Parse(videos_id));

                vw_film_video current_video = db.Select(current_query).SingleOrDefault();
                var short_title = current_video.title.Substring(0, 10);
                query.Where(e => e.code.Contains(current_video.code) || e.title.Contains(short_title));
                if (current_video.tags != null)
                {
                    query.Where(e => e.tags.Contains(current_video.tags));
                }
                query.Where(e => e.status == 2);
                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));
                List<vw_film_video> rows = db.Select(query)
                    .Skip(0).Take(10).ToList();
                return rows;
            }
        }

        #region ===== NEW =====
        public List<vw_film_video> GetNewVideo(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                if (page.categoryid > 0)
                {
                    query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                    && (x.catalog_id == page.categoryid || y.catalogid == page.categoryid));
                }
                //if (page.link_id > 0)
                //{
                //    query.Join<vw_film_video, film_catalog>((x, y) => x.catalog_id == y.catalogid && (y.isview == page.typekid));
                //}
                query.Where(e => e.status == 2 && e.catalog_id != 5);
                //query.Where(e => e.status == 2 && e.filter==1 && e.catalog_id!=5);
                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));
                query.Skip(page.offset).Take(page.limit);
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_video> GetNewVideoByToken(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();

                if (page.categoryid > 0)
                {
                    query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                    && (x.catalog_id == page.categoryid || y.catalogid == page.categoryid));
                }
                else query.Where(e => e.catalog_id != 5); // không phải truyền hình

                //check token
                if (page.link_id > 0)
                {
                    if (page.typekid != 0) { query.Where(x => x.isview == 0 || x.isview == page.typekid); }
                }
                query.Where(e => e.status == 2);

                //query.Where(e => e.filter == 1 ); // seeding
                query.Skip(page.offset).Take(page.limit);
                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountViewNew(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();

                if (page.categoryid > 0)
                {
                    query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                    && (x.catalog_id == page.categoryid || y.catalogid == page.categoryid));
                }
                //check token
                if (page.link_id > 0)
                {
                    if (page.typekid != 0) { query.Where(x => x.isview == 0 || x.isview == page.typekid); }
                }
                query.Where(e => e.status == 2);

                //query.Where(e => e.filter == 1); // seeding

                query.OrderByDescending(x => (x.datecreated ?? DateTime.Now));

                long count = db.Count(query);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                if (count > limit) { count = limit; }
                //List<vw_film_video> rows = db.Select(query)
                //    .Skip(offset).Take(limit).ToList();
                return count;
            }
        }

        #endregion ===== NEW =====

        #region ===== RECOMMEND =====
        public List<vw_film_video> GetRecommendVideo(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                query.Where(x => (x.catalog_id != 5) && x.status == 2);
                if (page.link_id > 0)
                {
                    //query.Join<vw_film_video, film_catalog>((x, y) => x.catalog_id == y.catalogid && (y.isview == page.typekid));
                    if (page.typekid != 0) query = query.Where(x => x.isview == 0 || x.isview == page.typekid);
                }

                if (page.categoryid > 0)
                {
                    if (page.categoryid == 3)
                    {
                        query = query.Where(x => x.catalog_id_code == page.categoryid || x.catalog_id_code == 17);
                    }
                    else
                    {
                        query = query.Where(x => x.catalog_id_code == page.categoryid);
                    }
                }
                //query.Where(e => e.filter == 1); // seeding

                query.OrderByDescending(x => (System.Guid.NewGuid().ToString()));
                //List<vw_film_video> rows = db.Select(query)
                //    .Skip(page.offset).Take(page.limit).ToList();

                query = query.Skip(page.offset).Take(page.limit);
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }

        }
        public List<vw_film_video> GetRecommendVideoByToken(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                query.Where(x => (x.catalog_id != 5) && x.status == 2);
                query.OrderByDescending(x => (System.Guid.NewGuid().ToString()));
                if (page.link_id > 0)
                {
                    if (page.typekid != 0) query = query.Where(x => x.isview == 0 || x.isview <= page.typekid);
                }
                if (page.categoryid > 0)
                {
                    film_catalog catalog = new film_catalogService().GetByCode(page.categoryid.ToString());
                    if (catalog != null)
                    {
                        if (catalog.Id == 1)
                        {
                            query = query.Join<vw_film_video, film_catalog_film>((x, y) => x.Id == y.filmid
                            && (y.catalogid == 1) && x.status == 2);
                        }
                        else
                        {
                            query = query.Where(e => e.catalog_id == catalog.Id);
                        }
                    }
                }
                //query = query.Where(e => e.filter == 1); // seeding

                query = query.Skip(page.offset).Take(page.limit);
                //List<vw_film_video> rows = db.Select(query)
                //    .Skip(page.offset).Take(page.limit).ToList();
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        #endregion ===== RECOMMEND =====

        internal List<vw_film_video> GetAllEpisodeByVideoId(string video_id, int start_index = 0, int max_result = 10)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                var current_query = db.From<vw_film_video>().Where(e => e.code == video_id);
                vw_film_video current_video = db.Select(current_query).SingleOrDefault();
                query.Where(x => x.episode_id == current_video.episode_id);
                query.OrderByDescending(x => (x.title)).Skip(start_index).Take(max_result);
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        internal List<vw_film_video> GetAllEpisodeByFilmId(int film_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                var current_query = db.From<vw_film_video>().Where(e => e.Id == film_id);
                vw_film_video current_video = db.Select(current_query).SingleOrDefault();
                query.Where(x => x.episode_id == current_video.episode_id);
                query.OrderByDescending(x => (x.title));
                query.Where(e => e.status == 2);
                List<vw_film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            if (page.cpid == -1) page.cpid = 0;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                if (page.categoryid > 0) query = query.Where(e => e.catalog_id == page.categoryid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search) || e.tags.Contains(page.search)));
                if (page.cpid > 0) query = query.Where(e => e.cpid == page.cpid);
                query = query.Where(e => e.status == 2);

                /*
                  if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
                */

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                if (page.categoryid > 0) query = query.Where(e => e.catalog_id == page.categoryid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                // int cpid = -1;
                if (page.cpid > 0) query = query.Where(e => e.cpid == page.cpid);

                /*
                  if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
                */

                return db.Count(query);
            }
        }
        public film_video GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public List<vw_film_video> GetListViewByID(PagingModel page)
        {

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>().Where(e => e.catalog_id == page.categoryid);
                query.Where(e => e.status == 2);
                List<vw_film_video> rows = db.Select(query).Skip(0).Take(page.limit).ToList();
                return rows;
            }
        }
        public vw_film_video GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).FirstOrDefault();
            }
        }
        public vw_film_video GetViewById(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>().Where(e => e.Id == id);
                return db.Select(query).FirstOrDefault();
            }
        }
        public vw_film_video GetViewByAliasID(string code)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>().Where(e => e.code == code);
                query.Where(e => e.status == 2);
                return db.Select(query).FirstOrDefault();
            }
        }
        public int UpdateOrInsert(film_video obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.desc = obj.desc;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.seriid = comm.GetUserId();
                        objUpdate.imdb = obj.imdb;
                        objUpdate.format = obj.format;
                        objUpdate.sub_type = obj.sub_type;
                        objUpdate.publish_year = obj.publish_year;
                        objUpdate.publish_countryid = obj.publish_countryid;
                        objUpdate.duration = obj.duration;
                        objUpdate.actor = obj.actor;
                        objUpdate.director = obj.director;
                        objUpdate.film_type = obj.film_type;
                        objUpdate.episode = obj.episode;
                        objUpdate.episode_current = obj.episode_current;
                        objUpdate.contract_copyright = obj.contract_copyright;
                        objUpdate.contract_exprired = DateTime.Now;
                        objUpdate.contract_appendix = obj.contract_appendix;
                        objUpdate.copyright_appendix = obj.copyright_appendix;
                        if (obj.copyright_expired != null)
                        {
                            objUpdate.copyright_expired = obj.copyright_expired;
                        }
                        objUpdate.catalog_id = obj.catalog_id;

                        if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                        {
                            //objUpdate.upload_file = "http://115.146.121.190:5003/" + obj.upload_file;
                            objUpdate.upload_file = "http://115.146.121.190:1935/vod/_definst_/" + obj.upload_file + "/playlist.m3u8";
                        }
                        else { objUpdate.upload_file = obj.upload_file; }
                        //objUpdate.thumb_file = "http://115.146.121.190:5001/" + obj.thumb_file;
                        if (obj.thumb_file != null && !obj.thumb_file.StartsWith("http"))
                        {
                            objUpdate.thumb_file = "http://115.146.121.190:5001/" + obj.thumb_file;
                        }
                        objUpdate.exclusive = obj.exclusive;
                        objUpdate.price = obj.price;
                        objUpdate.status = obj.status;
                        objUpdate.cpid = obj.cpid;
                        int a = db.Update(objUpdate);
                        if (a > 0) return obj.Id;
                        return 0;
                    }
                    return -1;
                }
                else
                {
                    //var queryCount = db.From<film_video>()
                    //	.Where(e => e.title == obj.title).Select(e => e.Id);
                    //var objCount = db.Count(queryCount);
                    //if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.desc = obj.desc;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.seriid = comm.GetUserId();
                    objUpdate.imdb = obj.imdb;
                    objUpdate.format = obj.format;
                    objUpdate.sub_type = obj.sub_type;
                    objUpdate.publish_year = obj.publish_year;
                    objUpdate.publish_countryid = obj.publish_countryid;
                    objUpdate.duration = obj.duration;
                    objUpdate.actor = obj.actor;
                    objUpdate.director = obj.director;
                    objUpdate.film_type = obj.film_type;
                    objUpdate.episode = obj.episode;
                    objUpdate.episode_current = obj.episode_current;
                    objUpdate.contract_copyright = obj.contract_copyright;
                    objUpdate.contract_exprired = DateTime.Now;
                    objUpdate.contract_appendix = obj.contract_appendix;
                    //objUpdate.copyright_appendix = obj.copyright_appendix;
                    if (obj.copyright_expired != null)
                    {
                        objUpdate.copyright_expired = obj.copyright_expired;
                    }
                    objUpdate.catalog_id = obj.catalog_id;
                    //objUpdate.upload_file = "http://115.146.121.190:5001/" + obj.upload_file;

                    if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                    {
                        //objUpdate.upload_file = "http://115.146.121.190:5001/" + obj.upload_file;
                        objUpdate.upload_file = "http://115.146.121.190:1935/vod/_definst_/" + obj.upload_file + "/playlist.m3u8";
                    }
                    else { objUpdate.upload_file = obj.upload_file; }
                    if (obj.thumb_file != null && !obj.thumb_file.StartsWith("http"))
                    {
                        objUpdate.thumb_file = "http://115.146.121.190:5001/" + obj.thumb_file;
                    }

                    objUpdate.exclusive = obj.exclusive;
                    objUpdate.price = obj.price;
                    objUpdate.status = obj.status;
                    objUpdate.cpid = obj.cpid;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int UpdateStatus(film_video obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.status = obj.status;
                        return db.Update(objUpdate);
                    }

                }
            }
            return -1;
        }
        public int UpdateCheckbox(int[] catalog_id, int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //xóa hết catalog id của film id
                var query = db.From<film_catalog_film>().Where(e => e.filmid == filmid);
                db.Delete(query);

                //lặp qua mảng id
                foreach (var catalogid in catalog_id)
                {
                    var obj = new film_catalog_film();
                    obj.filmid = filmid;
                    obj.catalogid = catalogid;
                    db.Insert<film_catalog_film>(obj);
                }
                return -1;
            }
        }
        public int UpdateImage(string url, int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //inset img vào film_video_image
                var query = db.From<film_video_image>().Where(e => e.filmid == filmid);
                var obj = new film_video_image();
                obj.url = "http://115.146.121.190:5001/" + url;
                obj.filmid = filmid;
                db.Insert<film_video_image>(obj);
                // Console.WriteLine("hello world.1..");

                var query1 = db.From<film_video>().Where(e => e.Id == filmid);
                // Console.WriteLine("hello world.2..");
                var objUpdate = db.Select(query1).SingleOrDefault();
                if (objUpdate.thumb_file == null)
                {
                    objUpdate.thumb_file = "http://115.146.121.190:5001/" + url;
                    db.Update<film_video>(objUpdate);
                }
                // neu thumb_file != null thi xoa tat ca thumb_file trong film_video di roi update file url moi hien thi trong anh dai dien giao dien admin
                else
                {
                    var delete = db.From<film_video>().Where(e => e.thumb_file == url);
                    db.Delete(delete);
                    objUpdate.thumb_file = "http://115.146.121.190:5001/" + url;
                    db.Update<film_video>(objUpdate);
                }
            }
            return -1;

        }
        public List<film_video_image> getupdateImg(int Filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.filmid == Filmid);
                return db.Select(query).ToList<film_video_image>();
            }
        }
        public int deteleimage(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.Id == id);
                return db.Delete(query);
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_video InitEmpty()
        {
            var obj = new film_video();
            obj.Id = 0;
            obj.thumb_file = "";
            obj.upload_file = "";

            return obj;
        }
        public List<vw_film_video> GetAllByTypeKid(PagingModel page, int isview)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10000 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                if (isview != 0)
                {
                    query = query.Where(e => e.isview == 0 || e.isview == isview);
                }
                query = query.Skip(offset).Take(limit);
                List<vw_film_video> rows = db.Select(query)
                   .ToList();
                return rows;
            }
        }
        public List<vw_film_video> GetViewAllItemAPI(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10000 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Skip(offset).Take(limit);
                List<vw_film_video> rows = db.Select(query)
                    .ToList();
                return rows;
            }
        }
    }
}