﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_catalogService : LamltService
    {

        public film_catalogService()
        {

        }
        public List<film_catalog> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                query = query.Skip(offset).Take(limit);
                List<film_catalog> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_catalog> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_catalog>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                query = query.Skip(offset).Take(limit);
                List<vw_film_catalog> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }

        internal List<vw_film_catalog> GetCateChild(int cateparrent_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_catalog>().Where(e => e.catalogid == cateparrent_id);
                return db.Select(query).ToList();
            }
        }
        internal List<vw_film_catalog> GetAllCateChild()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_catalog>().Where();
                return db.Select(query).ToList();
            }
        }


        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_catalog>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public film_catalog GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public film_catalog GetByCode(string code)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog>().Where(e => e.code.ToString() == code);
                return db.Select(query).SingleOrDefault();
            }
        }
        public List<film_video> GetAllVideoByCateSlug(string slug, int start, int limit)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (slug == null)
                {
                    return db.Select(db.From<film_video>().OrderByDescending(x => x.datecreated)).Skip(start).Take(limit).ToList();
                }
                start = start > 0 ? start : 0;
                limit = limit > 0 ? limit : 10;
                slug = slug.Replace("-", " ");
                switch (slug)
                {
                    //Giải thích code: && e.catalog_id != 5 vì 5 có title là Truyền hình
                    case "hot":
                        return db.Select(db.From<film_video>().Where(e => e.catalog_id != 5).OrderByDescending(e => e.datecreated).Skip(start).Take(limit)).ToList();
                    case "featured":
                    case "new":
                        goto ketqua;
                    moi: start = new Random().Next(0, 10); goto ketqua;
                    ketqua: return db.Select(db.From<film_video>().Where(e => e.showhome == 1 && e.catalog_id != 5).OrderByDescending(e => e.datecreated).Skip(start).Take(limit)).ToList();
                    default:
                        film_catalog cate = db.Select(db.From<film_catalog>().Where(e => e.title.Contains(slug))).SingleOrDefault();
                        if (cate != null)
                        {

                            return db.Select(db.From<film_video>().Where(e => e.catalog_id == cate.Id && e.catalog_id != 5)).Skip(start).Take(limit).ToList();
                        }
                        goto moi;
                }

            }
        }
        public vw_film_catalog GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_catalog>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_catalog obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_catalog>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.desc = obj.desc;
                        objUpdate.title = obj.title;

                        objUpdate.userid = comm.GetUserId();
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.catalogid = obj.catalogid;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_catalog>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.desc = obj.desc;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.catalogid = obj.catalogid;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.title = obj.title;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_catalog InitEmpty()
        {
            var obj = new film_catalog();
            obj.Id = 0;
            return obj;
        }


        public List<film_catalog> GetAllByTypeKid(PagingModel page, int isview)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10000 };
            if (page.search == null) page.search = "";
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                if (isview == 0)
                {
                    query = query.Where(e => e.isview == isview);
                }
                else
                {
                    query = query.Where(e => e.isview == isview || e.isview == 0);
                }
                query = query.Skip(offset).Take(limit);
                List<film_catalog> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_banner> GetAllByTypeKid(int catalogid, int isview, int ipad)
        {
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                query.OrderByDescending(x => x.Id);
                if (ipad == 1)
                {
                    query = query.Where(e => e.type_client == 3);
                }
                else
                {
                    query = query.Where(e => e.type_client == 2);
                }

                if (catalogid != -1)
                {
                    query = query.Where(e => (e.display == catalogid));
                    //query = query.Where(e => (e.show_home !=1));
                    if (isview != 0)
                    {
                        if (isview == 1)
                        {
                            query = query.Where(e => e.typekid == isview);
                        }
                        else
                        {
                            query = query.Where(e => e.typekid >= 0);
                        }
                    }
                }
                else
                {
                    query = query.Where(e => (e.display == catalogid));
                    query = query.Where(e => e.show_home > 0);
                    //query = query.Where(e => e.typekid <= isview);
                    query = query.Where(e => e.typekid <= isview);
                }
                List<vw_film_banner> rows = db.Select(query).ToList();
                return rows;
            }
        }
    }
}