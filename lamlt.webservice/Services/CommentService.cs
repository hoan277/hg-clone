﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_commentsService : LamltService
    {

        public film_commentsService()
        {

        }
        public List<film_comments> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_comments>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                query = query.Skip(offset).Take(limit);
                List<film_comments> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_comments> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_comments>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.content.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                query = query.Skip(offset).Take(limit);
                List<vw_film_comments> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_comments>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public film_comments GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_comments>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_comments GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_comments>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public List<vw_film_comments> GetAllCommentByFilmId(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_comments>().Where(e => e.film_id == id);
                return db.Select(query);
            }
        }
        public List<vw_film_comments> GetAllCommentByFilmId(int id, int start, int max)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_comments>().Where(e => e.film_id == id).OrderByDescending(e => e.Id).Skip(start).Take(max);
                return db.Select(query);
            }
        }
        public long GetAllCommentByFilmId_Count(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_comments>().Where(e => e.film_id == id).Select(e => e.Id);
                return db.Count(query);
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(film_comments obj, string token)
        {
            if (token == null) return -1;
            using (var db = _connectionData.OpenDbConnection())
            {
                film_user current_film_user = db.Select(db.From<film_user>().Where(e => e.token == token)).FirstOrDefault();
                if (current_film_user.token == null && current_film_user.token_expired < DateTime.Now)
                {
                    return -1;
                }
                if (obj.Id > 0)
                {
                    var objUpdate = db.Select(db.From<film_comments>().Where(e => e.Id == obj.Id)).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.userid = current_film_user.Id;
                        objUpdate.film_id = obj.film_id;
                        objUpdate.full_name = current_film_user.fullname;
                        objUpdate.phone = current_film_user.phone;
                        objUpdate.email = current_film_user.email;
                        objUpdate.title = obj.title;
                        objUpdate.content = obj.content;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid_created = current_film_user.userid.Value;
                        objUpdate.like = 0;
                        objUpdate.dislike = 0;
                        objUpdate.status = 0;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    //var queryCount = db.From<film_comments>()
                    //    .Where(e => e.title == obj.title).Select(e => e.Id);
                    //var objCount = db.Count(queryCount);
                    //if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    // Check nếu không có token và token hết hạn

                    objUpdate.userid = current_film_user.Id;
                    objUpdate.film_id = obj.film_id;
                    objUpdate.full_name = current_film_user.fullname;
                    objUpdate.phone = current_film_user.phone;
                    objUpdate.email = current_film_user.email;
                    objUpdate.title = obj.title;
                    objUpdate.content = obj.content;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid_created = current_film_user.userid.Value;
                    objUpdate.like = 0;
                    objUpdate.dislike = 0;
                    objUpdate.status = 0;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_comments>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_comments InitEmpty()
        {
            var obj = new film_comments();
            obj.Id = 0;
            return obj;
        }
        public int UpdateStatus(film_comments obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_comments>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.status = obj.status;
                        return db.Update(objUpdate);
                    }

                }
            }
            return -1;
        }
        /*
		genvie here
		create view vw_film_comments
		as
			select film_comments.Id,film_comments.title,film_comments.desc,film_comments.datecreated,film_comments.userid,film_comments.catalogid   ,film_comments.title as catalogid_title  from film_comments   inner join film_comments on film_comments.catalogid=film_comments.id  
		*/
    }
}