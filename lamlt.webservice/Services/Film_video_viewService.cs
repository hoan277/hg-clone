﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_viewService : LamltService
    {

        public film_video_viewService() { }

        public List<film_video_view> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_view>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Skip(offset).Take(limit);
                List<film_video_view> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public film_video_view GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_view>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_video_view GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_view>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
            }
            return -1;
        }
        public int UpdateOrInsertApi(film_video_view obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query1 = db.From<film_video_view>().Where(e => e.videoid == obj.videoid && e.userid == obj.userid).OrderByDescending(e => e.dateupdated);
                var objUpdate = db.Select(query1).SingleOrDefault();
                if (objUpdate != null)
                {
                    //objUpdate.Id = obj.Id;
                    objUpdate.videoid = obj.videoid;
                    objUpdate.userid = obj.userid;
                    objUpdate.status = obj.status;
                    objUpdate.view = obj.view;
                    objUpdate.custom_duration = obj.custom_duration;
                    objUpdate.time_start = obj.time_start;
                    objUpdate.time_end = obj.time_end;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    int a = db.Update(objUpdate);
                    if (a > 0) return obj.Id;
                    return 0;
                }
                else
                {
                    var obj1 = InitEmpty();
                    obj1.videoid = obj.videoid;
                    obj1.userid = obj.userid;
                    obj1.status = obj.status;
                    obj1.view = obj.view;
                    obj1.custom_duration = obj.custom_duration;
                    obj1.time_start = 0;
                    obj1.time_end = obj.time_end;
                    obj1.datecreated = DateTime.Now;
                    obj1.dateupdated = DateTime.Now;
                    return (int)db.Insert(obj1, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_view>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_video_view InitEmpty()
        {
            var obj = new film_video_view();
            obj.Id = 0;
            return obj;
        }
        public long CountVideoView(PagingModel page)
        {
            if (page.search == null) { page.search = ""; }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 100;
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                return db.Count(query);
            }
        }

        public film_video_view GetByUserAndFilm(int userid, int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_view>().Where(e => e.userid == userid && e.videoid == filmid).OrderByDescending(e => e.dateupdated);
                return db.Select(query).FirstOrDefault();
            }
        }

    }
}