﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_providerService : LamltService
    {
        public film_providerService()
        {
        }

        public List<film_provider> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_provider>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/
                query = query.Skip(offset).Take(limit);
                List<film_provider> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_provider> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_provider>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/
                query = query.Skip(offset).Take(limit);
                List<vw_film_provider> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_provider>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        internal object getby_subtate(int sub_state)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "", scode = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var q = db.From<film_provider>();
                if (sub_state > 0) q.Where(e => e.Id == sub_state);
                List<film_provider> lst = db.Select(q);
                if (lst.Count > 0)
                {
                    scode = "success";
                    message = "Lấy dữ liệu thành công";
                    dict.Add("length", lst.Count);
                    dict.Add("provider", lst);
                }
                else
                {
                    scode = "error";
                    message = "Không tìm thấy gói cước nào phù hợp với dữ liệu nhập vào";
                }
            }
            dict.Add("code", scode);
            dict.Add("message", message);
            return dict;
        }

        public Dictionary<object, object> GetAll(int id, string title, string code)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "", scode = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var q = db.From<film_provider>();
                if (id > 0) q.Where(e => e.Id == id);
                if (!string.IsNullOrEmpty(title)) q.Where(e => e.title == title || e.title.Contains(title));
                if (!string.IsNullOrEmpty(code)) q.Where(e => e.code == code);
                List<film_provider> lst = db.Select(q);
                if (lst.Count > 0)
                {
                    scode = "success";
                    message = "Lấy dữ liệu thành công";
                    dict.Add("length", lst.Count);
                    dict.Add("provider", lst);
                }
                else
                {
                    scode = "error";
                    message = "Không tìm thấy gói cước nào phù hợp với dữ liệu nhập vào";
                }
            }
            dict.Add("code", scode);
            dict.Add("message", message);
            return dict;
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_provider>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public film_provider GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_provider>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_provider GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_provider>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_provider obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_provider>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.code = obj.code;
                        objUpdate.datecreated = obj.datecreated;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_provider>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.code = obj.code;
                    objUpdate.datecreated = obj.datecreated;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_provider>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_provider InitEmpty()
        {
            var obj = new film_provider();
            obj.Id = 0;
            return obj;
        }

        /*
		genvie here
		create view vw_film_provider
		as
			select film_provider.Id,film_provider.title,film_provider.code,film_provider.datecreated  from film_provider
		*/
    }
}