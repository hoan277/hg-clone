﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class adsService : LamltService
    {

        public adsService()
        {

        }
        public List<ads> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<ads>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                query = query.Skip(offset).Take(limit);
                List<ads> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_ads> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_ads>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                query = query.Skip(offset).Take(limit);
                List<vw_ads> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<ads>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }

        internal Dictionary<object, object> GetAds(int id, string title, string type, string ads_type, string item_type, int offset = 0, int limit = 10, string orderby = "datecreated")
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "", message = "";
            List<vw_ads> rs = new List<vw_ads>();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_ads>();
                if (id > 0) query.Where(e => e.Id == id);
                if (!string.IsNullOrEmpty(title)) query.Where(e => e.title.Contains(title) || e.title == title);
                if (!string.IsNullOrEmpty(type)) query.Where(e => e.type.Contains(type) || e.type == type);
                if (!string.IsNullOrEmpty(ads_type)) query.Where(e => e.ads_type.Contains(type) || e.ads_type == ads_type);
                if (!string.IsNullOrEmpty(item_type)) query.Where(e => e.item_type.Contains(type) || e.item_type == item_type);
                switch (orderby)
                {
                    case "id": query.OrderByDescending(e => e.Id); break;
                    case "title": query.OrderByDescending(e => e.title); break;
                    case "random": query.OrderByRandom(); break;
                    case "datecreated":
                    default: query.OrderByDescending(e => e.datecreated); break;
                }
                if (offset > 0) query.Skip(offset);
                if (limit > 0) query.Take(limit);
                rs = db.Select(query);
                code = rs.Count > 0 ? "success" : "error";
                message = rs.Count > 0 ? "Láy dữ liệu thành công" : "Không có dữ liệu hoặc không tồn tại dữ liệu phù hợp với điều kiện bạn vừa tìm kiếm";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            if (rs.Count > 0)
            {
                dict.Add("length", rs.Count);
                dict.Add("list_ads", rs);
            }
            return dict;
        }

        public ads_extend GetAds1(int id, string title, string type, string ads_type, string item_type, int offset, int limit, string orderby, int android, int typekid)
        {
            ads_extend ads_ex = new ads_extend();
            string code = "", message = "";
            List<vw_ads> rs = new List<vw_ads>();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_ads>();
                if (id > 0) query.Where(e => e.Id == id);
                if (!string.IsNullOrEmpty(title)) query.Where(e => e.title.Contains(title) || e.title == title);
                if (!string.IsNullOrEmpty(type)) query.Where(e => e.type.Contains(type) || e.type == type);
                if (!string.IsNullOrEmpty(ads_type)) query.Where(e => e.ads_type.Contains(ads_type) || e.ads_type == ads_type);
                if (!string.IsNullOrEmpty(item_type)) query.Where(e => e.item_type.Contains(item_type) || e.item_type == item_type);
                if (android > 0) query.Where(e => e.is_android == android);
                if (typekid > 0) query.Where(e => e.typekid <= typekid);
                switch (orderby)
                {
                    case "id": query.OrderByDescending(e => e.Id); break;
                    case "title": query.OrderByDescending(e => e.title); break;
                    case "random": query.OrderByRandom(); break;
                    case "datecreated":
                    default: query.OrderByDescending(e => e.datecreated); break;
                }
                if (offset > 0) query.Skip(offset);
                if (offset > 0) query.Take(offset);
                rs = db.Select(query);
                code = rs.Count > 0 ? "success" : "error";
                message = rs.Count > 0 ? "Láy dữ liệu thành công" : "Không có dữ liệu hoặc không tồn tại dữ liệu phù hợp với điều kiện bạn vừa tìm kiếm";
                ads_ex.code = code;
                ads_ex.message = message;
                if (rs.Count > 0)
                {
                    ads_ex.length = rs.Count;
                    ads_ex.list_vw_ads = rs;
                }
            }
            return ads_ex;
        }


        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_ads>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public ads GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<ads>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public ads GetByFilmId(int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<ads>().Where(e => e.item_id == filmid.ToString());
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_ads GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_ads>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(ads obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<ads>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.ads_id = obj.ads_id;
                        objUpdate.ads_type = obj.ads_type;
                        objUpdate.item_id = obj.item_id;
                        objUpdate.item_type = obj.item_type;
                        objUpdate.content = obj.content;
                        objUpdate.size = obj.size;
                        objUpdate.note = obj.note;
                        objUpdate.datecreated = obj.datecreated;
                        objUpdate.status = obj.status;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<ads>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.ads_id = obj.ads_id;
                    objUpdate.ads_type = obj.ads_type;
                    objUpdate.item_id = obj.item_id;
                    objUpdate.item_type = obj.item_type;
                    objUpdate.content = obj.content;
                    objUpdate.size = obj.size;
                    objUpdate.note = obj.note;
                    objUpdate.datecreated = obj.datecreated;
                    objUpdate.status = obj.status;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<ads>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public ads InitEmpty()
        {
            var obj = new ads();
            obj.Id = 0;
            return obj;
        }
        //public string GetVASTByFilmId(int filmid)
        //{
        //    bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
        //    string path = "/home/www/data/data/media/automedia/vast/";
        //    if (isDev)
        //    {
        //        path = "D:\\";
        //    }
        //    XmlWriterSettings settings = new XmlWriterSettings { Indent = true };
        //    string file = path + "vast" + "_" + filmid + ".xml";
        //    using (XmlWriter writer = XmlWriter.Create(file, settings))
        //    {
        //        writer.WriteStartElement("VAST"); writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance"); writer.WriteAttributeString("xsi", "noNamespaceSchemaLocation", null, "vast3_draft.xsd");
        //        writer.WriteStartElement("Ad"); writer.WriteAttributeString("id", "midroll-1"); writer.WriteAttributeString("sequence", "1");
        //        writer.WriteStartElement("InLine");
        //        writer.WriteStartElement("AdSystem"); writer.WriteAttributeString("version", "2.0"); writer.WriteValue("JW Player"); writer.WriteEndElement();
        //        writer.WriteStartElement("AdTitle"); writer.WriteValue("Static Tag"); writer.WriteEndElement();
        //        writer.WriteStartElement("Error"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif?err=[ERRORCODE]"); writer.WriteEndElement();
        //        writer.WriteStartElement("Impression"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Creatives");
        //        writer.WriteStartElement("Creative"); writer.WriteAttributeString("sequence", "1");
        //        writer.WriteStartElement("Linear");
        //        writer.WriteStartElement("Duration"); writer.WriteValue("00:00:15"); writer.WriteEndElement();
        //        writer.WriteStartElement("TrackingEvents");
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "start"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "firstQuartile"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "midpoint"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "thirdQuartile"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "complete"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "pause"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "mute"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteStartElement("Tracking"); writer.WriteAttributeString("event", "fullscreen"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteEndElement();
        //        // end tracking
        //        writer.WriteStartElement("VideoClicks");
        //        writer.WriteStartElement("ClickThrough"); writer.WriteValue("http://www.jwplayer.com"); writer.WriteEndElement();
        //        writer.WriteStartElement("ClickTracking"); writer.WriteValue("http://demo.jwplayer.com/static-tag/pixel.gif"); writer.WriteEndElement();
        //        writer.WriteEndElement();
        //        //end video click
        //        writer.WriteStartElement("MediaFiles");
        //        writer.WriteStartElement("MediaFile"); writer.WriteAttributeString("id", "1"); writer.WriteAttributeString("delivery", "progressive"); writer.WriteAttributeString("type", "video/mp4"); writer.WriteAttributeString("bitrate", "400"); writer.WriteAttributeString("width", "640"); writer.WriteAttributeString("height", "360"); writer.WriteValue("http://localhost/ads/testAd.mp4"); writer.WriteEndElement();
        //        writer.WriteEndElement();
        //        //end medifile
        //        writer.WriteEndElement();
        //        // line con
        //        writer.WriteEndElement();
        //        //end creative
        //        writer.WriteEndElement();
        //        writer.WriteStartElement("Creative");
        //        writer.WriteStartElement("CompanionAds");
        //        writer.WriteStartElement("Companion"); writer.WriteAttributeString("id", "1"); writer.WriteAttributeString("width", "300"); writer.WriteAttributeString("height", "250");
        //        writer.WriteStartElement("StaticResource"); writer.WriteAttributeString("creativeType", "image/jpg"); writer.WriteValue("http://demo.jwplayer.com/static-tag/jwplayer-rectangle.jpg"); writer.WriteEndElement();
        //        writer.WriteStartElement("CompanionClickThrough"); writer.WriteValue("http://www.jwplayer.com"); writer.WriteEndElement();
        //        // end StaticResource
        //        writer.WriteEndElement();
        //        // end companion1
        //        writer.WriteStartElement("Companion"); writer.WriteAttributeString("id", "2"); writer.WriteAttributeString("width", "728"); writer.WriteAttributeString("height", "90");
        //        writer.WriteStartElement("StaticResource"); writer.WriteAttributeString("creativeType", "image/jpg"); writer.WriteValue("http://demo.jwplayer.com/static-tag/jwplayer-leaderboard.jpg"); writer.WriteEndElement();
        //        writer.WriteStartElement("CompanionClickThrough"); writer.WriteValue("http://www.jwplayer.com"); writer.WriteEndElement();
        //        // end StaticResource
        //        writer.WriteEndElement();
        //        // end companion1
        //        writer.WriteEndElement();
        //        // end CompanionAds
        //        writer.WriteEndElement();
        //        // end Creative
        //        //end creatives
        //        //inline
        //        writer.WriteEndElement();
        //        writer.WriteEndElement();
        //        writer.WriteEndElement();
        //        writer.Flush();
        //    }
        //    return file;
        //}
        public string GetVASTByFilmId1(ads ads_item)
        {
            if (ads_item == null) return null;
            string time_skip = convert_time(ads_item.time_show.Value);
            string duration = convert_time(ads_item.duration.Value);
            string content = ads_item.content;
            string embedded = ads_item.embedded;
            string result = "<?xml version='1.0' encoding='UTF-8'?>" +
"<VAST xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='vast.xsd' version='2.0'>" +
 "<Ad id='24283604'>" +
  "<InLine>" +
   "<AdSystem>GDFP</AdSystem>" +
   "<AdTitle>IAB Vast Samples Skippable</AdTitle>" +
   "<Description><![CDATA[IAB Vast Samples Skippable ad]]></Description>" +
   "<Error><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=videoplayfailed]]></Error>" +
   "<Impression><![CDATA[http://pubads.g.doubleclick.net/pcs/view?xai=AKAOjsv-nzXfxSSzGVz86wLSz7wSMYz_l1E6FrGm5X95N1G4-3sH9_gepEx7T_pHb4yJNMaS0eLtavIlFQiFYg0ePhG9EodL6CwMvTWrLRBZopOjGy0yoKDE5Jdd1CN-f33YniTpp9t2WoAG1NJDyRZ6lM7DsLWuTp_qKqZIZjPy0N2QH5I6oDIseRtGVaMFiBZOPa0Oy2mOxyQtG0w7bn1vX7ULXkI98lmgiB5aeDgA_sqwY56G-Ar_qyCqg1U6&sig=Cg0ArKJSzPxi-7u6J8hFEAE&adurl=]]></Impression>" +
   "<Creatives>" +
    "<Creative id='32948875124' AdID='ABCD1234567' sequence='1'>" +
     "<Linear skipoffset='" + time_skip + "'>" +
      "<Duration>" + duration + "</Duration>" +
      "<TrackingEvents>" +
       "<Tracking event='start'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=part2viewed&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='firstQuartile'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=videoplaytime25&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='midpoint'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=videoplaytime50&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='thirdQuartile'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=videoplaytime75&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='complete'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=videoplaytime100&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='mute'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=admute&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='unmute'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=adunmute&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='rewind'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=adrewind&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='pause'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=adpause&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='resume'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=adresume&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='creativeView'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=vast_creativeview&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='fullscreen'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=adfullscreen&ad_mt=[AD_MT]]]></Tracking>" +
       "<Tracking event='acceptInvitation'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=acceptinvitation]]></Tracking>" +
      "</TrackingEvents>" +
      "<VideoClicks>" +
       "<ClickThrough id='GDFP'><![CDATA[" + embedded + "]]></ClickThrough>" +
      "</VideoClicks>" +
      "<MediaFiles>" +
       "<MediaFile id='GDFP' delivery='progressive' width='640' height='360' type='video/mp4' bitrate='324' scalable='true' maintainAspectRatio='true'><![CDATA[" + content + "]]></MediaFile>" +
       "<MediaFile id='GDFP' delivery='progressive' width='640' height='360' type='video/webm' bitrate='348' scalable='true' maintainAspectRatio='true'><![CDATA[http://redirector.gvt1.com/videoplayback/id/8bd2c53a3e15469d/itag/43/source/gfp_video_ads/acao/yes/mime/video%2Fwebm/ctier/L/ip/0.0.0.0/ipbits/0/expire/1599121841/sparams/ip,ipbits,expire,id,itag,source,acao,mime,ctier/signature/64A5B840B35798AC2C06827ECBD759452BD55A0E.4844D36C514EAAA350FE85EDD7A430047E3F7FCE/key/ck2/file/file.webm]]></MediaFile>" +
       "<MediaFile id='GDFP' delivery='progressive' width='320' height='180' type='video/3gpp' bitrate='234' scalable='true' maintainAspectRatio='true'><![CDATA[http://redirector.gvt1.com/videoplayback/id/8bd2c53a3e15469d/itag/36/source/gfp_video_ads/acao/yes/mime/video%2F3gpp/ctier/L/ip/0.0.0.0/ipbits/0/expire/1599121841/sparams/ip,ipbits,expire,id,itag,source,acao,mime,ctier/signature/2BA7779D0B589147AC6A0DD0D435F0D298D405B6.99781E2CE4CFD5F61BF2F351E400BE63CA41C86C/key/ck2/file/file.3gp]]></MediaFile>" +
       "<MediaFile id='GDFP' delivery='progressive' width='176' height='144' type='video/3gpp' bitrate='86' scalable='true' maintainAspectRatio='true'><![CDATA[http://redirector.gvt1.com/videoplayback/id/8bd2c53a3e15469d/itag/17/source/gfp_video_ads/acao/yes/mime/video%2F3gpp/ctier/L/ip/0.0.0.0/ipbits/0/expire/1599121841/sparams/ip,ipbits,expire,id,itag,source,acao,mime,ctier/signature/2D8C1D1C9E9CAA9DF82FD3DCD6978229AA28301C.987C2A47236802CD19B724908D0A87563722AA52/key/ck2/file/file.3gp]]></MediaFile>" +
      "</MediaFiles>" +
     "</Linear>" +
    "</Creative>" +
    "<Creative id='32948875244' sequence='1'>" +
     "<CompanionAds>" +
      "<Companion id='32948875244' width='300' height='250'>" +
       "<StaticResource creativeType='image/jpeg'><![CDATA[http://pagead2.googlesyndication.com/simgad/14302991065291703000]]></StaticResource>" +
       "<TrackingEvents>" +
        "<Tracking event='creativeView'><![CDATA[http://pubads.g.doubleclick.net/pcs/view?xai=AKAOjss35eCElo2ktS4pPNk97V5BZoJsokQOjnkKofA8au9jqRkUz7U8k31uDGTffpu4p8cBrf--E7TTzG0uJKgHw6dlqt-Vr_L9zY2y10hsa1XW0DoQlsQFHoHZhVMiZiT-eYUfBwjlHn-oEXBRStZGsrp9GUsMsEbc7V1s0Ps45saQWAyX98UVrLVaN3CZy4hps7XZnnsk3-5AGIgUZt72lsLdg5CwS3Nv98Dr84gUJDw_vJWnqCBy1XaLXDbQ&sig=Cg0ArKJSzALN-Ssn-h1GEAE&adurl=]]></Tracking>" +
       "</TrackingEvents>" +
       "<CompanionClickThrough><![CDATA[https://pubads.g.doubleclick.net/pcs/click?xai=AKAOjsu_O-Qz4mC0LXzIOeWzWoh6_gP7gaPZ_QdcoHrHWufnHpFFtgDLrM6FhEf_irO5abEelRh1-xxRaJGivRKbrqgwnECO8NIeGjgsVgvkgS4a88S2Y5KOCmmuZ4KghUWnPHuLkwOCWF25p-jWr5lN5U5Yx0F2jXqNGzQHIo1RxHr5MqXyUeQ750NrNpyeTkWxix7a4xbLk4I7Cga9cNgcWheh5ngllGVbkePxnKxMqOKy-TQkfIXssw&sig=Cg0ArKJSzKFKruHhZNtU&adurl=https://developers.google.com/interactive-media-ads/]]></CompanionClickThrough>" +
      "</Companion>" +
      "<Companion id='32948875364' width='728' height='90'>" +
       "<StaticResource creativeType='image/jpeg'><![CDATA[http://pagead2.googlesyndication.com/simgad/12955881070016219226]]></StaticResource>" +
       "<TrackingEvents>" +
        "<Tracking event='creativeView'><![CDATA[http://pubads.g.doubleclick.net/pcs/view?xai=AKAOjsuf_YFR13OmNDmmNs9oUh3xP-ROno9jZrLXdNxod3vEA2v4Peeg6v5q-bXTlLbCnII5hwbPO_oNYdVXjwR6aqJtHa6-C8d7PJihTkfahFsETlu7-pV4TUFUGaPkLueaRMFNhGcw5o7rJMdBrzEIsi4RkJFRhG1WdCovNXBLPTJORVVQF81YUjhTXbSzJZh9HhQQbhG7a7ZpQn8b-7PBaRYtJaZthzyZzj_CZFww0W2JZ6ESw-UCZKryp7x1&sig=Cg0ArKJSzD9TG2-bYFB8EAE&adurl=]]></Tracking>" +
       "</TrackingEvents>" +
       "<CompanionClickThrough><![CDATA[https://pubads.g.doubleclick.net/pcs/click?xai=AKAOjsvqlnzVUe8OY73ce0QVKrpeKtG7_FPhmv8DS9Xu_HiH5-5EHth5ceFPURcQvtlk97r4-8aZqgpvyt_nE8w-WajO6vhouR9lNYqNRBx0m8nt8Dui0XnbAT8KGH4t_5jq11wk48ppTprL8Epzt9hCzANESPUF7rNGVm6uBcEYdO3nAUsmcsqHzLUIX5koSDs5xQFcVxeZWfIU9M906A81ITdlisHeokKnllxWsLKp-Lj7j4IywLmr0w&sig=Cg0ArKJSzExHwq_mEFOi&adurl=https://developers.google.com/interactive-media-ads/]]></CompanionClickThrough>" +
      "</Companion>" +
     "</CompanionAds>" +
    "</Creative>" +
   "</Creatives>" +
   "<Extensions><Extension type='waterfall' fallback_index='0'/><Extension type='geo'><Country>VN</Country><Bandwidth>4</Bandwidth><BandwidthKbps>20000</BandwidthKbps></Extension><Extension type='DFP'><SkippableAdType>Generic</SkippableAdType><CustomTracking><Tracking event='engagedView'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=video_engaged_view]]></Tracking><Tracking event='skip'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=videoskipped]]></Tracking><Tracking event='skipShown'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=video_skip_shown]]></Tracking></CustomTracking></Extension><Extension type='metrics'><FeEventId>UVVQX6rHMMqo8gXwt57oAQ</FeEventId><AdEventId>CP_w_c74y-sCFYIOvAodqysBiQ</AdEventId></Extension><Extension type='ShowAdTracking'><CustomTracking><Tracking event='show_ad'><![CDATA[http://pubads.g.doubleclick.net/pcs/view?xai=AKAOjsunpGcU6ZA-A7No7pp9aViFtthgz-JMByO97nCW5fl8sintNPC7gPid3qjbh2i_jPCKrnm_4wU_KZCPElVPlsAG5j2EyugZ3z_33ndwOs_mLceBX3ExmLXsigohNfpS8P9wgJX14GRLPaAmy2QybR_dafWB0oiB8fW-esmsbe0Qk0iNI6ajJDVraQxgYrgwYPtJj80TisOFGJZ4gRIO6vp53OMmwQgNR3WUVGlk71shgOMRsQendOQclYolcqw&sig=Cg0ArKJSzAuluW71ioU-EAE&adurl=]]></Tracking></CustomTracking></Extension><Extension type='uiSettings'><UiHideable>1</UiHideable></Extension><Extension type='video_ad_loaded'><CustomTracking><Tracking event='loaded'><![CDATA[http://pubads.g.doubleclick.net/pagead/conversion/?ai=Bk5MBUVVQX7-EMYKd8AWr14TICPSqrIwHAAAAEAEglIfHFjgAWPTen996YMEFugEKMzIweDUwX3htbMgBBcACAuACAOoCIC82MDYyL2lhYl92YXN0X3NhbXBsZXMvc2tpcHBhYmxl-AKE0h6AAwGQA_QImAPgA6gDAeAEAdIFBRDUk8oLkAYBoAYjqAfs1RuoB_PRG6gHltgbqAfC2hvYBwHgBwPSCAcIgGEQARgd2AgCmAsB&sigh=1g5_61Si0JE&label=video_ad_loaded]]></Tracking></CustomTracking></Extension></Extensions>" +
  "</InLine>" +
 "</Ad>" +
"</VAST>";
            return result;
        }

        public string convert_time(int time)
        {
            TimeSpan time_sp = TimeSpan.FromSeconds(time);
            string str_time = string.Format("{0:D2}:{1:D2}:{2:D2}", time_sp.Hours, time_sp.Minutes, time_sp.Seconds);
            Console.WriteLine(str_time);
            return str_time;
        }
        public class ads_extend
        {
            public string code { get; set; }
            public string message { get; set; }
            public int length { get; set; }
            public List<vw_ads> list_vw_ads { get; set; }
        }
    }
}