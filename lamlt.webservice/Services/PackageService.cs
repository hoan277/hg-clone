﻿using Golaco.bank;
using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class packageService : LamltService
    {

        public packageService()
        {

        }
        public string create_transaction(int packageid, int memberid, string addressfrom)
        {
            packageService pack = new packageService();
            package itemPackage = pack.GetItemById(packageid);
            //var user_id = Comm.GetUserId();

            film_transactions trans = new film_transactions();
            trans.txtid = System.Guid.NewGuid().ToString();
            trans.datecreated = DateTime.Now;
            trans.amount1 = itemPackage.price;
            trans.currency1 = "VND";
            trans.idRef = packageid.ToString();
            trans.keyRef = "0";
            trans.statusBanking = "0";
            trans.txtHashFee = "bank";
            trans.typeName = "2";
            trans.statusWS1 = 1;
            trans.memberid = memberid;
            trans.addressFrom = addressfrom;
            int result = new Film_TransactionsService().UpdateOrInsert(trans);
            if (result > 0)
            {
                //call sang ngân hàng
                VTCHolder vtcHolder = new VTCHolder();
                vtcHolder.amount = trans.amount1.ToString();
                vtcHolder.currency = trans.currency1.ToString();

                vtcHolder.receiver_account = InstanceVTC.receiver_account;  // Tài khoản hứng tiền của đối tác tại VTC
                vtcHolder.reference_number = trans.txtid;           // Mã đơn hàng của đối tác, VTC và đối tác dùng đơn hàng này làm cơ sở đối soát
                vtcHolder.transaction_type = "sale";
                vtcHolder.website_id = InstanceVTC.websiteid;//txtWebsiteID.Text.Trim();

                //cập nhật vào db để đối soát
                string urlTemp = vtcHolder.urlRedirect();
                trans.urlTemp = urlTemp;
                trans.statusBanking = "2";
                trans.keyRef = "2";
                updateTransactionObj(trans);
                return trans.urlTemp;
            }
            else return "";
        }
        public void updateTransactionObj(film_transactions trans)
        {
            new Film_TransactionsService().UpdateOrInsert(trans);
        }
        public List<package> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                query = query.Skip(offset).Take(limit);
                List<package> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_package> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_package>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/
                query = query.Skip(offset).Take(limit);

                List<vw_package> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public Dictionary<object, object> GetPackageByProvider(int provider_id, string package_code, string provider_code)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "", code = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                if (provider_id > 0) query.Where(e => e.provider_id == provider_id);
                if (!string.IsNullOrEmpty(package_code)) query.Where(e => e.code == package_code);
                if (!string.IsNullOrEmpty(provider_code))
                {
                    var pro_id = db.Select(db.From<film_provider>().Where(e => e.code == provider_code)).SingleOrDefault().Id;
                    query.Where(e => e.provider_id == pro_id);
                }
                List<package> lists = db.Select(query);
                if (lists.Count > 0)
                {
                    code = "success";
                    message = "Lấy dữ liệu thành công";
                    dict.Add("length", lists.Count);
                    dict.Add("package", lists);
                }
                else
                {
                    code = "error";
                    message = "Không tìm thấy gói cước nào phù hợp với dữ liệu nhập vào";
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public package GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_package GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_package>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(package obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<package>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.description = obj.description;
                        objUpdate.price = obj.price;
                        objUpdate.note = obj.note;
                        objUpdate.typepayment = obj.typepayment;
                        objUpdate.code = obj.code;
                        objUpdate.provider_id = obj.provider_id;
                        objUpdate.date_created = DateTime.Now;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<package>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.description = obj.description;
                    objUpdate.price = obj.price;
                    objUpdate.note = obj.note;
                    objUpdate.code = obj.code;
                    objUpdate.provider_id = obj.provider_id;
                    objUpdate.typepayment = obj.typepayment;

                    objUpdate.date_created = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public package InitEmpty()
        {
            var obj = new package();
            obj.Id = 0;
            return obj;
        }

        private package GetItemById(int packageid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>().Where(e => e.Id == packageid);
                package rows = db.Select(query).FirstOrDefault();
                return rows;
            }
        }
        public List<vw_package> GetPackages()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_package>();
                return db.Select(query); ;
            }
        }
    }

    internal class Film_TransactionsService : LamltService
    {
        public int UpdateOrInsert(film_transactions obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_transactions>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.txtid = obj.txtid;
                        objUpdate.address = obj.address;
                        objUpdate.amount1 = obj.amount1;
                        objUpdate.amount2 = obj.amount2;
                        objUpdate.currency1 = obj.currency1;
                        objUpdate.currency2 = obj.currency2;
                        objUpdate.status_url = obj.status_url;
                        objUpdate.qrcore_url = obj.qrcore_url;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.idRef = obj.idRef;
                        objUpdate.keyRef = obj.keyRef;
                        objUpdate.typeTrans = obj.typeTrans;
                        objUpdate.rateUSD1 = obj.rateUSD1;
                        objUpdate.rateUSD2 = obj.rateUSD2;
                        objUpdate.addressWS = obj.addressWS;
                        objUpdate.memberid = obj.memberid;
                        objUpdate.txthash = obj.txthash;
                        objUpdate.statusWS1 = obj.statusWS1;
                        objUpdate.txtHashFee = obj.txtHashFee;
                        objUpdate.statusFee = obj.statusFee;
                        objUpdate.memberidTo = obj.memberidTo;
                        objUpdate.addressTo = obj.addressTo;
                        objUpdate.addressFrom = obj.addressFrom;
                        objUpdate.typeName = obj.typeName;
                        objUpdate.txtHash1 = obj.txtHash1;
                        objUpdate.username = obj.username;
                        objUpdate.note = obj.note;
                        objUpdate.bankid = obj.bankid;
                        objUpdate.statusBanking = obj.statusBanking;
                        objUpdate.fullname = obj.fullname;
                        objUpdate.phone = obj.phone;
                        objUpdate.email = obj.email;
                        objUpdate.urlTemp = obj.urlTemp;
                        objUpdate.statusBankingResult = obj.statusBankingResult;
                        objUpdate.orderIdRef = obj.orderIdRef;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_transactions>()
                        .Where(e => e.txtid == obj.txtid).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return (-2);
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.txtid = obj.txtid;
                    objUpdate.address = obj.address;
                    objUpdate.amount1 = obj.amount1;
                    objUpdate.amount2 = obj.amount2;
                    objUpdate.currency1 = obj.currency1;
                    objUpdate.currency2 = obj.currency2;
                    objUpdate.status_url = obj.status_url;
                    objUpdate.qrcore_url = obj.qrcore_url;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.idRef = obj.idRef;
                    objUpdate.keyRef = obj.keyRef;
                    objUpdate.typeTrans = obj.typeTrans;
                    objUpdate.rateUSD1 = obj.rateUSD1;
                    objUpdate.rateUSD2 = obj.rateUSD2;
                    objUpdate.addressWS = obj.addressWS;
                    objUpdate.memberid = obj.memberid;
                    objUpdate.txthash = obj.txthash;
                    objUpdate.statusWS1 = obj.statusWS1;
                    objUpdate.txtHashFee = obj.txtHashFee;
                    objUpdate.statusFee = obj.statusFee;
                    objUpdate.memberidTo = obj.memberidTo;
                    objUpdate.addressTo = obj.addressTo;
                    objUpdate.addressFrom = obj.addressFrom;
                    objUpdate.typeName = obj.typeName;
                    objUpdate.txtHash1 = obj.txtHash1;
                    objUpdate.username = obj.username;
                    objUpdate.note = obj.note;
                    objUpdate.bankid = obj.bankid;
                    objUpdate.statusBanking = obj.statusBanking;
                    objUpdate.fullname = obj.fullname;
                    objUpdate.phone = obj.phone;
                    objUpdate.email = obj.email;
                    objUpdate.urlTemp = obj.urlTemp;
                    objUpdate.statusBankingResult = obj.statusBankingResult;
                    objUpdate.orderIdRef = obj.orderIdRef;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactions>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_transactions GetByTxtId(string txtid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactions>().Where(e => e.txtid == txtid);
                return db.Select(query).FirstOrDefault();
            }
        }
        public film_transactions InitEmpty()
        {
            var obj = new film_transactions();
            obj.Id = 0;
            return obj;
        }
    }
}