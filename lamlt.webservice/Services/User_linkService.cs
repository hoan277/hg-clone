﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class user_linkService : LamltService
    {

        public user_linkService()
        {

        }
        public List<user_link> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/

                query = query.Skip(offset).Take(limit);
                List<user_link> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_user_link> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/

                query = query.Skip(offset).Take(limit);
                List<vw_user_link> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/

                return db.Count(query);
            }
        }

        internal int GetByUserId(int id)
        {
            throw new NotImplementedException();
        }

        public user_link GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_user_link GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_user_link>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public List<vw_user_link> GetAllUL(int userid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                return db.Select(db.From<vw_user_link>().Where(e => e.userid == userid)).ToList() ?? null;
            }
        }
        public int UpdateByToken(string token, user_link obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query1 = db.From<film_user>().Where(e => e.token == token);
                var objUpdate1 = db.Select(query1).FirstOrDefault();
                if (objUpdate1 == null) return -1;
                if (!objUpdate1.token_expired.HasValue) return -1;
                if (objUpdate1.token_expired.Value.CompareTo(DateTime.Now) <= 0) return -1;

                if (obj.Id > 0)
                {
                    var query = db.From<user_link>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        //objUpdate.userid = obj.userid;
                        objUpdate.name = obj.name;
                        objUpdate.typekid = obj.typekid;
                        objUpdate.datecreated = obj.datecreated;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    return -1;
                }
            }
        }
        public int UpdateOrInsert(user_link obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<user_link>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.userid = obj.userid;
                        objUpdate.name = obj.name;
                        objUpdate.typekid = obj.typekid;
                        objUpdate.datecreated = obj.datecreated;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<user_link>()
                        .Where(e => e.name == obj.name).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.userid = obj.userid;
                    objUpdate.name = obj.name;
                    objUpdate.typekid = obj.typekid;
                    objUpdate.datecreated = obj.datecreated;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public user_link InitEmpty()
        {
            var obj = new user_link();
            obj.Id = 0;
            return obj;
        }
        public int register_userlink(string token, string name, int typekid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.token == token);
                film_user objUser = db.Select(query).FirstOrDefault();
                if (objUser != null)
                {
                    return (int)db.Insert(new user_link()
                    {
                        userid = objUser.Id,
                        name = name,
                        typekid = typekid,
                        datecreated = DateTime.Now
                    }, selectIdentity: true);
                }
                return -1;
            }
        }
        public int delete_userlink(string token, int linkid)
        {
            int rs = -1;
            using (var db = _connectionData.OpenDbConnection())
            {
                film_user objUser = db.Select(db.From<film_user>().Where(e => e.token == token)).FirstOrDefault();
                if (objUser != null)
                {
                    user_link ul = db.Select(db.From<user_link>().Where(e => e.userid == objUser.Id).Where(e => e.Id == linkid)).FirstOrDefault();
                    if (ul != null)
                    {
                        rs = db.Delete(ul);
                    }
                }
            }
            return rs;
        }
        public int use_userlink(string token, int linkid)
        {
            int rs = -1;
            using (var db = _connectionData.OpenDbConnection())
            {
                film_user objUser = db.Select(db.From<film_user>().Where(e => e.token == token)).FirstOrDefault();
                if (objUser != null)
                {
                    objUser.link_id = linkid;
                    rs = db.Update(objUser);
                }
            }
            return rs;
        }
        public user_link GetById(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>().Where(e => e.Id == id);
                return db.Select(query).SingleOrDefault();
            }
        }
        public user_link GetByuserid(int userid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>().Where(e => e.userid == userid);
                return db.Select(query).FirstOrDefault();
            }
        }
    }
}