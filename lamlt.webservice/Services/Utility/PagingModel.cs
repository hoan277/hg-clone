﻿namespace Lala.App
{
    public class PagingModel
    {
        public static int LIMIT = 12;
        public int offset;
        public int limit;
        public string search;
        public int current;
    }
}
