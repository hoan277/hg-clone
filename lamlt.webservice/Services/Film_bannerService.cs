﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_bannerService : LamltService
    {

        public film_bannerService()
        {

        }
        public List<film_banner> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_banner>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<film_banner> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_banner>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public film_banner GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_banner>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_banner GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_banner obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_banner>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.position = obj.position;
                        objUpdate.prior = obj.prior;
                        if (obj.file != null && !obj.file.StartsWith("http"))
                        {
                            objUpdate.file = "http://115.146.121.190:5001/" + obj.file;
                        }
                        objUpdate.html = obj.html;
                        objUpdate.desc = obj.desc;
                        objUpdate.show_home = obj.show_home;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_banner>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.position = obj.position;
                    objUpdate.prior = obj.prior;
                    if (obj.file != null && !obj.file.StartsWith("http"))
                    {
                        objUpdate.file = "http://115.146.121.190:5001/" + obj.file;
                    }
                    objUpdate.html = obj.html;
                    objUpdate.desc = obj.desc;
                    objUpdate.show_home = obj.show_home;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_banner>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_banner InitEmpty()
        {
            var obj = new film_banner();
            obj.Id = 0;
            obj.file = "";
            return obj;
        }

        public List<vw_film_banner> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query=query.Where(e => (e.title.Contains(page.search)));


                query = query.Skip(offset).Take(limit);
                List<vw_film_banner> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_banner> GetAllBanner(string categoryid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {

                if (string.IsNullOrEmpty(categoryid) || categoryid == "-1")
                {
                    return db.Select(db.From<vw_film_banner>().Where(e => e.type_client == 2)).ToList();
                }
                else
                {
                    film_catalog catalog = db.Select(db.From<film_catalog>().Where(e => e.code.ToString() == categoryid)).FirstOrDefault();
                    if (catalog == null) return new List<vw_film_banner>();

                    var query = db.From<vw_film_banner>().Where(e => e.film_catalog_id == catalog.Id && e.type_client == 2);
                    var rows = db.Select(query).ToList();
                    return rows;
                }
            }
        }

        public List<vw_film_banner> GetAllViewByCatalog(PagingModel page, int catalog_id)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10000 };
            if (page.search == null) page.search = "";
            if (page.limit == 0) { page.limit = 10000; };
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.film_catalog_id == catalog_id);
                query = query.Skip(offset).Take(limit);
                List<vw_film_banner> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_banner> GetAllByTypeKidBanner(int catalogid, int isview)
        {
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                query.OrderByDescending(x => x.Id);
                query = query.Where(e => e.type_client == 2);
                query = query.Where(e => e.typekid <= isview);
                if (catalogid != -1)
                {
                    //query = query.Where(e => (e.show_home != 1));
                    query = query.Where(e => (e.film_catalog_code == catalogid));
                    //if (isview != 0)
                    //{
                    //    query = query.Where(e => e.typekid <= isview);
                    //}
                }
                else
                {
                    query = query.Where(e => e.show_home > 0);
                    //query = query.Where(e => e.typekid <= isview);
                }
                List<vw_film_banner> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_banner> GetByTypekid_Display(int catalogid, int isview)
        {
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>().OrderByDescending(x => x.Id);
                query = query.Where(e => e.type_client == 2);
                query = query.Where(e => e.typekid <= isview && e.typekid >= 0);
                query = query.Where(e => e.display == catalogid);
                List<vw_film_banner> rows = db.Select(query).ToList();
                return rows;
            }
        }
    }
}