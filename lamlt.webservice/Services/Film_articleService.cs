﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_articleService : LamltService
    {

        public film_articleService()
        {

        }
        public List<film_article> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                query = query.Skip(offset).Take(limit);
                List<film_article> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_article> GetAllItemView(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_article>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Skip(offset).Take(limit);
                List<vw_film_article> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public List<vw_film_article> GetAllNews(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_article>();
                if (id.Contains("-"))
                {
                    query.Where(e => e.url.Contains(id) || e.title.Contains(id));
                }
                else
                {
                    if (int.Parse(id) > 0)
                    {
                        query.Where(e => e.Id == int.Parse(id));
                    }
                }
                return db.Select(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public film_article GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_article GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_article>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_article obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_article>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.url = obj.url;
                        objUpdate.ordinal = obj.ordinal;
                        objUpdate.target = obj.target;
                        objUpdate.status = obj.status;
                        objUpdate.created_time = DateTime.Now;
                        objUpdate.title = obj.title;
                        objUpdate.summary = obj.summary;
                        objUpdate.content = obj.content;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_article>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    // objUpdate.Id = obj.Id;
                    objUpdate.url = obj.url;
                    objUpdate.ordinal = obj.ordinal;
                    objUpdate.target = obj.target;
                    objUpdate.status = obj.status;
                    objUpdate.created_time = DateTime.Now;
                    objUpdate.title = obj.title;
                    objUpdate.summary = obj.summary;
                    objUpdate.content = obj.content;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_article InitEmpty()
        {
            var obj = new film_article();
            obj.Id = 0;
            return obj;
        }
        /*
		public List<vw_film_article> GetViewAllItem(PagingModel page) 
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

          //  ServiceStackHelper.Help();
          //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
			var query = db.From<vw_film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
				query=query.Where(e => (e.title.Contains(page.search)));
				
				
				
                List<vw_film_article> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;                
            }
        }
		genvie here
		create view vw_film_article
		as
			select film_article.Id,film_article.url,film_article.ordinal,film_article.target,film_article.status,film_article.created_time,film_article.title,film_article.summary,film_article.content  from film_article  
		*/
    }
}