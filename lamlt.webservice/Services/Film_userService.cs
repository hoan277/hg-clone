﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.webservice.Services;
using Microsoft.AspNetCore.Http;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.Legacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace lamlt.web.Services
{
    public class film_userService : LamltService
    {
        public film_userService()
        {
        }

        public List<film_user> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.fullname.Contains(page.search)));
                /**/
                query = query.Skip(offset).Take(limit);
                List<film_user> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_user> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_user>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.fullname.Contains(page.search)));
                /**/
                query = query.Skip(offset).Take(limit);
                List<vw_film_user> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public Dictionary<object, object> CheckExpired(film_user oUser)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            DateTime ngaydk = DateTime.Now, ngayhh = DateTime.Now;

            var packageName = "";
            if (oUser != null)
            {
                using (var db = _connectionData.OpenDbConnection())
                {
                    var objFilmTrans = db.Select(db.From<film_transactions>().Where(e => e.username == oUser.username).OrderByDescending(x => x.Id).Limit(1)).FirstOrDefault();
                    if (objFilmTrans != null)
                    {
                        ngaydk = objFilmTrans.datecreated.Value;
                        if (HelperUtil.isVina(oUser.username))
                        {
                            switch (oUser.sub_type)
                            {
                                case 1:
                                    packageName = "LALATV_NGAY";
                                    ngayhh = ngaydk.Add(new TimeSpan(24, 0, 0));
                                    break;

                                case 2:
                                    packageName = "LALATV_TUAN";
                                    ngayhh = ngaydk.Add(new TimeSpan(7, 0, 0, 0));
                                    break;

                                case 5:
                                    packageName = "LALATV_GIADINH";
                                    ngayhh = ngaydk.Add(new TimeSpan(24, 0, 0, 0));
                                    break;

                                case 10:
                                    packageName = "BANK_THANG";
                                    ngayhh = ngaydk.Add(new TimeSpan(30, 0, 0, 0));
                                    break;
                            }
                        }
                        else
                        {
                            switch (oUser.sub_type)
                            {
                                // Case 1 2 5 này là của Vina check tạm lúc chưa cập nhật lại db, cập nhật xong db sẽ xóa các case này trong code đi
                                case 1:
                                case 7:
                                    packageName = "LALATV_NGAY";
                                    ngayhh = ngaydk.Add(new TimeSpan(24, 0, 0));
                                    break;

                                case 2:
                                case 8:
                                    packageName = "LALATV_TUAN";
                                    ngayhh = ngaydk.Add(new TimeSpan(7, 0, 0, 0));
                                    break;

                                case 5:
                                case 9:
                                    packageName = "LALATV_THANG";
                                    ngayhh = ngaydk.Add(new TimeSpan(30, 0, 0, 0));
                                    break;

                                case 10:
                                    packageName = "BANK_THANG";
                                    ngayhh = ngaydk.Add(new TimeSpan(30, 0, 0, 0));
                                    break;
                            }
                        }

                        var pack = new packageService().GetPackageByProvider(0, oUser.sub_type.ToString(), null);
                        List<package> objPack = (List<package>)pack["package"];
                        var objPackage = new package();
                        for (int i = 0; i < 1; i++)
                        {
                            dict.Add("Id", objPack[i].Id);
                            dict.Add("title", objPack[i].title);
                            dict.Add("description", objPack[i].description);
                            dict.Add("note", objPack[i].note);
                            dict.Add("typepayment", objPack[i].typepayment);
                            dict.Add("provider_id", objPack[i].provider_id);
                            dict.Add("code", objPack[i].code);
                        }
                        dict.Add("date_created", ngaydk);
                        dict.Add("date_expired", ngayhh);
                        return dict;
                    }
                    else
                    {
                        var pack = new packageService().GetPackageByProvider(0, oUser.sub_type.ToString(), null);
                        if (pack != null)
                        {
                            List<package> objPack = (List<package>)pack["package"];
                            var objPackage = new package();
                            for (int i = 0; i < 1; i++)
                            {
                                dict.Add("id", objPack[i].Id);
                                dict.Add("title", objPack[i].title);
                                dict.Add("description", objPack[i].description);
                                dict.Add("note", objPack[i].note);
                                dict.Add("typepayment", objPack[i].typepayment);
                                dict.Add("provider_id", objPack[i].provider_id);
                                dict.Add("code", objPack[i].code);
                                dict.Add("price", objPack[i].price);
                            }
                            film_drm film_drm = new film_userService().getDrmItemByUserId(oUser.Id);
                            if (film_drm != null)
                            {
                                dict.Add("date_created", film_drm.datecreate);
                                dict.Add("date_expired", film_drm.datesubcribe);
                                return dict;
                            }
                        }

                        dict = new Dictionary<object, object>();
                        dict.Add("code", "error");
                        dict.Add("message", "Tài khoản này chưa gia hạn hoặc chưa đăng ký gói cước nào");
                        return dict;
                    }
                }
            }
            else
            {
                dict.Add("code", "error");
                dict.Add("message", "Đăng nhập thất bại");
                return dict;
            }
        }

        public film_user CheckLogin(string user, string pass)
        {
            var token = Guid.NewGuid().ToString();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                query = query.Where(e => (e.username == user) || e.email == user);
                film_user oUser = db.Select(query).FirstOrDefault();
                if (user == oUser.email)
                {
                    user = oUser.username;
                }
                if (oUser != null)
                {
                    // Lưu mã token mới vào db
                    //objUpdate = db.Select(query).SingleOrDefault();
                    if (oUser != null)
                    {
                        oUser.Id = oUser.Id;
                        oUser.username = oUser.username;
                        oUser.password = oUser.password;
                        oUser.email = oUser.email;
                        oUser.phone = oUser.phone;
                        oUser.fullname = oUser.fullname;
                        oUser.desc = oUser.desc;
                        oUser.datecreated = oUser.datecreated;
                        oUser.userid = comm.GetUserId();
                        oUser.cpid = oUser.cpid;
                        oUser.roleid = oUser.roleid;
                        oUser.token = token;
                        oUser.token_expired = DateTime.Now.Add(new TimeSpan(30, 0, 0, 0));
                        db.Update(oUser);
                    }

                    //if (oUser.password == MD5.md5(pass).ToLower() )
                    if (user == oUser.username && MD5.md5(pass).ToLower() == oUser.password)
                    {
                        var queryRole = db.From<film_role>();
                        queryRole = queryRole.Where(e => (e.Id == oUser.roleid));

                        film_role oRole = db.Select(queryRole).FirstOrDefault();

                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_USERID, oUser.Id);
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_CPID, oUser.cpid ?? 0);
                        System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_USERNAME, oUser.username.ToString());

                        if (oRole != null) System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_ROLE, oRole.code);
                        return oUser;
                    }
                    if (false) //(user == oUser.username && pass == oUser.password)
                    {
                        film_role oRole = new film_role() { Id = 1, title = "BT", code = "BT" };

                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_USERID, oUser.Id);
                        System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_USERNAME, oUser.username.ToString());
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_CPID, oUser.cpid ?? 0);
                        if (oRole != null) System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_ROLE, oRole.code);
                        return oUser;
                    }
                }
                return null;
            }
        }

        internal Dictionary<string, object> renew_token(string token)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string message = "", code = "";
            film_user objUpdate = null;
            using (var db = _connectionData.OpenDbConnection())
            {
                var old_obj = db.Select(db.From<film_user>().Where(e => e.token == token)).FirstOrDefault();
                if (old_obj != null)
                {
                    if (old_obj.token_expired >= DateTime.Now)
                    {
                        objUpdate = new film_user
                        {
                            email = old_obj.email,
                            phone = old_obj.phone,
                            fullname = old_obj.fullname,
                            gender = old_obj.gender,
                            birthday = old_obj.birthday,
                            Id = old_obj.Id,
                            username = old_obj.username,
                            userid = old_obj.userid,
                            desc = old_obj.desc,
                            datecreated = old_obj.datecreated,
                            cpid = old_obj.cpid,
                            roleid = old_obj.roleid,
                            sub_state = old_obj.sub_state,
                            sub_type = old_obj.sub_type,
                            token = Guid.NewGuid().ToString(),
                            token_expired = DateTime.Now.Add(new TimeSpan(30, 0, 0, 0))
                        };
                        db.Update(objUpdate);
                        code = "success";
                        message = "Đã cập nhật lại token mới cho tài khoản '" + objUpdate.username + "'";
                    }
                    else
                    {
                        code = "error";
                        message = "Token đã hết hạn không thể re-new";
                    }
                }
                else
                {
                    code = "error";
                    message = "Token không đúng";
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            if (objUpdate != null)
            {
                dict.Add("user", objUpdate);
            }
            return dict;
        }

        public Dictionary<string, object> RecoveryPassword(string token, string email, string phone)
        {
            string send_via = "email";
            if (!string.IsNullOrEmpty(email))
            {
                if (!email.Contains("@"))
                {
                    phone = email;
                    email = null;
                    send_via = "phone";
                }
            }
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string message = "", code = "";
            string newpass = HelperUtil.RandomString(6, true);
            film_user rs_user = null;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                if (token != null) query.Where(e => e.token == token);
                if (email != null) query.Where(e => e.email == email);
                if (phone != null) query.Where(e => e.phone == phone);
                var rand = new Random().Next(0, 6);

                var old_obj = db.Select(query).FirstOrDefault();
                if (old_obj != null)
                {
                    var objUpdate = new film_user
                    {
                        email = old_obj.email,
                        phone = old_obj.phone,
                        fullname = old_obj.fullname,
                        gender = old_obj.gender,
                        birthday = old_obj.birthday,
                        Id = old_obj.Id,
                        username = old_obj.username,
                        password = MD5.md5(newpass),
                        token = old_obj.token,
                        userid = old_obj.userid,
                        desc = old_obj.desc,
                        datecreated = old_obj.datecreated,
                        cpid = old_obj.cpid,
                        roleid = old_obj.roleid,
                        sub_state = old_obj.sub_state,
                        sub_type = old_obj.sub_type,
                        token_expired = old_obj.token_expired
                    };
                    rs_user = objUpdate;
                    db.Update(objUpdate);
                    var body = "Chào <b>" + old_obj.username + "</b>, ";
                    body += "chúng tôi đã xác nhận có một sự thay đổi về mật khẩu của bạn vào lúc <b>" + DateTime.Now.ToString("HH:ss:mm") + " ngày " + DateTime.Now.ToString("dd/mm/yyyy") + "</b><br /><br />"; ;
                    body += "Mật khẩu mới <span style='color:red;font-size:16px;font-weight:600;border:1px solid;padding:10px;'>" + newpass + "</span>";
                    body += "<br /><br />Vui lòng đăng nhập và thay đổi lại mật khẩu qua đường dẫn sau: <a href='http://lalatv.com.vn/' tagget='_blank'>Đăng nhập</a><br /><br />";
                    body += "Xin cảm ơn bạn đã sử dụng dịch vụ LALATV";
                    //new Thread(new ThreadStart(delegate {
                    //    HelperUtil.SendEmail(email, "Mật khẩu khôi phục của LalaTV", body, null);
                    //})).Start();
                    //LogService.logItem("send_via " + send_via);
                    //LogService.logItem("isVina " + HelperUtil.isVina(old_obj.phone));
                    if (HelperUtil.isVina(old_obj.phone) && send_via != "email")
                    {
                        //LogService.logItem("Chuẩn bị gửi tin nhắn SMS");
                        // Gọi API sang http://lalatv.vn/sms_recovery_password?mobile=84949803099&content=content
                        var s = old_obj.phone;
                        var first_str = s.Substring(0, 1);
                        var second_str = s.Substring(1);
                        if (first_str.StartsWith("0")) first_str = first_str.Replace("0", "84");
                        var mobile = first_str.ToString() + second_str.ToString();
                        var content = "[LALATV] - Mat khau khoi phuc cua ban la: " + newpass;
                        WebClient client = new WebClient();
                        //LogService.logItem("Chuẩn bị gửi");
                        string reply = client.DownloadString("http://lalatv.vn/sms_recovery_password?mobile=" + mobile + "&content=" + content);

                        if (reply == "sent")
                        {
                            // LogService.logItem("Đã gửi tin nhắn");
                            code = "success";
                            message = "Mật khẩu đã được gửi vào điện thoại của bạn, vui lòng kiểm tra tin nhắn";
                        }
                        else
                        {
                            //LogService.logItem("Lỗi gửi tin nhắn VASCloudHelper");
                            code = "error";
                            message = "Lỗi VASCloudHelper::sendSMS";
                        }
                    }
                    else
                    {
                        //LogService.logItem("Chuẩn bị gửi email");
                        HelperUtil.SendEmail(old_obj.email, "Mật khẩu khôi phục của LalaTV", body, null);
                        code = "success";
                        message = "Mật khẩu đã được gửi vào email của bạn, vui lòng kiểm tra email";
                        //LogService.logItem("Đã gửi email");
                    }
                }
                else
                {
                    code = "error";
                    message = "Token, Email hoặc Số điện thoại không đúng";
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            if (rs_user != null)
            {
                dict.Add("user", rs_user);
            }
            return dict;
        }

        internal Dictionary<string, object> HuyDV(string token)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string message = "", code = "", url = "", sub = "";
            int price = 0;
            film_user rs_user = null;

            using (var db = _connectionData.OpenDbConnection())
            {
                var old_obj = db.Select(db.From<film_user>().Where(e => e.token == token)).FirstOrDefault();
                if (old_obj != null)
                {
                    switch (old_obj.sub_type)
                    {
                        case 1: price = 3000; sub = "LALATV_NGAY"; break;
                        case 2: price = 10000; sub = "LALATV_TUAN"; break;
                        case 5: price = 30000; sub = "LALATV_THANG"; break;
                    }
                    var objUpdate = new film_user
                    {
                        email = old_obj.email,
                        phone = old_obj.phone,
                        fullname = old_obj.fullname,
                        gender = old_obj.gender,
                        birthday = old_obj.birthday,
                        Id = old_obj.Id,
                        username = old_obj.username,
                        password = old_obj.password,
                        token = old_obj.token,
                        userid = old_obj.userid,
                        desc = old_obj.desc,
                        datecreated = old_obj.datecreated,
                        cpid = old_obj.cpid,
                        roleid = old_obj.roleid,
                        sub_state = 0,
                        sub_type = old_obj.sub_type,
                        token_expired = old_obj.token_expired
                    };
                    db.Update(objUpdate);
                    code = "success";
                    message = "Hủy dịch vụ thành công";
                    rs_user = objUpdate;
                    switch (old_obj.payment_type)
                    {
                        case 0: // SMS
                            if (HelperUtil.isViettel(old_obj.phone))
                            {
                                url = "http://viettel.lalatv.com.vn/wap/test.php?type=cancel&mobile=" + old_obj.phone + "&price=" + price + "&sub=" + sub + "&req=" + DateTime.Now.ToFileTime();
                            }
                            break;

                        case 1:// Banking
                            new ServiceMeService().update_create_user_log(old_obj.phone, old_obj.sub_type.ToString(), old_obj.sub_state.ToString(), "");
                            break;
                    }
                }
                else
                {
                    code = "error";
                    message = "Mã token không đúng";
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            dict.Add("url", url);
            dict.Add("user", rs_user);
            return dict;
        }

        internal Dictionary<string, object> ChangePass(string token, string oldpass, string newpass)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string message, code;
            film_user rs_user = null;
            using (var db = _connectionData.OpenDbConnection())
            {
                var old_obj = db.Select(db.From<film_user>().Where(e => e.token == token)).FirstOrDefault();
                if (old_obj != null)
                {
                    if (old_obj.password.ToLower() != MD5.md5(oldpass).ToLower())
                    {
                        code = "notmatch";
                        message = "Mật khẩu cũ không đúng";
                    }
                    else if (MD5.md5(oldpass).ToLower() == MD5.md5(newpass).ToLower())
                    {
                        code = "duplicate";
                        message = "Mật khẩu mới không được giống mật khẩu cũ";
                    }
                    else
                    {
                        var objUpdate = new film_user
                        {
                            email = old_obj.email,
                            phone = old_obj.phone,
                            fullname = old_obj.fullname,
                            gender = old_obj.gender,
                            birthday = old_obj.birthday,
                            Id = old_obj.Id,
                            username = old_obj.username,
                            password = MD5.md5(newpass),
                            token = old_obj.token,
                            userid = old_obj.userid,
                            desc = old_obj.desc,
                            datecreated = old_obj.datecreated,
                            cpid = old_obj.cpid,
                            roleid = old_obj.roleid,
                            sub_state = old_obj.sub_state,
                            sub_type = old_obj.sub_type,
                            token_expired = old_obj.token_expired
                        };
                        db.Update(objUpdate);
                        code = "success";
                        message = "Cập nhật mật khẩu thành công";
                        rs_user = objUpdate;
                    }
                }
                else
                {
                    code = "error";
                    message = "Mã token không đúng";
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            if (rs_user != null)
            {
                dict.Add("user", rs_user);
            }
            return dict;
        }

        internal int UpdateProfile(film_user obj)
        {
            if (obj.token == null) return -1;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                query = query.Where(e => e.token == obj.token);
                var old_obj = db.Select(query).FirstOrDefault();
                if (obj != null)
                {
                    var objUpdate = new film_user
                    {
                        // Dữ liệu mới
                        email = obj.email,
                        phone = obj.phone,
                        fullname = obj.fullname,
                        gender = obj.gender,
                        birthday = obj.birthday,
                        // Dữ liệu cũ
                        Id = old_obj.Id,
                        username = old_obj.username,
                        password = old_obj.password,
                        token = old_obj.token,
                        userid = old_obj.userid,
                        desc = old_obj.desc,
                        datecreated = old_obj.datecreated,
                        cpid = old_obj.cpid,
                        roleid = old_obj.roleid,
                        sub_state = old_obj.sub_state,
                        sub_type = old_obj.sub_type,
                        token_expired = old_obj.token_expired
                    };
                    return db.Update(objUpdate);
                }
            }
            return -1;
        }

        public int DeleteToken(string token)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                query = query.Where(e => e.token == token);
                film_user obj = db.Select(query).FirstOrDefault();
                if (obj != null)
                {
                    var objUpdate = new film_user();
                    objUpdate.Id = obj.Id;
                    objUpdate.username = obj.username;
                    objUpdate.password = obj.password;
                    objUpdate.email = obj.email;
                    objUpdate.phone = obj.phone;
                    objUpdate.fullname = obj.fullname;
                    objUpdate.desc = obj.desc;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.cpid = obj.cpid;
                    objUpdate.roleid = obj.roleid;
                    objUpdate.token = "";
                    return db.Update(objUpdate);
                }
                return -1;
            }
        }

        public film_user CheckLoginEmail(string email, string pass)
        {
            //LogService.logItem("-------------------- CheckLoginEmail(string email, string pass) -----------------");
            //LogService.logItem("email: " + email);
            //LogService.logItem("pass: " + pass);
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                query = query.Where(e => (e.email == email));
                film_user oUser = db.Select(query).FirstOrDefault();
                if (oUser != null)
                {
                    if (email == oUser.email && MD5.md5(pass.ToLower()) == oUser.password)
                    {
                        var queryRole = db.From<film_role>();
                        queryRole = queryRole.Where(e => (e.Id == oUser.roleid));
                        film_role oRole = db.Select(queryRole).FirstOrDefault();
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_USERID, oUser.Id);
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_CPID, oUser.cpid ?? 0);
                        System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_USERNAME, oUser.username.ToString());
                        if (oRole != null) System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_ROLE, oRole.code);
                        return oUser;
                    }
                }
                return null;
            }
        }

        public film_user CheckLoginSmartTV(string user, string pass)
        {
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                query = query.Where(e => (e.username == user) || e.email == user);
                film_user oUser = db.Select(query).FirstOrDefault();

                if (oUser != null)
                {
                    if (oUser.password == MD5.md5(pass).ToLower())
                    //if (user == oUser.username && pass == oUser.password)
                    {
                        oUser.token = System.Guid.NewGuid().ToString();
                        oUser.token_expired = DateTime.Now.AddDays(30);
                        db.Update(oUser);
                        var queryRole = db.From<film_role>();
                        queryRole = queryRole.Where(e => (e.Id == oUser.roleid));

                        film_role oRole = db.Select(queryRole).FirstOrDefault();

                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_USERID, oUser.Id);
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_CPID, oUser.cpid ?? 0);
                        System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_USERNAME, oUser.username.ToString());

                        if (oRole != null) System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_ROLE, oRole.code);
                        return oUser;
                    }
                }
                return null;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.fullname.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_user>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.fullname.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public film_user GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public film_user GetByPhone(string phone)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.phone == phone);
                return db.Select(query).FirstOrDefault();
            }
        }

        public film_user GetByToken(string token)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.token == token);
                film_user film = db.Select(query).FirstOrDefault();
                if (film != null)
                {
                    DateTime dt = film.token_expired ?? DateTime.Now.AddDays(-1);
                    if (dt.CompareTo(DateTime.Now) >= 0) return film;
                }
            }
            return null;
        }

        public film_drm getDrmByUserId(int userid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_drm>().Where(e => e.userid == userid && e.datesubcribe >= DateTime.Now);
                //var query = db.From<film_drm>().Where(e => e.userid == userid).OrderByDescending(e=>e.datesubcribe);
                film_drm film = db.Select(query).FirstOrDefault();
                //if(film.datesubcribe.CompareTo(DateTime.Now)>=0) return film;
                return film;
            }
        }

        public film_drm getDrmItemByUserId(int userid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //var query = db.From<film_drm>().Where(e => e.userid == userid && e.datesubcribe >= DateTime.Now);
                var query = db.From<film_drm>().Where(e => e.userid == userid).OrderByDescending(e => e.datesubcribe);
                film_drm film = db.Select(query).FirstOrDefault();
                //if(film.datesubcribe.CompareTo(DateTime.Now)>=0) return film;
                return film;
            }
        }

        public vw_film_user GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_user>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public Dictionary<object, object> normal_register(string fullname, string phone, string pass)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            film_userService userService = new film_userService();
            using (var db = _connectionData.OpenDbConnection())
            {
                film_user objUser = db.Select(db.From<film_user>().Where(e => e.username == phone)).FirstOrDefault();
                if (objUser == null)
                {
                    film_user oInit = userService.InitEmpty();
                    oInit.username = phone;
                    oInit.phone = phone;
                    oInit.email = "";
                    oInit.password = MD5.md5(pass).ToLower();
                    oInit.fullname = fullname;
                    oInit.token = System.Guid.NewGuid().ToString();
                    oInit.token_expired = DateTime.Now.AddHours(72);
                    oInit.datecreated = DateTime.Now;
                    int ix = userService.UpdateOrInsert(oInit);
                    if (ix > 0)
                    {
                        dict.Add("code", "success");
                        dict.Add("message", "Đăng ký thành công");
                    }
                    else
                    {
                        dict.Add("code", "error");
                        dict.Add("message", "Đăng ký thất bại, vui lòng liên hệ admin");
                    }
                }
                else
                {
                    dict.Add("code", "error");
                    dict.Add("message", "Tài khoản đã tồn tại, vui lòng chọn tài khoản khác");
                }
                return dict;
            }
        }

        public int UpdateOrInsert(film_user obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_user>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.username = obj.username;
                        objUpdate.password = obj.password;
                        objUpdate.email = obj.email;
                        objUpdate.phone = obj.phone;
                        objUpdate.fullname = obj.fullname;
                        objUpdate.desc = obj.desc;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.cpid = obj.cpid;
                        objUpdate.roleid = obj.roleid;

                        objUpdate.token = obj.token;
                        objUpdate.token_expired = obj.token_expired;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.username = obj.username;
                    objUpdate.password = obj.password;
                    objUpdate.email = obj.email;
                    objUpdate.phone = obj.phone;
                    objUpdate.fullname = obj.fullname;
                    objUpdate.desc = obj.desc;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.cpid = obj.cpid;
                    objUpdate.roleid = obj.roleid;
                    objUpdate.token = obj.token;
                    objUpdate.token_expired = obj.token_expired;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_user InitEmpty()
        {
            var obj = new film_user();
            obj.Id = 0;
            return obj;
        }

        /*
		genvie here
		create view vw_film_user
		as
			select film_users.Id,film_users.username,film_users.password,film_users.email,film_users.phone,film_users.fullname,film_users.desc,film_users.datecreated,film_users.userid,film_users.cpid,film_users.roleid   ,film_cp.title as cpid_title  ,film_role.fullname as roleid_title  from film_users   inner join film_cp on film_users.cpid=film_cp.id    inner join film_role on film_users.roleid=film_role.id
		*/
    }
}