﻿using Lalatv.Models;
using lamlt.Controllers;
using lamlt.data;
using lamlt.web.Models;
using Microsoft.Extensions.Primitives;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace lamlt.web.Services
{
    public class ServiceMeService : LamltService
    {
        public ServiceMeService()
        {
        }

        public List<film_article> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/
                query = query.Skip(offset).Take(limit);
                List<film_article> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<VideoRecommend> getAllRecommend()
        {
            List<VideoRecommend> lst = new List<VideoRecommend>();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.status == 2).OrderByDescending(y => y.datecreated).Take(15);// .Where(e => e.username == email);
                var lstData = db.Select(query);
                foreach (film_video video in lstData)
                {
                    lst.Add(new VideoRecommend() { videoid = video.Id, rate = 0.99f });
                }
            }
            /*
            lst.Add(new VideoRecommend() { videoid = 2704, rate = 0.99f });
            lst.Add(new VideoRecommend() { videoid = 2706, rate = 0.98f });
            lst.Add(new VideoRecommend() { videoid = 2708, rate = 0.91f });
            lst.Add(new VideoRecommend() { videoid = 2710, rate = 0.92f });
            lst.Add(new VideoRecommend() { videoid = 2711, rate = 0.91f });
            lst.Add(new VideoRecommend() { videoid = 2712, rate = 0.90f });
            lst.Add(new VideoRecommend() { videoid = 2713, rate = 0.90f });
            lst.Add(new VideoRecommend() { videoid = 2714, rate = 0.50f });

            lst.Add(new VideoRecommend() { videoid = 2716, rate = 0.81f });
            lst.Add(new VideoRecommend() { videoid = 2717, rate = 0.80f });
            lst.Add(new VideoRecommend() { videoid = 2718, rate = 0.90f });
            lst.Add(new VideoRecommend() { videoid = 2719, rate = 0.90f });
            lst.Add(new VideoRecommend() { videoid = 2720, rate = 0.80f });
            lst.Add(new VideoRecommend() { videoid = 2721, rate = 0.10f });

            lst.Add(new VideoRecommend() { videoid = 2724, rate = 0.90f });
            lst.Add(new VideoRecommend() { videoid = 2725, rate = 0.10f });

            return lst.OrderBy(e=>Guid.NewGuid().ToString()).Take(5).ToList<VideoRecommend>();
            */
            return lst;
        }

        public object processing_register(string user, string email, string password)
        {
            film_userService userService = new film_userService();
            using (var db = _connectionData.OpenDbConnection())
            {
                film_user check_user = null;
                film_user check_email = null;
                if (string.IsNullOrEmpty(user)) return null;
                check_user = db.Select(db.From<film_user>().Where(e => e.username == user)).FirstOrDefault();
                if (!string.IsNullOrEmpty(email))
                {
                    check_email = db.Select(db.From<film_user>().Where(e => e.email == email)).FirstOrDefault();
                }
                if (check_user == null && check_email == null)
                {
                    film_user oInit = userService.InitEmpty();
                    oInit.username = user;
                    oInit.phone = user;
                    oInit.email = email;
                    oInit.password = MD5.md5(password).ToLower();
                    oInit.fullname = user;
                    oInit.token = System.Guid.NewGuid().ToString();
                    oInit.token_expired = DateTime.Now.AddHours(72);
                    oInit.fullname = user;
                    oInit.datecreated = DateTime.Now;
                    int ix = userService.UpdateOrInsert(oInit);
                    if (ix > 0)
                    {
                        oInit.Id = ix;
                        oInit.password = "";
                        return oInit;
                    }
                    return null;
                }
                return null;
            }
        }

        public object processing_login(string email, string password)
        {
            film_userService userService = new film_userService();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.username == email);

                film_user rows = db.Select(query).FirstOrDefault();
                if (rows != null)
                {
                    if (rows.password == password) return rows;
                    return null;
                }
                return null;
            }
        }

        public string convertToSlug(string title)
        {
            if (title != null)
            {
                Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                string temp = title.Normalize(NormalizationForm.FormD);
                return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace(":", "").Replace(",", "").Replace(",", "").Replace(";", "").Replace("|", "").Replace(".", "").Replace(" ", "-").Replace("--", "-").ToLower();
            }
            else
            {
                return title;
            }
        }

        public Video convert_to_video(vw_film_video video_db, string catid)
        {
            if (string.IsNullOrEmpty(catid)) catid = (video_db.catalog_id ?? 0).ToString();
            List<film_video_image> film_images = new film_videoService().GetAll_Images(video_db.Id);
            List<string> arr_images = film_images.Select(e => e.url).ToList<string>();
            Video video = new Video();
            if (video_db.film_type == 1)
            {
                string s_title = video_db.title.Replace("- Tập", "- tập").Replace("- tập", "-tập")
                    .Split(new string[] { "-tập" }, StringSplitOptions.None)[0];
                video_db.title = s_title;
            }
            video.id = video_db.Id;
            video.episode = video_db.episode;
            video.episode_current = video_db.episode_current;
            video.like = video_db.like;
            video.dislike = video_db.dislike;
            video.aliasId = video_db.code;
            video.title = video_db.title;
            video.description = video_db.desc;
            video.farmId = video_db.catalog_id ?? 0;
            video.thumbnails = new Thumbnails() { small = video_db.thumb_file, medium = video_db.thumb_file, high = video_db.thumb_file, standard = video_db.thumb_file, maxres = video_db.thumb_file };
            video.createTime = DateTime.Now.Ticks;
            video.duration = video_db.duration;
            video.state = new State() { video = "INITIAL", process = "COMPLETE", approve = "WAITING" };
            video.view = 1;
            video.imageUrl = video_db.thumb_file;
            video.images = new Images() { small = arr_images, medium = arr_images, high = arr_images, maxres = arr_images };
            video.embed = new Embed() { shortCode = "[mecloud]" + video.aliasId + "[/mecloud]", script = "http://lalatv.com.vn/" };
            video.streams = new List<object>();
            video.owner = new Owner() { displayname = "displayname", email = "email", ownerId = 0 };
            video.ads = true;
            video.catalog_id_title = video_db.catalog_id_title;
            video.catalog_id = video_db.catalog_id.Value;
            video.catalog_id_code = video_db.catalog_id_code;
            video.director = video_db.director;
            video.publish_year = video_db.publish_year;
            video.duration = video_db.duration ?? "0";
            video.actor = video_db.actor;
            video.country = video_db.publish_countryid_title;
            video.url_trailer = video_db.trainer_url;
            video.url_trailer_thumbnail = video_db.trainer_thumbnail;
            video.isview = video_db.isview;

            video.cateIds = new List<int>() { int.Parse(catid) };
            if ((int.Parse(catid)) == 7) video.cateIds = new List<int>() { int.Parse(catid), 10 };
            video.typeIds = new List<int>() { video_db.catalog_id ?? 0 };
            video.tagIds = new List<int>() { video_db.catalog_id ?? 0 };
            video.tags = new List<string>() { };

            video.linksDownload = new List<string>() { video_db.upload_file };
            return video;
        }

        public Video convert_to_video_view(vw_film_video video_db, string catid, int userid)
        {
            if (string.IsNullOrEmpty(catid)) catid = (video_db.catalog_id ?? 0).ToString();
            List<film_video_image> film_images = new film_videoService().GetAll_Images(video_db.Id);
            List<string> arr_images = film_images.Select(e => e.url).ToList<string>();
            Video video = new Video();
            if (video_db.film_type == 1)
            {
                string s_title = video_db.title.Replace("- Tập", "- tập").Replace("- tập", "-tập")
                    .Split(new string[] { "-tập" }, StringSplitOptions.None)[0];
                video_db.title = s_title;
            }
            video.id = video_db.Id;
            video.episode = video_db.episode;
            video.episode_current = video_db.episode_current;
            video.like = video_db.like;
            video.dislike = video_db.dislike;
            video.aliasId = video_db.code;
            video.title = video_db.title;
            video.description = video_db.desc;
            video.farmId = video_db.catalog_id ?? 0;
            video.thumbnails = new Thumbnails() { small = video_db.thumb_file, medium = video_db.thumb_file, high = video_db.thumb_file, standard = video_db.thumb_file, maxres = video_db.thumb_file };
            video.createTime = DateTime.Now.Ticks;
            video.state = new State() { video = "INITIAL", process = "COMPLETE", approve = "WAITING" };
            video.view = 1;
            video.imageUrl = video_db.thumb_file;
            video.images = new Images() { small = arr_images, medium = arr_images, high = arr_images, maxres = arr_images };
            video.embed = new Embed() { shortCode = "[mecloud]" + video.aliasId + "[/mecloud]", script = "http://lalatv.com.vn/" };
            video.streams = new List<object>();
            video.owner = new Owner() { displayname = "displayname", email = "email", ownerId = 0 };
            video.ads = true;
            video.catalog_id_title = video_db.catalog_id_title;
            video.catalog_id = video_db.catalog_id.Value;
            video.catalog_id_code = video_db.catalog_id_code;
            video.director = video_db.director;
            video.publish_year = video_db.publish_year;
            video.duration = video_db.duration ?? "0";
            video.actor = video_db.actor;
            video.country = video_db.publish_countryid_title;
            video.url_trailer = video_db.trainer_url;
            video.url_trailer_thumbnail = video_db.trainer_thumbnail;
            video.isview = video_db.isview;

            video.cateIds = new List<int>() { int.Parse(catid) };
            if ((int.Parse(catid)) == 7) video.cateIds = new List<int>() { int.Parse(catid), 10 };
            video.typeIds = new List<int>() { video_db.catalog_id ?? 0 };
            video.tagIds = new List<int>() { video_db.catalog_id ?? 0 };
            video.tags = new List<string>() { };

            video.linksDownload = new List<string>() { video_db.upload_file };

            film_video_view filmView = new film_video_viewService().GetByUserAndFilm(userid, video_db.Id);
            if (filmView != null)
            {
                video.time_end = filmView.time_end;
            }
            return video;
        }

        public Video convert_to_video_in_db(Video video, int catid1, VideoResultDetail detail)
        {
            film_catalog film_catalog = new film_catalogService().GetByCode(catid1.ToString());
            if (film_catalog == null) return null;
            int catid = film_catalog.Id;
            List<film_video_image> film_images = new List<film_video_image>();
            List<string> lstFile = video.images.small.Union(video.images.medium).Union(video.images.high).Union(video.images.maxres).ToList<string>();
            foreach (string sFile in lstFile)
            {
                film_images.Add(new film_video_image() { url = sFile });
            }

            List<film_catalog_film> film_catalogs = new List<film_catalog_film>();
            foreach (int id_cat in video.cateIds)
            {
                film_catalogs.Add(new film_catalog_film() { catalogid = id_cat });
            }

            List<string> arr_images = film_images.Select(e => e.url).ToList<string>();
            film_video video_db = new film_video();
            video_db.code = video.aliasId;
            video_db.catalog_id = catid;
            video_db.title = video.title;
            video_db.desc = video.description;
            video_db.catalog_id = catid;
            video_db.thumb_file = video.thumbnails.maxres;
            video_db.datecreated = DateTime.FromFileTimeUtc(video.createTime);
            video_db.duration = video.duration.ToString();
            string fileupload = detail.videos.First().linkDirects[detail.videos.First().linkDirects.Count - 2].hls;
            video_db.upload_file = fileupload;//video.linksDownload.First().ToString();
            video_db.publish_countryid = 4;
            video_db.status = 2;
            video_db.price = 0;
            video_db.cpid = 7;
            video.catalog_id_title = video.catalog_id_title;
            video.isview = video.isview;
            video.time_end = video.time_end;
            video_db.tags = String.Join(",", video.tags);
            using (var db = _connectionData.OpenDbConnection())
            {
                long idvideo = db.Insert<film_video>(video_db, selectIdentity: true);
                film_images.ForEach(c => c.filmid = (int)idvideo);
                db.InsertAll<film_video_image>(film_images);

                film_catalogs.ForEach(c => c.filmid = (int)idvideo);
                db.InsertAll<film_catalog_film>(film_catalogs);
            }
            /*
            video.state = new State() { video = "INITIAL", process = "COMPLETE", approve = "WAITING" };
            video.view = 1;
            video.images = new Images() { small = arr_images, medium = arr_images, high = arr_images, maxres = arr_images };
            video.embed = new Embed();
            video.streams = new List<object>();
            video.owner = new Owner() { displayname = video_db.userid.ToString() };
            video.ads = true;

            video.cateIds = new List<int>() { video_db.catalog_id ?? 0 };
            video.typeIds = new List<int>() { video_db.catalog_id ?? 0 };
            video.tagIds = new List<int>() { video_db.catalog_id ?? 0 };
            video.tags = new List<string>() { };

            video.linksDownload = new List<string>() { video_db.upload_file };
            */
            return video;
        }

        public VideoDetail convert_to_video_detail(vw_film_video video_db)
        {
            VideoDetail video = new VideoDetail();
            video.id = video_db.Id;
            video.aliasId = video_db.code.ToString();
            video.title = video_db.title;
            video.episode = video_db.episode;
            video.catalog_id = video_db.catalog_id.Value;
            video.catalog_id_title = video_db.catalog_id_title;
            video.slug = convertToSlug(video.title);
            video.url = convertToSlug(video.title) + "-p" + video.id;
            video.episode_current = video_db.episode_current;
            video.url_image = video_db.thumb_file;
            video.url_trailer = video_db.trainer_url;
            video.url_trailer_thumbnail = video_db.trainer_thumbnail;
            video.actor = video_db.actor;
            video.desc = video_db.desc;
            video.duration = video_db.duration;
            video.publish_year = video_db.publish_year;
            video.country = video_db.publish_countryid_title;
            video.director = video_db.director;
            video.like = video_db.like;

            if (video_db.upload_file == null) video_db.upload_file = "";
            video.linkDirects = new List<LinkDirect>()
            {
                new LinkDirect(){quality="360p",hls=video_db.upload_file.Replace("http://","//").Replace("_720p.","_360p."),size=1910101 },
                new LinkDirect(){quality="480p",hls=video_db.upload_file.Replace("http://","//").Replace("_720p.","_360p."),size=1910101 },
                new LinkDirect(){quality="720p",hls=video_db.upload_file.Replace("http://","//").Replace("_720p.","_360p."),size=1910101 },
                new LinkDirect(){quality="auto",hls=video_db.upload_file.Replace("http://","//").Replace("_720p.","_360p."),size=1910101 }
            };
            return video;
        }

        private void logItem(string s)
        {
            using (StreamWriter w = new StreamWriter("logDebugLong.txt", true))
            {
                w.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ":" + s);
            }
        }

        public object processing_data(StringValues access_token, StringValues query_type, StringValues user_type, StringValues start_index, StringValues max_results, StringValues categoryId, StringValues videos_id, string search = "", string packageid = "-1", string method = "-1", string typevideo = "", string device = "")
        {
            if (string.IsNullOrEmpty(start_index)) start_index = "0";
            if (string.IsNullOrEmpty(max_results)) max_results = "10";
            PagingModel page = new PagingModel()
            { offset = int.Parse(start_index), limit = int.Parse(max_results), cpid = -1, token = access_token };
            // khai bao service
            film_userService film_userService = new film_userService();
            user_linkService user_linkService = new user_linkService();
            film_videoService film_videoService = new film_videoService();
            DateTime dtStart = DateTime.Now;
            //setting parameters
            var check_categoryid = (categoryId.ToString() == "" || categoryId.ToString() == "-1") ? false : true;
            if (check_categoryid)
            {
                var obj = new film_catalogService().GetByCode(categoryId.ToString());
                if (obj != null) page.categoryid = obj.Id;
            }
            logItem("Step 0.1===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
            dtStart = DateTime.Now;

            //setting token
            if (!string.IsNullOrEmpty(access_token)) page.token = access_token;
            if (!string.IsNullOrEmpty(page.token))
            {
                film_user user = film_userService.GetByToken(page.token);
                if (user != null)
                {
                    user_link user_Link = user_linkService.GetById(user.link_id);
                    if (user_Link != null)
                    {
                        page.link_id = user_Link.Id;
                        page.typekid = user_Link.typekid;
                    }
                }
            }
            logItem("Step 1===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
            dtStart = DateTime.Now;

            if (query_type == "INFO")
            {
                film_userService film = new film_userService();
                film_user user = film.GetByToken(access_token);

                if (user != null)
                {
                    user.password = "";
                    user.is_required_paid = user.datecreated.Value.CompareTo(DateTime.Now.AddMonths(-1)) > 0 && user.sub_type == 0 ? 0 : 1;
                    var checkExpired = new film_userService().CheckExpired(user);
                    return new DataResult() { error = 0, data = user, package = checkExpired };
                }
                else return new DataResult() { error = 1, data = null };
            }
            if (query_type == "NEW1")
            {
                film_user user = film_userService.GetByToken(page.token);
                if (user != null)
                {
                    user_link user_Link = user_linkService.GetById(user.link_id);
                    if (user_Link != null)
                    {
                        page.link_id = user_Link.Id;
                        page.typekid = user_Link.typekid;
                    }
                }
                List<Video> lstVideo = new List<Video>();
                //var check_categoryid = categoryId.ToString() == "" ? false : true;
                //if (check_categoryid)
                //{
                //    page.categoryid = new film_catalogService().GetByCode(categoryId.ToString()).Id;
                //}
                VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                List<vw_film_video> lst = film_videoService.GetNewVideo(page);
                foreach (vw_film_video video_db in lst)
                {
                    Video video = convert_to_video(video_db, categoryId.ToString());
                    if (video.images.high.Count == 0)
                    {
                        video.images.small = new List<string>() { video.thumbnails.high };
                        video.images.standard = new List<string>() { video.thumbnails.high };
                        video.images.high = new List<string>() { video.thumbnails.high };
                        video.images.maxres = new List<string>() { video.thumbnails.high };
                        video.imageUrl = video.imageUrl;
                    }
                    video.slug = convertToSlug(video.title);
                    video.url = convertToSlug(video.title) + "-p" + video.id;
                    video.episode_current = video.episode_current;
                    video.episode = video.episode;
                    video.like = video.like;
                    video.catalog_id = video.catalog_id;
                    video.dislike = video.dislike;
                    video.catalog_id_title = video.catalog_id_title;
                    video.director = video.director;
                    video.publish_year = video.publish_year;
                    video.duration = video.duration;
                    video.actor = video.actor;
                    lstVideo.Add(video);
                }
                videoResult.total = film_videoService.CountAll(page);
                videoResult.videos = lstVideo;
                return videoResult;
            }
            if (query_type == "NEW")
            {
                film_user user = film_userService.GetByToken(page.token);
                List<Video> lstVideo = new List<Video>();
                VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                // check danh mục (categoriId)

                // check token "" !=null
                if (!string.IsNullOrEmpty(page.token))
                {
                    if (user != null)
                    {
                        user_link user_Link = user_linkService.GetById(user.link_id);
                        if (user_Link != null)
                        {
                            page.link_id = user_Link.Id;
                            page.typekid = user_Link.typekid;
                            List<vw_film_video> lst = film_videoService.GetNewVideoByToken(page);
                            foreach (vw_film_video video_db in lst)
                            {
                                Video video = convert_to_video(video_db, categoryId.ToString());
                                if (video.images.high.Count == 0)
                                {
                                    video.images.small = new List<string>() { video.thumbnails.high };
                                    video.images.standard = new List<string>() { video.thumbnails.high };
                                    video.images.high = new List<string>() { video.thumbnails.high };
                                    video.images.maxres = new List<string>() { video.thumbnails.high };
                                    video.imageUrl = video.imageUrl;
                                }
                                video.slug = convertToSlug(video.title);
                                video.url = convertToSlug(video.title) + "-p" + video.id;
                                video.episode_current = video.episode_current;
                                video.episode = video.episode;
                                video.like = video.like;
                                video.catalog_id = video.catalog_id;
                                video.dislike = video.dislike;
                                video.catalog_id_title = video.catalog_id_title;
                                video.director = video.director;
                                video.publish_year = video.publish_year;
                                video.duration = video.duration;
                                video.actor = video.actor;
                                lstVideo.Add(video);
                            }
                            videoResult.total = film_videoService.CountViewNew(page);
                            videoResult.videos = lstVideo;
                            return videoResult;
                        }
                        videoResult.msg = "Không nhận diện được người dùng";
                        videoResult.videos = lstVideo;
                        return videoResult;
                    }
                    videoResult.msg = "Token sai hoặc hết hạn";
                    videoResult.videos = lstVideo;
                    return videoResult;
                }
                // không có token
                dtStart = DateTime.Now;
                List<vw_film_video> lst1 = film_videoService.GetNewVideo(page);
                logItem("Step a00===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
                dtStart = DateTime.Now;
                foreach (vw_film_video video_db in lst1)
                {
                    Video video = convert_to_video(video_db, categoryId.ToString());
                    if (video.images.high.Count == 0)
                    {
                        video.images.small = new List<string>() { video.thumbnails.high };
                        video.images.standard = new List<string>() { video.thumbnails.high };
                        video.images.high = new List<string>() { video.thumbnails.high };
                        video.images.maxres = new List<string>() { video.thumbnails.high };
                        video.imageUrl = video.imageUrl;
                    }
                    video.slug = convertToSlug(video.title);
                    video.url = convertToSlug(video.title) + "-p" + video.id;
                    video.episode_current = video.episode_current;
                    video.episode = video.episode;
                    video.like = video.like;
                    video.catalog_id = video.catalog_id;
                    video.dislike = video.dislike;
                    video.catalog_id_title = video.catalog_id_title;
                    video.director = video.director;
                    video.publish_year = video.publish_year;
                    video.duration = video.duration;
                    video.actor = video.actor;
                    lstVideo.Add(video);
                }
                videoResult.total = lstVideo.Count;
                videoResult.videos = lstVideo;
                logItem("Step a01===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");

                return videoResult;
            }
            if (query_type == "CATEGORY")
            {
                List<Video> lstVideo = new List<Video>();
                film_videoService film_service = new film_videoService();
                VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                if (int.Parse(categoryId) == -1)
                {
                    page.categoryid = int.Parse(categoryId);

                    dtStart = DateTime.Now;
                    List<vw_film_video> lst1 = film_service.GetViewAllItem_Episode(page);
                    List<vw_film_video> lst = film_service.GetViewAllItem_mobile(page);

                    logItem("Step 2===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
                    dtStart = DateTime.Now;

                    lst = lst1.Union(lst).OrderBy(e => e.datecreated).ToList<vw_film_video>();
                    foreach (vw_film_video video_db in lst)
                    {
                        Video video = convert_to_video(video_db, categoryId.ToString());
                        if (video.images.high.Count == 0)
                        {
                            video.images.small = new List<string>() { video.thumbnails.high };
                            video.images.standard = new List<string>() { video.thumbnails.high };
                            video.images.high = new List<string>() { video.thumbnails.high };
                            video.images.maxres = new List<string>() { video.thumbnails.high };
                            video.imageUrl = video.imageUrl;
                        }
                        video.slug = convertToSlug(video.title);
                        video.url = convertToSlug(video.title) + "-p" + video.id;
                        video.episode_current = video.episode_current;
                        video.episode = video.episode;
                        video.like = video.like;
                        video.dislike = video.dislike;
                        video.catalog_id = video.catalog_id;
                        video.catalog_id_title = video.catalog_id_title;
                        video.director = video.director;
                        video.publish_year = video.publish_year;
                        video.duration = video.duration;
                        video.actor = video.actor;
                        video.filter = video.filter;
                        lstVideo.Add(video);
                    }
                    videoResult.total = lstVideo.Count();
                    videoResult.videos = lstVideo;
                    logItem("Step 3===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
                    dtStart = DateTime.Now;
                    return videoResult;
                }
                if (int.Parse(categoryId) == 100)
                {
                    page.catalogcode = int.Parse(categoryId);
                    List<vw_film_video> lst = film_service.GetViewByLive(page);
                    foreach (vw_film_video video_db in lst)
                    {
                        Video video = convert_to_video(video_db, categoryId.ToString());
                        if (video.images.high.Count == 0)
                        {
                            video.images.small = new List<string>() { video.thumbnails.high };
                            video.images.standard = new List<string>() { video.thumbnails.high };
                            video.images.high = new List<string>() { video.thumbnails.high };
                            video.images.maxres = new List<string>() { video.thumbnails.high };
                            video.imageUrl = video.imageUrl;
                        }
                        video.slug = convertToSlug(video.title);
                        video.url = convertToSlug(video.title) + "-p" + video.id;
                        video.episode_current = video.episode_current;
                        video.episode = video.episode;
                        video.like = video.like;
                        video.dislike = video.dislike;
                        video.catalog_id = video.catalog_id;
                        video.catalog_id_title = video.catalog_id_title;
                        video.director = video.director;
                        video.publish_year = video.publish_year;
                        video.duration = video.duration;
                        video.actor = video.actor;
                        video.filter = video.filter;
                        lstVideo.Add(video);
                    }
                    videoResult.total = lstVideo.Count();
                    videoResult.videos = lstVideo;
                    return videoResult;
                }
                dtStart = DateTime.Now;
                film_catalog film_Catalog = new film_catalogService().GetByCode(categoryId.ToString());
                logItem("Step 3.0===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");

                if (film_Catalog != null)
                {
                    //Stopwatch watch = new Stopwatch();
                    //watch.Start();
                    page.categoryid = film_Catalog.Id;
                    dtStart = DateTime.Now;
                    List<vw_film_video> lst = film_service.GetViewAllItem_mobile(page);
                    logItem("Step 3.1.0===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
                    dtStart = DateTime.Now;

                    List<vw_film_video> lst1 = film_service.GetViewAllItem_Episode(page);
                    logItem("Step 3.1.1===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
                    dtStart = DateTime.Now;
                    lst = lst1.Union(lst).ToList<vw_film_video>();
                    foreach (vw_film_video video_db in lst)
                    {
                        Video video = convert_to_video(video_db, categoryId.ToString());
                        if (video.images.high.Count == 0)
                        {
                            video.images.small = new List<string>() { video.thumbnails.high };
                            video.images.standard = new List<string>() { video.thumbnails.high };
                            video.images.high = new List<string>() { video.thumbnails.high };
                            video.images.maxres = new List<string>() { video.thumbnails.high };
                            video.imageUrl = video.imageUrl;
                        }
                        video.slug = convertToSlug(video.title);
                        video.url = convertToSlug(video.title) + "-p" + video.id;
                        video.episode_current = video.episode_current;
                        video.episode = video.episode;
                        video.like = video.like;
                        video.dislike = video.dislike;
                        video.catalog_id = video.catalog_id;
                        video.catalog_id_title = video.catalog_id_title;
                        video.director = video.director;
                        video.publish_year = video.publish_year;
                        video.duration = video.duration;
                        video.actor = video.actor;
                        video.filter = video.filter;
                        lstVideo.Add(video);
                    }
                    videoResult.total = lstVideo.Count();
                    videoResult.videos = lstVideo;
                    logItem("Step 4.0===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");

                    //TimeSpan timeSpan = watch.Elapsed;
                    //LogService.logItem(string.Format("Time: {0}h {1}m {2}s {3}ms", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds));
                    //LogService.logItem("Xong hàm :---- "+DateTime.Now.ToString("yyyy-mm-dd hh-mm-ss") + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond));
                    return videoResult;
                }
                logItem("Step 4.1===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
                videoResult.total = 0;
                videoResult.videos = lstVideo;
                return videoResult;
            }
            if (query_type == "SEARCH")
            {
                List<Video> lstVideo = new List<Video>();
                // = int.Parse(categoryId.ToString());
                page.categoryid = -1;// new film_catalogService().GetByCode(categoryId.ToString()).Id;
                page.search = search;
                VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                film_videoService film_service = new film_videoService();
                //List<vw_film_video> lst = film_service.GetViewAllItem_mobile(page);

                List<vw_film_video> lst = film_service.GetViewAllItem_mobile(page);
                List<vw_film_video> lst1 = film_service.GetViewAllItem_Episode(page);


                lst = lst1.Union(lst).OrderBy(e => e.datecreated).OrderBy(e => e.datecreated).ToList<vw_film_video>();
                foreach (vw_film_video video_db in lst)
                {
                    lstVideo.Add(convert_to_video(video_db, categoryId.ToString()));
                }
                videoResult.total = film_service.CountAll(page);
                videoResult.videos = lstVideo;
                return videoResult;
            }
            if (query_type == "RELATED")
            {
                List<Video> lstVideo = new List<Video>();
                if (!String.IsNullOrEmpty(categoryId.ToString()))
                    page.catalogcode = int.Parse(categoryId.ToString());

                VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                film_videoService film_service = new film_videoService();
                List<vw_film_video> lst = film_service.GetRelatedVideo(videos_id);
                foreach (vw_film_video video_db in lst)
                {
                    lstVideo.Add(convert_to_video(video_db, "0"));
                }
                videoResult.total = film_service.CountAll(page);
                videoResult.videos = lstVideo;
                return videoResult;
            }
            if (query_type == "HISTORY")
            {
                List<Video> lstVideo = new List<Video>();
                film_user user = new film_userService().GetByToken(access_token);
                if (user == null) return lstVideo;

                if (!String.IsNullOrEmpty(categoryId.ToString()))
                    page.catalogcode = int.Parse(categoryId.ToString());
                page.userid = user.Id;
                page.link_id = user.link_id;

                VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                film_videoService film_service = new film_videoService();
                List<vw_film_video> lst = film_service.GetViewHistory(page);
                foreach (vw_film_video video_db in lst)
                {
                    lstVideo.Add(convert_to_video_view(video_db, "0", page.userid));
                }
                videoResult.total = film_service.CountAll(page);
                videoResult.videos = lstVideo;
                return videoResult;
            }
            if (query_type == "EPISODE")
            {
                List<Video> lstVideo = new List<Video>();
                VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                film_videoService film_service = new film_videoService();
                //List<vw_film_video> lst = film_service.GetRelatedVideo(videos_id);
                List<vw_film_video> lst = new film_videoService().GetAllEpisodeByVideoId(videos_id, page.offset, page.limit);
                foreach (vw_film_video video_db in lst)
                {
                    var videox = convert_to_video(video_db, "0");

                    videox.images.small = new List<string>() { };
                    videox.images.standard = new List<string>() { };
                    videox.images.high = new List<string>() { };
                    videox.images.maxres = new List<string>() { };
                    videox.state = null;
                    lstVideo.Add(videox);
                }
                videoResult.total = film_service.CountAll(page);
                videoResult.videos = lstVideo;
                return videoResult;
            }
            if (query_type == "RECOMMEND")
            {
                List<Video> lstVideo = new List<Video>();
                VideoResult videoResult = new VideoResult()
                {
                    error = 1,
                    msg = "Thất bại"
                };
                if (!string.IsNullOrEmpty(categoryId))
                {
                    if (int.Parse(categoryId) <= 0) categoryId = "-1";
                    page.categoryid = int.Parse(categoryId);
                }
                if (!string.IsNullOrEmpty(page.token))
                {
                    film_user user = new film_userService().GetByToken(page.token);
                    if (user != null)
                    {
                        user_link user_Link = user_linkService.GetById(user.link_id);
                        if (user_Link != null)
                        {
                            page.link_id = user_Link.Id;
                            page.typekid = user_Link.typekid;
                            videoResult.error = 0;
                            videoResult.msg = "Thành công";
                            List<vw_film_video> lst = film_videoService.GetRecommendVideoByToken(page);
                            foreach (vw_film_video video_db in lst)
                            {
                                lstVideo.Add(convert_to_video(video_db, "0"));
                            }
                            videoResult.total = lstVideo.Count();
                            videoResult.videos = lstVideo;
                            return videoResult;
                        }
                        videoResult.msg = "Không nhận diện được tài khoản";
                        return videoResult;
                    }
                    videoResult.msg = "Token sai hoặc hết hạn";
                    videoResult.videos = lstVideo;
                    return videoResult;
                }
                // không có token
                List<vw_film_video> list_not_token = film_videoService.GetRecommendVideo(page);
                foreach (vw_film_video video_db in list_not_token)
                {
                    lstVideo.Add(convert_to_video(video_db, "0"));
                }
                videoResult.total = lstVideo.Count();
                videoResult.videos = lstVideo;
                videoResult.msg = "Thiếu token";
                return videoResult;
            }
            if (query_type == "DIRECTSTREAM")
            {
                //setup params
                page.token = access_token.ToString();
                VideoResultDetail videoResult = new VideoResultDetail() { error = 0, msg = "" };
                film_videoService film_service = new film_videoService();
                string[] arrVideos = videos_id.ToString().Split(',');
                List<VideoDetail> detail = new List<VideoDetail>();
                foreach (string videos_idx in arrVideos)
                {
                    //vw_film_video o_film_video = film_service.GetViewByID(videos_idx.ToString());
                    vw_film_video o_film_video = null;
                    if (typevideo == "id") o_film_video = film_service.GetViewById(int.Parse(videos_idx));
                    else o_film_video = film_service.GetViewByAliasID(videos_idx);
                    if (o_film_video == null) continue;
                    new film_video_user_logService().UpdateViewToken(o_film_video.Id, page.token);
                    VideoDetail detailx = convert_to_video_detail(o_film_video);
                    for (int jx = 0; jx <= detailx.linkDirects.Count - 1; jx++)
                    {
                        LinkDirect linkDirect = detailx.linkDirects[jx];
                        linkDirect.hls = linkDirect.hls + "?token=" + access_token;
                        detailx.linkDirects[jx] = linkDirect;
                    }
                    detail.Add(detailx);
                }
                videoResult.videos = detail;
                return videoResult;
            }
            if (query_type == "AIO")
            {
                List<film_catalog> allCate = new film_catalogService().GetAllItem(null);
                List<KeyValuePair<object, object>> lstResults = new List<KeyValuePair<object, object>>();
                foreach (var cate in allCate)
                {
                    LogService.logItem(cate.Id);
                    page.categoryid = int.Parse(cate.Id.ToString()); // Gán CateID cho page.cateID để lấy video theo cateID
                    page.limit = int.Parse(max_results);
                    VideoResult videoResult = new VideoResult() { error = 0, msg = "" };
                    film_videoService film_service = new film_videoService();
                    List<vw_film_video> lst = film_service.GetListViewByID(page);
                    List<Video> lstVideo = new List<Video>();
                    // Lấy ra được 12 video sau đó Add vào List lstVideo
                    foreach (vw_film_video video_db in lst)
                    {
                        Video video = convert_to_video(video_db, categoryId);
                        if (video.images.high.Count == 0)
                        {
                            video.images.small = new List<string>() { video.thumbnails.high };
                            video.images.standard = new List<string>() { video.thumbnails.high };
                            video.images.high = new List<string>() { video.thumbnails.high };
                            video.images.maxres = new List<string>() { video.thumbnails.high };
                            video.imageUrl = video.imageUrl;
                        }
                        video.slug = convertToSlug(video.title);
                        video.url = convertToSlug(video.title) + "-p" + video.id;
                        video.episode_current = video.episode_current;
                        video.episode = video.episode;
                        video.like = video.like;
                        video.dislike = video.dislike;
                        video.catalog_id = video.catalog_id;
                        video.catalog_id_title = video.catalog_id_title;
                        video.director = video.director;
                        video.publish_year = video.publish_year;
                        video.duration = video.duration;
                        video.actor = video.actor;
                        lstVideo.Add(video);
                        videoResult.total = film_service.CountAll(page);
                        videoResult.videos = lstVideo;
                    }
                    // Sau khi lấy được 12 video và theo cateID thì thêm nó vào List Result
                    lstResults.Add(new KeyValuePair<object, object>(cate, lstVideo));

                    // Chạy vòng for tiếp theo
                    //LogService.logItem(lstResults);
                }
                return lstResults;
            }
            if (query_type == "payment")
            {
                if (user_type == "list_method")
                {
                    List<PaymentMethodResult> lst = new List<PaymentMethodResult>();
                    lst.Add(new PaymentMethodResult() { code = 1, title = "SMS", desc = "" });
                    lst.Add(new PaymentMethodResult() { code = 2, title = "Bank", desc = "" });
                    return lst;
                }
                if (user_type == "create")
                {
                    packageService service = new packageService();
                    film_userService userService = new film_userService();
                    film_user user = userService.GetByToken(access_token);
                    if (user == null) return new PaymentResult()
                    {
                        status = "0",
                        desc = "Access token is invalid",
                        url = ""
                    };

                    string url = service.create_transaction(int.Parse(packageid), user.Id, device);
                    return new PaymentResult()
                    {
                        status = "1",
                        desc = "",
                        url = url
                    };
                }
            }
            return new VideoResult();
        }

        //subcribe:20190828090505:hongancp,Hongancp!@#45,LALATV_NGAY,975293382,20190828090519,20190828090519,REAL,3000,#FULLSM,20190829000000,
        //getcontent:20190826102309:hongancp,Hongancp!@#45,LALATV_NGAY,867728907,Kta,0,Kta

        public int update_create_user(string phone, string subtype, string substate, string fullcmd)
        {
            update_create_user_log(phone, subtype, substate, fullcmd);
            phone = common_phone(phone);
            film_user dtx = check_user(phone);
            int result = 0;
            if (dtx == null)
            {
                using (var db = _connectionData.OpenDbConnection())
                {
                    string sSql = "INSERT INTO  film_users" +
                "(username ,password ,avatar ,email ,phone ,fullname,`desc`," +
                "datecreated ,userid ,cpid , roleid ,gender , birthday,sub_type,sub_state)" +
                " VALUES "
                + "(" + comm.to_sqltext(phone) + ",'e10adc3949ba59abbe56e057f20f883e' ,''," + comm.to_sqltext(phone)
                + " ," + comm.to_sqltext(phone) + " ," + comm.to_sqltext(phone)
                + "," + comm.to_sqltext(fullcmd) + ","
                + "CURRENT_DATE() ,-1 ,1 , -1 ,-1 , null," + comm.to_sqlnumber(subtype) + "," + comm.to_sqlnumber(substate) + ")";
                    int x = db.ExecuteNonQuery(sSql);
                    LogUtil.getInstance().logSk(sSql + "====>" + x.ToString(), "update");
                    result = x;
                }
            }
            else
            {
                using (var db = _connectionData.OpenDbConnection())
                {
                    string sSql = "update film_users set fullname=fullname ";
                    if (subtype != "-1") sSql = sSql + ",sub_type=" + comm.to_sqlnumber(subtype);
                    if (substate != "-1") sSql = sSql + ",sub_state = " + comm.to_sqlnumber(substate);
                    sSql = sSql + " where phone=" + comm.to_sqltext(phone);
                    int x = db.ExecuteNonQuery(sSql);
                    LogUtil.getInstance().logSk(sSql + "====>" + x.ToString(), "update");
                    result = x;
                }
            }
            if (subtype != "0")
            {
                updatefilm_drm(subtype, substate, phone);
            }
            return result;
        }

        public int create_token_for_user(string phone, string subtype, string substate)
        {
            string token = System.Guid.NewGuid().ToString();
            string days = "1";
            if (subtype == "1") days = "1";
            if (subtype == "2") days = "7";
            if (subtype == "5") days = "30";

            using (var db = _connectionData.OpenDbConnection())
            {
                string sSql = "INSERT INTO `film_drm`" +
                                "(`name`,`userid`,`key`,`metadata`,`datecreate`,`datesubcribe`,`note`)" +
                " VALUES "
                + "(" + comm.to_sqltext(phone) + ",1," + comm.to_sqltext(token)
                + "," + comm.to_sqltext(subtype + "," + substate)
                + ",CURRENT_DATE() ,DATE_ADD(CURRENT_DATE(),INTERVAL " + days + " DAY)"
                + "," + comm.to_sqltext("From Sms Viettel") + ")";
                int x = db.ExecuteNonQuery(sSql);
                LogUtil.getInstance().logSk(sSql + "====>" + x.ToString(), "update");
                return x;
            }
        }

        public void update_create_user_log(string phone, string subtype, string substate, string fullcmd)
        {
            phone = common_phone(phone);
            using (var db = _connectionData.OpenDbConnection())
            {
                string sSql = "INSERT INTO  film_users_log" +
            "(username ,password ,avatar ,email ,phone ,fullname,`desc`," +
            "datecreated ,userid ,cpid , roleid ,gender , birthday,sub_type,sub_state,date_hb)" +
            " VALUES "
            + "(" + comm.to_sqltext(phone) + ",'e10adc3949ba59abbe56e057f20f883e' ,''," + comm.to_sqltext(phone)
            + " ," + comm.to_sqltext(phone) + " ," + comm.to_sqltext(phone)
            + "," + comm.to_sqltext(fullcmd) + ","
            + "NOW() ,-1 ,1 , -1 ,-1 , null," + comm.to_sqlnumber(subtype) + "," + comm.to_sqlnumber(substate) + ",NOW())";
                int x = db.ExecuteNonQuery(sSql);
                //x = x + 1;
                LogUtil.getInstance().logSk(sSql + "====>" + x.ToString(), "update");
            }
        }

        public static string to_sqltext(string value)
        {
            return to_sql(value, "text");
        }

        public static string to_sql(string value, string datatype)
        {
            if ((value == (null)) || (value == ("")) || (value == ("NULL")))
            {
                return "NULL";
            }
            else
            {
                string temp;
                //temp = datatype;
                temp = (string)datatype.ToLower();
                if (temp.Equals("number"))
                {
                    double Num;
                    bool isNum;
                    //isNum = false;
                    //try{
                    isNum = double.TryParse(value, out Num);
                    //}
                    //catch
                    //{
                    //    isNum = false;
                    //}

                    if (isNum)
                    {
                        return Num.ToString();
                    }
                    else
                    {
                        return "0";
                    }
                }
                else if (temp.Equals("date"))
                {
                    return "Convert([datetime], '" + value + "', 103)";
                }
                else //Luu tru va hien thi theo dinh dang dd/mm/yyyy - code 103
                {
                    return "N'" + value.Replace("'", "''") + "'";
                }
            }
        }

        public static film_user check_user(string phone)
        {
            return new film_userService().GetByPhone(phone);
            /*phone = common_phone(phone);
            string sSql = "select *  from film_users where phone=" + comm.to_sqltext(phone);
            return DBUtil.SelectTable(sSql);*/
        }

        public static string getStateUser(string phone)
        {
            phone = common_phone(phone);
            film_user u = new film_userService().GetByPhone(phone);
            if (u == null) return "0";
            return (u.sub_state ?? 0).ToString();
            /*
              phone = common_phone(phone);
            string sSql = "select * from film_users where phone=" + comm.to_sqltext(phone);
            DataTable dt = check_user(phone);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["sub_state"].ToString();
            }
            return "0";
            */
        }

        public static string getSubTypeUser(string phone)
        {
            phone = common_phone(phone);
            film_user u = new film_userService().GetByPhone(phone);
            if (u == null) return "0";
            return (u.sub_type ?? 0).ToString();
            /*
            phone = common_phone(phone);
            string sSql = "select * from film_users where phone=" + to_sqltext(phone);
            DataTable dt = check_user(phone);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["sub_type"].ToString();
            }
            return "0";
            */
        }

        public static string[] get_user_subcribe(string phone)
        {
            phone = common_phone(phone);
            film_user u = new film_userService().GetByPhone(phone);

            if (u == null) return new string[] { "0", "0" };
            return new string[] { (u.sub_type ?? 0).ToString(), (u.sub_state ?? 0).ToString() };
            /*
            phone = common_phone(phone);
            string sSql = "select sub_type,sub_state from film_users where phone=" + comm.to_sqltext(phone);
            DataTable dt = check_user(phone);
            if (dt.Rows.Count > 0)
            {
                return new string[] { dt.Rows[0]["sub_type"].ToString(), dt.Rows[0]["sub_state"].ToString() };
            }
            return new string[] { "0", "0" };
            */
        }

        public static string common_phone(string phone)
        {
            if (phone.StartsWith("84")) phone = phone.Substring(2);
            if (!phone.StartsWith("0")) phone = "0" + phone;
            return phone;
        }

        public class VideoCatalog
        {
            public int error { get; set; }
            public string msg { get; set; }
            public string data { get; set; }
        }

        public string processing_syn_img(int videoid)
        {
            StringBuilder sb = new StringBuilder();

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query = query.Where(e => (e.code != null));
                //if (videoid>0) query.Where(e => (e.Id ==videoid));
                List<film_video> lst_film = db.Select(query);
                foreach (film_video film in lst_film)
                {
                    if (film.thumb_file.StartsWith("http://img.lalatv.com.vn/") || film.thumb_file.StartsWith("http://image.lalatv.com.vn/"))
                    {
                        film.thumb_file = get_file_img_thumb_by_code(film.code);
                    }
                    var query_image = db.From<film_video_image>().Where(e => e.filmid == film.Id);
                    List<film_video_image> lst_film_image = db.Select(query_image);
                    foreach (film_video_image image in lst_film_image)
                    {
                        if (image.url.StartsWith("http://img.lalatv.com.vn/") || image.url.StartsWith("http://image.lalatv.com.vn/"))
                        {
                            image.url = get_file_img_thumb_by_code(film.code);
                            sb.Append("image.url1=" + image.url);
                        }
                        sb.Append("image.url2=" + image.url);
                    }
                    db.UpdateAll(lst_film_image);
                }
                db.UpdateAll(lst_film);
            }
            return sb.ToString();
        }

        public string processing_syn_img_sql(int videoid)
        {
            StringBuilder sb = new StringBuilder();
            using (StreamWriter w = new StreamWriter("/sql.txt", true, Encoding.UTF8))
            {
                using (var db = _connectionData.OpenDbConnection())
                {
                    var query = db.From<film_video>();
                    query = query.Where(e => (e.code != null));
                    List<film_video> lst_film = db.Select(query);
                    foreach (film_video film in lst_film)
                    {
                        if (film.thumb_file.StartsWith("http://img.lalatv.com.vn/") || film.thumb_file.StartsWith("http://image.lalatv.com.vn/"))
                        {
                            w.WriteLine("update mvm_videos set image=N'" + film.thumb_file + "' where image like '%/" + film.code + "/%';");
                        }
                    }
                }
            }
            return sb.ToString();
        }

        public string get_file_img_thumb_by_code(string alias)
        {
            string root_path = "/home/data/images/";//thumb/clip/
            DirectoryInfo dir = new DirectoryInfo(root_path);
            FileInfo[] arrFiles = dir.GetFiles(alias + ".png", SearchOption.AllDirectories);
            if (arrFiles.Length == 0) arrFiles = dir.GetFiles(alias + "-*.jpg", SearchOption.AllDirectories);
            //return arrFiles.Length.ToString();
            //return ("count of arrfiles is "+ arrFiles.Length.ToString());
            if (arrFiles.Length > 0)
            {
                string full_path = arrFiles[0].FullName;//arrFiles[0].DirectoryName+"/"+
                return "http://image.lalatv.com.vn/" + full_path.Replace(root_path, "");
            }
            return "";
        }

        public void processing_syn()
        {
            //step1: download category
            List<film_catalog> lstCatalog = new List<film_catalog>();
            List<int> lstIds = new List<int>() { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
            foreach (var catalogid in lstIds)
            {
                lstCatalog.Add(new film_catalog() { Id = catalogid });
            }

            // lstCatalog.Add(new film_catalog() { Id = 5 });
            //step2: download film by each catalog
            using (StreamWriter w = new StreamWriter("videos_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt", false, Encoding.UTF8))
            {
                using (StreamWriter w_detail = new StreamWriter("videos_detail_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt", false, Encoding.UTF8))
                {
                    foreach (film_catalog catalog in lstCatalog)
                    {
                        List<film_video> lstFilm = new List<film_video>();
                        using (WebClient wc = new WebClient())
                        {
                            string urlVideoByCatalogs = "http://sdk.lalatv.com.vn/video?sk=1c1a610eae5028a9fe99fdd2d9176c03dc77&action=GET&query-type=CATEGORY&user-type=CHANNEL&start-index=0&max-results=100&categoryId=" + catalog.Id.ToString();
                            var content = wc.DownloadString(urlVideoByCatalogs);

                            VideoResult data = Newtonsoft.Json.JsonConvert.DeserializeObject<VideoResult>(content);

                            w.WriteLine(content);

                            List<Video> lst_video = data.videos;
                            List<film_video> lst_video_db = new List<film_video>();
                            foreach (Video video in lst_video)
                            {
                                string urlDetail = "http://sdk.lalatv.com.vn/video?access-token=f0ffc06c967469816e61c65b3486a4b2&action=GET&query-type=DIRECTSTREAM&videos-id=" + video.aliasId.ToString();
                                var content1 = wc.DownloadString(urlDetail);

                                VideoResultDetail data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<VideoResultDetail>(content1);
                                w_detail.WriteLine(data1);
                                convert_to_video_in_db(video, catalog.Id, data1);
                            }
                        }
                    }
                }
            }
        }

        public AuthenResult processing_authen(StringValues access_token, StringValues query_type, StringValues user_type, StringValues start_index, StringValues max_results, StringValues categoryId)
        {
            return new AuthenResult()
            {
                error = 0,
                msg = "",
                accesstoken = System.Guid.NewGuid().ToString().Replace("-", ""),
                expired = DateTime.Now.AddDays(2).Ticks
            };
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public film_article GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_article GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_article>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_article obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_article>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.url = obj.url;
                        objUpdate.ordinal = obj.ordinal;
                        objUpdate.target = obj.target;
                        objUpdate.status = obj.status;
                        objUpdate.created_time = DateTime.Now;
                        objUpdate.title = obj.title;
                        objUpdate.summary = obj.summary;
                        objUpdate.content = obj.content;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_article>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    // objUpdate.Id = obj.Id;
                    objUpdate.url = obj.url;
                    objUpdate.ordinal = obj.ordinal;
                    objUpdate.target = obj.target;
                    objUpdate.status = obj.status;
                    objUpdate.created_time = DateTime.Now;
                    objUpdate.title = obj.title;
                    objUpdate.summary = obj.summary;
                    objUpdate.content = obj.content;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int updatefilm_drm(string sub_type, string sub_state, string phone)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                string token = "";
                token = System.Guid.NewGuid().ToString();

                film_user user = db.Select<film_user>(x => x.phone == phone).FirstOrDefault();
                // film_drm drm = new film_drm();
                var query = db.From<film_drm>()
                         .Where(e => (e.userid == user.Id));
                var drm = db.Select(query).FirstOrDefault();
                if (drm != null)
                {
                    drm.name = user.phone;
                    drm.userid = user.Id;
                    drm.key = token;
                    drm.datecreate = DateTime.Now;
                    drm.note = "create sms";
                    if (sub_type == "1")
                    {
                        DateTime dtExpired = DateTime.Now.AddDays(1);
                        drm.datesubcribe = dtExpired;
                    }
                    else if (sub_type == "2")
                    {
                        DateTime dtExpired = DateTime.Now.AddDays(7);
                        drm.datesubcribe = dtExpired;
                    }
                    else
                    {
                        DateTime dtExpired = DateTime.Now.AddDays(30);
                        drm.datesubcribe = dtExpired;
                    }
                    return (int)db.Update(drm);
                }
                else
                {
                    drm = InitEmpty1();
                    drm.name = user.username;
                    drm.userid = user.Id;
                    drm.key = token;
                    drm.datecreate = DateTime.Now;
                    drm.note = "create sms";
                    if (sub_type == "1")
                    {
                        DateTime dtExpired = DateTime.Now.AddDays(1);
                        drm.datesubcribe = dtExpired;
                    }
                    else if (sub_type == "2")
                    {
                        DateTime dtExpired = DateTime.Now.AddDays(7);
                        drm.datesubcribe = dtExpired;
                    }
                    else
                    {
                        DateTime dtExpired = DateTime.Now.AddDays(30);
                        drm.datesubcribe = dtExpired;
                    }
                    return (int)db.Insert(drm, selectIdentity: true);
                }
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_article>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_article InitEmpty()
        {
            var obj = new film_article();
            obj.Id = 0;
            return obj;
        }

        public film_drm InitEmpty1()
        {
            var obj = new film_drm();
            obj.id = 0;
            return obj;
        }

        /*
		public List<vw_film_article> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

          //  ServiceStackHelper.Help();
          //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
			var query = db.From<vw_film_article>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
				query=query.Where(e => (e.title.Contains(page.search)));

                List<vw_film_article> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
		genvie here
		create view vw_film_article
		as
			select film_article.Id,film_article.url,film_article.ordinal,film_article.target,film_article.status,film_article.created_time,film_article.title,film_article.summary,film_article.content  from film_article
		*/
    }

    public class LogUtil
    {
        public static LogUtil getInstance()
        {
            return new LogUtil();
        }

        public void logSk(string s, string file)
        {
            try
            {
                using (StreamWriter w = new StreamWriter("log_" + file + "_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true, Encoding.UTF8))
                {
                    w.WriteLine(DateTime.Now.ToString("HH:mm:ss") + ":" + s);
                }
            }
            catch { }
        }
    }
}