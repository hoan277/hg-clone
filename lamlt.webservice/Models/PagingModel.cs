﻿namespace lamlt.web.Models
{
    public class PagingModel
    {
        public int offset;
        public int limit;
        public string search;
        public int cpid;
        public int categoryid;
        public int catalogid;
        public int status;
        public int statusUserLog;
        public int catalogcode;
        public string start_time;
        public string end_time;
        public string cp_title;
        public int sub_state;
        public int sub_type;
        public int publish_year;
        public int publish_countryid;
        public int film_type;
        public string action_type;// Dùng cho trước khi xuất Excel film_video_processing
        public string typeName;// Dùng cho film_transactions
        public int userid;// Dùng cho token
        /*
         * dùng cho search link upload_file;
         */
        public string upload_file;
        // Dùng cho livestreaming
        public string live_type;
        public string live_status;
        public int link_id = -1;//dùng để holder giá trị tạm
        public int typekid = 2;
        public string token;
        public int typeid = -1;
    }
    public class Paging1Model
    {
        public int id;
        public string title;
        public string type;
        public string ads_type;
        public string item_type;
        public int offset;
        public int limit;
        public string orderby;
        public int android;
        public int typekid;
    }
}
