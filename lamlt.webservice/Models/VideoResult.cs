﻿using System.Collections.Generic;

namespace Lalatv.Models
{
    public class Thumbnails
    {
        public string small { get; set; }
        public string medium { get; set; }
        public string high { get; set; }
        public string standard { get; set; }
        public string maxres { get; set; }
    }

    public class State
    {
        public string video { get; set; }
        public string process { get; set; }
        public string approve { get; set; }
    }

    public class Images
    {
        public List<string> small { get; set; }
        public List<string> medium { get; set; }
        public List<string> high { get; set; }
        public List<string> standard { get; set; }
        public List<string> maxres { get; set; }
    }

    public class Embed
    {
        public string shortCode { get; set; }
        public string script { get; set; }
    }

    public class Owner
    {
        public string displayname { get; set; }
        public string email { get; set; }
        public int ownerId { get; set; }
    }

    public class FileSizeConvert
    {
        public int __invalid_name__1080p { get; set; }
        public int __invalid_name__360p { get; set; }
        public int __invalid_name__480p { get; set; }
        public int __invalid_name__720p { get; set; }
    }

    public class Video
    {
        public int id { get; set; }
        public string aliasId { get; set; }
        public string title { get; set; }
        public string slug { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string catalog_id_title { get; set; }
        public string imageUrl { get; set; }
        public string director { get; set; }
        public string actor { get; set; }
        public string country { get; set; }
        public int publish_year { get; set; }
        public int farmId { get; set; }
        public int catalog_id { get; set; }
        public int catalog_id_code { get; set; }
        public Thumbnails thumbnails { get; set; }
        public long createTime { get; set; }
        public string duration { get; set; }
        public State state { get; set; }
        public int view { get; set; }
        public Images images { get; set; }
        public Embed embed { get; set; }
        public List<object> streams { get; set; }
        public Owner owner { get; set; }
        public bool ads { get; set; }
        public List<int> cateIds { get; set; }
        public List<int> typeIds { get; set; }
        public List<int> tagIds { get; set; }
        public List<string> tags { get; set; }
        public List<string> linksDownload { get; set; }
        public long fileSize { get; set; }
        public long totalFileSize { get; set; }
        public int? episode_current { get; set; }
        public int? episode { get; set; }
        public int? like { get; set; }
        public int? dislike { get; set; }
        public FileSizeConvert fileSizeConvert { get; set; }
        public string url_trailer { get; set; }
        public string url_trailer_thumbnail { get; set; }
        public int? isview { get; set; }
        public int? count_share { get; set; }
        public int? filter { get; set; }
        public int? time_end { get; set; }
    }

    public class PaymentMethodResult
    {
        public int code { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string provider { get; set; }
    }

    public class PaymentResult
    {
        public string status { get; set; }
        public string url { get; set; }
        public string desc { get; set; }
    }

    public class VideoResult
    {
        public int error { get; set; }
        public string msg { get; set; }
        public long total { get; set; }
        public List<Video> videos { get; set; }
    }

    public class DataResult
    {
        public int error { get; set; }
        public string msg { get; set; }
        public object data { get; set; }
        public object package { get; set; }
    }

    public class AuthenResult
    {
        public int error { get; set; }
        public string msg { get; set; }
        public string accesstoken { get; set; }
        public long expired { get; set; }
    }

    public class LinkDirect
    {
        public string quality { get; set; }
        public string hls { get; set; }
        public long size { get; set; }
    }

    public class VideoDetail
    {
        public int id { get; set; }
        public string aliasId { get; set; }
        public string title { get; set; }
        public string slug { get; set; }
        public string catalog_id_title { get; set; }
        public int catalog_id { get; set; }
        public string url { get; set; }
        public List<LinkDirect> linkDirects { get; set; }
        public int error { get; set; }
        public int? episode { get; set; }
        public int? episode_current { get; set; }
        public string url_image { get; set; }
        public string url_trailer { get; set; }
        public string url_trailer_thumbnail { get; set; }
        public string desc { get; set; }
        public string actor { get; set; }
        public string director { get; set; }
        public string country { get; set; }
        public int publish_year { get; set; }
        public string duration { get; set; }
        public int episode_total { get; set; }
        public int like { get; set; }
        public int count_share { get; set; }
        public int filter { get; set; }
    }

    public class VideoResultDetail
    {
        public int error { get; set; }
        public string msg { get; set; }
        public List<VideoDetail> videos { get; set; }
    }
}