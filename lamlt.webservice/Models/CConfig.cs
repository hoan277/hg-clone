﻿namespace lamlt.web.Models
{
    public class CConfig
    {
        public static string SESSION_USERNAME = "username";
        public static string SESSION_ROLE = "userrole";
        public static string SESSION_SUPPERADMIN_FLAG = "issuperadmin";
        public static string SESSION_USERID = "userid";
        public static string SESSION_CPID = "hotelid";
        public static string SESSION_USER = "user";
    }
}
