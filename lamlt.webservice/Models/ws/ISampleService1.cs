﻿using System.ServiceModel;

namespace lamlt.webservice.Models.ws
{
    [ServiceContract]
    public interface ISampleService1
    {

        [OperationContract]
        void XmlMethod(System.Xml.Linq.XElement xml);
        [OperationContract]
        MyCustomModel TestCustomModel(MyCustomModel inputModel);
        [OperationContract]
        string contentRequest(string username, string password, string serviceid, string msisdn, string param, int amount, string command);
    }
}
