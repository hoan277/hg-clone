﻿using System.ServiceModel;

namespace lamlt.webservice.Models.ws
{
    [ServiceContract]
    public interface ISampleService
    {

        [OperationContract]
        void XmlMethod(System.Xml.Linq.XElement xml);
        [OperationContract]
        MyCustomModel TestCustomModel(MyCustomModel inputModel);
        [OperationContract]
        string SubRequest(string username, string password, string serviceid, string msisdn, string chargetime, string param, string mode, int amount, string command, string nextrenew, string cp_request_id);
    }
}
