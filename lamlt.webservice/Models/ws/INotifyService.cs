﻿using System.ServiceModel;

namespace lamlt.webservice.Models.ws
{
    [ServiceContract]
    public interface INotifyService
    {

        [OperationContract]
        void XmlMethod(System.Xml.Linq.XElement xml);
        [OperationContract]
        MyCustomModel TestCustomModel(MyCustomModel inputModel);
        [OperationContract]
        string resultRequestfunction(string userName, string password, string cpRequestId, string mobile, string price, string cmd, string responseCode);
    }
}
