﻿using System.ServiceModel;

namespace lamlt.webservice.Models.ws
{
    [ServiceContract]
    public interface ISmsGwService
    {

        [OperationContract]
        void XmlMethod(System.Xml.Linq.XElement xml);
        [OperationContract]
        MyCustomModel TestCustomModel(MyCustomModel inputModel);
        [OperationContract]
        string resultRequestfunction(string username, string password, string msisdn, string content, string shortcode, string alias, string param, string moid, string serviceid, string transid, string datetime, string datasign);
    }
}
