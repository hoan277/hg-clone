﻿using System.ServiceModel;

namespace lamlt.webservice.Models.ws
{
    [ServiceContract]
    public interface IReceiveResult
    {

        [OperationContract]
        void XmlMethod(System.Xml.Linq.XElement xml);
        [OperationContract]
        MyCustomModel TestCustomModel(MyCustomModel inputModel);
        [OperationContract]
        string resultRequestfunction(string username, string password, string serviceid, string msisdn, string chargetime, string param, string mode, int amount, string detail, string nextRenewalTime, string transid);
    }
}
