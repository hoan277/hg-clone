﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.IO;

namespace EmployeeJquery.Ui
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                        .UseKestrel()
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .CaptureStartupErrors(true)
                        .UseSetting("detailedErrors", "true")
                        .ConfigureLogging((hostingContext, logging) =>
                        {
                            logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                            logging.AddConsole();
                            logging.AddDebug();
                            logging.AddEventSourceLogger();
                        })
                        .UseIISIntegration()
                        .UseStartup<Startup>()

                        //.UseKestrel(options =>
                        //{
                        //    options.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(120);
                        //    options.Limits.RequestHeadersTimeout = TimeSpan.FromMinutes(120);
                        //})
                        //.UseUrls("http://0.0.0.0:7005")
                        .UseUrls("http://0.0.0.0:5005")
                        .UseKestrel(o => { o.Limits.MaxRequestBodySize = null; })
                        .Build();

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
