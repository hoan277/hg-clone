﻿
var Cookie = {
    create: function (name, value, days) {
        var expires = "";
        if (!days) days = 30;
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    get: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    },
    clear: function (name) {
        Cookie.create(name, "", -1);
    }
}


//function updateCache() {
//    var xhttp = new XMLHttpRequest();
//    xhttp.onreadystatechange = function () {
//        if (this.readyState == 4 && this.status == 200) {
//            window.applicationCache.update();
//        }
//    };
//    xhttp.open("GET", "/Home/cache ", true);
//    xhttp.send();
//}
//window.applicationCache.addEventListener('updateready', function (e) {
//    if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
//        window.location.reload();
//    }
//}, false);


//# =================================================================================================================================================

function localStorageSize() {
    var log = [], total = 0, logText = "";
    for (var x in localStorage) {
        var xLen = ((localStorage[x].length * 2 + x.length * 2) / 1024);
        log.push(x + " = " + xLen.toFixed(2) + " KB \n");
        total += xLen
    };

    if (total > 1024) {
        log.push("Total = " + (total / 1024).toFixed(2) + " MB \n");
    } else {
        log.push("Total = " + total.toFixed(2) + " KB \n");
    };

    logText = log.join("\n");
    console.info(logText); // Size
    alert(logText);
}

function localStorageKey(options) {
    if (!options.url)
        keyLocalStorage = undefined;

    else if (options.url.toLowerCase().indexOf("/roomcheckout/payroomprice") > -1) {
        keyLocalStorage = options.type + off_GetHostUrl(options.url) + "?checkin:" + options.data.checkinID;
    }
    else if (options.url.toLowerCase().indexOf("/checkingroup/paypricebyroom") > -1) {
        keyLocalStorage = options.type + off_GetHostUrl(options.url) + "?checkin:" + options.data.checkinID + ";roomid:" + options.data.roomId;
    }
    else if (options.url.toLowerCase().indexOf(".js") > -1 || options.url.toLowerCase().indexOf(".css") > -1) // ko add local Storage các file js
        keyLocalStorage = undefined;
    else if (options.url.toLowerCase().indexOf("otabe.fotel") > -1 || options.url.toLowerCase().indexOf("shop.fotel") > -1) {// ko add local Storage các ở shop, hay ota
        keyLocalStorage = undefined;
    }
    else if (options.type.toLowerCase() == "post" && options.data)
        keyLocalStorage = options.type + off_GetHostUrl(options.url) + "?" + off_SetKeyToUrl(options.data);
    else
        keyLocalStorage = options.type + off_GetHostUrl(options.url);

    return keyLocalStorage ? keyLocalStorage.toLowerCase() : undefined;
}

function off_SetKeyToUrl(dataPost) {
    if (typeof dataPost === 'string') return dataPost;
    var objDataKey = "";
    for (var key in dataPost) {
        if (!dataPost.hasOwnProperty(key)) continue;
        if (key == "draw") continue;
        var dataInData = dataPost[key];
        // kiểm tra date
        if (typeof (dataInData) == "string" && (testDate(dataInData) || testDate2(dataInData))) {
            objDataKey += (key + "=" + "date;");
            continue;
        } else if (Object.prototype.toString.call(dataInData) === "[object Date]") {
            objDataKey += (key + "=" + "date;");
            continue;
        } else if (typeof dataInData === 'object' && dataInData) {
            objDataKey += off_SetKeyToUrl(dataInData);
            continue;
        }
        objDataKey += (key + "=" + dataInData + ";");
    }
    return objDataKey;
}

function off_GetHostUrl(url) {
    var nUrl = url;
    if (url.indexOf('?') > -1) {
        var splUrl = url.split("?");
        var splData = splUrl[1].split("&");
        nUrl = splUrl[0] + "?";
        for (var i = 0; i < splData.length; i++) {
            var d1 = splData[i];
            var splD = d1.split("=");
            nUrl += (splD[0] + "=");
            if (typeof splD[1] === 'string' && (new Date(splD[1]) !== "Invalid Date") && !isNaN(new Date(splD[1]))) {
                nUrl += "date";
            } else {
                nUrl += splD[1];
            }
            nUrl += ";";
        }
    }

    var host = location.protocol + "//" + location.host;
    if (nUrl.indexOf("http") > -1)
        return nUrl;
    return (host + nUrl);
}

// dd/MM/yyyy
function testDate(str) {
    if (!str) return false;
    var t = (str + "").match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
    if (t === null) return false;
    var d = parseInt(t[1]), m = parseInt(t[2], 10), y = parseInt(t[3], 10);
    if (m >= 1 && m <= 12 && d >= 1 && d <= 31) {
        return true;
    }
    return false;
}

// dd/MM/yyyy HH:mm
function testDate2(str) {
    if (!str) return false;
    var t = (str + "").match(/^(\d{2})\/(\d{2})\/(\d{4})\s(\d{2}):(\d{2})$/);
    if (t === null) return false;
    var d = parseInt(t[1]), m = parseInt(t[2], 10), y = parseInt(t[3], 10);
    if (m >= 1 && m <= 12 && d >= 1 && d <= 31) {
        return true;
    }
    return false;
}

function getLocalStorageByUrl(url, dataPostStr) {
    if (url.toLowerCase().indexOf(("/ReservationRoom/DatPhong").toLowerCase()) > -1) {
        var url2 = url.split("?");
        url = url2[0];
    }
    for (var i = 0; i < localStorage.length; i++) {

        var key = localStorage.key(i);
        if (key.length > 0 && key.indexOf(url) > -1) {
            //console.log(key);
            if (dataPostStr && key.indexOf(dataPostStr) > -1) {
                return localStorage.getItem(key);
            }
            return localStorage.getItem(key);
        }
    }
    return null;
}

function deleteLocalStorageByUrl(url) {
    if (!navigator.onLine) return;
    for (var i = 0; i < localStorage.length; i++) {
        var key = localStorage.key(i);
        if (key.length > 0 && key.indexOf(url) > -1) {
            localStorage.removeItem(key);
        }
    }
    return;
}

window.addEventListener("beforeunload", function (event) {
    if (!navigator.onLine) {
        event.returnValue = "Kết nối mạng bị ngắt vui lòng kiểm tra lại";
        return event.returnValue;
    } else {
        //updateCache();
    }
});

// =====================================================================================================================

// =====================================================================================================================
function checkSavePost(url) {
    var objUrlOff = [
       "/RoomCheckOut/InsertService", // thêm mới dịch vụ
       "/RoomCheckOut/InsertNewOtherService", // thêm mới dịch vụ khác
       "/RoomCheckOut/LockCustomerService", // xóa dịch vụ

       "/tbl_CheckInDeposit/update", // thêm mới trả trước
       "/tbl_CheckInDeposit/delete",// xóa dịch vụ trả trước 

       "/RoomCheckOut/SaveCheckOut", // lưu thông tin checkin

       "/ReservationRoom/DatTruoc", // đặt trước
       "/ReservationRoom/CheckIn", // nhận phòng
       "/RoomCheckOut/PaymentCheckOut", // thanh toán 

       "/CheckInGroup/NhanPhong",// nhận phòng đoàn
       "/CheckInGroup/DatTruoc", //đặt trước đoàn
       "/CheckInGroup/PaymentCheckOut", //  thanh toán đoàn
    ];
    var index = -1;
    for (var i = 0; i < objUrlOff.length; i++) {
        if (objUrlOff[i].toLowerCase() == url.toLowerCase()) {
            index = i;
            break;
        }
    }
    return index > -1;
}

//  thằng này là save để khi có mạng post lên  // save những dữ liệu đã post lúc mất mạng vào 1 key
/// thằng này save datapost
function saveDataPostLocalStorage(options, time) {
    if (navigator.onLine) return;
    // url  những url này khi mất mạng được save vào localstorage để khi có mạng post lên
    if (!checkSavePost(options.url)) return;

    //debugger;
    var dataLocal_json = [];
    var dataLocal = getSyncData();
    if (dataLocal)
        dataLocal_json = dataLocal;
    // xử lý dữ liệu đã có, edit dữ liệu, xóa dữ liệu trong dataLocal_json
    if (dataLocal_json.length > 0) {
        // check path == RoomCheckOut/LockCustomerService = check xóa dịch vụ trong local Storage
        if (options.url.toLowerCase() == ("/RoomCheckOut/LockCustomerService").toLowerCase()) {
            var newDataLocal = dataLocal_json.filter(function (x) {
                return !((x.url.toLowerCase() == ("/RoomCheckOut/InsertService").toLowerCase() || x.url.toLowerCase() == ("/RoomCheckOut/InsertNewOtherService").toLowerCase())
                            && x.time == options.data.cussvID);
            });
            setSyncData(newDataLocal);
            return;
        }
            //    // edit trả trước
            //else if (options.url.toLowerCase() == ("/tbl_CheckInDeposit/update").toLowerCase()) {
            //    var obj = options.data.obj;
            //    if (obj.Id != 0)// edit => check edit item trong localStorage
            //    {
            //        var flagtbl_CheckInDeposit = 0;
            //        for (var i in dataLocal_json) {
            //            var x = dataLocal_json[i];
            //            if (x.url.toLowerCase() == ("/tbl_CheckInDeposit/update").toLowerCase() &&
            //                (x.data.obj.Id > 0 && x.data.obj.Id == obj.Id)) { // deposit cũ -> đã update -> update lần nữa// deposit thêm mới khi mất mạng-> update lần nữa
            //                options.data.obj.Id = x.data.obj.Id;
            //                dataLocal_json[i].data = options.data;
            //                flagtbl_CheckInDeposit = 1;
            //                break;
            //            }
            //        }
            //        if (flagtbl_CheckInDeposit == 1) {
            //            setSyncData(dataLocal_json);
            //            return;
            //        }
            //    }
            //}
            // check xóa dịch vụ trong local Storage
        else if (options.url.toLowerCase() == ("/tbl_CheckInDeposit/delete").toLowerCase()) {
            var newDataLocal = dataLocal_json.filter(function (x) {
                return !(x.url.toLowerCase() == ("/tbl_CheckInDeposit/update").toLowerCase() && x.time == options.data.cussvID);
            });
            if (newDataLocal.length != dataLocal_json.length) {
                setSyncData(newDataLocal);
                return;
            }
        }

    }

    // save mới
    var obj = {
        data: options.data,
        url: options.url,
        time: time
    }
    dataLocal_json.push(obj);
    setSyncData(dataLocal_json);
    return;
}

// save những dữ liệu đã post lúc mất mạng vào 1 key
function setSyncData(data) {
    localStorage.setItem("LocalStorageSyncData", JSON.stringify(data));
}

function removeSyncData() {
    localStorage.removeItem("LocalStorageSyncData");
}

// lấy (data đã post lúc mất mạng vào 1 key)  đã lưu  
function getSyncData() {
    var dataLocal = localStorage.getItem("LocalStorageSyncData");
    if (dataLocal && dataLocal != "")
        return JSON.parse(dataLocal);
    return null;
}



// post data lên server khi có mạng
function syncDataLocalStorage() {

    var data = getSyncData();
    if (!data) {
        return false;
    }

    if (typeof jQuery === undefined) return false;
    var newData = data.map(function (obj) {
        var rObj = {};
        rObj.time = obj.time;
        rObj.url = obj.url.toLowerCase();
        rObj.data = JSON.stringify(obj.data);
        return rObj;
    });

    // post dữ liệu đã thao tác lên server 
    $.ajax({
        url: "/Common/syncDataLocalStorage",
        type: 'post',
        cache: false,
        async: true,
        data: { model: newData },
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        beforeSend: function () { showDialogLoading("Đồng bộ dữ liệu..."); },
        complete: function () { hideDialogLoading(); },
        success: function (rs) {
            console.log(rs);
            // clear localStorage
            removeSyncData();
            alert("Đồng bộ dữ liệu thành công", function () {
                hashChangeSodophong();
            });
        },
        error: function (e) {
            alert("Đồng bộ dữ liệu thất bại");
        }
    });
}
// =====================================================================================================================

// =====================================================================================================================
// fakeData  những ajax cần post // tạo data để hoàn thành ajax
function fakeDataLocalStorage(options, time) {
    // online -> ko thao tác
    if (navigator.onLine) return null;
    // có một số url post lên server thì mới cần fake
    if (!checkSavePost(options.url)) return null;
    // obj respon
    var obj = {
        IntStatus: 1,
        Message: "Không có kết nối internet, Thông tin giao dịch đã được lưu tạm trên máy",
        Status: "01"
    };
    // check url tạo fake data
    switch (options.url.toLowerCase()) {

        case ("/roomcheckout/insertservice").toLowerCase():// thêm sản phẩm dịch vụ 
            obj.Data = fake_insertservice(options, time);
            break;

        case ("/roomcheckout/insertnewotherservice").toLowerCase():  // thêm dịch vụ khác
            obj.Data = fake_insertnewotherservice(options, time);
            break;

        case ("/tbl_checkIndeposit/update").toLowerCase():  // thêm mới trả trước  
            obj.Data = fake_tbl_checkIndepositUpdate(options, time);
            break;

        case ("/roomcheckout/payroomprice").toLowerCase():  // fake data call price   
            fakeData = fake_DataCallPrice(options, time);
            return fakeData;
            break;
        case ("/ReservationRoom/DatTruoc").toLowerCase():  // đặt trước   
        case ("/ReservationRoom/CheckIn").toLowerCase():  // nhận phòng   
            fakeData = fake_JsonResult(options, time);
            return fakeData;
            break;
        case ("/CheckInGroup/DatTruoc").toLowerCase():  // đặt trước theo đoàn   
        case ("/CheckInGroup/NhanPhong").toLowerCase():  // nhận phòng theo đoàn   
            fakeData = fake_Result(options, time);
            return fakeData;
            break;
        default:
            obj.Data = options.data;
            break;
    }

    // fake data
    var fakeData = {
        status: 200,
        statusText: "OK",
        response: { text: obj },
        headers: ""
    };
    return fakeData;
}

// dataType json
function fake_JsonResult(options, time) {
    var obj = {
        result: 1,
        mess: "Thao tác thành công"
    }
    var fakeData = {
        status: 200,
        statusText: "OK",
        response: { text: JSON.stringify(obj) },
        headers: ""
    };
    return fakeData;
}

// dataType *
function fake_Result(options, time) {
    var obj = {
        result: 1,
        mess: "Thao tác thành công"
    }
    var fakeData = {
        status: 200,
        statusText: "OK",
        response: { text: obj },
        headers: ""
    };
    return fakeData;
}


function fake_insertservice(options, time) {
    var p = $("#slProductID option:selected").data("price");
    var q = $("#QuantityProduct").val();
    var roomId = $("#Roomid").val();
    var roomName = $("#Roomname").val();
    // thanh toán theo đoàn
    if (location.href.indexOf("/CheckInGroup/CheckOut") > -1) {
        roomId = $("#dllroom").val();
        roomName = $("#dllroom option:selected").text();
        if (roomId <= 0) {
            roomId = $("#dllroom  option:eq(1)").val();
            roomName = $("#dllroom  option:eq(1)").text();
        }
    }
    return {
        CheckinID: $("#CheckInID").val(),
        cussvID: time,

        Quantity: q,
        SalePrice: p,
        SysHotelID: $("#SysHotelID").val(),
        TotalSale: q * p,
        datecreated: "/Date(" + time + ")/",

        productid: $("#slProductID").val(),
        Name: $("#slProductID option:selected").text(),
        roomid: roomId,
        RoomName: roomName,
        status: 1,
    };
}

function fake_insertnewotherservice(options, time) {
    var roomId = $("#Roomid").val();
    var roomName = $("#Roomname").val();
    // thanh toán theo đoàn
    if (location.href.indexOf("/CheckInGroup/CheckOut") > -1) {
        roomId = $("#dllroom").val();
        roomName = $("#dllroom option:selected").text();
        if (roomId <= 0) {
            roomId = $("#dllroom  option:eq(1)").val();
            roomName = $("#dllroom  option:eq(1)").text();
        }
    }
    var p = $("#PriceOther").val();
    var obj = {
        CheckinID: $("#CheckInID").val(),
        cussvID: time,

        Quantity: 1,
        SalePrice: p,
        SysHotelID: $("#SysHotelID").val(),
        TotalSale: p * 1,
        datecreated: "/Date(" + time + ")/",

        productid: 0,
        Name: $("#ServerOther").val(),
        roomid: roomId,
        RoomName: roomName,
        status: 1,
    };
    return obj;
}

function fake_tbl_checkIndepositUpdate(options, time) {
    var d = options.data.obj;
    var obj = {
        time: time,
        Id: d.Id > 0 ? d.Id : time,
        payment_type: d.payment_type,
        note: d.note,
        amount: d.amount,
        checkinid: d.checkinid,
        isAdd: d.Id > 0 ? false : true
    };
    return obj;
}

// fake data callPrice
function fake_DataCallPrice(options, time) {
    var obj = {
        Message: "Không có kết nối internet, Không thể load dữ liệu giá phòng",
        Status: 1
    };
    var fakeData = {
        status: 200,
        statusText: "OK",
        response: { text: obj },
        headers: ""
    };
    //"Date: Wed, 01 Jan 2017 01:01:01 GMT \r\n" +
    //                "X-AspNetMvc-Version: 5.2\r\n" +
    //                "Server: Microsoft-IIS/10.0\r\n" +
    //                "X-AspNet-Version: 4.0\r\n" +
    //                "X-Powered-By: ASP.NET\r\n" +
    //                "Content-Type: application/json; charset=utf-8\r\n" +
    //                "Cache-Control: private\r\n" +
    //                "X-SourceFiles: =\r\n" +
    //                "Content-Length: 0\r\n"


    var dataPost = options.data;
    // ko cos localStorage
    if (localStorage.length <= 0) {
        fakeData.response = { text: obj };
        return fakeData;
    }
    var dataLocalStorage = getLocalStorageByUrl(options.url.toLowerCase(), ("roomid=" + dataPost.roomId + ";checkinid=" + dataPost.checkinID + ";").toLowerCase());
    // ko có dữ liệu về roomPrice trong localStorage
    if (!dataLocalStorage) {
        fakeData.response = { text: obj };
        return fakeData;
    }

    var obbStorage = JSON.parse(dataLocalStorage);
    var response = JSON.parse(obbStorage.response.text);
    var dataPostOld = obbStorage.data
    var dataPost = options.data;

    console.log(dataPost, dataPostOld);
    console.log(response);

    // update thông tin cá nhân
    // update thông tin giảm giá tăng giá
    // update thông tin trả trước giảm trừ
    // update thông tin danh sách dịch vụ
    // update thông tin danh sách tiền phòng

    // tính tổng thanh toán 
    fakeData.headers = obbStorage.headers;
    fakeData.response = { text: JSON.stringify(response) };
    return fakeData;
}

//# =================================================================================================================================================

//# ========================================================= xxxx ===========================================================================
/// thằng này save response
// thằng này là save để khi mất mạng lấy cái mà view
function checkSaveKeyLocalStorage(key) {
    // những thằng ko cần cache
    var lstUrl = ["/Common/getDataLocalStorage", // auto 
        "/Common/getAllResAndCheckin", // auto 
        "/home/notification",
        //"common/updateorderbysdp",
    ];
    var index = -1;
    for (var i = 0; i < lstUrl.length; i++) {
        var url = lstUrl[i].toLowerCase();
        if (key.indexOf(url) > -1) {
            index = i;
            break;
        }
    }
    return index == -1;
}

function autoSaveLocalStorage(options, doneObj) {
    try {
        if (!navigator.onLine) return;
        // save quá nhiều cùng 1 url 
        if (localStorage.length > 0) {
            for (var i = 0; i < localStorage.length; i++) {
                var key = localStorage.key(i);
                // cùng key => xóa key cũ update key mới
                if (key == options.keyLocalStorage) {
                    localStorage.removeItem(key);
                    continue;
                }
                if (key.length <= 0) {
                    continue
                }

                //// chỉ để 1 bản ghi tính giá cho 1 checkin 1 room
                //if (options.keyLocalStorage.indexOf("/roomcheckout/payroomprice") > -1 &&
                //    key.indexOf("/roomcheckout/payroomprice") > -1 &&
                //    options.data) {
                //    var fk = "roomid=" + options.data.roomId + ";checkinid=" + options.data.checkinID + ";";
                //    if (key.indexOf(fk) > -1) {
                //        localStorage.removeItem(key);
                //        continue;
                //    }
                //}
                //// đặt phòng chỉ để 1 bản ghi // ReservationRoom/DatPhong
                //if (options.keyLocalStorage.indexOf("/reservationroom/datphong") > -1 &&
                //    !(options.keyLocalStorage.indexOf("/reservationroom/datphongdetail") > -1) &&
                //    key.indexOf("/reservationroom/datphong") > -1) {
                //    localStorage.removeItem(key);
                //    continue;
                //}
            }
        }
        if (options.keyLocalStorage && checkSaveKeyLocalStorage(options.keyLocalStorage))
            localStorage.setItem(options.keyLocalStorage, JSON.stringify(doneObj));
    } catch (e) {
        console.error(e);
    }
}

//# =================================================================================================================================================
