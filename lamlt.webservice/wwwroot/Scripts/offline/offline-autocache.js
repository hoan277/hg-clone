﻿
//# ============================================== auto post nhưng url cần thiết =========================================

function autoGetLocalStorageData1(url, dataPostStr) {
    if (!navigator.onLine) return false;
    $.ajax({
        type: "POST",
        url: "/Common/getAllResAndCheckin",
        data: {},
        cache: false,
        async: true,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    })
    .then(function (data) {
        if (data.IntStatus == 1 && data.Data.length <= 0) return;
        var objAjax = [];
        for (var i = 0; i < data.Data.length; i++) {
            var item = data.Data[i];
            if (item.Key == "Checkin") {
                var url = "/RoomCheckOut/Index?CheckinID=" + item.Value.id;
                var urlPrice = "/RoomCheckOut/PayRoomPrice";
                // group

                if (item.Value.isGroup > 0) {
                    if (item.Value.getroomdata.length == 0) continue;
                    url = "/CheckInGroup/CheckOut?checkinId=" + item.Value.id;
                    urlPrice = "/CheckInGroup/PayPriceByRoom";
                    for (var i2 = 0; i2 < item.Value.getroomdata.length; i2++) {
                        objAjax.push({
                            type: "POST",
                            url: "/Common/GetCheckInIdByDoan",
                            data: item.Value.getroomdata[i2]
                        });
                    }
                } else {
                    objAjax.push({
                        type: "POST",
                        url: "/Common/GetCheckInIDByRoomID",
                        data: { roomid: item.Value.getroomdata[0].roomid }
                    });
                }
                // view
                objAjax.push({
                    type: "GET",
                    url: url,
                });
                // tính giá
                objAjax.push({
                    type: "POST",
                    url: urlPrice,
                    data: item.Value.dataprice,
                });
                // trả trước 
                objAjax.push({
                    type: "GET",
                    url: "/tbl_CheckInDeposit/List",
                    data: {
                        search: item.Value.id,
                        length: 1000,
                        start: 0
                    }
                });
                //RoomCheckOut/PayRoomPrice

            } else if (item.Key == "Reservation") {

                var url = "/ReservationRoom/DatPhongDetail?id=" + item.Value.id;
                // group
                if (item.Value.isGroup > 0) {
                    url = "/CheckInGroup/ChiTietDoan?id=" + item.Value.id;
                    for (var i2 = 0; i2 < item.Value.getroomdata.length; i2++) {
                        objAjax.push({
                            type: "POST",
                            url: "/Common/GetReservationIDByCustomer",
                            data: item.Value.getroomdata[i2]
                        });
                    }
                } else {
                    objAjax.push({
                        type: "POST",
                        url: "/Common/GetReservationIDByRoomID",
                        data: { roomid: item.Value.getroomdata[0].roomid }
                    });
                }
                objAjax.push({
                    type: "GET",
                    url: url,
                });
            }
        }
        // đặt phòng
        objAjax.push({
            type: "GET",
            url: "/ReservationRoom/DatPhong",
        });
        return objAjax;
    })
    .then(function (objAjax) {
        if (!objAjax || objAjax.length <= 0) return;
        setTimeout(function () {
            if (!navigator.onLine) return false;
            var arr = [];
            // xóa localStorage cũ 
            var urlRoom = ["/RoomCheckOut/Index?CheckinID=",
                            "/CheckInGroup/CheckOut?checkinId=",
                            "/RoomCheckOut/PayRoomPrice",
                            "/CheckInGroup/PayPriceByRoom",
                            "/Common/GetCheckInIdByDoan",
                            "/Common/GetCheckInIDByRoomID",
                            "/tbl_CheckInDeposit/List",
                            "/ReservationRoom/DatPhongDetail",
                            "/CheckInGroup/ChiTietDoan"];
            for (var i = 0; i < urlRoom.length; i++) {
                deleteLocalStorageByUrl(urlRoom[i].toLowerCase());
            }
            for (var i = 0; i < objAjax.length; i++) {
                var op = objAjax[i];
                op.async = true;
                op.timeout = 60000;
                var key = localStorageKey(op);
                var x = $.ajax(op);
                arr.push(x);
            }
            if (!navigator.onLine)
                return false;
            else
                return Promise.all(arr).then(x=> { console.log(x); });
        }, 5000);
    })

}




var autoLocalStorageData = {
    getOne: function () {
        if (!navigator.onLine) return false;
        $.ajax({
            type: "POST",
            url: "/Common/getAllResAndCheckin",
            data: {},
            cache: false,
            async: true,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        })
        .then(function (data) {
            if (data.IntStatus == 1 && data.Data.length <= 0) return;
            var objAjax = [];
            for (var i = 0; i < data.Data.length; i++) {
                var item = data.Data[i];
                if (item.Key == "Checkin") {
                    var url = "/RoomCheckOut/Index?CheckinID=" + item.Value.id;
                    var urlPrice = "/RoomCheckOut/PayRoomPrice";
                    // group

                    if (item.Value.isGroup > 0) {
                        if (item.Value.getroomdata.length == 0) continue;
                        url = "/CheckInGroup/CheckOut?checkinId=" + item.Value.id;
                        urlPrice = "/CheckInGroup/PayPriceByRoom";
                        for (var i2 = 0; i2 < item.Value.getroomdata.length; i2++) {
                            objAjax.push({
                                type: "POST",
                                url: "/Common/GetCheckInIdByDoan",
                                data: item.Value.getroomdata[i2]
                            });
                        }
                    } else {
                        objAjax.push({
                            type: "POST",
                            url: "/Common/GetCheckInIDByRoomID",
                            data: { roomid: item.Value.getroomdata[0].roomid }
                        });
                    }
                    // view
                    objAjax.push({
                        type: "GET",
                        url: url,
                    });
                    // tính giá
                    objAjax.push({
                        type: "POST",
                        url: urlPrice,
                        data: item.Value.dataprice,
                    });
                    // trả trước 
                    objAjax.push({
                        type: "GET",
                        url: "/tbl_CheckInDeposit/List",
                        data: {
                            search: item.Value.id,
                            length: 1000,
                            start: 0
                        }
                    });
                    //RoomCheckOut/PayRoomPrice

                } else if (item.Key == "Reservation") {

                    var url = "/ReservationRoom/DatPhongDetail?id=" + item.Value.id;
                    // group
                    if (item.Value.isGroup > 0) {
                        url = "/CheckInGroup/ChiTietDoan?id=" + item.Value.id;
                        for (var i2 = 0; i2 < item.Value.getroomdata.length; i2++) {
                            objAjax.push({
                                type: "POST",
                                url: "/Common/GetReservationIDByCustomer",
                                data: item.Value.getroomdata[i2]
                            });
                        }
                    } else {
                        objAjax.push({
                            type: "POST",
                            url: "/Common/GetReservationIDByRoomID",
                            data: { roomid: item.Value.getroomdata[0].roomid }
                        });
                    }
                    objAjax.push({
                        type: "GET",
                        url: url,
                    });
                }
            }
            // đặt phòng
            objAjax.push({
                type: "GET",
                url: "/ReservationRoom/DatPhong",
            });

            return objAjax;
        })
        .then(function (objAjax) {
            if (!objAjax || objAjax.length <= 0) return;
            setTimeout(function () {
                if (!navigator.onLine) return false;
                var arr = [];
                // xóa localStorage cũ 
                var urlRoom = ["/RoomCheckOut/Index?CheckinID=",
                                "/CheckInGroup/CheckOut?checkinId=",
                                "/RoomCheckOut/PayRoomPrice",
                                "/CheckInGroup/PayPriceByRoom",
                                "/Common/GetCheckInIdByDoan",
                                "/Common/GetCheckInIDByRoomID",
                                "/tbl_CheckInDeposit/List",
                                "/ReservationRoom/DatPhongDetail",
                                "/CheckInGroup/ChiTietDoan"];
                for (var i = 0; i < urlRoom.length; i++) {
                    deleteLocalStorageByUrl(urlRoom[i].toLowerCase());
                }
                for (var i = 0; i < objAjax.length; i++) {
                    var op = objAjax[i];
                    op.async = true;
                    op.timeout = 60000;
                    var key = localStorageKey(op);
                    var x = $.ajax(op);
                    arr.push(x);
                }
                if (!navigator.onLine)
                    return false;
                else
                    return Promise.all(arr).then(x=> { console.log(x); });
            }, 5000);
        })

    },
    buildAjaxCheckin: function (item) {
        // group 
        if (item.Value.isGroup > 0) {

            // view
            objAjax.push({
                type: "GET",
                url: "/CheckInGroup/CheckOut?checkinId=" + item.Value.id,
            });
            // tính giá
            objAjax.push({
                type: "POST",
                url: "/CheckInGroup/PayPriceByRoom",
                data: item.Value.dataprice,
            });
            if (item.Value.getroomdata.length == 0) return null;
            for (var i2 = 0; i2 < item.Value.getroomdata.length; i2++) {
                objAjax.push({
                    type: "POST",
                    url: "/Common/GetCheckInIdByDoan",
                    data: item.Value.getroomdata[i2]
                });
            }

        } else {

            objAjax.push({
                type: "POST",
                url: "/Common/GetCheckInIDByRoomID",
                data: { roomid: item.Value.getroomdata[0].roomid }
            });
            // view
            objAjax.push({
                type: "POST",
                url: "/RoomCheckOut/Index?CheckinID=" + item.Value.id,
            });
            // tính giá
            objAjax.push({
                type: "POST",
                url: "/RoomCheckOut/PayRoomPrice",
                data: item.Value.dataprice,
            });

        }
        // trả trước 
        objAjax.push({
            type: "GET",
            url: "/tbl_CheckInDeposit/List",
            data: {
                search: item.Value.id,
                length: 1000,
                start: 0
            }
        });
    },
    buildAjaxRes: function (item) {

        // group
        if (item.Value.isGroup > 0) {
            objAjax.push({
                type: "GET",
                url: "/CheckInGroup/ChiTietDoan?id=" + item.Value.id,
            });

            for (var i2 = 0; i2 < item.Value.getroomdata.length; i2++) {
                objAjax.push({
                    type: "POST",
                    url: "/Common/GetReservationIDByCustomer",
                    data: item.Value.getroomdata[i2]
                });
            }
        } else {
            objAjax.push({
                type: "GET",
                url: "/ReservationRoom/DatPhongDetail?id=" + item.Value.id,
            });
            objAjax.push({
                type: "POST",
                url: "/Common/GetReservationIDByRoomID",
                data: { roomid: item.Value.getroomdata[0].roomid }
            });
        }

    },

    alert: function () {
        alert("xx");
    },
    cacheCommon: function () {
        $.ajax({
            type: "POST",
            url: "/Common/getDataCommonLocalStorage",
            cache: false,
            async: true,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        })
       .then(function (data) {
           if (data.IntStatus == 1 && data.Data.length <= 0) return;
           for (var i = 0; i < data.Data.length; i++) {
               var item = data.Data[i];
               autoLocalStorageData.setLocalStorage(item);
           }
       })
    },
    getAll: function () {
        $.ajax({
            type: "POST",
            url: "/Common/getDataLocalStorage",
            cache: false,
            async: true,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        })
        .then(function (data) {
            if (data.IntStatus == 1 && data.Data.length <= 0) return;
            for (var i = 0; i < data.Data.length; i++) {
                var item = data.Data[i];
                autoLocalStorageData.setLocalStorage(item);
            }
        })
    },
    setLocalStorage: function (item) {
        var key = "";
        var dataPost = {};
        // tạo key
        if (item.type.toLowerCase() == "get")
            key = localStorageKey({
                data: {},
                type: item.type,
                url: item.url
            });
        else {
            if (item.data && item.data.length > 0)
                dataPost = JSON.parse(item.data);
            key = localStorageKey({
                data: dataPost,
                type: item.type,
                url: item.url
            });
        }

        var respon = { text: item.respon };
        if (item.responType == "json")
            respon = { text: JSON.parse(item.respon) };
        // tao data
        var obj = {
            data: (item.type.toLowerCase() == "get") ? null : dataPost,
            headers: "",
            response: respon,
            url: item.url,
        }
        // lưu key
        localStorage.setItem(key, JSON.stringify(obj));
    },

    setlocalStorageByData: function (data) {
        if (data.IntStatus == 1 && data.Data.length <= 0) return;
        for (var i = 0; i < data.Data.length; i++) {
            var item = data.Data[i];
            autoLocalStorageData.setLocalStorage(item);
        }
    }

}
setTimeout(function () {
    var isFirstLogin = Cookie.get('isFirstLogin');
    if (isFirstLogin != null && (isFirstLogin == 1 || isFirstLogin == "1")) {
        //autoLocalStorageData.getAll();
        Cookie.create('isFirstLogin', '0', 1);
    }
}, 3000);

// post get all
function autoGetLocalStorageDataAll(url, dataPostStr) {
    if (!navigator.onLine) return false;

    $.ajax({
        type: "POST",
        url: "/Common/getDataLocalStorage",
        data: {},
        cache: false,
        async: true,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    })
    .then(function (data) {
        if (data.IntStatus == 1 && data.Data.length <= 0) return;
        for (var i = 0; i < data.Data.length; i++) {
            var item = data.Data[i];
            var key = "";
            var dataPost = {};
            // tạo key
            if (item.type.toLowerCase() == "get")
                key = localStorageKey({
                    data: {},
                    type: item.type,
                    url: item.url
                });
            else {
                if (item.data && item.data.length > 0)
                    dataPost = JSON.parse(item.data);
                key = localStorageKey({
                    data: dataPost,
                    type: item.type,
                    url: item.url
                });
            }

            var respon = { text: item.respon };
            if (item.responType == "json")
                respon = { text: JSON.parse(item.respon) };
            // tao data
            var obj = {
                data: (item.type.toLowerCase() == "get") ? null : dataPost,
                headers: "",
                response: respon,
                url: item.url,
            }
            // lưu key
            localStorage.setItem(key, JSON.stringify(obj));
        }
        return;
    })
}
//setTimeout(autoGetLocalStorageData, 3000);

//# =================================================================================================================================================

