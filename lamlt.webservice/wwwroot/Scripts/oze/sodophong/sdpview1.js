﻿
var sdp = {
    order: function (value, callBack) {
        var $i = $("#liorder #dropdownSelected");
        var $item = $("#liorder .dropdown-menu").find('[data-value=' + value + ']');
        if ($item.length == 0) return;
        ////
        //$("#liorder .dropdown-menu li").show();
        //$item.hide();
        $i.html($item.text() + ' <span class="caret"></span>');
        //$i.off('click').click(function () {
        //    $item.trigger('click');
        //})
        Sv.Post({
            url: "/Common/UpdateOrderBySDP",
            data: { v: value }
        }, function () {
            if (callBack)
                callBack();
        });
    }
}