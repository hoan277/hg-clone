﻿var open1check = false;
function openSdpBookingSearch() {
    if (!open1check) {
        open1check = true;
        sdpBooking.loadingContent();
        // đếm số phòng 
    } else {
        var idModal = "sdpbookingRp";
        sdpBooking.openModal(idModal, "Danh sách đặt phòng");
    }
}

var sdpBooking = {

    initModal: function (idModal, title) {

        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">' + title + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body pdb0">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '        </div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave_' + idModal + '" class="btn btn-info" > Lọc </button> ';
        html += '            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
    },

    openModal: function (idModal, title, callback) {
        if ($("#" + idModal).length > 0) {
            if ($("#containerSodophong [data-toggle=popover]").length > 0)
                $("#containerSodophong [data-toggle=popover]").popover('hide');
            $("#" + idModal).modal("show");
        } else {
            Sv.RequestStart();
            sdpBooking.initModal(idModal, title);
            $("#" + idModal + " .modal-body-content").html('<p>loading..</p>');
            $.get("/Sodophong/SearchBox", function (rs) {
                $("#" + idModal + " .modal-body-content").html(rs);
                sdpBooking.setupdate(idModal);
                sdpBooking.resetInput(idModal);
                $("#" + idModal + " #btnSave_" + idModal).focus();
                sdpBooking.search(idModal); 
                Sv.RequestEnd();

                $("#" + idModal).modal("show");
            });
        }
    },

    resetInput: function (idModal) {
        Sv.SetDateTime($("#" + idModal + " input[name='FromDate']"), Sv.Set00InDay(new Date()));
        Sv.SetDateTime($("#" + idModal + " input[name='ToDate']"), Sv.Set59InDay(Sv.AddDays(30)));
    },

    setupdate: function (idModal) {
        Sv.SetupDatePicker([{
            e: $("#" + idModal + " input[name='FromDate']"),
            format: "DD/MM/YYYY HH:mm",
        }, {
            e: $("#" + idModal + " input[name='ToDate']"),
            format: "DD/MM/YYYY HH:mm",
        }]);
    },

    loadingContent: function (data, callBack) {
        if (!data) {
            data = {
                fdate: moment(Sv.Set00InDay(new Date())).format("DD/MM/YYYY HH:mm"),
                tdate: moment(Sv.Set59InDay(Sv.AddDays(30))).format("DD/MM/YYYY HH:mm"),
                roomtype: 0,
                roomid: 0,
            }
        }
        Sv.Post({
            url: "/Sodophong/GridBooking",
            data: data
        }, function (rs) {
            $("#containerSodophong").html(rs);
            sdpBooking.roowClick("#containerSodophong");
            sdpBooking.popover($("#containerSodophong"));
            sdpBooking.countRoom($("#containerSodophong table tbody>tr"));

            sdpBooking.selectedStatus(-1);

            sdpBooking.viewStatus();
            sdpBooking.order(4);

            sdpBooking.setRoomMenu("#containerSodophong");

            if (callBack)
                callBack();
        });
    },

    search: function (idModal) {
        $("#" + idModal + " #btnSave_" + idModal).off("click").click(function () {
            $("#containerSodophong").html("loading...<img src='/images/load.gif'/>");
            var data = sdpBooking.getDataForm(idModal);
            sdpBooking.loadingContent(data, function () { 
                $("#" + idModal).modal("hide"); 
            });
        });
    },

    getDataForm: function (idModal) {
        return {
            fdate: $("#" + idModal + " [name=FromDate]").val(),
            tdate: $("#" + idModal + " [name=ToDate]").val(),
            roomtype: $("#" + idModal + " [name=ddlRoomTypeID]").val(),
            roomid: $("#" + idModal + " [name=ddlRoomID]").val(),
        };
    },

    // selected khi click vào 1 row
    roowClick: function (box) {
        $(box + " #grid-sdp").off("click").click(function (e) {
            $(box + " table tr.selected").removeClass("selected");
            $(e.target).closest("tr").addClass("selected");
        });
    },

    // popover
    popover: function ($e) {
        //  var content = ['<div class="popover-content">',
        //'<div class="arrow">',
        //"And here's some amazing content. It's very engaging. Right?",
        //'</div>',
        //'</div>'].join('');
        var timeoutHide = null;
        // $e = $("#containerSodophong")
        $e.popover({
            selector: '[data-toggle=popover]',
            container: "#sdp-grid-view",
            placement: "top",
            html: true,
            trigger: "click"
        });
        //    .on('shown.bs.popover', function (e) {
        //    // auto ẩn sau 10s
        //    var $pop = $(e.target).next();
        //    if (timeoutHide)
        //        clearTimeout(timeoutHide);
        //    timeoutHide = setTimeout(function () {
        //        $pop.popover('hide');
        //    }, 10000); 
        //}).on('hide.bs.popover', function (e) {
        //    // auto ẩn sau 10s
        //    if (timeoutHide)
        //        clearTimeout(timeoutHide);
        //});

        $e.on('click', function (e) {
            var $p = $(e.target).closest(".roombk-item");
            var $p2 = $(e.target).closest(".popover"); // fix khi click bên trong popover
            if ($p.length == 0 && $p2.length == 0)
                $e.find("[data-toggle=popover]").popover('hide');
            else if ($p.length > 0) {
                $e.find("[data-toggle=popover]").not($p).popover('hide');
            }
        });
        $e.find(".grid-sc").scroll(function (e) {
            if ($e.find('div.popover:visible').length) {
                $e.find("[data-toggle=popover]").popover('hide');
            }
        });
    },

    // open menu đặt phòng
    setRoomMenu: function (box) {
        $(box + " #grid-sdp tbody td.headcol").off("click").click(function (e) {
            var $td = $(e.target);
            console.log(e.target);
            //  string.Format("javascript:showMenuSoDoPhong(event,{0},'{1}',{2},{3},{4})", obj.id, obj.Name, (obj.Status ?? 0), (obj.Status1 ?? 0), cusId);
            // cusid == đoàn = customerId, khách lẻ = 0;
            // $td.data('status') bỏ mặt định để trạng thái phòng = 0
            showMenuSoDoPhong(e, $td.data('id'), $td.attr('title'),0, $td.data('status1'), 0);
        });
    },

    countRoom: function (arr) {
        var obj = { all: 0, r0: 0, r1: 0, r4: 0 };
        $.each(arr, function (index, elem) {
            var s = $(elem).attr("sstatus");
            if (s == 0) {
                obj.r0 += 1;
            } else if (s == 1) {
                obj.r1 += 1;
            } else if (s == 4) {
                obj.r4 += 1;
            }
            obj.all += 1;
        });

        $("#liStatus .rs-all .number-all").html(obj.all);
        $("#liStatus .rs-0 .number-all").html(obj.r0);
        $("#liStatus .rs-1 .number-all").html(obj.r1);
        $("#liStatus .rs-4 .number-all").html(obj.r4);
    }, 

    viewStatus: function () {
        var arr = $("#containerSodophong table tbody>tr"); 
        var status = $("#liStatus").find(".status-selected").data("status");
        console.log(status);
        $.each(arr, function (index, obj) {
            // console.log(index, obj, status);
            // $("#search_status").val()=0: tất cả, 1: trống,2: đang ở,4: đặt trước
            var s = $(obj).attr("sstatus");
            if (status== -1 || s == status) {
                $(obj).show();
            } else {
                $(obj).hide();
            } 
        });
    },

    selectedStatus: function (status) {
        $("#liStatus").find(".status-selected").removeClass("status-selected");
        $("#liStatus .sl-room[data-status=" + status + "] ").addClass("status-selected");
    },

    order: function (value, callBack) {
        var $i = $("#liorder #dropdownSelected");
        var $item = $("#liorder .dropdown-menu").find('[data-value=' + value + ']');
        if ($item.length == 0) return;
        //// 
        $i.html($item.text() + ' <span class="caret"></span>');

        Sv.Post({
            url: "/Common/UpdateOrderBySDP",
            data: { v: value }
        }, function () {
            if (callBack)
                callBack();
        });
    },


}
