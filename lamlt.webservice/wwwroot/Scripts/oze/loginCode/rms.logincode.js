﻿function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function postLogin(data) {
    $.ajax({
        url: "/Accounts/LoginCode",
        type: 'Post',
        data: data,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        beforeSend: function () {
            Sv.RequestStart();
        },
        complete: function () {
            Sv.RequestEnd();
        },
        success: function (mgs) {
            if (mgs.Message == "001" || mgs.Message == "002") {
                // save cookie
                createCookie('CookieUserName-xxxx', data.username, 30);
                createCookie('CookieCode-xxxx', data.code, 30);

                createCookie('isFirstLogin', '1', 1);
                if (mgs.Data === "RMS_TELLER" || mgs.Data === "QUANLY")
                    window.location.href = "/Cafe/Dashboard";
                else if (mgs.Data === "RMS_COOK")
                    window.location.href = "/Cafe/Kitchen";
                else if (mgs.Data === "RMS_WAITER")
                    window.location.href = "/Cafe/Kitchen";
                else
                    window.location.href = "/Home/Index";
            } else {
                $(".message-login").show();
                $(".message-login").html(mgs.Message);
            }
        }
    });
}
function setupValidate() {
    var form = $('#login-form').on();
    form.validate({
        rules: {
            passcode: {
                required: true
            }
        },
        messages: {
            passcode: {
                required: 'Bạn cần nhập mã số đăng nhập'
            }
        }
    });
}
$(document).ready(function () {
    // check logout
    setupValidate();
    var url = window.location.href;
    if (url.indexOf('type=logout') < 0) {
        // check  Cookie
        var user = readCookie('CookieUserName-xxxx');
        var pass = readCookie('CookieCode-xxxx');
        if (user == null || user == '') {
            window.location = '/Accounts/Login'
        }
        if (user && pass) {
            var data = {
                username: user,
                code: pass,
                command: "SignInSub"
            };
            postLogin(data);
        }
    } else {
        // xóa cookie
        createCookie('CookieCode-xxxx', '', -1);
    }
    $("#login-form").submit(function (ev) {
        ev.preventDefault();
        if ($(this).valid()) {
            var $f = $("#login-form");
            var data = {
                code: $f.find("[name=passcode]").val(),
                username: readCookie('CookieUserName-xxxx'),
                command: "SignInSub",
                chkRememberMe: true
            };
            postLogin(data);
        }
    });
});
