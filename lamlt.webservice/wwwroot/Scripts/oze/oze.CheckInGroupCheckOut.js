﻿var newLeaderId = 0;
function CtrlCheckOut() {
    var base = this;
    OzeBase.apply(this, arguments);

    // tính giá theo khung giờ
    base.ToPrice = function (e) {

    }

    // init
    base.init = function (o) {
        // default
        Sv.SetupInputMask();
        toggleMenu("menu_letan");

        // change room
        $("#dllroom").off("change").change(function (e) {

            callToPrice();
            // ẩn trả trước và giảm trừ 
            var roomId = $("#dllroom").val();
            if (roomId > 0) {
                $("#Deposit").parents(".form-group").hide();
                $("#Deduction").parents(".form-group").hide();
            } else {
                $("#Deposit").parents(".form-group").show();
                $("#Deduction").parents(".form-group").show();
            }

        });

        // change khung giờ
        $("#dllKhungGio").off("change").change(function (e) {
            callToPrice();
        });

        // thanh toán
        $("#btnPay").off("click").click(function () {
            var roomId = $("#dllroom").val();
            var roomName = $("#dllroom option:selected").text();
            var msg = "Bạn có chắc chắn muốn thanh toán hóa đơn này không";
            if (roomId > 0) {
                msg = "Bạn có chắc chắn muốn thanh toán cho phòng: " + roomName + " không";
            }
            bootbox.confirm(msg, function (result) {
                if (!result) return;
                updateInformation();
                var data = {
                    checkInid: parseInt($("#CheckInID").val()),
                    tdate: $("#Leave_DateCheckOut").val(),
                    khunggio: $("#dllKhungGio").val(),
                    temp: information,
                    roomId: roomId
                }
                Sv.Post({
                    url: "/CheckInGroup/PaymentCheckOut",
                    data: data
                }, function (rs) {
                    if (rs.Status === "01") {
                        bootboxLamlt.alert("Thao tác thành công");
                        if (window.hotelConfig && window.hotelConfig.autoPrintOrder && window.hotelConfig.autoPrintOrder == 1) {
                            printShowAuto(); 
                        } else {
                            hashChangeSodophong();
                        }  
                    } else {
                        bootboxLamlt.alert(rs.Message);
                    }
                });
            });
        });

    }

    $("#btnDetailPrint").off().on('click', function () {
        printShow();
    });


    $("#btnPrint").off("click").on('click', function () {

        htmlPrint();
    });

};

// viết lại phần add dịch vụ - sản phẩm
function addService(e) {
    e.preventDefault();
    var quantity = parseInt($("#QuantityProduct").val());
    var checkinID = parseInt($("#CheckInID").val());
    var hotelID = parseInt($("#SysHotelID").val());
    var customerid = parseInt($("#CustomerId").val());
    var productId = parseInt($("#slProductID").val());
    var roomId = $("#dllroom").val();
    if (roomId < 0) {
        //alert("Không thể thêm dịch vụ khi không chọn phòng sử dụng dịch vụ!");
        //return;
        roomId = $("#dllroom  option:eq(1)").val();
    }
    //$("#addptqgtn").click(function () {
    if (productId === 0) {
        alert('Vui lòng chọn dịch vụ !');
        return;
    }
    if (quantity === 0) {
        alert('Chưa nhập số lượng !');
        return;
    }
    var url = "/RoomCheckOut/InsertService";
    Sv.Post({
        url: url, data: {
            productId: productId,
            checkinID: checkinID,
            hotelID: hotelID,
            customerid: customerid,
            roomId: roomId,
            Quantity: quantity
        }
    }, function (rs) {
        if (rs.Status === "01") {
            $(".listService").append(binHtmlSP(rs.Data));
            $("#QuantityProduct").val(0);
            $("#slProductID").val(0);

            var t = parseFloat($("#txtTotalPrice").val().replace('.', '')) + parseFloat(rs.Data.TotalSale);
            var p = parseFloat($("#txtTotalPay").val().replace('.', '')) + parseFloat(rs.Data.TotalSale);
            bindTotal({ tongtien: t, thanhtoan: p });
            g_Utils.SetAmount();

        } else {
            alert("Thêm mới dịch vụ thất bại");
        }
        setMaxHeight();
    });
}
// viết lại phần add dịch vụ
function addServiceOrther(e) {
    e.preventDefault();
    var name = $("#ServerOther").val();
    var PriceOther = parseFloat($("#PriceOther").val());
    var checkinID = parseInt($("#CheckInID").val());
    var hotelID = parseInt($("#SysHotelID").val());
    var customerid = parseInt($("#CustomerId").val());
    var roomId = $("#dllroom").val();
    if (roomId < 0) {
        //alert("Không thể thêm dịch vụ khi không chọn phòng sử dụng dịch vụ!");
        //return;
        roomId = $("#dllroom  option:eq(1)").val();
    }
    if (name === "") {
        alert('Vui lòng nhập dịch vụ !');
        return;
    }
    if (PriceOther === 0 || PriceOther === "") {
        alert('Chưa nhập giá dịch vụ !');
        return;
    }
    var url = "/RoomCheckOut/InsertNewOtherService";
    Sv.Post({
        url: url,
        data: {
            name: name,
            checkinID: checkinID,
            hotelID: hotelID,
            customerid: customerid,
            roomId: roomId,
            price: PriceOther
        }
    }, function (rs) { 
        if (rs.Status === "01") {
            $(".listServiceOrther").append(bindHtmlDV(rs.Data));
            $("#ServerOther").val("");
            $("#PriceOther").val(0);

            var t = parseFloat($("#txtTotalPrice").val().replace('.', '')) + parseFloat(PriceOther);
            var p = parseFloat($("#txtTotalPay").val().replace('.', '')) + parseFloat(PriceOther);
            bindTotal({ tongtien: t, thanhtoan: p });
            g_Utils.SetAmount();

        } else {
            alert(rs.Message);
            return;
        }
        setMaxHeight();
    });

}
// tính tiền
function callToPrice(e) {
    var roomId = $("#dllroom").val();
    var data = {
        tDate: $("#Leave_DateCheckOut").val(),
        roomId: roomId,
        checkinID: parseInt($("#CheckInID").val()),
        khunggio: $("#dllKhungGio").val(),
        temp: information
    };
    Sv.Post({
        url: "/CheckInGroup/PayPriceByRoom",
        data: data
    }, function (rs) {
        if (rs.Status != 1) {
            bootboxLamlt.alert(rs.Message);
        } else {
            bindThongtinThanhToan(rs);
            // set lai width box tien phong, tien dich vu
            Sv.SetScrollHeight($(".box-item"));
            setMaxHeight();
        }
    });
}

// danh sách khách trong đoàn
function listCustomerInGroup(checkInId, doanId, leaderId) {
    //alert(checkInId + "=" + doanId + "=" + leaderId);
    if (newLeaderId > 0)
        leaderId = newLeaderId;

    $("#modalDetails1").off('show.bs.modal');
    $("#modalDetails1 .modal-body-content").html('<p>loading..</p>');
    $("#modalDetails1").on('show.bs.modal', function () {
        $("#modalDetails1 .modal-title").html("Thông tin khách trong đoàn");
        showDialogLoading();
        $.post("/CustomerManage/GetRoomMate_Doan", { doanId: doanId, customerid: leaderId }, function (rs) {
            hideDialogLoading();
            $("#modalDetails1 .modal-body-content").html(rs);
            $("#modalDetails1 button#btnSave").css("display", "none");
            $("#modalDetails1 button#btnUpdateDetail").css("display", "none");
            $("#modalDetails1 button#btnUndoRoom").css("display", "none");
            $("#modalDetails1 button#btnChangeRoom").css("display", "none");
        });
    });
    $("#modalDetails1").modal("show");

}

function addCustomerInGroup(checkInId, doanId, leaderId) {
    if (newLeaderId > 0)
        leaderId = newLeaderId;

    $("#modalDetails1").off('show.bs.modal');
    $("#modalDetails1").on('show.bs.modal', function () {
        $("#modalDetails1 .modal-title").html("Thêm khách trong đoàn");
        $("#modalDetails1 .modal-body-content").html('<p>loading..</p>');
        $.post("/CustomerArriveManage/AddCustomerGroup", { checkinID: checkInId, doanId: doanId, leaderId: leaderId }, function (rs) {
            $("#modalDetails1 .modal-body-content").html(rs);
            Sv.SetupDatePicker([{ e: $("#DOB"), max: new Date() }]);
            setupAutoCompleteDoan(leaderId);
            $("#modalDetails1 button#btnSave").css("display", "inline-block");
            $("#modalDetails1 button#btnUpdateDetail").css("display", "inline-block");

            if ($("#modalDetails1 #btnSave").length > 0)
                $("#modalDetails1 #btnSave").off("click").click(function () { addRoomMate2(); });
            if ($("#modalDetails1 #btnUpdateDetail").length > 0)
                $("#modalDetails1 #btnUpdateDetail").off("click").click(function () { addRoomMate2(); });

        });
    });

    $("#modalDetails1").modal("show");
}

function addRoomMate2() {
    // thêm khách ở cùng
    if (!$("#formDetail").valid()) return;
    var pdata = getFormData($("#formDetail"));
    pdata.Leader = $('#formDetail #Leader').is(":checked");
    pdata.Payer = $('#formDetail #Payer').is(":checked");
    showDialogLoading();
    $.post("/CustomerArriveManage/AddUsingRoom_Doan", { obj: pdata }, function (rs) {
        hideDialogLoading();
        if (rs.Status === "01") {
            newLeaderId = rs.Data;
            bootbox.alert(rs.Message, function () {
                $("#modalDetails1").modal("hide");
            });

        }
        else {
            alert(rs.Message);
        }
    });
}

function printShow() {
    if (window.hotelConfig.jsPrintFn) {
        window[window.hotelConfig.jsPrintFn]();
    } else {
        $("#modalDetails").off('show.bs.modal');
        $("#modalDetails").on('show.bs.modal', function () {
            $("#modalDetails .modal-body-content").html("");
            updateInformation();
            var data = {
                tDate: $("#Leave_DateCheckOut").val(),
                roomId: $("#dllroom").val(),
                IsboolRoom: $('#ckRoom').is(":checked"),
                IsboolService: $('#ckService').is(":checked"),
                checkinID: parseInt($("#CheckInID").val()),
                khunggio: $("#dllKhungGio").val(),
                temp: information
            };
            Sv.Post({
                url: "/CheckInGroup/ViewPrintPay",
                data: data
            }, function (rs) {
                $("#modalDetails .modal-body-content").html(rs);
                printCtr.load("", getPrintData());
            });
        });
        $("#modalDetails").modal("show");
    }
}


function printShowAuto() {
    if (window.hotelConfig.jsPrintFn) {
        window[window.hotelConfig.jsPrintFn]();
    } else {
        $("#modalDetails .modal-body-content").html('<p>loading..</p>');
        updateInformation();
        var data = {
            tDate: $("#Leave_DateCheckOut").val(),
            roomId: $("#dllroom").val(),
            IsboolRoom: $('#ckRoom').is(":checked"),
            IsboolService: $('#ckService').is(":checked"),
            checkinID: parseInt($("#CheckInID").val()),
            khunggio: $("#dllKhungGio").val(),
            temp: information
        };

        $.post("/CustomerCheckOut/ViewPrintPay", data, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            changePrintOption();
            printCtr.load("", getPrintData());
            setTimeout(function () {
                // in thường - in nhiệt
                if (window.hotelConfig && window.hotelConfig.printType === 1) {
                    htmlPrint2();
                } else {
                    htmlPrint();
                }
                hashChangeSodophong();
            }, 200);
        });
    }
}

function setupAutoCompleteDoan(leaderId) {
    var obj = {
        istext: true,
        onSelect: function (item) {
            var data = (JSON.parse(item.object));
            $('#Name').val(data.Name).change();
            $("#formDetail   [name='Name']").val(data.Name);
            $("#formDetail   [name='CustomerId']").val(data.Id);
            $("#formDetail   [name='Company']").val(data.Company);
            $("#formDetail   [name='CountryId']").val(data.CountryId == null ? "238" : data.CountryId);
            $("#formDetail   [name='Sex']").val(data.Sex ? data.Sex : 1);
            $("#formDetail   [name='Email']").val(data.Email);
            $("#formDetail   [name='IdentifyNumber']").val(data.IdentifyNumber);
            $("#formDetail   [name='Phone']").val(data.Mobile);
            $("#formDetail   [name='Address']").val(data.Address);

            var dob = data.DOB.length > 0 ? moment(new Date(parseInt(data.DOB.slice(6, -2)))).format("DD-MM-YYYY") : "";
            $("#txtDOB").val(dob);

            valueold = data.Name;
        },
        onNotSelect: function () {
            $("#formDetail   [name='CustomerId']").val(0);
        },
        displayField: function (item) {
            var sdt = "", idt = "";
            if (item.Mobile != undefined)
                sdt = " - SĐT: " + item.Mobile;
            if (item.IdentifyNumber != undefined)
                idt = " - CMT: " + item.IdentifyNumber;

            return "KH: " + item.Name + sdt + idt;
        },
        valueField: "Name",
        ajax: {
            url: "/CustomerArriveManage/SelectCustomer",
            preDispatch: function (query) {
                return { search: query, customerold: leaderId };
            }
        }
    }
    Sv.TypeaheadConfig($('#Name'), obj);
}

function cancelCheckinDoan(idRes) {
    bootbox.prompt(
    {
        title: "Nhập lý do hủy:",
        inputType: 'textarea',
        callback: function (result) {
            if (!result) {
                alert("Vui lòng nhập lý do hủy.");
                return;
            }
            $.ajax({
                url: '/CheckInGroup/CancelCheckIn',
                type: "Post",
                datatype: "json",
                data: { id: idRes, "note": result },
                beforeSend: function () { Sv.RequestStart(); },
                complete: function () { Sv.RequestEnd(); },
                success: function (response) {
                    if (response.IntStatus == 1) {
                        bootboxLamlt.alert("Thao tác thành công");
                        hashChangeSodophong();
                    }
                    else {
                        alert("Thao tác không thành công:" + response.Message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Thao tác không thành công:" + thrownError);
                }
            });
        }
    });
}

function changePriceItem(e) {
    var $e = $(e.target);
    var model = getDatachangeprice($e);
    model.dllroom = $("#dllroom").val();
    showDialogLoading();
    $.post("/CheckInGroup/SavePriveLevelCheckOut", model, function (rs) {
        hideDialogLoading();
        //console.log(rs);
        if (rs.Status == 1) {
            bindThongtinThanhToan(rs);
            setMaxHeight();
            g_Utils.SetAmount();
        }
        else {
            bootboxLamlt.alert(rs.Message);
        }
    });
}

// đổi phòng
function doi_phong_doan() {

    Sv.SetupModalPost({
        modalId: "doi_phong_doan",
        title: "Đổi phòng",
        url: "/CheckInGroup/doi_phong_doan",
        data: { checkin: $("#CheckInID").val(), roomselected: $("#dllroom").val() },
        modalclass: "modal-lg"
    }
    , function () { }
    , function () {
        var $m = $("#doi_phong_doan");
        var obj = {
            roomOldId: $m.find("#doi_phong_dllroom").val(),
            roomOldName: $m.find("#doi_phong_dllroom option:selected").text(),
            toRoomId: $m.find("#doi_phong_roomId").val(),
            toRoomName: $m.find("#doi_phong_roomId").data("name"),
            note: $m.find("#doiphong_note").val(),
            checkinid: $m.find("#doi_phong_checkinId").val(),
        }

        if (obj.note == "") {
            alert("Vui lòng nhập lý do đổi phòng!");
            return;
        }
        confirm("Bạn chắc chắn muốn đổi phòng <b>" + obj.roomOldName + "</b> sang phòng <b>" + obj.toRoomName + "</b> không?", function (result) {
            if (result) {
                Sv.AjaxPost({
                    url: "/CheckInGroup/doi_phong_doan_submit",
                    data: obj,
                }, function (rs) {
                    if (rs.IntStatus == 1) {
                        $("#doi_phong_doan").on('hidden.bs.modal', function () {
                            bootboxLamlt.alert(rs.Message);
                            refreshHash();
                        })
                        if (typeof dialogMenuclose == "function")
                            dialogMenuclose();
                        $("#doi_phong_doan").modal("hide");
                    }
                    else {
                        alert(rs.Message);
                    }
                });
            }
        })
    })
}

// chuyển thanh toán
function transferRoom_doan() {
    updateInformation();
    Sv.SetupModalPost({
        modalId: "transfer_room",
        title: "Chuyển thanh toán",
        url: "/CheckInGroup/transfer_room",
        data: { checkin: $("#CheckInID").val(), roomselected: $("#dllroom").val(), temp: information, },
        modalclass: "modal-lg"
    }
   , function () { }
   , function () {
       var $m = $("#transfer_room");

       var obj = {
           roomOldId: $m.find("#transfer_room_dllroom").val(),
           roomOldName: $m.find("#transfer_room_dllroom option:selected").text(),
           toRoomId: $m.find("#transfer_room_roomId").val(),
           toRoomName: $m.find("#transfer_room_roomId").data("name"),
           note: $m.find("#dtransfer_room_note").val(),
           checkinid: $m.find("#transfer_room_checkinId").val(),
           checkincode: $m.find("#transfer_room_bookingCode").val(),
           tranRoom: $m.find("#tranRoomType1").is(":checked"),
           tranSv: $m.find("#tranRoomType2").is(":checked"),
           temp: information
       }

       if (!obj.tranRoom && !obj.tranSv) {
           alert("Vui lòng chọn chuyển thanh toán tiền phòng hoặc chuyển tiền dịch vụ!");
           return false;
       }

       if (obj.tranRoom && !obj.tranSv) {
           alert("Không thể chuyển thanh toán tiền phòng khi phòng đang có tiền dịch vụ chưa thanh toán!");
           return false;
       }
       if (obj.toRoomId <= 0) {
           alert("Vui lòng chọn phòng chuyển thanh toán tới!");
           return false;
       }

       if (obj.note == "") {
           alert("Vui lòng nhập lý do chuyển thanh toán!");
           return false;
       }
       var tt = "tiền dịch vụ";
       if (obj.tranRoom) {
           tt += " và tiền phòng";
       }
       var phong = obj.roomOldId == "-1" ? ("hóa đơn: <b>" + obj.checkincode + "</b>") : ("phòng: <b>" + obj.roomOldName + "</b> trong hóa đơn: <b>" + obj.checkincode + "</b>");

       var titleConfirm = "Bạn chắc chắn muốn chuyển thanh toán " + tt + " của " + phong + " qua phòng: <b>" + obj.toRoomName + "</b> ?";

       confirm(titleConfirm, function (result) {
           if (result) {
               Sv.AjaxPost({
                   url: "/CheckInGroup/transfer_room_submit",
                   data: obj,
               }, function (rsxxx) { 
                   if (rsxxx.IntStatus == 1) {
                       $("#transfer_room").on('hidden.bs.modal', function () {
                           bootboxLamlt.alert(rsxxx.Message);
                           refreshHash();
                           return;
                       }) 
                       $("#transfer_room").modal("hide");
                   }
                   else {
                       alert(rsxxx.Message);
                       return;
                   }
               });               
           } else {
               return;
           }
       })

   })
}

// thanh toán tiền dịch vụ
function payService_doan() {
    updateInformation();
    Sv.SetupModalPost({
        modalId: "payService_room",
        title: "Thanh toán tiền dịch vụ",
        url: "/CheckInGroup/payService_room",
        data: { checkin: $("#CheckInID").val(), roomselected: $("#dllroom").val(), payment_type: $("#dllPaymentType").val(), note: "" },
        modalclass: "modal-lg"
    }, function () { }
    , function () { 
        var $m = $("#payService_room");
        var isProduct = $m.find("#payService_room_product").val();
        if (isProduct == "0") {
            alert("Không có dịch vụ cần thanh toán, vui lòng kiểm tra lại.");
            return;
        }

        var obj = {
            roomId: $m.find("#payService_room_dllroom").val(),
            roomName: $m.find("#payService_room_dllroom option:selected").text(),
           
            note: $m.find("#payService_room_note").val(),
            checkinid: $m.find("#payService_room_checkinId").val(),
            checkinCode: $m.find("#payService_room_bookingCode").val(),
            payment_type: $("#dllPaymentTypeRoom").val(),
        }
        var phong = "hóa đơn <b>" + obj.checkinCode + "</b>";
        if (obj.roomId > 0) {
            phong = "phòng: " + obj.roomName + " trong hóa đơn:<b>" + obj.checkinCode + "</b>";
        }
        var titleConfirm = "Bạn có chắc chắn thanh toán tiền dịch vụ của "+phong+" không?";
        confirm(titleConfirm, function (result) {
            if (result) { 
                Sv.AjaxPost({
                    url: "/CheckInGroup/payService_room_submit",
                    data: obj,
                }, function (rsxxx) {
                    if (rsxxx.IntStatus == 1) {
                        $("#payService_room").on('hidden.bs.modal', function () {
                            bootboxLamlt.alert(rsxxx.Message);
                            refreshHash();
                            return;
                        }) 
                        $("#payService_room").modal("hide");
                    }
                    else {
                        alert(rsxxx.Message);
                        return;
                    }
                });
            } else {
                return;
            }
        })

    });
}