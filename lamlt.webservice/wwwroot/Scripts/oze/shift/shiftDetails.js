﻿function shiftDetails() {
    var self = this;
    this.objName = "oshiftDetails";
    this.idModal = "ShiftDetails";
    this.$table;


    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/Shift/ShiftDetailsList",
                "data": function (d) {
                    //d.FromDate = $("input[name='FromDate']").val();
                    //d.ToDate = $("[name='ToDate']").val(); 
                    d.Status = $("#status").val();
                }
            },
            "columns":
                [
                {
                    "data": null, className: 'smallCol', render: function (data, type, row, infor) {
                        return self.$table.page.info().page + infor.row + 1;
                    }
                },
                // mã
                {
                    "data": "Rel_Code", className: 'hidden-xs', "orderable": "false",
                    render: function (data, type, row, infor) {
                        if (window.hotelConfig.orderNumber && window.hotelConfig.orderNumber == 1) {
                            return getSaleOrderNumber(data);
                        } else {
                            return data;
                        }
                    }
                },
                // loại
               {
                   "data": null,
                   render: function (data, type, row, infor) {
                       return row.Rel_TypeName;
                   }
               },
               // ngày
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        if (row.CreateDate != null) {
                            return moment(new Date(parseInt(row.CreateDate.slice(6, -2)))).format("DD/MM/YYYY HH:mm");
                        }
                        return "";
                    }
                },
                // tiền GD
                 {
                     className: 'text-right',
                     "data": null,
                     render: function (data, type, row, infor) {
                         return convert2Money(row.Rel_TotalAmount.toString());
                     }
                 },
                 // PaymentTypeName
                 {
                     className: 'text-right',
                     "data": "PaymentTypeName",
                 },
                 // thực nhận
                  {
                      className: 'text-right',
                      "data": null,
                      render: function (data, type, row, infor) {
                          return convert2Money(row.TotalAmount.toString());
                      }
                  },
                  // STS
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return row.StatusName;
                    }
                },
                {
                    "data": null, className: 'fixWidth', render: function (data, type, row) {
                        var htmlMenu = "";

                        htmlMenu += ('<div class="input-table" onclick="javascript:' + self.objName + '.editDialog(' + row.Id + ',0)">' +
                                              '<a class="btn btn-primary" title="Thông tin chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                          '</div>');
                        if (row.Status == 0) {
                            htmlMenu += ('<div class="input-table" onclick="javascript:' + self.objName + '.editDialog(' + row.Id + ', 1)">' +
                                              '<a class="btn btn-primary" title="Xác nhận giao dịch" ><i class="fa fa-check-square"></i></a>' +
                                          '</div>');
                        }

                        return htmlMenu;
                    }
                }
                ],
            "initComplete": function (settings, json) {
                hideDialogLoading();
                self.setTableFooter(self.$table, json.relTotalAmount, json.totalAmount);
            },
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload(function (data) {
            self.setTableFooter(self.$table, data.relTotalAmount, data.totalAmount);
        });
    }
    this.setTableFooter = function ($t, v1, v2) { 
        if (v1 != undefined)
            $($t.column(4).footer()).html(convert2Money(v1.toString()));
        else
            $($t.column(4).footer()).html("0");

        if (v2 != undefined)
            $($t.column(6).footer()).html(convert2Money(v2.toString()));
        else
            $($t.column(6).footer()).html("0");
    }



    $("#btnSearch").click(function () {
        self.searchGrid();
    });

    this.editDialog = function (id, isedit) {

        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal + " .modal-title").html('Thông tin giao dịch');
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/Debt/Edit/?id=" + id + "&isEdit=" + isedit, function () {
                if (isedit == 1) {
                    Sv.SetupInputMask();
                    $("#" + self.idModal + " #btnSave" + self.idModal).show().off("click").show().click(function () {
                        if (!$("#formAddDept").valid()) return;
                        var obj = {
                            id: $("#id").val(),
                            note: $("#NoteConfirm").val(),
                            totalAmount: $("#totalAmount").val()
                        };

                        showDialogLoading();
                        $.post("/Debt/Update", obj, function (data) {
                            hideDialogLoading();
                            if (data.IntStatus > 0) {
                                bootboxLamlt.alert(data.Message, function () {
                                    $("#" + self.idModal).modal("hide");
                                    self.searchGrid();
                                });
                            }
                            else {
                                alert("Có lỗi khi xác nhận giao dịch:" + data.Message);
                            }
                        });
                    });
                } else
                    $("#" + self.idModal + " #btnSave" + self.idModal).off("click").hide();

                $("#" + self.idModal).off('show.bs.modal');

            });
        });
        $("#" + self.idModal).modal("show");
    }






    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">Nhập tiền trả trước</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '       </div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.idModal + '" class="btn btn-info" > Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }

}