﻿function ctrlImport() {
    var _this = this;

    var $form = $("#formImport");
    var $main = $("#import-content");
    var $btnImport = $("#btnImport");
    var $btnDownload = $("#btnDownload");
    

    // tải dữ liệu từ excel
    _this.import = function () {
        if ($form.find("input[name='FileUpload']")[0].files[0] == undefined) {
            alert("Vui lòng chọn file!");
            return;
        }
        confirm("Bạn có chắc chắn muốn thiết lập cấu hình phòng, giá, nhà cung cấp, sản phẩm cho khách sạn không?", function (result) {
            console.log("vãi lọ");
            var dataSubmit = new FormData();
            dataSubmit.append("FileUpload", $form.find("input[name='FileUpload']")[0].files[0]);
            Sv.AjaxPostFile({
                url: "/ImportData/UploadfileImport",
                data: dataSubmit,
            }, function (rs) {
                bootboxLamlt.alert(rs.Message);
            }, null);

        });       
    }

    _this.download = function () {

        window.open("/ImportData/Download");
    }
    _this.OpenModalImport = function (data) {
        Sv.InitModal({
            modalId: "modalImport",
            modalclass: "modal-lg",
            title: "Xác nhận dữ liệu"
        });
        var $modal = $("#modalImport");
        var html = "";
        data.filter(function (e) {
            if (e.Key == "html")
                html = e.Value;
            return [];
        });
        $modal.find(".modal-body-content").html(html);
        _this.ClientDatable($('.tableclient'));
        $modal.off('show.bs.modal').modal("show");
    }

    _this.ClientDatable = function ($e) {
        return $e.DataTable({
            paging: false,
            serverSide: false,
            processing: false,
            searching: false,
            ordering: false
        });
    }









    _this.init = function () {
        $btnImport.on("click", _this.import);
        $btnDownload.on("click", _this.download);
        
    }

}