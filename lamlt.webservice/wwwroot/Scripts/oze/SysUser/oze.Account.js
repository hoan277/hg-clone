﻿var Unit = function() {
    var base = this;
    base.SetUpValidate = function() {
        var $form = $("#formChange").on();
        $form.validate({
            rules: {
                currentpw: {
                    required: true
                },
                newpw: {
                    required: true
                },
                cfnewpw : {
                    required: true,
                    equalTo: "#newpw"
                }
            },
            messages: {
                currentpw: {
                    required: "Bạn chưa nhập mật khẩu hiện tại"
                },
                newpw: {
                    required: "Bạn cần nhập mật khẩu mới"
                },
                cfnewpw: {
                    required: "Bạn cần nhập xác nhận mật khẩu mới",
                    equalTo: "Nhập lại mật khẩu không đúng"
                }
            }
        });
    }

    base.ChangePass = function() {
        if ($("#formChange").valid()) {
            var obj = {
                Command: "changpwsub",
                CurrentPass :  $("input[name=currentpw]").val(),
                NewPass : $("input[name=newpw]").val(),
                ReNewPass :  $("input[name=cfnewpw]").val()
            }           
            Sv.AjaxPost({
                Url: "/Accounts/ChangePassword",
                Data: { model: obj }
            }, function (respon) {
                Dialog.Alert(respon.Message, respon.Status === "00" ? Dialog.Error : Dialog.Success, function() {
                    if (respon.DirectLink !== "") {
                        window.location = respon.DirectLink;
                    }
                });
            }, function() {
                Dialog.Alert("Có lỗi trong quá trình xử lý", Dialog.Error);
            });
            
        }
    }

}
$(document).ready(function() {
    var unit = new Unit();
    unit.SetUpValidate();
    $('#btnSave').click(function() {
        unit.ChangePass();
    });
});