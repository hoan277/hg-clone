﻿if (!daysView) daysView = -30;
function travelAgentDiscount() {
    var self = this;

    this.idModal = "travelAgentDiscount";
    this.$table;

    this.init = function () {
        Sv.SetupDatePicker([{ e: $("#formdate"), format: "DD/MM/YYYY HH:mm" }, { e: $("#todate"), format: "DD/MM/YYYY HH:mm" }]);
        self.searchRefresh();
    }

    this.searchRefresh = function () {
        if (!daysView) daysView = -30;
        Sv.SetDateTime($("#formdate"), Sv.Set00InDay(Sv.AddDays(daysView)));
        Sv.SetDateTime($("#todate"), Sv.Set59InDay(new Date()));
        $("#dllTravel").val(-1);
        $("#dllStatus").val(-1);
        $("#orderText").val("");

    }

    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/TravelAgent/DiscountList",
                "data": function (d) {
                    d.search = "";//$("#txtSearch").val()
                    d.columns = null;
                    d.order = null;

                    d.formdate = $("#formdate").val();
                    d.todate = $("#todate").val();
                    d.dllTravel = $("#dllTravel").val();
                    d.dllStatus = $("#dllStatus").val();
                    d.orderText = $("#orderText").val();
                }
            },
            "columns":
                [{
                    "data": null, className: 'smallCol', render: function (data, type, row, infor) {
                        console.log(row);
                        return self.$table.page.info().page + infor.row + 1;
                    }
                },
                { "data": "ordercode" },
                {
                    "data": null,
                    render: function (data, type, row) {
                        var htmlMenu = Sv.DateToString(row.DatePayment, "DD/MM/YYYY HH:mm");
                        return htmlMenu;
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row) {
                        return Sv.NumberToString(row.orderTotalRoomAmount) + "  VND";
                    }
                },
                { "data": "customername" },
                { "data": "TravelName" },
                {
                    "data": "Discount",
                    "className": "text-right",
                    render: function (data, type, row) {
                        var html = "";
                        if (row.Discount && row.Discount > 0) {
                            html = Sv.NumberToString(row.Discount) + " " + (row.DiscountType == 1 ? "%" : "VND");
                        } else {
                            html = Sv.NumberToString(row.cf_discount) + " " + (row.cf_discountType == 1 ? "%" : "VND");
                        }

                        return html;
                    }
                },
                //{
                //    "data": "DiscountTotal",
                //    render: function (data, type, row) {
                //        var html = "";
                //        if (row.Discount && row.Discount > 0) {
                //            html = Sv.NumberToString(row.DiscountTotal) + " VND";
                //        } else {
                //            html = Sv.NumberToString(row.cf_discountTotal) + " VND";
                //        } 
                //        return html;
                //    }
                //},
                { "data": "statusName" },
                {
                    "data": null,
                    render: function (data, type, row) {
                        var htmlMenu = "";
                        if (row.status == 0) {
                            htmlMenu += ('<div class="input-table" onclick="javascript:editDialog(' + row.Id + ')">' +
                                            '<a class="btn btn-primary" title="Xác nhận chiết khấu" ><i class="fa fa-check"></i></a>' +
                                        '</div>');
                            htmlMenu += ('<div class="input-table" onclick="javascript:cancelDialog(' + row.Id + ')">' +
                                            '<a class="btn btn-primary" title="Hủy" ><i class="fa fa-times"></i></a>' +
                                        '</div>');
                        }
                        htmlMenu += ('<div class="input-table" onclick="javascript:viewDialog(' + row.Id + ')">' +
                                        '<a class="btn btn-primary" title="Thông tin chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                    '</div>');
                        return htmlMenu;
                    }
                }]
        });
    }

    this.searchGrid = function () {
        self.$table.ajax.reload();
    }

    this.editDialog = function (id) {
        var htmlBtn = '<button type="button" id="TravelAgentDiscountConfirm" class="btn btn-info" > Xác nhận </button> ';
        htmlBtn += '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';

        Sv.SetupModal({
            modalId: self.idModal,
            modalclass: "modal-lg",
            title: "Chiết khấu cho đại lý",
            url: "/TravelAgent/DiscountDetail/" + id,
            button: htmlBtn
        }, function () {

            $("#travelAgentDiscount #Discount").change(function (e) {
                var type = $("#travelAgentDiscount #DiscountType").val();
                var d = $("#travelAgentDiscount #Discount").val();
                var t = $("#travelAgentDiscount #orderTotalRoomAmount").val();
                if (type == 1 && !(d >= 0 && d <= 100)) {
                    $("#Discount").val(0);
                    self.changeTotalDiscount(0, type, t);
                    alert("Vui lòng nhập chiết khấu chính xác!");
                    return;
                }
                self.changeTotalDiscount(d, type, t);
            }).trigger("change");

            $("#travelAgentDiscount #DiscountType").off().change(function () {
                $("#Discount").val(0);
                var type = $("#travelAgentDiscount #DiscountType").val();
                var t = $("#travelAgentDiscount #orderTotalRoomAmount").val();

                self.changeTotalDiscount(0, type, t);
            });

            $("#TravelAgentDiscountConfirm").off("click").click(function () {
                var $f = $("#formTravelAgentDiscount");
                if (!$f.valid()) return;

                var data = getFormData($("#formTravelAgentDiscount"));
                console.log(data);

                Sv.Post({
                    url: "/TravelAgent/UpdateDiscount",
                    data: { model: data }
                }, function (rs) {
                    if (rs.IntStatus > 0) {
                        $modal.modal("hide"); //$("#" + self.idModal).();
                        bootboxLamlt.alert(rs.Message);
                        self.searchGrid()
                    } else {
                        alert(rs.Message);
                    }
                });
            });

        });
    }

    this.viewDialog = function (id) {
        var htmlBtn = '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        Sv.SetupModal({
            modalId: self.idModal,
            modalclass: "modal-lg",
            title: "Chiết khấu cho đại lý",
            url: "/TravelAgent/DiscountDetail/" + id,
            button: htmlBtn
        }, function () {
            $("#" + self.idModal + " [name=Discount]").prop("disabled", true);
            $("#" + self.idModal + " [name=DiscountType]").prop("disabled", true);
            $("#" + self.idModal + " [name=note]").prop("disabled", true);
        });
    }

    this.cancelDialog = function (id) {
        var htmlBtn = '<button type="button" id="TravelAgentDiscountCancel" class="btn btn-info" > Hủy </button> ';
        htmlBtn += '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';

        Sv.SetupModal({
            modalId: self.idModal,
            modalclass: "modal-lg",
            title: "Chiết khấu cho đại lý",
            url: "/TravelAgent/DiscountDetail/" + id,
            button: htmlBtn
        }, function () {
            $("#" + self.idModal + " [name=Discount]").prop("disabled", true);
            $("#" + self.idModal + " [name=DiscountType]").prop("disabled", true); 


            $("#TravelAgentDiscountCancel").off("click").click(function () {
                var $f = $("#formTravelAgentDiscount");
                if (!$f.valid()) return;

                var data = getFormData($("#formTravelAgentDiscount"));
                Sv.Post({
                    url: "/TravelAgent/CancelDiscount",
                    data: { model: data }
                }, function (rs) {
                    if (rs.IntStatus > 0) {
                        $modal.modal("hide"); //$("#" + self.idModal).();
                        bootboxLamlt.alert(rs.Message);
                        self.searchGrid()
                    } else {
                        alert(rs.Message);
                    }
                });
            });


        });
    }

    this.changeTotalDiscount = function (discount, type, amount) {
        var total = 0;
        //%
        if (type == 1) {
            total = Math.round((discount / 100) * amount);
        }
            // vnd
        else {
            total = discount;
        }

        $("[name=DiscountTotal]").val(total);
    }

}

var ctrl = new travelAgentDiscount();

function editDialog(id) {
    ctrl.editDialog(id);
}

function viewDialog(id) {
    ctrl.viewDialog(id);
}

function cancelDialog(id) {
    ctrl.cancelDialog(id);
}

$(document).ready(function () {
    toggleMenu("menu_letan");
    ctrl.init();
    ctrl.setupGrid();

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            ctrl.searchGrid();
        }
    });
    $("#btnSearch").click(function () {
        ctrl.searchGrid();
    });
    $("#btnRefresh").click(function () {
        ctrl.searchRefresh();
    });

});