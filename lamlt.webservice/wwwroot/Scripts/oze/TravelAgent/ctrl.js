﻿function travelAgent() {
    var self = this;

    this.controller = "TravelAgent";
    this.idModal = "TravelAgent";
    this.$table;

    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/" + self.controller + "/List",
                "data": function (d) {
                    d.search = $("#txtSearch").val();
                    d.columns = null;
                    d.order = null;
                }
            },
            "columns":
                [{
                    "data": null, className: 'smallCol', render: function (data, type, row, infor) {
                        console.log(row);
                        return self.$table.page.info().page + infor.row + 1;
                    }
                },
                { "data": "name" },
                { "data": "email" },
                { "data": "phone" }, 
                {
                    "data": null,
                    "className": "text-right",
                    render: function (data, type, row) {
                        var html = "";
                        html += Sv.NumberToString(row.discount);
                        html += (row.discountType == 0) ? " VND" : " %";                        
                        return html;
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row) {
                        var htmlMenu = "";
                        htmlMenu += ('<div class="input-table" onclick="javascript:editDialog(' + row.Id + ')">' +
                                                '<a class="btn btn-primary" title="Thông tin chi tiết" ><i class="fa fa-pencil-square-o"></i></a>' +
                                            '</div>');
                        return htmlMenu;
                    }
                }]
        });
    }

    this.searchGrid = function () {
        self.$table.ajax.reload();
    }


    this.editDialog = function (id) {
        Sv.SetupModal({
            modalId: self.idModal,
            modalclass: "modal-lg",
            title: "Đại lý",
            url: "/TravelAgent/Edit/" + id,
        }, function () {

        }, function () {
            var $f = $("#form" + self.controller);
            if (!$f.valid()) return;

            var data = getFormData($("#form" + self.controller));
            Sv.Post({
                url: "/TravelAgent/Update",
                data: { model: data }
            }, function (rs) {
                if (rs.IntStatus > 0) {
                    $modal.modal("hide"); //$("#" + self.idModal).();
                    bootboxLamlt.alert(rs.Message);
                    self.searchGrid()
                } else {
                    alert(rs.Message);
                }
            });
        });

    }


}
var ctrl = new travelAgent();

function editDialog(id) {
    ctrl.editDialog(id);
}
$(document).ready(function () {
    toggleMenu("menu_letan");
    ctrl.setupGrid();
    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            ctrl.searchGrid();
        }
    });
    $("#btnSearch").click(function () {
        ctrl.searchGrid();
    });


});