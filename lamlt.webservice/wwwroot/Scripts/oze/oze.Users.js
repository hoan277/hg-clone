﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}
function editDialog(id) {

    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        $("#myModal .modal-body-content").load("/Users/Edit/" + id, function () {
            $("#myModal .btn-header").html("Thông tin người dùng");
            if ($("#SysHotelID2").length > 0 && $("#Prov").length > 0) {
                var chosenKs = Sv.chosenSetup($("#SysHotelID2"), {
                    placeholder_text: "Lựa chọn khách sạn quản lý",
                });

                var chosenTinh = Sv.chosenSetup($("#Prov"), {
                    placeholder_text: "Lựa chọn tỉnh",
                });


                // tỉnh
                $("#Prov").off("change").on("change", function () {
                    changeTinh();
                });

                // nhóm quyền
                $("[name='grouptypeid']").off("change").on("change", function () {
                    showKsKhac();
                });

                showKsKhac();
            }
            var chosenKsChinh = Sv.chosenSetup($("#SysHotelID"), {
                placeholder_text: "Lựa chọn Khách sạn",
                onchange: function (event, value) {
                    //console.log(c,v);
                    if ($("#SysHotelID").val() > 0) {
                        $("#divRight").show();
                        updateValKsKhac($("#SysHotelID").val());
                    } else {
                        $("#divRight").hide();
                    }
                    if ($("#SysHotelID2").length > 0 && $("#Prov").length > 0) {
                        showKsKhac();
                    }
                }
            }); 
            function showKsKhac() {

                var ks = $('#SysHotelID').val();
                var ck = $('input[name=grouptypeid]:checked', '#myModal');
                if (ck.length == 0 || !(ks > 0)) {
                    $("#ksOther").hide();
                    return;
                } else {

                    updateValKsKhac(ks);

                    $("#ksOther").show();
                    return;
                } 
            }

            function updateValKsKhac(id) {

                $("#SysHotelID2 option").prop("disabled", false);

                var vall = $("#SysHotelID2").val();
                if (vall == null || vall.length <= 0) {
                    $("#SysHotelID2").val("");
                    $("#SysHotelID2 option[value='" + id + "']").prop("disabled", true);
                    $("#SysHotelID2").trigger('chosen:updated');
                    return;
                }
                var idexOfI = vall.indexOf(id);

                if (idexOfI > -1) {
                    vall.splice(idexOfI, 1);
                }
                $("#SysHotelID2").val(vall);
                $("#SysHotelID2 option[value='" + id + "']").prop("disabled", true);
                $("#SysHotelID2").trigger('chosen:updated');
            }

            function changeTinh() {

                var v = $("#Prov").val();
                Sv.Post({
                    url: "/Users/ChangeProvince",
                    data: { id: v },
                }, function (data) {
                    if (data.result) {
                        var html = "";
                        for (var i = 0; i < data.result.length; i++) {
                            var item = data.result[i];
                            html += "<option value=\"" + item.Id + "\">" + item.Name + "</option>";
                        }
                        $("#SysHotelID2").empty();
                        $("#SysHotelID2").append(html);
                        $("#SysHotelID2 option[value='" + $("#SysHotelID").val() + "']").prop("disabled", true);
                        $("#SysHotelID2").trigger('chosen:updated');
                    } else {
                        $("#SysHotelID2").empty();
                        $("#SysHotelID2").trigger('chosen:updated');
                    }
                });

            }
        });
        $("#myModal button#btnSavePass").css("display", "none");
        $("#myModal button#btnSave").css("display", "inline");
        $("#btnSave").off("click").click(function () {
            if (!$("#dummyUsers").valid()) return;
            var pdata = getFormData($("#dummyUsers"));
            pdata.ListSysHotelID = $("#SysHotelID2").val();
            showDialogLoading();
            $.post("/Users/update", { obj: pdata }, function (data) {
                hideDialogLoading();
                //closeDlgLoadingData();
                if (data.result > 0) {
                    bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                }
                else {
                    if (data.result == -2) alert("Có lỗi khi tạo người dùng:tên truy cập này này đã tồn tại");
                    else alert("Có lỗi khi tạo người dùng");

                }
            });
        });
    });
    $("#myModal").modal("show");
}
function deleteDialog(id) {
     
}
function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .btn-header").html("Thông tin người dùng");
        $("#myModal .modal-body-content").load("/Users/GetDetail/" + id);
        $("#myModal button#btnSavePass").css("display", "none");
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}

function viewDialogReset(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .btn-header").html("Cấp lại mật khẩu cho nhân viên");
        $("#myModal .modal-body-content").load("/Users/ResetPassForStaff/" + id);
        $("#myModal button#btnSavePass").css("display", "inline-block");
        $("#myModal button#btnSave").css("display", "none");
        var form = $('#formResetPass').on();
        var pass = form.find('#Passwords').val();
        $("#btnSavePass").off("click").click(function () {
            if (!$("#formResetPass").valid()) return;
            showDialogLoading();
            $.post("/Users/ResetPass", { pass: $('#Passwords').val(), userId: $('#userid').val() }, function (data) {
                hideDialogLoading();
               
                if (data.Status === '00') {
                    bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                }
                else {
                    bootboxLamlt.alert("Thao tác không thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                }
            });
        });
    });
    $("#myModal").modal("show");
}

