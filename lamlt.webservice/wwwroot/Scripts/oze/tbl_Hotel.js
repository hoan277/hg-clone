﻿function tbl_Hotel() {
    var self = this;
    this.tableName = "tbl_Hotel";
    this.idModal = "modaltbl_Hotel";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/tbl_Hotel/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo khách sạn:" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.post("/" + self.tableName + "/delete", { id: id }, function (data) {
                            if (data.result > 0) {
                                    bootbox.alert("Thao tác thành công", function () { self.searchGrid(); });
        }
        else {
                                    alert("Có lỗi khi xóa nhà cung cấp:" + data.mess);
        }
        });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    d.search = $("#txtSearch").val();
                    delete d.columns;

                }
            },
            "columns":
                [
				{ 'data': 'Id' }
                , { 'data': 'code' }
                , { 'data': 'name' }
                , { 'data': 'address' }
                , { 'data': 'phone' }
                , {
                    'data': null, render: function (data, type, row) {
                        console.log(row);
                        return Sv.DateToString(row.createdate, "DD/MM/YYYY HH:mm");
                    }
                }
                , {
                    'data': 'status', render: function (data, type, row) {
                        if (row.status == 1) return "Sử dụng";
                        return "Không sử dụng";
                    }
                }
                , { 'data': 'SourceFromName' }
                , {
                    "data": null, render: function (data, type, row) {
                        var htmlMenu =
                          '<div class="edit-delete-table">' +
                             
                              '<div class="input-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.Id + ')">' +
                                         //'<img src="/images/icon/icon-view.png" style=" border: none;" title="Thông tin chi tiết">' +
                                    '<a  class="btn btn-info btn-flat" title="Thông tin chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                              '</div>' +
                              '<div class="input-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:o' + self.tableName + '.editDialog(' + row.Id + ')">' +
                                 '<a  class="btn btn-info btn-flat" title="Chỉnh sửa" ><i class="fa fa-pencil-square-o"></i></a>' +                      
                              '</div>' +   
                              //'<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.Id + ')">' +
                              //    //'<img src="/images/icon/icon-delete.png" style=" border: none;" title="Xóa">' +
                              //'</div>' +                
                              '<div class="input-table" onclick="javascript:o' + self.tableName + '.setDateExpire(' + row.Id + ')">' +
                                    '<a  class="btn btn-info btn-flat" title="Set Date Expire" ><i class="fa fa-calendar"></i></a>' +
                                '</div>' +
                              '<div class="input-table" onclick="javascript:o' + self.tableName + '.viewLog(' + row.Id + ')">' +
                                    '<a  class="btn btn-info btn-flat" title="Thời gian sử dụng gần nhất" ><i class="fa fa-external-link"></i></a>' +
                                '</div>' +
                              '<div class="input-table" onclick="javascript:o' + self.tableName + '.viewUserLog(' + row.Id + ')">' +
                                    '<a  class="btn btn-info btn-flat" title="User thao tác" ><i class="fa fa-file-text-o"></i></a>' +
                                '</div>' +
                              '<div class="input-table" onclick="javascript:o' + self.tableName + '.ticket(' + row.Id + ')">' +
                                    '<a  class="btn btn-info btn-flat" title="CSKH" ><i class="fa fa-comment-o "></i></a>' +
                                '</div>' +
                              '<div class="input-table" onclick="javascript:o' + self.tableName + '.listTicket(' + row.Id + ', \'' + row.name + '\')">' +
                                    '<a  class="btn btn-info btn-flat" title="Lịch sử CSKH" ><i class="fa fa-comments-o "></i></a>' +
                                '</div>'
                        '</div>';
                        return htmlMenu;
                    }
                }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" ><span class="fa fa-check">Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.setDateExpire = function (xid) {
        Sv.SetupModal({
            modalclass: "modal-default",
            modalId: "tbl_Hotel_setDateExpire",
            url: "/tbl_Hotel/setDateExpire",
            data: { id: xid },
            title: "Cập nhật thời gian hết hạn"
        },
         function () {
             Sv.SetupDatePicker([{ e: $("#DateExpire"), format: "DD/MM/YYYY HH:mm" }]);
         },
         function () {  
             var data = Sv.FormGetData($("#formDateExpire"));
             data.id =  xid;
             console.log(data);
                 Sv.AjaxPost({
                     url: "/tbl_Hotel/updateDateExpire",
                     data:data
                 }, function (respon) {
                     if (respon.IntStatus == 1) {
                         bootboxLamlt.alert("Thao tác thành công", function () {
                             $("#tbl_Hotel_setDateExpire").modal("hide");        
                         });
                     }
                     else {
                         bootboxLamlt.alert("Có lỗi:" + respon.Message);
                     }
                 });     
         }); //alert("set date  Expire: " + id);
    }

    this.viewLog = function (xid) {

        Sv.SetupModal({
            modalclass: "modal-lg",
            modalId: "tbl_Hotel_viewlog",
            url: "/tbl_Hotel/viewlog",
            data: { id: xid },
            title: "Thời gian sử dụng khách sạn",
            button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>'
        },
         function () {
             
         },
         function () {
            
         }); 
    }

    this.viewUserLog = function (xid) {

        Sv.SetupModal({
            modalclass: "modal-lg",
            modalId: "tbl_Hotel_viewlog",
            url: "/tbl_Hotel/viewUserLog",
            data: { id: xid },
            title: "Hành động sử dụng khách sạn"    ,
            button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>'
        },
         function () {

         },
         function () {

         });
    }

    this.ticket = function (xid) {
        Sv.SetupModal({
            modalclass: "modal-lg",
            modalId: "new_ticket",
            url: "/tbl_Hotel/new_ticket",
            data: { id: xid },
            title: "Ghi chú chăm sóc khách sạn"
        },
         function () {
           
         },
         function () {
             var data = Sv.FormGetData($("#formTicket"));
                          console.log(data)                                      
             Sv.AjaxPost({
                 url: "/tbl_Hotel/save_ticket",
                 data: { model: data }
             }, function (respon) {
                 if (respon.IntStatus == 1) {
                     bootboxLamlt.alert("Thao tác thành công", function () {
                         $("#new_ticket").modal("hide");
                     });
                 }
                 else {
                     bootboxLamlt.alert("Có lỗi:" + respon.Message);
                 }
             });
         }); 
    }

    this.listTicket = function (xid, name) {

        Sv.SetupModal({
            modalclass: "modal-lg modal-full-width",
            modalId: "tbl_Hotel_lsticket",
            url: "/tbl_Hotel/list_ticket",
            data: { id: xid },
            title: "Ghi chú chăm sóc khách sạn: " + name,
            button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>'
        },
         function () {

         },
         function () {

         });
    }
}