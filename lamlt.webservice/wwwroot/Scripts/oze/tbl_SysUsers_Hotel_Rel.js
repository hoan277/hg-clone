﻿function tbl_SysUsers_Hotel_Rel(userid) {
    var self = this;
    this.userid = userid;
    this.tableName = "tbl_SysUsers_Hotel_Rel";
    this.idModal = "modaltbl_SysUsers_Hotel_Rel";
    this.$table;

    this.indexDialog = function (userid) {
        self.userid = userid;
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Index?userid=" + self.userid);
            /*
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/tbl_SysUsers_Hotel_Rel/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo nhà cung cấp:" + data.mess);
                    }
                });
            });
            */
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/tbl_SysUsers_Hotel_Rel/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootboxLamlt.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo nhà cung cấp:" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }

    this.addHotelToUser = function (hotelid, userid) {
        if (bootbox.confirm("Bạn chắc chắn muốn gán", function (result) {
            if (!result) return;
            var pData = { SysHotelID: hotelid, UserID: userid, GroupID: 3 };
            $.post("/" + self.tableName + "/Update", { obj: pData }, function (data) {
                            if (data.result > 0) {
                                    bootboxLamlt.alert("Thao tác thành công", function () { self.searchGrid(); });
        }
        else {
                                    bootboxLamlt.alert("Có lỗi khi gán:" + data.mess);
        }
        });
        }));
    }
    this.remoteHotelFromUser = function (hotelid, userid) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            var pData = { SysHotelID: hotelid, UserID: userid, GroupID: 3 };
            $.post("/" + self.tableName + "/Delete", { obj: pData }, function (data) {
                            if (data.result > 0) {
                                    bootboxLamlt.alert("Thao tác thành công", function () { self.searchGrid(); });
                            }
                            else {
                                  bootboxLamlt.alert("Có lỗi khi xóa");
                            }
        });
        }));
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.post("/" + self.tableName + "/delete", { id: id }, function (data) {
                            if (data.result > 0) {
                                    bootboxLamlt.alert("Thao tác thành công", function () { self.searchGrid(); });
        }
        else {
                                    alert("Có lỗi khi xóa nhà cung cấp:" + data.mess);
        }
        });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#exampleUserHotel').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "paging": false,
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    d.search = $("#search_UserHotel").val();
                    d.belongto = $("#search_UserHotel_belongto").val();
                    d.userid = self.userid;
                    delete d.columns;

                }
            },
            "columns":
                [{ 'data': 'Id' }
                , { 'data': 'syshotelid_title' }
                //, { 'data': 'userid_title' }
                //, { 'data': null, render: function (data, type, row, infor) { return row.datecreated === null ? '' : moment(new Date(parseInt(row.datecreated.slice(6, -2)))).format('DD-MM-YYYY HH:mm'); } }
                ,{
                    "data": null, render: function (data, type, row) {
                        var htmlMenu = "";
                        var v = $("#search_UserHotel_belongto").val();
                        if (v == 1) {
                            htmlMenu = '<div class="edit-delete-table">' +
                                  '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.remoteHotelFromUser(' + row.Id + ',' + self.userid + ')">' +
                                      '<img src="/images/icon/icon-delete.png" style=" border: none;" title="Xóa">' +
                                  '</div>' +
                              '</div>';
                        } else if (v == 0) {
                            htmlMenu = '<div class="edit-delete-table">' +
                                 '<div class="edit-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:o' + self.tableName + '.addHotelToUser(' + row.Id + ',' + self.userid + ')">' +
                                     '<img src="/images/icon/icon-add.png" style=" border: none;" title="Chỉnh sửa">' +
                                 '</div>'
                            '</div>';
                        }
                        return htmlMenu;
                    }
                }]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">Phân quyền khách sạn</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '       </div>';
        html += '        <div class="modal-footer">';
        html += '             <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}