﻿
var $table;

$(document).ready(function () {
    $("#ddlHotelId").select2();
    $("#ddlUserId").select2();


    $("#btnSearch").click(function () {
        searchGrid();
    });
    $("#btnAdd").click(function () {
        addMenu();
    });
    $("#ddlHotelId").change(function (e) {
        changeHotelId();
    });
    setTimeout(function () { if($("#ddlHotelId").length > 0) changeHotelId(); }, 100);

    showDialogLoading();
    var tableOption = {
        "ajax": {
            "url": "/Setting/MenuRelList",
            "data": function (d) {
                d.search = "";
                d.hotelId = $("#ddlHotelId").val();
                d.userId = $("#ddlUserId").val();
                d.keyword = $("#keyword").val();
            }
        },
        "columns":
            [
                {
                    "data": "Id",
                    render: function (data, type, row, infor) {
                        return $table.page.info().page + infor.row + 1;
                    }
                },
                { "data": "MenuName", },
                { "data": "MenuLink", },
                { "data": "HotelName", },
                {
                    "data": "UserName",
                    render: function (data, type, row, infor) {
                        return row.UserName == "" ? row.UserFullName : row.UserName + "-" + row.UserFullName;
                    }
                },
                {
                    "data": "Status",
                    render: function (data, type, row, infor) {
                        return data == 1 ? "Hoạt động" : "Khóa";
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        //var htmlMenu = ('<div class="input-table" onclick="javascript:viewMenu(' + row.Id + ')">' +
                        //            '<a  class="btn btn-info btn-flat" title="Chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                        //        '</div>');
                        var htmlMenu = ('<div class="input-table" onclick="javascript:updateRel(' + row.MenuId + ',' + row.HotelId + ',' + row.UserId + ')">' +
                                    '<a  class="btn btn-primary btn-flat" title="Thao tác" ><i class="fa fa-edit"></i></a>' +
                                '</div>');
                        //htmlMenu += ('<div class="input-table" onclick="javascript:removeMenu(' + row.Id + ')">' +
                        //            '<a  class="btn btn-danger btn-flat" title="Xóa" ><i class="fa fa-times"></i></a>' +
                        //        '</div>');
                        return htmlMenu;
                    }
                },
            ],
        "initComplete": function (settings, json) { hideDialogLoading(); },
    };
    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));
});

function changeHotelId() {
    var id = $("#ddlHotelId").val()
    Sv.Post({ url: "/Setting/getUserInHotel", data: { hotelId: id } }, function (rs) {
        var html = getUserHtml(rs);
        $("#ddlUserId").empty();
        $("#ddlUserId").append(html);
    });
    searchGrid();
}

function getUserHtml(data) {
    var html = "<option value=\"0\" >Người dùng</option>";
    if (data.length > 0) {
        $.each(data, function (i, e) {
            html += "<option value=\"" + e.Id + "\" >" + (e.UserName + "-" + e.FullName) + "</option>";
        });
    }
    return html;
}

function updateRel(MenuId, HotelId, UserId) {
    var modelId = "editMenuRelModalId";
    Sv.SetupModal({
        modalId: modelId,
        title: "Phân quyền menu",
        url: "/Setting/EditMenuRel",
        data: { MenuId, HotelId, UserId },
        modalclass: "modal-lg",
    }, function () {
         
    }, function () {
        var data = [];
        $("#" + modelId + " .table tbody>tr").each(function (i, e) {
            var obj = {
                MenuId: $(e).find(".menu_id").val(),
                UserId: $(e).find(".user_id").val(),
                Id: $(e).find(".config_id").val(),
                HotelId: $(e).find(".hotel_id").val(),
                Status: $(e).find(".menu_status").val(),
            };
            data.push(obj);
        });
        confirm("Bạn chắc chắn xác nhận phân quyền cho menu này?", function (c) {
            if (!c)
                return;
            Sv.Post({ url: "/Setting/UpdateMenuRel", data: { obj: data } }, function (rs) {
                if (rs.IntStatus > 0) {
                    $("#" + modelId).modal("hide");
                    bootboxLamlt.alert(rs.Message);
                    searchGrid();
                } else {
                    alert(rs.Message);
                }
            });
        })
    }, function () {
        $(".menu_status1").change(function (e) { 
            var s = $(this).val();
            var u = $(this).siblings(".user_id").val();
            if (u == "-1") {
                // look
                if (s == 0) {
                    $(".menu_status").not(".menu_status1").val(0).prop('disabled', true);
                }
                    //un look
                else {
                    $(".menu_status").not(".menu_status1").val(1).prop('disabled', false);
                }

            }
        });
    });

} 

function searchGrid() {
    $table.ajax.reload(function (data) { });
}

function addMenu() {
    alert("Thêm mới menu. thêm trong db đi :D ");
}