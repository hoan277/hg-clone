﻿var g_Reservation;
function Ctrl() {
    var base = this;
    OzeBase.apply(this, arguments);


    base.ListRomData = []; // data room đặt phong
    base.ListRomTemp = [];  // data room đặt phong temp
    base.GridRoom = {};  // table grid
    base.ListCustomerData = [];  // data danh sách KH
    base.GridCustomer = {};  // table grid
    base.Price = {};  // table grid

    // setup modal
    base.ModalInit = function (option, optionAjax) {
        var $e = $("#myModalTemp");
        if (option.e != undefined)
            $e = option.e;

        $e.find(".modal-dialog").removeClass("modal-lg").removeClass("modal-md").addClass(option.class);

        $e.find(".modal-title").html(option.title);
        $e.off('show.bs.modal').on('show.bs.modal', function (e) {
            Sv.Post({
                url: optionAjax.url,
                data: optionAjax.data,
                beforeSend: function () {
                    $e.find(".modal-body").html('<p>loading..</p>');
                    Sv.RequestStart();
                }
            }, function (data) {
                if (typeof optionAjax.callback == "function") {
                    optionAjax.callback(data);
                } else {
                    $e.find(".modal-body").html(data);
                }
            });
        })
        if (option.modal != undefined) {
            $e.modal(option.modal);
        } else {
            $e.modal("show");
        }
        //$e.modal("show");
    }


    //================================== lưu khách hàng ===============================================================================
    // ẩn 1 element
    base.hideElemetn = function ($e) {
        if ($e.length > 0) {
            $e.hide();
        }
    };
    // check and uncheck
    base.checked = function ($e, check) {
        if (check)
            $e.iCheck('check');
        else
            $e.iCheck('uncheck');
        $e.prop("checked", check);
    }
    // kiểm tra trưởng đoàn - ngươi thanh toán
    base.checkedCustomer = function (data) {
        var item = data.filter(function (obj) {
            return (obj.Leader == 1)
        });
        if (item.length > 0) {
            base.checked($('#chkLeader'), false);
        } else {
            base.checked($('#chkLeader'), true);
        }
        var itemPayer = data.filter(function (obj) {
            return (obj.Payer == 1)
        });
        if (itemPayer.length > 0) {
            base.checked($('#chkPayer'), false);
        } else {
            base.checked($('#chkPayer'), true);
        }
    }

    // load dux lieu form cusstomer
    base.BindDataFormCustomer = function (data) {
        var $p = $("#formReservation");
        $p.validate().resetForm();

        if (data == null) {
            data = {};
        }

        $p.find('#txtname').val(data.FullName);
        $p.find('#txtDOB').val(data.DOB);
        $p.find('#txtIdentify').val(data.IdentityNumber);
        var ct = 238;
        if (data.CitizenshipCode != null)
            ct = data.CitizenshipCode;
        $p.find('#dllQuocTich').val(ct);
        $p.find('#txtaddress').val(data.Address);
        $p.find('#txtEmail').val(data.Email);
        $p.find('#txtMobile').val(data.Mobile);
        $p.find('#txtCompany').val(data.Company);

        var cbLeader = false;
        if (data.Leader === 1) {
            base.checked($p.find('#chkLeader'), true);
        } else {
            base.checked($p.find('#chkLeader'), false);
        }

        var cbPayer = false;
        if (data.Payer === 1) {
            base.checked($p.find('#chkPayer'), true);
        } else {
            base.checked($p.find('#chkPayer'), false);
        }

    }
    // setup grid
    base.GridCustomerLoad = function (data) {
        if (data != undefined)
            base.ListCustomerData = data;
        var tableOption = {
            data: base.ListCustomerData,
            serverSide: false,
            info: false,
            columns:
                [
                    {
                        "data": null,// STT
                        render: function (data, type, row, infor) {
                            if (base.GridCustomer.length > 0) {
                                return base.GridCustomer.page.info().page + infor.row + 1;
                            }
                            else
                                return infor.row + 1;
                        }
                    },
                    {
                        "data": "Leader", // truong doan
                        render: function (data, type, row, infor) {
                            if (data === 1)
                                return "x";// return "Trưởng đoàn";
                            return "";
                        }
                    },
                    {
                        "data": "Payer",// nguoi thanh toan
                        render: function (data, type, row, infor) {
                            if (data === 1)
                                return "x";// return "Người thanh toán";
                            return "";
                        }
                    },
                    {
                        "data": "FullName", className: 'hidden-xs',// ten khach hang
                    },

                    //{
                    //    "data": "Mobile", className: 'hidden-xs', // SDT
                    //},

                    //{
                    //    "data": "RoomName", // phong
                    //},
                    // thao tác
                    {
                        "data": null,
                        render: function (data, type, row, infor) {
                            var htmlMenu =
                                            //'<div class="input-table"  onclick="javascript:viewCustomer(' + row.ID + ')">' +
                                            //    '<img src="/images/icon/icon-edit.png" title="Chi tiết">' +
                                            //'</div>'+
                                            '<div class="input-table input-table2"  onclick="javascript:ctrl.removeCustomer(' + row.ID + ')">' +
                                                '<img src="/images/icon/icon-delete.png" title="Xóa">' +
                                            '</div>';
                            return htmlMenu;
                        }
                    }
                ]
        };
        base.GridCustomer = Sv.SetUpDataTable($('#gridCustomer'), tableOption);
        return base.GridCustomer;
    }
    // validate thông tin khách hàng khi thêm khách hàng trong đoàn
    base.validCustomer = function () {
        var $p = $("#formReservation");
        var ck = $p.validate().element('#txtname'); //&& $p.validate().element('#dpDOB');
        var cki = $p.validate().element('#txtIdentify'); //&& $p.validate().element('#dpDOB');
        if (ck == true && cki == true) {
            var cLeader = Object.keys(base.ListCustomerData).filter(function (k) {
                return base.ListCustomerData[k].Leader == 1
            });
            var cbLeader = $p.find('#chkLeader').is(":checked");
            var cbPayer = $p.find('#chkPayer').is(":checked");
            if (cLeader.length >= 1 && cbLeader) {
                bootboxLamlt.alert("Vui lòng lựa chọn một trưởng đoàn!");
                return false;
            }
            var ckPayer = Object.keys(base.ListCustomerData).filter(function (k) {
                return base.ListCustomerData[k].Payer == 1
            });
            if (ckPayer.length >= 1 && cbPayer) {
                bootboxLamlt.alert("Vui lòng lựa chọn một người thanh toán!");
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }

    // ===================================== chọn phòng =============================================================================
    // setup grid
    base.GridRoomLoad = function (data) {
        if (data != undefined)
            base.ListRomData = data;
        var rv = base.ListRomData.sort();
        var tableOption = {
            data: rv,
            serverSide: false,
            //paging: false,
            "bPaginate": false,
            info: false,
            columns:
                [
                    {
                        "data": null,// STT
                        render: function (data, type, row, infor) {
                            if (base.GridRoom.length > 0) {
                                return base.GridRoom.page.info().page + infor.row + 1;
                            }
                            else
                                return infor.row + 1;
                        }
                    },
                    {
                        "data": "tbl_Room_TypeName", // name
                    },
                    {
                        "data": "Name", // name
                    },
                    // thao tác
                    {
                        "data": null,
                        render: function (data, type, row, infor) {
                            var htmlMenu =
                                    //'<div class="input-table"  onclick="javascript:viewRoom(' + row + ')">' +
                                    //      '<img src="/images/icon/icon-edit.png" title="Chi tiết">' +
                                    //'</div>' +
                                    '<div class="input-table input-table2"  onclick="javascript:ctrl.removeRoom(' + row.Id + ')">' +
                                        '<img src="/images/icon/icon-delete.png" title="Xóa">' +
                                    '</div>';
                            return htmlMenu;
                        }
                    }
                ]
        };
        base.GridRoom = Sv.SetUpDataTable($('#gridRoom'), tableOption);
        return base.GridRoom;
    }

    this.removeRoom = function (id) {
        // base.ListRomData
        var item = base.ListRomData.filter(function (obj) {
            return (obj.Id == id)
        });
        if (item.length > 0) {
            var index = base.ListRomData.indexOf(item[0]);
            base.ListRomData.splice(index, 1);
            base.DataTableLoad(base.GridRoom, base.ListRomData);
            bootboxLamlt.alert("Thao tác thành công");
            // load giá
            base.LoadPrice();
        } else {
            bootboxLamlt.alert("Không có dữ liệu xóa!");
        }
    }

    this.removeCustomer = function (id) {
        var item = base.ListCustomerData.filter(function (obj) {
            return (obj.ID == id)
        });
        if (item.length > 0) {
            // remove item
            var index = base.ListCustomerData.indexOf(item[0]);
            base.ListCustomerData.splice(index, 1);
            //base.ListCustomerData.splice(item[0], 1);
            // checked người thanh toán - trưởng doánd
            base.checkedCustomer(base.ListCustomerData);
            // load grid
            base.DataTableLoad(base.GridCustomer, base.ListCustomerData);
            //alert
            bootboxLamlt.alert("Thao tác thành công");
        } else {
            bootboxLamlt.alert("Không có dữ liệu xóa!");
        }
    }

    base.newRomTemp = function (data) {
        var obj = [];
        for (var i = 0; i < data.length; i++) {
            obj.push({
                Id: data[i].Id,
                Name: data[i].Name,
                RoomType_ID: data[i].RoomType_ID,
                tbl_Room_TypeName: data[i].tbl_Room_TypeName
            });
        }
        return obj;
    }

    // chọn, bỏ chọn phòng
    base.selectRoom = function (id, name, type, typeName) {
        var $e = $("#Rome_steam").find("[data-room='" + id + "']");
        var obj = { Id: id, Name: name, RoomType_ID: type, tbl_Room_TypeName: typeName };
        // bỏ chọn
        if ($e.hasClass("room-selected")) {
            base.objectRemove(base.ListRomTemp, obj, "Id");
            $e.removeClass("room-selected");
        }
            // chọn
        else {
            base.ListRomTemp.push(obj);
            $e.addClass("room-selected");
        }
    }
    // set room selected
    base.setRoomSelected = function () {
        $("#Rome_steam").find("room-selected").removeClass("room-selected");
        if (base.ListRomData.length > 0) {
            for (var i = 0; i < base.ListRomData.length; i++) {
                var item = base.ListRomData[i];
                var $e = $("#Rome_steam").find("[data-room='" + item.Id + "']");
                if ($e.length > 0)
                    $e.addClass("room-selected");
            }
        }
    }

    // click lưu - kiểm tra phòng - chọn phòng
    base.SaveSelectedRoom = function (e) {
        e.preventDefault();


        base.ListRomData = base.newRomTemp(base.ListRomTemp);
        base.DataTableLoad(base.GridRoom, base.ListRomData);
        // load giá
        base.LoadPrice();
        
        base.changeStatus();
        $("#myModalTemp").modal("hide");


    }

    base.changeRoomType = function (e) {
        var type = $("#dllRoomTypeID").val();
        if (type > 0) {
            // clear room
            var items = base.ListRomData.filter(function (obj) { return (obj.RoomType_ID == type); });
            base.ListRomData = items;
            base.DataTableLoad(base.GridRoom, base.ListRomData);
        }
        base.LoadPrice();
    }

    base.LoadPrice = function (e) {
        Sv.Post({
            url: "/Common/GetPriceByDoan",
            data: { datetime: $('#txtFromDate').val(), roomtypeid: $("#dllRoomTypeID").val(), room: base.ListRomData },
        }, function (data) {
            base.Price = data;
            base.changeGia();
        });
    }

    base.changeGia = function () {
        var data = base.Price;
        var $v = $("#txtPrice");
        var $lab = $("#lbl_GiaPhong");
        var khung = $("#dllPriceKhungGio").val();
        $("#lblPolicyPrice").val(data.title)
        if (data.type == -1)
            return;
        // giá giờ
        if (khung == 0 || khung == -1) {
            $lab.html("Giá phòng/Giờ");
            $v.val(data.PriceHours);
        }
            // giá ngày
        else if (khung == 1) {
            $lab.html("Giá phòng/Ngày");
            $v.val(data.PriceDay);
        }
            // giá đêm
        else if (khung == 2) {
            $lab.html("Giá phòng/Đêm");
            $v.val(data.PriceNight);
        }
            // giá tháng
        else if (khung == 3) {
            $lab.html("Giá phòng/Tháng");
            $v.val(data.PriceMonth);
        }
    }

    // =================================== abc
    // validate thôngtin khách hàng khi nhận phòng, book phòng
    // validate thông tin phòng nhận phòng, book phòng
    base.validAdd = function () {


        if (base.ListCustomerData.length == 0) {
            base.btnAddCustomerToGrid();
            if (base.ListCustomerData.length == 0) {
                return false;
            }
        }
        // phải chọn hạng phòng, hoặc chọn phòng trước khi đặt trước
        // phải chọn phòng khi nhận phòng
        //if (base.ListRomData.length == 0) {

        //    return false;
        //}
        var ckLeader = Object.keys(base.ListCustomerData).filter(function (k) {
            return (base.ListCustomerData[k].Leader == 1)
        });
        if (ckLeader.length != 1) {
            bootboxLamlt.alert("Vui lòng lựa chọn một trưởng đoàn!");
            return false;
        }
        var ckPayer = Object.keys(base.ListCustomerData).filter(function (k) {
            return base.ListCustomerData[k].Payer == 1
        });
        if (ckPayer.length != 1) {
            bootboxLamlt.alert("Vui lòng lựa chọn một người thanh toán!");
            return false;
        }
        return true;
    }
    // get thông tin 
    base.getFormData = function () {
        var resroom = {
            customer: base.ListCustomerData,
            room: base.ListRomData,

            'action': $('[name="action"]').val(),

            'RoomTypeID': $('#dllRoomTypeID').val(),//hạng phòng

            'ID': $("#hdReservationID").val(), // ReservationID
            'ReservationCode': $('#txtResCode').val(),//mã đặt phòng

            'ArrivalDate': $('#txtFromDate').val(),//thời gian đến (booking)
            'LeaveDate': $('#txtToDate').val(),//thời gian đi (booking)              

            'KhungGio': $('#dllPriceKhungGio').val(),//giá phòng theo khung (tính theo giờ, giá cả ngày, giá đêm)
            'Price': $('#txtPrice').val().replace(',', ''),//giá phòng (tính theo loại đặt phòng)

            'Payment_Type_ID': $('#dllPaymentType').val(),//ID phương thức thanh toán
            'Deposit': $('#txtDeposit').val().replace(',', ''),//tiền cọc
            'Deduction': $('#txtDeduction').val().replace(',', ''),//giảm trừ
            'Discount': $('#txtDiscount').val().replace(',', ''),//giảm giá
            'DiscountType': $('#dllDiscountType').val(),//giảm giá
            'StatusReservation': $('#dllStatus').val(),//trạng thái
            'Note': $('#txtNote').val(),//ghi chú 
            'TravelId': $("#dllTravel").val(),
        }
        return resroom;
    }

    // button thêm khách hàng trong đòan
    base.btnAddCustomerToGrid = function (e) {
        if (e != undefined)
            e.preventDefault();
        var $p = $("#formReservation");
        var cbLeader = $p.find('#chkLeader').is(":checked");
        var cbPayer = $p.find('#chkPayer').is(":checked");
        //var rbSex = $p.find('#rbSex input[name=r2]:checked').val();
        var id = 1;
        var maxId = base.maxObj(base.ListCustomerData, "ID");
        if (id > 0) {
            id = maxId + 1;
        }

        // validate
        if (base.validCustomer() == false)
            return false;
        // 1 người thanh toán
        // 1 người trưởng đoàn
        // validate name

        var cust = {
            //THÔNG TIN KHÁCH HÀNG
            'ID': id,
            'FullName': $p.find('#txtname').val(), //Tên đầy đủ
            //'Sex': rbSex, //giới tính
            'DOB': $p.find('#txtDOB').val(), //ngày tháng năm sinh dd/mm/yyyy
            'IdentityNumber': $p.find('#txtIdentify').val(), // CMND/Hộ chiếu ....
            'CountryID': $p.find('#dllQuocTich').val(), // Quốc tịch
            'CitizenshipCode': $p.find('#dllQuocTich').val(), // Quốc tịch
            'Address': $p.find('#txtaddress').val(), //địa chỉ
            'Email': $p.find('#txtEmail').val(), //email
            'Mobile': $p.find('#txtMobile').val(), //mobile
            'Company': $p.find('#txtCompany').val(), //Company
            'RoomID': 0, //ID phòng
            'RoomName': "",
            'Leader': cbLeader == true ? 1 : 0, //là Trưởng đoàn
            'Payer': cbPayer == true ? 1 : 0 //là người thanh toán
        };
        base.ListCustomerData.push(cust);
        // clear data
        base.BindDataFormCustomer(null);
        base.checkedCustomer(base.ListCustomerData);
        // draw table
        base.DataTableLoad(base.GridCustomer, base.ListCustomerData);
    }
    // kiểm tra
    base.KTPhong = function (e) {
        e.preventDefault();
        var objData = { dtFrom: $("#txtFromDate").val(), dtTo: $("#txtToDate").val(), roomType: $("#dllRoomTypeID").val() };
        var modalOpen = function (data) {
            if (data.Status == "0") {
                $("#myModalTemp").find(".modal-body").html(data.Message);
                $("#myModalTemp #btnSave").off("click").hide();
            } else {
                $("#myModalTemp").find(".modal-body").html(data);
                // set selected
                base.ListRomTemp = [];
                base.ListRomTemp = base.newRomTemp(base.ListRomData);

                base.setRoomSelected();

                $("#myModalTemp #btnSave").show().off("click").on("click", base.SaveSelectedRoom);
            }
        }
        var option = { 'title': "Chọn phòng", 'class': "modal-md", 'modal': { backdrop: 'static' } };
        var optionAjax = { 'url': "/CheckInGroup/checkRoom", 'data': objData, callback: modalOpen };
        base.ModalInit(option, optionAjax);

    };
    // nhận phòng
    base.Save = function (e) {
        e.preventDefault();
        var ckValid = base.validAdd();
        if (ckValid == true) {
            var resroom = base.getFormData();
            var title = "Bạn có chắc chắn muốn nhận phòng?";
            bootbox.confirm(title, function (result) {
                if (!result) return;
                var url = "/CheckInGroup/NhanPhong";

                Sv.Post({ url: url, data: { model: resroom } },
                    function (response) {
                        if (response.result >= 1) {
                            bootboxLamlt.alert("Thao tác thành công")

                            if (window.islockCard && window.islockCard == 1) { 
                                var url = "/Common/LockCard_Doan?checkinid=" + response.checkinId;
                                new LockCardControl().showModal(url, function () { hashChangeSodophong(); });
                            } else {
                                hashChangeSodophong();
                            }  
                        }
                        else {
                            bootboxLamlt.alert("Thao tác không thành công:" + response.mess);
                        }

                    });
            });
        }
    };
    // Đặt trước - lưu
    base.Book = function (e) {
        e.preventDefault();
        var ckValid = base.validAdd();
        if (ckValid == true) {
            var resroom = base.getFormData();
            var title = "Bạn có chắc chắn muốn đặt trước?";
            if (resroom.action == "edit") {
                title = "Bạn có chắc chắn muốn lưu lại thông tin đặt phòng";
            }
            bootbox.confirm(title, function (result) {
                if (!result) return;
                // data

                var url = "/CheckInGroup/DatTruoc";
                Sv.Post({ url: url, data: { model: resroom } },
                    function (response) {
                        if (response.result >= 1) {
                            bootboxLamlt.alert("Thao tác thành công", function () { hashChangeSodophong(); });
                        }
                        else {
                            bootboxLamlt.alert("Thao tác không thành công:" + response.mess);
                        }

                    });
            });
        }
    };

    this.changeStatus = function () {
        if (base.ListRomData.length > 0)
            $('#dllStatus').val(2);
        else
            $('#dllStatus').val(1); 
    }



    // init
    base.init = function (o) {
        
        // default
        Sv.SetupInputMask();
        setupAutoComplete();
        toggleMenu("menu_letan");

        // selected trưởng đoàn
        if (o.action == undefined) {
            base.checked($('#chkLeader'), true);
            base.checked($('#chkPayer'), true);
        } else {
            base.checked($('#chkLeader'), false);
            base.checked($('#chkPayer'), false);
        }

        // setup grid
        if (o.gridcustomer == true)
            base.GridCustomerLoad();
        // setup grid
        if (o.gridRoom == true)
            base.GridRoomLoad();

        $('#dllRoomTypeID').off("change").on("change", base.changeRoomType);;

        // on click
        //kiểm tra phòng click
        $("#btnAddCustomerToGrid").off("click").on("click", base.btnAddCustomerToGrid);
        //kiểm tra phòng click
        $("#btnCheckKiemTraPhong").off("click").on("click", base.KTPhong);
        //save
        $("#btnResSave").off("click").on("click", base.Save);
        //book
        $("#btnBook").off("click").on("click", base.Book);

        base.changeStatus();

    }

}


function changeLabelGiaPhong(val) {
    ctrl.changeGia();
}

function setupAutoComplete() {
    var obj = {
        istext: true,
        onSelect: function (item) {

            var data = (JSON.parse(item.object));
            console.log(data);
            $('#txtname').val(data.Name).change();

            $('#Name').prop('disabled', false);
            $("#hdCusID").val(data.Id);
            $('#txtIdentify').val(data.IdentityNumber);
            $("#dllQuocTich").val(data.CountryId == null ? "238" : data.CountryId);
            $("#txtaddress").val(data.Address);
            $("#txtMobile").val(data.Mobile);
            $("#txtCompany").val(data.Company);
            $("#txtEmail").val(data.Email);
            var dob = data.DOB.length > 0 ? moment(new Date(parseInt(data.DOB.slice(6, -2)))).format("DD-MM-YYYY") : "";
            $("#txtDOB").val(dob);
            // sex
            $("#formReservation  :input[name='r2']").val(data.Sex);
        },
        displayField: function (item) {
            var sdt = "", idt = "";
            if (item.Mobile != undefined)
                sdt = " -  SĐT: " + item.Mobile;
            if (item.IdentifyNumber != undefined)
                idt = " -  CMT: " + item.IdentifyNumber;

            return "KH: " + item.Name + sdt + idt;
        },
        valueField: "Name",
        ajax: {
            url: "/CustomerArriveManage/SelectCustomer",
            preDispatch: function (query) {
                return { search: query, customerold: -1 };
            }
        }
    }
    Sv.TypeaheadConfig($('#txtname'), obj);
}

function setupAutoComplete1() {
    var gUtils = new Utils();
    gUtils.ConfigAutocomplete('#txtname', "/CustomerArriveManage/SelectCustomer", "Name", "Name",
            function (item) {
                var data = (JSON.parse(item.object));
                $('#Name').prop('disabled', false);
                $("#txtName").val(data.Name);
                $("#hdCusID").val(data.Id);
                $("#txtCompany").val(data.Company);
                $("#dllQuocTich").val(data.CountryId == null ? "" : data.CountryId);
                $("#txtEmail").val(data.Email);
                $("#txtIdentify").val(data.IdentifyNumber);
                $("#txtMobile").val(data.Mobile);
                $("#txtDOB").val(moment(new Date(parseInt(data.DOB.slice(6, -2)))).format("DD-MM-YYYY"));
            },
      function (query) {
          var obj =
          {
              search: query,
              customerold: -1
          };
          return obj;
      },
      function (data) {
          return data;
      }
  );

}

function showTTTV() {
    var option = {
        id: $("#hdCusID").val(),
        countryid: $("#dllQuocTich").val(),
        isChecked: $("#formReservation .chkTTTVchecked").is(":checked")
    };
    g_Reservation.showTTTV2("myModalTemp", false, option);
}
