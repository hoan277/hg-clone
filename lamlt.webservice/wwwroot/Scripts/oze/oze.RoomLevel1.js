﻿var $table;
$(document).ready(function () {
    toggleMenu("menu_caidat");
    showDialogLoading();
    $table = $('#example').DataTable({
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "processing": true,
        "serverSide": true,
        "initComplete": function (settings, json) {
            hideDialogLoading();
        },
        /*bFilter: false, bInfo: false,*/
        "dom": '<"top">rt<"bottom" lpi><"clear">',
        "ajax": {
            "url": "/RoomLevel1/List",
            "data": function (d) {
                d.search = $("#txtSearch").val();
            }
        },
        "columns":
            [
            { "data": "Id", "orderable": "false" },           
            { "data": "Name", "orderable": "false" },
            { "data": "Code", "orderable": "false" },
            {
                "data": null, render: function (data, type, row) {
                    var htmlMenu =
                        '<div class="input-table" onclick="javascript:viewDialog(' + row.Id + ')">' +
                            '<a class="btn btn-primary" title="Chi tiết"><i class="fa fa-info-circle"></i></a>' +
                        '</div>' +
                        '<div class="input-table" onclick="javascript:editDialog(' + row.Id + ')">' +
                            '<a class="btn btn-primary" title="Chỉnh sửa"><i class="fa fa-pencil-square-o"></i></a>' +
                        '</div>'+
                        '<div class="input-table" onclick="javascript:deleteDialog(' + row.Id + ')">' +
                            '<a class="btn btn-primary" title="Xóa"><i class="fa fa-times"></i></a>' +
                        '</div>' +
                        '<div class="input-table" onclick="javascript:viewListRoom(' + row.Id + ')">' +
                            '<a class="btn btn-primary" title="Danh sách phòng"><i class="fa fa-list"></i></a>' +
                        '</div>';
                    return htmlMenu;
                }
            }
            ]
    });
    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
        }
    });
    $("#btnSearch").off("click").on('click', function (e) {
        searchGrid();
    });

});

function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
} 

// sauwr
function editDialog(id) {
    showDialogLoading();

    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        if (id == 0) {
            $("#myModal").find('.modal-title').html("Thêm mới");
        } else {
            $("#myModal").find('.modal-title').html("Cập nhật"); 
        }

        $("#myModal button#btnSave").css("display", "inline");
        $("#myModal .modal-body-content").load("/RoomLevel1/Edit/" + id, function () {
            hideDialogLoading();


            $("#btnSave").off("click");
            $("#btnSave").click(function () {
                if (!$("#frmDetail").valid()) return;
                var pdata = getFormData($("#frmDetail"));
                showDialogLoading();
                setTimeout(function () {
                    $.post("/RoomLevel1/Update", { obj: pdata }, function (data) {
                        hideDialogLoading(); 
                        if (data.result > 0) {
                            bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                        }
                        else {
                            alert("Có lỗi khi tạo hạng phòng");
                        }
                    });
                }, 50);
            }); 
        }); 
    });
    $("#myModal").modal("show");
}

// 
function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").find('.modal-title').html("Xem chi tiết");
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/RoomLevel1/GetDetail/" + id);
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}


function deleteDialog(id) {
    if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
        if (!result) return;
        $.post("/RoomLevel1/delete", { id: id }, function (data) {
            if (data.result > 0) {
                bootboxLamlt.alert("Thao tác thành công", function () { searchGrid(); });
            } else {
                if (data.result == -1) {
                    bootbox.alert("Tầng/ lầu đang được sử dụng, kiểm tra lại");
                } else {
                    alert("Có lỗi khi xóa tầng/ lầu:" + data.mess);
                }
            }
        });
    })); 
}


function viewListRoom(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal").find('.modal-title').html("Danh sách phòng");
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/RoomLevel1/GetListRoom/" + id);
    });
    $("#myModal").modal("show");
}