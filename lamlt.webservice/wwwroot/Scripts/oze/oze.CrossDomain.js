﻿var urlShop = "http://localhost:54678";
var domainShop = function ()
{
    this.divContainer = "main-content";
    var self = this;
    this.loadMenu=function()
    {
        $("#menu_ozeshop").load(urlShop + "/SaleOrderManage/Index?embed=1");
    }
    this.loadSaleOrderManage = function () {
        $("#"+self.divContainer).load(urlShop + "/SaleOrderManage/Index?embed=1");
    }
    this.loadProductsPriceManage = function () {
        $("#"+self.divContainer).load(urlShop + "/ProductsPriceManage/Index?embed=1");
    }
    this.loadPurchaseOrderVendorManage = function () {
        $("#"+self.divContainer).load(urlShop + "/PurchaseOrderVendorManage/Index?embed=1");
    }
    this.loadUnit = function () {
        $("#"+self.divContainer).load(urlShop + "/Unit/Index?embed=1");
    }
    this.loadProductOze = function () {
        $("#"+self.divContainer).load(urlShop + "/ProductOze/Index?embed=1");
    }
    this.loadProductsPriceManage = function () {
        $("#"+self.divContainer).load(urlShop + "/ProductsPriceManage/Index?embed=1");
    }
    this.loadPromotionManager = function () {
        $("#"+self.divContainer).load(urlShop + "/PromotionManager/Index?embed=1");
    }
    this.loadDiscountManager = function () {
        $("#"+self.divContainer).load(urlShop + "/DiscountManager/Index?embed=1");
    }    
    this.loadProductGroup = function ()
    {
        $("#"+self.divContainer).load(urlShop + "/ProductGroup/Index?embed=1");
    }
    this.loadPurchaseOrderOzeManage = function () {
        $("#"+self.divContainer).load(urlShop + "/PurchaseOrderOzeManage/Index?embed=1");
    }
    this.loadSaleOrderManage = function () {
        $("#"+self.divContainer).load(urlShop + "/SaleOrderManage/Index?embed=1");
    }
    this.loadPurchaseOrder = function () {
        $("#"+self.divContainer).load(urlShop + "/PurchaseOrder/Nhanhang?embed=1");
    }
}
