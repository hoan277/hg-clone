﻿function UserLog() {
    var self = this;
    this.$table;

    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/UserLog/List",
                "data": function (d) {
                    d.search = "";
                    d.hotelid = $("#hotelid").val();
                    d.hotelname = $("#hotelid option:selected").val();

                    d.username = $("#txtSearch").val();
                    d.functionname = $("#txtFunction").val();

                    d.FromDate = $("input[name='FromDate']").val();
                    d.ToDate = $("input[name='ToDate']").val();

                    delete d.columns;
                }
            },
            "columns": self.columns()
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }

    this.columns = function () {
        var columns = [{
            "data": null,// STT
            render: function (data, type, row, infor) {
                if (self.$table.length > 0) {
                    return self.$table.page.info().page + infor.row + 1;
                }
                else
                    return infor.row + 1;
            }
        }];
        if (listHotel > 1) {
            columns.push({ 'data': 'HotelName' });
        }
        columns.push({ 'data': 'UserName' });
        columns.push({
            'data': null,
            render: function (data, type, row, infor) {
                return Sv.DateToString(row.LogDate, "DD/MM/YYYY HH:mm");
            }
        });
        if (isSp == 1) {
            columns.push({
                'data': null,
                render: function (data, type, row, infor) {
                    return "/" + row.ControllerName + "/" + row.ActionName;
                }
            });
        }
        columns.push({
            'data': null,
            render: function (data, type, row, infor) {
                if (row.Type == 2) {
                    return row.SysNote;
                }
                var str = row.ActionText;
                if (row.ControllText.length > 0)
                    str += (" - " + row.ControllText);
                return str
            }
        });
        return columns;

    }

}

var $table;
var otbl_userLog = new UserLog();
$(document).ready(function () {
    reportDate();
    setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

    otbl_userLog.setupGrid();

    $("#hotelid").select2();

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            otbl_userLog.searchGrid();
        }
    });
    $("#btnSearch").off("click").on("click", function (e) {

        otbl_userLog.searchGrid();

    });
    $("#btnRefresh").off("click").on("click", function (e) {
        $("#hotelid").val("-1");
        $("#txtSearch").val("");
        $("#txtFunction").val("");
        if (!daysView) daysView = -30;
        Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView)));
        Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
        otbl_userLog.searchGrid();
    });
});
