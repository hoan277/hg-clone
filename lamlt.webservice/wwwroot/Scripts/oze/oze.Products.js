﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}
function GetDataSubmit(form) {
    var obj = [];

    form.find("tr.item").each(function (index) {
        var trItem = $(this);
        var v1 = trItem.find(".vol").attr('V1').trim();
        var v2 = trItem.find(".vol").attr('V2').trim();
        var v3 = trItem.find(".vol").attr('V3').trim();
        var p = trItem.find(".productvalue").text().replace(/\./g, '');
        //debugger; 
        var itemObj =
        {
            Index: index,
            Price: p ? p : 0,
            UnitId: trItem.find(".unit").attr('UnitID').trim(),
            UnitMiniId: trItem.find(".unitMini").attr('UnitIDMin').trim(),
            vol1: v1 != "null" ? v1 : 0,
            vol2: v2 != "null" ? v2 : 0,
            vol3: v3 != "null" ? v3 : 0,
            Quantity: trItem.find(".Quantity").attr('quantity').trim(),
            weightShip: trItem.find(".weightShip").attr('weightShip').trim()

        }
        obj.push(itemObj);
    });
    return obj;
}

function editDialog(id, callback) {
    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        $("#myModal .modal-body-content").load("/Product/Edit/" + id, function () {

            currentListPrice = [];
            loadTable(id);
            $("#SupplierID").select2({ width: "100%", dropdownParent: $("#myModal") });
            $("#UnitID").select2({ width: "100%", dropdownParent: $("#myModal") });
            $("#ProductGroupID").select2({ width: "100%", dropdownParent: $("#myModal") });
            Sv.SetupInputMask();
            $("#myModal button#btnSave").css("display", "inline");

            // save
            $("#myModal button#btnSave").off("click").click(function () {
                setupValidate();
                if (!$("#dummyProduct").valid()) return;
                var pdata = getFormData($("#dummyProduct"));
                var listPrice = GetDataSubmit($("form#dummyProduct"));

                var dataModel = new FormData();

                var files = $("form#dummyProduct #PictureFile")[0].files;
                for (var i = 0; i < files.length; i++) {
                    dataModel.append("file" + i, files[i]);
                }
                dataModel.append("obj", JSON.stringify(pdata));
                dataModel.append("listprice", JSON.stringify(listPrice));

                Sv.AjaxPostFile({
                    url: "/Product/update",
                    data: dataModel
                }, function (data) {

                    if (data.result > 0) {
                        bootboxLamlt.alert("Thao tác thành công", function () {
                            $("#myModal").modal("hide");
                            if (typeof callback == "function")
                                callback();
                            else
                                searchGrid();
                        });
                    }
                    else if (data.result == -6) {
                        alert("Sản phẩm đã tồn tại trong danh sách");
                    }
                    else if (data.result == -7) {
                        alert("Sản phẩm đã tồn tại trong danh sách");
                    }
                    else if (data.result == -8) {
                        alert("Mã sản phẩm đã tồn tại trong danh sách");
                    }
                    else {
                        alert("Có lỗi khi cập nhật sản phẩm");
                    }
                });
            });

            // image 
            $("#PictureFile").
                off("change").
                change(function () {
                    readURL(this, $("#imageView"));
                });


        });
    });
    $("#myModal").modal("show");
}
function setupValidate() {
    var form = $('#dummyProduct').on();
    form.validate({
        rules: {
            Name: {
                required: true
            },
            QuotaMinimize: {
                required: true,
                priceMin: true
            },
            SalePrice: {
                required: true,
                priceMin: true
            },
            PriceOrder: {
                required: true,
                priceMin: true
            },
            txtQuantityMin: {
                required: true,
                priceMin: true
            },
            txtWeightShip: {
                required: true
            }
        },
        messages: {
            Name: {
                required: 'Vui lòng nhập tên sản phẩm'
            },
            QuotaMinimize: {
                required: 'Vui lòng nhập định mức tối thiểu',
                priceMin: 'Định mức tối thiểu không được phép âm'
            },
            SalePrice: {
                required: 'Vui lòng nhập giá bán',
                priceMin: 'Giá bán không được phép âm'
            },
            PriceOrder: {
                required: 'Vui lòng nhập giá nhập',
                priceMin: 'Giá nhập không được phép âm'
            },
            txtQuantityMin: {
                required: 'Vui lòng nhập quy các đóng gói',
                priceMin: 'Quy cách đóng gói không được phép âm'
            },
            txtWeightShip: {
                required: 'Vui lòng nhập trọng lượng'
            }
        }
    });
}
function deleteDialog(id) {
    if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
        if (!result) return;
        $.post("/Product/delete", { id: id }, function (data) {
            if (data.result > 0) {
                bootboxLamlt.alert("Thao tác thành công", function () { searchGrid(); });
    }
    else {
        if (data.result == -1) {
            bootbox.alert(data.mess);
    } else
                alert("Có lỗi khi xóa Sản phẩm:" + data.mess);
    }
    });
    }));
}
function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/Product/GetDetail/" + id, function () { loadTableView(id); });
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}

function viewDialogStall(id) {
    $("#modalStall").off('show.bs.modal');
    $("#modalStall .modal-body-content").html('<p>loading..</p>');
    $("#modalStall").on('show.bs.modal', function () {
        $("#modalStall .modal-body-content").load("/Product/GetStallProduct/" + id);
        $("#modalStall button#btnSaveStall").css("display", "inline-block");
        $("#btnSaveStall").off("click").click(function () {
            Sv.AjaxPost({
                url: "/Product/updateStall",
                data: { stall: $('#selectStall').val(), id: $('#ID').val() }
            }, function (data) {
                if (data) {
                    bootboxLamlt.alert("Thao tác thành công", function () {
                        $("#modalStall").modal("hide");
                        searchGrid();
                    });
                }
                else {
                    alert("Có lỗi khi thao tác");
                }
            });
        });
    });
    $("#modalStall").modal("show");
}

function readURL(input, view) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            view.attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function getFileData($e) {
    var data = new FormData();
    var files = $e[0].files;
    for (var i = 0; i < files.length; i++) {
        data.append("file" + i, files[i]);
    }
    return data;
}

function checkAll(event) {
    event.stopPropagation();
    var isCheck = $('.col-export-all').is(':checked');
    $('.col-export').prop('checked', isCheck);
    return false;
}
