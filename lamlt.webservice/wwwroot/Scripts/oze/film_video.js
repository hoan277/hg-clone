﻿function film_video() {
    var self = this;
    this.tableName = "film_video";
    this.idModal = "modalfilm_video";
    this.$table;

    this.editDialog = function (id) {

        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                // console.log(pdata);
                $.post("/film_video/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {

                        /*debugger;
                        console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                        imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                        //console.log(imgs)
                        self.UpdateCheckbox(data.result, imgs);
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });


                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
				/*
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data) 
					{ 
						hideDialogLoading();						
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});*/
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    // updateEdit view edit
    this.updateEdit = function (id) {
        if (!$("#form" + self.tableName).valid()) return;
        var pdata = getFormData($("#form" + self.tableName));
        showDialogLoading();
        // console.log(pdata);
        $.post("/film_video/update", { obj: pdata }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {

                /*debugger;
                console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                //console.log(imgs)
                self.UpdateCheckbox(data.result, imgs);
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); });


            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });
    }
    //  chuyển trạng thái status rồi update trạng thái
    this.updateStatus = function (id, status) {
        // debugger;
        $.post("/film_video/updatestatus", {
            obj: { id: id, status: status }
        }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });

    }
    // update anh trong bang film_video_image
    this.UpdateImage = function (url, filmid) {
        //debugger;
        $.post("/film_video/UpdateImage", {
            //obj: { id: id, url: url }
            url: url, filmid: filmid
        }, function (data) {
            hideDialogLoading();
            self.getImageupdate(filmid);
            //if (data.result > 0) {
            //    bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            //}
            //else {
            //    alert("Có lỗi khi tạo :" + data.mess);
            //}
        });
    }
    // select hinh anh theo filmid trong ban film_video_image
    this.getImageupdate = function (filmid) {
        $.get("/film_video/getImageupdate?id=" + filmid, function (data) {
            hideDialogLoading();
            $("#list_image").html("");
            for (var index in data.result) {
                if (data.result[index].url != undefined) {
                    $("#list_image").append("<div style= 'width:20%;display:inline-grid;margin:5px'><img src='" + data.result[index].url + "' width=100px/><a href='javascript:new film_video().delimage(" + data.result[index].id + "," + data.result[index].filmid + ")'>'<img src='/images/icon/icon-delete.png' style='border: none;' title='Xóa'></a>'</div>");
                }
            }
        });
    }
    // xóa hình ảnh trong film_video_image theo id
    this.delimage = function (id, filmid) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/deteleimage/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { self.getImageupdate(filmid); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    // checkbox 
    this.UpdateCheckbox = function (filmid, catalog_id) {

        $.post("/film_video/UpdateCheckbox", {
            //obj: { catalog_id: catalog_id, filmid: film_catalog_film.filmid }
            catalog_id: catalog_id, filmid: filmid
        }, function (data) {
        });
    }
    this.edits = function (id) {
        window.location.href = "/" + self.tableName + "/Edits/" + id;
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_video').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_video_title").val();
                    d.cpid = $("#search_film_video_cpid").val();

                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'title' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.desc == null) return "";
                            if (row.desc.length <= 50) return row.desc;
                            return row.desc.substr(0, 49) + "...";
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.format == 0) return "HD";
                            return "SD";
                        }
                    }
                    , { 'data': 'publish_year' }
                    , { 'data': 'publish_countryid_title' }
                    , { 'data': 'duration' }
                    , { 'data': 'actor' }
                    , { 'data': 'director' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.film_type == 0) return "Việt Sub";
                            if (row.film_type == 1) return "Thuyết Minh";
                            if (row.film_type == 2) return "Sub+Thuyết Minh ";
                            return "Unknown";
                        }
                    }
                    , { 'data': 'catalog_id_title' }
                    , { 'data': 'thumb_file' }
                    , { 'data': 'price' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.exclusive == 1) return "<input type='checkbox' checked/>";
                            return return "<input type='checkbox'/>";;
                        }
                    }
                    , { 'data': 'price' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.status == 0) return "Khởi tạo";
                            if (row.status == 1) return "Chờ duyệt";
                            if (row.status == 2) return "Đã duyệt";
                            if (row.status == 3) return "Không duyệt";
                            return "Unknown";
                        }
                    }
                    , { 'data': 'cpid_title' },
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu =
                                '<div class="edit-delete-table">' +
                                '<div class="edit-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:o' + self.tableName + '.edits(' + row.id + ')" >' +
                                '<img src="/images/icon/icon-edit.png" style=" border: none;" title="Chỉnh sửa">' +
                                '</div>' +
                                '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<img src="/images/icon/icon-delete.png" style=" border: none;" title="Xóa">' +
                                '</div>' +
                                '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<img src="/images/icon/icon-view.png" style=" border: none;" title="Thông tin chi tiết">' +
                                '</div>';
                            if (window.rightcode == 'BT' && row.status == 0)
                                htmlMenu = htmlMenu + '<div class ="status-table" id= "status-table" data-toggle ="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.updateStatus(' + row.id + ',1) ">' +
                                    '<i class = "fa fa-check text-danger" title ="chờ duyệt"></i>';
                            if (window.rightcode == 'BT' && row.status == 2)
                                htmlMenu = htmlMenu + '<div class ="status-table" id= "status-table" data-toggle ="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.updateStatus(' + row.id + ',0) ">' +
                                    '<i class = "fa fa-check text-danger" title ="Ẩn"></i>';
                            if (window.rightcode == 'TT' && row.status == 0)
                                htmlMenu = htmlMenu + '<div class="status-table" id= "status-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.updateStatus(' + row.id + ',1)">' +
                                    '<i class = "fa fa-check text-danger" title ="chờ duyệt"></i>';
                            if (window.rightcode == 'BQ' && row.status == 1)
                                htmlMenu = htmlMenu + '<div class="status-table" id= "status-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.updateStatus(' + row.id + ',2)">' +
                                    '<i class = "fa fa-check text-danger" title="duyệt"></i>';
                            if (window.rightcode == 'BQ' && row.status == 2)
                                htmlMenu = htmlMenu + '<div class="status-table" id= "status-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.updateStatus(' + row.id + ',3)">' +
                                    '<i class = "fa fa-check text-danger" title="Ẩn"></i>';

                            return htmlMenu;
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }

}