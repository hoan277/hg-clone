﻿// validate file type
$.validator.addMethod("filetype", function (value, element) {
    if (value.length == 0)
        return true;
    var type = $(element)[0].type;
    if (type != "file")
        return true;
    var fileTyle = [
        "application/vnd.ms-excel", //xls
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" //xlsx
    ];
    var files = $(element)[0].files;
    for (var i = 0; i < files.length; i++) {
        var itemFile = files[i];
        var ck = fileTyle.indexOf(itemFile.type);
        if (ck < 0) {
            return false;
        }
    }
    return true;
}, "");
// validate file size
$.validator.addMethod("filesize", function (value, element) {
    if (value.length == 0)
        return true;
    var type = $(element)[0].type;
    if (type != "file")
        return true;
    var files = $(element)[0].files;
    for (var i = 0; i < files.length; i++) {
        var itemFile = files[i];
        // 200MB = 200000000 bytes
        if (itemFile.size > 200000000) {
            return false;
        }
    }
    return true;
}, "");
// validate count file
$.validator.addMethod("filesmax", function (value, element) {
    if (value.length == 0)
        return true;
    var type = $(element)[0].type;
    if (type != "file")
        return true;
    var files = $(element)[0].files;
    if (files.length > 10)
        return false;
    else
        return true;
}, "");

var Import = function () {
    var base = this;
    this.tableResult = $("#tableResult");
    this.$tableview = $("#tableview");
    this.$tableEdit = $("#tableEdit");
    this.tableImportResult = $("#tableImportResult");
    this.$table = $("#table");
    this.$btnOpenAdd = $("#btnAdd");
    this.$btnupload = $("#btnupload");
    this.$modalAdd = $("#addModal");
    this.$btnConfirm = $('#btnConfirm');
    this.UploadData = [];
    this.ValidateFile = function () {
        var $form = $("#formadd");
        $form.validate({
            rules: {
                UploadFile: {
                    required: true,
                    filetype: true,
                    filesize: true
                }
            },
            messages: {
                UploadFile: {
                    required: 'Vui lòng chọn file cần tải lên',
                    filetype: 'File tải lên không đúng định dạng',
                    filesize: 'File tải lên dung lượng quá lớn'
                }
            }
        });
    };

    this.Uploadfile = function () {
        var data = new FormData();
        var obj = {},
            $form = $("#formadd").on();
        var files = $form.find("input[name='UploadFile']")[0].files;
        for (var i = 0; i < files.length; i++) {
            data.append("file" + i, files[i]);
        }
        var sStore = $('#sStore').val();
        if(sStore == null || sStore == '')
            sStore = '0';
        data.append("store", sStore);
        Sv.AjaxPostFile({
            Url: '/Store/UploadFileResetTonKho',
            Data: data
        }, function (rs) {
            if (rs.Status == "01") {
                base.UploadData = rs.ListUpladed;
                base.LoadUploadResult();
            } else {
                Dialog.Alert(rs.Message, (rs.Status == "01" ? Dialog.Success : Dialog.Error), function () { });
            }
        }, function () {
            Dialog.Alert("Có lỗi xảy ra trong quá trình xử lý", Dialog.Error);
        });
    };

    this.LoadUploadResult = function () {
        base.tableImportResult.bootstrapTable('load', base.UploadData);
        var totalOk = 0;
        var totalFail = 0;
        $.each(base.UploadData, function (i) {
            if (base.UploadData[i].Status == true) {
                totalOk++;
            } else {
                totalFail++;
            }
        });
        if (totalOk == 0) {
            $("#btnConfirm").prop('disabled', 'disabled');
        } else {
            $("#btnConfirm").removeAttr('disabled', 'disabled');
        }
    }

    this.UploadTableColumns = function () {
        var obj = [
            Sv.BootstrapTableColumn("String", {
                title: 'STT',
                field: 'Index',
                width: '10%',
                align: "center",
                formatter: function (value, data, index) {
                    return Sv.BootstrapTableSTT(base.$table, index);
                }
            }),
            Sv.BootstrapTableColumn("String", {
                title: 'Mã sản phẩm',
                field: 'Code',
                width: '20%',
                align: "center"
            }),
            Sv.BootstrapTableColumn("String", {
                title: 'Tên',
                field: 'Name',
                width: '30%',
                align: "center"
            }),
            Sv.BootstrapTableColumn("String", {
                title: 'Đơn vị',
                field: 'Unit',
                width: '10%',
                align: "center"
            }),
             Sv.BootstrapTableColumn("String", {
                 title: 'Số lượng',
                 field: 'Quantity',
                 width: '10%',
                 align: "center",
                 formatter: function (value, data, index) {
                     return value;
                 }
             }),
            Sv.BootstrapTableColumn("String", {
                title: 'Trạng thái',
                field: 'Status',
                align: "center",
                width: '15%',
                formatter: function (value, data, index) {
                    if (value == true) {
                        return 'Hợp lệ';
                    } else {
                        return 'Không hợp lệ';
                    }
                }
            })
        ];
        return obj;
    };

    this.ResetformAdd = function () {
        var $form = $("#formadd");
        var validator = $form.validate();
        validator.resetForm();
        base.UploadData = [];
        base.LoadUploadResult();
        $("#fileupload").val('');
        $("#btnConfirm").prop('disabled', 'disabled');
        $("#btnExport").prop('disabled', 'disabled');
    };
}

$(document).ready(function () {
    var imp = new Import();
    imp.ValidateFile();
    imp.$btnOpenAdd.click(function () {
        document.getElementById("fileupload").value = "";
        imp.ResetformAdd();
        imp.$modalAdd.modal({
            backdrop: "static"
        });
    });
    imp.tableImportResult.bootstrapTable({
        classes: 'table table-condensed',
        formatNoMatches: function () {
            return "Chưa có dữ liệu";
        },
        columns: imp.UploadTableColumns()
    });
    imp.$btnupload.click(function () {
        var $form = $("#formadd").on();
        if ($form.valid()) {
            imp.Uploadfile();
        }
    });
    $("#btnDowloadFile").click(function (e) {
        window.location = "/Store/DownloadTemplate?file=template_tonkho.xlsx";
    });

    imp.$btnConfirm.click(function () {
        Sv.AjaxPost({ Url: '/Store/ImportConfirm', Data: { model: imp.UploadData } }, function (res) {
            if (res.Status === "01") {
                Dialog.Alert(res.Message, Dialog.Success, function () {
                    imp.$modalAdd.modal('hide');
                    setTimeout(function() { location.reload() }, 300);
                });
            } else {
                Dialog.Alert(res.Message, Dialog.Error, function () {
                    imp.$modalAdd.modal('hide');
                });
            }
        }, function () {
            Dialog.Alert('Có lỗi trong quá trình xử lý', Dialog.Error, function () {
                imp.$modalAdd.modal('hide');
            });
        });
    });
})