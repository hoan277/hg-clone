﻿

$(document).ready(function () {

    var w = $("#hotelregister-box").width();
    $("#hotelregister-box #tab2.hr-content").css("left", w + "px"); 
});

window.removeEventListener("beforeunload", fneforeunload);
 
function openCapcha() {
 
    confirm("Bạn chắc chắn muốn tạo khách sạn <strong>" + HotelInfo.name + "</strong> không?", function (result) {
        if (!result) return;

        var dataP = {
            info: HotelInfo,
            roomtype: LstRoomType, 
        }
        showDialogLoading();    
        $.post("/tbl_Hotel/SaleSubmitRegister", dataP)
          .done(function (rs) {
              hideDialogLoading();
              if (rs.IntStatus > 0){
                  alert("Tạo thành công khách sạn <strong>" + HotelInfo.name + "</strong>.", function () {
                      window.location.reload();
                  });
              } else {
                  alert(rs.Message);
              }
          });
    });
}
