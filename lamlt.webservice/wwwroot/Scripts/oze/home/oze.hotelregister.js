﻿
jQuery.validator.setDefaults({
    ignore: '.ignore, :hidden, :disabled, :not(:visible) ',
    errorElement: 'span',
    errorPlacement: function (error, element) {
        var parent = element.parent();
        if (parent.hasClass('input-group')) {
            parent.parent().append(error);
        } else {
            parent.append(error);
        }
    },
    success: function (label) { // khi hết lỗi thì làm gì
        label.closest('.form-group').removeClass('has-error');
        label.remove();
    },

});


var HotelInfo = {};
var LstRoomType = [];
var PriceRoomType = [];

$(document).ready(function () {
    // set height
    setHeight($(".hr-content.active"));
    //setup next 
    setUpNext();

    numberMask();

    // clear dữ liệu
    HotelInfo = {};
    LstRoomType = [];
    PriceRoomType = [];

    $("#tab1form").validate();
    $("#tab2form").validate();

    $("#btnCaptcha").click(registerSubmit); 
});
 
//window.addEventListener("beforeunload", fneforeunload);
function fneforeunload(event) {
    event.returnValue = "Các thay đổi đã thực hiện sẽ không được lưu";
    return event.returnValue;
}

function alert(msg, callback) {
    bootbox.alert({
        title: "Thông báo",
        message: msg,
        callback: function () {
            if (callback) callback.call();
        }
    });
}

function openCapcha() {
    $("#captErr").hide();
    $("#modalCaptcha").off().on('show.bs.modal', function (e) {
        $("#captcha_box > a").trigger("click");
    })
    $("#modalCaptcha").modal("show");
    $("#CaptchaInputText").change(function () {
        $("#captErr").hide();
    });
    $("#btnSubmit").off().click(function () {
        var dataP = {
            info: HotelInfo,
            roomtype: LstRoomType,
            CaptchaInputText: $("#CaptchaInputText").val(),
            CaptchaInputKey: $("#CaptchaDeText").val(),
        }
         
        Sv.Post({ url: "/Home/SubmitRegister", data: dataP }) 
            .then(function (rs) { 
                if (rs.IntStatus == -10) {
                    $("#captcha_box > a").trigger("click");
                    $("#captErr").html("Mã captcha không chính xác vui lòng thử lại").show();
                }
                else if (rs.IntStatus == 1) {
                  //  window.removeEventListener("beforeunload", fneforeunload);
                    window.location.href = "/dang-ky-thanh-cong"

                } else {
                    $("#captErr").html(rs.Message).show();
                } 
               // console.log(rs);
            });
    });

}

function showDialogLoading(msg) {
    if (!msg) msg = "";
    if ($("body > div.ajaxInProgress").length <= 0) {
        var str = '<div class="ajaxInProgress"><div class="loading-ct" >' +
            '<div>' + msg + '</div>' +
            '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' +
            ' </div> </div>';
        $("body").append(str);
    }
    $("body > div.ajaxInProgress").show();
}

function hideDialogLoading() {
    if ($("body > div.ajaxInProgress").length > 0)
        $("body > div.ajaxInProgress").hide();
}

$(window).resize(function () {
    setHeight($(".hr-content.active"));
});

function numberMask() {
    var ua = navigator.userAgent.toLowerCase();
    isAndroid = ua.indexOf("android") > -1;
    if (isAndroid) {
        $("body").find(".number-mask").on().prop('type', 'number');
    } else {
        $("body").find(".number-mask").on().inputmask({
            alias: 'decimal',
            groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
            radixPoint: ",", /* Ký tự phân cách với phần thập phân */
            autoGroup: true,
            digits: 0, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
            autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
            allowMinus: true, /* Không cho nhập dấu trừ */
            allowPlus: false, /* Không cho nhập dấu cộng */
            integerDigits: 16,

        });
    }

}

function setHeight($e) {
    if (!$e) $e = $(".hr-content.active");
    var h = $e.height();
    $("#content_reg").animate({ "min-height": h }, { duration: 500, });
}

// add row thêm mới hạng phòng
function addRow() {

    if (!$("#tab2form").valid()) return;
    var obj = saveRoomType();
    // append html
    var html = bindHtml1RoomType(obj)
    $("#tab2form #table1 #tr_roomtype").before(html);
    // update stt
    updateTableStt();
    $("#roomType").focus();

}
// add row thêm mới hạng phòng
function addRow2() {

    if (!$("#tab2form").valid()) return;
    var obj = saveRoomType2();
    // append html
    var html = bindHtml2RoomType(obj)
    $("#tab2form #table2 #tr_roomtype2").before(html);
    // update stt
    updateTableStt();
    $("#roomType2").focus();

}

function removeRow(id) {
    if (LstRoomType.length <= 0) return;
    LstRoomType = LstRoomType.filter(function (x) { return x.id != id; });
    $("#table1 tr#tr_" + id).remove();
    updateTableStt();
}

function removeRow2(id) {
    if (LstRoomType.length <= 0) return;
    LstRoomType = LstRoomType.filter(function (x) { return x.id != id; });
    $("#table2 tr#tr_" + id).next().remove();
    $("#table2 tr#tr_" + id).next().remove();
    $("#table2 tr#tr_" + id).next().remove();
    $("#table2 tr#tr_" + id).next().remove();
    $("#table2 tr#tr_" + id).remove();
    updateTableStt();
}


function updateTableStt() {
    $("#tab2form #table1 tbody tr").each(function (i, e) { $(e).find("td:eq(0)").html(i + 1); });
    $("#tab2form #table2 tbody tr").each(function (i, e) { $(e).find("td.sttt").html((i / 5) + 1); });
}

// chuyển tab
function setUpNext() {
    // next
    $("#btnNext").click(function () {
        if (!$("#tab1form").valid()) return;

        $("#tab2form").validate().resetForm();
        saveInfo();

        activeTab($("#tab1"), $("#tab2"), "left", function () {
            $("#roomType").focus();
        });
        setHeight($("#tab2"));
    });

    //$("#btnNext2").click(function () {
    //    // validate hạng phòng
    //    if (LstRoomType.length <= 0) {
    //        $("#tab2form").valid();
    //        return;
    //    }
    //    $("#tab3form").validate().resetForm();
    //    bindPrice();
    //    activeTab($("#tab2"), $("#tab3"), "left");
    //    setHeight($("#tab3"));
    //});

    // prev
    $("#btnPrev1").click(function () {
        $("#tab1form").validate().resetForm();
        activeTab($("#tab2"), $("#tab1"), "right");
        setHeight($("#tab1"));
    });

    //$("#btnPrev2").click(function () {
    //    $("#tab2form").validate().resetForm();
    //    activeTab($("#tab3"), $("#tab2"), "right");
    //    setHeight($("#tab2"));
    //});
}

// tabActive: tab hiện tại
// tabMove: tab chuyển tới
// chuyển sang bên nào
function activeTab($tabMove, $tabActive, move, callback) {
    var w = $("#hotelregister-box").width();
    //showDialogLoading();
    $tabMove.css("position", "absolute").animate(move == 'left' ? { 'left': '-' + w + 'px' } : { 'left': w + 'px' }, { duration: 500, });
    $tabActive.css("position", "absolute").animate({ 'left': '0' }, {
        duration: 500,
        complete: function () {
            $(".hr-content").removeClass("active");
            $tabActive.addClass("active");

            if (typeof callback == "function")
                callback();
            $tabActive.css("position", "relative");
            //hideDialogLoading();
        }
    });
}

function registerSubmit() {
    if (!$("#tab2form").valid()) {
        $("#tab2form").validate().resetForm();
    } else {
        if ($("#table2").is(":visible"))
            addRow2();
        else if ($("#table1").is(":visible"))
            addRow();
    }
    if (LstRoomType.length <= 0) {
        alert("Vui lòng nhập thông tin hạng phòng của khách sạn.");
        return;
    }
    // kiểm tra max phòng
    if (totalRoom() > 100) {
        alert("Để sử dụng khách sạn lớn hơn 50 phòng vui lòng liên hệ với chúng tôi.");
        return;
    } 
    openCapcha(); 
}

function totalRoom() {
    var sum = 0;
    for (var i in LstRoomType) {
        sum += parseFloat(LstRoomType[i].roomQuantity);
    }
    return sum;
}

function saveInfo() {
    // save thông tin giá phòng
    HotelInfo = {
        name: $("#hotelName").val(),
        email: $("#hotelEmail").val(),
        phone: $("#hotelPhone").val(),
        pass: $("#hotelPass").val(),
        repass: $("#hotelRePass").val(),
        address: $("#hotelAddress").val(),
    };
}

function saveRoomType() {
    // tạo db
    var obj = {
        id: (new Date()).getTime(),
        name: $("#roomType").val(),
        roomQuantity: $("#roomQuantity").val(),
        priceHour: $("#priceHour").val(),
        priceDay: $("#priceDay").val(),
        priceNight: $("#priceNight").val(),
    }
    // push vào data
    LstRoomType.push(obj);
    // reset
    $("#roomType").val("");
    $("#roomQuantity").val("0");
    $("#priceHour").val("0");
    $("#priceDay").val("0");
    $("#priceNight").val("0");

    return obj;
}

function saveRoomType2() {
    // tạo db
    var obj = {
        id: (new Date()).getTime(),
        name: $("#roomType2").val(),
        roomQuantity: $("#roomQuantity2").val(),
        priceHour: $("#priceHour2").val(),
        priceDay: $("#priceDay2").val(),
        priceNight: $("#priceNight2").val(),
    }
    // push vào data
    LstRoomType.push(obj);
    // reset
    $("#roomType2").val("");
    $("#roomQuantity2").val("0");
    $("#priceHour2").val("0");
    $("#priceDay2").val("0");
    $("#priceNight2").val("0");

    return obj;
}

function bindHtml1RoomType(obj) {
    var html = '<tr id="tr_' + obj.id + '">' +
                '<td>1</td>' +
                '<td>' + obj.name + '</td>' +
                '<td class="text-right">' + Sv.NumberToString(obj.roomQuantity) + '</td>' +
                '<td class="text-right">' + Sv.NumberToString(obj.priceHour) + '</td>' +
                '<td class="text-right">' + Sv.NumberToString(obj.priceDay) + '</td>' +
                '<td class="text-right">' + Sv.NumberToString(obj.priceNight) + '</td>' +
                '<td class="td-button"><button type="button" onclick="removeRow(' + obj.id + ')" class="btn btn-sm btn-info btn-red">-</button></td>' +
                '</tr>';
    return html;
}

function bindHtml2RoomType(obj) {
    var html = '<tr id="tr_' + obj.id + '">' +
                    '<td class="sttt" rowspan="5" style="vertical-align:middle">1</td>' +
                    '<td>Hạng phòng</td>' +
                    '<td>' + obj.name + '</td>' +
                    '<td rowspan="5" style="vertical-align:middle" class="td-button"><button type="button" onclick="removeRow2(' + obj.id + ')" class="btn btn-sm btn-info btn-red">-</button></td>' +
                '</tr>' +
                '<tr>' +
                    '<td>Số phòng</td>' +
                    '<td class="text-right">' + Sv.NumberToString(obj.roomQuantity) + '</td>' +
                '</tr>' +
                '<tr>' +
                    '<td>Giá giờ</td>' +
                    '<td class="text-right">' + Sv.NumberToString(obj.priceHour) + '</td>' +
                '</tr>' +
                 '<tr>' +
                    '<td>Giá ngày</td>' +
                    '<td class="text-right">' + Sv.NumberToString(obj.priceDay) + '</td>' +
                '</tr>' +
                 '<tr>' +
                    '<td>Giá đêm</td>' +
                    '<td class="text-right">' + Sv.NumberToString(obj.priceNight) + '</td>' +
                '</tr>';
    return html;
}
