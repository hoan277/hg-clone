﻿
jQuery.validator.setDefaults({
    ignore: '.ignore, :hidden, :disabled, :not(:visible) ',
    errorElement: 'span',
    errorPlacement: function (error, element) {
        var parent = element.parent();
        if (parent.hasClass('input-group')) {
            parent.parent().append(error);
        } else {
            parent.append(error);
        }
    },
    //highlight: function (element, error, valid) {
    //    if ($(element).hasClass("chosen")) {
    //        $(element).next().css("border", "1px solid red");
    //    } else if ($(element).hasClass("input-group-addon")) {

    //    } else {
    //        $(element).css("border", "1px solid red");
    //    }

    //},
    //unhighlight: function (element) {
    //    if ($(element).hasClass("chosen")) { // input-group-addon
    //        $(element).next().css("border", "1px solid #c3cbd9");
    //    } else if ($(element).hasClass("input-group-addon")) {

    //    } else {
    //        $(element).css("border", "1px solid #c3cbd9");
    //    }
    //},
    success: function (label) { // khi hết lỗi thì làm gì
        label.closest('.form-group').removeClass('has-error');
        label.remove();
    },

});


var HotelInfo = {};
var LstRoomType = [];
var PriceRoomType = [];

$(document).ready(function () {
    // set height
    setHeight($(".hr-content.active"));
    //setup next 
    setUpNext();

    numberMask();

    // clear dữ liệu
    HotelInfo = {};
    LstRoomType = [];
    PriceRoomType = [];

    $("#tab1form").validate();
    $("#tab2form").validate();

    $("#btnCaptcha").click(registerSubmit);

});

//window.addEventListener("beforeunload", function (event) {
//    event.returnValue = "Các thay đổi đã thực hiện sẽ không được lưu";
//    return event.returnValue;
//});

function alert(msg, callback) {
    bootbox.alert({
        title: "Thông báo",
        message: msg,
        callback: function () {
            if (callback) callback.call();
        }
    });
}

function openCapcha() {
    $("#captErr").hide();
    $("#modalCaptcha").on('show.bs.modal', function (e) {
        $("#captcha_box > a").trigger("click");
    })
    $("#modalCaptcha").modal("show");
    $("#CaptchaInputText").change(function () {
        $("#captErr").hide();
    });
    $("#btnSubmit").off().click( function () {
        var dataP = {
            info: HotelInfo,
            roomtype: LstRoomType,
            price: PriceRoomType,
            CaptchaInputText: $("#CaptchaInputText").val(),
            CaptchaInputKey: $("#CaptchaDeText").val(),
        }
        showDialogLoading();
        $.post("/Home/SubmitRegister", dataP)
            .done(function (rs) {
                hideDialogLoading();
                if (rs.IntStatus == -10) {
                    $("#captcha_box > a").trigger("click"); 
                    $("#captErr").show();
                }
                else if (rs.IntStatus == 1) {
                    window.location.href = "/Register/Done"

                } else {
                    $("#captErr").show();
                }


                console.log(rs);
            });
    });

}


function showDialogLoading(msg) {
    if (!msg) msg = "";
    if ($("body > div.ajaxInProgress").length <= 0) {
        var str = '<div class="ajaxInProgress"><div class="loading-ct" >' +
            '<div>' + msg + '</div>' +
            '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' +
            ' </div> </div>';
        $("body").append(str);
    }
    $("body > div.ajaxInProgress").show();
}

function hideDialogLoading() {
    if ($("body > div.ajaxInProgress").length > 0)
        $("body > div.ajaxInProgress").hide();
}

$(window).resize(function () {
    setHeight($(".hr-content.active"))
});

function numberMask() {
    var ua = navigator.userAgent.toLowerCase();
    isAndroid = ua.indexOf("android") > -1;
    if (isAndroid) {
        $("body").find(".number-mask").on().prop('type', 'number');
    } else {
        $("body").find(".number-mask").on().inputmask({
            alias: 'decimal',
            groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
            radixPoint: ",", /* Ký tự phân cách với phần thập phân */
            autoGroup: true,
            digits: 0, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
            autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
            allowMinus: true, /* Không cho nhập dấu trừ */
            allowPlus: false, /* Không cho nhập dấu cộng */
            integerDigits: 16,

        });
    }

}

function setHeight($e) {
    if (!$e) $e = $(".hr-content.active");
    var h = $e.height();
    $("#content_reg").animate({ "min-height": h }, { duration: 500, });
}

// add row thêm mới hạng phòng
function addRow() {

    if (!$("#tab2form").valid()) return;
    var obj = saveRoomType();
    // append html
    var html = bindHtml1RoomType(obj)
    $("#tab2form table #tr_roomtype").before(html);
    // update stt
    updateTableStt();

}

function removeRow(id) {
    if (LstRoomType.length <= 0) return;
    LstRoomType = LstRoomType.filter(function (x) { return x.id != id; });
    $("tr#tr_" + id).remove();
    updateTableStt();
}

function updateTableStt() {
    $("#tab2form table tbody tr").each(function (i, e) { $(e).find("td:eq(0)").html(i + 1); });
}

// chuyển tab
function setUpNext() {
    // next
    $("#btnNext").click(function () {
        if (!$("#tab1form").valid()) return;

        $("#tab2form").validate().resetForm();
        saveInfo();

        activeTab($("#tab1"), $("#tab2"), "left");
        setHeight($("#tab2"));
    });

    $("#btnNext2").click(function () {
        // validate hạng phòng
        if (LstRoomType.length <= 0) {
            $("#tab2form").valid();
            return;
        }

        $("#tab3form").validate().resetForm();
        bindPrice();


        activeTab($("#tab2"), $("#tab3"), "left");
        setHeight($("#tab3"));
    });
    // prev
    $("#btnPrev1").click(function () {
        $("#tab1form").validate().resetForm();
        activeTab($("#tab2"), $("#tab1"), "right");
        setHeight($("#tab1"));
    });

    $("#btnPrev2").click(function () {
        $("#tab2form").validate().resetForm();
        activeTab($("#tab3"), $("#tab2"), "right");
        setHeight($("#tab2"));
    });
}

// tabActive: tab hiện tại
// tabMove: tab chuyển tới
// chuyển sang bên nào
function activeTab($tabMove, $tabActive, move, callback) {
    //showDialogLoading();
    $tabMove.css("position", "absolute").animate(move == 'left' ? { 'left': '-800px' } : { 'left': '800px' }, { duration: 500, });
    $tabActive.css("position", "absolute").animate({ 'left': '0' }, {
        duration: 500,
        complete: function () {
            $(".hr-content").removeClass("active");
            $tabActive.addClass("active");

            if (typeof callback == "function")
                callback();
            $tabActive.css("position", "relative");
            //hideDialogLoading();
        }
    });
}


function registerSubmit() {
    if (!$("#tab3form").valid()) return;
    // save thông tin giá phòng
    savePeice();
    var title = "Xác nhận tạo khách sạn <b>" + HotelInfo.name + "</b> và dùng thử phần mềm trong 1 tháng?";
    openCapcha();
}





function saveInfo() {
    // save thông tin giá phòng
    HotelInfo = {
        name: $("#hotelName").val(),
        email: $("#hotelEmail").val(),
        phone: $("#hotelPhone").val(),
        pass: $("#hotelPass").val(),
        repass: $("#hotelRePass").val(),
        address: $("#hotelAddress").val(),
    };
}

function saveRoomType() {
    // tạo db
    var obj = {
        id: (new Date()).getTime(),
        name: $("#roomType").val(),
        code: $("#roomTypeCode").val(),
        roomQuantity: $("#roomQuantity").val(),
    }
    // push vào data
    LstRoomType.push(obj);
    // reset
    $("#roomType").val("");
    $("#roomTypeCode").val("");
    $("#roomQuantity").val("0");

    return obj;
}
function bindHtml1RoomType(obj) {
    var html = '<tr id="tr_' + obj.id + '">' +
                '<td>1</td>' +
                '<td>' + obj.name + '</td>' +
                '<td>' + obj.code + '</td>' +
                '<td class="text-right">' + obj.roomQuantity + '</td>' +
                '<td class="td-button"><button type="button" onclick="removeRow(' + obj.id + ')" class="btn btn-sm btn-info btn-red">-</button></td>' +
                '</tr>';
    return html;
}

function bindPrice() {
    var html = "";
    for (var i = 0; i < LstRoomType.length; i++) {
        var obj = LstRoomType[i];
        var p = (PriceRoomType[i] && PriceRoomType[i].id == obj.id) ? PriceRoomType[i] : undefined;
        var v1 = 0, v2 = 0, v3 = 0;
        if (p) {
            v1 = p.priceHour;
            v2 = p.priceDay;
            v3 = p.priceNight;
        }
        var ipH = '<input type="text" name="priceHour[' + obj.id + ']" id="priceHour[' + obj.id + ']" class="form-control form-sm number-mask priceHour" min=1 max="99999999999" value="' + v1 + '" autocomplete="off" required>';
        var ipD = '<input type="text" name="priceDay[' + obj.id + ']" id="priceDay[' + obj.id + ']" class="form-control form-sm number-mask priceDay"  min=1 max="99999999999" value="' + v1 + '" autocomplete="off" required>';
        var ipN = '<input type="text" name="priceNight[' + obj.id + ']" id="priceNight[' + obj.id + ']" class="form-control form-sm number-mask priceNight" min=1 max="99999999999" value="' + v1 + '" autocomplete="off" required>';

        html += '<tr id="pr_' + obj.id + '" data-id="' + obj.id + '">' +
                      '<td>' + (i + 1) + '</td>' +
                      '<td>' + obj.name + '</td>' +
                      '<td class="td-button">' + ipH + '</td>' +
                      '<td class="td-button">' + ipD + '</td>' +
                      '<td class="td-button">' + ipN + '</td>' +
                    '</tr>';
    }

    $("#tab3form table tbody").html(html);
    numberMask();
    $("#tab3form").validate();
}
function savePeice() {
    // save thông tin giá phòng
    $("#tab3form table tbody tr").each(function (i, e) {
        var $e = $(e);
        var obj = {
            id: $e.data("id"),
            priceHour: $e.find(".priceHour").val(),
            priceDay: $e.find(".priceDay").val(),
            priceNight: $e.find(".priceNight").val(),
        }
        PriceRoomType.push(obj);
    });
}
