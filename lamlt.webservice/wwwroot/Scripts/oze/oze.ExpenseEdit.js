﻿var ExpenseDetail = [];
var index = 0;
var totalMoney = 0;
function getTotalAmount() {
    var temp = 0;
    $.each(ExpenseDetail, function (index, value) {
        temp += parseFloat(value.amount);
    });
    totalMoney = temp;
    return temp.toString();
}
function removeA(arr, i) {
    arr.splice(i, 1);
    $("#frmExpense input[name=amount]").val(convert2Money(getTotalAmount()));
    return arr;
}

function RemoveItem(el) { 
    Dialog.ConfirmCustom("Xác nhận",
        "Bạn có chắc xóa mục chi này không?",
        function () {
            var elm = $(el).closest('tr');
            var id = elm.data('id');
            elm.remove();
            for (var i = 0; i < ExpenseDetail.length; i++) {
                if (ExpenseDetail[i].index === id)
                    removeA(ExpenseDetail, i);
            }
        });
}

function InitTable(id) {
    $.get("/Expense/GetListExpenseDetailById", { id: id }, function (data) {
        if (data.result != null) {
            ExpenseDetail = data.result;
            getTotalAmount();
            $("#frmExpense input[name=amount]").val(convert2Money(getTotalAmount()));
            index = 0;
            $.each(ExpenseDetail, function (i, value) {
                index++;
                var obj = {};
                obj.Id = value.Id;
                obj.index = index;
                ExpenseDetail[i].index = index;// thêm trường index dùng cho việc xóa
                obj.title = value.title;
                obj.note = value.note;
                obj.amount = value.amount;
                obj.datecreated = value.datecreated;
                obj.expenseid = id; 
                var newRowContent =
                    "<tr data-id='" + obj.index + "'> <td> " +
                        obj.title +
                        " </td> <td> " +
                        convert2Money(obj.amount) +
                        "</td> <td> " +
                        moment(new Date(parseInt(obj.datecreated.slice(6, -2)))).format("DD-MM-YYYY") +
                //moment(new Date()).format("DD-MM-YYYY") +
                " </td> <td> " +
                obj.note +
                " </td>  <td> " +
                "<i class='fa fa-times' onclick='RemoveItem(this)' aria-hidden='true'></i></td>" +
                " </tr>";
                $("#expenseDetail-table tbody").append(newRowContent);
            });
        }
        else {
            alert("Có lỗi khi lấy thông tin chi tiết phiếu chi", 1);
        }
    });
}

var ExpenseEdit = function () {
    var base = this;
    this.$table = $("#table");
    this.ResetForm = function (form) {

        if (form.length) {
            form.find("input, textarea, select")
                .each(function (index) {
                    var input = $(this);
                    if (input.is(":radio, :checkbox")) {
                        input.prop("checked", this.defaultChecked);
                    } else if (input.is("select")) {
                        input.val("-1");
                    } else {
                        input.val("");
                    }
 
                });
        }
         
    };

    this.Save = function () {

        if ($("#frmExpense").valid()) {
            if (ExpenseDetail.length == 0) {
                alert("Vui lòng thêm mới mục chi");
                return;
            }
            Dialog.ConfirmCustom("Xác nhận",
                "Bạn có chắc chắn ghi phiếu chi tiền này?",
                function () {
                    var expenseObj = {};
                    expenseObj.Id = $("#frmExpense input[name=id]").val();
                    expenseObj.code = $("#frmExpense input[name=code]").val();
                    expenseObj.amount = totalMoney;
                    expenseObj.note = $("#frmExpense input[name=note]").val();
                    expenseObj.agencyid = $("#frmExpense input[name=agencyid]").val();

                    Sv.AjaxPost({
                        Url: "/Expense/Update",
                        Data: { obj: expenseObj, listDetail: ExpenseDetail }
                    },
                        function (rs) {
                            console.log(rs);
                            if (rs.result > 0) {
                                if (expenseObj.Id == 0)
                                    Dialog.Alert("Thêm mới thành công",
                                        Dialog.Success,
                                        function () {
                                            location.href = "#/Expense/Index";
                                        });
                                else
                                    Dialog.Alert("Cập nhật thành công",
                                    Dialog.Success,
                                    function () {
                                        location.href = "#/Expense/Index";
                                    });
                                //base.LoadTableSearch();
                            } else {
                                if (expenseObj.Id == 0)
                                    Dialog.Alert("Thêm mới không thành công!", Dialog.Error);
                                else
                                    Dialog.Alert("Cập nhật không thành công!", Dialog.Error);

                            }
                        });
                });
        }
        return;
    };

    this.AddDetail = function () {
        if ($("#frmExpenseDetail").valid()) {
            index++;
            var obj = {};
            obj.Id = 0;
            obj.index = index;
            obj.title = $("#frmExpenseDetail input[name=title]").val();
            obj.note = $("#frmExpenseDetail input[name=note]").val();
            obj.amount = $("#frmExpenseDetail input[name=amount]").val();
            //obj.datecreated = $("#frmExpenseDetail .datecreated").val();
            obj.datecreated = new Date();
            obj.expenseid = 0;
            ExpenseDetail.push(obj);
            $("#frmExpense input[name=amount]").val(convert2Money(getTotalAmount()));
            var newRowContent =
                "<tr data-id='" + index + "'> <td> " +
                    obj.title +
                    " </td> <td> " +
                    convert2Money(obj.amount) +
                    "</td> <td> " +
                    moment(new Date()).format("DD-MM-YYYY") +
                    " </td> <td> " +
                    obj.note +
                    " </td>  <td> " +
                    "<i class='fa fa-times' onclick='RemoveItem(this)' aria-hidden='true'></i></td>" +
                    " </tr>";
            $("#expenseDetail-table tbody").append(newRowContent);
            base.ResetForm($("#frmExpenseDetail"));
        } else {
            return;
        }

    };
    this.SetupBlur = function setUpBlur() {
        $("#div_Amount").focusout(function () { 
            $("#div_Amount").html(MoneyToString($("#frmExpenseDetail input[name=amount]").val()));
        });
    }
};
$(document)
    .ready(function () {
        var unit = new ExpenseEdit();
        var id = $("#frmExpense input[name=id]").val();
        unit.SetupBlur();
        if (id != 0)
            InitTable(id);
        $("#btnOk")
            .click(function () {
                unit.AddDetail();
            });
        $("#btnSave").click(function () {
            unit.Save();
        });
    });