﻿var register = {
    // load Handlebars
    loadHandlebars: function (parentAppendId, handlebarId, data) {
        return new Promise(function (resolve, reject) {
            var handlebarHtml = $(handlebarId).html();
            var template = Handlebars.compile(handlebarHtml);
            var titleHtml = template(data);
            $(parentAppendId).append(titleHtml);
            resolve();
        });
    },
    // set width của cate
    setWidthHeight: function () {
        var cw = $('.cate-item').width();
        $('.cate-item').css({
            'height': cw + 'px'
        });
    },

    openCapcha: function (callback) {
        $("#modalCaptcha").off().on('show.bs.modal', function (e) {
            if (!$("#CaptchaInputText").hasClass("form-control")) {
                $("#CaptchaInputText").addClass("form-control");
            }
            $("#captErr").html("");
            $("#CaptchaInputText").val("");
            $("#CaptchaInputText").keyup(function () {
                $("#captErr").html(register.captchaCheck());
            });

            $("#captcha_box > a").trigger("click");
        }).modal("show");

        $("#btnSubmit").off().click(function () {
            var dataP = {
                CaptchaInputText: $("#CaptchaInputText").val(),
                CaptchaInputKey: $("#CaptchaDeText").val(),
            }
            var ck = register.captchaCheck();
            $("#captErr").html(ck);
            if (ck.length == 0)
                callback(dataP);
        });
    },

    captchaCheck: function () {
        var captcha = $("#CaptchaInputText").val();
        if (!captcha || captcha.length == 0)
            return ("Mã Captcha không chính xác");
        if (captcha.length != 5)
            return ("Mã Captcha không chính xác");
        return ("");
    },

    captchaSowErr: function (mgs) {
        $("#captErr").html(mgs);
    },

    step1: function () {
        this
            .loadCate({})
            .then(function () {
                register.setWidthHeight();
                $(window).resize(function () { register.setWidthHeight(); });
                $('.cate-item').click(function (e) {
                    var json = $(this).data("json");
                    console.log(json);
                    if (json.Code.toLowerCase() === "hotel")
                    {
                        window.location.href = "/dang-ky-ks"
                    }
                    else {
                        window.SystemTypeInfo = json;
                        register.step2();
                    }
                });
            });
    },
    step2: function () {
        this
            .rotelLoad({ type: window.SystemTypeInfo })
            .then(function () {
                //$("#district").select2();
                // setup validate
                var roValid = register.rotelValidate();

                // set user name
                $('#email').keyup(function () {
                    register.setInputData("#email", "#username");
                   // roValid.element("#username");
                });
                $("#email").blur(function () {
                    roValid.element("#username");
                });

                // submit
                $('#rotelStep2').click(function () {
                    var valid = $("#div_rotel").valid();
                    if (!valid) return;
                    register.rotelSubmit();
                });

                // back
                $('#back').click(function () {
                    register.step1();
                });
            });
    },

    // load category html
    loadCate: function (data) {
        $("#register-content").empty();
        return this.loadHandlebars("#register-content", "#handlabars_category", data);
    },


    // load info html
    rotelLoad: function (data) {
        $("#register-content").empty();
        return this.loadHandlebars("#register-content", "#handlabars_rotel", data);
    },

    rotelValidate: function () {
        return $("#div_rotel").validate({
            rules: {
                fullName: {
                    maxlength: 200,
                    required: true
                },
                email: {
                    //required: true,
                    email: true
                },
                phone: {
                    required: true,
                    phonenumber: true
                },
                username: {
                    minlength: 3,
                    username: true,
                    maxlength: 200,
                    required: true,
                },
                pass: {
                    required: true,
                    minlength: 4,
                    maxlength: 100,
                },
                repass: {
                    required: true,
                    equalTo: "#pass"
                },
                sysname: {
                    minlength: 3,
                    maxlength: 200,
                    required: true
                },
            },
            //messages: {

            //}
        });
    },

    rotelSubmit: function () {
        this.openCapcha(function (data) {
            data.obj = getFormData($("#div_rotel"));
            data.systype = window.SystemTypeInfo;
            console.log(data)
            Sv.Post({ url: "/Register/Submit", data: data })
                .then(function (rs) {
                    if (rs.IntStatus == -10) {
                        $("#captcha_box > a").trigger("click");
                        register.captchaSowErr(rs.Message);
                        return;
                    } else if (rs.IntStatus == 1) {
                        window.location.href = "/dang-ky-thanh-cong";
                    } else {
                        alert(rs.Message);
                    }
                    console.log(rs);
                });
        });
    },

    setInputData: function (e, u) {
        $(u).val($(e).val());
    },




}

$(function () {

    if (!window.SystemTypeInfo || window.SystemTypeInfo.Id == 0) {
        register.step1();
    } else {
        // step 2 
        register.step2();
    }

});

