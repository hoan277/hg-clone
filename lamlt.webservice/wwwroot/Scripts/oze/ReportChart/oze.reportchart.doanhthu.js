﻿var chartDatPhong = function () {
    var _this = this;

    this.FromDate = $("[name='dpDateFrom']");
    this.ToDate = $("[name='dpDateTo']");
    this.datetype = $("[name='datetype']");
    this.btnSearch = $("#btnSearch");
    this.barElement = $("#barChart");

    // change thời gian
    _this.OnChangeDateType = function () {
        // _this.datetype.on("change", _this.ChangeDateType);
    };

    _this.OnSearch = function () {
        _this.btnSearch.on("click", _this.Search);
    };

    // change date set up date
    _this.ChangeDateType = function () {
        var type = _this.datetype.val();
        switch (type) {
            case "day":
                _this.SetUpdateDay();
                break;
            case "week":
                _this.SetUpdateWeek();
                break;
            case "month":
                _this.SetUpdateMonth();
                break;
            case "quarter":
                _this.SetUpdateQuarter();
                break;
            case "year":
                _this.SetUpdateYear();
                break;
            default:
                bootboxLamlt("Không xác định được thời gian");
                break;
        }
    };
    // setup date day
    _this.SetUpdateDay = function () {

        _this.FromDate.datetimepicker({
            format: "DD/MM/YYYY",
            viewMode: 'days',
            defaultDate: Sv.AddDays(-29),
            showTodayButton: true,
            showClose: true,
            sideBySide: true,
            showClear: true,
        });
        _this.ToDate.datetimepicker({
            format: "DD/MM/YYYY",
            viewMode: 'days',
            defaultDate: Sv.AddDays(0),
            showTodayButton: true,
            showClose: true,
            showClear: true,
        });
    };
    _this.SetUpdateWeek = function () { };
    _this.SetUpdateMonth = function () { };
    _this.SetUpdateQuarter = function () { };
    _this.SetUpdateYear = function () { };

    // search
    _this.Search = function () {

        var type = _this.datetype.val();
        var d1 = _this.FromDate.val();
        var d2 = _this.ToDate.val();
        var typeReport = 1;
        switch (type) {
            case "day":
                typeReport = 1;
                break;
            case "week":
                typeReport = 2;
                break;
            case "month":
                typeReport = 3;
                break;
            case "quarter":
                typeReport = 4;
                break;
            case "year":
                typeReport = 5;
                break;
            default:
                bootboxLamlt("Không xác định được thời gian");
                return;
                break;
        }

        _this.AjaxPost("/ReportChart/DoanhThuReport", { fromDate: d1, toDate: d2, type: typeReport });
    }
    // ajax search
    _this.AjaxPost = function (url, data) {
        Sv.AjaxPost({
            url: url,
            data: data
        }, function (respon) {
            if (respon.Status == "1") {
                _this.ChartUpdate(respon.Data);
            } else {
                bootboxLamlt.alert(respon.Message);
            }
        });
    };



    //-------------
    //- BAR CHART -
    //-------------
    _this.barChartOptions = {
        title: {
            display: true,
            text: "Thống kê: doanh thu - số lượt nghỉ"
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    display: false
                },
                scaleLabel: {
                    display: false,
                    labelString: 'Thời gian'
                }
            }],
            yAxes: [{
                        type: "linear",
                        display: true,
                        position: "left",
                        id: "y-axis-1",
                        gridLines: {
                            display: true,
                            color: "rgba(0, 114, 210, 0.5)"
                        },
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Doanh thu'
                        }
                    }, {
                        type: "linear",
                        display: true,
                        position: "right",
                        id: "y-axis-2",
                        gridLines: {
                            display: true,
                            color: "rgba(242, 125, 46, 0.5)",                            
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Số lượt nghỉ'
                        },
                        ticks: {
                            beginAtZero: true
                        },
                    },
            ]
        },
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
            label: function (tooltipItem, data) {
                var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                return "longld" + datasetLabel + ': ' + tooltipItem.xLabel;
            }
        }, 
    };
    _this.barChartCanvas = document.getElementById("barChart").getContext("2d");
    _this.barChart = undefined;
    _this.barChartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            type: 'line',
            label: "Doanh thu",
            data: [200, 185, 590, 621, 250, 400, 95],
            fill: false,
            backgroundColor: '#0072d2',
            borderColor: '#0072d2',
            hoverBackgroundColor: '#0072d2',
            hoverBorderColor: '#0072d2',
            yAxisID: 'y-axis-1'
        }, {
            type: 'bar',
            label: "Số lượt nghỉ",
            data: [0, 0, 0, 0, 0, 0, 0],            
            borderColor: '#f27d2e',
            backgroundColor: '#f27d2e',
            pointBorderColor: '#f27d2e',
            pointBackgroundColor: '#f27d2e',
            pointHoverBackgroundColor: '#f27d2e',
            pointHoverBorderColor: '#f27d2e',
            yAxisID: 'y-axis-2'
        }]
    };

    _this.ChartUpdate = function (data) {
        if (_this.barChart !== undefined) {
            _this.barChart.destroy();
        }
        _this.barChartData.labels = data.labels;
        _this.barChartData.datasets[0].data = data.datasets[0].data;
        _this.barChartData.datasets[1].data = data.datasets[1].data;

        _this.barChart = new Chart(_this.barChartCanvas, {
            type: 'bar',
            data: _this.barChartData,
            options: _this.barChartOptions,
        });
    };

    // init
    _this.Init = function () {
        _this.OnChangeDateType();
        _this.OnSearch();
    };
}

$(document).ready(function () {
    var ctrl = new chartDatPhong();
    ctrl.SetUpdateDay();
    ctrl.Init();
});
