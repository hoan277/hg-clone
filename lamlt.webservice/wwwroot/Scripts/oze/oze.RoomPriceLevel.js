﻿
var $modalMail = $("#modalDetails");

$(document).ready(function () {
    
  
    // thêm mới
    $("#btnAdd").click(function () { modalOpen("/RoomPriceLevel/ViewInsert"); });
    // lưu
    $modalMail.on('click', '#btnUAddDetail', function (e) {
        var $form = $("#formDetail_f1");
        if ($form.valid()) {
            var url = "/RoomPriceLevel/InsertOrUpdate";
            dataPost = getFromData($form);
            Sv.Post({
                url: url,
                data: dataPost
            }, function (data) {
                if (data.result) {
                    bootbox.alert(data.Message,
                        function () {
                            searchGrid();
                            $("#modalDetails").modal("hide");
                        });
                } else {
                    alert(data.Message);
                }
            });
        }
    });

    //giá theo  giờ 
    $modalMail.on('click', '#addpricemain', function (e) {
        addTienGio($(this), $(".pricebytime"), function ($b, c) {
            $b.append(HtmlPhuTroi(e, {
                class: 'pricebytimeitem' + c, label: "Giá:", function: undefined, value: c + 1
            },
                                    { id: 'txtGio' + c, name: 'txtGio' + c, addon: 'H' },
                                    { id: 'txtGiatheogio' + c, name: 'txtGiatheogio' + c, addon: "VND" }));
        });
    });
    //phụ trội quá giờ theo trả ngày
    $modalMail.on('click', '#addptqgtn', function (e) {
        addPhuTroi($(this), $(".phutroigiotheongay"), function ($b, c) {
            $b.append(HtmlPhuTroi(e,
                                { class: 'itemptgtn' + c, label: "Quá:", function: undefined, value: c + 1 },
                                { id: 'txtGioquatheongay' + c, name: 'txtGioquatheongay' + c, addon: 'H' },
                                { id: 'txtGiaquatheongay' + c, name: 'txtGiaquatheongay' + c, addon: [{ v: 0, n: "VND" }, { v: 1, n: "%" }] }))
        });
    });
    //pPhụ trội quá giờ trả theo đêm
    $modalMail.on('click', '#addlatenight', function (e) {
        addPhuTroi($(this), $(".phutroigiotheodem"), function ($b, c) {
            $b.append(HtmlPhuTroi(e, {
                class: 'itemptgtnnight' + c, label: "Giá:", function: undefined, value: c + 1
            },
                                { id: 'txtGioquatheodem' + c, name: 'txtGioquatheodem' + c, addon: 'H' },
                                { id: 'txtGiaquatheodem' + c, name: 'txtGiaquatheodem' + c, addon: [{ v: 0, n: "VND" }, { v: 1, n: "%" }] }))
        });
    });
    //Phụ trội nhận phòng sớm theo ngày
    $modalMail.on('click', '#addEarlyDay', function (e) {
        addPhuTroi($(this), $(".EarlyDay"), function ($b, c) {
            $b.append(HtmlPhuTroi(e,
                                { class: 'EarlyDayItem' + c, label: "Sớm:", function: undefined, value: c + 1 },
                                { id: 'txtGiosomtheongay' + c, name: 'txtGiosomtheongay' + c, addon: 'H' },
                                { id: 'txtGiasomtheongay' + c, name: 'txtGiasomtheongay' + c, addon: [{ v: 0, n: "VND" }, { v: 1, n: "%" }] }));
        });
    });
    //Phụ trội nhận phòng sớm theođêm
    $modalMail.on('click', '#addEarlyNight', function (e) {
        addPhuTroi($(this), $(".EarlyNight"), function ($b, c) {
            $b.append(HtmlPhuTroi(e,
                                { class: 'EarlyNightItem' + c, label: "Sớm:", function: undefined, value: c + 1 },
                                { id: 'txtGiosomquadem' + c, name: 'txtGiosomquadem' + c, addon: 'H' },
                                { id: 'txtGiasomquadem' + c, name: 'txtGiasomquadem' + c, addon: [{ v: 0, n: "VND" }, { v: 1, n: "%" }] }));
        });
    });
    //Quá số người
    $modalMail.on('click', '#AddLimitPerson', function (e) {
        addPhuTroi($(this), $(".LimitPerson"), function ($b, c) {
            $b.append(HtmlPhuTroi(e,
                                { class: 'LimitPersonItem' + c, itemNum: 10, label: "Quá:", function: undefined, value: c + 1 },
                                { id: 'txtsonguoi' + c, name: 'txtsonguoi' + c, addon: 'N' },
                                { id: 'txtGiasonguoi' + c, name: 'txtGiasonguoi' + c, addon: [{ v: 0, n: "VND" }, { v: 1, n: "%" }] }));
        });
    });
    //phị trội trẻ em
    $modalMail.on('click', '#AddLimitPerson_Child', function (e) {
        addPhuTroi($(this), $(".LimitPerson_Child"), function ($b, c) {
            $b.append(HtmlPhuTroi(e,
                                { class: 'LimitPersonItem_Child' + c, itemNum: 10, label: "Quá:", function: undefined, value: c + 1 },
                                { id: 'txt_NumberChild' + c, name: 'txt_NumberChild' + c, addon: 'N' },
                                { id: 'txt_Child' + c, name: 'txt_Child' + c, addon: [{ v: 0, n: "VND" }, { v: 1, n: "%" }] }));
        });
    });

    // show/ hide phu trội, giá
    $modalMail.on("click", '.elem-toggle', function (e) {
        showboxpt($(this), null);
    });


});


function getFromData($form) {
    var listPriceDay = [],
        listXtraDay = [],
        listXtraNight = [],
        listEarlyDay = [],
        listEarlyNight = [],
        listLimitPerson = [],
        listLimitPerson_Child = [];
    // phụ trội listPriceDay
    $(".pricebytime").find(".item-pt").each(function () { listPriceDay.push(getPhuTroiVale($(this))); });
    //Phuj trội theo ngày listXtraDay
    $(".phutroigiotheongay").find(".item-pt").each(function () { listXtraDay.push(getPhuTroiVale($(this))); });
    // phụ trội theo đêm listXtraNight
    $(".phutroigiotheodem").find(".item-pt").each(function () { listXtraNight.push(getPhuTroiVale($(this))); });
    // trả phòng sớm theo ngày listEarlyDay
    $(".EarlyDay").find(".item-pt").each(function () { listEarlyDay.push(getPhuTroiVale($(this))); });
    // trả phòng sớm theo đêm listEarlyNight
    $(".EarlyNight").find(".item-pt").each(function () { listEarlyNight.push(getPhuTroiVale($(this))); });
    // Phụ trội quá số lượng người lớn listLimitPerson
    $(".LimitPerson").find(".item-pt").each(function () { listLimitPerson.push(getPhuTroiVale($(this))); });
    // Phu trội trẻ em listLimitPerson_Child
    $(".LimitPerson_Child").find(".item-pt").each(function () { listLimitPerson_Child.push(getPhuTroiVale($(this))); });

    var t2 = $("#formDetail_f1  [name='T2']")[0].checked;
    var t3 = $("#formDetail_f1  [name='T3']")[0].checked;
    var t4 = $("#formDetail_f1  [name='T4']")[0].checked;
    var t5 = $("#formDetail_f1  [name='T5']")[0].checked;
    var t6 = $("#formDetail_f1  [name='T6']")[0].checked;
    var t7 = $("#formDetail_f1  [name='T7']")[0].checked;
    var cn = $("#formDetail_f1  [name='CN']")[0].checked;

    var dayOfWeeks = "";
    //dkm concat ko chay phai dung +=
    if (t2) { dayOfWeeks = "2"; }
    if (t3) { if (dayOfWeeks === "") dayOfWeeks += "3"; else { dayOfWeeks += "-3"; } }
    if (t4) { if (dayOfWeeks === "") dayOfWeeks += "4"; else { dayOfWeeks += "-4"; } }
    if (t5) { if (dayOfWeeks === "") dayOfWeeks += "5"; else { dayOfWeeks += "-5"; } }
    if (t6) { if (dayOfWeeks === "") dayOfWeeks += "6"; else { dayOfWeeks += "-6"; } }
    if (t7) { if (dayOfWeeks === "") dayOfWeeks += "7"; else { dayOfWeeks += "-7"; } }
    if (cn) { if (dayOfWeeks === "") dayOfWeeks += "8"; else { dayOfWeeks += "-8"; } }
    var model = {
        Id: $("#formDetail_f1  [name='Id']").val(),
        SysHotelID: $('select[name="ListHotel"]').val(),
        RoomTypeID: $('select[name="ListRoomType"]').val(),
        Twin: $("#formDetail_f1  [name='twin']").val(),
        title: $("#formDetail_f1  [name='txtTitle']").val(),
        _Single: $("#formDetail_f1  [name='single']").val(),

        Number_Adult: $("#formDetail_f1  [name='txtNumber_Adult']").val(),
        PriceNight: $("#formDetail_f1  [name='txtGiaquadem']").val(),
        PriceDay: $("#formDetail_f1  [name='txtGiatheongay']").val(),
        PriceMonth: $("#formDetail_f1  [name='txtGiatheothang']").val(),
        CreateID: $("#formDetail_f1  [name='txtSonguoilon']").val(),
        Note: $("#formDetail_f1  [name='txtLuuy']").val(),
        dayOfWeeks: dayOfWeeks,
        hoursFrom: $("#formDetail_f1  [name='hoursFrom']").val(),
        hoursTo: $("#formDetail_f1  [name='hoursTo']").val(),
    }
    var fDate = $("#formDetail_f1  [name='Formdate']").val();
    var tDate = $("#formDetail_f1  [name='Todate']").val();

    return dataPost = {
        fDate: fDate,
        tDate: tDate,
        listXtraDay: listXtraDay,
        listPriceDay: listPriceDay,
        listXtraNight: listXtraNight,
        listEarlyDay: listEarlyDay,
        listEarlyNight: listEarlyNight,
        listLimitPerson: listLimitPerson,
        listLimitPerson_Child: listLimitPerson_Child,
        model: model
    };
}

function getPhuTroiVale($p) {
    var item = {};
    // type 
    if ($p.parent().hasClass("phutroigiotheongay")) {
        item.typeExtra = 1;
    } else if ($p.parent().hasClass("phutroigiotheodem")) {
        item.typeExtra = 2;
    } else if ($p.parent().hasClass("EarlyDay")) {
        item.typeExtra = 3;
    } else if ($p.parent().hasClass("EarlyNight")) {
        item.typeExtra = 4;
    } else if ($p.parent().hasClass("LimitPerson")) {
        item.typeExtra = 5;
    } else if ($p.parent().hasClass("LimitPerson_Child")) {
        item.typeExtra = 6;
    }

    item.numberHours = parseInt($p.find(".number-check").val());
    item.numberExtra = parseInt($p.find(".number-check").val());

    item.price = parseFloat($p.find(".amount-check").val());
    item.priceExtra = parseFloat($p.find(".amount-check").val());

    if ($p.find(".type-check").length > 0)
        item.typeValue = parseInt($p.find(".type-check").val());
    else
        item.typeValue = 0;
    item.Id = $p.attr("accept");
    return item;
}

function addPhuTroi($this, $box, callback) {
    showboxpt($this, true);

    var countItem = $box.find(".item-pt").length;
    if (countItem >= maxRowsPt) {
        alert("Cấu hình: Số giờ tối đa tính phụ trội " + maxRowsPt + " giờ");
        return;
    }
    if (typeof callback === 'function') {
        callback($box, countItem);
    }

    OneSetAmount($box.find(".amount-double-mask"));
    countPhuTroi();
}

function addTienGio($this, $box, callback) {
    showboxpt($this, true);

    var countItem = $box.find(".item-pt").length;
    if (countItem >= maxRowsGio) {
        alert("Cấu hình: Số giờ tối đa cho phép tính tiền theo giờ  " + maxRowsGio + " giờ");
        return;
    }
    if (typeof callback === 'function') {
        callback($box, countItem);
    }

    OneSetAmount($box.find(".amount-double-mask"));
    countPhuTroi();
}

// copy phụ trội
function setupCopyPhuTroi() {
    bindHtmlButonCopy();
    setEvent();
}

function bindHtmlButonCopy() {
    var $p = $(".phutroi").parent();
    //if ($p.hasClass("no-copy")) return;
    $p.each(function (i) {
        var $this = $(this);
        // kiểm tra length
        var itempt = $this.parent().next().find(".item-pt");
        //console.log($this, itempt);
        if (itempt.length <= 0) {
            // remove
            var ckIt = $this.find(".coppyPt");
            if (ckIt.length > 0) {
                $this.find(".coppyPt").remove();
            }
            return;
        };

        var ckIt = $this.find(".coppyPt");
        // check đã có
        if (ckIt.length > 0) return;

        // kiểm tra ko thêm copy
        if ($this.hasClass("no-copy")) return;

        //var htm = '<label class="input-group-addon"><i class="fa fa-copy phutroi-addon coppyPt" ></i></label>';
        var htm = '<div class="coppyPt dropdown input-group-addon">' +
                    '<div class=" dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">' +
                        '<i class="fa fa-copy phutroi-addon " ></i>' +
                        '<span class="caret"></span>' +
                    '</div>' +
                    '<ul class="dropdown-menu" role="menu" aria-labelledby="menuCopy">' +
                        '<li role="presentation"><a class="a-no-href" role="menuitem" tabindex="-1" data-toelement=".phutroigiotheongay">Copy sang - Phụ trội quá giờ trả theo ngày</a></li>' +
                        '<li role="presentation"><a class="a-no-href" role="menuitem" tabindex="-1" data-toelement=".phutroigiotheodem" >Copy sang - Phụ trội quá giờ trả theo đêm</a></li>' +
                        '<li role="presentation"><a class="a-no-href" role="menuitem" tabindex="-1" data-toelement=".EarlyDay" >Copy sang - Phụ trội nhận phòng sớm theo ngày</a></li>' +
                        '<li role="presentation"><a class="a-no-href" role="menuitem" tabindex="-1" data-toelement=".EarlyNight" >Copy sang - Phụ trội nhận phòng sớm theo đêm</a></li>' +
                        '<li role="presentation"><a class="a-no-href" role="menuitem" tabindex="-1" data-toelement=".LimitPerson" >Copy sang - Phụ trội quá số lượng người lớn</a></li>' +
                        '<li role="presentation"><a class="a-no-href" role="menuitem" tabindex="-1" data-toelement=".LimitPerson_Child" >Copy sang - Phụ trội trẻ em</a></li>' +
                    '</ul>' +
                  '</div>';
        $(htm).insertAfter($this.find(".phutroi"));
        //$this.append(htm);

    });
}

function setEvent() {
    $(".coppyPt").find("[role='menuitem']").off().on("click", function (e) {
        console.log(e);
        var $e = $(e.target);
        var $f = $e.parents(".form-group").next();
        var toE = $e.data("toelement");
        var $t = $(toE);
        var data = [];
        $f.find(".item-pt").each(function (index1, elm1) {
            var $e1 = $(elm1);
            var obj = {
                gio: $e1.find(".number-check").val(),
                value: $e1.find(".amount-check").val(),
                type: $e1.find(".type-check").val(),
            };
            data.push(obj);
        });
        if (data.length <= 0) {
            alert("Không thể copy phụ trội khi không có dữ liệu!");
        }
        var html = "";
        for (var i = 0; i < data.length; i++) {
            html += bindHtmlCopy(e, toE, data[i]);
        }
        $t.html(html);
        // update html khi đã có phụ trội
        bindHtmlButonCopy();
        setEvent();

        OneSetAmount($t.find(".amount-double-mask"));
        countPhuTroi();

    })


}

function bindHtmlCopy(e, toE, item, i) {
    // child
    //data.e = e;
    var option1 = {
        value: item.gio
    };
    var i1 = {

        addon: "H"
    };
    var i2 = {
        value: item.value,
        addonvalue: item.type,
        addon: [{ v: 0, n: "VND" }, { v: 1, n: "%" }],
    };
    switch (toE) {
        // Phụ trội quá giờ trả theo ngày
        case ".phutroigiotheongay":
            option1.class = "itemptgtn" + i;
            option1.label = "Quá";

            i1.id = "txtGioquatheongay" + i;
            i1.name = "txtGioquatheongay" + i;
            i2.id = "txtGiaquatheongay" + i;
            i2.name = "txtGiaquatheongay" + i;

            break;
            // Phụ trội quá giờ trả theo đêm 
        case ".phutroigiotheodem":
            option1.class = "itemptgtnnight" + i;
            option1.label = "Quá";

            i1.id = "txtGioquatheodem" + i;
            i1.name = "txtGioquatheodem" + i;
            i2.id = "txtGiaquatheodem" + i;
            i2.name = "txtGiaquatheodem" + i;

            break;
            // Phụ trội nhận phòng sớm theo ngày 
        case ".EarlyDay":
            option1.class = "EarlyDayItem" + i;
            option1.label = "Sớm";

            i1.id = "txtGiosomtheongay" + i;
            i1.name = "txtGiosomtheongay" + i;
            i2.id = "txtGiasomtheongay" + i;
            i2.name = "txtGiasomtheongay" + i;

            break;
            // Phụ trội nhận phòng sớm theo đêm 
        case ".EarlyNight":
            option1.class = "EarlyNightItem" + i;
            option1.label = "Sớm";

            i1.id = "txtGiosomquadem" + i;
            i1.name = "txtGiosomquadem" + i;
            i2.id = "txtGiasomquadem" + i;
            i2.name = "txtGiasomquadem" + i;

            break;
            // Phụ trội quá số lượng người lớn 
        case ".LimitPerson":
            option1.class = "LimitPersonItem" + i;
            option1.label = "Quá";

            i1.id = "txtsonguoi" + i;
            i1.name = "txtsonguoi" + i;
            i2.id = "txtGiasonguoi" + i;
            i2.name = "txtGiasonguoi" + i;

            i1.addon = "N";

            break;
            //Phụ trội trẻ em  case ".phutroigiotheongay":
        case ".LimitPerson_Child":
            option1.class = "LimitPersonItem_Child" + i;
            option1.label = "Quá";

            i1.id = "txt_NumberChild" + i;
            i1.name = "txt_NumberChild" + i;
            i2.id = "txt_Child" + i;
            i2.name = "txt_Child" + i;

            i1.addon = "N";

            break;
        default:
            break;

    }
    return HtmlPhuTroi(e, option1, i1, i2);

}

// đếm số item trong 1 phụ trội
function countPhuTroi() {
    setupCopyPhuTroi();
    $(".elem-toggle").each(function (i) {
        countItem($(this));
    });
}

function hideAll() {
    $(".elem-toggle").each(function (i) {
        var $p = $(this);
        var box = $p.parents(".form-group").next();
        box.hide();
        countItem($p);
    });
}

function countItem($e) {
    var $box = $e.parents(".form-group").next();
    var count = $box.children().length;
    var text = $e.text().split('(');
    var isShow = $box.is(":visible");
    var str = text[0] += " (" + count + ")";
    // show
    if (isShow || count == 0) {
        str += " <span class='iconphutroi'><i class='fa fa-angle-down'></i></span>";
    }
        // hide
    else {
        str += " <span class='iconphutroi'><i class='fa fa-angle-up'></i></span>";
    }
    $e.html(str);
}

// show/ hidden list phụ trội item
function showboxpt($p, ck) {

    var box = $p.parents(".form-group").next();
    var show = box.is(":visible");
    if (ck == null) {
        if (show) {
            box.slideUp(300);
        } else {
            box.slideDown(300);
        }
    } else {
        if (ck = true)
            box.slideDown(300);
        if (ck = false)
            box.slideUp(300);
    }
}

function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}

// modal open 
function modalOpen(url, dataPost, callback) {
    $("#modalDetails").off('show.bs.modal');
    $("#modalDetails").on('show.bs.modal', function () {
        $("#modalDetails .modal-body-content").html('<p>loading..</p>');
        $("#modalDetails button#btnUAddDetail").css("display", "none");
        $.post(url, dataPost, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            $("body").addClass("modal-open1");
            OneSetAmount($(".amount-double-mask"));
            $("body").addClass("modal-open1");
            g_Utils.SetDateDefaultAdd();
            $("#TittleBox").html("Thêm mới giá theo phòng");
            countPhuTroi();
            if (typeof callback === "function") {
                callback();
            } else {
                $("#modalDetails button#btnUAddDetail").css("display", "inline");
            }
        });
    });
    $("#modalDetails").modal("show");
}
//view cập nhật thông tin khách hàng
function viewDetailDialog(id) {
  
    modalOpen("/RoomPriceLevel/GetEditDetail", { id: id }, function () {
        $("#TittleBox").html("Xem thông tin giá phòng");
        hideAll();
        $("#modalDetails button#btnUAddDetail").css("display", "none");
        $("#modalDetails #formDetail_f1 input").attr("disabled", true);
        $("#modalDetails #formDetail_f1 textarea").attr("disabled", true);
        $("#modalDetails #formDetail_f1 select").prop("disabled", true);
        $("#modalDetails i.fa").removeAttr("id");
        $("#modalDetails i.fa").removeAttr("onclick");
    }); 
}

function viewEditlDialog(id) {
    modalOpen("/RoomPriceLevel/GetEditDetail", { id: id }, function () {
        $("#modalDetails button#btnUAddDetail").css("display", "inline");
        $("#TittleBox").html("Chỉnh sửa giá theo phòng");
        OneSetAmount($(".amount-double-mask"));
        g_Utils.SetDateDefaultAdd();
        hideAll();
    });
    //$("#modalDetails").off('show.bs.modal');
    //$("#modalDetails").on('show.bs.modal', function () {
    //    $("#TittleBox").html("Sửa giá theo phòng");
    //    //RuleValidateSubmitToAdd();
    //    $("#modalDetails .modal-body-content").html('<p>loading..</p>');
    //    $.post("/RoomPriceLevel/GetEditDetail", { id: id }, function (rs) {
    //        $("#modalDetails .modal-body-content").html(rs);
    //        $("body").addClass("modal-open1");
    //        $("#modalDetails button#btnUAddDetail").css("display", "inline");

    //        OneSetAmount($(".amount-double-mask"));
    //        g_Utils.SetDateDefaultAdd();
    //        countPhuTroi();
    //    });

    //    $("#btnUpdateDetail").off("click");
    //});
    //$("#modalDetails").modal("show");
}

function DeleteDialog(id) {
    Dialog.ConfirmCustom("Xác nhận xóa", "Bạn có chắc muốn xóa giá này?", function () {
        Sv.Post({
            url: "/RoomPriceLevel/Delete",
            data: { id: id }
        }, function (data) {
            if (data.result > 0) {
                bootbox.alert("Xóa giá thành công.", function () {
                    searchGrid();
                });
            }
            else {
                alert("Lỗi trong quá trình xử lý!");
            }
        });
    });

}

function HtmlPhuTroi(e, option, i1, i2) {

    var optionSelect = "";
    var max = option.itemNum != undefined ? option.itemNum : 24;
    // 1H, 2H,...24H
    for (var i = 1; i <= max; i++) {
        var sl1 = (option.value != undefined && option.value == i) ? "selected" : "";
        optionSelect += '<option value="' + i + '" ' + sl1 + ' >' + i + '</option>'
    }
    var iClass = "";
    var addonHtml = "";

    // addon value 
    if (!i2.addonvalue)
        i2.addonvalue = 0;
    // VND - %
    if (typeof i2.addon === 'string') {
        addonHtml = '<span class="input-group-addon addon-2">' + i2.addon + '</span>';
    } else if (typeof i2.addon === 'object') {

        var $p = $(e.target).parents(".form-group").next();
        var $lastChild = $p.find(".item-pt:last-child");
        var typeV = (i2.addonvalue != undefined) ? i2.addonvalue : $lastChild.find(".type-check").val();

        addonHtml = '<select class="form-control type-check select-addon1" name="' + i2.name + '_type" >';
        for (var i = 0; i < i2.addon.length; i++) {
            var sl = i2.addon[i].v == typeV ? "selected" : "";
            addonHtml += '<option value="' + i2.addon[i].v + '" ' + sl + ' >' + i2.addon[i].n + '</option>';
        }
        addonHtml += '</select>';
        iClass = "input-addon1";
    }
    if (!i2.value)
        i2.value = 0;

    // html
    var html = '<div class="item-pt ' + option.class + '" accept="0">' +
                    '<div class="form-group">' +
                        '<div class=" col-sm-5" style="display: flex;">' +
                            '<div class="col-sm-12 mp box-phutroi">' +
                                '<div class="input-group"> ' +
                                    '<span class="input-group-addon addon-label">' + option.label + '</span> ' +
                                    '<select class="form-control number-check" required="1" id="' + i1.id + '" name="' + i1.name + '">' +
                                    optionSelect +
                                    '</select>' +
                                    '<span class="input-group-addon addon-1">' + i1.addon + '</span> ' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-sm-7" style="display: flex; padding-left: 0px; padding-right: 0px;"> ' +
                            '<div class="col-sm-12 mp box-phutroi">' +
                                    '<div class="input-group"> ' +
                                        '<input class="form-control amount-check amount-double-mask ' + iClass + '" required="1" id="' + i2.id + '" name="' + i2.name + '" placeholder="0" min="0" value="' + i2.value + '">' +
                                        addonHtml +
                                        '<span class="input-group-addon"><i class="fa fa-remove phutroi-addon" onclick="' + (option.function != undefined ? option.function : "removeitem(this)") + '"></i></span>' +
                                    '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
    return html;
}

function removeitem(e) {
    $(e).parents(".item-pt").remove();

    countPhuTroi();
}

function OneSetAmount($e) {
    if (!Sv.isAndroid()) {
        $e.on().inputmask({
            alias: 'decimal',
            placeholder: '',
            groupSeparator: '.',
            radixPoint: ',',
            autoGroup: true,
            digits: 2,
            allowPlus: false,
            allowMinus: false,
            autoUnmask: true,
            integerDigits: 11,
            min: 0,
            max: 999999999,
        });
    } else {
        $e.on().prop('type', 'number');
    }
};

function copyDetailDialog(id) {
    modalOpen("/RoomPriceLevel/GetCopyDetail", { id: id }, function () {
        $("#modalDetails button#btnUAddDetail").css("display", "inline");
        $("#TittleBox").html("Copy giá theo phòng");
        OneSetAmount($(".amount-double-mask"));
        g_Utils.SetDateDefaultAdd();
        hideAll();
    });
}
