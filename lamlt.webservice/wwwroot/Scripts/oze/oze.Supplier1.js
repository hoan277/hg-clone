﻿function Supplier() {
    var self = this;
    self.tableName = "Supplier";
    self.idModal = "modalSupplier";
    self.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#modalSupplier .modal-body-content").load("/Supplier/Edit/" + id, function () {
                Sv.SetupInputMask();
            });
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                
                showDialogLoading();
                $.post("/Supplier/update", { obj: pdata }, function (data) {
                    hideDialogLoading(); 
                    if (data.result > 0)
                    { 
                        bootboxLamlt.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); }); 
                    }
                    else {
                        alert("Có lỗi khi tạo nhà cung cấp:" + data.mess,1);
                    }
                });
            });
            // $("#" + self.idModal).off('show.bs.modal');
        }).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.post("/" + self.tableName + "/delete", { id: id }, function (data) {
                                if (data.result > 0) {
                                    bootboxLamlt.alert("Thao tác thành công", function () { self.searchGrid(); });
        }
        else {
            if (data.result == -1) {
                                    bootbox.alert("Nhà cung cấp đang được sử dụng, kiểm tra lại");
        } else {
                                    alert("Có lỗi khi xóa nhà cung cấp:" + data.mess);
        }
        }
        });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    d.search = $("#txtSearch").val();
                }
            },
            "columns":
                [
                { "data": null, "orderable": "false", 
                        render: function (data, type, row, infor) {
                            //return self.$table.page.info().page + infor.row + 1;
                            return self.$table.page.info().page * self.$table.page.len() + infor.row + 1;
                        } },
                { "data": "Code", "orderable": "false" },
                { "data": "Name", "orderable": "false" },
                //{ "data": "Address", "orderable": "false" },
                { "data": "Contact_Person_Name", "orderable": "false" },
                { "data": "Contact_Person_Phone", "orderable": "false" },
                { "data": "Contact_Person_Email", "orderable": "false" },
                {
                    "data": null, render: function (data, type, row) {
                        var htmlMenu =
                          '<div class="edit-delete-table">' +
                              '<div class="edit-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:o' + self.tableName + '.editDialog(' + row.Id + ')">' +
                                  '<img src="/images/icon/icon-edit.png" style=" border: none;" title="Chỉnh sửa">' +
                              '</div>' +
                              '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.Id + ')">' +
                                  '<img src="/images/icon/icon-delete.png" style=" border: none;" title="Xóa">' +
                              '</div>' +
                              '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.Id + ')">' +
                                         '<img src="/images/icon/icon-view.png" style=" border: none;" title="Thông tin chi tiết">' +
                              '</div>' +
                          '</div>';
                        return htmlMenu;
                    }
                }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">Thông tin NCC</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" ><span class="fa fa-check">Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}