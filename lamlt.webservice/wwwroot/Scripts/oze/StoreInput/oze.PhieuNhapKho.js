﻿function PhieuNhapKho() {
    var self = this;
    this.tableName = "Supplier";
    this.idModal = "modalSupplier";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/Supplier/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        //alert("Thao tác thành công",1);
                        bootboxLamlt.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                        //$("#" + self.idModal).modal("hide"); self.searchGrid(); 
                    }
                    else {
                        alert("Có lỗi khi tạo nhà cung cấp:" + data.mess, 1);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.post("/" + self.tableName + "/delete", { id: id }, function (data) {
                                if (data.result > 0) {
                                    bootboxLamlt.alert("Thao tác thành công", function () { self.searchGrid(); });
        }
        else {
            if (data.result == -1) {
                                    bootbox.alert("Nhà cung cấp đang được sử dụng, kiểm tra lại");
        } else {
                                    alert("Có lỗi khi xóa nhà cung cấp:" + data.mess);
        }
        }
        });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/StoreInput/ListPurchase",
                "data": function (d) {
                    d.search = $("#txtSearch").val();
                    d.FromDate = $("input[name='FromDate']").val();
                    d.ToDate = $("[name='ToDate']").val();
                    d.Keyword = $("#keyword").val();
                    d.columns = "";
                }
            },
            "columns":
                [
                {
                    "data": null, "orderable": "false",
                    render: function (data, type, row, infor) {
                        //return self.$table.page.info().page + infor.row + 1;
                        return self.$table.page.info().page * self.$table.page.len() + infor.row + 1;
                    }
                },
                {
                    'data': null, render: function (data, type, row) {
                        console.log(row);
                        return Sv.DateToString(row.InputDate, "DD/MM/YYYY");
                    }
                },
                { "data": "OrderCode", "orderable": "false" },
                { "data": "giaohang_ten", "orderable": "false" },
                { "data": "giaohang_donvi", "orderable": "false" },
                { "data": "AmountNoTax", "orderable": "false", defaultContent: "0" },
                { "data": "chiphi", "orderable": "false", defaultContent: "0" },
                {
                    "data": "TotalAmount", "orderable": "false", defaultContent: "0", render: function (data, type, row) {
                        return Math.round(row.TotalAmount + row.chiphi);
                    }
                },
                { "data": "note", "orderable": "false", defaultContent: "" },

                {
                    "data": null, render: function (data, type, row) {
                        //console.log(row);
                        //var htmlMenu =
                        //  '<div class="edit-delete-table">' +
                        //      '<div class="edit-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:o' + self.tableName + '.editDialog(' + row.Id + ')">' +
                        //          '<img src="/images/icon/icon-edit.png" style=" border: none;" title="Chỉnh sửa">' +
                        //      '</div>' +
                        //      '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.Id + ')">' +
                        //          '<img src="/images/icon/icon-delete.png" style=" border: none;" title="Xóa">' +
                        //      '</div>' +
                        //      '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.Id + ')">' +
                        //                 '<img src="/images/icon/icon-view.png" style=" border: none;" title="Thông tin chi tiết">' +
                        //      '</div>' +
                        //  '</div>';
                        var htmlMenu = "<div style='width:10px;display:inline'><a title='Xem chi tiết hóa đơn nhập kho - In hóa đơn'  href='javascript:print(" + row.Id + ",\"" + row.OrderCode + "\")'>" +
                            "<i class='fa fa-info-circle'></i></a></div> &nbsp;" ;
                            if(!(window.RightCode == "LETAN" && window.hotelConfig.SysHotelID == 1438))   {   // lễ tân 103    
                                htmlMenu+= "<div style='width:10px;display:inline'><a title='Chỉnh sửa hóa đơn nhập kho'  href='javascript:edit(" + row.Id + ",\"" + row.OrderCode + "\")'>" +
                            "<i class='fa fa-pencil-square-o'></i></a></div> &nbsp;";
                        }
                        //"<div style='width:10px;display:inline'><a title='In hóa đơn'  href='javascript:print(" + row.Id + ",\"" + row.OrderCode + "\")'>" +
                        //"<i class='fa fa-print'></i></a></div>";
                        return htmlMenu;
                    }
                }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">Nhà cung cấp</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" ><span class="fa fa-check">Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}

$(document).ready(function () {
    toggleMenu("menu_kho");
    $("#btnNk").off().on("click", function (e) {
        window.location.href = "/StoreInput/Input";
    });
});

function edit(id, code) {
    window.location.href = "/StoreInput/EditInput?id=" + id;

}

function print(id, code) {
    //  console.log(id, code);
    var o = {
        title: "Hóa đơn nhập kho",
    }
    new orderPrint().callView(o, "/StoreInput/PrintReview2", { id: id });

}