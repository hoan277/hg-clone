﻿function tbl_CheckInDeposit(checkinid) {
    var self = this;
    this.tableName = "tbl_CheckInDeposit";
    this.idModal = "modaltbl_CheckInDeposit";
    this.checkinid = checkinid;
    this.$table;
    this.validateF;


    this.editDialog = function (id, checkinid) {
        if (!navigator.onLine) {
            self.editDialogOffLine(id, checkinid);
            return false;
        }
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/?id=" + id + "&checkinid=" + checkinid, function () {
                Sv.SetupInputMask();
                $("#" + self.idModal + " #form" + self.tableName).validate().resetForm();
            });
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/tbl_CheckInDeposit/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    if (navigator.onLine) { // on line
                        if (data.result > 0) {
                            bootboxLamlt.alert("Thao tác thành công", function () {
                                $("#" + self.idModal).modal("hide");
                                self.searchGrid();
                            });
                        }
                        else {
                            alert("Có lỗi thêm trả trước:" + data.mess);
                        }
                    } else { // off line
                        bootboxLamlt.alert(data.Message);
                        $("#" + self.idModal).modal("hide");
                        self.searchGridOffline(data.Data);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }

    this.deleteDialog = function (id) {
        if (!isPermissionRemove()) {
            alert("Để xóa vui lòng liên hệ quản lý. xin cám ơn");
            return;
        }
        bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.post("/" + self.tableName + "/delete", { id: id }, function (data) {
                if (navigator.onLine) { // on line
                    if (data.result > 0) bootboxLamlt.alert("Thao tác thành công", function () { self.searchGrid(); });
                    else alert("Có lỗi khi xóa nhà cung cấp:" + data.mess);
                } else {
                    bootboxLamlt.alert(data.Message);
                    self.searchGridOffline_remove(data.Data);
                }
            });
        });
    }

    this.viewDialog = function(id) {
        var modalId = self.idModal + "view";
        if (!document.getElementById(modalId)) { self.initModal(modalId); }
        $("#" + modalId).off('show.bs.modal');
        $("#" + modalId).on('show.bs.modal', function () { 
            var tr = $("#div_DataDeposit table tr[data-id='" + id + "']");
            $("#" + modalId + " #view_amount").html(Sv.NumberToString(tr.data("amount"))); //.html(Sv.NumberToString(data.amount));
            $("#" + modalId + " #view_paymenttype").html(tr.data("paymenttype"));
            $("#" + modalId + " #view_note").html(tr.data("note"));
            $("#" + modalId + " #view_datecreated").html(tr.data("date"));
            //' <tr data-id="' + data.Id + '" data-amount="' + data.amount + '" data-paymenttype="' + data.payment_type + '" data-note="' + data.note + '" data-date="' + d + '" >' 
            $("#" + modalId + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + modalId).modal("show");
    }

    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#listDataDeposit').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    d.search = self.checkinid;
                    delete d.columns;

                }
            },
            "columns":
                [
				{ 'data': 'amount' }
                , { 'data': 'note' }
                , { 'data': null, render: function (data, type, row, infor) { return row.datecreated === null ? '' : moment(new Date(parseInt(row.datecreated.slice(6, -2)))).format('DD-MM-YYYY HH:mm'); } },
                {
                    "data": null, render: function (data, type, row) {
                        var htmlMenu =
                          '<div class="edit-delete-table">' +
                              '<div class="edit-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:o' + self.tableName + '.editDialog(' + row.Id + ')">' +
                                  '<img src="/images/icon/icon-edit.png" style=" border: none;" title="Chỉnh sửa">' +
                              '</div>' +
                              '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.Id + ')">' +
                                  '<img src="/images/icon/icon-delete.png" style=" border: none;" title="Xóa">' +
                              '</div>' +
                              '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.Id + ')">' +
                                         '<img src="/images/icon/icon-view.png" style=" border: none;" title="Thông tin chi tiết">' +
                              '</div>' +
                          '</div>';
                        return htmlMenu;
                    }
                }
                ]
        });
    }

    this.searchGrid = function (callback) {
        var url = "/tbl_CheckInDeposit/List";
        showDialogLoading();
        $("#div_DataDeposit").html('');
        $.post(url,
            {
                search: self.checkinid,
                length: 1000,
                start: 0
            },
            function (rs1) {
                hideDialogLoading();
                $("#Deposit").val(rs1.totalAmount);
                if (rs1.data.length > 0) {
                    var html = '<label class="col-md-12 pd0 control-label">Trả trước </label>';
                    html += (' <table class="table table-striped table-bordered" cellspacing="0" width="100%">' +
                              ' <thead>' +
                              ' <tr>' +
                              '   <th width="5%">#</th> ' +
                              '   <th width="30%" class="nw">Thanh toán</th>' +
                              '   <th width="40%" class="nw">Thời gian</th>' +
                              '   <th width="20%" class="nw">Tổng tiền</th>' +
                              '   <th width="5%">#</th> ' +
                              ' </tr>' +
                              ' </thead>');
                    html += '<tbody>';

                    $.each(rs1.data, function (key, rs) {
                        var d = rs.datecreated == null ? "" : moment(new Date(parseInt(rs.datecreated.slice(6, -2)))).format("DD/MM/YYYY HH:mm");
                        html += ' <tr data-id="' + rs.Id + '" data-amount="' + rs.amount + '" data-paymenttype="' + (rs.payment_type == 1 ? 1 : 0) + '" data-note="' + rs.note + '" data-date="' + d + '" >' +
                              '   <td>' + (key + 1) + '</td> ' +
                              '   <td>' + (rs.payment_type == 1 ? 'Tiền mặt' : 'Qua thẻ') + '</td>' +
                              '   <td>' + d + '</td>' +
                              '   <td class="text-right">' + Sv.NumberToString(rs.amount) + '</td>' +
                              '   <td class="nw">' +
                                    //' <a onclick="otbl_CheckInDeposit.editDialog(' + rs.Id + ',' + rs.checkinid + ')"><i class="fa fa-pencil-square-o" ></i></a> ' +
                                    ' <a onclick="otbl_CheckInDeposit.viewDialog(' + rs.Id + ')"><i class="fa fa-info-circle" ></i></a> ' +
                                    (isPermissionRemove() ? (' <a onclick="otbl_CheckInDeposit.deleteDialog(' + rs.Id + ')"><i class="fa fa-remove" ></i></a> ')  : '' )+
                              '   </td> ' +
                              ' </tr>';

                    });
                    html += '</tbody>';
                    html += '</table>';
                    $("#div_DataDeposit").html(html);
                } else {
                    $("#div_DataDeposit").html("");
                }
                if (typeof callback === "function") {
                    callback(rs1);
                } else {
                    updateInformation();
                    callToPrice();
                }
            });
        //self.$table.ajax.reload();
    }

    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-md">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">Tiền trả trước</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '       </div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-info" > Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }

    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }

    // sau khi thêm hoặc sau khi update trả trước
    this.searchGridOffline = function (data) {
        //debugger;
        if (data.isAdd) {
            var d = moment(new Date(data.time)).format("DD/MM/YYYY HH:mm");
            var htmlTr = ' <tr data-id="' + data.Id + '" data-amount="' + data.amount + '" data-paymenttype="' + data.payment_type + '" data-note="' + data.note + '" data-date="' + d + '" >' +
                         '   <td>' + 1 + '</td> ' +
                         '   <td>' + (data.payment_type == 1 ? 'Tiền mặt' : 'Qua thẻ') + '</td>' +
                         '   <td>' + d + '</td>' +
                         '   <td class="text-right">' + Sv.NumberToString(data.amount) + '</td>' +
                         '   <td class="nw">' +
                                ' <a onclick="otbl_CheckInDeposit.editDialog(' + data.Id + ',' + data.checkinid + ')"><i class="fa fa-pencil-square-o" ></i></a> ' +
                                (isPermissionRemove() ? ( ' <a onclick="otbl_CheckInDeposit.deleteDialog(' + data.Id + ')"><i class="fa fa-remove" ></i></a> ' ) : '' ) +
                            '</td> ' +
                         '</tr>';

            if ($("#div_DataDeposit").find("table").length > 0) {
                var html = $("#div_DataDeposit").find("tbody").prepend(htmlTr);
                // đánh lại index, stt
                $("#div_DataDeposit").find("table tbody tr").each(function (i, e) {
                    $(e).find("td:eq(0)").html(i + 1);
                });

            } else {
                var html = '<label class="col-md-12 pd0 control-label">Trả trước </label>';
                html += '<label class="col-md-12 pd0 control-label">Trả trước </label>';
                html += (' <table class="table table-striped table-bordered" cellspacing="0" width="100%">' +
                          ' <thead>' +
                          ' <tr>' +
                          '   <th width="5%">#</th> ' +
                          '   <th width="30%" class="nw">Thanh toán</th>' +
                          '   <th width="40%" class="nw">Thời gian</th>' +
                          '   <th width="20%" class="nw">Tổng tiền</th>' +
                          '   <th width="5%">#</th> ' +
                          ' </tr>' +
                          ' </thead>');
                html += '<tbody>' + htmlTr + '</tbody>';
                html += '</table>';
                $("#div_DataDeposit").append(html);
            }
        } else {
            var tr = $("#div_DataDeposit").find("table tr[data-id='" + data.Id + "']");
            if (tr.length > 0) {
                tr.data("amount", data.amount);
                tr.data("paymenttype", data.payment_type);
                tr.data("note", data.note);

                tr.find("td:eq(3)").html(Sv.NumberToString(data.amount));
                tr.find("td:eq(1)").html(data.payment_type == 1 ? 'Tiền mặt' : 'Qua thẻ');
            }

        }

        self.offCallPrice();

    }

    // sau khi remove
    this.searchGridOffline_remove = function (data) {

        var tr = $("#div_DataDeposit").find("table tr[data-id='" + data.id + "']");
        if (tr.length > 0) {
            tr.remove();
            $("#div_DataDeposit").find("table tbody tr").each(function (i, e) {
                $(e).find("td:eq(0)").html(i + 1);
            });
        }

        var lastTable = $("#div_DataDeposit").find("table tbody tr");
        if (lastTable.length <= 0) {
            $("#div_DataDeposit").html("");
        }

        self.offCallPrice();
    }

    // update hóa đơn, thêm mới hóa đơn offline
    this.editDialogOffLine = function (id, checkinid) {
        var tr;
        if (id > 0) {
            tr = $("#div_DataDeposit table tr[data-id='" + id + "']");
            if (tr.length <= 0) {
                alert("Không xác định được hóa đơn trả trước cần update");
                return;
            }
        }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {

            console.log("#" + self.idModal + " #form" + self.tableName);
            $("#" + self.idModal + " #form" + self.tableName).validate().resetForm();
            if (id > 0) {
                $("#form" + self.tableName + " #Id").val(id);
                $("#form" + self.tableName + " #amount").val(tr.data("amount"));
                $("#form" + self.tableName + " #payment_type").val(tr.data("paymenttype"));
                $("#form" + self.tableName + " #note").val(tr.data("note"));
            } else {
                $("#form" + self.tableName + " #Id").val(0);
                $("#form" + self.tableName + " #amount").val(0);
                $("#form" + self.tableName + " #payment_type").val(1);
                $("#form" + self.tableName + " #note").val("");
            }
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/tbl_CheckInDeposit/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    bootboxLamlt.alert(data.Message);
                    $("#" + self.idModal).modal("hide");
                    self.searchGridOffline(data.Data);
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }

    //tính lại giá
    this.offCallPrice = function () {

        // update giá
        var totalDp = 0;
        $("#div_DataDeposit").find("table tbody tr").each(function (i, e) {
            var amount = ($(e).find("td:eq(3)").html()).replace(/[.]/g, "");
            totalDp += parseFloat(amount);
        });

        $("#Deposit").val(totalDp);

        var totalAll = parseFloat($("#txtTotalPrice").val()); // tổng thanh toán
        var totalGt = parseFloat($("#txtDeduction").val()); // Giảm trừ

        bindTotal({
            tratruoc: totalDp,
            thanhtoan: totalAll - totalDp - totalGt
        });

        updateInformation();
    }

}