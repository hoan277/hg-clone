﻿function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function postLogin(data) {
    $.ajax({
        url: "/film_user/Login",
        type: 'Post',
        data: data,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        beforeSend: function () {
            Sv.RequestStart();
        },
        complete: function () {
            Sv.RequestEnd();
        },
        success: function (mgs) { 
            if (mgs.message == 1)
            {
                createCookie('CookieUserName-xxxx', data.username, 30);
                // save cookie
                if (data.chkRememberMe == true) {
                    createCookie('CookieUserName', data.username, 30);
                    createCookie('CookiePassword', data.password, 30);
                } else {
                    createCookie('CookieUserName', '', -1);
                    createCookie('CookiePassword', '', -1);
                }
                createCookie('isFirstLogin', '1', 1);
                window.location.href = "/home/index";
            
            } else {
                $("#mgslogin").show();
                $("#mgslogin .text-center").html("Đăng nhập không thành công, kiểm tra lại mật khẩu hoặc tên đăng nhập");
            }
        }
    });

}

$(document).ready(function () {
    // check logout
    var url = window.location.href;
    if (url.indexOf('type=logout') < 0) {
        // check  Cookie
        var user = readCookie('CookieUserName');
        var pass = readCookie('CookiePassword');
        if (user && pass) {
            var data = {
                username: user,
                password: pass,
                __RequestVerificationToken: $("#login-form").find("[name=__RequestVerificationToken]").val(),
                command: "SignInSub",
                chkRememberMe: true,
            };
            postLogin(data);
        }
    } else {
        // xóa cookie
        createCookie('CookieUserName', '', -1);
        createCookie('CookiePassword', '', -1);
    }
    $("#login-form").submit(function (ev) {
        ev.preventDefault();
        var $f = $("#login-form");
        var data = {
            user: $f.find("[name=UserName]").val(),
            pass: $f.find("[name=Password]").val(),
            __RequestVerificationToken: $f.find("[name=__RequestVerificationToken]").val(),
            command: "SignInSub",
            chkRememberMe: $f.find("[name=chkRememberMe]").is(":checked")
        };
        postLogin(data);
    });


});
