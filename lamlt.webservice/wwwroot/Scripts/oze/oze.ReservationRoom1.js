﻿
console.log(window.islockCard);
//(new ReservationRoom()).AssignRoom(id)
function ReservationRoom() {
    var self = this;
    // ke thua OzeBase
    OzeBase.apply(this, arguments);

    var currentMainPrice = {};
    var currentMainPriceHour = {};
    this.TTTV = {
        Name:"",        
        Phone:"",
        Email:"",
        Company:"",
        Address:"",  
        DOB:"01/01/2000",   
        Sex:"1",  
        mucdichcutru: "Khác",
        loaigiayto:"1",  
        tongiao:"0",  
        dantoc: "Kinh",
        ProvinceID:"0",  
        DistrictID:"0",  
        RewardID:"0",  
    };
    this.TableCustomerHistory = {};
    this.changeLabelGiaPhong = function (val) {
        if (val == 0) $("#lbl_GiaPhong").html("Giá phòng/Giờ");
        if (val == 1) $("#lbl_GiaPhong").html("Giá phòng/Ngày");
        if (val == 2) $("#lbl_GiaPhong").html("Giá phòng/Đêm");
        if (val == 3) $("#lbl_GiaPhong").html("Giá phòng/Tháng");
        if (val == -1) $("#lbl_GiaPhong").html("Giá phòng/Giờ");
        self.showPrice();
    }
    this.clearPrice = function () {
        currentMainPrice = {};
        currentMainPriceHour = {};
    }
    this.chooseRoom = function (roomid) {
        $('#dllRoom').val(roomid).trigger('change');
        $.get("/Common/GetRoomTypeByRoomID?roomid=" + $('#dllRoom').val(), function (data) {
            if (data.result) {
                if (data.result.Id) {
                    $("#dllRoomTypeID").val(data.result.Id);
                    self.loadPrice();

                    //document.activeElement.blur()
                    //$(this).find("#modalDetails .modal-body :input:visible:first").focus();
                    //$("#dllRoomTypeID").focus();
                }
            }
        });
        $("#myModalTemp").modal("hide");
    }
    this.viewDetailDatPhong = function (id) {
        window.location.hash = "#/ReservationRoom/DatPhongDetail?id=" + id;
    }
    this.editRoomMate = function (id) {
        $("#myModal").off('show.bs.modal');
        $("#myModal").on('show.bs.modal', function () {
            $("#myModal .modal-body-content").html('<p>loading..</p>');
            $("#myModal .modal-body-content").load("/ReservationRoom/EditRoomMate/" + id);
            $("#myModal button#btnSave").css("display", "inline");
            $("#btnSave").off("click");
            $("#btnSave").click(function () {
                if (!$("#dummyRoomMate").valid()) return;
                var pdata = self.getFormDataMate();
                showDialogLoading();
                $.post("/ReservationRoom/AddRoomMate", pdata, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi thêm bạn cùng phòng");
                    }
                });
            });
            $("#myModal").off('show.bs.modal');
        });
        $("#myModal").modal("show");
    }
    this.bindLocalTTTV = function () {
        var TTTV = self.TTTV;
        if (TTTV.ngaynhapcanh) { $("#ngaynhapcanh").val(TTTV.ngaynhapcanh); }
        if (TTTV.cuakhau) { $("#cuakhau").val(TTTV.cuakhau); }
        if (TTTV.sophieuXNC) { $("#sophieuXNC").val(TTTV.sophieuXNC); }


        if (TTTV.hanthithuc) { $("#hanthithuc").val(TTTV.hanthithuc); }
        if (TTTV.hantamtru) { $("#hantamtru").val(TTTV.hantamtru); }
        if (TTTV.mucdichcutru) { $("#mucdichcutru").val(TTTV.mucdichcutru); }
        if (TTTV.loaigiayto) { $("#loaigiayto").val(TTTV.loaigiayto); }
        if (TTTV.IdentifyNumber) { $("#IdentifyNumber").val(TTTV.IdentifyNumber); }
        if (TTTV.nghenghiep) { $("#nghenghiep").val(TTTV.nghenghiep); }
        if (TTTV.dantoc) { $("#dantoc").val(TTTV.dantoc); }
        if (TTTV.tongiao) { $("#tongiao").val(TTTV.tongiao); }
        $("#chkTTTV").prop('checked', false);
    }

    this.getCustomerData2 = function () {
        var cbLeader = $('#cbLeader input[name=chkLeaderf]:checked').val();
        var cbTTTV = $('#dllFriend input[name=chkTTTVf]:checked').val();
        var cust = {
            //THÔNG TIN KHÁCH HÀNG    
            'ID': 0,
            'FullName': $('#txtnamef').val(),//Tên đầy đủ
            'Sex': 1,//$('#txtname').val(),//giới tính
            'DOB': $('#txtDOBf').val(),//ngày tháng năm sinh dd/mm/yyyy
            'IdentityNumber': $('#txtIdentifyf').val(),// CMND/Hộ chiếu ....
            'CitizenshipCode': $('#dllQuocTichf').val(),// Quốc tịch
            'Address': $('#txtaddressf').val(),//địa chỉ
            'Email': $('#txtEmailf').val(),//email
            'Mobile': $('#txtMobilef').val(),//mobile
            'Company': $('#txtCompanyf').val(),//Company
            'RoomID': $('#dllRoom').val(),//ID phòng
            'GroupID': $('#dllOrgf').val(),////Code đoàn (nếu là đặt phòng đi theo đoàn)
            'GroupJoinID': $('#dllOrgJoinf').val(),//ID phòng
            'Leader': cbLeader,//là Trưởng đoàn
            'Payer': cbPayer,//ID phòng
            'ReserCode': $('#txtResCode').val(),
            'CountryId': $('#dllQuocTich').val(),
        };
        return cust;
    }


    this.getReservationRoomData = function () {
        var cbLeader = $('#cbLeader input[name=chkLeader]:checked').val();
        var cbTTTV = $("#formReservation #chkTTTV").parent().hasClass("checked") ? 1 : 0;
        var cbPayer = $('#cbPayer input[name=chkPayer]:checked').val();
        // gan cac doi tuong vao object:THÔNG TIN KHÁCH HÀNG
        var cust = self.TTTV;
        cust.ID = $("#hdCusID").val();
        cust.FullName = $('#txtname').val();
        cust.IdentityNumber = $('#txtIdentify').val();
        cust.RoomID = $('#dllRoom').val();
        cust.Leader = cbLeader;
        cust.Payer = cbPayer;
        cust.TTTV = cbTTTV;
        //var cust =
        //   {
        //       //THÔNG TIN KHÁCH HÀNG
        //       'ID': $("#hdCusID").val(),
        //       'FullName': $('#txtname').val(),//Tên đầy đủ
        //       'Sex': 1, //rbSex,//$('#txtname').val(),//giới tính
        //       'DOB': $('#txtDOB').val(),//ngày tháng năm sinh dd/mm/yyyy
        //       'IdentityNumber': $('#txtIdentify').val(),// CMND/Hộ chiếu ....
        //       'CitizenshipCode': $('#dllQuocTich').val(),// Quốc tịch
        //       'Address': $('#txtaddress').val(),//địa chỉ
        //       'Email': self.TTTV,//email
        //       'Mobile': $('#txtMobile').val(),//mobile
        //       'Company': $('#txtCompany').val(),//Company
        //       'RoomID': $('#dllRoom').val(),//ID phòng
        //       'GroupID': $('#dllOrg').val(),////Code đoàn (nếu là đặt phòng đi theo đoàn)
        //       'GroupJoinID': $('#dllOrgJoin').val(),//ID phòng
        //       'Leader': cbLeader,//là Trưởng đoàn
        //       'Payer': cbPayer,    //ID phòng
        //       'CountryId': $("#dllQuocTich").val(),
        //       'TTTV': cbTTTV
        //   };
        // có thay đổi giá ko 
        var price = {
            pricelevelId: $("#pricelevelId").val(),
            ischange: $("#changePrice").val() === "1" ? true : false,
            title: $("#lblPolicyPrice").val(),
            price: $("#txtPrice").val(),
            type: $("#dllPriceKhungGio").val(),
            khunggia: $("#dllPriceKhungGio").val(),
            typetitle: $("#dllPriceKhungGio option:selected").text(),
            datefrom: $('#txtFromDate').val(),
            dateto: $('#txtToDate').val(),
        };
        var resroom = {
            //THÔNG TIN ĐẶT PHÒNG
            'ID': $("#hdReservationID").val(),
            'CustomerName': $('#txtname').val(),//Ten khach dat phong
            'ReservationCode': $('#txtResCode').val(),//mã đặt phòng
            'ReservationType': 0,//loại đặt phòng 
            'Payment_Type_ID': $('#dllPaymentType').val(),//ID phương thức thanh toán
            'ArrivalDate': $('#txtFromDate').val(),//thời gian đến (booking)
            'LeaveDate': $('#txtToDate').val(),//thời gian đi (booking)
            'Adult': $('#txtAdult').val(),//số người lớn
            'Children': $('#txtChildren').val(),//số trẻ nhỏ
            'Holiday': $('#dllPriceDipLe').val(),//giá phòng theo dịp (tính theo ngày thường, ngày lễ)
            'KhungGio': $('#dllPriceKhungGio').val(),//giá phòng theo khung (tính theo giờ, giá cả ngày, giá đêm)
            'Price': $('#txtPrice').val().replace(',', ''),//giá phòng (tính theo loại đặt phòng)
            'Deposit': $('#txtDeposit').val().replace(',', ''),//tiền cọc
            'Deduction': $('#txtDeduction').val().replace(',', ''),//giảm trừ
            'Discount': $('#txtDiscount').val().replace(',', ''),//giảm giá
            'DiscountType': $('#dllDiscountType').val(),//giảm giá
            
            //'upPrice': $('#upPrice').val().replace(',', ''),//tăng giá
            //'upPriceType': $('#dllUpPrice').val(),//tăng giá

            'Note': $('#txtNote').val(),//ghi chú 
            'RoomTypeID': $('#dllRoomTypeID').val(),//loại phòng
            'RoomID': $('#dllRoom').val(),//loại phòng
            'RoomLevelPriceID': $('#RoomLevelPriceID').val(),//chính sách giá
            'ReservationStatus': $('#dllStatus').val(),
            'TravelId': $("#dllTravel").val(),
            customer: cust,
            priceLevel: price,
        }
        return resroom;
    }

    this.showTTTVInfo = function (idmodal, checked) {
        var divTT = $("#" + idmodal + " #ttcutru");
        if (checked) {
            divTT.show();
        } else {
            divTT.hide();
        }
    }

    // checked: true = NN; false = VN
    this.showTTTVInfo_NN = function (idmodal, checked) {
        var $divNN = $("#" + idmodal + " #ttcutru_nn");
        var $divVN = $("#" + idmodal + " #ttcutru_vn");
        var $loaigiayto = $("#" + idmodal + " #loaigiayto");
        if (checked) {
            // thay đổi loại giấy tờ
            self.appendloaigiayto($loaigiayto, "NN");
            $divNN.show();
            $divVN.hide();
        } else {
            // thay đổi loại giấy tờ
            self.appendloaigiayto($loaigiayto, "VN");
            $divNN.hide();
            $divVN.show();
        }
    }

    this.appendloaigiayto = function ($e, typeKH, value) {
        $e.empty();
        var html = "";
        var v = value != undefined ? value : 1;
        function selectedv(v1, v2) {
            if (v1 == v2) return "selected";
            else return "";
        }
        if (typeKH == "VN") {
            html += '<option value="1" ' + selectedv(v, 1) + '>Chứng minh nhân dân</option>' +
                    '<option value="2" ' + selectedv(v, 2) + '>Loại giấy tờ cơ quan NN cấp (có ảnh)</option>' +
                    '<option value="3" ' + selectedv(v, 3) + '>Giấy xác nhận cử đi công tác</option>' +
                    '<option value="4" ' + selectedv(v, 4) + '>Giấy xác nhận của cơ quan liên hệ công tác</option>' +
                    '<option value="5" ' + selectedv(v, 5) + '>Giấy xác nhận của UBND phường, xã, thị trấn nơi cư trú</option>';
        } else {
            html += '<option value="11" ' + selectedv(v, 11) + '>Hộ chiếu phổ thông</option>' +
                    '<option value="12" ' + selectedv(v, 12) + '>Giấy thông hành</option>' +
                    '<option value="13" ' + selectedv(v, 13) + '>Hộ chiếu công vụ</option>' +
                    '<option value="14" ' + selectedv(v, 14) + '>Hộ chiếu ngoại giao</option>' +
                    '<option value="15" ' + selectedv(v, 15) + '>Các loại giấy tờ thay thế hộ chiếu khác</option>';
        }
        $e.html(html);
        return $e;
    }

    this.showTTTV = function (id, countryid, mode, idmodal, isChecked, callback, callbackSave) {
        if (!idmodal)
            idmodal = "myModalTemp";
        var $modal = $("#" + idmodal);
        $modal.off('show.bs.modal');
        $modal.find('.modal-title').html("Thông tin cư trú");
        $modal.find(".modal-dialog").removeClass("modal-lg").removeClass("modal-md").addClass("modal-lg");
        $modal.on('show.bs.modal', function () {
            $modal.find(".modal-body-content").html('<p>loading..</p>');
            $modal.find(".modal-body-content").load("/ReservationRoom/KhaiBaoTamTru?id=" + id + "&countryid=" + countryid, function () {
                Sv.SetupDatePicker([{ e: $("#ngaynhapcanh") }, { e: $("#hanthithuc") }, { e: $("#hantamtru") }, { e: $("#DOB") }]);
                if (id == 0) {
                    self.bindLocalTTTV();
                }
                var chkTTTV = $("#" + idmodal + " #chkTTTV");
                if (isChecked == true) {
                    chkTTTV.parent().off("click").on("click", function () {
                        self.showTTTVInfo(idmodal, chkTTTV.is(":checked"));
                    }).trigger("click");
                } else {
                    chkTTTV.parent().off("click").on("click", function () {
                        self.showTTTVInfo(idmodal, chkTTTV.is(":checked"));
                    });
                    self.showTTTVInfo(idmodal, chkTTTV.is(":checked"));
                }

                $("#" + idmodal + " input:radio[name=typef]").off("change").change(function () {
                    if (this.value == "VN") {
                        self.showTTTVInfo_NN(idmodal, false);
                    } else if (this.value == "NN") {
                        self.showTTTVInfo_NN(idmodal, true);
                    }
                });

                if (typeof callback === "function") {
                    callback();
                }
            });
            var $btnSave = $("#" + idmodal + " button#btnSave");
            $btnSave.css("display", "inline");
            $btnSave.off("click").click(function () {
                var $form = $("#" + idmodal + " #formKhaiBaoTamTru");
                if ($form.valid()) {
                    var objModel = self.getFormTTTV();
                    //if (!$("#formKhaiBaoTamTru").valid()) return;
                    //nếu là mode để cập nhật thì cập nhật lên server
                    if (mode) {
                        showDialogLoading();
                        var pdata = { obj: objModel };
                        $.post("/ReservationRoom/UpdateTamTruTamVang", pdata, function (data) {
                            hideDialogLoading();
                            if (typeof callbackSave === "function") {
                                callbackSave(self.getFormTTTV(), data);
                            } else {
                                //closeDlgLoadingData();
                                if (data.result > 0) {
                                    bootboxLamlt.alert("Thao tác thành công", function () { $("#" + idmodal).modal("hide"); });
                                }
                                else {
                                    alert("Có lỗi khi cập nhật tạm trú tạm vắng");
                                }
                            }
                        });
                    }
                        //ko có mode thì mặc định là để save vào client
                    else {
                        self.TTTV = objModel;
                    }
                    $("#" + idmodal).modal("hide");
                }
            });
            if ($("#myModalTemp #btnColseModal").length > 0) {
                $("#myModalTemp #btnColseModal").off("click").on("click", function () {
                    $("#myModalTemp").modal("hide");
                });
            }

            $("#" + idmodal).off('show.bs.modal');

        });
        $("#" + idmodal).modal("show");
    }

    this.showTTTV2 = function (idmodal, mode, option) {
        if (!idmodal) idmodal = "myModalTemp";
        $("#" + idmodal).find('.modal-title').html("Thông tin cư trú");
        $("#" + idmodal).off('show.bs.modal');
        $("#" + idmodal).find(".modal-dialog").removeClass("modal-lg").removeClass("modal-md").addClass("modal-lg");
        $("#" + idmodal).on('show.bs.modal', function () {
            $("#" + idmodal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + idmodal + " .modal-body-content").load("/ReservationRoom/KhaiBaoTamTru?id=" + option.id + "&countryid=" + option.countryid, function () {
                Sv.SetupDatePicker([{ e: $("#ngaynhapcanh") }, { e: $("#hanthithuc") }, { e: $("#hantamtru") }, { e: $("#DOB") }]);
                if (option.id == 0) {
                    self.bindLocalTTTV();
                }
                var chkTTTV = $("#" + idmodal + " #chkTTTV");
                if (option.isChecked == true) {
                    chkTTTV.prop("checked", true);
                } else {
                    chkTTTV.prop("checked", false);
                }
                chkTTTV.parent().off("click").on("click", function () {
                    self.showTTTVInfo(idmodal, chkTTTV.is(":checked"));
                });
                self.showTTTVInfo(idmodal, chkTTTV.is(":checked"));

                $("#" + idmodal + " input:radio[name=typef]").off("change").change(function () {
                    if (this.value == "VN") {
                        self.showTTTVInfo_NN(idmodal, false);
                    } else if (this.value == "NN") {
                        self.showTTTVInfo_NN(idmodal, true);
                    }
                });
                if (typeof option.callbackShow === "function") {
                    option.callbackShow();
                }
            });

            $("#" + idmodal + " button#btnSave").css("display", "inline");
            $("#" + idmodal + " button#btnSave").off("click");
            $("#" + idmodal + " button#btnSave").click(function () {
                if (!$("#" + idmodal + " #formKhaiBaoTamTru").valid()) return;

                //nếu là mode để cập nhật thì cập nhật lên server
                if (mode) {
                    showDialogLoading();
                    var pdata = { obj: self.getFormTTTV() };
                    $.post("/ReservationRoom/UpdateTamTruTamVang", pdata, function (data) {
                        hideDialogLoading();
                        if (typeof option.callbackSave === "function") {
                            option.callbackSave(self.getFormTTTV(), data);
                        } else {
                            //closeDlgLoadingData();
                            if (data.result > 0) {
                                bootboxLamlt.alert("Thao tác thành công", function () { $("#" + idmodal).modal("hide"); });
                            }
                            else {
                                alert("Có lỗi khi cập nhật tạm trú tạm vắng");
                            }
                        }
                    });
                }
                    //ko có mode thì mặc định là để save vào client
                else {
                    var pdata = self.getFormTTTV();
                    debugger
                    self.TTTV = pdata;
                    if (typeof option.callbackSave === "function") {
                        option.callbackSave(pdata, pdata);
                    }
                }
                $("#" + idmodal).modal("hide");
            });
            if ($("#myModalTemp #btnColseModal").length > 0) {
                $("#myModalTemp #btnColseModal").off("click").on("click", function () {
                    $("#myModalTemp").modal("hide");
                });
            }

            $("#" + idmodal).off('show.bs.modal');

            if (typeof option.callback === "function") {
                option.callback();
            }
        });
        $("#" + idmodal).modal("show");

    }

    this.AssignRoom = function (id) {
        $("#myModal").off('show.bs.modal');
        $("#myModal").on('show.bs.modal', function () {
            $("#myModal .modal-body-content").html('<p>loading..</p>');
            $("#myModal .modal-body-content").load("/ReservationRoom/AssignRoom/" + id);
            $("#myModal button#btnSave").css("display", "inline");
            $("#btnSave").off("click");
            $("#btnSave").click(function () {
                var pdata = { reservationid: $("#hdReservationID").val(), roomid: $("#dllRoom").val() };
                showDialogLoading();
                $.post("/ReservationRoom/GanPhong", pdata, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootboxLamlt.alert("Thao tác thành công", function () {
                            $("#myModal").modal("hide");
                            window.location.hash = "#/ReservationRoom/DatPhongDetail?id=" + $("#hdReservationID").val();
                        });
                    }
                    else {
                        alert("Có lỗi khi gán phòng:" + data.mess);
                    }
                });
            });
        });
        $("#myModal").modal("show");
    }
    this.getFormDataMate = function () {
        // gan cac doi tuong vao object
        var cust = self.getCustomerData2();
        return { obj: cust, checkinid: $("#hdReservationID").val() };

    }
    this.getFormTTTV = function () {    
        // gan cac doi tuong vao object
        var cust = getFormData($("#formKhaiBaoTamTru"));
        if (cust.typef == "VN") {
            cust.countryId = 238;
        }
        return cust;
    }
    this.loadPrice = function () {
        $.get("/Common/GetPriceByDateInAndRoomType?roomtypeid=" + $('#dllRoomTypeID').val() + "&datetime=" + $('#txtFromDate').val() + "&resId=" + $('#hdReservationID').val(), function (data) {
            if (data.result) {
                currentMainPrice = data.result;
                currentMainPriceHour = data.hours;
                self.showPrice();
            }
            else {
                self.clearPrice();
                $("#lblPolicyPrice").val("");
                $("#txtPrice").val(0);
            }
           
        }); 
    }
    this.showPrice = function () {
        //hiển thị label 
        if (currentMainPrice.title)
            $("#lblPolicyPrice").val(currentMainPrice.title);

        //tracking id chinh sach gia
        if (currentMainPrice.Id)
            $("#RoomLevelPriceID").val(currentMainPrice.Id); 
        //theo giờ hoặc tự động
        if ($("#dllPriceKhungGio").val() == "0" || $("#dllPriceKhungGio").val() == "-1") {
            if (currentMainPriceHour && currentMainPriceHour.price)
                $("#txtPrice").val(currentMainPriceHour.price);
            else {
                $("#txtPrice").val(0);
            }
        }
        else if ($("#dllPriceKhungGio").val() == "1") {
            if (currentMainPrice.PriceDay)
                $("#txtPrice").val(currentMainPrice.PriceDay); //theo đêm
            else
                $("#txtPrice").val(0);
        }
        else if ($("#dllPriceKhungGio").val() == "2") {
            if (currentMainPrice.PriceNight)
                $("#txtPrice").val(currentMainPrice.PriceNight); //theo ngày
            else
                $("#txtPrice").val(0);
        }
        else if ($("#dllPriceKhungGio").val() == "3") {
            if (currentMainPrice.PriceMonth)
                $("#txtPrice").val(currentMainPrice.PriceMonth); //theo tháng
            else
                $("#txtPrice").val(0);
        }

        // tick thay đổi giá
        $("#changePrice").val(0);

    }
    this.edit = function (idRes) {
        window.location.hash = "#/ReservationRoom/DatPhong?id=" + idRes;
    }
    this.huyPhong = function (idRes) {
        bootbox.prompt(
        {
            title: "Nhập lý do hủy:",
            inputType: 'textarea',
            callback: function (result) { 
                if (result == null) 
                    return;
                
                if (result == "") {
                    alert("Vui lòng nhập lý do hủy");
                    return;
                }
                $.ajax({
                    url: '/ReservationRoom/HuyDatPhong',
                    type: "Post",
                    datatype: "json",
                    data: { id: idRes, "note": result },
                    success: function (response) {
                        if (response.result == 1) {
                            bootboxLamlt.alert("Thao tác thành công");
                            hashChangeSodophong();
                        }
                        else {
                            alert("Thao tác không thành công:" + response.mess);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Thao tác không thành công:" + thrownError);
                    }
                });
            }
        });
    }
    this.huyPhongDoan = function (idRes) {
        bootbox.prompt(
        {
            title: "Hủy phòng theo đoàn - Nhập lý do hủy:",
            inputType: 'textarea',
            callback: function (result) {
                if (result == null)
                    return;
                if (result == "") {
                    alert("Vui lòng nhập lý do hủy");
                    return;
                }
                $.ajax({
                    url: '/CheckInGroup/HuyDatPhongDoan',
                    type: "Post",
                    datatype: "json",
                    data: { id: idRes, "note": result },
                    success: function (response) {
                        if (response.Status == "1") {
                            bootboxLamlt.alert("Thao tác thành công");
                            hashChangeSodophong();
                        }
                        else {
                            alert("Thao tác không thành công:" + response.Message);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Thao tác không thành công:" + thrownError);
                    }
                });
            }
        });
    }
    this.cancelCheckin = function (idRes) {
        bootbox.prompt(
        {
            title: "Nhập lý do hủy:",
            inputType: 'textarea',
            callback: function (result) {
                if (result == null)
                    return;

                if (result == "") {
                    alert("Vui lòng nhập lý do hủy");
                    return;
                }
                $.ajax({
                    url: '/ReservationRoom/CancelCheckIn',
                    type: "Post",
                    datatype: "json",
                    data: { id: idRes, "note": result },
                    beforeSend: function () { Sv.RequestStart(); },
                    complete: function () { Sv.RequestEnd(); },
                    success: function (response) {
                        if (response.result == 1) {
                            bootboxLamlt.alert("Thao tác thành công");
                            hashChangeSodophong();
                        }
                        else {
                            alert("Thao tác không thành công:" + response.mess);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Thao tác không thành công:" + thrownError);
                    }
                });
            }
        });
    }
    this.cancelCheckinDoan = function (idRes) {
        bootbox.prompt(
        {
            title: "Nhập lý do hủy:",
            inputType: 'textarea',
            callback: function (result) {
                if (result == null)
                    return;

                if (result == "") {
                    alert("Vui lòng nhập lý do hủy");
                    return;
                }
                $.ajax({
                    url: '/CheckInGroup/CancelCheckIn',
                    type: "Post",
                    datatype: "json",
                    data: { id: idRes, "note": result },
                    beforeSend: function () { Sv.RequestStart();   }, 
                    complete: function () {   Sv.RequestEnd();  },
                    success: function (response) {
                        if (response.IntStatus == 1) {
                            bootboxLamlt.alert("Thao tác thành công");
                            hashChangeSodophong();
                        }
                        else {
                            alert("Thao tác không thành công:" + response.Message);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Thao tác không thành công:" + thrownError);
                    }
                });
            }
        });
    }

    this.checkRoom = function (dtFrom, dtTo, typeRoomId) {

        var objData = { dtFrom: dtFrom, dtTo: dtTo, typeRoomId: typeRoomId };
        $("#myModalTemp").find(".modal-dialog").removeClass("modal-lg").removeClass("modal-md").addClass("modal-md");
        $("#myModalTemp").off('show.bs.modal');
        $("#myModalTemp .modal-title").html("Kiểm tra phòng");

        $("#myModalTemp").on('show.bs.modal', function () {
            $("#myModalTemp .modal-body-content").html('<p>loading..</p>');
            $.post("/ReservationRoom/checkRoom", objData, function (data) {
                hideDialogLoading();
                $("#myModalTemp .modal-body-content").html(data);
            });
        });
        var $b = $("#myModalTemp #btnColseModal");
        if ($b.length > 0) {
            $b.off("click").on("click", function () {
                $("#myModalTemp").modal("hide");
            });
        }


        $("#myModalTemp").modal("show");

    }
     
    this.doiphong = function (roomid) {
        showDialogLoading(); 
        $("#modalDetailsDp").find(".modal-dialog").removeClass("modal-lg").removeClass("modal-md").addClass("modal-lg");
        $("#modalDetailsDp").off('show.bs.modal');
        $("#modalDetailsDp .modal-title").html("Đổi phòng");
        $("#modalDetailsDp").on('show.bs.modal', function () {
            $("#modalDetailsDp .modal-body-content").html('<p>loading..</p>');
            
            $.post("/ReservationRoom/ChangeRoomDoiPhong", { roomId: roomid }, function (data) {
                hideDialogLoading();
                $("#modalDetailsDp .modal-body-content").html(data);

                $("#modalDetailsDp #btnSaveDp").off("click").click(function () {
                    var obj = {
                        id: $("#roomId").val(),
                        name: $("#roomId").data("name"),
                        CheckInID: $("#checkinId").val(),
                        bookingCode: $("#bookingCode").val(),
                        customerId: $("#customerId").val(),
                        roomOldId: $("#roomOldId").val(),
                        roomOldName: $("#roomOldId").data("name"),
                        Note: $("#doiphong_note").val(),
                        tdate: $("#Leave_Date").val(),
                    }
                    console.log(obj);
                    if (obj.id <= 0) {
                        alert("Vui lòng chọn phòng cần đổi!");
                        return;
                    }
                    if (obj.Note == "") {
                        alert("Vui lòng nhập lý do đổi phòng!");
                        return;
                    }
                    
                    confirm("Bạn có muốn chắc chắn đổi phòng: <b>" + obj.roomOldName + "</b> sang phòng: <b>" + obj.name + "</b> không?", function (res) {
                        if (res) {
                            showDialogLoading(); 
                            $.post("/ReservationRoom/ChangeRoomDoiPhong_submit", obj, function (rs) {
                                hideDialogLoading(); 
                                if (rs.IntStatus ==1) {
                                    $('#modalDetailsDp').on('hidden.bs.modal', function () {
                                        bootboxLamlt.alert(rs.Message);
                                        hashChangeSodophong();
                                    })
                                    if (typeof dialogMenuclose == "function")
                                        dialogMenuclose();
                                    $("#modalDetailsDp").modal("hide");
                                }
                                else {
                                    alert(rs.Message);
                                }
                            });
                        }
                    });
                     
                });
            });
        });
          
        $("#modalDetailsDp").modal("show");

    }
    
    this.changeStatus = function () {
        var room = $('#dllRoom').val(); 
        if (parseInt(room) > 0) {
            $('#dllStatus').val(2);
        }
        else
            $('#dllStatus').val(1);
    }


    this.setupClick = function () {
        $('#dllRoomTypeID').change(function () { 
            $('#dllRoom').val(0);
            self.changeStatus();
            loadRoomByLevelRoom($('#dllRoomTypeID').val(), "dllRoom", 0, function () {
                $("#dllRoom").select2({width:'100%'})
                self.loadPrice();
            });
        });
        $('#dllRoom').change(function () {
            self.changeStatus();
            if ($('#dllRoom').val() == 0) { 
                return;
            } 
            $.get("/Common/GetRoomTypeByRoomID?roomid=" + $('#dllRoom').val(), function (data) {
                if (data.result) {
                    if (data.result.Id) {
                        $("#dllRoomTypeID").val(data.result.Id); 
                        self.loadPrice();
                    }
                }
               
            });
        });
        //fn_Nhận phòng   --> DONE add booking, not done edit booking
        var btnCheckKiemTraPhong = $('#btnCheckKiemTraPhong');
        btnCheckKiemTraPhong.click(function () {
            var objData = { dtFrom: $("#txtFromDate").val(), dtTo: $("#txtToDate").val() };
            self.checkRoom($("#txtFromDate").val(), $("#txtToDate").val(), $("#dllRoomTypeID").val());
            /*
           // alert($("#txtToDate").val());
            $.ajax({
                url: '/ReservationRoom/checkRoom',
                type: "Post",
                datatype: "json",
                data: objData,
                success: function (response)
                {
                    BootstrapDialog.show(
                        {
                        message: response
                    });
    
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    // error message
                    alert("Thao tác không thành công:" + thrownError);
                }
            })
            */
        });

        var btnBook = $('#btnBook');
        btnBook.on("click", self.DatTruoc);

        var btnNhanPhongTheoBooking = $('#btnResSaveByBooking');
        btnNhanPhongTheoBooking.click(function () {
            var dataRoomId = btnNhanPhongTheoBooking.data('roomid');
            if (!(dataRoomId && dataRoomId > 0)) {
                bootboxLamlt.alert("Vui lòng chọn phòng trước khi nhận phòng");           
                return false;
            }
            //debugger;
            bootbox.confirm("Bạn có chắc chắn muốn nhận phòng?", function (result) {
                if (!result) return;

                $.ajax({
                    url: '/ReservationRoom/CheckInByReservationID',
                    type: "Post",
                    datatype: "json",
                    data: { reservationid: $("#hdReservationID").val() },
                    beforeSend: function () {
                        Sv.RequestStart();
                    },
                    complete: function () {
                        Sv.RequestEnd();
                    },
                    success: function (response) {
                        if (response.result == 1) {
                            bootboxLamlt.alert("Thao tác thành công"); 
                            if (window.islockCard && window.islockCard == 1) { 
                                var url = "/Common/LockCard2?checkinid=" + response.checkinId;
                                new LockCardControl().showModal(url, function () { hashChangeSodophong(); });
                            } else {
                                hashChangeSodophong();
                            } 
                        }
                        else {
                            alert("Thao tác không thành công:" + response.mess);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // error message
                        alert("Thao tác không thành công:" + thrownError);
                    }
                });
            });
        });

        var btnAssignRoom = $('#btnAssignRoom');
        btnAssignRoom.click(function () {
            self.AssignRoom($("#hdReservationID").val());
        });

        var btnNhanPhong = $('#btnResSave');
        btnNhanPhong.click(self.NhanPhong);

        $(".bgresrom1").keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                self.DatTruoc();
            }
        });

        var btnCancel = $('#btnCancel');
        btnCancel.click(function () {

            self.huyPhong($("#hdReservationID").val());
        });

        // fn_Thêm/Sửa thông tin bạn cùng phòng  ---> DONE add friend, not done edit friend
        $('#btnAdd').click(function () {
            //form = self.getParentByTagName(this, 'form');
            var cust = self.getCustomerData2();

            // thay doi url theo them/sua
            if (m_check_modify) {
                url = '/ReservationRoom/ReservationEditFriend';
                cust.ID = $(this).data("id");
            } else {
                url = '/ReservationRoom/ReservationAddFriend';
            }

            if ($(form).valid()) {
                $.ajax({
                    beforeSend: function () {
                        Sv.RequestStart();
                    },
                    complete: function () {
                        Sv.RequestEnd();
                    },
                    url: url,
                    type: "Post",
                    datatype: "json",
                    data: { customer: cust },
                    success: function (response) {
                        var mess, result;
                        mess = response.mess;

                        if (!m_check_modify) { //add
                            if (mess[1] == 1) { //success                                

                                //lay lai data vừa insert bin nguoc lai vao cac control
                                if (mess[2] != null) {
                                    var obj = mess[2];
                                    //append 1 row thông tin ban cung phòng moi
                                    $("#dllFriend").append('<a href="#myModal" data-toggle="modal" class="edit-friend" data-id="' + obj[0].ID + '"> <i class="fa fa-check-square-o"></i>&nbsp;&nbsp;'
                                                            + obj[0].FullName + '</a><br/>');
                                    self.callModalEdit();

                                    //disable nút add
                                    $("#btnAdd").prop('disabled', true); //disable
                                }
                            }
                        }
                        else {//edit
                            if (mess[1] == 1) { //success   
                                //lay lai data fill vao cac control
                                if (mess[2] != null) {
                                    var obj = mess[2];
                                    $('#txtnamef').val(obj[0].FullName);
                                    $('#rbSex input[name=r2f]:checked').val(obj[0].Sex);
                                    $('#txtDOBf').val(obj[0].DOB);
                                    //self.setDateTimePicker($('#dpDOBf'), 'DD/MM/YYYY')
                                    $('#txtIdentifyf').val(obj[0].IdentityNumber);
                                    $('#dllQuocTichf').val(obj[0].CitizenshipCode);
                                    $('#txtaddressf').val(obj[0].Address);
                                    $('#txtEmailf').val(obj[0].Email);
                                    $('#txtMobilef').val(obj[0].Mobile);
                                    $('#txtCompanyf').val(obj[0].Company);
                                    $('#dllRoom').val(obj[0].RoomID);
                                    $('#dllOrgf').val(obj[0].GroupID);
                                    $('#dllOrgJoinf').val(obj[0].GroupJoinID);
                                    $('#cbLeader input[name=chkLeaderf]:checked').val(obj[0].Leader);
                                    $('#cbPayer input[name=chkPayerf]:checked').val(obj[0].Payer);
                                }
                            }
                        }

                        // success message
                        //self.notify(mess[0], mess[1], false);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // error message
                        //self.notify(thrownError, -1, false);
                    }
                })
            }
        });
    }

    this.DatTruoc = function () {

        if ($("#formReservation").valid()) {
            bootbox.confirm("Bạn có chắc chắn muốn đặt trước?", function (result) {
                if (!result) return;
                var resroom = self.getReservationRoomData();
                $.ajax({
                    beforeSend: function () {
                        Sv.RequestStart();
                    },
                    complete: function () {
                        Sv.RequestEnd();
                    },
                    url: '/ReservationRoom/DatTruoc',
                    type: "Post",
                    dataType: "json",
                    data: { reservation: resroom },
                    success: function (response) {
                        if (response.result >= 1) {
                            bootboxLamlt.alert("Thao tác thành công", function () {
                                var action = $("[name=action]").val();
                                var rsId = $("[name=hdReservationID]").val();
                                if (action === "uptime") {
                                    Sv.RedirectUrl("/ReservationRoom/DatPhongDetail?id=" + rsId);
                                } else {
                                    hashChangeSodophong();
                                }
                            });
                        }
                        else {
                            alert("Thao tác không thành công:" + response.mess);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // error message
                        alert("Thao tác không thành công, lỗi:" + thrownError);
                    }
                });
            });
        }

    }
    this.NhanPhong = function () {
        if ($("#formReservation").valid()) {
            bootbox.confirm("Bạn có chắc chắn muốn nhận phòng?", function (result) {
                if (!result) return;
                var resroom = self.getReservationRoomData();
                $.ajax({
                    url: '/ReservationRoom/CheckIn',
                    type: "Post",
                    beforeSend: function () {
                        Sv.RequestStart();
                    },
                    complete: function () {
                        Sv.RequestEnd();
                    },
                    dataType: "json",
                    data: { reservation: resroom },
                    success: function (response) {
                        bootboxLamlt.alert("Thao tác thành công");
                        if (response.result == 1) { 
                            if (window.islockCard && window.islockCard == 1) {
                                var url = "/Common/LockCard?roomId=" + resroom.RoomID + "&status=1";
                                new LockCardControl().showModal(url, function () { hashChangeSodophong(); });
                            } else {
                                hashChangeSodophong();
                            }  
                        }
                        else {
                            alert("Thao tác không thành công:" + response.mess);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // error message
                        alert("Thao tác không thành công:" + thrownError);
                    }
                });
            });
        }

    }

    this.setupValidate = function () {
        //validate Nhan Phong
        $("form").validate({
            rules: {
                txtname: {
                    required: true
                },
                txtIdentify: {
                    //required: true
                },
                //txtDOB: {
                //    required: true,
                //    length: 10
                //},
                txtFromDate: {
                    required: true
                },
                txtToDate: {
                    required: true
                },
                //txtAdult: {
                //    required: true
                //},
                //txtChildren: {
                //    required: true
                //},
                //txtDeposit: {
                //    required: true,
                //},
                //txtDiscount: {
                //    required: true
                //},
                //txtPrice: {
                //    required: true
                //}
            },
            messages: {
                txtname: {
                    required: "Vui lòng nhập Họ và tên"
                },
                txtIdentify: {
                    //required: "Vui lòng nhập số CMT/Passport"
                },
                //txtDOB: {
                //    required: "Vui lòng nhập Ngày tháng năm sinh",
                //    length: "Độ dài là 10 ký tự"
                //},
                txtFromDate: {
                    required: "Vui lòng nhập Ngày giờ đến"
                },
                txtToDate: {
                    required: "Vui lòng nhập Ngày giờ đi"
                },
                //txtAdult: {
                //    required: "Vui lòng nhập Số người lớn"
                //},
                //txtChildren: {
                //    required: "Để là 0 nếu không có"
                //},
                //txtDeposit: {
                //    required: "Để là 0 nếu không có Trả trước"
                //},
                //txtDiscount: {
                //    required: "Để là 0 nếu không có Giảm trừ"
                //},
                //txtPrice: {
                //    required: "Để là 0 nếu không có Giá phòng"
                //}
            },
        });
    }

    // tìm kiếm khách hàng cũ
    this.setupCustomerAutoComplete = function ($e) {

        var obj = {
            istext: true,
            onNotSelect: function () {
                //****** ko set lại form khách hàng *****
                //setFromKH(null);
                //remove lịch sử khách hàng
                self.setHistoryKH(null);
            },
            onSelect: function (item) {
                var data = (JSON.parse(item.object));
                $e.val(data.Name).change();
                self.setFromKH(data);
                // setup lịch sử
                self.setHistoryKH(data);
            },
            displayField: function (item) {
                var sdt = "", idt = "";
                if (item.Mobile != undefined)
                    sdt = " -  SĐT: " + item.Mobile;
                if (item.IdentifyNumber != undefined)
                    idt = " -  CMT: " + item.IdentifyNumber;

                return "KH: " + item.Name + sdt + idt;
            },
            valueField: "Name",
            ajax: {
                url: "/CustomerArriveManage/SelectCustomer",
                preDispatch: function (query) {
                    return { search: query, customerold: -1 };
                }
            }
        }
        Sv.TypeaheadConfig($e, obj);
    }

    // binding dữ liệu vào from KH
    this.setFromKH = function (data) {
        var isData = Sv.ObjectNotNull(data);
        //console.log(isData, data);
        //$('#txtname').val(isData ? data.Name : ""); // thằng này là thằng change-> ko đổi
        $('#Name').prop('disabled', false);
        $("#hdCusID").val(isData ? data.Id : "");
        $('#txtIdentify').val(isData ? data.IdentifyNumber : "");
        $("#dllQuocTich").val(isData && data.CountryId != null ? data.CountryId : "238");
        $("#txtaddress").val(isData ? data.Address : "");
        $("#txtMobile").val(isData ? data.Mobile : "");
        $("#txtCompany").val(isData ? data.Company : "");
        $("#txtEmail").val(isData ? data.Email : "");
        var dob = isData ? (data.DOB != null && data.DOB.length > 0 ? moment(new Date(parseInt(data.DOB.slice(6, -2)))).format("DD-MM-YYYY") : "") : "";
        $("#txtDOB").val(dob);
        $("#formReservation  :input[name='r2']").val(isData ? data.Sex : "");
    };

    // lấy thông tin đặt phòng trước đó của KH
    this.setHistoryKH = function (data) {
        var $e = $("#customerHis");
        var $eTable = $("#tableCustomerHis");
        var isData = Sv.ObjectNotNull(data);
        if ($e.length <= 0) return;

        //console.log(data);
        if (!isData) {
            // remove table nếu có           
            $e.hide();
        } else {
            $e.show();
            // setup table
            showDialogLoading();
            var tableOption = {
                "ajax": {
                    "url": "/ReservationRoom/CustomerHistoryReservationRoom",
                    "data": function (d) {
                        d.columns = "";
                        d.search = "";
                        d.customerId = data.Id;
                    }
                },
                "columns":
                [
                    { "data": null, render: function (data, type, row, infor) { return self.TableCustomerHistory.page.info().page + infor.row + 1; } },
                    { "data": "CustomerName", "orderable": "false" },
                    { "data": "BookingCode", "orderable": "false" },
                    { "data": "RoomTypeName", "orderable": "false" },
                    { "data": "RoomName", "orderable": "false" },
                    { "data": null, render: function (data, type, row, infor) { return Sv.DateToString(row.Arrive_Date, "DD-MM-YYYY HH:mm"); } },
                    { "data": null, render: function (data, type, row, infor) { return Sv.DateToString(row.Leave_Date, "DD-MM-YYYY HH:mm"); } },
                    { "data": "StatusName", "orderable": "false" },
                ],
                "initComplete": function (settings, json) {
                    hideDialogLoading();
                },
            };
            if (Sv.ObjectNotNull(self.TableCustomerHistory))
                self.TableCustomerHistory.destroy();

            self.TableCustomerHistory = Sv.SetUpDataTable($eTable, Sv.DataTableOption(tableOption));
        }
    }

    //fnReady    
    this.fnReady = function () {

        Sv.SetupDatePicker([
            { e: $("input[name='txtDOB']"), defaultDate: "" },
            { e: $("input[name='txtDOBf']"), defaultDate: "" },
            { e: $("input[name='txtFromDate']"), format: "DD/MM/YYYY HH:mm" },
            { e: $("input[name='txtToDate']"), format: "DD/MM/YYYY HH:mm" }]);

        var $fdate = $("input[name='txtFromDate']");
        //if ($fdate.length>0)
        //    $fdate.data("DateTimePicker").minDate(moment(new Date()));
        var $tdate = $("input[name='txtToDate']")
        if ($tdate.length > 0)
            $tdate.data("DateTimePicker").minDate(moment(new Date()));   
        // change Ngày giờ đến
        if ($fdate.length > 0)
            $fdate.on('dp.change', function () {
                var fnF = $("input[name='txtFromDate']").data("DateTimePicker");
                var fnT = $("input[name='txtToDate']").data("DateTimePicker");

                fnT.minDate(fnF.date());
                var dateDf = fnF.date().add(1, 'day').minute(0);
                if (hoursCheckOut)
                    dateDf.hour(hoursCheckOut)
                fnT.date(dateDf);
                console.log("change1");
                self.loadPrice();
            });
        //// change Ngày giờ đi
        //if ($tdate.length > 0)
        //    $tdate.on('dp.change', function () {
        //        // $("input[name='txtToDate']").data("DateTimePicker").hide(); 
        //        console.log(1);
        //        self.loadPrice();
        //    });

        //format number    
        var total = 0, price = 0, deposit = 0, discount = 0;
        // change giá
        if ($('#txtPrice').length > 0)
            $("#txtPrice").change(function () {
                //tinh tong
                price = $("#txtPrice").val();
                deposit = $("#txtDeposit").val();
                discount = $("#txtDiscount").val();
                total = 0;
                //tinh tong
                if ($.isNumeric(price)) {
                    total = parseFloat(total) + parseFloat(price);
                    $("div").remove(".notifyjs-container");
                }
                else {
                    self.notify('Vui lòng nhập Giá phòng dạng số.', -1, false);
                }
                if ($.isNumeric(deposit)) {
                    total = parseFloat(total) - parseFloat(deposit);
                    //alert(total);
                }
                if ($.isNumeric(discount)) {
                    if ($.isNumeric(price)) {
                        total = parseFloat(total) - (parseFloat(price) * (parseFloat(discount) / 100));
                    }
                }
                $('#Total').html(self.formatNumber(total));
                // tick thay đổi giá
                $("#changePrice").val(1);
            });
        // change trả trước
        if ($('#txtDeposit').length > 0)
            $("#txtDeposit").change(function () {
                //tinh tong
                price = $("#txtPrice").val();
                deposit = $("#txtDeposit").val();
                discount = $("#txtDiscount").val();
                total = 0;

                if ($.isNumeric(price)) {
                    total = parseFloat(total) + parseFloat(price);
                    $("#txtPrice").val(price);
                    //alert(total);
                }
                if ($.isNumeric(deposit)) {
                    total = parseFloat(total) - parseFloat(deposit);
                    $("#txtDeposit").val(deposit);
                    $("div").remove(".notifyjs-container");
                }
                else {
                    self.notify('Vui lòng nhập Trả trước dạng số.', -1, false);
                }
                if ($.isNumeric(discount)) {
                    price = $("#txtPrice").val();
                    if ($.isNumeric(price)) {
                        total = parseFloat(total) - (parseFloat(price) * (parseFloat(discount) / 100));
                    }
                    $("#txtDiscount").val(discount);
                }
                $('#Total').html(self.formatNumber(total));
            });
        // giảm trừ
        if ($('#txtDiscount').length > 0)
            $("#txtDiscount").change(function () {
                //tinh tong
                price = $("#txtPrice").val();
                deposit = $("#txtDeposit").val();
                discount = $("#txtDiscount").val();
                total = 0;

                if ($.isNumeric(price)) {
                    total = parseFloat(total) + parseFloat(price);
                    $("#txtPrice").val(price);
                    //alert(total);
                }
                if ($.isNumeric(deposit)) {
                    total = parseFloat(total) - parseFloat(deposit);
                    $("#txtDeposit").val(deposit);
                    //alert(total);
                }
                if ($.isNumeric(discount)) {
                    price = $("#txtPrice").val();
                    if ($.isNumeric(price)) {
                        total = parseFloat(total) - (parseFloat(price) * (parseFloat(discount) / 100));
                    }
                    $("#txtDiscount").val(discount);
                    $("div").remove(".notifyjs-container");
                }
                else {
                    self.notify('Vui lòng nhập Giảm trừ dạng số.', -1, false);
                }
                $('#Total').html(self.formatNumber(total));
            });
        // fix auto complete
        if ($('#formReservation #txtname').length > 0 && ($("#divBooking").length > 0 && !$("#divBooking").hasClass("clearfix")))
            self.setupCustomerAutoComplete($('#formReservation #txtname'));
        // thông tin khách hàng cũ
        if ($("#customerHis").length > 0) {
            self.setHistoryKH(null);
        }
        
        self.changeStatus();
    }

    this.init = function () {
        this.setupClick();
        this.setupValidate();
        this.fnReady();
    }

}
 