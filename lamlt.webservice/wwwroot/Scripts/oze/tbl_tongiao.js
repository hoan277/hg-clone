﻿function tbl_tongiao()
{
    var self = this;
    this.tableName = "tbl_tongiao";
    this.idModal = "modaltbl_tongiao";
    this.$table;

    this.editDialog = function (id)
    {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function ()
        {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/"+ self.tableName +"/Edit/" + id);
            $("#" + self.idModal + " button#btnSave"+ self.tableName ).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function ()
            {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
				/*
                $.post("/tbl_tongiao/update", { obj: pdata }, function (data)
                {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0)
                    {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :"+data.mess);
                    }
                });*/
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data) 
					{ 
						hideDialogLoading();						
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});
            });
            $("#"+self.idModal).off('show.bs.modal');
        });
        $("#"+self.idModal).modal("show");
    }
    this.deleteDialog = function (id)
    {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result)
        {
            if (!result) return;

            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/"+id,
                //data: JSON.stringify(pdata),
                success: function (data)
                {
						hideDialogLoading();
						if (data.result > 0) {
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                        }
                        else {
							            alert("Có lỗi khi tạo :" + data.mess);
                        }
                },
                contentType: "application/json",
                dataType: 'json'
            });
            /*$.post("/" + self.tableName + "/Delete", { id: id }, function (data)
            {
                            if (data.result > 0) 
							{
                                    bootbox.alert("Thao tác thành công", function () { self.searchGrid(); });
                        }
                        else {
                                    alert("Có lỗi khi xóa nhà cung cấp:" + data.mess);
                        }
            });
            */
        }));
    }
    this.viewDialog = function viewDialog(id)
    {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#"+self.idModal).off('show.bs.modal');
        $("#"+self.idModal+" .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function ()
        {
            $("#"+self.idModal+" .modal-body-content").load("/"+ self.tableName +"/GetViewDetail/" + id);
            $("#"+self.idModal+" button#btnSave" + self.tableName).css("display", "none");
        });
        $("#"+self.idModal).modal("show");
    }
    this.setupGrid = function ()
    {
        showDialogLoading();
        self.$table = $('#example_tbl_tongiao').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json)
            {
                hideDialogLoading();               
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/"+ self.tableName+"/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
					delete d.columns;
					
                }
            },
            "columns":
                [
				{'data': 'id'}
                ,{'data': 'name'}
                ,{'data': 'note'},
                {
                    "data": null, render: function (data, type, row)
                    {
                        var htmlMenu =
                          '<div class="edit-delete-table">' +
                              '<div class="edit-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:o'+self.tableName+'.editDialog(' + row.id + ')">' +
                                  '<img src="/images/icon/icon-edit.png" style=" border: none;" title="Chỉnh sửa">' +
                              '</div>' +
                              '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                  '<img src="/images/icon/icon-delete.png" style=" border: none;" title="Xóa">' +
                              '</div>' +
                              '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                         '<img src="/images/icon/icon-view.png" style=" border: none;" title="Thông tin chi tiết">' +
                              '</div>' +
                          '</div>';
                        return htmlMenu;
                    }
                }
                ]
        });
    }
    this.searchGrid=function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal)
    {
        var html = '<div id="'+idModal+'" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">'+self.tableName+'</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName+ '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal= function (idModal)
    {
        $('#'+idModal).modal('hide');
    }
}