﻿var $table;
$(document).ready(function () {
    toggleMenu("menu_kho");
    showDialogLoading();
    $table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "initComplete": function (settings, json) {
            hideDialogLoading();
        },
        /*bFilter: false, bInfo: false,*/
        "dom": '<"top">rt<"bottom" lpi><"clear">',
        "ajax": {
            "url": "/ProductGroup/List",
            "data": function (d) {
                delete d.columns;
                d.search = $("#txtSearch").val();
            }
        },
        "columns":
            [{
                "data": null, "orderable": "false",
                render: function (data, type, row, infor) {
                    // return $table.page.info().page * $table.page.len() + infor.row + 1;
                    return $table.page.info().page + infor.row + 1;
                }
            },
            { "data": "Name", "orderable": "false" },
            {
                "data": "Status", "orderable": "false",
                render: function (data, type, row) {
                    return row.Status == 1 ? "Sử dụng" : "Không sử dụng";
                }
            },
            { "data": "Description", "orderable": "false" },
            {
                "data": null, render: function (data, type, row) {
                    var htmlMenu = ('<div class="input-table" onclick="javascript:viewDialog(' + row.Id + ')">' +
                                            '<a class="btn btn-primary" title="Thông tin chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                        '</div>');
                    htmlMenu += ('<div class="input-table" onclick="javascript:editDialog(' + row.Id + ')">' +
                                         '<a class="btn btn-primary" title="Cập nhật" ><i class="fa fa-pencil-square-o"></i></a>' +
                                     '</div>');
                    htmlMenu += ('<div class="input-table" onclick="javascript:removeDialog(' + row.Id + ')">' +
                                         '<a class="btn btn-danger" title="Xóa" ><i class="fa fa-times-circle"></i></a>' +
                                     '</div>');
                    return htmlMenu;
                }
            }
            ]
    });

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
            return false;
        }
    });

    $("#btnSearch").off().on("click", function (e) {
        searchGrid();
    });
    $("#btnAdd").off().on("click", function (e) {
        editDialog(0);
    });
});

function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}

function viewDialog(id) {
    var modelId = "viewDialogModalId";
    Sv.SetupModal({
        modalId: modelId,
        title: "Thông tin nhóm sản phẩm",
        url: "/ProductGroup/Details",
        data: { id },
        modalclass: "modal-default",
        button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
    }, function () {

    }, function () {

    }, function () {

    });

}

function editDialog(id) {
    var modelId = "editDialogModalId";
    Sv.SetupModal({
        modalId: modelId,
        title: "Thông tin nhóm sản phẩm",
        url: "/ProductGroup/Edit",
        data: { id },
        modalclass: "modal-default",
    },
    function () { },
    function () {
        var data = getFormData($("#dummyProductGroup"));
        Sv.Post({
            url: "/ProductGroup/Update",
            data: { obj: data}
            })
        .then(function (rs) {
            if (rs.IntStatus > 0) {
                $("#" + modelId).modal("hide");
                bootboxLamlt.alert(rs.Message);
                searchGrid();
            } else {
                alert(rs.Message);
            }
        });
    },
    function () {

    });

}

function removeDialog(id) {
    confirm("Bạn chắc chắn muốn xóa?", function (cck) {
        if (!cck) return false;
        Sv.Post({
            url: "/ProductGroup/Delete",
            data: { id }
        })
        .then(function (rs) {
            if (rs.IntStatus > 0) {
                $("#" + modelId).modal("hide");
                bootboxLamlt.alert(rs.Message);
                searchGrid();
            } else {
                alert(rs.Message);
            }
        });
    });
}