﻿
var $table;
function Detail(id) {
    Sv.AjaxPost({
        Url: "/StoreInput/GetDetail",
        Data: { Id: id }
    },
            function (obj) {
                console.log(obj);
                if (obj) {
                    $("#SoPhieu").val(obj.OrderCode);
                    $("#detailid").val(obj.Id);
                    $("#SoChungTu").val(obj.SupplierCode);
                    if (obj.InputDate != null) {
                        $("#NgayNhap").val(moment(new Date(parseInt(obj.InputDate.slice(6, -2)))).format("DD-MM-YYYY"));
                    } else {
                        $("#NgayNhap").val("");
                    }
                    if (obj.DatePayment != null) {
                        $("#NgayChungTu")
                            .val(moment(new Date(parseInt(obj.DatePayment.slice(6, -2)))).format("DD-MM-YYYY"));
                    } else {
                        $("#NgayChungTu").val("");
                    }
                    $("#StoreId").val(obj.StoreID);
                    $("#Product").val(obj.ProductId);
                    $("#ProductCode").val(obj.ProductCode);
                    $("#NhaCC").val(obj.SupplierID);
                    $("#NhomDv").val(obj.CateId);
                    if (obj.ManufactureDate != null) {
                        $("#NgaySx")
                            .val(moment(new Date(parseInt(obj.ManufactureDate.slice(6, -2)))).format("DD-MM-YYYY"));
                    } else {
                        $("#NgaySx").val('');
                    }
                    if (obj.ExpirationDate != null) {
                        $("#HSD").val(moment(new Date(parseInt(obj.ExpirationDate.slice(6, -2)))).format("DD-MM-YYYY"));
                    } else {
                        $("#HSD").val('');
                    }
                    // $("#NgaySx").val(obj.OrderCode);
                    // $("#HSD").val(obj.OrderCode);
                    $("#SoLuongNhap").val(obj.Quantity);
                    $("#Unit").val(obj.UnitId);
                    $("#DonGia").val(obj.Price);
                    $("#modalDetail").modal('show');
                } else {

                }
            });


}

function SetupValidateSearch() {
    var $form = $('#formSearch').on();
    $form.on().validate({
        rules: {
            FromDate: {
                required: true
            },
            ToDate: {
                required: true
            },
        },
        messages: {
            FromDate: {
                required: 'Bạn cần chọn ngày cần tìm kiếm'
            },
            ToDate: {
                required: 'Bạn cần chọn ngày cần tìm kiếm'
            },
        }
    });
}
var pageLoad = 0;
$(document) .ready(function () {

       $("#btnRefresh") .click(function () {
           rsdata();
           if (pageLoad > 0){
               searchGrid();
           } else {
               pageLoad = 1;
           }
      });

       rsdata();
       // setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

        toggleMenu("menu_kho");
        $('#btnXuatKho').off().click(function (e) {
            window.location.href = '/kho/XuatKho';
        });

        Sv.SetupDateTime($("#FromDate"), $("#ToDate"));
        SetupValidateSearch();
        

        $("#btnSearch")
            .click(function () {
                var form = $('#formSearch').on();
                if (form.valid()) {
                    searchGrid();
                }
            });
        $("#keyword").keypress(function (e) {
            if (e.which == 13) {
                var form = $('#formSearch').on();
                if (form.valid()) {
                    searchGrid();
                }
            }
        });
        showDialogLoading();
        $table = $("#example")
            .DataTable({
                "language": {
                    "sProcessing": "Đang xử lý...",
                    "sLengthMenu": "Xem _MENU_ mục",
                    "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                    "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                    "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                    "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                    "sInfoPostFix": "",
                    "sSearch": "Tìm:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Đầu",
                        "sPrevious": "Trước",
                        "sNext": "Tiếp",
                        "sLast": "Cuối"
                    }
                },
                "processing": true,
                "serverSide": true,
                "initComplete": function (settings, json) {
                    hideDialogLoading();
                    //alert( 'DataTables has finished its initialisation.' );
                },
                /*bFilter: false, bInfo: false,*/
                "dom": '<"top">rt<"bottom" lpi><"clear">',
                "ajax": {
                    "url": "/kho/DanhSachXuatKho",
                    "data": function (d) {
                        console.log(d)
                        d.search = "";
                        d.FromDate = $("input[name='FromDate']").val();
                        d.ToDate = $("[name='ToDate']").val();
                        d.Keyword = $("#keyword").val();
                        d.columns = "";
                        //d.search = "";
                        //d.FromDate = "";
                        //d.ToDate = "";
                        //d.Keyword = "";
                    }
                },
                "columns":
                [
                    {
                        "data": null,
                        render: function (data, type, row, infor) {
                            console.log(row);
                            return $table.page.info().page + infor.row + 1;
                        }
                    },
                    { "data": "OrderCode", "orderable": "false" },
                    {
                        "data": null,
                        render: function (data, type, row, infor) {
                            return moment(new Date(parseInt(row.InputDate.slice(6, -2)))).format("DD-MM-YYYY");
                        }
                    },
                    { "data": "SupplierName", "orderable": "false" },
                    { "data": "ProductName", "orderable": "false" },
                    { "data": "ProductCode", "orderable": "false" },
                    { "data": "Unit", "orderable": "false" },
                    {
                        "data": null,
                        render: function (data, type, row, infor) {
                            return row.Price.toString().replace(".", ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
                        },
                        className: "dt-body-right"
                    },
                    { "data": "Quantity", "orderable": "false" },

                    {
                        "data": null,
                        render: function (data, type, row, infor) {
                            return "<div style='width:10px;display:inline'><a title='Thông tin phiếu xuất kho'  href='javascript:Detail(" +
                                row.Id +
                                ")'><i class='fa fa-search-plus'></i></a></div> &nbsp;" +
                            "<div style='width:10px;display:inline'><a title='In hóa đơn'  href='javascript:print(" + row.Id + ",\"" + row.OrderCode + "\")'>" +
                           "<i class='fa fa-print'></i></a></div>";
                        }
                    }
                ]
            });


        // in hóa đơn
        $("#btnDetailPrint").off().on("click", function (e) {
            var code = $("#FrmOrder #SoPhieu").val();
            var iddetail = $("#FrmOrder #detailid").val();
            var o = {
                title: "Hóa đơn xuất kho chi tiết",
            }
            new orderPrint().callView(o, "/Kho/PrintReview_XuatKho", { code: code, id: iddetail, detail: true });
        });

        $("#btnPrint").off().on("click", function (e) {
            var code = $("#FrmOrder #SoPhieu").val();
            var o = {
                title: "Hóa đơn xuất kho",
            }
            new orderPrint().callView(o, "/Kho/PrintReview_XuatKho", { code: code, id: 0, detail: false });
        });

    });
function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function rsdata() {
    if (!daysView) daysView = -30;       
    Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView)));
    Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
    $("#keyword").val("");
}


function print(id, code) {
    console.log(id, code);
    var o = {
        title: "Hóa đơn xuất kho",
    }
    new orderPrint().callView(o, "/Kho/PrintReview_XuatKho", { code: code, id: id, detail: false });

}