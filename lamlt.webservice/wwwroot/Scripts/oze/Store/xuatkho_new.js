﻿// validate file type
$.validator.addMethod("filetype", function (value, element) {
    if (value.length == 0)
        return true;
    var type = $(element)[0].type;
    if (type != "file")
        return true;
    var fileTyle = [
        "application/vnd.ms-excel", //xls
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" //xlsx
    ];
    var files = $(element)[0].files;
    for (var i = 0; i < files.length; i++) {
        var itemFile = files[i];
        var ck = fileTyle.indexOf(itemFile.type);
        if (ck < 0) {
            return false;
        }
    }
    return true;
}, "");
// validate file size
$.validator.addMethod("filesize", function (value, element) {
    if (value.length == 0)
        return true;
    var type = $(element)[0].type;
    if (type != "file")
        return true;
    var files = $(element)[0].files;
    for (var i = 0; i < files.length; i++) {
        var itemFile = files[i];
        // 200MB = 200000000 bytes
        if (itemFile.size > 200000000) {
            return false;
        }
    }
    return true;
}, "");
// validate count file
$.validator.addMethod("filesmax", function (value, element) {
    if (value.length == 0)
        return true;
    var type = $(element)[0].type;
    if (type != "file")
        return true;
    var files = $(element)[0].files;
    if (files.length > 10)
        return false;
    else
        return true;
}, "");
var Products = [];
var index = 0;


var Input = function () {
    var base = this;
    this.$table = $("#table");
    this.tableResult = $("#tableResult");
    this.$tableview = $("#tableview");
    this.$tableEdit = $("#tableEdit");
    this.tableImportResult = $("#tableImportResult");
    this.$table = $("#table");
    this.$btnOpenAdd = $("#btnAdd");
    this.$btnAddFile = $("#btnAddFile");
    this.$btnupload = $("#btnupload");
    this.$modalAdd = $("#addModal");
    this.$fileupload = $('#fileupload');

    this.ResetForm = function (form) {
        console.log("clear");
        if (form.length) {
            // $("#StoreId").val("");

            $("#Product").val("");
            $("#ProductCode").val("");

            $("#Unit").val("");
            $("#NhaCC").val("");
            $("#NhomDv").val("");

            $("#TK").val("");
            $("#TKCP").val("");
            Sv.SetupDatePicker([{ e: $("#NgaySanXuat") }, { e: $("#HanSuDung") }]);

            $("#SoLuongNhap").val("0");
            $("#DonGia").val("0");
            $("#DonGiaLast").val("0");

            $("#Tax").val("0");
        }
        Sv.SetupInputMask();
    };

    this.Save = function () {
        if ($("#FrmOrder").valid()) {
            if (Products.length == 0) {
                alert("Vui lòng chọn sản phẩm nhập kho");
                return;
            }
            Dialog.ConfirmCustom("Xác nhận", "Bạn có chắc chắn nhập kho đơn hàng này?", function () {
                var order = base.getOrder();
                Sv.AjaxPost({
                    Url: "/StoreInput/Add",
                    Data: { order: order, orderdetails: Products }
                },
                   function (rs) {
                       if (rs.ResponseCode === "01") {
                           Dialog.Alert(rs.Message, Dialog.Success, function () { location.href = "/StoreInput/PhieuNhapKho"; });
                       } else {
                           Dialog.Alert(rs.Message, Dialog.Error);
                       }
                   });
            })
        }
        return;
    };

    this.preview = function () {
        if ($('#product-table tbody tr').length) {
            var order = base.getOrder();
            new orderPrint().callView({ title: "Xem hóa đơn nhập kho" }, "/StoreInput/PreView", { order: order, orderdetails: Products }, function () {
                $("#btnSave_orderPrintxxx").hide();
            });
        } else {
            bootboxLamlt.alert('Chưa có sản phẩm nào');
        }

    }

    this.saveInputEdit = function () {
        if ($("#FrmOrder").valid()) {
            if (Products.length == 0) {
                alert("Vui lòng chọn sản phẩm nhập kho");
                return;
            }
            Dialog.ConfirmCustom("Xác nhận", "Bạn có chắc chắn chỉnh sửa đơn hàng này?", function () {
                var order = base.getOrder();
                Sv.AjaxPost({
                    Url: "/StoreInput/Edit",
                    Data: { order: order, orderdetails: Products }
                }, function (rs) {
                    if (rs.IntStatus === 1) {
                        Dialog.Alert(rs.Message, Dialog.Success, function () {
                            location.href = "/StoreInput/PhieuNhapKho"; // "/StoreInput/index";
                        });
                    } else {
                        Dialog.Alert(rs.Message, Dialog.Error);
                    }
                });
            })
        }
        return;
    };

    this.getOrder = function () {
        var order = {};
        order.IdDetails = $("#IdDetails").val();

        order.loaiNhapKho = $("#loaiNhapKho").val();
        order.loaiNhapKhoName = $("#loaiNhapKho :selected").text();
        order.SoPhieu = $("#SoPhieu").val();
        order.SoPhieu1 = $("#SoPhieu1").val();
        order.NgayNhapHD = $("input[name=NgayNhapHD]").val();

        order.chiphi = $("#ChiPhi").val();
        order.payment_type = $("#payment_type").val();
        order.NgayChungTu = $("input[name=NgayChungTu]").val();
        order.SoChungTu = $("#SoChungTu").val();

        order.tk311 = $("#tk311").val();
        order.note = $("#note").val();

        order.giaohang_ten = $("#giaohang").val();
        order.giaohang_donvi = $("#dvgiaohang").val();
        order.giaohang_diachi = $("#diachidv").val();

        return order;
    }

    this.ChangeProduct = function () {
        $("#ProductCode").val("");
        $("#NhaCC").val("");
        $("#NhomDv").val("");
        $("#Unit").val("");
        $("#DonGia").val("");
        var id = $("#Product").val() != "" ? $("#Product").val() : "-1";
        if (id == "-1") return;
        Sv.AjaxPost({
            Url: "/StoreInput/GetProductInfo",
            Data: { ProductId: id }
        }, function (respon) {
            if (respon) {
                var rs = respon.product;
                var unit = respon.unit;
                var p = respon.priceOrderLast;

                $("#Unit").empty();
                $("#Unit").append("<option value=\"\">Chọn ĐVT</option>");
                //add DVT
                if (unit) {
                    for (var i = 0; i < unit.length; i++) {
                        $("#Unit").append("<option value=\"" + unit[i].Id + "\">" + unit[i].Name1 + "</option>");
                    }
                } else {
                    $("#Unit").append("<option value=\"" + rs.UnitID + "\">" + rs.UnitName + "</option>");
                }
                // sp
                if (rs) {
                    $("#ProductCode").val(rs.Code);
                    $("#NhaCC").val(rs.SupplierID);
                    $("#NhomDv").val(rs.ProductGroupID);
                    $("#Unit").val(rs.UnitID);
                    $("#DonGia").val(rs.PriceOrder);

                    $("#DonGiaLast").val(Sv.NumberToString(p));
                }

            } else {
            }
            var validator = $("#formProduct").validate();
            validator.element("#Product");
            validator.element("#DonGia");
        }, function () { });
    };

    // xác định giá nhập
    this.ChangeUnit = function () {

        $("#DonGia").val("");
        var id = $("#Product").val() != "" ? $("#Product").val() : "-1";
        var unitId = $("#Unit").val() != "" ? $("#Unit").val() : "-1";
        Sv.AjaxPost({
            Url: "/StoreInput/GetProductOrderPrice",
            Data: { productId: id, unitId: unitId }
        }, function (respon) {
            if (respon) {
                $("#DonGia").val(respon.priceOrder);
                $("#DonGiaLast").val(Sv.NumberToString(respon.priceOrderLast));
            } else {
                $("#DonGia").val(0);
            }
        });
    };

    this.AddProduct = function () {
        if ($("#formProduct").valid()) {

            var obj = {};
            obj.ID = index;

            obj.StoreId = $("#formProduct #StoreId").val();
            obj.StoreName = $("#formProduct #StoreId :selected").text();

            obj.ProductId = $("#formProduct #Product").val();
            obj.ProductCode = $("#formProduct #ProductCode").val();
            obj.ProductName = $("#formProduct #Product :selected").text();

            obj.UnitId = $("#formProduct #Unit").val();
            obj.UnitName = $("#formProduct #Unit :selected").text();

            obj.CateId = $("#formProduct #NhomDv").val();
            obj.CateName = $("#formProduct #NhomDv :selected").text();

            obj.SupplierId = $("#formProduct #NhaCC").val();
            obj.SupplierName = $("#formProduct #NhaCC :selected").text();

            obj.TK = $("#formProduct input[name=TK]").val();
            obj.TKCP = $("#formProduct input[name=TKCP]").val();

            obj.NgaySanXuat = $("#formProduct input[name=NgaySanXuat]").val();
            obj.HanSuDung = $("#formProduct input[name=HanSuDung]").val();

            var p = $("#formProduct #DonGia").val();
            obj.Price = parseFloat(p ? p.toString().replace(',', '.') : "0"); // giá nhập chưa thuế
            obj.PriceStr = obj.Price + "";

            var q = $("#formProduct #SoLuongNhap").val();
            obj.Quantity = parseFloat(q ? q.toString().replace(',', '.') : "0");

            var t = $("#formProduct #Tax").val();
            obj.Tax = parseFloat(t ? t.toString().replace(',', '.') : "0");
            obj.TaxStr = obj.Tax + "";

            obj.Total = base.getTotal(obj.Quantity, obj.Price, obj.Tax);
            obj.TotalStr = obj.Total + "";

            if (obj.Quantity <= 0) {
                alert("Số lượng nhập phải lớn hơn 0");
                return;
            }

            // Math.round((obj.Price * obj.Quantity) + Math.round(((obj.Tax / 100) * obj.Price) * obj.Quantity));

            var kiemtrasp = 0;
            $.each(Products, function (i, x) {
                if (x.StoreId == obj.StoreId && x.ProductId == obj.ProductId) {
                    kiemtrasp = 1;
                }
            });
            // update dữ liệu sp
            if (kiemtrasp == 1) {
                Products = Products.filter(function (x) { return !(x.StoreId == obj.StoreId && x.ProductId == obj.ProductId) });
                Products.push(obj);
                bootboxLamlt.clear();
                bootboxLamlt.alert("Update dữ liệu sản phẩm: " + obj.ProductName + " cho kho: " + obj.StoreName + "!");
                base.updateProductHtml(obj);
            }
                // thêm mới sp
            else {
                Products.push(obj);
                base.addProductHtml(obj);
            }
            base.footerTable();
            base.ResetForm($("#formProduct"));
            $("#Product").trigger('change.select2');
            index++;
        }
    };
    this.addProductHtmlContent = function (obj) {
        var html = "<td> " + obj.ID + " </td> " +
                        "<td> " + obj.StoreName + " </td> " +
                        "<td> " + obj.ProductName + " </td> " +
                        "<td> " + obj.ProductCode + " </td> " +
                        "<td> " + obj.UnitName + " </td> " +
                        "<td> " + obj.CateName + " </td> " +
                        //"<td> " + obj.TKCP + " </td> " +
                        "<td class=\"text-right\"> " + Sv.NumberToString(obj.Quantity) + " </td> " +
                        "<td class=\"text-right\"> " + Sv.NumberToString(obj.Price) + " </td> " +
                        "<td class=\"text-right\"> " + Sv.NumberToString(obj.Tax) + " </td> " +
                        "<td class=\"text-right\"> " + Sv.NumberToString(obj.Total) + "</td> " +
                        "<td><i class='fa fa-pencil-square-o curpoi' onclick='unit.editProduct(" + obj.ProductId + "," + obj.StoreId + ")' aria-hidden='true'></i> &nbsp; <i class='fa fa-times curpoi' onclick='unit.removeProduct(" + obj.ID + "," + obj.ProductId + "," + obj.StoreId + ")' aria-hidden='true'></i></td>";
        return html;

    };
    this.addProductHtml = function (obj) {
        var newRowContent =
                    "<tr data-id='" + obj.ID + "' data-product=" + obj.ProductId + " data-store=" + obj.StoreId + "> " +
                       base.addProductHtmlContent(obj) +
                    "</tr>";
        $("#product-table tbody").append(newRowContent);
    }

    this.updateProductHtml = function (obj) {
        var $e = $("[data-store=" + obj.StoreId + "][data-product=" + obj.ProductId + "]");
        $e.html(base.addProductHtmlContent(obj));
    }


    this.editProduct = function (id, storeid) {
        var data = Products.filter(function (x) { return (x.ProductId == id && x.StoreId == storeid) });
        var d = data[0];
        var $f = $("#formProduct");
        $f.find("#StoreId").val(d.StoreId);
        $f.find("#Product").val(d.ProductId).trigger("change");
        var setIntervalxxxxx = setInterval(function () { Sv.RequestStart(); }, 1);
        setTimeout(function () {
            $f.find("#Unit").val(d.UnitId);
            $f.find("#TK").val(d.TK);
            $f.find("#TKCP").val(d.TKCP);
            $f.find("#NgaySanXuat").val(d.NgaySanXuat);
            $f.find("#HanSuDung").val(d.HanSuDung);
            $f.find("#SoLuongNhap").val(d.Quantity);
            $f.find("#DonGia").val(d.Price);
            $f.find("#Tax").val(d.Tax);
            $("#formProduct").valid();
            Sv.RequestEnd();
            clearInterval(setIntervalxxxxx);
        }, 800);

    }

    this.removeProduct = function (index, id, storeid) {
        Dialog.ConfirmCustom("Xác nhận", "Bạn có chắc xóa sản phẩm này?", function () {
            $("[data-store=" + storeid + "][data-product=" + id + "]").remove();
            Products = Products.filter(function (x) { return !(x.ProductId == id && x.StoreId == storeid) });
            base.footerTable();
        });
    }

    this.SetupValidate = function () {
        var $form = $("#FrmOrder").on();
        $form.validate({
            rules: {
                NgayNhapHD: {
                    required: true,
                    //comparedatewithNow: true
                },
                Tax: {
                    priceMin: true
                }
            },
            messages: {
                NgayNhapHD: {
                    required: 'Ngày nhập không được bỏ trống',
                    // comparedatewithNow: 'Ngày nhập không được nhỏ hơn hiện tại '
                },
                Tax: {
                    priceMin: "Thuế không được phép âm"
                }
            }
        });
    }

    this.AddProductFromSv = function (obj) {
        obj.ID = index;
        var p = obj.Price ? obj.Price : 0;
        var t = obj.Tax ? obj.Tax : 0;
        var q = obj.Quantity ? obj.Quantity : 0;

        obj.Total = base.getTotal(q, p, t);

        Products.push(obj);
        base.addProductHtml(obj);
        base.footerTable();
        index++;
    };

    this.UploadFileError = function (obj) {
        var newRowContent =
                   "<tr data-id='" + obj.ID + "'> " +
                       "<td> " + obj.ID + " </td> " +
                       "<td> " + obj.StoreName + " </td> " +
                       "<td> " + obj.ProductCode + " </td> " +
                       "<td> " + obj.ProductName + " </td> " +
                       "<td> " + obj.UnitName + " </td> " +
                       "<td class=\"text-right\"> " + Sv.NumberToString(obj.Quantity) + " </td> " +
                       "<td class=\"text-right\"> " + Sv.NumberToString(obj.Price) + " </td> " +
                       "<td> " + obj.mgs + "</td> " +
                   "</tr>";
        return newRowContent;
    };

    this.footerTable = function () {
        var total = 0;
        $.each(Products, function (i, obj) { total += obj.Total; });
        $("#product-table  tbody> tr").each(function (i, e) {
            $(e).find("td:first").html(i + 1);
        });
        $("#product-table tfoot").empty();
        $("#product-table tfoot").append("<tr><td colspan=\"10\" class=\"text-right\">Tổng tiền</td><td colspan=\"2\" class=\"text-right\">" + Sv.NumberToString(total) + "</td></tr>");
    }

    this.UploadData = [];

    this.ValidateFile = function () {
        var $form = $("#formadd");
        $form.validate({
            ignore: [],
            rules: {
                UploadFile: {
                    required: true,
                    filetype: true,
                    filesize: true
                }
            },
            messages: {
                UploadFile: {
                    required: function () {
                        bootboxLamlt.alert('Vui lòng chọn file cần tải lên');
                        return 'Vui lòng chọn file cần tải lên';
                    },
                    filetype: function () {
                        bootboxLamlt.alert('File tải lên không đúng định dạng');
                        //clear
                        return 'File tải lên không đúng định dạng';
                    },
                    filesize: function () {
                        bootboxLamlt.alert('File tải lên dung lượng quá lớn');
                        //clear
                        return 'File tải lên dung lượng quá lớn';
                    }
                }
            }
        });
    };

    this.Setupvalidate2 = function () {
        var $form = $('#formProduct').on();
        $form.validate({
            rules: {
                Tax: {
                    priceMin: true
                }
            },
            messages: {
                Tax: {
                    priceMin: "Thuế không được phép âm"
                }
            }
        });
    }

    this.Uploadfile = function () {
        var data = new FormData();
        var obj = {},
            $form = $("#formadd").on();
        var files = $form.find("input[name='UploadFile']")[0].files;
        for (var i = 0; i < files.length; i++) {
            data.append("file" + i, files[i]);
        }
        Sv.AjaxPostFile({
            Url: '/Store/UploadFileExcelStoreInput',// '/Store/UploadFileExcel'
            Data: data
        }, function (rs) {
            if (rs.IntStatus == 1) {
                var htmlError = "<table id=\"examplexxxx\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">" +
                                "<thead>" +
                                    "<tr>" +
                                        " <th>STT</th>" +
                                        " <th>Kho</th>" +
                                        " <th>Mã SP</th>" +
                                        " <th>Tên SP</th>" +
                                        " <th>ĐVT</th>" +
                                        " <th>Số lượng</th>" +
                                        " <th>Đơn giá</th>" +
                                        " <th>Lỗi</th>" +
                                    " </tr>" +
                                " </thead>";

                var err = 0;
                for (var j = 0; j < rs.Data.length; j++) {
                    if (rs.Data[j].Status) {
                        base.AddProductFromSv(rs.Data[j]);
                    } else {
                        err = 1;
                        htmlError += base.UploadFileError(rs.Data[j]);
                    }
                }
                if (err == 1) {
                    htmlError += "</table>";
                    alert(htmlError);
                }
            } else {
                Dialog.Alert(rs.Message, (rs.Status == "01" ? Dialog.Success : Dialog.Errsor), function () { });
            }
        }, function () {
            Dialog.Alert("Có lỗi xảy ra trong quá trình xử lý", Dialog.Error);
        });
    };

    this.LoadUploadResult = function () {
        //base.tableImportResult.bootstrapTable('load', base.UploadData);
        var totalOk = 0;
        var totalFail = 0;
        $.each(base.UploadData, function (i) {
            if (base.UploadData[i].Status == true) {
                totalOk++;
            } else {
                totalFail++;
            }
        });
        if (totalOk == 0) {
            $("#btnConfirm").prop('disabled', 'disabled');
        } else {
            $("#btnConfirm").removeAttr('disabled', 'disabled');
        }
    }

    this.init = function () {
        // select2
        $("#Product").select2({
            width: '100%',
            "language": {
                "noResults": function () {
                    return "Không tìm thấy sản phẩm";
                }
            }
        });
        $("#Product").change(function () {
            base.ChangeProduct();
        })

        Sv.SetupInputMask();
    }

    this.newProduct = function () {
        editDialog(0, function () {
            // load lại sản phẩm
            Sv.AjaxPost({
                Url: "/StoreInput/GetallProducts",
                Data: {}
            }, function (d) {
                if (d.length > 0) {
                    var html = '<option value="">Chọn sản phẩm </option>';
                    $.each(d, function (i, x) {
                        console.log(x);
                        html += '<option value="' + x.ID + '">' + x.Name + ' </option>';
                    });
                    $("#Product").html(html);
                    base.select2Product();
                }
            });


        });
    }

    this.getTotal = function (quantity, price, tax) {
        var total = Math.round(price * quantity) + Math.round((tax / 100) * price * quantity);
        return total;
    }




};
var unit = null;
$(document).ready(function () {
    unit = new Input();
    unit.SetupValidate();
    unit.Setupvalidate2();
    unit.ValidateFile();
    unit.init();

    unit.$btnAddFile.click(function () {
        document.getElementById("fileupload").value = "";
        unit.$fileupload.trigger('click');
    });

    $("#btnDowloadFile").click(function (e) {
        window.location = "/Store/DownloadTemplate?file=template-nhapkho.xlsx";
    });

    unit.$fileupload.on('change', function () {
        var $form = $("#formadd").on();
        if ($form.valid()) {
            unit.Uploadfile();
        }
    });

    // chang unit
    $("#Unit").change(function () {
        unit.ChangeUnit();
    });

    $("#btnOk").click(function () {
        unit.AddProduct();
    });

    $("#btnSaveExport").click(function () {
        unit.Save();
    });
    $("#btnPreview").click(function () {
        unit.preview();
    });

    //$("#add_product").off("click").click(function () {
    //    unit.newProduct();
    //});

    $("#btnback").click(function () {
        window.location.href = "/StoreInput/PhieuNhapKho";
    });
});


function edit(data) {
    //console.log(data);
    var d = data[0];
    $("#IdDetails").val(d.Id);
    $("#loaiNhapKho").val(d.loainhap);
    // $("#loaiNhapKho").prop("disabled", true)
    $("#SoPhieu").val(d.OrderCode);
    $("#SoPhieu1").val(d.OrderCodeFake);
    $("input[name=NgayNhapHD]").val(Sv.DateToString(d.InputDate));

    $("#ChiPhi").val(d.chiphi);
    $("#payment_type").val(d.PaymentTypeID);
    $("#SoChungTu").val(d.SupplierCode ? d.SupplierCode : "");
    $("input[name=NgayChungTu]").val(Sv.DateToString(d.DatePayment));

    $("#tk311").val(d.tk311 ? d.tk311 : "");
    $("#note").val(d.note ? d.note : "");

    $("#giaohang").val(d.giaohang_ten);
    $("#dvgiaohang").val(d.giaohang_diachi);
    $("#diachidv").val(d.giaohang_donvi);
    var unit = new Input();
    index = Products ? Products.length : 0;
    $.each(data, function (i, x) {
        var obj = {};
        obj.ID = index + 1;

        obj.StoreId = x.StoreID;
        obj.StoreName = x.StoreName;

        obj.ProductId = x.ProductId;
        obj.ProductCode = x.ProductCode;
        obj.ProductName = x.ProductName;

        obj.UnitId = x.UnitId;
        obj.UnitName = x.Unit;

        obj.CateId = x.ProductGroupId;
        obj.CateName = x.ProductGroupName;

        obj.SupplierId = x.SupplierID;
        obj.SupplierName = x.SupplierName;

        obj.TK = x.tk ? x.tk : "";
        obj.TKCP = x.tkcp ? x.tkcp : "";

        obj.NgaySanXuat = Sv.DateToString(d.ManufactureDate);
        obj.HanSuDung = Sv.DateToString(d.ExpirationDate);

        obj.Price = x.Price;
        obj.PriceStr = obj.Price + "";


        obj.Quantity = x.Quantity;

        obj.Tax = x.Tax;
        obj.TaxStr = obj.Tax + "";


        obj.Total = unit.getTotal(obj.Quantity, obj.Price, obj.Tax);
        obj.TotalStr = obj.Total + "";
        Products.push(obj);
        unit.addProductHtml(obj);

        index++;
    });

    unit.footerTable();
    Sv.SetupInputMask();
    $("#btnSaveExport").html("Lưu chỉnh sửa").attr("Id", "btnSaveEditExport");
    $("#btnSaveEditExport").click(function () { unit.saveInputEdit() });

}
