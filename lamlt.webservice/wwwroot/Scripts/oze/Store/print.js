﻿
var orderPrint = function () {
    var _this = this;

    this.callView = function (o, url, data, callback) {
        var obj = {
            modalId: "orderPrintxxx",
            title: o.title,
            url: url,
            data: data,
            modalclass: "modal-full-width"
        };
        Sv.SetupModal(obj, function () {
            $("#btnSave_" + obj.modalId).html("In");
            $("#modalDetail").modal("hide");
        }, function () {
            _this.htmlPrint($("#" + obj.modalId + " .modal-body"))
        }, function () { 
            //$("#orderPrintxxx").off('hide.bs.modal').on('hide.bs.modal', function () {
            //    //if( $("#modalDetail").length > 0)
            //    //    $("#modalDetail").modal('show');
            //    //Sv.ResetModalOpen();
            //});
            if (typeof callback === "function")
                callback();
        });

    }
    // gethtml
    this.getPrintHtml = function ($e) {
        var html = "";
        html += ('<html>');
        html += ('<head>');
        html += ('<link rel="stylesheet" href="/Content/bootstrap.min.css" type="text/css" />');
        html += ('<link rel="stylesheet" href="/Content/hungpvCustom.css" type="text/css" />');
        html += ('<link rel="stylesheet" href="/Content/hungpvprint.css" type="text/css" />');
        html += ('</head>');
        html += ('<body class="container-fluid" onload="window.print()" >');
        html += ('<button class="button-print btn btn-primary noprint" onclick="window.print();"><i class="glyphicon glyphicon-print"></i></button>');
        html += ('<div class="row">'); 
        html += $e.html(); 
        html += ("</div>");
        //html += ("<script src='/Scripts/print/autoprint.js'></script>");
        html += ('</body>');
        html += ('</html>');
        return html; 
    }

    this.htmlPrint = function ($e) {
        var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
        mywindow.document.write(_this.getPrintHtml($e));
        mywindow.document.close();
        mywindow.focus();

        setTimeout(function () {
            mywindow.close();
        },500);
    }


} 