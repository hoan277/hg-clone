﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}
function editDialog(id) {

    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        $("#myModal .modal-body-content").load("/function/Edit/" + id);
        $("#myModal button#btnSave").css("display", "inline");
        $("#btnSave").off("click");
        $("#btnSave").click(function () {
            if (!$("#dummySupplier").valid()) return;
            var pdata = getFormData($("#dummySupplier")); 
            showDialogLoading();
                $.post("/function/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo chức năng");
                    }
                });
        });
    });
    $("#myModal").modal("show");
}
function deleteDialog(id) {
    bootbox.confirm("Bạn có chắc muốn xóa chức năng này không ?", function (result) {
        if (result) {
            showDialogLoading();
            $.post("/function/Delete", { id: id }, function (data) {
                hideDialogLoading(); 
                if (data.result > 0) {
                    bootbox.alert("Xóa chức năng thành công.", function () {
                        searchGrid();
                    });
                }
                else {
                    alert("Xóa chức năng thất bại!");
                }
            });
        }
    }); 
}
 
function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/function/GetDetail/" + id);
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}
