﻿
var $table;
$(document).ready(function () {

    reportDate();
    setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

    function rsdata() {
        if (!daysView) daysView = -30;
        Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView))); 
        Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));

        $("#keyword").val("");
        $("#ddlRoomTypeID").val("0");
        $("#ddlRoomID").val("0");
    }


    $("#btnRefresh").click(function () {
        rsdata();
        $table.ajax.reload();
    });

    $("#btnSearch").click(function () {
        $table.ajax.reload();
    });

    $(".box-body").keypress(function (e) {
        if (e.which == 13) {
            $table.ajax.reload();
        }
    });

    $("#btnExport").click(function () {
        exportExcel();
    });

    var tableOption = {
        "ajax": {
            "url": "/Report/ListHuyNhanPhong",
            "data": function (d) {
                d.search = "";
                d.FromDate = $("input[name='FromDate']").val();
                d.ToDate = $("[name='ToDate']").val();
                d.CustomerName = $("#keyword").val();
                d.RoomTypeId = $("#ddlRoomTypeID").val();
                d.RoomTypeName = $("#ddlRoomTypeID option:selected").text();
                d.RoomId = $("#ddlRoomID").val();
                d.RoomName = $("#ddlRoomID  option:selected").text();
                d.columns = "";
            }
        },
        "columns":
        [
            {
                "data": null, render: function (data, type, row, infor) {
                    //console.log($table.page.info());
                    return $table.page.info().page + infor.row + 1;
                }
            },
            { "data": "BookingCode", "orderable": "false" },
            { "data": "CustomerName", "orderable": "false" },
            {
                "data": null,
                render: function (data, type, row, infor) {
                    return Sv.DateToString(row.CancelDate, "DD/MM/YYYY HH:mm");
                }
            },
            { "data": "RoomTypeName", "orderable": "false" },
            { "data": "RoomName", "orderable": "false" },
            {
                "data": null,
                render: function (data, type, row, infor) {
                    //console.log(row);
                    var htmlMenu = ('<div class="input-table" onclick="javascript:viewCheckInDetails(' + row.Id+ ')">' +
                               '<a href="javascript:void()" class="btn btn-primary" title="Chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                           '</div>');
                    return htmlMenu;
                }
            },
        ],
        "initComplete": function (settings, json) {
            //var api = this.api();
            //$(api.column(4).footer()).html("Tổng: " + json.totalAmount.toString().replace(".", ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        },
    };

    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));

});

function exportExcel() {
    window.open("/Report/ExportBaoCaoHuyNhanPhong");
}

function viewCheckInDetails(id) {
    Sv.SetupModal({
        id: "modalDetails",
        url: "/ReservationRoom/CheckInDetail/"+id,
        title: "Chi tiết hủy nhận phòng"
    },
        function () {
            //$("#btnSave").html("<i class='fa fa-save'></i> Lưu").show();
        }, // open
        function () { // save
            //if ($("#formAddTransferExpense").valid()) {
            //    var data = Sv.FormGetData($("#formAddTransferExpense"));
            //    //console.log(data, t);
            //    Sv.AjaxPost({
            //        url: "/TransferExpense/SaveAdd",
            //        data: { model: data }
            //    }, function (respon) {
            //        if (respon.Status == "1") {
            //            bootboxLamlt.alert("Thao tác thành công", function () {
            //                $("#modalDetails").modal("hide");
            //                $table.ajax.reload();
            //            });
            //        }
            //        else {
            //            bootboxLamlt.alert("Có lỗi:" + respon.Message);
            //        }
            //    });
            //}
        });
}