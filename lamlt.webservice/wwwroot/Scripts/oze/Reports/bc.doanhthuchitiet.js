﻿
var $table;

function rsdata() {
    if (!daysView) daysView = -30;
    Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView)));
    Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
    $("#ddlRoomTypeID").val(0);
    $("#ddlRoomID").val(0);
}

var pageLoad = 0;
$(document).ready(function () {

    $("#btnRefresh").click(function () {
        rsdata();
        if (pageLoad > 0) {
            searchGrid();
        } else {
            pageLoad = 1;
        }
    });
    rsdata();

    $("#btnSearch")
        .click(function () {
            searchGrid();
        });
    $("#btnRefresh")
       .click(function () {
           rsdata();
           searchGrid();
       });
    $("#btnExport")
            .click(function () {
                exportExcel();
            });
    //$("#isCountry")
    //    .change(function () {
    //        searchGrid();
    //    });

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
        }
    });
    showDialogLoading();
    var tableOption = {
        "ajax": {
            "url": "/Report/DataBaoCaoChiTietDoanhThu",
            "data": function (d) {
                d.columns = "";
                d.FromDate = $("input[name='FromDate']").val();
                d.ToDate = $("input[name='ToDate']").val();
                d.RoomType = $("[name='ddlRoomTypeID']").val();
                d.RoomTypeName = $("[name='ddlRoomTypeID'] option:selected").text();
                d.Room = $("[name='ddlRoomID']").val();
                d.RoomName = $("[name='ddlRoomID'] option:selected").text();
                d.search = $("#searchReport").val();
            }
        },
        "columns":
            [
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return $table.page.info().page + infor.row + 1;
                    }
                },
                {
                    "data": "ordercode",
                    render: function (data, type, row, infor) {
                        if (window.hotelConfig.orderNumber && window.hotelConfig.orderNumber == 1) {
                            return getSaleOrderNumber(data);
                        } else {
                            return data;
                        }
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return Sv.DateToString(row.DatePayment, "DD/MM/YYYY HH:mm");
                    }
                },
                {
                    "data": "CustomerName",
                },
                {
                    "data": "RoomName",
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return Sv.NumberToString(row.TienPhong);
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return Sv.NumberToString(row.TienDv);
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        if (row.Discount == 0)
                            return "0";
                        var typePrice = "VND";
                        if (row.discountType == 1)
                            typePrice = "%";
                        return Sv.NumberToString(row.Discount) + "" + typePrice;
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        if (row.upPrice == 0)
                            return "0";
                        var typePrice = "VND";
                        if (row.discountType == 1)
                            typePrice = "%";
                        return Sv.NumberToString(row.upPrice) + "" + typePrice;
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return Sv.NumberToString(row.Deduction);
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return Sv.NumberToString(row.Deposit);
                    }
                },
                {
                    "data": "PaymentName",
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return Sv.NumberToString(row.TotalAmount);
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        var tt = row.TotalAmount - row.Deduction - row.Deposit;
                        return Sv.NumberToString(tt);
                    },
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        var htmlMenu = ('<div class="input-table" onclick="javascript:viewOrderInfo(' + row.id + ',' + row.CheckInID + ')">' +
                                    '<a  class="btn btn-primary btn-flat" title="Chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                '</div>');
                        if ((window.hotelConfig.undoCheckout && window.hotelConfig.undoCheckout == 1) && (isUserAdmin.toLowerCase() == "true")) {
                            htmlMenu += ('<div class="input-table" onclick="javascript:undoCheckout(' + row.id + ',\'' + row.RoomName + '\',' + row.CheckInID + ')">' +
                                    '<a  class="btn btn-danger btn-flat" title="Undo checkout" ><i class="fa fa-undo"></i></a>' +
                                '</div>');
                        }
                        return htmlMenu;
                    }
                },
            ],
        "initComplete": function (settings, json) {
            hideDialogLoading();
            setTableFooter($table, json.recordsTotal, json.totalAmount, json.total);

        },
    };
    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));

});

//thông tin  order 
function viewOrderInfo(orderId, checkinId) {
    if (window.hotelConfig.jsPrintFn) {
        window[window.hotelConfig.jsPrintFn + "_bc"](orderId, checkinId);
    } else {

        $("#modalDetails").off('show.bs.modal').on('show.bs.modal', function () {
            $("#TittleBox").html("Thông tin hóa đơn");
            $("#modalDetails .modal-body-content").html('<p>loading..</p>');
            Sv.AjaxPost({
                url: "/CustomerCheckOut/ViewSaleOrder",
                data: { orderId: orderId, checkinId: checkinId }
            }, function (rs) {
                $("#modalDetails .modal-body-content").html(rs);
                Sv.SetupNumberMask([{ e: $(".amount-mask") }]);
                Sv.SetScrollHeight($(".box-item"));
                Sv.SetupInputMask();
            });
        }).modal("show");
    }
}

function setTableFooter($t, v1, v2, v3) {
    $($t.column(1).footer()).html("");
    if (v1 != undefined)
        $($t.column(1).footer()).html("Tổng số lượng: " + convert2Money(v1.toString()));
    else
        $($t.column(1).footer()).html("Tổng số lượng: 0");
    if (v2 != undefined)
        $($t.column(2).footer()).html("Tổng tiền: " + convert2Money(v2.toString()) + " VND");
    else
        $($t.column(2).footer()).html("Tổng tiền: 0");
    if (v3 != undefined)
        $($t.column(7).footer()).html("Tổng thanh toán: " + convert2Money(v3.toString()) + " VND");
    else
        $($t.column(7).footer()).html("Tổng thanh toán: 0");
}

function searchGrid() {
    $table.ajax.reload(function (data) {
        setTableFooter($table, data.recordsTotal, data.totalAmount, data.total);
    });
}

function exportExcel() {
    window.open("/Report/ExportBaoCaoChiTietDoanhThu");
}

function undoCheckout(orderId, roomName, checkinId) {
    confirm("Bạn chắc chắn muốn hoàn tác lại hóa đơn thanh toán của phòng:<b>" + roomName + "</b> không?", function (res) {
        if (res) {
            Sv.Post({
                url: "/Common/UndoCheckout",
                data: { orderId: orderId, checkinId: checkinId }
            }, function (rs) {
                if (rs.IntStatus == 1) {
                    searchGrid();
                    bootboxLamlt.alert("Hoàn tác lại hóa đơn thanh toán của phòng:<b>" + roomName + "</b> thành công!");
                }
                else {
                    alert(rs.Message);
                } 
            });
        }
    });
}