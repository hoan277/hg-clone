﻿
var $table;

$(document).ready(function () {

    $("#btnRefresh").click(function () {       
        $("#name").val("");
        $("#province").val("-1");
        searchGrid();
    }) 
    setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);
    var chosenTinh = Sv.chosenSetup($("#province"), {
        placeholder_text: "",
    });
    var chosenKs = Sv.chosenSetup($("#hotel"), {
        placeholder_text: "",
    });

    $("#btnExport").click(function () {
        exportExcel();
    });

    $("#btnSearch").click(function () {
        searchGrid();
    });
    $(".box-body").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
        }
    });
    //showDialogLoading();    	
    var tableOption = {
        "ajax": {
            "url": "/report/ListBaoCaoThoiGianSuDung",
            "data": function (d) {
                d.search = "";
                d.hotelId = $("#hotel").val();
                d.hotelname = $("#hotel option:selected").text();
                //d.province = $("#province").val();
                //d.provinceName = $("#province option:selected").text();
            }
        },
        "columns":
            [
                {
                    "data": null,
                    render: function (data, type, row, infor) { 
                        var pInfo = $table.page.info();
                        return (pInfo.page * pInfo.length) + infor.row + 1;
                    }
                },
                { "data": "Code", "orderable": "false" },
                { "data": "Name", "orderable": "false" },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        if (row.DateActive != null) 
                            return Sv.DateToString(row.DateActive, "DD/MM/YYYY HH:mm");                        
                        return "";
                    }
                }, 
                { "data": "GetTimeSpanString", "orderable": "false" },

            ],
            "initComplete": function (settings, json) {    },
    };
    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));
});

function searchGrid() {
    $table.ajax.reload();
}

function exportExcel() {
    window.open("/Report/ExportBaoCaoThoiGianSuDung");
}