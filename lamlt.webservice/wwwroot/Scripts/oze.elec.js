﻿function EControl() {
    var self = this;
    this.turnOff = function (roomid, callback) {
        $.get("/Room1/GetStatusElec?roomid=" + roomid, function (data) {
            self.send(data.url, roomid, data.binary, data.stt, "0", callback);
        });
    };
    this.turnOn = function (roomid, callback) {
        $.get("/Room1/GetStatusElec?roomid=" + roomid, function (data) {
            self.send(data.url, roomid, data.binary, data.stt, "1", callback);
        });
    };
    this.send = function (urlservice, roomid, binary, stt, cmd, callback) {
        var url = "http://localhost:64000";
        if (urlservice)
            url = urlservice;
        $.post(url + "/service?stt=" + stt + "&binary=" + binary + "&cmd=" + cmd, function (data) { 
            $.post("/Room1/UpdateStatusElec?stt=" + stt + "&cmd=" + cmd + "&roomid=" + roomid + "&resultElec=" + data, function (data1) {
                callback(data1);
            });
        }).fail(function (xhr, status, error) {
            callback({ result: -1 });
        });

    };
    this.initModal = function () {
        if (document.getElementById("div_voice") == null) {
            var html = '<div id="div_voice" class="modal fade" role="dialog">';
            html += '<div class="modal-dialog modal-lg">';
            html += '    <div class="modal-content">';
            html += '        <div class="modal-header">';
            html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
            html += '            <h4 class="modal-title btn-header">Điều khiển giọng nói</h4>';
            html += '        </div>';
            html += '        <div class="modal-body">';
            html += '            <div class="modal-body-content">';
            html += '                <p>Loading...</p>';
            html += '            </div>';
            html += '</div>';

            html += '    </div>';
            html += '</div>';
            html += '</div>';
            $("body").append(html);
        }
        $("#div_voice").off('show.bs.modal');
        $("#div_voice").on('show.bs.modal', function () {
            $("#div_voice .modal-body-content").html('<p>loading..</p>');
            $("#div_voice .modal-body-content").load("/Voice/Index");
        });
        $("#div_voice").modal();
    };
}
