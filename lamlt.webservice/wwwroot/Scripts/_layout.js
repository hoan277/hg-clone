﻿$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
function alert(msg, callback) {
    bootbox.alert({
        title: "Thông báo",
        message: msg,
        callback: function () {
            if (callback) callback.call();
        }
    });
}
function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {}; 
    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    }); 
    return indexed_array;
}