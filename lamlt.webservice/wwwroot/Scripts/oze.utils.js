﻿var g_Utils;
function Utils() {
    var self = this;
    OzeBase.apply(this, arguments);
    this.showTooltip = function () {
        $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
    }

    this.init = function () {
        sdpSetClass();
        notifyLazyEvent();
        self.SetAmount();
        self.buildStyleRadio();
        updateWrapperHeight();
        this.showTooltip();
        if (window.hotelConfig &&window.hotelConfig.notificationShow && window.hotelConfig.notificationShow == 1)
                setupViewNotification();

    }
    //vivu
    this.SetDate = function (parameters) {
        $(parameters).datetimepicker({
            //debug:true,
            //locale: 'vi',
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            //maxDate: new Date(),
            //defaultDate: new Date(),
            showClose: false

        });
    };
    this.SetDateMax = function (parameters) {
        $(parameters).datetimepicker({
            //debug:true,
            //locale: 'vi',
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            maxDate: new Date(),
            defaultDate: new Date(),
            showClose: false

        });
    };
    //vivu
    this.SetAmount = function () {
        if (!self.isAndroid()) {
            $('input.amount-double-mask')
                .on()
                .inputmask({
                    alias: 'decimal',
                    placeholder: '',
                    groupSeparator: '.',
                    radixPoint: ',',
                    autoGroup: true,
                    digits: 2,
                    allowPlus: false,
                    allowMinus: false,
                    autoUnmask: true,
                    integerDigits: 16,
                });
            $('float-mask')
              .on()
              .inputmask({
                  alias: 'float',
                  placeholder: '',
                  groupSeparator: '.',
                  radixPoint: ',',
                  autoGroup: true,
                  digits: 2,
                  allowPlus: true,
                  allowMinus: true,
                  autoUnmask: true,
                  integerDigits: 16,
              });
            $('input.amount-double-mask_0')
               .on()
               .inputmask({
                   alias: 'decimal',
                   placeholder: '',
                   groupSeparator: '.',
                   radixPoint: ',',
                   autoGroup: true,
                   digits: 2,
                   allowPlus: false,
                   allowMinus: false,
                   autoUnmask: true,
                   integerDigits: 16,
               });

            $('input.amount-double-maskpay_0')
                .on()
                .inputmask({
                    alias: 'decimal',
                    placeholder: '',
                    groupSeparator: '.',
                    radixPoint: ',',
                    autoGroup: true,
                    pattern: /-/,
                    digits: 0,
                    allowPlus: true,
                    allowMinus: true,
                    autoUnmask: true,
                    integerDigits: 16,
                    reverse: true,
                    translation: {
                        'S': {
                            pattern: /-/,
                            optional: true
                        }
                    }
                });

            $('input.amount-number-mask')
                .on()
                .inputmask({
                    alias: 'decimal',
                    placeholder: '',
                    groupSeparator: '.',
                    radixPoint: ',',
                    autoGroup: true,
                    digits: 0,
                    allowPlus: false,
                    allowMinus: false,
                    autoUnmask: true,
                    integerDigits: 16,

                });
        } else {
            $('input.amount-double-mask').on().prop('type', 'number');
            $('input.amount-double-mask_0').on().prop('type', 'number');
            $('input.amount-double-maskpay_0').on().prop('type', 'number');
            $('input.amount-number-mask').on().prop('type', 'number');
        }
    };
    //vivu
    this.addDays = function (day) {
        var dat = new Date();
        dat.setDate(dat.getDate() + parseInt(day));

        return new Date(dat);
    }
    this.SetDateDefault = function () {
        //Author: vivu  
        $(".date-search-picker_fdate").datetimepicker({
            //debug:true,
            //locale: 'vi',
            format: 'DD/MM/YYYY HH:mm',
            showTodayButton: true,
            /*maxDate: new Date(),*/
            defaultDate: g_Utils.addDays(-30),
            showClose: false

        });

        $(".date-search-picker_tdate").datetimepicker({
            //locale: 'vi',
            format: 'DD/MM/YYYY HH:mm',
            showTodayButton: true,
            /*maxDate: new Date(),*/
            defaultDate: new Date(),
            showClose: false
        });
        $(".date-picker").blur(function () {
            if ($(this).val() == "") {
                var date = new Date();
                var _fn = date.getDate();
                var _ft = date.getMonth();
                var _fna = date.getFullYear();
                var ft = (_ft + 1);
                if (ft < 10)
                    ft = "0" + ft;
                var fdate = (_fn + "/" + ft + "/" + _fna);
                $(this).val(fdate);

            }
        });

    };


    this.SetDateDefaultAdd = function () {
        //Author: vivu  
        $(".date-add-picker_fdate").datetimepicker({
            //debug:true,
            //locale: 'vi',
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            /*minDate: new Date(),*/
            defaultDate: new Date(),
            showClose: false,

        });

        $(".date-add-picker_tdate").datetimepicker({
            //locale: 'vi',
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            /* minDate: new Date(),*/
            defaultDate: new Date(),
            showClose: false
        });
        $(".date-add-picker").datetimepicker({
            //locale: 'vi',
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            defaultDate: new Date(),
            showClose: false
        });
        $(".date-add-picker_fdate_full").datetimepicker({
            //debug:true,
            //locale: 'vi',
            format: 'DD/MM/YYYY HH:mm',
            showTodayButton: true,
            /*minDate: new Date(),*/
            defaultDate: new Date(),
            showClose: false,
            sideBySide: true
        });

        $(".date-add-picker_tdate_full").datetimepicker({
            //locale: 'vi',
            format: 'DD/MM/YYYY HH:mm',
            showTodayButton: true,
            /*minDate: new Date(),*/
            defaultDate: g_Utils.addDays(1),
            showClose: false,
            sideBySide: true
        });
        //$(".date-add-picker_tdate_full_notdefault").datetimepicker({
        //    //locale: 'vi',
        //    format: 'DD/MM/YYYY HH:mm',
        //    showTodayButton: true,
        //   defaultDate: new Date(),
        //    showClose: false
        //});
        $(".date-picker").blur(function () {
            if ($(this).val() == "") {
                var date = new Date();
                var _fn = date.getDate();
                var _ft = date.getMonth();
                var _fna = date.getFullYear();
                var ft = (_ft + 1);
                if (ft < 10)
                    ft = "0" + ft;
                var fdate = (_fn + "/" + ft + "/" + _fna);
                $(this).val(fdate);

            }
        });
    };

    this.ConfigAutocomplete = function (idControl, url, displayField, valueField, fnSelect, fnQuery, fnProcess, option) {
        var optionDefault = {
            onSelect: function (item) {
                $(idControl).data("seletectedValue", item.value);
                $(idControl).data("seletectedText", item.text);
                if (item.object !== undefined)
                    $(idControl).data("seletectedObject", JSON.parse(item.object));
                //$(idControl).valid();
                if (typeof (fnSelect) == "function")
                    fnSelect(item);
            },
            displayField: function (i) {
                if (typeof displayField === 'string')
                    return i[displayField];
                else
                    return displayField(i);
            },
            valueField: valueField,
            ajax: {
                url: url,
                preDispatch: (fnQuery == undefined ? function (query) {
                    return {
                        search: query
                    }
                } : fnQuery),
                preProcess: (fnProcess == undefined ? function (data) {
                    if (data.success === false) {
                        return false;
                    }
                    return data;
                } : fnProcess)
            }
        }
        $.extend(optionDefault, option);
        $(idControl).typeahead(optionDefault);
    };



    /**
     * Reset control AutoComplete
     * @param {ID control cần reset} idControlReset 
     * @returns {} 
     */
    this.resetTypeHead = function (idControlReset) {
        $(idControlReset).val('');
        $(idControlReset).data("seletectedValue", null);
        $(idControlReset).data("seletectedText", null);
        $(idControlReset).parent().find('.typeahead > li').remove();
    }
}
$(document).ready(function () {
    g_Utils = new Utils();
    g_Utils.init();

    g_Utils.SetDateDefault();
    g_Utils.SetDateDefaultAdd();
})