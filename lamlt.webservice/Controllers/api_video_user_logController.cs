﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;
using System.Web;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    public class api_video_user_logController : Controller
    {
        #region ===== GET LIST =====
        [HttpGet]
        [Route("~/film_video_user_log/List")]
        public ActionResult List(int? length, int? start, string token, int linkid)
        {
            if (token == null)
            {
                return Json(new
                {
                    code = -4,
                    msg = "Không có token",
                    draw = 0,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data = ""
                });
            }
            if (start == null) { start = 0; };
            if (length == null) { length = 1000; };
            film_video_user_logService svFilmUserLog = (new film_video_user_logService());
            List<vw_film_video_user_log> data = svFilmUserLog.GetViewAllItem(new PagingModel() { offset = start.Value, limit = length.Value, link_id = linkid }, token);
            int recordsTotal = (int)svFilmUserLog.CountAll(new PagingModel() { offset = start.Value, limit = length.Value, link_id = linkid }, token);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            if (data == null)
            {
                return Json(new
                {
                    code = -5,
                    msg = "Token sai hoặc đã hết hạn",
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data
                });
            }
            else
            {
                return Json(new
                {
                    code = 1,
                    msg = "Thành công",
                    draw,
                    recordsTotal,
                    recordsFiltered,
                    data
                });
            }
        }
        #endregion ===== LIST =====

        #region ===== UPDATE OR INSERT =====
        [HttpPost]
        [Route("~/film_video_user_log/Update")]
        public ActionResult Update(int? video_id, int? linkid, string token, string time)
        {
            // check null 
            if (token == null)
            {
                return Json(new { code = -2, msg = "Không có token" });
            }
            if (video_id == null)
            {
                return Json(new { code = -3, msg = "Không có video id" });
            }
            if (linkid == null)
            {
                return Json(new { code = -3, msg = "Không có profile id" });
            }
            if (time == null)
            {
                return Json(new { code = -6, msg = "Không có thời gian xem" });
            }

            int result = new film_video_user_logService().UpdateOrInsert(video_id.Value, linkid.Value, token, time);

            if (result > 0)
            {
                return Json(new { code = result, msg = "Thành công" });
            }

            else if (result == -4)
            {
                return Json(new { code = result, msg = "Sai token hoặc token đã hết hạn" });
            }
            else if (result == -5)
            {
                return Json(new { code = result, msg = "Sai video id" });
            }
            else
            {
                return Json(new { code = -1, msg = "Xử lý lỗi" });
            }
        }
        #endregion ===== UPDATE OR INSERT =====

    }
}