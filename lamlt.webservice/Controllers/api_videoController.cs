﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/video")]
    public class api_videoController : Controller
    {
        // GET: film_videos
        [HttpGet]
        public JsonResult Index()
        {
            StringValues access_token;
            Request.Query.TryGetValue("token", out access_token);
            StringValues access_token1;
            Request.Query.TryGetValue("access-token", out access_token1);

            StringValues action;
            Request.Query.TryGetValue("action", out action);
            StringValues query_type;
            Request.Query.TryGetValue("query-type", out query_type);
            StringValues user_type;
            Request.Query.TryGetValue("user-type", out user_type);
            StringValues start_index;
            Request.Query.TryGetValue("start-index", out start_index);
            StringValues max_results;
            Request.Query.TryGetValue("max-results", out max_results);
            StringValues categoryId;
            Request.Query.TryGetValue("categoryId", out categoryId);
            StringValues videos_id;
            Request.Query.TryGetValue("videos-id", out videos_id);
            StringValues videos_search;
            Request.Query.TryGetValue("search", out videos_search);
            string search = "";
            try { search = videos_search.ToString(); } catch { }

            StringValues typevideo_s;
            Request.Query.TryGetValue("typevideo", out typevideo_s);
            string typevideo = "";
            try { typevideo = typevideo_s.ToString(); } catch { }

            StringValues device_s;
            Request.Query.TryGetValue("ios", out device_s);
            string device = "";
            try { device = device_s.ToString(); } catch { }



            StringValues packageid_s = "";
            Request.Query.TryGetValue("packageid", out packageid_s);
            string packageid = "-1";
            try { packageid = packageid_s.ToString(); } catch { }
            string access_token_s = "";
            try { access_token_s = access_token.ToString(); } catch { }

            string access_token_s1 = "";
            try { access_token_s1 = access_token1.ToString(); } catch { }

            string access_token_last = "";
            if (!string.IsNullOrEmpty(access_token_s1)) access_token_last = access_token_s1;
            if (!string.IsNullOrEmpty(access_token)) access_token_last = access_token;

            if (!string.IsNullOrEmpty(categoryId))
            {
                if (int.Parse(categoryId) <= 0) categoryId = "-1";
            }
            //LogService.logItem("Chuẩn bị gọi hàm processing_data");
            //Stopwatch watch = new Stopwatch();
            //watch.Start();
            DateTime dtStart = DateTime.Now;
            object datax = new ServiceMeService().processing_data(access_token_last, query_type, user_type, start_index, max_results, categoryId, videos_id, search, packageid, "-1", typevideo, "ios");

            logItem("Step 0===>take time is :" + DateTime.Now.Subtract(dtStart).TotalMilliseconds.ToString() + "ms");
            dtStart = DateTime.Now;
            //watch.Stop();
            //TimeSpan timeSpan = watch.Elapsed;
            //LogService.logItem(string.Format("Time: {0}h {1}m {2}s {3}ms", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds));
            return Json(datax);
        }
        private void logItem(string s)
        {
            using (StreamWriter w = new StreamWriter("logDebugLong.txt", true))
            {
                w.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ":" + s);
            }
        }
        [HttpGet]
        [Route("~/authen")]
        public ActionResult authen()
        {
            StringValues access_token;
            Request.Query.TryGetValue("token", out access_token);
            return Json(new { token = access_token });
        }
        [HttpGet]
        [Route("~/film_video/syn")]
        public ActionResult syn(int? syn)
        {
            if (syn == 1)
            {
                new ServiceMeService().processing_syn();
                return Json(new { a = 1 });
            }
            return Json(new { a = 0 });
        }

        [HttpGet]
        [Route("~/film_video/List1")]
        public ActionResult ListByToken(int length, int start, string search, int cpid, string token)
        {
            if (length == 0) { length = 10000; }
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                cpid = cpid
            };
            film_videoService svrfilm_video = (new film_videoService());
            List<vw_film_video> list_video_new = new List<vw_film_video>();
            if (token != null)
            {
                film_user user = new film_userService().GetByToken(token);
                if (user != null)
                {
                    user_link user_Link = new user_linkService().GetByuserid(user.Id);
                    if (user_Link == null)
                    {
                        return Json(list_video_new);
                    }
                    list_video_new = svrfilm_video.GetAllByTypeKid(page, user_Link.typekid);
                    return Json(new
                    {
                        list_video_new
                    });
                }
                return Json(new { list_video_new });
            }
            List<vw_film_video> list1 = svrfilm_video.GetViewAllItemAPI(page);
            return Json(list1);
        }
        [HttpGet]
        [Route("~/film_video/List")]
        public ActionResult List(int length, int start, string search, int cpid)
        {
            film_videoService svrfilm_video = (new film_videoService());
            List<vw_film_video> data = svrfilm_video.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
            int recordsTotal = (int)svrfilm_video.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_video/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return PartialView("film_videoEdit", obj);
        }
        [HttpGet]
        [Route("~/film_video/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return View("Edit", obj);
        }
        [HttpPost]
        [Route("~/film_video/Update")]
        public ActionResult Update(film_video obj)
        {
            int result = new film_videoService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_videoService().UpdateStatus(obj);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/film_video/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute] string id)
        {
            int result = new film_videoService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/getImageupdate")]
        public IActionResult getImageupdate(int id)
        {
            List<film_video_image> result = new film_videoService().getupdateImg(id);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid)
        {
            int result = new film_videoService().UpdateImage(url, filmid);
            return Json(new { result = result });
        }
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/film_video/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_videoService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video obj = new film_videoService().GetByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpGet]
        [Route("~/film_video/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_video/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/film_detail/{id}")]
        public JsonResult film_detail([FromRoute] string id)
        {
            vw_film_video video_detail = new film_videoService().GetViewByID(id);
            List<vw_film_video> related = new film_videoService().GetRelatedVideo(id);
            vw_film_catalog catalog = new film_catalogService().GetViewByID(video_detail.catalog_id.ToString());
            return Json(new
            {
                video_detail = video_detail,
                related = related,
                catalog = catalog
            });
        }
        [HttpGet]
        [Route("~/GetAllEpisodeByFilmId/{film_id}")]
        public JsonResult GetAllEpisodeByFilmId([FromRoute] int film_id)
        {
            if (film_id <= 0)
            {
                return Json(new
                {
                    film_id = film_id,
                    message = "film_id không được null",
                });
            }
            List<vw_film_video> list = new film_videoService().GetAllEpisodeByFilmId(film_id);
            return Json(new
            {
                message = "Thành công",
                all_episode = list
            });
        }
    }
}