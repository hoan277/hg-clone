﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/user_link")]
    public class user_linkController : Controller
    {
        // GET: user_links
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/user_link/List")]
        public ActionResult List(int length, int start, string search)
        {
            user_linkService svruser_link = (new user_linkService());
            List<user_link> data = svruser_link.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svruser_link.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/user_link/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            user_link obj = (new user_linkService()).GetByID(id);
            if (obj == null) obj = (new user_linkService()).InitEmpty();
            return PartialView("user_linkEdit", obj);
        }
        [HttpPost]
        [Route("~/userlink/UpdateProfile")]
        public ActionResult Update(string token, string name, int typekid, int linkid)
        {
            int result = new user_linkService().UpdateByToken(token, new user_link() { Id = linkid, typekid = typekid, name = name });
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/user_link/Update")]
        public ActionResult Update(user_link obj)
        {
            int result = new user_linkService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/user_link/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            user_link obj = new user_linkService().GetByID(id);
            return PartialView("user_linkDetail", obj);
        }
        [HttpGet]
        [Route("~/user_link/info/{id}")]
        public ActionResult GetDetailInfo([FromRoute] string id)
        {
            user_link obj = new user_linkService().GetByID(id);
            return Json(obj);
        }
        [HttpGet]
        [Route("~/user_link/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_user_link obj = new user_linkService().GetViewByID(id);
            return PartialView("user_linkDetail", obj);
        }
        [HttpDelete]
        [Route("~/user_link/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new user_linkService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/userlink/register")]
        public JsonResult register_userlink(string token, string name, int typekid)
        {
            if (token == null || token == "") return Json(new { message = "token không được để trống" });
            if (name == null || name == "") return Json(new { message = "name không được để trống" });
            if (typekid <= 0 || typekid > 2) return Json(new { message = "typekid không đúng, giá trị phải bằng 1 hoặc 2" });
            int user_link = new user_linkService().register_userlink(token, name, typekid);
            return Json(new { message = user_link > 0 ? "Thành công" : "Token hết hạn hoặc không đúng", userlink = new user_linkService().GetViewByID(user_link.ToString()) });
        }
        [HttpPost]
        [Route("~/userlink/delete")]
        public JsonResult delete_userlink(string token, int linkid)
        {
            if (token == null || token == "") return Json(new { message = "Token không được để trống" });
            int user_link = new user_linkService().delete_userlink(token, linkid);
            return Json(new { message = user_link > 0 ? "Thành công" : "Token không đúng hoặc link id tồn tại" });
        }
        [HttpPost]
        [Route("~/userlink/use")]
        public JsonResult use_userlink(string token, int linkid)
        {
            if (token == null || token == "") return Json(new { message = "Token không được để trống" });
            int user_link = new user_linkService().use_userlink(token, linkid);
            return Json(new { message = user_link > 0 ? "Thành công" : "Token không đúng hoặc link id tồn tại" });
        }
        [HttpGet]
        [Route("~/userlink/GetByUserId/{id}")]
        public JsonResult GetByUserId(int id)
        {
            if (id <= 0) return Json(new { message = "id phải lớn hơn 0" });
            var user_link = new user_linkService().GetAllUL(id);
            return Json(new { message = user_link != null ? "Thành công" : "Thất bại", count = user_link.Count, user_link = user_link });
        }
    }
}