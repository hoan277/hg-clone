﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_article")]
    public class film_articleController : Controller
    {
        // GET: film_articles
        [HttpGet]
        public ViewResult Index()
        {
            return View("Index");
        }

        [HttpGet]
        [Route("~/film_article/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_articleService svrfilm_article = (new film_articleService());
            List<vw_film_article> data = svrfilm_article.GetAllItemView(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_article.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_article/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_article obj = (new film_articleService()).GetByID(id);
            if (obj == null) obj = (new film_articleService()).InitEmpty();
            return PartialView("film_articleEdit", obj);
        }
        [HttpPost]
        [Route("~/film_article/Update")]
        public ActionResult Update(film_article obj)
        {
            int result = new film_articleService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_article/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_article obj = new film_articleService().GetByID(id);
            return PartialView("film_articleDetail", obj);
        }
        [HttpGet]
        [Route("~/film_article/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_article obj = new film_articleService().GetViewByID(id);
            return PartialView("film_articleDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_article/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_articleService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_article/GetAllNews/{id}")]
        public JsonResult GetAllNews(string id)
        {
            var result = new film_articleService().GetAllNews(id);
            return Json(new { message = result != null ? "Thành công" : "Thất bại", count = result.Count, result = result });
        }
    }
}