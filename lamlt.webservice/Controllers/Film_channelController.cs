﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_channel")]
    public class film_channelController : Controller
    {
        // GET: film_channels
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_channel/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_channelService svrfilm_channel = (new film_channelService());
            List<film_channel> data = svrfilm_channel.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_channel.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_channel/Edit")]
        public PartialViewResult Edit(string id)
        {
            film_channel obj = (new film_channelService()).GetByID(id);
            if (obj == null) obj = (new film_channelService()).InitEmpty();
            return PartialView("film_channelEdit", obj);
        }
        [HttpPost]
        [Route("~/film_channel/Update")]
        public ActionResult Update(film_channel obj)
        {
            int result = new film_channelService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_channel/GetDetail")]
        public PartialViewResult GetDetail(string id)
        {
            film_channel obj = new film_channelService().GetByID(id);
            return PartialView("film_channelDetail", obj);
        }
        [HttpGet]
        [Route("~/film_channel/GetViewDetail")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_channel obj = new film_channelService().GetViewByID(id);
            return PartialView("film_channelDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_channel/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_channelService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}