﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/ads")]
    [EnableCors("MyPolicy")]
    public class adsController : Controller
    {
        // GET: ads
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/ads/List")]
        public ActionResult List(int length, int start, string search)
        {
            adsService svrad = (new adsService());
            List<vw_ads> data = svrad.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrad.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/ads/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            ads obj = (new adsService()).GetByID(id);
            if (obj == null) obj = (new adsService()).InitEmpty();
            return PartialView("adsEdit", obj);
        }
        [HttpPost]
        [Route("~/ads/Update")]
        public ActionResult Update(ads obj)
        {
            int result = new adsService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/ads/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            ads obj = new adsService().GetByID(id);
            return PartialView("adsDetail", obj);
        }
        [HttpGet]
        [Route("~/ads/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_ads obj = new adsService().GetViewByID(id);
            return PartialView("adsDetail", obj);
        }
        [HttpDelete]
        [Route("~/ads/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new adsService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/ads/GetAds1")]
        public JsonResult GetAds1(int id, string title, string type, string ads_type, string item_type, int offset, int limit, string orderby)
        {
            var obj = new adsService().GetAds(id, title, type, ads_type, item_type, offset, limit, orderby);
            return Json(obj);
        }

        [HttpGet]
        [Route("~/ads/GetAds")]
        public JsonResult GetAds(int id, string title, string type, string ads_type, string item_type, int offset, int limit, string orderby, int android, int typekid)
        {

            adsService adsSV = new adsService();
            var obj = adsSV.GetAds1(id, title, type, ads_type, item_type, offset, limit, orderby, android, typekid);
            return Json(obj);
        }

        [HttpGet]
        [Route("~/api/ads-vast")]
        [EnableCors("MyPolicy")]
        public ActionResult GetVAST(int filmid)
        {
            string xmlData = "";
            if (filmid < 0) return Content(xmlData, "text/xml", System.Text.Encoding.UTF8);
            adsService adsSV = new adsService();
            ads ads_item = adsSV.GetByFilmId(filmid);
            xmlData = adsSV.GetVASTByFilmId1(ads_item);
            return Content(xmlData, "text/xml", System.Text.Encoding.UTF8);
        }
    }
}