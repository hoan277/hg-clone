﻿using lamlt.web.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;

namespace Lala.Controllers
{
    public class ImageController : Controller
    {
        private readonly IHostingEnvironment _appEnvironment;
        private static Color canvasColor;

        public ImageController(IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("~/api/image")]
        public IActionResult index1(string url, int width, int height)
        {
            Image img = DownloadImageFromUrl(url);
            Image filnew1 = resizeImageLamlt(width, height, img);
            //Image filnew1 = resizeImageLamlt(700, 200, img);
            return File(ImageToByteArray(filnew1), "image/jpeg");
        }
        [HttpGet]
        [Route("~/api/image2")]
        public IActionResult index2(string url, int width, int height)
        {
            Image img = DownloadImageFromUrl(url);
            Image filnew1 = resizeImageLamlt(width, height, img);
            return File(ImageToByteArray(filnew1), "image/jpeg");
        }

        public Image resizeImageLamlt(int newWidth, int newHeight, Image imgPhoto)
        {
            //Image imgPhoto = Image.FromFile(stPhotoPath);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;

            //Consider vertical pics
            if (sourceWidth < sourceHeight)
            {
                int buff = newWidth;

                newWidth = newHeight;
                newHeight = buff;
            }

            int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
            float nPercent = 0, nPercentW = 0, nPercentH = 0;

            nPercentW = ((float)newWidth / (float)sourceWidth);
            nPercentH = ((float)newHeight / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((newWidth -
                          (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((newHeight -
                          (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);


            Bitmap bmPhoto = new Bitmap(newWidth, newHeight);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(System.Drawing.ColorTranslator.FromHtml("#eff1f4"));
            grPhoto.InterpolationMode =
                System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.SmoothingMode = SmoothingMode.HighQuality;
            grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
            grPhoto.CompositingQuality = CompositingQuality.HighQuality;


            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            imgPhoto.Dispose();
            return bmPhoto;
        }
        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        // thay đổi kích thước anh
        private Image resize_image(Image img, int newWidth, int maxHeight)
        {

            if (img.Width <= newWidth)
            {
                newWidth = img.Width;
            }

            var newHeight = img.Height * newWidth / img.Width;
            if (newHeight > maxHeight)
            {
                // Resize with height instead  
                newWidth = img.Width * newHeight / img.Height;
                newHeight = maxHeight;
            }

            var res = new Bitmap(newWidth, newHeight);

            using (var graphic = Graphics.FromImage(res))
            {
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;
                graphic.DrawImage(img, 0, 0, newWidth, newHeight);
            }

            return res;
        }

        public System.Drawing.Image DownloadImageFromUrl(string imageUrl)
        {
            System.Drawing.Image image = null;
            Console.WriteLine("imageUrl is " + imageUrl);
            if (imageUrl == null) { imageUrl = "https://via.placeholder.com/350"; }
            try
            {
                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;
                webRequest.Credentials = CredentialCache.DefaultCredentials;
                webRequest.Method = "GET";
                webRequest.Headers.Add("Accept-Language", "en-US");
                webRequest.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
                System.Net.WebResponse webResponse = webRequest.GetResponse();
                System.IO.Stream stream = webResponse.GetResponseStream();
                image = System.Drawing.Image.FromStream(stream);
                //var filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\images", imageUrl.Split('/')[imageUrl.Split('/').Length-1]);
                //image.Save(filePath);
                webResponse.Close();
            }
            catch (Exception ex)
            {
                LogService.logItem(ex.Message);
                Console.WriteLine(ex);
                return null;
            }
            //var filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\images", imageUrl);
            return image;

        }


        public bool IsMobileDevice(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            string[] mobileDevices = new string[] {"iphone","ppc",
                                                   "windows ce","blackberry",
                                                   "opera mini","mobile","palm",
                                                   "portable","opera mobi" };
            string userAgent = request.UserAgent.ToString().ToLower();
            return mobileDevices.Any(x => userAgent.Contains(x));
        }
    }

}
