﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/img")]
    public class api_imgController : Controller
    {
        // GET: film_videos
        //vi/XmZeuM4xLX/maxres/1 ---.jpg
        [Route("~/img/syn_img")]
        public JsonResult syn_img(int? syn)
        {
            if (syn > 0)
            {
                new ServiceMeService().processing_syn_img(syn ?? 0);
            }
            return Json(new { a = 0 });
        }
        [Route("~/img/syn_img_sql")]
        public JsonResult syn_img_sql(int? syn)
        {
            if (syn > 0)
            {
                new ServiceMeService().processing_syn_img_sql(syn ?? 0);
            }
            return Json(new { a = 0 });
        }
        [HttpGet]
        [Route("~/film_video/syn")]
        public ActionResult syn(int? syn)
        {
            if (syn == 1)
            {
                new ServiceMeService().processing_syn();
                return Json(new { a = 1 });
            }
            return Json(new { a = 0 });
        }
        //[HttpGet]
        //[Route("~/film_video/List")]
        //public ActionResult List(int length, int start, string search, int cpid)
        //{
        //    film_videoService svrfilm_video = (new film_videoService());
        //    List<vw_film_video> data = svrfilm_video.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
        //    int recordsTotal = (int)svrfilm_video.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
        //    int recordsFiltered = recordsTotal;
        //    int draw = 1;
        //    StringValues valueDraw = "";
        //    HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
        //    try { draw = int.Parse(valueDraw); } catch { }
        //    return Json(new
        //    {
        //        draw,
        //        recordsTotal,
        //        recordsFiltered,
        //        data
        //    });
        //}
        [HttpGet]
        [Route("~/film_video/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return PartialView("film_videoEdit", obj);
        }
        [HttpGet]
        [Route("~/film_video/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return View("Edit", obj);
        }
        [HttpPost]
        [Route("~/film_video/Update")]
        public ActionResult Update(film_video obj)
        {
            int result = new film_videoService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_videoService().UpdateStatus(obj);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/film_video/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute] string id)
        {
            int result = new film_videoService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/getImageupdate")]
        public IActionResult getImageupdate(int id)
        {
            List<film_video_image> result = new film_videoService().getupdateImg(id);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid)
        {
            int result = new film_videoService().UpdateImage(url, filmid);
            return Json(new { result = result });
        }
        //  class filmid
        //{
        //    public int[] catatogid { get;set }
        //    int filmid { get; set}
        //}
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/film_video/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_videoService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video obj = new film_videoService().GetByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpGet]
        [Route("~/film_video/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_video/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }


    }
}