﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace lamlt.webservice.Controllers
{
    public class film_commentController : Controller
    {
        [Produces("application/json")]
        [Route("~/film_comment")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("~/film_comment/GetAllCommentByFilmId")]
        //start-index=0&max-results=80
        public ActionResult GetAllCommentByFilmId(int id)
        {
            var filmservice = new film_commentsService();
            StringValues start_index;
            Request.Query.TryGetValue("start-index", out start_index);
            StringValues max_results;
            Request.Query.TryGetValue("max-results", out max_results);
            int start = 0;
            int max = 10;
            try
            {
                start = int.Parse(start_index);
                max = int.Parse(max_results);
            }
            catch { }
            long totalRecord = filmservice.GetAllCommentByFilmId_Count(id);
            return Json(new { error = 0, msg = "", total = totalRecord, data = filmservice.GetAllCommentByFilmId(id) });
        }
        [HttpGet]
        [Route("~/film_comment/List")]
        public ActionResult List()
        {
            return Json(new film_commentsService().GetViewAllItem(null));
        }
        [HttpPost]
        [Route("~/film_comment/Update")]
        public ActionResult Update(film_comments obj, string token)
        {
            if (token == null) return Json(new { message = "Mã token không được để trống", token = token });
            int result = new film_commentsService().UpdateOrInsert(obj, token);
            return Json(new { message = result <= 0 ? "Nhận xét thất bại" : "Nhận xét thành công", result = result });
        }
    }
}