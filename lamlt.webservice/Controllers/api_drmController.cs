﻿using Lalatv.Models;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace lamlt.Controllers
{
    [Route("~/drm")]
    public class api_drmController : Controller//
    {
        [Produces("application/octet-stream")]
        [HttpGet]
        [Route("~/drm/index")]
        public byte[] Index()
        {
            string sQuery = Request.QueryString.ToString();
            Boolean isValid = true;
            StringValues token;
            StringValues catid;
            StringValues itemid;
            Request.Query.TryGetValue("token", out token);
            Request.Query.TryGetValue("catid", out catid);
            Request.Query.TryGetValue("itemid", out itemid);
            isValid = check_token(token.ToString(), catid, itemid);


            if (!isValid)
            {
                Response.StatusCode = 403;
                return new byte[] { };
                /*
                var result= new HttpResponseMessage();
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("application/octet-stream");
                return result;
                */
            }
            else
            {
                //Response.AddHeader("Content-Type", "binary/octet-stream");
                //Response.AddHeader("Pragma", "nocache");
                String keyStr = "DE51A7254739C0EDF1DCE13BBB308FF0";

                int len = keyStr.Length / 2;
                byte[] keyBuffer = new byte[len];

                for (int i = 0; i < len; i++)
                    keyBuffer[i] = Convert.ToByte(keyStr.Substring(i * 2, 2), 16);
                /*
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(keyBuffer);
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("binary/octet-stream");
                return result;
                */
                return keyBuffer;
            }
        }
        [HttpGet]
        [Route("~/drm/genkey")]
        public IActionResult GenKey()
        {
            List<film_video> list = new List<film_video>();
            var videoService = new film_videoService();
            StringValues itemid;
            Request.Query.TryGetValue("itemid", out itemid);
            if (itemid == "all") list = videoService.GetAllItem(new PagingModel() { offset = 0, limit = 1000000 });
            else list.Add(videoService.GetByID(itemid.ToString()));
            foreach (film_video item in list)
            {
                createDrmKey(item);
            }
            return Json(new { s = 1 });
        }
        [HttpGet]
        [Route("~/drm/test")]
        public IActionResult GetFile()
        {
            string sQuery = Request.QueryString.ToString();
            Boolean isValid = true;
            StringValues token;
            StringValues catid;
            StringValues itemid;
            Request.Query.TryGetValue("token", out token);
            Request.Query.TryGetValue("catid", out catid);
            Request.Query.TryGetValue("itemid", out itemid);
            isValid = check_token(token.ToString(), catid, itemid);

            if (!isValid)
            {
                return StatusCode(403);
                //Response.StatusCode = 403;
                //return new FileContentResult(new byte[] { }, "application/octet-stream");

                //Response.en
                //return new byte[] { };
                /*
                var result= new HttpResponseMessage();
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("application/octet-stream");
                return result;
                */
            }
            else
            {
                //Response.AddHeader("Content-Type", "binary/octet-stream");
                //Response.AddHeader("Pragma", "nocache");
                String keyStr = "DE51A7254739C0EDF1DCE13BBB308FF1";

                int len = keyStr.Length / 2;
                byte[] keyBuffer = new byte[len];

                for (int i = 0; i < len; i++)
                    keyBuffer[i] = Convert.ToByte(keyStr.Substring(i * 2, 2), 16);
                /*
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(keyBuffer);
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("binary/octet-stream");
                return result;
                */
                //return keyBuffer;
                return new FileContentResult(keyBuffer, "application/octet-stream");
            }
        }
        public bool check_token(string token, string catid, string itemid)
        {
            //return !string.IsNullOrEmpty(token);
            //if (!string.IsNullOrEmpty(itemid)) return true;
            //if (catid == "1" || catid == "5" || catid == "7" || catid == "8" || catid == "9"
            //   || catid == "10" || catid == "14") return true;// free phim
            film_userService userService = new film_userService();
            film_user oLogin = userService.GetByToken(token);
            if (oLogin == null) return false;
            film_drm drm = new film_userService().getDrmByUserId(oLogin.Id);
            return drm != null;
        }
        public void createDrmKey(film_video video)
        {
            string sRootKey = "/home/amnhacsaigon/wowza/mapping/keys";
            string sUrl = video.upload_file;
            sUrl = sUrl.Replace("https://stream.lalatv.com.vn/vod/_definst_/mp4:", "");
            sUrl = sUrl.Replace("https://stream.lalatv.com.vn/vod/_definst_/smil:", "");
            sUrl = sUrl.Replace("/playlist.m3u8", "");
            string[] arrParts = sUrl.Split('/');
            var sDirWillCreate = sRootKey;

            foreach (string item in arrParts)
            {
                sDirWillCreate = sDirWillCreate + "/" + item;
                string file360 = sDirWillCreate.Split(".")[0] + "-360p.mp4.key";
                string file480 = sDirWillCreate.Split(".")[0] + "-480p.mp4.key";
                string file720 = sDirWillCreate.Split(".")[0] + "-720p.mp4.key";

                List<string> lstFile = new List<string>();
                lstFile.Add(file360);
                lstFile.Add(file480);
                lstFile.Add(file720);
                if (item.ToLower().EndsWith(".mp4") || item.ToLower().EndsWith(".smil"))
                {
                    //CuocSanChimQuy.Birders Guide To Everything.mp4
                    sDirWillCreate = sDirWillCreate + ".key"; // tao file tai day
                    lstFile.Add(sDirWillCreate);
                    //for here
                    foreach (var file in lstFile)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                        using (StreamWriter w = new StreamWriter(file))
                        {
                            w.WriteLine("key: DE51A7254739C0EDF1DCE13BBB308FF1");
                            w.Write("url: https://sdk.lalatv.com.vn/drm/test?keyinfo=securekey&itemid=" + video.Id.ToString() + "&catid=" + video.catalog_id.ToString());
                        }
                    }
                }
                else if (!Directory.Exists(sDirWillCreate))
                {
                    Directory.CreateDirectory(sDirWillCreate);
                }
            }
        }
        // GET: film_videos
        [HttpGet]
        [Route("~/sign")]
        public JsonResult Sign(string s)
        {
            StringValues access_token;
            Request.Query.TryGetValue("access-token", out access_token);
            StringValues action;
            Request.Query.TryGetValue("action", out action);
            StringValues query_type;
            Request.Query.TryGetValue("query-type", out query_type);
            StringValues user_type;
            Request.Query.TryGetValue("user-type", out user_type);
            StringValues start_index;
            Request.Query.TryGetValue("start-index", out start_index);
            StringValues max_results;
            Request.Query.TryGetValue("max-results", out max_results);
            StringValues categoryId;
            Request.Query.TryGetValue("categoryId", out categoryId);
            AuthenResult datax = new ServiceMeService().processing_authen(access_token, query_type, user_type, start_index, max_results, categoryId);
            //return Json(new {error=datax.error,accesstoken=datax.accesstoken,expired= datax.expired });
            return Json(datax);

        }
        private byte[] SignData(X509Certificate2 certificate, byte[] dataToSign)
        {
            // get xml params from current private key
            var rsa = (RSA)certificate.PrivateKey;
            var xml = RSAHelper.ToXmlString(rsa, true);
            var parameters = RSAHelper.GetParametersFromXmlString(rsa, xml);

            // generate new private key in correct format
            var cspParams = new CspParameters()
            {
                ProviderType = 24,
                ProviderName = "Microsoft Enhanced RSA and AES Cryptographic Provider"
            };
            var rsaCryptoServiceProvider = new RSACryptoServiceProvider(cspParams);
            rsaCryptoServiceProvider.ImportParameters(parameters);

            // sign data
            var signedBytes = rsaCryptoServiceProvider.SignData(dataToSign, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            return signedBytes;
        }
        public static class RSAHelper
        {
            public static RSAParameters GetParametersFromXmlString(RSA rsa, string xmlString)
            {
                RSAParameters parameters = new RSAParameters();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlString);

                if (xmlDoc.DocumentElement.Name.Equals("RSAKeyValue"))
                {
                    foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
                    {
                        switch (node.Name)
                        {
                            case "Modulus": parameters.Modulus = Convert.FromBase64String(node.InnerText); break;
                            case "Exponent": parameters.Exponent = Convert.FromBase64String(node.InnerText); break;
                            case "P": parameters.P = Convert.FromBase64String(node.InnerText); break;
                            case "Q": parameters.Q = Convert.FromBase64String(node.InnerText); break;
                            case "DP": parameters.DP = Convert.FromBase64String(node.InnerText); break;
                            case "DQ": parameters.DQ = Convert.FromBase64String(node.InnerText); break;
                            case "InverseQ": parameters.InverseQ = Convert.FromBase64String(node.InnerText); break;
                            case "D": parameters.D = Convert.FromBase64String(node.InnerText); break;
                        }
                    }
                }
                else
                {
                    throw new Exception("Invalid XML RSA key.");
                }

                return parameters;
            }
            public static string ToXmlString(RSA rsa, bool includePrivateParameters)
            {
                RSAParameters parameters = rsa.ExportParameters(includePrivateParameters);

                return string.Format("<RSAKeyValue><Modulus>{0}</Modulus><Exponent>{1}</Exponent><P>{2}</P><Q>{3}</Q><DP>{4}</DP><DQ>{5}</DQ><InverseQ>{6}</InverseQ><D>{7}</D></RSAKeyValue>",
                    Convert.ToBase64String(parameters.Modulus),
                    Convert.ToBase64String(parameters.Exponent),
                    Convert.ToBase64String(parameters.P),
                    Convert.ToBase64String(parameters.Q),
                    Convert.ToBase64String(parameters.DP),
                    Convert.ToBase64String(parameters.DQ),
                    Convert.ToBase64String(parameters.InverseQ),
                    Convert.ToBase64String(parameters.D));
            }
        }
        //[HttpGet]
        //[Route("~/film_video/List")]
        //public ActionResult List(int length, int start, string search, int cpid)
        //{
        //    film_videoService svrfilm_video = (new film_videoService());
        //    List<vw_film_video> data = svrfilm_video.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
        //    int recordsTotal = (int)svrfilm_video.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
        //    int recordsFiltered = recordsTotal;
        //    int draw = 1;
        //    StringValues valueDraw = "";
        //    HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
        //    try { draw = int.Parse(valueDraw); } catch { }
        //    return Json(new
        //    {
        //        draw,
        //        recordsTotal,
        //        recordsFiltered,
        //        data
        //    });
        //}
        [HttpGet]
        [Route("~/film_video/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return PartialView("film_videoEdit", obj);
        }
        [HttpGet]
        [Route("~/film_video/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return View("Edit", obj);
        }
        [HttpPost]
        [Route("~/film_video/Update")]
        public ActionResult Update(film_video obj)
        {
            int result = new film_videoService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_videoService().UpdateStatus(obj);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/film_video/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute] string id)
        {
            int result = new film_videoService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/getImageupdate")]
        public IActionResult getImageupdate(int id)
        {
            List<film_video_image> result = new film_videoService().getupdateImg(id);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid)
        {
            int result = new film_videoService().UpdateImage(url, filmid);
            return Json(new { result = result });
        }
        //  class filmid
        //{
        //    public int[] catatogid { get;set }
        //    int filmid { get; set}
        //}
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/film_video/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_videoService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video obj = new film_videoService().GetByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpGet]
        [Route("~/film_video/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_video/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}