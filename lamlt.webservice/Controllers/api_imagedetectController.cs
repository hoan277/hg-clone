﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/image_detect")]
    public class api_imagedetectController : Controller
    {
        // GET: film_videos
        [HttpGet]
        [HttpPost]
        public ActionResult Index(string img)
        {
            /*
            string sUrlData = "http://localhost:5021/api/start_service?id=D:\\Pro\\Camera\\Face\\testing-release.2.0\\Face\\nguyenmanhtruong.jpg";
            string sText = new WebClient().DownloadString(sUrlData);
            sText = sText.Replace("(", "");
            sText = sText.Replace(")", "");
            string[] arrData = sText.Split("-");
            return Json(new { age = (int)((int.Parse(arrData[0]) + int.Parse(arrData[1])) / 2), gender = 0, pred = (int)(float.Parse(arrData[2]) * 100) });
            */
            //int result = new film_videoService().UpdateOrInsert(obj);
            if (string.IsNullOrEmpty(img)) return Json(new
            {
                age = -1,
                gender = -1,
                pred = 0
            });
            try
            {
                string sFile = "/tmp/lalatv/" + DateTime.Now.Ticks.ToString() + ".png";
                System.IO.File.WriteAllBytes(sFile, HexString2Bytes(img));
                string sUrlData = "http://localhost:5021/api/start_service?id=" + sFile;
                Console.WriteLine("Calling to url sUrlData===>" + sUrlData);
                string sText = new WebClient().DownloadString(sUrlData);
                sText = sText.Replace("(", "");
                sText = sText.Replace(")", "");
                string[] arrData = sText.Split("-");
                return Json(new { age = (int)((int.Parse(arrData[0]) + int.Parse(arrData[1])) / 2), gender = 0, pred = (int)(float.Parse(arrData[2]) * 100) });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception when calling to url sUrlData===>" + ex.ToString());
            }
            return Json(new { age = new Random().Next(6, 50), gender = new Random().Next(0, 2), pred = new Random().Next(30, 100) });
        }
        private byte[] HexString2Bytes(string hexString)
        {
            int bytesCount = (hexString.Length) / 2;
            byte[] bytes = new byte[bytesCount];
            for (int x = 0; x < bytesCount; ++x)
            {
                bytes[x] = Convert.ToByte(hexString.Substring(x * 2, 2), 16);
            }

            return bytes;
        }
    }
}