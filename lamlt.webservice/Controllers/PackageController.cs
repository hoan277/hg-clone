﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/package")]
    public class packageController : Controller
    {
        // GET: packages
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/package/List")]
        public ActionResult List(int length, int start, string search)
        {
            packageService svrpackage = (new packageService());
            List<vw_package> data = svrpackage.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrpackage.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpGet]
        [Route("~/package/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            package obj = (new packageService()).GetByID(id);
            if (obj == null) obj = (new packageService()).InitEmpty();
            return PartialView("packageEdit", obj);
        }

        [HttpPost]
        [Route("~/package/Update")]
        public ActionResult Update(package obj)
        {
            int result = new packageService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/package/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new packageService().quatity(id);
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/package/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            package obj = new packageService().GetByID(id);
            return PartialView("packageDetail", obj);
        }

        [HttpGet]
        [Route("~/package/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_package obj = new packageService().GetViewByID(id);
            return PartialView("packageDetail", obj);
        }

        [HttpDelete]
        [Route("~/package/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new packageService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        public static string getValueInURL(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        [HttpGet]
        [Route("~/api/package/getall")]
        public JsonResult GetAll(int length, int start, string search)
        {
            packageService svrpackage = (new packageService());
            List<vw_package> data = svrpackage.GetPackages();
            return Json(new { data });
        }

        [HttpGet]
        [Route("~/api/package/GetPackageByProvider")]
        public ActionResult GetPackageByProvider(int provider_id, string package_code, string provider_code)
        {
            packageService svrpackage = (new packageService());
            var data = svrpackage.GetPackageByProvider(provider_id, package_code, provider_code);
            return Json(data);
        }
    }
}