﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_cp")]
    public class film_cpController : Controller
    {
        // GET: film_cps
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_cp/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_cpService svrfilm_cp = (new film_cpService());
            List<film_cp> data = svrfilm_cp.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_cp.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_cp/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_cp obj = (new film_cpService()).GetByID(id);
            if (obj == null) obj = (new film_cpService()).InitEmpty();
            return PartialView("film_cpEdit", obj);
        }
        [HttpPost]
        [Route("~/film_cp/Update")]
        public ActionResult Update(film_cp obj)
        {
            int result = new film_cpService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_cp/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_cp obj = new film_cpService().GetByID(id);
            return PartialView("film_cpDetail", obj);
        }
        [HttpGet]
        [Route("~/film_cp/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_cp obj = new film_cpService().GetViewByID(id);
            return PartialView("film_cpDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_cp/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_cpService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}