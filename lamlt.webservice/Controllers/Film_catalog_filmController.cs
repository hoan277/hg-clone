﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_catalog_film")]
    public class film_catalog_filmController : Controller
    {
        // GET: film_catalog_films
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_catalog_film/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_catalog_filmService svrfilm_catalog_film = (new film_catalog_filmService());
            List<film_catalog_film> data = svrfilm_catalog_film.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_catalog_film.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_catalog_film/Edit")]
        public PartialViewResult Edit(string id)
        {
            film_catalog_film obj = (new film_catalog_filmService()).GetByID(id);
            if (obj == null) obj = (new film_catalog_filmService()).InitEmpty();
            return PartialView("film_catalog_filmEdit", obj);
        }
        [HttpPost]
        [Route("~/film_catalog_film/Update")]
        public ActionResult Update(film_catalog_film obj)
        {
            int result = new film_catalog_filmService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_catalog_film/GetDetail")]
        public PartialViewResult GetDetail(string id)
        {
            film_catalog_film obj = new film_catalog_filmService().GetByID(id);
            return PartialView("film_catalog_filmDetail", obj);
        }
        [HttpGet]
        [Route("~/film_catalog_film/GetViewDetail")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_catalog_film obj = new film_catalog_filmService().GetViewByID(id);
            return PartialView("film_catalog_filmDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_catalog_film/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_catalog_filmService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/GetAllVideoByCateSlug/")]
        public JsonResult GetAllVideoByCateSlug(string slug, int start, int limit)
        {
            var result = new film_catalogService().GetAllVideoByCateSlug(slug, start, limit);
            return Json(result);
        }
    }
}