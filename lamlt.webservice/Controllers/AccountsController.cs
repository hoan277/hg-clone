﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace lamlt.web.Controllers
{
    public class AccountsController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        //[Route("~/film_catalog_film/Update")]
        [Route("~/login")]
        public ActionResult Login(string user, string pass, string password)
        {
            //film_user oLogin = new film_userService().CheckLogin(user,pass);
            //return Json(new { result = (oLogin != null?1:0) });
            if (pass == null)
            {
                pass = password;
            }
            film_user oLogin = new film_userService().CheckLogin(user, pass);
            int status1 = (oLogin != null ? 0 : 1);
            if (oLogin != null)
            {
                Dictionary<object, object> checkExpired = new film_userService().CheckExpired(oLogin);
                return Json(new { result = 1, status = status1, package_info = checkExpired, user = oLogin });
            }
            else
            {
                return Json(new { code = "error", message = "Tài khoản hoặc mật khẩu không đúng" });
            }
        }
        [HttpPost]
        [Route("~/Login1")]
        public ActionResult Login1([FromBody] film_user objUser)
        {
            film_user oLogin = new film_userService().CheckLogin(objUser.email, objUser.password);
            int status1 = (oLogin != null ? 0 : 1);
            if (oLogin != null)
            {
                Dictionary<object, object> checkExpired = new film_userService().CheckExpired(oLogin);
                string mgs = oLogin != null ? "Đăng nhập thành công" : "Đăng nhập thất bại";
                return Json(new { message = mgs, result = 1, status = status1, package_info = checkExpired, token = System.Guid.NewGuid().ToString(), user = oLogin });
            }
            else
            {
                return Json(new { code = "error", message = "Tài khoản hoặc mật khẩu không đúng" });
            }
        }
        [HttpPost]
        //[Route("~/film_catalog_film/Update")]
        [Route("~/login_android")]
        public ActionResult LoginAndroid(LoginRequest login)
        {
            //film_user oLogin = new film_userService().CheckLogin(user,pass);
            //return Json(new { result = (oLogin != null?1:0) });
            film_user oLogin = new film_userService().CheckLogin(login.user, login.password.ToLower());
            int status1 = (oLogin != null ? 0 : 1);
            if (oLogin != null)
            {
                Dictionary<object, object> checkExpired = new film_userService().CheckExpired(oLogin);
                string mgs = oLogin != null ? "Đăng nhập thành công" : "Đăng nhập thất bại";
                return Json(new { message = mgs, result = 1, status = status1, package_info = checkExpired, token = System.Guid.NewGuid().ToString(), user = oLogin });
            }
            else
            {
                return Json(new { code = "error", message = "Tài khoản hoặc mật khẩu không đúng" });
            }
        }
        [HttpPost]
        //[Route("~/film_catalog_film/Update")]
        [Route("~/login_smarttv")]
        public ActionResult LoginSmartTV(string user, string pass)
        {
            film_user oLogin = new film_userService().CheckLoginSmartTV(user, pass);
            int status1 = (oLogin != null ? 0 : 1);
            string token = (oLogin != null ? oLogin.token.ToString() : "");
            if (oLogin != null)
            {
                oLogin.is_required_paid = 0;
                string mgs = oLogin != null ? "Đăng nhập thành công" : "Đăng nhập thất bại";
                Dictionary<object, object> checkExpired = new film_userService().CheckExpired(oLogin);
                return Json(new { message = mgs, result = 1, status = status1, token, package_info = checkExpired, user = oLogin });
            }
            else
            {
                return Json(new { code = "error", message = "Tài khoản hoặc mật khẩu không đúng" });
            }
        }
        [HttpPost]
        [Route("~/ChangePass")]
        public JsonResult ChangePass(string token, string oldpass, string newpass)
        {
            if (token == null) return Json(new { code = "token_null", message = "Token không được để trống" });
            if (oldpass == null) return Json(new { code = "oldpass_null", message = "Mật khẩu cũ không được để trống" });
            if (newpass == null) return Json(new { code = "newpass_null", message = "Mật khẩu mới không được để trống" });
            Dictionary<string, object> dict = new film_userService().ChangePass(token, oldpass, newpass);
            return Json(dict);
        }
        [HttpPost]
        [Route("~/HuyDV")]
        public JsonResult HuyDV(string token)
        {
            if (token == null) return Json(new { code = "token_null", message = "Token không được để trống" });
            Dictionary<string, object> dict = new film_userService().HuyDV(token);
            return Json(dict);

        }
        [HttpPost]
        [Route("~/RecoveryPassword")]
        public JsonResult RecoveryPassword(string token, string email, string phone)
        {
            //if (phone != null || phone != "") return Json(new { code = "not_support", message = "Chưa hỗ trợ qua SMS" });
            if (string.IsNullOrEmpty(email)) return Json(new { code = "error", message = "Email không được để trống" });
            Dictionary<string, object> dict = new film_userService().RecoveryPassword(token, email, phone);
            return Json(dict);
        }
        [HttpGet]
        [Route("~/renew_token/{token}")]
        public JsonResult renew_token([FromRoute] string token)
        {
            if (token == null) return Json(new { code = "error", message = "Token không được để trống" });
            return Json(new film_userService().renew_token(token));
        }
    }

    public class LoginRequest
    {
        public string user { get; set; }
        public string password { get; set; }
    }
}
