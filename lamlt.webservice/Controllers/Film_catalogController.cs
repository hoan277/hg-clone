﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_catalog")]
    public class film_catalogController : Controller
    {
        // GET: film_catalogs
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_catalog/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_catalogService svrfilm_catalog = (new film_catalogService());
            List<vw_film_catalog> data = svrfilm_catalog.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_catalog.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_catalog/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_catalog obj = (new film_catalogService()).GetByID(id);
            if (obj == null) obj = (new film_catalogService()).InitEmpty();
            return PartialView("film_catalogEdit", obj);
        }
        [HttpPost]
        [Route("~/film_catalog/Update")]
        public ActionResult Update(film_catalog obj)
        {
            int result = new film_catalogService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_catalog/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_catalog obj = new film_catalogService().GetByID(id);
            return PartialView("film_catalogDetail", obj);
        }
        [HttpGet]
        [Route("~/film_catalog/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_catalog obj = new film_catalogService().GetViewByID(id);
            return PartialView("film_catalogDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_catalog/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_catalogService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_catalog/GetCateChild/{cateparrent_id}")]
        public JsonResult GetCateChild(int cateparrent_id)
        {
            if (cateparrent_id <= 0)
            {
                //return Json(new {message="Thất bại, cate parrent id không được nhỏ hơn hoặc bằng 0" });
                return Json(new film_catalogService().GetAllCateChild());
            }
            return Json(new film_catalogService().GetCateChild(cateparrent_id));
        }
        //https://sdk.lalatv.com.vn/film_catalog/GetBannerByCate/25?token=&language=en&ios=1

        [HttpGet]
        [Route("~/film_catalog/GetBannerByCate/{categoryId}")]
        public ActionResult GetBannerByCate(string token, string categoryId)
        {
            int ipad = 1;
            if (string.IsNullOrEmpty(categoryId))
            {
                categoryId = "-1";
            }
            List<vw_film_banner> list_banner_new = new List<vw_film_banner>();
            if (token != null)
            {
                PagingModel page = new PagingModel() { offset = 0, limit = 100, categoryid = int.Parse(categoryId) };
                film_user user = new film_userService().GetByToken(token);
                if (user != null)
                {
                    user_link user_Link = new user_linkService().GetById(user.link_id);
                    if (user_Link == null)
                    {
                        return Json(list_banner_new);
                    }
                    film_bannerService film_bannerSV = new film_bannerService();
                    list_banner_new = new film_catalogService().GetAllByTypeKid(int.Parse(categoryId), user_Link.typekid, ipad);
                }
                else list_banner_new = new film_catalogService().GetAllByTypeKid(int.Parse(categoryId), 2, ipad);
            }
            else
            {
                list_banner_new = new film_catalogService().GetAllByTypeKid(int.Parse(categoryId), 2, ipad);
            }
            return Json(list_banner_new);
        }
    }
}