﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video")]
    public class film_videoController : Controller
    {
        // GET: film_videos
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        //[HttpGet]
        //[Route("~/film_video/List")]
        //public ActionResult List(int length, int start, string search, int cpid)
        //{
        //    film_videoService svrfilm_video = (new film_videoService());
        //    List<vw_film_video> data = svrfilm_video.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
        //    int recordsTotal = (int)svrfilm_video.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
        //    int recordsFiltered = recordsTotal;
        //    int draw = 1;
        //    StringValues valueDraw = "";
        //    HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
        //    try { draw = int.Parse(valueDraw); } catch { }
        //    return Json(new
        //    {
        //        draw,
        //        recordsTotal,
        //        recordsFiltered,
        //        data
        //    });
        //}
        [HttpGet]
        [Route("~/film_video/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return PartialView("film_videoEdit", obj);
        }
        [HttpGet]
        [Route("~/film_video/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return View("Edit", obj);
        }
        [HttpPost]
        [Route("~/film_video/Update")]
        public ActionResult Update(film_video obj)
        {
            int result = new film_videoService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_videoService().UpdateStatus(obj);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/film_video/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute] string id)
        {
            int result = new film_videoService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/getImageupdate")]
        public IActionResult getImageupdate(int id)
        {
            List<film_video_image> result = new film_videoService().getupdateImg(id);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid)
        {
            int result = new film_videoService().UpdateImage(url, filmid);
            return Json(new { result = result });
        }
        //  class filmid
        //{
        //    public int[] catatogid { get;set }
        //    int filmid { get; set}
        //}
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/film_video/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_videoService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video obj = new film_videoService().GetByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpGet]
        [Route("~/film_video/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpGet]
        [Route("~/livestreaming/List")]
        public JsonResult GetAllLivestreaming()
        {
            var obj = new livestreamingService().GetViewAllItem(null);
            return Json(obj);
        }
        [HttpDelete]
        [Route("~/film_video/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/GetVideoLive")]
        public JsonResult GetVideoLive()
        {
            StringValues limit;
            HttpContext.Request.Query.TryGetValue("limit", out limit);
            var obj = new livestreamingService().GetVideoLive(int.Parse(limit));
            return Json(obj);
        }
        [HttpGet]
        [Route("~/film_video/GetVideoByCate")]
        public JsonResult GetVideoByCate()
        {
            StringValues cateId;
            StringValues limit;
            HttpContext.Request.Query.TryGetValue("cateId", out cateId);
            HttpContext.Request.Query.TryGetValue("limit", out limit);
            var obj = new livestreamingService().GetVideoByCate(int.Parse(cateId), int.Parse(limit));
            return Json(obj);
        }
        [HttpGet]
        [Route("~/film_video/GetVideoByCateOrderBy")]
        public JsonResult GetVideoByCateOrderBy()
        {
            StringValues s_cateId;
            StringValues s_limit;
            StringValues s_orderby;
            StringValues s_offset;
            StringValues s_type;
            StringValues s_filter;
            HttpContext.Request.Query.TryGetValue("cateId", out s_cateId);
            HttpContext.Request.Query.TryGetValue("limit", out s_limit);
            HttpContext.Request.Query.TryGetValue("offset", out s_offset);
            HttpContext.Request.Query.TryGetValue("orderby", out s_orderby);
            HttpContext.Request.Query.TryGetValue("type", out s_type);
            HttpContext.Request.Query.TryGetValue("filter", out s_filter);
            int cateId = int.Parse(s_cateId);
            int limit = !string.IsNullOrEmpty(s_limit) ? int.Parse(s_limit) : 10;
            int offset = !string.IsNullOrEmpty(s_offset) ? int.Parse(s_offset) : 10;
            string orderby = s_orderby;
            string type = s_type;
            if (string.IsNullOrEmpty(orderby))
            {
                orderby = "random";
            }
            string filter = s_filter;
            var obj = new livestreamingService().GetVideoByCateOrderBy(cateId, limit, offset, orderby, type, filter);
            return Json(obj);
        }
        [HttpPost]
        [Route("~/film_video/SearchByTitle")]
        public JsonResult SearchByTitle(string[] list_title)
        {
            var obj = new film_videoService().SearchByTitle(list_title);
            return Json(obj);
        }
        [HttpGet]
        [Route("~/film_video/update_duration")]
        public JsonResult update_duration(int id, string title, string duration)
        {
            var obj = new film_videoService().update_duration(id, title, duration);
            return Json(obj);
        }
    }
}