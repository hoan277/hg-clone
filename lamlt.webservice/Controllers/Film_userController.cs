﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_user")]
    public class film_userController : Controller
    {
        // GET: film_users
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_user/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_userService svrfilm_user = (new film_userService());
            List<vw_film_user> data = svrfilm_user.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_user.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_user/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_user obj = (new film_userService()).GetByID(id);
            if (obj == null) obj = (new film_userService()).InitEmpty();
            return PartialView("film_userEdit", obj);
        }
        [HttpPost]
        [Route("~/film_user/Update")]
        public ActionResult Update([FromRoute] film_user obj)
        {
            int result = new film_userService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/register")]
        public JsonResult register(string user, string email, string password)
        {
            if (password == null) return Json(new { message = "Password không được rỗng" });
            object objUser = new ServiceMeService().processing_register(user, email, password);
            return Json(new { message = objUser != null ? "Đăng ký thành công" : "Đã tồn tại tài khoản hoặc email", user_info = objUser });
        }
        [HttpPost]
        [Route("~/normal_register")]
        public JsonResult normal_register(string fullname, string phone, string pass)
        {
            if (fullname == null) return Json(new { message = "Họ và tên không được rỗng" });
            if (phone == null) return Json(new { message = "Số điện thoại không được rỗng" });
            if (pass == null) return Json(new { message = "Password không được rỗng" });
            return Json(new film_userService().normal_register(fullname, phone, pass));
        }
        [HttpPost]
        [Route("~/film_user/Login")]
        public ActionResult Login(string user, string pass)
        {
            film_user oResult = new film_userService().CheckLogin(user, pass);
            return Json(new { message = (oResult != null ? 1 : 0) });
        }
        [HttpGet]
        [Route("~/film_user/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_user obj = new film_userService().GetByID(id);
            return PartialView("film_userDetail", obj);
        }
        [HttpGet]
        [Route("~/film_user/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_user obj = new film_userService().GetViewByID(id);
            return PartialView("film_userDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_user/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_userService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/logout")]
        public JsonResult logout(string token)
        {
            if (token == null)
            {
                return Json(new { message = "Xóa token thất bại", token = token });
            }
            int result = new film_userService().DeleteToken(token);
            return Json(new { message = "Thành công", result = result });
        }
        [HttpPost]
        [Route("~/film_user/UpdateProfile")]
        public ActionResult UpdateProfile(film_user obj)
        {
            if (obj.token == null) return Json(new { message = "Mã token không được để trống", token = obj.token });
            int result = new film_userService().UpdateProfile(obj);
            return Json(new { message = result <= 0 ? "Cập nhật thất bại" : "Cập nhật thành công", result = result });
        }
    }
}