﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_role")]
    public class film_roleController : Controller
    {
        // GET: film_roles
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_role/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_roleService svrfilm_role = (new film_roleService());
            List<film_role> data = svrfilm_role.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_role.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_role/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_role obj = (new film_roleService()).GetByID(id);
            if (obj == null) obj = (new film_roleService()).InitEmpty();
            return PartialView("film_roleEdit", obj);
        }
        [HttpPost]
        [Route("~/film_role/Update")]
        public ActionResult Update(film_role obj)
        {
            int result = new film_roleService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_role/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_role obj = new film_roleService().GetByID(id);
            return PartialView("film_roleDetail", obj);
        }
        [HttpGet]
        [Route("~/film_role/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_role obj = new film_roleService().GetViewByID(id);
            return PartialView("film_roleDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_role/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_roleService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}