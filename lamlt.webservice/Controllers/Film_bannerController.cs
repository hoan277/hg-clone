﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_banner")]
    public class film_bannerController : Controller
    {
        // GET: film_banners
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_banner/ListFull")]
        public ActionResult List(int length, int start, string search)
        {
            film_bannerService svrfilm_banner = (new film_bannerService());
            List<film_banner> data = svrfilm_banner.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_banner.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpGet]
        [Route("~/film_banner/List")]
        public ActionResult ListByToken(int length, int start, string search, string token)
        {
            PagingModel page = new PagingModel() { offset = start, limit = length, search = search };
            if (token != null)
            {
                film_user user = new film_userService().GetByToken(token);
                if (user != null)
                {
                    user_link user_Link = new user_linkService().GetById(user.link_id);
                    //typekid=0: Tất cả, typekid=1: Trẻ em , typekid=2: Người lớn
                    List<film_catalog> list_catalog = new film_catalogService().GetAllByTypeKid(null, user_Link.typekid);
                    List<vw_film_banner> list_banner_new = new List<vw_film_banner>();
                    film_bannerService film_bannerSV = new film_bannerService();
                    foreach (var catalog in list_catalog)
                    {
                        List<vw_film_banner> list_baner = film_bannerSV.GetAllViewByCatalog(page, catalog.Id);
                        foreach (var banner in list_baner)
                        {
                            list_banner_new.Add(banner);
                        }
                    }
                    return Json(new { code = 200, msg = "Thành công", data = list_banner_new });
                }
                return Json(new { code = 0, msg = "Sai token hoặc token hết hạn", data = "" });

            }
            return Json(new { code = -1, msg = "Thiếu token", data = "" });
        }


        [HttpGet]
        [Route("~/film_banner/GetAllBanner/")]
        public ActionResult GetAllBanner(string token, string categoryId, int ipad)
        {
            if (string.IsNullOrEmpty(categoryId))
            {
                categoryId = "-1";
            }
            List<vw_film_banner> list_banner_new = new List<vw_film_banner>();
            if (token != null)
            {
                PagingModel page = new PagingModel() { offset = 0, limit = 100, categoryid = int.Parse(categoryId) };
                film_user user = new film_userService().GetByToken(token);
                if (user != null)
                {
                    user_link user_Link = new user_linkService().GetById(user.link_id);
                    if (user_Link == null)
                    {
                        return Json(list_banner_new);
                    }
                    film_bannerService film_bannerSV = new film_bannerService();
                    list_banner_new = new film_bannerService().GetAllByTypeKidBanner(int.Parse(categoryId), user_Link.typekid);
                }
                else list_banner_new = new film_bannerService().GetAllByTypeKidBanner(int.Parse(categoryId), 2);
            }
            else
            {
                list_banner_new = new film_bannerService().GetAllByTypeKidBanner(int.Parse(categoryId), 2);
            }
            return Json(list_banner_new);
        }
        [HttpGet]
        [Route("~/film_banner/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_banner obj = (new film_bannerService()).GetByID(id);
            if (obj == null) obj = (new film_bannerService()).InitEmpty();
            return PartialView("film_bannerEdit", obj);
        }
        [HttpPost]
        [Route("~/film_banner/Update")]
        public ActionResult Update(film_banner obj)
        {
            int result = new film_bannerService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_banner/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_banner obj = new film_bannerService().GetByID(id);
            return PartialView("film_bannerDetail", obj);
        }
        [HttpGet]
        [Route("~/film_banner/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_banner obj = new film_bannerService().GetViewByID(id);
            return PartialView("film_bannerDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_banner/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_bannerService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}