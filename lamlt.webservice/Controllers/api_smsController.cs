﻿using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Net.Http;
using System.Text;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/sms")]
    public class api_smsController : Controller
    {
        // GET: film_videos
        [HttpGet]
        public JsonResult Index()
        {
            //msisdn,subtype,substate, sLine
            //string phone,string subtype, string substate, string fullcmd
            StringValues access_token;
            Request.Query.TryGetValue("access-token", out access_token);
            StringValues phone;
            Request.Query.TryGetValue("phone", out phone);
            StringValues msisdn;
            Request.Query.TryGetValue("msisdn", out msisdn);
            StringValues subtype;
            Request.Query.TryGetValue("subtype", out subtype);
            StringValues substate;
            Request.Query.TryGetValue("substate", out substate);
            StringValues line;
            Request.Query.TryGetValue("line", out line);
            ServiceMeService service = new ServiceMeService();
            object datax = service.update_create_user(phone, subtype, substate, line);

            //service.create_token_for_user(phone, subtype, substate);
            //string myJson = "{'Username': 'myusername','Password':'pass'}";
            try
            {
                postToServer(phone, line, "cp12", "aWZsaXgyMDIwMDgxMg==");
            }
            catch (Exception ex)
            {
                LogUtil.getInstance().logSk("====>Exception: " + ex.ToString(), "update_pr");
            }
            return Json(datax);
        }

        public async void postToServer(string phone, string line, string username, string password)
        {
            LogUtil.getInstance().logSk("====>Call to server for line " + line, "update_pr");

            //line = "20200812075024:hongancp,Hongancp!@-45,LALATV_NGAY,326200348,20200812075036,0,REAL,3000,XNA,20200813000000,";
            if (string.IsNullOrEmpty(line)) return;
            if (line.Split(':').Length <= 1) return;
            string requestid = line.Split(':')[0];
            string[] arritem = line.Split(':')[1].Split(',');
            string price = arritem[7];
            string keyword = arritem[8];
            string transactiontime = arritem[4];
            phone = "84" + phone;
            string string_to_sign = phone + ": " + requestid + ": " + username + ": " + password;
            string myJson =
                    "{'ServiceName':'IFLIX','Type':1,'Channel':'SMS', 'Msisdn':'" + phone + "'," +
                    "'RequestID':'" + requestid + "','ChargedFee':" + price + ",'Detail':'" + keyword + "'," +
                    "'TransactionTime':'" + transactiontime + "','Status':1," +
                    "'Description':'Register','Username':'" + username + "','Password':'" + password + "'," +
                    "'Sign':'" + string_to_base64(string_to_sign) + "'}";
            LogUtil.getInstance().logSk("====>Params: " + myJson, "update_pr");
            if (keyword.ToLower().StartsWith("xna"))
            {
                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(
                        "http://report.vimotech.vn/updatePackage.html",
                         new StringContent(myJson, Encoding.UTF8, "application/json"));
                    string result = await response.Content.ReadAsStringAsync();
                    LogUtil.getInstance().logSk("====>response: " + result, "update_pr");
                }
            }
        }

        public string string_to_base64(string txt)
        {
            //string txt = "Test";
            byte[] encodedBytes = System.Text.Encoding.ASCII.GetBytes(txt);
            string encodedTxt = Convert.ToBase64String(encodedBytes);
            return encodedTxt;
        }
    }
}