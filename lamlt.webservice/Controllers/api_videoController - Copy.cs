﻿using System;
using System.Collections.Generic;
using lamlt.web.Services;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using lamlt.web.Models;
using System.Data;
using lamlt.data;

using System.Threading;
using Microsoft.Extensions.Primitives;
using Lalatv.Models;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/video")]
    public class api_videoController : Controller
    {
        // GET: film_videos
        [HttpGet]
        public JsonResult Index()
        {
            StringValues access_token;
            Request.Query.TryGetValue("access-token", out access_token);
            StringValues action;
            Request.Query.TryGetValue("action", out action);
            StringValues query_type;
            Request.Query.TryGetValue("query-type", out query_type);
            StringValues user_type;
            Request.Query.TryGetValue("user-type", out user_type);
            StringValues start_index;
            Request.Query.TryGetValue("start-index", out start_index);
            StringValues max_results;
            Request.Query.TryGetValue("max-results", out max_results);
            StringValues categoryId;
            Request.Query.TryGetValue("categoryId", out categoryId);
            StringValues videos_id;
            Request.Query.TryGetValue("videos-id", out videos_id);
            StringValues videos_search;
            Request.Query.TryGetValue("search", out videos_search);
            string search = "";
            try { search = videos_search.ToString(); } catch { }
            object datax=new ServiceMeService().processing_data(access_token, query_type, user_type, start_index, max_results, categoryId, videos_id,search);
            return Json(datax);
        }
        [HttpGet]
        [Route("~/authen")]
        public ActionResult authen()
        {
            StringValues access_token;
            Request.Query.TryGetValue("token", out access_token);
            return Json(new { token = access_token });
        }
        [HttpGet]
        [Route("~/film_video/syn")]
        public ActionResult syn(int? syn)
        {
            if (syn == 1)
            { 
                new ServiceMeService().processing_syn();
                return Json(new { a = 1 });
            }
            return Json(new {a=0});
        }
        [HttpGet]
        [Route("~/film_video/List")]
        public ActionResult List(int length, int start, string search, int cpid)
        {
            film_videoService svrfilm_video = (new film_videoService());
            List<vw_film_video> data = svrfilm_video.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
            int recordsTotal = (int)svrfilm_video.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_video/Edit/{id}")]
        public PartialViewResult Edit([FromRoute]string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return PartialView("film_videoEdit", obj);
        }
        [HttpGet]
        [Route("~/film_video/Edits/{id}")]
        public ActionResult Edits([FromRoute]string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return View("Edit", obj);
        }
		[HttpPost]
        [Route("~/film_video/Update")]
        public ActionResult Update(film_video obj)
        {
            int result = new film_videoService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_videoService().UpdateStatus(obj);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/film_video/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute]string id)
        {
            int result = new film_videoService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route ("~/film_video/getImageupdate")]
        public IActionResult getImageupdate(int id)
        {
            List<film_video_image> result = new film_videoService().getupdateImg(id);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid)
        {
            int result = new film_videoService().UpdateImage(url, filmid);
            return Json(new { result = result });
        }
        //  class filmid
        //{
        //    public int[] catatogid { get;set }
        //    int filmid { get; set}
        //}
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/film_video/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_videoService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
		[Route("~/film_video/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute]string id)
        {
			film_video obj = new film_videoService().GetByID(id);
            return PartialView("film_videoDetail",obj);
        }
		[HttpGet]
		[Route("~/film_video/GetViewDetail/{id}")]
		public PartialViewResult GetViewDetail([FromRoute]string id)
        {
			vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoDetail",obj);
        }
		[HttpDelete]
		[Route("~/film_video/Delete/{id}")]
        public JsonResult Delete([FromRoute]string id)
        {
            int result = new film_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }  
        
        
    }    
}