﻿function film_role() {
    var self = this;
    this.tableName = "film_role";
    this.idModal = "modalfilm_role";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_role/Update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.menuDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/menu_edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                var menu = $("#menu").val();
                if (menu != null) {
                    showDialogLoading();
                    $.ajax({
                        method: "POST",
                        url: "/film_role/menu_update",
                        data: {
                            id: id,
                            list_menu: menu
                           
                        },
                        success: function (result) {
                            hideDialogLoading();
                            if (result.result > 0) {
                                alert(result.result);
                            }
                            else {
                                alert(0);
                            }
                            $("#" + self.idModal).modal("hide"); self.searchGrid();
                        },
                        error: function (result) {
                            alert("error"+result.result);
                        }
                    });
                }
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_role').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "searchable": false,
            "orderable": false,
            "targets": 0,
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
                    delete d.columns;

                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    ,{ 'data': 'title' }
                    , { 'data': 'code' }
                    //, { 'data': 'menu' }
                    , {
                        'data': null, render: function (data, type, row) {
                            return (row.status == 1) ? "Đã duyệt" : "Chưa duyệt";
                        }
                    }
                    , { 'data': 'desc' }
                    //, {
                    //    'data': null, render: function (data, type, row) {
                    //        return (row.datacreated == null) ? "" : $datepicker.formatDate("dd/mm/yy", new Date(row.datecreated));
                    //    }
                    //}
                    //, {
                    //    'data': null, render: function (data, type, row) {
                    //        return (row.dateupdated == null) ? "" : $datepicker.formatDate("dd/mm/yy", new Date(row.dateupdated));
                    //    }
                    //}
                    , {
                        "data": null, render: function (data, type, row) {
                            var info =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')"><span style="color:#3f51b5">Chi tiết</span></button>';
                            var edit =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')"><span style="color:#f39c12">Sửa</span></button>';
                            var delete1 =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')"><span style="color:#761c19">Xóa</span></button>';
                            var active =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')"><span style="color:#f17c2d">Duyệt</span></button>';
                            var menu =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.menuDialog(' + row.id + ')"><span style="color:#f39c12">Menu</span></button>';
                            var html = "";
                            if (row.status == 1) {
                                htmlMenu = info + edit + delete1 ;
                            }
                            else {
                                htmlMenu = info + edit + delete1  + active;
                            }
                            return htmlMenu;
                        }
                    }

                ],
            // stt
            "order": [[1, 'asc']]
        });
        // stt
        self.$table.on('order.dt search.dt', function () {
            self.$table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '           <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button> <button type="button" id="btnSave' + self.tableName + '" class="btn btn-success" >Lưu</button> ';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}