﻿function film_video() {
    var self = this;
    this.tableName = "film_video";
    this.idModal = "modalfilm_video";
    this.$table;
    $("#tatca").on("click", function () {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "tatca"
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    });
    $("#dachon").on("click", function () {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "dachon",
                list_id: selected_id.toString()
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    });
    // total : 14 function
    this.removePlayer = function removePlayer() {
        var myPlayer = WowzaPlayer.get('playerElement');
        if (myPlayer != null) {
            myPlayer.finish();
            myPlayer.destroy();
        }
    }
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                // console.log(pdata);
                $.post("/film_video/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {

                        /*debugger;
                        console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                        imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                        //console.log($("input[type='checkbox'][name='showhome']").val());
                        //console.log(imgs)
                        self.UpdateCheckbox(data.result, imgs);
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    // updateEdit view edit
    this.updateEdit = function (id) {
        if (!$("#form" + self.tableName).valid()) return;
        var pdata = getFormData($("#form" + self.tableName));
        showDialogLoading();
        // console.log(pdata);
        $.post("/film_video/update", { obj: pdata }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {

                /*debugger;
                console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                //console.log(imgs)
                self.UpdateCheckbox(data.result, imgs);
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); });
            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });
    }
    //  chuyển trạng thái status rồi update trạng thái
    this.updateStatus = function (id, status) {
        // debugger;
        $.post("/film_video/updatestatus", {
            obj: { id: id, status: status }
        }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });

    }
    // update anh trong bang film_video_image
    this.UpdateImage = function (url, filmid) {
        //debugger;
        $.post("/film_video/UpdateImage", {
            //obj: { id: id, url: url }
            url: url, filmid: filmid
        }, function (data) {
            hideDialogLoading();
            self.getImageupdate(filmid);
            //if (data.result > 0) {
            //    bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            //}
            //else {
            //    alert("Có lỗi khi tạo :" + data.mess);
            //}
        });
    }
    // select hinh anh theo filmid trong ban film_video_image
    this.getImageupdate = function (filmid) {
        $.get("/film_video/getImageupdate?id=" + filmid, function (data) {
            hideDialogLoading();
            $("#list_image").html("");
            for (var index in data.result) {
                if (data.result[index].url != undefined) {
                    $("#list_image").append("<div style= 'width:20%;display:inline-grid;margin:5px'><img src='" + data.result[index].url + "' width=100px/><a href='javascript:new film_video().delimage(" + data.result[index].id + "," + data.result[index].filmid + ")'>'<img src='/images/icon/icon-delete.png' style='border: none;' title='Xóa'></a>'</div>");
                }
            }
        });
    }
    /*
     * drm gen key
     * delete key
     */

    this.drmDialog = function (filmid) {
        $("a#" + filmid + " span").addClass("spin");
        $.ajax({
            type: "GET",
            url: "https://sdk.lalatv.com.vn/drm/genkey?itemid=" + filmid,
            success: function (data) {
                alert(data.s == 1 ? "Đã tạo key thành công" : "Tạo key thất bại");
                $("a#" + filmid + " span").removeClass("spin");
            },
            error: function (xhr) {
                alert(xhr.responseText);
            },
        });
    }
    this.deletedrmDialog = function (fileName) {
        $.ajax({
            type: "POST",
            url: "/film_video/deletedrmDialog/",
            data: { fileName: fileName },
            success: function (data) {
                alert(data.message);
            }, error: function (data) {
                alert("Lỗi xóa key");
            }
        });
    }
    // xóa hình ảnh trong film_video_image theo id
    this.delimage = function (id, filmid) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/deteleimage/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { self.getImageupdate(filmid); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    // checkbox
    this.UpdateCheckbox = function (filmid, catalog_id) {

        $.post("/film_video/UpdateCheckbox", {
            catalog_id: catalog_id, filmid: filmid
        }, function (data) {
        });
    }
    this.edits = function (id) {
        window.location.href = "/" + self.tableName + "/Edits/" + id;
    }
    this.reconvert = function (current_id, upload_file) {
        var filename = upload_file.match(/automedia\/[^/]*.mp4/)[0].replace("automedia", "").replace("/", "");
        if (bootbox.confirm("Bạn có chắc chắn muốn chuyển đổi lại không?", function (result) {
            if (!result) return;
            $("a#" + current_id).addClass("current_convert");
            $("a#" + current_id).attr("title", "Đang chuyển đổi, vui lòng đợi trong giây lát!");
            $("a#" + current_id + " span").addClass("spin");
            $("a#" + current_id).html('<span class="glyphicon glyphicon-repeat spin" aria-hidden="true" style="color:#FF9800"></span> Đang chạy');
            $.ajax({
                type: 'get',
                contentType: 'application/json',
                dataType: 'json',
                url: "/" + self.tableName + "/reconvert",
                data: {
                    file_upload: filename
                },
                success: function (data) {
                    $("a#" + current_id).removeClass("current_convert");
                    $("a#" + current_id).attr("title", "Đã hoàn thành!");
                    $("a#" + current_id + " span").removeClass("spin");
                    $("a#" + current_id).html('<span class="glyphicon glyphicon-check" aria-hidden="true" style="color:#4CAF50"></span> Hoàn thành');
                }
            });
        }));
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        ofilm_video.removePlayer();
        if (!document.getElementById(self.idModal)) {
            self.initModal(self.idModal);
        }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.trainerDialog = function trainerDialog(id) {
        ofilm_video.removePlayer();
        if (!document.getElementById(self.idModal)) {
            self.initModal(self.idModal);
        }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/get_trainer/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        ofilm_video.removePlayer();
        showDialogLoading();
        var stt = 1;
        self.$table = $('#example_film_video').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            }, "rowId": "id",
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_video_title").val();
                    d.cpid = $("#search_film_video_cpid").val();
                    d.status = $("#search_film_video_status").val();
                    d.catalogid = $("#search_film_video_catalogid").val();
                    d.film_type = $("#film_type").val();
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var fileName = "'" + row.upload_file + "'";
                            var info2 =
                                '<button type="button" class="btn btn-outline-primary tool_btn tool_btnfirst" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')"><span style="color:#3f51b5">Xem</span></button>';
                            var edit2 =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.edits(' + row.id + ')"><span style="color:#f39c12">Sửa</span></button>';
                            var delete2 =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')"><span style="color:#761c19">Xóa</span></button>';
                            var gen_key2 =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.drmDialog(' + row.id + ')"><span style="color:#f17c2d">Gen Key</span></button>';
                            var delete_key2 =
                                '<button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.deletedrmDialog(' + fileName + ')"><span style="color:#f17c2d">Xoá key</span></button>';
                            var trainer2 =
                                '<button type="button" class="btn btn-outline-primary tool_btn tool_btnlast" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.trainerDialog(' + row.id + ')"><span style="color:#3f51b5">Trainer</span></button>';
                            var htmlMenu = "";

                            if (row.trainer_url == null || row.trainer_url == "") {
                                htmlMenu = '<div class="tools">' + info2 + edit2 + delete2 + gen_key2 + delete_key2 + '</div>';
                            }
                            else {
                                htmlMenu = '<div class="tools">' + info2 + edit2 + delete2 + gen_key2 + delete_key2 + trainer2 + '</div>';
                            }
                            return htmlMenu;
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            var check = 0;
                            var rs = "";
                            rs += check == 0 ? "{'STT':'" + stt + "'," : ",{'STT':'" + stt + "',";
                            var filmType = row.film_type == 0 ? "Phim lẻ" : "Phim bộ";
                            rs += "'Tên Phim':'" + row.title + "',";
                            rs += "'Code':'" + row.code + "',";
                            rs += "'Loại nội dung':'" + row.catalog + "',";
                            rs += "'Mô tả phim':'" + row.desc + "',";
                            rs += "'Thể Loại':'" + filmType + "',";
                            rs += "'Năm sản xuất':'" + row.publish_year + "',";
                            rs += "'Quốc gia':'" + row.publish_countryid_title + "',";
                            rs += "'Quốc gia':'" + row.catalog_id_title + "',";
                            rs += "'Link phim':'" + row.upload_file + "}";
                            check++;
                            stt++;
                            var htmlMenu = '<div class="checkbox"><label><input class="export_selected" type="checkbox" id="export_selected" name="export_selected" type="checkbox" value="' + rs + '">' + row.id + '</label></div >';
                            return htmlMenu;

                        }
                    }
                    , { 'data': 'title' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.desc == null) return "";
                            if (row.desc.length <= 50) return row.desc;
                            return row.desc.substr(0, 49) + "...";
                        }
                    }
                    , { 'data': 'publish_year' }
                    , { 'data': 'publish_countryid_title' }
                    , { 'data': 'catalog_id_title' }
                    , {
                        'data': null, render: function (row) {
                            return row.film_type == 1 ? "Phim bộ" : "Phim lẻ";
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row, thumb_file) {
                            var htmlMenu = '<img src ="' + data.thumb_file + '" alt = "image" width = "100" height = "100"/>';
                            return htmlMenu;
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.exclusive == 0) return "không phải độc quyền";
                            if (row.exclusive == 1) return "độc quyền";
                            return "Unknown";
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.showhome == 0) return "không hiển thị";
                            if (row.showhome == 1) return "hiển thị";
                            return "Unknown";
                        }
                    }
                    , { 'data': 'price' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.status == 0) return "Nháp";
                            if (row.status == 1) return "Chờ duyệt";
                            if (row.status == 2) return "Đã duyệt";
                            if (row.status == 3) return "Từ chối duyệt";
                            if (row.status == 4) return "Đã cập nhật ảnh";
                            if (row.status == 5) return "Đang convert";
                            if (row.status == 6) return "Cập nhật ảnh lỗi";
                            if (row.status == 7) return "Quá hạn";
                            return "Unknown";
                        }
                    }
                    , { 'data': 'cpid_title' }
                    , { 'data': 'code' }
                    , { 'data': 'tags' }
                    , { 'data': 'episode_id' }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.upload_trainer = function upload_trainer() {
        var fd = new FormData();
        var files = $('#upload_trainer1')[0].files[0];
        console.log(files);
        fd.append('file', files);
        $.ajax({
            url: "/" + 'film_video' + "/upload_trainer/",
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                ofilm_Video_image.LoadProgressBar(data);
            },
            error: function (data) {
                alert("Có lỗi khi upload");
            }
        });
    }
    this.LoadProgressBar = function LoadProgressBar(result) {
        var progressbar = $("#progressbar-5");
        var progressLabel = $(".progress-label");
        progressbar.show();
        $("#progressbar-5").progressbar({
            //value: false,
            change: function () {
                progressLabel.text(
                    progressbar.progressbar("value") + "%");  // Showing the progress increment value in progress bar
            },
            complete: function () {
                progressLabel.text("Loading Completed!");
                progressbar.progressbar("value", 0);  //Reinitialize the progress bar value 0
                progressLabel.text("");
                progressbar.hide(); //Hiding the progress bar
                var markup = "<tr><td>" + result + "</td><td><a href='#' onclick='DeleteFile(\"" + result + "\")'><span class='glyphicon glyphicon-remove red'></span></a></td></tr>"; // Binding the file name
                $("#ListofFiles tbody").append(markup);
                $('#Files').val('');
                $('#FileBrowse').find("*").prop("disabled", false);
            }
        });
        function progress() {
            var val = progressbar.progressbar("value") || 0;
            progressbar.progressbar("value", val + 1);
            if (val < 99) {
                setTimeout(progress, 25);
            }
        }
        setTimeout(progress, 100);
    }
    this.upload_youtube = function upload_youtube() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/export_upload_youtube",
            data: {
                type: "dachon",
                list_id: selected_id.toString()
            },
            success: function (data) {
                alert("Đang upload. Vui lòng đợi");
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
}
