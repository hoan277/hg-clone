﻿function film_video_update() {
    var self = this;
    this.tableName = "film_video_update";
    this.idModal = "modalfilm_video_update";
    this.$table;

    this.editDialog = function (id) {

        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {                                  
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                // console.log(pdata);
                $.post("/film_video_update/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {

                        /*debugger;
                        console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                        imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                        //console.log(imgs)
                        self.UpdateCheckbox(data.result, imgs);
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });


                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
				/*
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data) 
					{ 
						hideDialogLoading();						
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});*/
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    // updateEdit view edit
    this.updateEdit = function (id) {
        if (!$("#form" + self.tableName).valid()) return;
        var pdata = getFormData($("#form" + self.tableName));
        showDialogLoading();
        // console.log(pdata);
        $.post("/film_video_update/update", { obj: pdata }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {

                /*debugger;
                console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                //console.log(imgs)
                self.UpdateCheckbox(data.result, imgs);
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); });


            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });
    }
    //  chuyển trạng thái status rồi update trạng thái
    this.updateStatus = function (id, status) {
        // debugger;
        $.post("/film_video_update/updatestatus", {
            obj: { id: id, status: status }
        }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });

    }
    // update anh trong bang film_video_update_image
    this.UpdateImage = function (url, filmid) {
        //debugger;
        $.post("/film_video_update/UpdateImage", {
            //obj: { id: id, url: url }
            url: url, filmid: filmid
        }, function (data) {
            hideDialogLoading();
            self.getImageupdate(filmid);
            //if (data.result > 0) {
            //    bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            //}
            //else {
            //    alert("Có lỗi khi tạo :" + data.mess);
            //}
        });
    }
    // select hinh anh theo filmid trong ban film_video_update_image
    this.getImageupdate = function (filmid) {
        $.get("/film_video_update/getImageupdate?id=" + filmid, function (data) {
            hideDialogLoading();
            $("#list_image").html("");
            for (var index in data.result) {
                if (data.result[index].url != undefined) {
                    $("#list_image").append("<div style= 'width:20%;display:inline-grid;margin:5px'><img src='" + data.result[index].url + "' width=100px/><a href='javascript:new film_video_update().delimage(" + data.result[index].id + "," + data.result[index].filmid + ")'>'<img src='/images/icon/icon-delete.png' style='border: none;' title='Xóa'></a>'</div>");
                }
            }
        });
    }
    // xóa hình ảnh trong film_video_update_image theo id
    this.delimage = function (id, filmid) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/deteleimage/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { self.getImageupdate(filmid); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    // quét file scan
    this.getfiles = function (status, upload_file) {
        showDialogLoading();
        // this.filePaths = Directory.GetFiles("~\upload_video\"."*.mp4");
        $.get("/film_video_update/getfiles", {
            status: status, upload_file: upload_file
        }, function (data) {
                hideDialogLoading();
                self.searchGrid(); 
                //if (data.result > 0) {
                //bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                //}
                //else {
                //    alert("Có lỗi khi tạo :" + data.mess);
                //}
            });
        
    }
    // checkbox 
    this.UpdateCheckbox = function (filmid, catalog_id) {

        $.post("/film_video_update/UpdateCheckbox", {
            //obj: { catalog_id: catalog_id, filmid: film_catalog_film.filmid }
            catalog_id: catalog_id, filmid: filmid
        }, function (data) {
            });
    }
    this.edits = function (id) {
        window.location.href = "/" + self.tableName + "/Edits/" + id;
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_video_update').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_video_update_title").val();
                    //   d.cpid = $("#search_film_video_update_cpid").val();
                    d.status = $("#search_film_video_status").val();

                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , {
                        'data':'title' 
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.desc == null) return "";
                            if (row.desc.length <= 50) return row.desc;
                            return row.desc.substr(0, 49) + "...";
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.format == null) return "";
                            if (row.format == 0) return "HD";
                            return "SD";
                        }
                    }
                    , { 'data': 'publish_year' }
                    , { 'data': 'publish_countryid' }
                    , { 'data': 'duration' }
                    , { 'data': 'actor' }
                    , { 'data': 'director' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.film_type == 0) return "Việt Sub";
                            if (row.film_type == 1) return "Thuyết Minh";
                            if (row.film_type == 2) return "Sub+Việt ";
                            return "Unknown";
                        }
                    }
                    , { 'data': 'catalog_id' }
                    , {
                        "data": null, render: function (data, type, row) {
                            //var htmlMenu = '<img src ="@Model.thumb_file" alt = "image"/>';
                            var htmlMenu = '<img src ="' + row.thumb_file + '" alt = "image" width = "100" height = "100"/>';
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'exclusive' }
                    , { 'data': 'upload_file' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.status == 0) return "Nháp";
                            if (row.status == 1) return "Chờ duyệt";
                            if (row.status == 2) return "Đã duyệt";
                            if (row.status == 3) return "Từ chối"; 
                            return "Unknown";
                        }
                    }
                    , { 'data': 'cpid' }
                    , { 'data': 'code' }
                    , { 'data': 'tags' }
                    ,
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu =
                                '<a style="color: #00a65a;font-weight: bold;" href="#" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                                ' Xem </a> ' +
                                '<a href="#" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.edits(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                                ' Sửa </a> ' +
                                '<a href="#" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                ' Xóa </a> ';
                            return htmlMenu;
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }

}