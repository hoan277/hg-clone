﻿
var $table;

$(document)
    .ready(function () {
        reportDate(); 
        setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

    	function searchGrid() {
    	    showDialogLoading();
    	    if ($table)
    		    $table.destroy();
    		$table = $("#example")
            .DataTable({
            	fixedHeader: {
            		header: true,
            		footer: true
            	},
            	language: {
            		sProcessing: "Đang xử lý...",
            		sLengthMenu: "Xem _MENU_ mục",
            		sZeroRecords: "Không tìm thấy dòng nào phù hợp",
            		sInfo: "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            		"sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            		"sInfoFiltered": "(được lọc từ _MAX_ mục)",
            		"sInfoPostFix": "",
            		"sSearch": "Tìm:",
            		"sUrl": "",
            		"oPaginate": {
            			"sFirst": "Đầu",
            			"sPrevious": "Trước",
            			"sNext": "Tiếp",
            			"sLast": "Cuối"
            		}
            	},
            	"processing": true,
            	"serverSide": true,
            	"initComplete": function (settings, json) {
            		hideDialogLoading();
            		var api = this.api();
            		$(api.column(2).footer()).html("Tổng: " + convert2Money(json.totalAmount.toString()));
            	},
            	/*bFilter: false, bInfo: false,*/
            	"dom": '<"top">rt<"bottom" lpi><"clear">',
            	"ajax": {
            		"url": "/report/ListDoanhThu",
            		"data": function (d) {
            			d.search = "";
            			d.FromDate = $("input[name='FromDate']").val();
            			d.ToDate = $("[name='ToDate']").val();
            			d.columns = "";
            		}
            	},
            	"columns":
                [
                    {
                    	"data": null,
                    	render: function (data, type, row, infor) {

                    		return $table.page.info().page + infor.row + 1;
                    	}
                    },
                    {
                    	"data": null,
                    	render: function (data, type, row, infor) {
                    	    if (row.DatePayment != null) {
                    	        return moment(new Date(parseInt(row.DatePayment.slice(6, -2)))).format("DD-MM-YYYY");
                    		}
                    		return "";
                    	}
                    },
					 {
					 	"data": null,
					 	render: function (data, type, row, infor) {
					 	    return convert2Money(row.TotalAmount.toString());
					 	},
					 	className: "dt-body-right"
					 }

                ],
            	"footerCallback": function (row, data, start, end, display) {
            	}
            });

    	}
    	function exportExcel() {
    	    window.open("/Report/ExportExcelDoanhThu");
    	    //$.ajax({
    	    //    url: "/Report/ExportExcelDoanhThu", success: function (result) {    	            
    	    //    }
    	    //});
    	}
    	$("#btnSearch")
            .click(function () {
            	searchGrid();
            });

    	$("#btnRefresh")
        .click(function () {
            rsdata();
            searchGrid();
        });

    	$("#btnExport")
            .click(function () {
                exportExcel();
            });
    	$("#keyword").keypress(function (e) {
    		if (e.which == 13) {
    			searchGrid();
    		}
    	});
    	
     
    });



function rsdata() {
    if (!daysView) daysView = -30;
    Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView))); 
    Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
}