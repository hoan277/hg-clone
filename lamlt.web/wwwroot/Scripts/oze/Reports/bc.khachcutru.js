﻿
var $table;

$(document).ready(function () {
    reportDate();
    setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);
    function rsdata() {
        if (!daysView) daysView = -30;
        Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView))); 
        Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));

        $("#searchCustomerName").val("");
        $("#searchPhone").val("");

        $("#searchTax_Passpor").val("");
        $("#searchEmail").val("");

        $("#RoomType").val("0");
        $("#Room").val("0");

        $("#isCountry").val("-1");
    }


    toggleMenu("menu_baocao");
    
  

    $("#btnSearch")
        .click(function () {
            $table.ajax.reload();
        });

    $("#isCountry")
        .change(function () {
            $table.ajax.reload();
        });

    showDialogLoading();
    $table = $('#example').DataTable({
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "processing": true,
        "serverSide": true,
        "initComplete": function (settings, json) {
            hideDialogLoading();
            //alert( 'DataTables has finished its initialisation.' );
            //okoko
        },
        /*bFilter: false, bInfo: false,*/
        "dom": '<"top">rt<"bottom" lpi><"clear">',
        "ajax": {
            "url": "/Report/ListKhachCuTru",
            "data": function (d) {
                d.columns = "";
                d.search = $("#txtSearch").val();

                d.Fdate = $("[name='FromDate']").val();
                d.Tdate = $("[name='ToDate']").val();

                d.Name = $("#formSearch :input[name='searchCustomerName']").val();
                d.Email = $("#formSearch :input[name='searchEmail']").val();
                d.Phone = $("#formSearch :input[name='searchPhone']").val();
                d.Tax_Passpor = $("#formSearch :input[name='searchTax_Passpor']").val();
                d.CheckDate = $('input[name=checkDate]:checked', '#formSearch').val();
                d.RooTypeID = $("#formSearch :input[name='RoomType']").val();
                d.RoomID = $("#formSearch :input[name='Room']").val();
                d.isCountry = $("#formSearch [name='isCountry']").val();
            }
        },
        "columns":
            [{
                "data": null,
                render: function (data, type, row, infor) {
                    return $table.page.info().page + infor.row + 1;
                }
            },
            {
               "data": "CustomerName",
               //"orderable": "false"
            },
           {
               "data": "IdentifyNumber",
                //"orderable": "false" 
           },
            {
                "data": "RoomTypeName",
                //"orderable": "false" 
            },
            {
                "data": "RoomName",
                //"orderable": "false" 
            },
            {
                "data": "TTTV",
                render: function (data, type, row, infor) {
                    //console.log(row);
                    return row.TTTV === 1 ? "Có" : "Không";
                }
            },
           {
                "data": null,
                render: function (data, type, row, infor) {
                    return row.Arrive_Date === null ? "" : moment(new Date(parseInt(row.Arrive_Date.slice(6, -2)))).format("DD-MM-YYYY");
                }
            },
            {
                "data": null, 
                render: function (data, type, row, infor) {
                    return row.Leave_Date === null ? "" : moment(new Date(parseInt(row.Leave_Date.slice(6, -2)))).format("DD-MM-YYYY");
                }
            },
            {
                "data": null,
                render: function (data, type, row, infor) {
                    return row.Sex === 1 ? "Nam" : "Nữ";
                }
            }
            ]
    });

    
    $("#btnExport")
        .click(function () {
            exportExcel();
        });

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            $table.ajax.reload();
        }
    });

    $("#btnRefresh")
         .click(function () {
             rsdata();

             $table.ajax.reload();
         });
});
function exportExcel() {
    //$.get("/Report/ExportExcelCustomerArrive");
    window.open("/Report/ExportExcelCustomerArrive");
}