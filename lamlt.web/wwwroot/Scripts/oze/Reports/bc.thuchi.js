﻿
var $table;

$(document).ready(function () {

    reportDate();
    setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

    function rsdata() {
        if (!daysView) daysView = -30;
        Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView))); 
        Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
        //$("#ddlRoomTypeID").val(0);
        //$("#ddlRoomID").val(0);
    }

    $("#btnSearch")
        .click(function () {
            searchGrid();
        });
    $("#btnRefresh")
       .click(function () {
           rsdata();
           searchGrid();
       });
    $("#btnExport")
            .click(function () {
                exportExcel();
            });
    //$("#isCountry")
    //    .change(function () {
    //        searchGrid();
    //    });

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
        }
    });
    searchGrid();

});

//thông tin  order 
function viewOrderInfo(orderId, checkinId) {
    $("#modalDetails").off('show.bs.modal').on('show.bs.modal', function () {
        $("#TittleBox").html("Thông tin hóa đơn");
        $("#modalDetails .modal-body-content").html('<p>loading..</p>');
        Sv.AjaxPost({
            url: "/CustomerCheckOut/ViewSaleOrder",
            data: { orderId: orderId, checkinId: checkinId }
        }, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            Sv.SetupNumberMask([{ e: $(".amount-mask") }]);
            Sv.SetScrollHeight($(".box-item"));
            Sv.SetupInputMask();
        });
    }).modal("show");
}
//thông tin  order 
function viewExpenseInfo(id) {
    $("#modalDetails").off('show.bs.modal').on('show.bs.modal', function () {
        $("#TittleBox").html("Thông tin phiếu chi");
        $("#modalDetails .modal-body-content").html('<p>loading..</p>');
        Sv.AjaxPost({
            url: "/Expense/ViewExpenseOrder",
            data: { id: id }
        }, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            Sv.SetupNumberMask([{ e: $(".amount-mask") }]); 
            Sv.SetupInputMask();
        });
    }).modal("show");
}

//thông tin  hóa đơn xuất bán lẻ 
function viewOrderRetailInfo(id) {
    $("#modalDetails").off('show.bs.modal').on('show.bs.modal', function () {
        $("#TittleBox").html("Thông tin xuất bán lẻ");
        $("#modalDetails .modal-body-content").html('<p>loading..</p>');
        Sv.AjaxPost({
            url: "/ExportRetail/ViewOrder",
            data: { id: id }
        }, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            Sv.SetupNumberMask([{ e: $(".amount-mask") }]);
            Sv.SetupInputMask();
        });
    }).modal("show");
}

 
function searchGrid() {
    var data = {model: getSearchModel()};
    Sv.Post({
        url: "/Report/DataBaoCaoThuChi_Grid",
        data: data,
    }, function (html) {
        $("#boxgrid_thuchi").html(html);
        //$('#boxgrid_thuchi #example').DataTable({ "paging": false, });
    }, function (e) {
        console.log(e);
    });
}
function getSearchModel() {
    return {
        FromDate: $("input[name='FromDate']").val(),
        ToDate: $("input[name='ToDate']").val(),
    }
}

function exportExcel() {
    window.open("/Report/ExportBaoCaoThuChi");
}