﻿
var $table;

$(document).ready(function () {

    reportDate();
     
    setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);
    $("#btnRefresh").click(function () {
        rsdata();
        searchGrid();
    });

    $("#btnExport").click(function () {
        exportExcel();
    });

    $("#btnSearch").click(function () {
        searchGrid();
    });
    $(".box-body").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
        }
    });
    //showDialogLoading();    	
    var tableOption = {
        "ajax": {
            "url": "/report/ListTienPhong",
            "data": function (d) {
                d.search = "";
                d.FromDate = $("input[name='FromDate']").val();
                d.ToDate = $("[name='ToDate']").val();
                d.Keyword = $("#keyword").val();
                d.columns = "";
                d.RoomTypeName = $("#ddlRoomTypeID").val();
                d.RoomName = $("#ddlRoomID").val();
                d.KhungGio = $("#dllKhungGio").val();
                d.KhungGioName = $("#dllKhungGio option:selected").text();
            }
        },
        "columns":
            [
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        var pInfo = $table.page.info();
                        return (pInfo.page * pInfo.length) + infor.row + 1;
                    }
                },
                { "data": "ordercode", "orderable": "false" },
                { "data": "Customername", "orderable": "false" },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        if (row.CheckinDate != null) {
                            return Sv.DateToString(row.CheckinDate, "DD/MM/YYYY HH:mm");
                        }
                        return "";
                    }
                },
                 {
                     "data": null,
                     render: function (data, type, row, infor) {
                         if (row.CheckOutDate != null) {
                             return Sv.DateToString(row.CheckOutDate, "DD/MM/YYYY HH:mm");
                         }
                         return "";
                     }
                 },
                 {
                     "data": null,
                     render: function (data, type, row, infor) {
                         return Sv.NumberToString(row.TotalAmount);
                     },
                     className: "dt-body-right"
                 },
                { "data": "RoomLevel", "orderable": "false" },
                { "data": "RoomNo", "orderable": "false" },

            ],
        "initComplete": function (settings, json) {
            //hideDialogLoading();
            var api = this.api();
            $(api.column(4).footer()).html("Tổng: " + Sv.NumberToString(json.totalAmount));
        },
    };
    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));
});

function rsdata() {
    if (!daysView) daysView = -30;
    Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView))); 
    Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
    $("#keyword").val("");
    $("#ddlRoomTypeID").val("");
    $("#ddlRoomID").val("");
    $("#dllKhungGio").val("-1");
}

function searchGrid() {
    $table.ajax.reload(function (data) {
        $($table.column(4).footer()).html("Tổng: " + Sv.NumberToString(data.totalAmount));
    });
}

function exportExcel() {
    window.open("/Report/ExportBaoCaoTienPhong");
}