﻿
var $table;

$(document)
    .ready(function () {

        reportDate();

        setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

        function rsdata() {
            if (!daysView) daysView = -30;
            Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView))); 
            Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
            $("#ddlProduct").val("");
            $("#ddlRoomID").val("");
        }

        $("#btnExport").click(function () {
            exportExcel();
        });
        $("#btnSearch").click(function () {
            searchGrid();
        });
        $("#btnRefresh")
          .click(function () {
              rsdata();
              searchGrid();
          });

        $("#box-body").keypress(function (e) {
            if (e.which == 13) {
                searchGrid();
            }
        });
        showDialogLoading();
        var tableOption = {
            "ajax": {
                "url": "/report/ListTienDichVu",
                "data": function (d) {
                    d.search = "";
                    d.columns = "";
                    d.FromDate = $("input[name='FromDate']").val();
                    d.ToDate = $("input[name='ToDate']").val();
                    d.RoomId = $("select[name='ddlRoomID']").val();
                    d.RoomName = $("select[name='ddlRoomID'] option:selected").text();
                    d.ProductId = $("select[name='ddlProduct']").val();
                    d.ProductName = $("select[name='ddlProduct'] option:selected").text();
                }
            },
            "columns":
            [
                {
                    "data": null, render: function (data, type, row, infor) {
                        return $table.page.info().page + infor.row + 1;
                    }
                },
                { "data": "RoomName", "orderable": "false" },
                { "data": "ProductName", "orderable": "false" },
                {
                    "data": null, render: function (data, type, row, infor) {
                        return Sv.DateToString(row.DatePayment, "DD/MM/YYYY HH:mm")
                    },
                },
                { "data": "quantity", "orderable": "false", className: "dt-body-right" },
                {
                    "data": null,
                    render: function (data, type, row, infor) { return Sv.NumberToString(row.TotalAmount) },
                    className: "dt-body-right"
                },

            ],
            "initComplete": function (settings, json) {
                hideDialogLoading();
                var api = this.api();
                setTableFooter(api, json.totalQuantity, json.totalAmount);
            },
        };
        $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));

    });

function setTableFooter($t, v1, v2) {
    $($t.column(1).footer()).html("");
    $($t.column(3).footer()).html("Tổng số lượng: " + convert2Money(v1.toString()));
    $($t.column(5).footer()).html("Tổng tiền: " + convert2Money(v2.toString()));
}

function searchGrid() {
    $table.ajax.reload(function (data) {
        setTableFooter($table, data.totalQuantity, data.totalAmount);
    });
}
function exportExcel() {
    window.open("/Report/ExportExcelTienDichVu");
}