﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}

function getFormData($form) {
    var unindexed_array = $form.serialize();
    return unindexed_array;
}
function editDialog(id) {

    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        $("#myModal .modal-body-content").load("/groupfunction/Edit/" + id);
        $("#myModal button#btnSave").css("display", "inline");
        $("#btnSave").off("click");
        $("#btnSave").click(function () {
            if (!$("#dummySupplier").valid()) return;
            if ($('#groupid').val() == "0") {
                $('.lblerror').text("Bạn phải chọn nhóm trước.");
                $('#groupid').focus();
            }
            else {
                var pdata = getFormData($("#dummySupplier"));
                showDialogLoading();
                $.post((id < 1 ? "/groupfunction/update" : "/groupfunction/updateone"), pdata, function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); $('body').addClass('no-padding'); });
                    }
                    else {
                        alert("Có lỗi khi lưu quyền");
                    }
                });
            }
        });
    });
    $("#myModal").modal("show");
}
function deleteDialog(id) {
    bootbox.confirm("Bạn có chắc muốn xóa quyền này không ?", function (result) {
        if (result) {
            showDialogLoading();
            $.post("/groupfunction/Delete", { id: id }, function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    bootbox.alert("Xóa quyền thành công.", function () {
                        $('body').addClass('no-padding'); 
                        searchGrid();
                    });
                }
                else {
                    alert("Xóa quyền thất bại!");
                }
            });
        }
        else{
            $('body').addClass('no-padding'); 
        }
    });
}

function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/groupfunction/GetDetail/" + id);
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}
