﻿$(function () {
    $('#btnImport').click(function () {
        var fileExtension = ['xls', 'xlsx'];
        var filename = $('#file_excel').val();
        if (filename.length == 0) {
            alert("Vui lòng chọn file tải lên.");
            return false;
        }
        else {
            var extension = filename.replace(/^.*\./, '');
            if ($.inArray(extension, fileExtension) == -1) {
                alert("Vui lòng chọn file có định dạng .xls, xlsx");
                return false;
            }
        }
        var fd = new FormData();
        var fileUpload = $("#file_excel").get(0);
        var files = fileUpload.files;
        fd.append(files[0].name, files[0]);
        $.ajax({
            url: "/film_render/import_excel",
            method: "POST",
            data: fd,
            processData: false,
            contentType: false,
            success: function (data) {
                alert("Thêm mới thành công");
                $("#btnSearch").trigger('click');
            },
            error: function (xhr) { console.log(xhr.responseText); },
        });
    });
});

function film_render() {
    var self = this;
    this.tableName = "film_render";
    this.idModal = "modalfilm_render";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_render/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
				/*
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data)
					{
						hideDialogLoading();
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});*/
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_render').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
                    delete d.columns;
                    d.search = $("#search_film_render_title").val();
                }
            },
            "columns":
                [{
                    "data": null, render: function (data, type, row) {
                        var htmlMenu = "";
                        var info = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                            '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                            '</a>  ';
                        var edit = '<a href="javascript:void(0)" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                            '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                            '</a>  ';
                        var delete1 = '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                            '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                            '</a>  ';
                        htmlMenu = info + edit + delete1;
                        if (row.status != 1) {
                            htmlMenu = info + edit + delete1;
                        }
                        return htmlMenu;
                    }
                }
                    , { 'data': 'id' }
                    , { 'data': 'title' }
                    , { 'data': 'list_file' }
                    , { 'data': 'effect' }
                    , { 'data': 'thumb' }
                    , { 'data': 'channel_id_title' }
                    , { 'data': 'yt_title' }
                    , { 'data': 'yt_desc' }
                    , { 'data': 'yt_mode' }
                    , { 'data': 'yt_schedule' }
                    , { 'data': 'status' }
                    , { 'data': 'date_created' }
                    , { 'data': 'date_update' },

                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}