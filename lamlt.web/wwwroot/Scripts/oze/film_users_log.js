﻿function check_nha_mang(mobile) {
    if (/((086|096|097|098|032|033|034|035|036|037|038|039)+([0-9]{7})\b)/g.test(mobile)) {
        return "Viettel";
    } else if (/((091|094|081|082|083|084|085|088)+([0-9]{7})\b)/g.test(mobile)) {
        return "Vinaphone";
    } else if (/((089)+([0-9]{7})\b)/g.test(mobile)) {
        return "Mobifone";
    } else if (/((099|059)+([0-9]{7})\b)/g.test(mobile)) {
        return "Gmobile";
    } else if (/((092|056|058)+([0-9]{7})\b)/g.test(mobile)) {
        return "Vietnam Mobile";
    } else {
        return "Unknown";
    }
}

function film_users_log() {
    var self = this;
    this.tableName = "film_users_log";
    this.idModal = "modalfilm_users_log";
    this.$table;
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_users_log').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_users_log_title").val();
                    //d.cpid = $("#search_film_users_log_sub_type").val();
                    //d.status = $("#search_film_users_log_sub_type").val();
                    //d.catalogid = $("#search_film_users_log_catalogid").val();
                    d.sub_type = $("#search_film_users_log_sub_type").val();
                    d.statusUserLog = $("#search_film_users_log_status").val();
                    //d.sub_state = $("#search_film_users_log_sub_state").val();
                    d.start_time = $("#start_time").val();
                    d.end_time = $("#end_time").val();
                    console.log(d.start_time);
                    console.log(d.end_time);
                    console.log(d.sub_type);
                    console.log(d.status);
                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'username' }
                    , { 'data': 'email' }
                    , { 'data': 'phone' }
                    //, {
                    //    "data": null, render: function (data, type, row) {
                    //        if (row.package_id == 1) return "Gói ngày";
                    //        if (row.package_id == 2) returan "Gói tuần";
                    //        if (row.package_id == 5) return "Gói tháng";
                    //        if (row.package_id == 11) return "Gói năm";
                    //        return "Unknown";
                    //    }
                    //}
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.sub_type == 1) return "Gói ngày";
                            if (row.sub_type == 2) return "Gói tuần";
                            if (row.sub_type == 3 || row.sub_type == 5) return "Gói tháng";
                            if (row.sub_type == 11) return "Gói năm";
                            return "Unknown";
                        }
                    }
                    , { 'data': 'datecreated' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.status == 1) return "Đăng ký";
                            if (row.status == 2) return "Gia hạn";
                            if (row.status == 3) return "Hết hạn";
                            if (row.status == 4) return "Hủy";
                            return "Unknown";
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            return check_nha_mang(row.username);
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}