﻿var ModalDeposit = function () {
    var _this = this;
    this.$divView = $("#div_Deposit");
    this.Data = [];

    this.save = function (modalId) {
        var $p = $("#" + modalId);
        _this.Data.push({
            Amount: $p.find("[name=amount]").val(),
            Note: $p.find("[name=note]").val(),
            Date: new Date()
        });
        _this.hideModal(modalId);
        _this.UpdateView(_this.Data, modalId);
    }

    this.remove = function (index, modalId) {
        _this.Data.splice(index, 1);
        _this.UpdateView(_this.Data, modalId);
    }

    _this.UpdateView = function (data, modalId) {
        var strHtml = "";
        var total = 0;
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                strHtml += '<div class="form-group row">';
                strHtml += '    <label class="col-xs-2 control-label">' + data[i].Amount + '</label> ';
                strHtml += '    <label class="col-xs-4 control-label">' + moment(data[i].Date).format("DD/MM/YYYY HH:mm") + '</label>  ';
                strHtml += '    <label class="col-xs-5 control-label">' + data[i].Note + '</label>  ';
                strHtml += '    <div class="col-xs-1" style="margin-top:10px"> ';
                strHtml += '        <a href="#"  class="rmv_deposit' + modalId + '" data-index="' + i + '" ><i class="fa fa-remove"></i></a>  ';
                strHtml += '    </div> ';
                strHtml += '</div>';
                total += parseFloat(data[i].Amount);
            }
        }
        _this.$divView.html(strHtml);
        _this.$divView.find(".rmv_deposit" + modalId).off("click").on("click", function () {
            var index = $(this).data("index");
            _this.remove(index, modalId);
        });

        // Tong tien
        $("[name=Deposit]").val(total);

        // tinh tien
        tinhtien();
    }

    this.initModal = function (modalId) {
        var html = '<div id="' + modalId + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-md">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title btn-header">Nhập tiền trả trước</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '           <div class="modal-body-content">';
        html += '               <div class="row form-group">';
        html += '                   <label class="col-xs-4 control-label">Nhập số tiền </label>';
        html += '                   <div class="col-xs-8">';
        html += '                       <input type="text" class="form-control number-mask" name="amount" value="0" />';
        html += '                   </div>';
        html += '                </div>';
        html += '               <div class="row form-group">';
        html += '                   <label class="col-xs-4 control-label">Ghi chú </label>';
        html += '                   <div class="col-xs-8">';
        html += '                       <input type="text" class="form-control" name="note" value="" />';
        html += '                   </div>';
        html += '                </div>';
        html += '           </div>';
        html += '        </div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + modalId + '" class="btn btn-default" ><span class="fa fa-check">Lưu</button> ';
        html += '            <button type="button" class="btn btn btn-df" data-dismiss="modal"> <i class="fa fa-reply"></i> Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);

        var $p = $("body").find("#" + modalId);
        Sv.SetupInputMask();

        $p.find("#btnSave" + modalId).off("click").on("click", function () {
            _this.save(modalId);
        });

    }

    this.show = function (modalId) {
        if (!document.getElementById(modalId)) { _this.initModal(modalId); }
        var $p = $("#" + modalId);
        $p.find("[name=amount]").val("");
        $p.find("[name=note]").val("");
        $p.modal("show");


    }

    this.hideModal = function (modalId) {
        $('#' + modalId).modal('hide');
    }
}

function getInfo() {
    var information = {
        khunggio: $("#modalDetails #dllKhungGio").val(),

        Deposit: $("#modalDetails #Deposit").val(),
        Deduction: $("#modalDetails #Deduction").val(), //Giảm trừ

        Name: $("#modalDetails #NameCheckout").val(),
        Phone: $("#modalDetails #PhoneCheckout").val(),
        Identity: $("#modalDetails #IdentifyNumberCheckout").val(),
        Email: $("#modalDetails #EmailCheckout").val(),

        Adult: $("#modalDetails #Number_People").val(),
        Children: $("#modalDetails #Number_Children").val(),

        Discount: $("#modalDetails #Discount").val(),
        DiscountType: $("#modalDetails #dllDiscountType").val(),

        UpPrice: $("#modalDetails #upPrice").length > 0 ? $("#modalDetails #upPrice").val() : 0,
        UpPriceType: $("#modalDetails #dllUpPrice").length > 0 ? $("#modalDetails #dllUpPrice").val() : 0,

        Description: $("#modalDetails #Description").val(),

        Payment_Type_ID: $("#dllPaymentType").val(), // phuong thuc thanh toan
    };
    return information;
}

function tinhtien() {
    var orderId = parseInt($("[name=OrderID]").val());
    var temp = getInfo();
    console.log(orderId, temp);
    Sv.Post({
        url: "/CustomerCheckOut/CheckOutOderUpdate",
        data: { orderId: orderId, temp: temp }
    }, function (rs) {
        if (rs.Status > 0) {
            $("[name=txtTotalPrice]").val(rs.Data.TotalPrice);
            $("[name=txtDeposit]").val(rs.Data.Deposit);
            $("[name=txtDeduction]").val(rs.Data.Deduction);
            $("[name=txtTotalPay]").val(rs.Data.TotalPay);
            $("#div_txtTotalPrice").html(rs.Data.totalPriceString);
            $("#div_txtTotalPay").html(rs.Data.totalString);
            if (rs.Data.html.length > 0) {
                $("#PriceEstimate").html(rs.Data.html);
            }
            Sv.SetupInputMask();
        }
    });
}

$(document).ready(function () {

    // NGÀY THÁNG
    //Sv.SetUpDatePiker([{ e: $(".date-picker") }]);

    // SỐ TIỀN HIỆN THỊ 000.000
    Sv.SetupInputMask();

    ////// CHANGE TRẢ TRƯỚC - tính tiền
    //$("#AddDeposit").parent().click(function () {
    //    (new ModalDeposit("xxxModalxxx")).show();
    //});
    //// CHANGE TRẢ TRƯỚC - tính tiền
    $("#modalDetails [name=Deposit]").off("change").change(function () { tinhtien() });

    // CHANGE Giảm trừ  - tính tiền
    $("#modalDetails [name=Deduction]").off("change").change(function () { tinhtien() });

    // CHANGE Giảm giá  - tính tiền
    $("#modalDetails [name=Discount]").off("change").change(function () { tinhtien() });

    // CHANGE Giảm giá theo tiền - theo %
    $("#modalDetails #dllDiscountType").off("change").change(function () {
        var v = $("#modalDetails #dllDiscountType").val();
        if (v == "0") {
            $("#modalDetails [name=Discount]").val(0);
            Sv.SetupNumberMask([{ e: $("#modalDetails [name=Discount]") }]);
        } else {
            $("#modalDetails [name=Discount]").val(0);
            Sv.SetupNumberMask([{ e: $("#modalDetails [name=Discount]"), alias: "percentage" }]);
        }
        tinhtien();
    });

    // CHANGE Tăng giá  - tính tiền
    $("#modalDetails #upPrice").off("change").change(function () { tinhtien() });

    // CHANGE Tăng giá  - tính tiền   
    $("#modalDetails #dllUpPrice").off("change").change(function () {
        var v = $("#modalDetails #dllUpPrice").val();
        if (v == "0") {
            $("#modalDetails #upPrice").val(0);
            Sv.SetupNumberMask([{ e: $("#modalDetails #upPrice") }]);
        } else {
            $("#modalDetails #upPrice").val(0);
            Sv.SetupNumberMask([{ e: $("#modalDetails #upPrice"), alias: "percentage" }]);
        }
        tinhtien();
    });

    // SUBMIT edit hóa đơn
    $("#btnUpdateDetail").off("click").on("click", function () {
        var orderId = parseInt($("[name=OrderID]").val());
        var temp = getInfo();
        Sv.Post({
            url: "/CustomerCheckOut/UpdateOrder",
            data: { orderId: orderId, temp: temp }
        }, function (rs) {
            if (rs.Status === "1") {
                bootboxLamlt.alert(rs.Message, function () {
                    $("#modalDetails").modal("hide");
                });
            }
            else {
                bootboxLamlt.alert(rs.Message);
            }

        });
    });

    // set lai width box tien phong, tien dich vu
    Sv.SetScrollHeight($(".box-item"));

});



function getModelPrint() {
    var checkinID = parseInt($("#modalDetails [name=CheckInID]").val());
    var IsboolService = $('#modalDetails #ckService').is(":checked");
    var IsboolRoom = $('#modalDetails  #ckRoom').is(":checked");
    var Leave_DateCheckOut = $("#modalDetails #Arrive_DatePlan").val();
    var khunggio = $("#modalDetails #dllKhungGio").val();

    var information = {
        khunggio: khunggio,

        Deposit: $("#modalDetails #Deposit").val(),
        Deduction: $("#modalDetails #Deduction").val(), //Giảm trừ

        Name: $("#modalDetails #NameCheckout").val(),
        Phone: $("#modalDetails #PhoneCheckout").val(),
        Identity: $("#modalDetails #IdentifyNumberCheckout").val(),
        Email: $("#modalDetails #EmailCheckout").val(),

        Adult: $("#modalDetails #Number_People").val(),
        Children: $("#modalDetails #Number_Children").val(),

        Discount: $("#modalDetails #Discount").val(),
        DiscountType: $("#modalDetails #dllDiscountType").val(),

        UpPrice: $("#modalDetails #upPrice").length > 0 ? $("#modalDetails #upPrice").val() : 0,
        UpPriceType: $("#modalDetails #dllUpPrice").length > 0 ? $("#modalDetails #dllUpPrice").val() : 0,

        Description: $("#modalDetails #Description").val(),
    };

    return {
        checkinID: checkinID,
        IsboolService: IsboolService,
        IsboolRoom: IsboolRoom,
        tDate: Leave_DateCheckOut,
        khunggio: khunggio,
        temp: information,
        
    }
     

}

// Tdate, int roomId, int checkinID, int khunggio, bool IsboolRoom, bool IsboolService, CheckoutTemp temp
function getModelPrint2() {
    var obj = getModelPrint();
    obj.roomId = -1;
    obj.Tdate = obj.tDate;
    return obj;
}

function printShow() {
    $("#modalPrint").off('show.bs.modal');
    $("#modalPrint").on('show.bs.modal', function () {
        $("#modalPrint .modal-body-content").html('<p>loading..</p>');
        //console.log(getModelPrint());
        showDialogLoading();
        var m = getModelPrint();
        isDoan(m.checkinID, function (rs) {
            if (rs > 0) {
                //$.post("/CheckInGroup/ViewPrintPay", getModelPrint2(), function (rs) {
                $.post("/CustomerCheckOut/ViewPrintPay", getModelPrint2(), function (rs) {
                    hideDialogLoading();
                    $("#modalPrint .modal-body-content").html(rs);
                    // event change
                    changePrintOption();
                    //
                    printCtr.load("", getPrintData());
                });
            } else {
                $.post("/CustomerCheckOut/ViewPrintPay", m, function (rs) {
                    hideDialogLoading();
                    $("#modalPrint .modal-body-content").html(rs);
                    // event change
                    changePrintOption();
                    //
                    printCtr.load("", getPrintData());
                });
            }
        }); 
    });
    $("#modalPrint").modal("show");
}

function orderSendMail() {
    var m = getModelPrint();
    if (!m.temp.Email) {
        alert("Không có thông tin mail của khách hàng cần gửi.");
        return;
    }
    Sv.Post({
        url: "/RoomCheckOut/SendMail",
        data: {}
    }, function (rs) {
        console.log(rs);
        bootboxLamlt.alert(rs.Message);
        $("body").addClass("modal-open1");
    });
}

function orderExport() {
    window.open('/RoomCheckOut/Export', '_blank');
    var win = window.open("", '_blank');
    if (win)
        win.focus();
}

function orderPrint() {
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write('<html>');
    mywindow.document.write('<head>');
    mywindow.document.write('<link rel="stylesheet" href="/Content/bootstrap.min.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/Content/hungpvCustom.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/Content/hungpvprint.css" type="text/css" />');
    mywindow.document.write('</head>');
    mywindow.document.write('<body class="container-fluid">');
    mywindow.document.write('<button class="button-print btn btn-primary noprint" onclick="window.print();"><i class="glyphicon glyphicon-print"></i></button>');
    mywindow.document.write('<div class="row">');

    var html = $("#modalPrint .modal-body-content").html();
    mywindow.document.write(html);

    mywindow.document.write("</div>");
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.focus();
}

function orderPrint2() {
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write('<html class="print2">');
    mywindow.document.write('<head>');
    mywindow.document.write('<link rel="stylesheet" href="/Content/bootstrap.min.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/Content/hungpvCustom.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/Content/hungpvprint.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/Content/billPrint.css" type="text/css" />');
    mywindow.document.write('</head>');
    mywindow.document.write('<body class="container-fluid">');
    mywindow.document.write('<button class="button-print btn btn-primary noprint" onclick="window.print();"><i class="glyphicon glyphicon-print"></i></button>');
    mywindow.document.write('<div class="row">');

    var html = $("#modalPrint .modal-body-content").html();
    mywindow.document.write(html);

    mywindow.document.write("</div>");
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.focus();
}
function changePrintOption() {
    $("#printType").off("change").on('change', function () {
        var v = $(this).val();
        var data = getPrintData();
        printCtr.load(v, data);
    });
}

function isDoan(id, callback) {
    Sv.Post({
        url: "/Common/CheckInsIsDoan",
        data: { checkinid: id }
    }, function (rs) {
        callback(rs);
    });
}
 