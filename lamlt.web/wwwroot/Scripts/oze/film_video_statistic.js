﻿function film_video_statistic() {
    var self = this;
    this.tableName = "film_video_statistic";
    this.idModal = "modalfilm_video_statistic";
    this.$table;
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_video_statistic').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_video_statistic_title").val();
                    d.cpid = $("#search_film_video_statistic_cpid").val();
                    d.status = $("#search_film_video_statistic_status").val();
                    d.catalogid = $("#search_film_video_statistic_catalogid").val();
                    d.start_time = $("#start_time").val();
                    d.end_time = $("#end_time").val();
                    console.log(d.start_time);
                    console.log(d.end_time);
                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'title' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.desc == null) return "";
                            if (row.desc.length <= 50) return row.desc;
                            return row.desc.substr(0, 49) + "...";
                        }
                    }
                    , { 'data': 'publish_year' }
                    , { 'data': 'publish_countryid_title' }
                    , { 'data': 'catalog_id_title' }
                    , { 'data': 'film_type' }
                    , { 'data': 'exclusive' }
                    , { 'data': 'price' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.status == 0) return "Nháp";
                            if (row.status == 1) return "Chờ duyệt";
                            if (row.status == 2) return "Đã duyệt";
                            if (row.status == 3) return "Từ chối duyệt";
                            if (row.status == 4) return "Đã cập nhật ảnh";
                            if (row.status == 5) return "Đang convert";
                            if (row.status == 6) return "Cập nhật ảnh lỗi";
                            return "Unknown";
                        }
                    }
                    , { 'data': 'cpid_title' }
                    , { 'data': 'code' }
                    , { 'data': 'tags' }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }

}