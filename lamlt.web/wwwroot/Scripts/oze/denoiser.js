﻿$(function () {
    $('#txtResult').hide();
})
let step1 = '', step2 = '', step3 = '';
var file_media = '';
var proId = '';
let selected_step = '';
$('#div_basic').hide();
$('#d_step1').on('change', function () {
    step1 = $(this).val();
    if (step1 == "basic") {
        selected_step = 'step1';
        $('#div_basic').show();
        $('#lblStep').html('Thiết lập cho Basic bước 1');
    } else {
        $('#div_basic').hide();
    }
    buildobj();
});
$('#d_step2').on('change', function () {
    step2 = $(this).val();
    if (step2 == "basic") {
        selected_step = 'step2';
        $('#div_basic').show();
        $('#lblStep').html('Thiết lập cho Basic bước 2');
    } else {
        $('#div_basic').hide();
    }
    buildobj();
});
$('#d_step3').on('change', function () {
    step3 = $(this).val();
    if (step3 == "basic") {
        selected_step = 'step3';
        $('#div_basic').show();
        $('#lblStep').html('Thiết lập cho Basic bước 3');
    } else {
        $('#div_basic').hide();
    }
    buildobj();
});
$('input[name=basic]').on('change', function () {
    switch (selected_step) {
        case 'step1':
            step1 = 'basic_' + $(this).val();
            break;
        case 'step2':
            step2 = 'basic_' + $(this).val();
            break;
        case 'step3':
            step3 = 'basic_' + $(this).val();
            break;
        default:
            step1 = 'basic_ReducedPower';
            step2 = 'basic_ReducedPower';
            step3 = 'basic_ReducedPower';
            break;
    }
    buildobj();
});
function buildobj() {
    step1 = step1 == "basic" ? 'basic_ReducedPower' : step1;
    step2 = step2 == "basic" ? 'basic_ReducedPower' : step2;
    step3 = step3 == "basic" ? 'basic_ReducedPower' : step3;
    $('label[for=d_step1]').html('Bước 1 (' + step1 + ')');
    $('label[for=d_step2]').html('Bước 2 (' + step2 + ')');
    $('label[for=d_step3]').html('Bước 3 (' + step3 + ')');
    let rs_html = '';
    rs_html += '{';
    rs_html += '\n\t"file": "' + file_media + '",';
    rs_html += '\n\t"video_type": "",';
    rs_html += '\n\t"step1": "' + step1 + '",';
    rs_html += '\n\t"step2": "' + step2 + '",';
    rs_html += '\n\t"step3": "' + step3 + '"';
    rs_html += '\n}';
    $('#txtResult').html(rs_html);
}

function denoiser() {
    var self = this;
    this.tableName = "denoiser";
    this.idModal = "modaldenoiser";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                $.get("/denoiser/get?title=" + $('#title').val(), function (data) {
                    if (data.length > 0) {
                        alert("Dữ liệu tên đã tồn tại vui lòng sử dụng tên khác");
                    } else {
                        var pdata = getFormData($("#form" + self.tableName));
                        showDialogLoading();

                        $.post("/denoiser/update", { obj: pdata }, function (data) {
                            hideDialogLoading();
                            //closeDlgLoadingData();
                            if (data.result > 0) {
                                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                            }
                            else {
                                alert("Có lỗi khi tạo :" + data.mess);
                            }
                        });
                    }
                })
                
                /*
                $.ajax({
                    type: 'POST',
                    url: "/"+ self.tableName +"/update",
                    data: JSON.stringify (pdata),
                    success: function(data) 
                    { 
                        hideDialogLoading();						
                        if (data.result > 0)
                        {
                            bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                        }
                        else {
                            alert("Có lỗi khi tạo :"+data.mess);
                        }
                    },
                    contentType: "application/json",
                    dataType: 'json'
                });*/
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.denoiserDialog = function denoiserDialog(id) {
        $('[href="#tab_2"]').tab('show');
        window.scrollTo({ top: 0, behavior: 'smooth' });
        $('#mgs').html("Vui lòng cấu hình các bước");
        proId = id;
        $('tr td').removeClass('active');
        $('tr#' + id + " td:nth-child(1)").addClass('active');
        $.ajax({
            url: "/denoiser/get?id=" + id,
            method: "GET",
            success: function (data) {
                file_media = data[0].mp4;
                buildobj();
            },
            error: function (xhr) { console.log(xhr.responseText); },
        });
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_denoiser').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "rowId": "id",
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
                    delete d.columns;
                    d.search = $("#search_denoiser_title").val();
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                           
                            var htmlMenu = "";
                            var info = '<a style="color: #03A9F4;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#03A9F4" title="Xem"></span> Xem' +
                                '</a>  ';
                            var edit = '<a href="javascript:void(0)" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span> Sửa' +
                                '</a>  ';
                            var delete1 = '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span> Xóa' +
                                '</a>';
                            var denoiser = '<br /><a style="color: #337ab7;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.denoiserDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-flash" aria-hidden="true" style="color:#673ab7" title="Denoiser"></span>Thiết lập lọc nhiễu' +
                                '</a>  ';
                            htmlMenu = info + edit + delete1 + denoiser;
                            if (row.status != 1) {
                                htmlMenu = info + edit + delete1 + denoiser;
                            }
                            return htmlMenu;
                        }
                    }
                    //, {
                    //    'data': null, render: function (data, type, row) {
                    //        var check_box = '<div class="checkbox"><label><input class="export_selected" type="checkbox" id="export_selected" name="export_selected" type="checkbox">' + row.id + '</label></div >';
                    //        var html1 = check_box;
                    //        return row.id ;
                    //    }
                    //}
                    , { 'data': 'id' }
                    , { 'data': 'title' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.mp4 == null) return "";
                            return '<a target="_blank" href="https://files.lalatv.com.vn:9090/' + row.mp4 + '">Xem</a>';
                        }
                    }
                    , { 'data': 'step' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.result == null) return "";
                            return '<a target="_blank" href="https://files.lalatv.com.vn:9090/' + row.result + '">Xem</a>';
                        }
                    }
                    //, { 'data': 'updated' }
                    , { 'data': 'status' },
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}