﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}
function editDialog(id) {

    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        $("#myModal .modal-body-content").load("/grouptype/Edit/" + id);
        $("#myModal button#btnSave").css("display", "inline");
        $("#btnSave").off("click");
        $("#btnSave").click(function () {
            if (!$("#dummySupplier").valid()) return;
            var r = validateCode($('#code').val());
            if (!r) {
                bootbox.alert("Mã nhóm không được chứa ký tự đặc biệt");
            }
            else {
                var pdata = getFormData($("#dummySupplier"));
                showDialogLoading();
                $.post("/grouptype/update", { obj: pdata }, function (data) {
                    hideDialogLoading();

                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo nhóm quyền");
                    }
                });
            }

        });
    });
    $("#myModal").modal("show");
}
function deleteDialog(id) {
    bootbox.confirm("Bạn có chắc muốn xóa nhóm này không ?", function (result) {
        if (result) {
            showDialogLoading();
            $.post("/grouptype/Delete", { id: id }, function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    bootbox.alert("Xóa nhóm thành công.", function () {
                        searchGrid();
                    });
                }
                else {
                    alert("Xóa nhóm thất bại!");
                }
            });
        }
    });
}

function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/grouptype/GetDetail/" + id);
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}
function validateCode(input) {
    var r = /^[a-zA-Z0-9\'\-\. ]+$/;
    return r.test(input);
}