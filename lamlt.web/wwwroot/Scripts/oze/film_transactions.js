﻿//@{
//    var total1 = ViewData["total"];
//}

function film_transactions() {
    var self = this;
    this.tableName = "film_transactions";
    this.idModal = "modalfilm_transactions";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_transactions/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.quatity = function (id) {
        $.post('/film_transactions/quatity', {
            id: id
        }, function (data) {

        });
    }
    this.setupGrid = function () {
        //self.$table.on('xhr', function () {
        //    var json = self.$table.ajax.json();
        //    alert(json.sum + ' is sum that need to be shown for me');
        //});

        showDialogLoading();
        self.$table = $('#example_film_transactions').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    d.search = String($("#s_title").val());
                    d.time_range = String($('#s_timerange').val());
                    d.arr_status = String($('#s_status').val());
                    d.arr_typeName = String($('#s_typeName').val());
                    delete d.columns;
                },
            },
            "footerCallback": function (row, data, start, end, display, total) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                //self.$table.on('xhr', function () {
                //    var json = self.$table.ajax.json();
                //    alert(json.sum + ' is sum that need to be shown for me');
                //});
                // Total over all pages
                total = api
                    .column(5)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                // Total over this page
                pageTotal = api
                    .column(5, { page: 'active' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                // Update footer
                $(api.column(5).footer()).html(pageTotal + ' (total ' + self.$table.ajax.json().sum + ')'
                );
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'user' }
                    , { 'data': 'user_phone' }
                    , { 'data': 'txtid' }
                    , {
                        'data': null, render: function (data, type, row) {
                            var typeName = "";
                            if (row.typeName == 1) {
                                typeName = "SMS";
                            }
                            else if (row.typeName == 2 || row.typeName == "bank") {
                                typeName = "Ngân hàng";
                            }
                            else if (row.typeName == 3) {
                                typeName = "Wap";
                            }
                            else if (row.typeName == 0) {
                                typeName = "Không xác định";
                            }
                            else {
                                typeName = row.typeName;
                            }
                            return typeName;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var datecreated = "";
                            if (row.datecreated != null) {
                                datecreated = moment(row.datecreated).format('DD/MM/YYYY HH:mm:ss');
                            }
                            return datecreated;
                        }
                    }
                    , {
                        'data': null, render: function (data, type, row) {
                            switch (row.statusBanking) {
                                case 0: return "Khởi tạo"; break;
                                case 1: return "Thành công"; break;
                                case 2: return "Đang xử lý"; break;
                                case -1: return "Thất bại"; break;
                                default: return "Unknown"; break;
                            }
                        }
                    }
                    , { 'data': 'amount1' }
                ]
        });

    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}