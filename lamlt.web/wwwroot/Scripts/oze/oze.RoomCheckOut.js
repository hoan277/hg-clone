﻿/// <reference path="print/printJs.js" />

var RoomPriceType =
{
    HOUR: 0,
    DAY: 1,
    NIGHT: 2,
    ALL: -1,
    MONTH:3,

    EXTRA_EARLY_NIGHT: 5,
    EXTRA_LATE_NIGHT: 6,
    EXTRA_EARLY_DAY: 7,
    EXTRA_LATE_DAY: 8,
    EXTRA_LATE_ADULT: 9,
    EXTRA_LATE_CHIDLREN: 10,
}

//1:Phụ trội quá giờ trả theo ngày
//    / 2:  Phụ trội quá giờ trả theo đêm / 3: Phụ trội nhận phòng sớm theo ngày/ 4:  Phụ trội nhận phòng sớm theo đêm
//    / 5 :Phụ trội quá số lượng người lớn

var $modalMail = $("#modalDetails");
//phụ trội quá giờ theo trả ngày
var counservice = 0;
var information;
var informationOld;
var PayMoney = {};

function updateInformation() {
    informationOld = information;
    information =
    {
        khunggio: $("#dllKhungGio").val(),

        Deposit: $("#Deposit").val(),
        Deduction: $("#Deduction").val(), //Giảm trừ

        Name: $("#NameCheckout").val(),
        Phone: $("#PhoneCheckout").val(),
        Identity: $("#IdentifyNumberCheckout").val(),
        Email: $("#EmailCheckout").val(),

        Adult: $("#Number_People").val(),
        Children: $("#Number_Children").val(),

        Discount: $("#Discount").val(),
        DiscountType: $("#dllDiscountType").val(),

        UpPrice: $("#upPrice").length > 0 ? $("#upPrice").val() : 0,
        UpPriceType: $("#dllUpPrice").length > 0 ? $("#dllUpPrice").val() : 0,

        Description: $("#Description").val(),
        Payment_Type_ID: $("#dllPaymentType").val(), // phuong thuc thanh toan

        TravelId: $("#dllTravel").val(), // đại lý
    }
}
var timeoutAuto = -1;
function nochange() {
    if (informationOld === undefined)
        return false;
    return (informationOld.khunggio == information.khunggio)
    && (informationOld.Deposit == information.Deposit)
    && (informationOld.Deduction == information.Deduction)

    && (informationOld.Adult == information.Adult)
    && (informationOld.Children == information.Children)

    && (informationOld.Discount == information.Discount)
    && (informationOld.DiscountType == information.DiscountType)

    && (informationOld.UpPrice == information.UpPrice)
    && (informationOld.UpPriceType == information.UpPriceType);

}
function callToPriceAuto() {
    if (nochange())
        return;
    if (timeoutAuto)
        clearTimeout(timeoutAuto);
    timeoutAuto = setTimeout(callToPrice, 300);
}
function setUpChange() {
    // Giảm trừ
    $("#Deduction").focusout(function () {
        $("#div_Deduction").html(MoneyToString($("#Deduction").val()));
        updateInformation();
        callToPriceAuto();
    });
    // Giảm giá
    $("#Discount").focusout(function () {
        //$("#div_Discount").html(MoneyToString($("#Discount").val()));
        updateInformation();
        callToPriceAuto();
    });
    // Giảm giá
    $("#dllDiscountType").change(function () {
        //$("#div_Discount").html(MoneyToString($("#Discount").val()));
        updateInformation();
        callToPriceAuto();
    });
    // Tăng giá ngày lễ
    $("#upPrice").change(function () {
        var v = $(this).val();
        if (!v)
            $(this).val(0);
        //$("#div_UpPrice").html(MoneyToString($("#upPrice").val()));
        updateInformation();
        callToPriceAuto();
    });
    // Tăng giá ngày lễ
    $("#dllUpPrice").change(function () {
        //$("#div_UpPrice").html(MoneyToString($("#upPrice").val()));
        updateInformation();
        callToPriceAuto();
    });

    //// Trả trước
    //$("#Deposit").focusout(function () {
    //    $("#div_Deposit").html(MoneyToString($("#Deposit").val()));
    //    updateInformation();
    //    callToPriceAuto();
    //});

    //Người lớn
    $("#Number_People").change(function () {
        updateInformation();
        callToPriceAuto();
    });
    // trẻ em
    $("#Number_Children").change(function () {
        updateInformation();
        callToPriceAuto();
    });
    // SĐT - updateInformation
    $("#PhoneCheckout").change(function () {
        updateInformation();
    });
    // tên - updateInformation
    $("#NameCheckout").change(function () {
        updateInformation();
    });
    // khung giờ
    $("#dllKhungGio").change(function () {
        updateInformation();
        callToPriceAuto();
    });
}
$(document).ready(function () {

    updateInformation();
    setUpChange();

    var datemin = moment($("#Leave_DateCheckOut").data("min"), "DD/MM/YYYY HH:mm").toDate();

    $("#Leave_DateCheckOut").datetimepicker({
        //locale: 'vi',
        debug: false,
        format: 'DD/MM/YYYY HH:mm',
        showTodayButton: true,
        showClose: false,
        sideBySide: true,
        minDate: datemin
    });

    $("#Leave_DatePlan").datetimepicker({
        //locale: 'vi',
        debug: false,
        format: 'DD/MM/YYYY HH:mm',
        showTodayButton: true,
        showClose: false,
        sideBySide: true,
        minDate: datemin
    });

    $("#btnDetailPrint").off().on('click', function () {
        printShow();
        //if (window.hotelConfig.jsPrintFn) {
        //    window[window.hotelConfig.jsPrintFn]();
        //} else {
        //    printShow();
        //}
    });

    $("#btnPay").click(function () {
        bootbox.confirm("Bạn có chắc chắn muốn thanh toán cho khách hàng này không?", function (result) {
            if (!result) return;
            updateInformation();
            var checkinID = parseInt($("#CheckInID").val());
            var roomId = parseInt($("#Roomid").val());
            var Leave_DateCheckOut = $("#Leave_DateCheckOut").val();
            showDialogLoading();
            $.post("/RoomCheckOut/PaymentCheckOut",
                { checkInid: checkinID, roomId: roomId, tdate: Leave_DateCheckOut, khunggio: $("#dllKhungGio").val(), temp: information },
                function (rs) {
                    hideDialogLoading();
                    if (rs.Status === "01")
                    {
                        bootboxLamlt.alert("Thao tác thành công!");

                        //auto clear-checkout
                        if (window.islockCard && window.islockCard == 1)
                        {
                            new LockCardControl().clear({ lockType: 4 });                            
                        } 
                        if (window.hotelConfig && window.hotelConfig.autoPrintOrder && window.hotelConfig.autoPrintOrder == 1) {
                            printShowAuto();
                            // setTimeout(function () { hashChangeSodophong(); }, 1000)
                        } else {
                            hashChangeSodophong();
                        }

                    } else {
                        bootboxLamlt.alert(rs.Message);
                    }
                    //g_Utils.SetAmount();
                });
        });
    });

    $("#btnPayBill").click(function () {
        bootbox.confirm("Bạn có chắc chắn muốn thanh toán cho khách hàng này không?", function (result) {
            if (!result) return;
            var checkinID = parseInt($("#CheckInID").val());
            var Leave_DateCheckOut = $("#Leave_DateCheckOut").val();
            showDialogLoading();
            $.post("/RoomCheckOut/PayBilltCheckOut",
                { checkInid: checkinID, tdate: Leave_DateCheckOut, khunggio: $("#dllKhungGio").val() },
                function (rs) {
                    hideDialogLoading();
                    if (rs.Status === "01") {
                        bootbox.alert(rs.Message,
                            function () {
                                $("body").addClass("modal-open1");
                            });
                    } else {
                        bootboxLamlt.alert(rs.Message);
                    }
                    //g_Utils.SetAmount();
                });
        });

    });

    $("#btnExport").click(function () {
        //$("#formExport #html").val(htmlPrinttext());
        ////window.open('/RoomCheckOut/Export2', '_blank');
        ////var win = window.open("", '_blank');
        ////if (win)
        ////    win.focus();
        $("#formExport").submit();
    });

    $("#btnPrint").off("click").on('click', function () {
        htmlPrint();
    });
    $("#btnPrint2").off("click").on('click', function () {
        htmlPrint2();
    });
    changePrintOption();


    //thêm dịch vụ
    $("#AddService").click(function (e) {
        addService(e);
    });

    //Theemdichj vụ khác
    $("#AddServiceOrther").click(function (e) {
        addServiceOrther(e);
    });

    $("#btnSendMail").click(function () {
        $.post("/RoomCheckOut/SendMail", function (rs) {
            //debugger;
            bootboxLamlt.alert(rs.Message);
            $("body").addClass("modal-open1");
        });
    });


    changeLeave_DateCheckOut();

    //
    $("#btnSaveEdit").off("click").click(function (e) {
        clickLeave_DateCheckOut(e)
    });

    $("#QuantityProduct").keypress(function (e) {
        if (e.which == 13) {
            $("#AddService").trigger("click");
        }
    });
    $("#PriceOther").keypress(function (e) {
        if (e.which == 13) {
            $("#AddServiceOrther").trigger("click");
        }
    });

    if ($(".toggleBox").length > 0) {
        toogleBox();
    }
    $(window).resize(function () {
        toogleBox();
    });
});

// click save
function clickLeave_DateCheckOut(e) {
    var check = $("#btnSaveEdit i").attr("class").includes('fa-pencil');
    showLeave_DateCheckOut(check, e);
    // save
    if (!check) {
        saveLeave_DateCheckOut();
    }
}
// esc, enter
function changeLeave_DateCheckOut() {
    $("#Leave_DateCheckOut").keyup(function (e) {
        //console.log(e.which);
        // esc
        if (e.which == 27) {
            showLeave_DateCheckOut(false);
            $("#Leave_DateCheckOut").val($("#Leave_DateOld").val());
        }
    });
    $("#Leave_DateCheckOut").keydown(function (e) {
        //console.log(e.which);
        // esc
        if (e.which == 13) {
            showLeave_DateCheckOut(false);
            saveLeave_DateCheckOut();
        }
    });
}
// ẩn hiện
function showLeave_DateCheckOut(isShow, e) {
    if (isShow) {
        $("#btnSaveEdit i").addClass("fa-check");
        $("#btnSaveEdit i").removeClass("fa-pencil");
        $("#Leave_DateCheckOut").prop("disabled", false);

        $("#btnDetailPrint").prop("disabled", true);
        $("#btnPay").prop("disabled", true);
        $("#btnCancelCheckIn").prop("disabled", true);
        $("#btnTransferCheckIn").prop("disabled", true);
        $("#btnsaveCheckInServer").prop("disabled", true);

        toogleBoxAction(e, true);
    } else {
        $("#btnSaveEdit i").addClass("fa-pencil");
        $("#btnSaveEdit i").removeClass("fa-check");
        $("#Leave_DateCheckOut").prop("disabled", true);

        $("#btnDetailPrint").prop("disabled", false);
        $("#btnPay").prop("disabled", false);
        $("#btnCancelCheckIn").prop("disabled", false);
        $("#btnTransferCheckIn").prop("disabled", false);
        $("#btnsaveCheckInServer").prop("disabled", false);
    }

}
//callToPrice
function saveLeave_DateCheckOut() {
    if ($("#Leave_DateCheckOut").val() === "") {
        bootboxLamlt.alert("Vui lòng nhập thời gian đi thực tế!");
        return;
    }
    if ($("#Leave_DateCheckOut").val() === $("#Leave_DateOld").val()) {
        hideDialogLoading();
        return;
    }
    // update old date
    $("#Leave_DateOld").val($("#Leave_DateCheckOut").val());
    // set khung giá
    updateKhungGio();
    // price
    callToPrice();

}

function updateKhungGio() {
    var fdate = moment($("#Arrive_Date").val(), 'DD/MM/YYYY HH:mm').toDate();
    var tdate = moment($("#Leave_DateCheckOut").val(), 'DD/MM/YYYY HH:mm').toDate();

    var tm = timespan(tdate.getTime() - fdate.getTime());

    var hours = tm.totalhours + ((tm.mins > startRoundMain) ? 1 : 0);

    var kh = $("#dllKhungGio").val();
    if (hours <= maxHours) {//maxHours
        appendKhungGio(true);
    } else {
        if (kh == 0)
            $("#dllKhungGio").val(-1);
        appendKhungGio(false);
    }
}

function appendKhungGio(isAppend) {
    var kh = $("#dllKhungGio").find("option[value=0]");
    // add
    if (isAppend) {
        if (kh.length == 0) {
            $("#dllKhungGio").find("option[value=-1]").after("<option value='0'>Theo giờ</option>");
        }
    }
        // remove
    else {
        if (kh.length == 1) {
            kh.remove();
        }
    }
}

//
function showRoomMateList(customerid) {
    $("#modalDetails1").off('show.bs.modal');
    $("#modalDetails1 .modal-body-content").html('<p>loading..</p>');
    $("#modalDetails1").on('show.bs.modal', function () {
        $("#modalDetails1 .modal-title").html("Thông tin khách trong phòng");
        showDialogLoading();
        $.post("/CustomerManage/GetRoomMateDetail", { customerid: customerid }, function (rs) {
            hideDialogLoading();
            $("#modalDetails1 .modal-body-content").html(rs);
            $("#modalDetails1 button#btnSave").css("display", "none");
            $("#modalDetails1 button#btnUpdateDetail").css("display", "none");
            $("#modalDetails1 button#btnUndoRoom").css("display", "none");
            $("#modalDetails1 button#btnChangeRoom").css("display", "none");
        });
    });
    $("#modalDetails1").modal("show");
};
function AddUsingRoomDialog(checkinID) {
    $("#modalDetails1").off('show.bs.modal');
    $("#modalDetails1").on('show.bs.modal', function () {
        $("#modalDetails1 .modal-title").html("Thêm khách ở cùng");
        //RuleValidateSubmitToAdd();
        $("#modalDetails1 .modal-body-content").html('<p>loading..</p>');
        $.post("/CustomerArriveManage/AddUsingCustomers", { checkinID: checkinID }, function (rs) {
            $("#modalDetails1 .modal-body-content").html(rs);
            Sv.SetupDatePicker([{ e: $(".birthday"), max: new Date() }]);
            g_Utils.SetDateDefaultAdd();
            g_Utils.ConfigAutocomplete('#Name', "/CustomerArriveManage/SelectCustomer", "Name", "Name",
                function (item) {
                    var data = (JSON.parse(item.object));
                    //"" CitizenshipCode : 238 Company : "" CountryId : null CreateBy : 1 CreateDate : "/Date(1481792366727)/" CustomerTypeID : 0 DOB : "/Date(1480525200000)/" Email : "" Fax : "" HotelCode : "OzeHotel0001" Id : 73 IdentifyNumber : "125309881" Mobile : "01658756994" ModifyBy : null ModifyDate : null Name : "Ngọc Lam Man" Phone : "" ReservationID : null Sex : 1 SourceID : 0 Status : true SysHotelID : null TaxCode : "" TeamMergeSTT : null TeamSTT : null

                    $("#modalDetails1 #formDetail :input").attr("disabled", true);
                    $('#Name').prop('disabled', false);
                    $("#formDetail  :input[name='Name']").val(data.Name);
                    $("#formDetail  :input[name='CustomerId']").val(data.Id);
                    $("#formDetail  :input[name='Company']").val(data.Company);
                    $("#formDetail  :input[name='CountryId']").val(data.CountryId == null ? "" : data.CountryId);
                    $("#formDetail  :input[name='Sex']").val(data.Sex);
                    $("#formDetail  :input[name='Email']").val(data.Email);
                    $("#formDetail  :input[name='IdentifyNumber']").val(data.IdentifyNumber);
                    $("#formDetail  :input[name='TeamMergeSTT']").val(data.TeamMergeSTT == null ? "0" : data.TeamMergeSTT);
                    $("#formDetail  :input[name='TeamSTT']").val(data.TeamSTT == null ? "0" : data.TeamSTT);
                    $("#formDetail  :input[name='Phone']").val(data.Mobile);
                    $("#formDetail  :input[name='DOB']").val(moment(new Date(parseInt(data.DOB.slice(6, -2)))).format("DD-MM-YYYY"));
                    valueold = data.Name;
                },
          function (query) {
              return {
                  //storeId: $('#txtKhoXuat_Add').val(),
                  search: query,
                  customerold: $("#formDetail  :input[name='CustomerIdOld']").val()
              }
          },
            function (data) {
                //if (data.Status === false) {
                //    return false;
                //}
                return data;
            }
          );
            $("#modalDetails1 button#btnSave").css("display", "inline-block");
            $("#modalDetails1 button#btnUpdateDetail").css("display", "inline-block");

            if ($("#modalDetails1 #btnSave").length > 0)
                $("#modalDetails1 #btnSave").off("click").click(function () { addRoomMate(); });
            if ($("#modalDetails1 #btnUpdateDetail").length > 0)
                $("#modalDetails1 #btnUpdateDetail").off("click").click(function () { addRoomMate(); });

        });
    });
    $("#modalDetails1").modal("show");
}
function EditCustomerDialog(checkinID) {
    $("#modalDetails1").off('show.bs.modal');
    $("#modalDetails1").on('show.bs.modal', function () {
        $("#modalDetails1 .modal-title").html("Thông tin khách hàng");
        //RuleValidateSubmitToAdd();
        $("#modalDetails1 .modal-body-content").html('<p>loading..</p>');
        $.post("/CustomerArriveManage/AddUsingCustomers", { checkinID: checkinID }, function (rs) {
            $("#modalDetails1 .modal-body-content").html(rs);
            SetDate(".birthday");
            g_Utils.SetDateDefaultAdd();
            g_Utils.ConfigAutocomplete('#Name', "/CustomerArriveManage/SelectCustomer", "Name", "Name",
                function (item) {
                    var data = (JSON.parse(item.object));
                    //"" CitizenshipCode : 238 Company : "" CountryId : null CreateBy : 1 CreateDate : "/Date(1481792366727)/" CustomerTypeID : 0 DOB : "/Date(1480525200000)/" Email : "" Fax : "" HotelCode : "OzeHotel0001" Id : 73 IdentifyNumber : "125309881" Mobile : "01658756994" ModifyBy : null ModifyDate : null Name : "Ngọc Lam Man" Phone : "" ReservationID : null Sex : 1 SourceID : 0 Status : true SysHotelID : null TaxCode : "" TeamMergeSTT : null TeamSTT : null

                    $("#modalDetails1 #formDetail :input").attr("disabled", true);
                    $('#Name').prop('disabled', false);
                    $("#formDetail  :input[name='Name']").val(data.Name);
                    $("#formDetail  :input[name='CustomerId']").val(data.Id);
                    $("#formDetail  :input[name='Company']").val(data.Company);
                    $("#formDetail  :input[name='CountryId']").val(data.CountryId == null ? "" : data.CountryId);
                    $("#formDetail  :input[name='Sex']").val(data.Sex);
                    $("#formDetail  :input[name='Email']").val(data.Email);
                    $("#formDetail  :input[name='IdentifyNumber']").val(data.IdentifyNumber);
                    $("#formDetail  :input[name='TeamMergeSTT']").val(data.TeamMergeSTT == null ? "0" : data.TeamMergeSTT);
                    $("#formDetail  :input[name='TeamSTT']").val(data.TeamSTT == null ? "0" : data.TeamSTT);
                    $("#formDetail  :input[name='Phone']").val(data.Mobile);
                    $("#formDetail  :input[name='DOB']").val(moment(new Date(parseInt(data.DOB.slice(6, -2)))).format("DD-MM-YYYY"));
                    valueold = data.Name;
                },
          function (query) {
              return {
                  //storeId: $('#txtKhoXuat_Add').val(),
                  search: query,
                  customerold: $("#formDetail  :input[name='CustomerIdOld']").val()
              }
          },
          function (data) {
              //if (data.Status === false) {
              //    return false;
              //}
              return data;
          }
      );

        });


    });
    $("#modalDetails1 button#btnUpdateDetail").css("display", "inline");
    $("#modalDetails1 button#btnUpdateDetail").css("disabled", "false");
    $("#modalDetails1 button#btnUndoRoom").css("disabled", "true");
    $("#modalDetails1 button#btnChangeRoom").css("disabled", "true");

    $("#modalDetails1 button#btnUndoRoom").css("display", "none");
    $("#modalDetails1 button#btnChangeRoom").css("display", "none");


    $("#modalDetails1").modal("show");
}
function addRoomMate() {
    // thêm khách ở cùng
    if (!$("#formDetail").valid()) return;
    var pdata = getFormData($("#formDetail"));
    var id = $("#formDetail [name='CustomerId']").val();

    if (id > 0) {
        var model = {
            CustomerId: id,
            CheckInID: $("#formDetail  :input[name='CheckInID']").val(),
            Roomid: $("#formDetail  :input[name='Roomid']").val(),
            SysHotelID: $("#formDetail  :input[name='SysHotelID']").val(),
            CustomerIdOld: $("#formDetail  :input[name='CustomerIdOld']").val()
        }
        pdata = model;
    }
    showDialogLoading();
    $.post("/CustomerArriveManage/AddUsingRoom", { obj: pdata }, function (rs) {
        hideDialogLoading();
        //closeDlgLoadingData();
        if (rs.Status === "01") {
            bootbox.alert(rs.Message, function () {
                //searchGrid();
                $("#modalDetails1").modal("hide");
            });
        }
        else {
            bootboxLamlt.alert(rs.Message);
        }
    });
}
//view cập nhật thông tin khách hàng
function viewEditDialog(id) {
    $("#modalDetails1").off('show.bs.modal');
    $("#modalDetails1").on('show.bs.modal', function () {

        $(".modal-title").html("Thông tin khách hàng");
        //RuleValidateSubmitToAdd();
        $("#modalDetails1 .modal-body-content").html('<p>loading..</p>');
        $.post("/CustomerManage/GetEdit", { id: id }, function (rs) {
            $("#modalDetails1 .modal-body-content").html(rs);
            g_Utils.SetDate(".birthday");
        });

        $("#modalDetails1 button#btnUpdateDetail").css("display", "inline");
        $("#btnUpdateDetail").off("click");

        $("#btnUpdateDetail").click(function () {

            if (!$("#formDetail").valid()) return;
            var pdata = getFormData($("#formDetail"));
            showDialogLoading();
            $.post("/CustomerManage/update", { obj: pdata }, function (data) {
                hideDialogLoading();
                //closeDlgLoadingData();
                if (data.result > 0) {
                    $("#NameCheckout").val(pdata.Name);
                    $("#PhoneCheckout").val(pdata.Mobile);
                    $("#IdentifyNumberCheckout").val(pdata.IdentifyNumber);
                    bootbox.alert("Cập nhật thông tin khách hàng thành công.", function () {
                        //searchGrid();
                        $("#modalDetails1").modal("hide");
                    });
                }
                else {
                    bootboxLamlt.alert("Cập nhật thống tin khách hàng thất bại!");
                }
            });
        });
    });
    $("#modalDetails1").modal("show");
}

function ExChangeDialogToTransfer(id) {
    initModalIfNeeed("transferModal");
    $("#transferModal")
        .off('show.bs.modal')
        .on('show.bs.modal', function () {
            $("#transferModal .modal-body-content").html('<p>loading..</p>');

            $.post("/CustomerArriveManage/getRoomTransfer", { id: id }, function (rs) {
                $("#transferModal .modal-body-content").html(rs);
                $("#btnSavetransferModal")
                    .off("click")
                    .click(function () {

                        var tranRoom = $("#tranRoomType1").is(":checked");
                        var tranSv = $("#tranRoomType2").is(":checked");

                        if (!tranRoom && !tranSv) {
                            alert("Vui lòng chọn chuyển thanh toán tiền phòng hoặc chuyển tiền dịch vụ!");
                            return;
                        }

                        if (tranRoom && !tranSv) {
                            alert("Không thể chuyển thanh toán tiền phòng khi phòng đang có tiền dịch vụ chưa thanh toán!");
                            return;
                        }

                        var $e = $("#transferModal").find(".room-selected");
                        if ($e.length != 1) {
                            alert("Vui lòng chọn 1 phòng cần chuyển thanh toán!");
                            return;
                        }

                        var roomId = $e.data("roomid");
                        var roomName = $e.data("name");
                        var ckCode = $("[name=BookingCode]").val();
                        var st = "";
                        if (tranRoom && tranSv) {
                            st = "";
                        } else if (tranSv) {
                            st = "tiền dịch vụ";
                        }
                        bootbox.confirm("Bạn có chắc chắn chuyển thanh toán " + st + " cho hóa đơn mã: <strong>" + ckCode + "</strong> tới phòng:  <strong>" + roomName + "</strong> không?", function (result) {
                            if (result) {
                                $("#transferModal").modal("hide");
                                submitTransfer(roomId, tranRoom, tranSv);
                            }
                        });
                    });

            });
        }).modal("show");
}

function submitTransfer(roomId, tranRoom, tranSv) {
    var CheckInID = $('#CheckInID').val();
    var tdate = $("#Leave_DateCheckOut").val();
    var roomOld = $('#Roomid').val();
    var Note = $('#Description').val();
    $.post("/CustomerArriveManage/TransferRoom", { roomOld: roomOld, toRoomId: roomId, tranRoom: tranRoom, tranSv: tranSv, CheckInID: CheckInID, Note: Note, tdate: tdate, temp: information }, function (rs) {
        hideDialogLoading();
        if (rs.Status === "01") {
            bootboxLamlt.alert(rs.Message, function () {
                hashChangeSodophong();
                $("#transferModal").modal("hide");
            });
        }
        else {
            bootboxLamlt.alert(rs.Message);
        }
    });
}

function initModalIfNeeed(idModal, classHtml) {
    if (document.getElementById(idModal)) { return; }

    var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
    html += '<div class="modal-dialog ' + classHtml + '">';
    html += '    <div class="modal-content">';
    html += '        <div class="modal-header">';
    html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
    html += '            <h4 class="modal-title btn-header">Chuyển thanh toán</h4>';
    html += '        </div>';
    html += '        <div class="modal-body">';
    html += '            <div class="modal-body-content">';
    html += '                <p>Loading...</p>';
    html += '            </div>';
    html += '</div>';
    html += '        <div class="modal-footer">';
    html += '            <button type="button" id="btnSavetransferModal" class="btn btn-info" > Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
    html += '        </div>';
    html += '    </div>';
    html += '</div>';
    html += '</div>';
    $("body").append(html);
    $("#" + idModal).modal();
}

// viết lại phần add dịch vụ - sản phẩm
function addService(e) {
    e.preventDefault();
    var quantity = parseInt($("#QuantityProduct").val());
    var checkinID = parseInt($("#CheckInID").val());
    var hotelID = parseInt($("#SysHotelID").val());
    var customerid = parseInt($("#CustomerId").val());
    var productId = parseInt($("#slProductID").val());
    var roomId = parseInt($("#Roomid").val());

    // add dịch vụ vào phòng đầu tiền
    if (roomId < 0) {
        bootboxLamlt.alert("Không thể thêm dịch vụ khi không chọn phòng sử dụng dịch vụ!");
        return;
    }


    //$("#addptqgtn").click(function () {
    if (productId === 0) {
        bootboxLamlt.alert('Vui lòng chọn dịch vụ !');
        return;
    }
    if (quantity === 0) {
        bootboxLamlt.alert('Chưa nhập số lượng !');
        return;
    }
    var url = "/RoomCheckOut/InsertService";
    Sv.Post({
        url: url, data: {
            productId: productId,
            checkinID: checkinID,
            hotelID: hotelID,
            customerid: customerid,
            roomId: roomId,
            Quantity: quantity
        }
    }, function (rs) {

        if (rs.Status === "01") {
            $(".listService").append(binHtmlSP(rs.Data));
            $("#QuantityProduct").val(0);
            $("#slProductID").val(0);
            binTotalPay(parseFloat(rs.Data.TotalSale));
        } else {
            bootboxLamlt.alert("Thêm mới dịch vụ thất bại");
        }
        setMaxHeight();
    });
}
// viết lại phần add dịch vụ
function addServiceOrther(e) {
    e.preventDefault();
    var name = $("#ServerOther").val();
    var PriceOther = parseFloat($("#PriceOther").val());
    var checkinID = parseInt($("#CheckInID").val());
    var hotelID = parseInt($("#SysHotelID").val());
    var customerid = parseInt($("#CustomerId").val());
    var roomId = parseInt($("#Roomid").val());

    if (roomId < 0) {
        bootboxLamlt.alert("Không thể thêm dịch vụ khi không chọn phòng sử dụng dịch vụ!");
        return;
    }
    if (name === "") {
        bootboxLamlt.alert('Vui lòng nhập dịch vụ !');
        return;
    }
    if (PriceOther === 0 || PriceOther === "") {
        bootboxLamlt.alert('Chưa nhập giá dịch vụ !');
        return;
    }
    var url = "/RoomCheckOut/InsertNewOtherService";
    Sv.Post({
        url: url,
        data: {
            name: name,
            checkinID: checkinID,
            hotelID: hotelID,
            customerid: customerid,
            roomId: roomId,
            price: PriceOther
        }
    }, function (rs) {
        if (rs.Status === "01") {
            $(".listServiceOrther").append(bindHtmlDV(rs.Data));
            $("#ServerOther").val("");
            $("#PriceOther").val(0);
            binTotalPay(parseFloat(PriceOther));

        } else {
            bootboxLamlt.alert(rs.Message);
            return;
        }
        setMaxHeight();
    });

}


function removeptqgtn(id) {
    if (!isPermissionRemove()) {
        alert("Để xóa dịch vụ vui lòng liên hệ quản lý. xin cám ơn");
        return;
    }
    showDialogLoading();
    $.post("/RoomCheckOut/LockCustomerService", { cussvID: id }, function (rs) {
        hideDialogLoading();
        if (rs.Status === "01") {
            //var price = $("#IditemService" + id).html().replace(/=|\./g, '');
            var price = $("#IditemService" + id).data("price");
            binTotalPay(-parseFloat(price));
            $('.itemService' + id).remove();
            bootboxLamlt.alert("Xóa dịch vụ thành công.");

        } else {
            bootboxLamlt.alert("Xóa dịch vụ thất bại!");
        }

    });

}
//
function removeOtherService(id) {
    if (!isPermissionRemove()) {
        alert("Để xóa dịch vụ vui lòng liên hệ quản lý. xin cám ơn");
        return;
    }
    showDialogLoading();
    $.post("/RoomCheckOut/LockCustomerService",
    {
        cussvID: id
    }, function (rs) {
        hideDialogLoading();
        if (rs.Status === "01") {
            //var price = $("#IditemService" + id).html().replace(/=|\./g, '');
            var price = $("#IditemService" + id).data("price");
            binTotalPay(-parseFloat(price));

            $('.itemServiceOrther' + id).remove();
            bootboxLamlt.alert("Xóa dịch vụ thành công.");
        } else {
            bootboxLamlt.alert("Xóa dịch vụ thất bại!");
        }

    });

}

function binTotalPay(total) {
    var t = parseFloat($("#txtTotalPrice").val()) + total;
    var p = parseFloat($("#txtTotalPay").val()) + total;
    bindTotal({ tongtien: t, thanhtoan: p });
    g_Utils.SetAmount();
}






// tính tiền
function callToPrice(e) {

    var roomId = parseInt($("#Roomid").val());
    var data = {
        tDate: $("#Leave_DateCheckOut").val(),
        roomId: roomId,
        checkinID: parseInt($("#CheckInID").val()),
        khunggio: $("#dllKhungGio").val(),
        temp: information
    };
    Sv.Post({
        url: "/RoomCheckOut/PayRoomPrice",
        data: data
    }, function (rs) {
        if (rs.Status != 1) {
            bootboxLamlt.alert(rs.Message);
        } else {
            bindThongtinThanhToan(rs);
            // set lai width box tien phong, tien dich vu
            Sv.SetScrollHeight($(".box-item"));
            setMaxHeight();
        }
    });
}

function bindThongtinThanhToan(data) {
    //console.log(data);
    bindRoomPrice(data.listRoomPrice);
    bindService(data.listService);

    bindTotal({
        tongtien: data.Total,
        tratruoc: data.totalDeposit,
        giamtru: data.totalDeduction,
        giamgia: data.InforCustomer.Discount,
        giamgiaType: data.InforCustomer.DiscountType,
        tanggia: data.InforCustomer.upPrice,
        tanggiaType: data.InforCustomer.upPriceType,
        thanhtoan: data.totalpay,
    });
}

function bindRoomPrice(data) {
    var html = "";
    if (data.length > 0) {
        html += ('<table class="table table-striped table-bordered dataTable" cellspacing="0" style="width: 100%;">' +
	            '<thead>' +
                '<tr role="row">' +
                '<th width="10%">Phòng</th>' +
                '<th width="37%">Thời gian sử dụng</th>' +
                '<th width="15%">ĐV thời gian</th>' +
                '<th width="20%">Ghi chú</th>' +
                '<th width="18%">Thành tiền</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>');

        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            //console.log(item);
            var isDisabled = (window.hotelConfig.price_changeprice != null && window.hotelConfig.price_changeprice == '1') || !(item.typePrice == RoomPriceType.MONTH || item.typePrice == RoomPriceType.HOUR || item.typePrice == RoomPriceType.DAY || item.typePrice == RoomPriceType.NIGHT || item.typePrice == RoomPriceType.ALL);
            var htmlInput = "";
            if (item.isTransfer || isDisabled) {
                htmlInput += '<div class="input-group"> ' +
                                '<input class="form-control number-mask pricelevelIt" name="pricelv_disabled_' + i + '" value="' + item.price + '" disabled />' +
                                '<span class="input-group-addon" disabled>VND </span>' +
                            '</div>';
            } else {
                htmlInput += '<div class="input-group"> ' +
                               '<input class="form-control number-mask pricelevelIt" onchange="changePriceItem(event)" name="pricelv' + i + '" value="' + item.price + '" />' +
                               '<span class="input-group-addon" disabled>VND </span>' +
                           '</div>';
            }
            //} else {
            //    htmlInput += '<div class="input-group"> ' +
            //                     '<input class="form-control number-mask pricelevelIt" name="pricelv' + i + '" value="' + item.price + '" disabled />' +
            //                     '<span class="input-group-addon" disabled>VND </span>' +
            //                 '</div>';
            //}

            html += ('<tr class="tpitem" data-id="' + item.pricePolicyId + '" data-roomid="' + item.roomid + '" data-typeprice="' + item.typePrice + '" data-titleprice="' + item.titlePrice + '" data-title="' + (Sv.NumberToString(item.quantiy) + ' ' + item.titlePrice) + '"  data-dtfrom="' + item.dtFrom + '" data-dtto="' + item.dtTo + '"  data-quantiy="' + item.quantiy + '"  >' +
                '<td>' + item.roomName + '</td>' +
                '<td>' + (Sv.DateToString(item.dtFrom, "DD/MM/YYYY HH:mm") + '-' + Sv.DateToString(item.dtTo, "DD/MM/YYYY HH:mm")) + '</td>' +
                '<td>' + (Sv.NumberToString(item.quantiy) + ' ' + item.titlePrice) + '</td>' +
                '<td>' + (item.pricePolicyName && item.pricePolicyName != 'null' ? item.pricePolicyName : '') + '</td>' +
                '<td class="text-right"> ' +
                    htmlInput +
                '</td> ' +
                '</tr>');
        }
        html += ('</tbody>' +
			'</table>');

    }

    $("#PriceEstimate").html(html);
    Sv.SetupInputMask();
}


function binHtmlSP(item) {
     
    var html = ('<div class="form-group bdbtsv itemService' + item.cussvID + '">' +
                               '<label class="col-xs-5 text-label"> ' + item.RoomName + ": " + item.Name + ' </label> ' +
                                '<label class="col-xs-2 pd0 text-label">' + Sv.DateToString(item.datecreated, "DD/MM/YYYY HH:mm") + ' </label> ' +
                                '<label class="col-xs-2 pd0 text-label text-right">' + Sv.NumberToString(item.SalePrice) + 'x' + Sv.NumberToString(item.Quantity) + '</label> ' +
                                '<label class="col-xs-2 pd0 control-label" id="IditemService' + item.cussvID + '" Item="' + item.cussvID + '" data-price="' + item.TotalSale + '">=' + Sv.NumberToString(item.TotalSale) + '</label>' +
                               '<label class="col-xs-1 control-label">' +
                                    (isPermissionRemove() ?  ('<a><i class="fa fa-remove" onclick="removeptqgtn(' + item.cussvID + ')"></i></a> ') : '') +
                                '</label> ' +
                            '</div>');
    return html;
}
function binHtmlSP0(item) {
    var html = ('<tr>' +
                  '<td>' + item.RoomName + '</td>' +
                  '<td>' + item.Name + '</td>' +
                  '<td>' + Sv.DateToString(item.datecreated, "DD/MM/YYYY HH:mm") + '</td>' +
                  '<td>' + Sv.NumberToString(item.Quantity) + ' x ' + Sv.NumberToString(item.SalePrice) + '</td>' +
                  '<td class="text-right" id="IditemService' + item.cussvID + '" Item="' + item.cussvID + '" data-price="' + item.TotalSale + '">' + Sv.NumberToString(item.TotalSale) + '</td> ' +
                  '<td>' + (isPermissionRemove() ? ('<a><i class="fa fa-remove" onclick="removeptqgtn(' + item.cussvID + ')"></i></a> ') : '') + '</td> ' +
                  '</tr>');
    return html;
}
function bindHtmlDV(item) {
    var html = ('<div class="form-group bdbtsv itemServiceOrther' + item.cussvID + '"> ' +
                                    '<label class="col-xs-5 text-label"> ' + item.RoomName + ": " + item.Name + ' </label>' +
                                    '<label class="col-xs-2 pd0 text-label"> ' + Sv.DateToString(item.datecreated, "DD/MM/YYYY HH:mm") + ' </label>' +
                                    '<label class="col-xs-2 pd0 text-label text-right">' + Sv.NumberToString(item.SalePrice) + 'x' + Sv.NumberToString(item.Quantity) + '</label> ' +
                                    '<label class="col-xs-2 pd0 control-label" id="IditemService' + item.cussvID + '" Item="' + item.cussvID + '" data-price="' + item.TotalSale + '" >=' + Sv.NumberToString(item.SalePrice * item.Quantity) + ' </label>' +
                                    '<label class="col-xs-1 control-label"> ' +
                                      (isPermissionRemove() ? ('<a><i class="fa fa-remove" onclick="removeOtherService(' + item.cussvID + ')"></i></a> ') : '') +
                                    '</div> ' +
                            '</div>');
    return html;
}

function bindService(listService) {
    //var htmlSp = tableSp();
    var htmlSp = "";
    var htmlDv = "";
    if (listService.length > 0) {
        for (var i = 0; i < listService.length; i++) {
            var item = listService[i];
            if (item.UnitID > 0)  // sp
                htmlSp += binHtmlSP(item);
            else // dịch vụ
                htmlDv += bindHtmlDV(item);
        }
    }
    //htmlSp += ('</tbody>' +
    //		'</table>');
    $(".listService").html(htmlSp);
    $(".listServiceOrther").html(htmlDv);
}
function bindTotal(option) {
    if (option.tongtien != undefined && option.tongtien != null) {
        //$("#txtTotalPrice").val(option.tongtien);
        PayMoney.tongtien = option.tongtien;
    }

    if (option.tratruoc != undefined && option.tratruoc != null) {
        //$("#txtDeposit").val(option.tratruoc);
        PayMoney.tratruoc = option.tratruoc;
    }

    if (option.giamtru != undefined && option.giamtru != null) {
        //$("#txtDeduction").val(option.giamtru);
        PayMoney.giamtru = option.giamtru;
    }

    if (option.thanhtoan != undefined && option.thanhtoan != null) {
        //$("#txtTotalPay").val(option.thanhtoan);
        PayMoney.thanhtoan = option.thanhtoan;
    }

    if (option.giamgia != undefined && option.giamgia != null) {
        //$("#txtGiamGia").val(option.giamgia);
        PayMoney.giamgia = option.giamgia;
    }
    if (option.giamgiaType != undefined && option.giamgiaType != null) {
        if (option.giamgiaType == 1) {
            $(".giamgia-addon").html("%");
        } else {
            $(".giamgia-addon").html("VND");
        }
        PayMoney.giamgiaType = option.giamgiaType;
    }
    if (option.tanggia != undefined && option.tanggia != null) {
        //$("#txtTangGia").val(option.tanggia);
        PayMoney.tanggia = option.tanggia;
    }

    if (option.tanggiaType != undefined && option.tanggiaType != null) {
        if (option.tanggiaType == 1) {
            $(".tanggia-addon").html("%");
        } else {
            $(".tanggia-addon").html("VND");
        }
        PayMoney.tanggiaType = option.tanggiaType;
    }
    paymentChangecurrency();
    toStringMoneyPay();
}

function toStringMoneyPay() {
    var $p = $("#boxPay");
    $p.find(".amount-double-mask_0, .amount-double-maskpay_0").each(function (e, v) {
        var value = $(v).val();
        var $t = $(v).parents(".form-group").find(".number-tostring");
        if (parseFloat(value) != 0) {
            $t.html(MoneyToString(parseFloat(value)));
        } else {
            $t.html("");
        }
    });
}

function tableSp() {
    var html = ('<table class="table table-striped table-bordered dataTable" cellspacing="0" style="width: 100%;">' +
                  '<thead>' +
                  '<tr role="row">' +
                  '<th width="10%">Phòng</th>' +
                  '<th width="40%">Sản phẩm</th>' +
                  '<th width="15%">T/g sử dụng</th>' +
                  '<th width="15%">Sl x Đg</th>' +
                  '<th width="15%">Thành tiền</th>' +
                   '<th width="5%"></th>' +
                  '</tr>' +
                  '</thead>' +
                  '<tbody>');
    //html += ('</tbody>' +
    //		'</table>');
    return html;
}

function tableDv() {
    var html = ('<table class="table table-striped table-bordered dataTable" cellspacing="0" style="width: 100%;">' +
                 '<thead>' +
                 '<tr role="row">' +
                 '<th width="5%">#</th>' +
                 '<th width="55%">Dịch vụ</th>' +
                 '<th width="20%">Thời gian sử dụng</th>' +
                 '<th width="20%">Thành tiền</th>' +
                 '</tr>' +
                 '</thead>' +
                 '<tbody>');
    //html += ('</tbody>' +
    //		'</table>');
    return html;
}

function toogleBox(e) {
    // set max height để dùng css transition
    $.each($(".box-oze"), function (i, e) {
        if ($(e).find(".header-toggler").length > 0) {
            var h = $(e).outerHeight();
            $(e).css("max-height", h);
            if (i < 3)
                $(e).addClass("box-oze-close");
        }
    })

    $(".toggleBox").off("click").on("click", function (e) {
        toogleBoxAction(e);
    });
    $(".header-toggler").off("click").on("click", function (e) {
        var icon = $(e.target).closest(".header-action-item");
        if (icon.length > 0) return;

        toogleBoxAction(e);
    });
}

function toogleBoxAction(e, ishow) {
    var $b = $(e.target).closest(".box-oze");
    if ($b.length <= 0) return;
    //console.log($b.find(".header-toggler"));
    if ($b.find(".header-toggler").length == 0) { return; }
    // set lại css max height
    var h = $b[0].scrollHeight;
    var cssH = $b.css("max-height").replace(/[^-\d\.]/g, '');
    if (cssH != h)
        $b.css("max-height", h);

    //var h = $b.scrollHeight;
    //$b.css("max-height", h*2);
    //toggleClass
    if (ishow) {
        if ($b.hasClass("box-oze-close"))
            $b.removeClass("box-oze-close");
    } else
        $b.toggleClass("box-oze-close");
}

function setMaxHeight(e) {
    $.each($(".box-oze"), function (i, e) {
        var h = $(e)[0].scrollHeight;
        $(e).css("max-height", h);
    })
}


function printShow() {     
    if (window.hotelConfig.jsPrintFn) {
        window[window.hotelConfig.jsPrintFn]();
    } else { 
        $("#modalDetails").off('show.bs.modal');
        $("#modalDetails").on('show.bs.modal', function () {
            $("#modalDetails .modal-body-content").html("");
            var checkinID = parseInt($("#CheckInID").val());
            var IsboolService = $('#ckService').is(":checked");
            var IsboolRoom = $('#ckRoom').is(":checked");
            var Leave_DateCheckOut = $("#Leave_DateCheckOut").val();
            var khunggio = $("#dllKhungGio").val();
            $("#modalDetails .modal-body-content").html('<p>loading..</p>');
            showDialogLoading();
            updateInformation();
            $.post("/RoomCheckOut/ViewPrintPay", { checkinID: checkinID, IsboolService: IsboolService, IsboolRoom: IsboolRoom, tDate: Leave_DateCheckOut, khunggio: khunggio, temp: information },
                function (rs) {
                    hideDialogLoading();
                    $("#modalDetails .modal-body-content").html(rs);
                    printCtr.load("", getPrintData());
                });
            $("#modalDetails button#btnUpdateDetail").css("display", "inline");
        });
        $("#modalDetails").modal("show");
    }
}


function printShowAuto() {    
    if (window.hotelConfig.jsPrintFn) {
        window[window.hotelConfig.jsPrintFn](101);
    } else {
        var checkinID = parseInt($("#CheckInID").val());
        var IsboolService = $('#ckService').is(":checked");
        var IsboolRoom = $('#ckRoom').is(":checked");
        var Leave_DateCheckOut = $("#Leave_DateCheckOut").val();
        var khunggio = $("#dllKhungGio").val();
        $("#modalDetails .modal-body-content").html('<p>loading..</p>');
        updateInformation();
        var m = { checkinID: checkinID, IsboolService: IsboolService, IsboolRoom: IsboolRoom, tDate: Leave_DateCheckOut, khunggio: khunggio, temp: information };

        $.post("/CustomerCheckOut/ViewPrintPay", m, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            changePrintOption();
            printCtr.load("", getPrintData());
            setTimeout(function () {
                // in thường - in nhiệt
                if (window.hotelConfig && window.hotelConfig.printType === 1) {
                    htmlPrint2();
                } else {
                    htmlPrint();
                }
                hashChangeSodophong();
            }, 200);
        });
    }
}


function htmlPrintxxx(rs, IsboolRoom, IsboolService) {
    var stt = 0;

    var totalSale = 0;
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write('<html>');
    mywindow.document.write('<head>');
    mywindow.document.write('<link rel="stylesheet" href="/Content/bootstrap.min.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/Content/hungpvCustom.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/Content/hungpvprint.css" type="text/css" />');
    mywindow.document.write('</head>');
    mywindow.document.write('<body class="container-fluid">');
    mywindow.document.write('<button class="button-print btn btn-primary noprint" onclick="window.print();"><i class="glyphicon glyphicon-print"></i></button>');
    mywindow.document.write('<div class="row">');

    mywindow.document.write('<div class="col-xs-7"> <div class="row">' +
        ' <div class="col-xs-12">' +
        ' <label class="col-xs-6 control-label ">Mã phiếu </label>' +
        ' <div class="col-xs-6">  </div> </div>' +
        ' <div class="col-xs-12"> ' +
        '<label class="col-xs-6 control-label ">Họ tên khách </label>' +
        ' <div class="col-xs-6"> ' + rs.InforCustomer.CustomerName + ' </div>' +
        ' </div> <div class="col-xs-12">' +
        ' <label class="col-xs-6 control-label ">Số điện thoại ' +
        '</label> <div class="col-xs-6"> ' + (rs.InforCustomer.Phone === null ? "" : rs.InforCustomer.Phone) + ' </div> </div>' +
        ' <div class="col-xs-12"> <label class="col-xs-6 control-label ">Ngày giờ thanh toán </label> ' +
        '<div class="col-xs-6"> ' + moment(new Date()).format('DD/MM/YYYY HH:mm') +
        ' </div> </div> </div> </div>' +
        ' <div class="col-xs-5"> <div class="col-xs-12 add-textcenter-hungpv"><img src="/images/icon/icon-logoanhung.png" style="width: 50px;display: inline;">' +
        ' <h4 style="color: goldenrod;display: inline;">' + rs.InforCustomer.HotelName + '</h4> </div> <div class="col-xs-12 add-textcenter-hungpv">' +
        ' <p style="font-size:12px">Địa chỉ: ' + rs.Hotel.Address + '</p> ' +
        '</div> <div class="col-xs-12 add-textcenter-hungpv"> ' +
        '<p style="font-size:12px">ĐT:' + rs.Hotel.Phone + '/DĐ:' + rs.Hotel.Mobile + '</p> </div> </div>');

    mywindow.document.write('<div class="col-xs-12">' +
        ' <table id="HistoryCustomer" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
        ' <thead>' +
        ' <tr>' +
        ' <th>STT</th> ' +
        ' <th>Nội dung</th>' +
        ' <th>Đơn vị</th>' +
        ' <th>Giá</th>' +
        ' <th>Số lượng</th> ' +
        '<th>Thành tiền</th> ' +
        '<th>Ghi chú</th> ' +
        '</tr>' +
        ' </thead> ');

    if (IsboolRoom) {
        mywindow.document.write('<tr><th colspan="7" style="text-align: left">Tiền phòng</th></tr>');
        for (var j = 0; j < rs.GetListPriceEstimate.length; j++) {
            stt = j + 1;
            totalSale = totalSale + parseFloat(rs.GetListPriceEstimate[j].price);
            mywindow.document.write(' <tr>' +
                '<td>' +
                stt +
                '</td>' +
                ' <td>' + rs.GetListPriceEstimate[j].roomName + ":" +
                moment(new Date(parseInt(rs.GetListPriceEstimate[j].dtFrom.slice(6, -2)))).format("DD/MM/YYYY HH:mm") + " - " + moment(new Date(parseInt(rs.GetListPriceEstimate[j].dtTo.slice(6, -2)))).format("DD/MM/YYYY HH:mm") +
                '</td> ' +
                ' <td>Tiền phòng</td> ' +
                '<td  class="dt-body-right">' +
                rs.GetListPriceEstimate[j].price.toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +
                '</td>' +
                ' <td  class="dt-body-right">' +
                rs.GetListPriceEstimate[j].quantiy.toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +
                '</td>' +
                ' <td  class="dt-body-right">' +
                rs.GetListPriceEstimate[j].price.toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +
                '</td>' +
                ' <td></td> ' +
                ' </tr>');
        }
    }
    if (IsboolService) {
        mywindow.document.write('<tr><th colspan="7" style="text-align: left">Dịch vụ</th></tr>');
        for (var i = 0; i < rs.GetListCustomerServices.length; i++) {
            stt = i + 1;
            totalSale = totalSale + parseFloat(rs.GetListCustomerServices[i].TotalSale);
            mywindow.document.write(' <tr>' +
                '<td>' +
                stt +
                '</td>' +
                ' <td>' +
                rs.GetListCustomerServices[i].Name +
                '</td> ' +
                ' <td>' +
                rs.GetListCustomerServices[i].UnitName +
                '</td> ' +
                '<td  class="dt-body-right">' +
                rs.GetListCustomerServices[i].SalePrice.toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +
                '</td>' +
                ' <td  class="dt-body-right">' +
                rs.GetListCustomerServices[i].Quantity.toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +
                '</td>' +
                ' <td  class="dt-body-right">' +
                rs.GetListCustomerServices[i].TotalSale.toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +
                '</td>' +
                ' <td></td> ' +
                ' </tr>');
        }
    }
    mywindow.document.write(' </table> ' +
    '</div> <div class="col-xs-12"> <div class="col-xs-6">' +
    ' </div> <div class="col-xs-6">' +
    ' <label class="col-xs-6 control-label ">Tổng tiền </label> ' +
        '<div class="col-xs-4"> ' + totalSale.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' </div>' +
         '<div class="col-xs-2"> VND </div>' +
    ' </div> <div class="col-xs-6"> </div> ' +
        '<div class="col-xs-6">' +
    ' <label class="col-xs-6 control-label ">Trả trước </label> ' +
    '<div class="col-xs-4"> ' + rs.InforCustomer.Deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' </div>' +
         '<div class="col-xs-2"> VND </div>' +
        '</div> <div class="col-xs-6"> ' +
    '</div> <div class="col-xs-6"> <label class="col-xs-6 control-label ">Giảm trừ </label> ' +
   '<div class="col-xs-4"> ' + rs.InforCustomer.Deduction.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' </div>' +
         '<div class="col-xs-2"> VND </div>' +
        '</div> <div class="col-xs-6"> ' +
    '</div> <div class="col-xs-6"> ' +
    '<label class="col-xs-6 control-label ">Tổng tiền thanh toán </label>' +
    '<div class="col-xs-4"> ' + (parseFloat(totalSale) - parseFloat(rs.InforCustomer.Deduction) - parseFloat(rs.InforCustomer.Deposit)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' </div>' +
         '<div class="col-xs-2"> VND </div>' +
        ' </div> </div> <div class="col-xs-9"> ' +
    '</div> <div class="col-xs-3"> Ngày...tháng... năm... </div> <br/> <div class="col-xs-12 "> ' +
    '<div class="row"> <div class="col-xs-4 add-textcenter-hungpv "> ' +
    '<label class="col-xs-12 control-label ">Khách hàng </label> <i>(Ký ghi rõ họ tên)</i> ' +
    '</div> <div class="col-xs-4 add-textcenter-hungpv">' +
    ' <label class="col-xs-12 control-label ">Quản lý </label> <i>(Ký ghi rõ họ tên)</i> ' +
    '</div> <div class="col-xs-4 add-textcenter-hungpv">' +
    ' <label class="col-xs-12 control-label ">Người lập </label> <i>(Ký ghi rõ họ tên)</i> </div> </div> </div>');

    mywindow.document.write("</div>");
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.focus();
}

function htmlPrint() {
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write(htmlPrinttext());
    mywindow.document.close();
    mywindow.focus();
}
function htmlPrint2() {
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write(htmlPrinttext2());
    mywindow.document.close();
    mywindow.focus();

}
function htmlPrinttext() {
    var html = "";
    html += ('<html>');
    html += ('<head>');
    html += ('<link rel="stylesheet" href="/Content/bootstrap.min.css" type="text/css" />');
    html += ('<link rel="stylesheet" href="/Content/hungpvCustom.css" type="text/css" />');
    html += ('<link rel="stylesheet" href="/Content/hungpvprint.css" type="text/css" />');
    html += ('</head>');
    html += ('<body class="container-fluid">');
    html += ('<button class="button-print btn btn-primary noprint" onclick="window.print();"><i class="glyphicon glyphicon-print"></i></button>');
    html += ('<div class="row">');

    html += $("#modalDetails .modal-body-content").html();

    html += ("</div>");
    //html += ("<script src='/Scripts/print/ctrl.js'></script>");
    html += ('</body>');
    html += ('</html>');
    return html;
}
function htmlPrinttext2() {
    var html = "";
    html += ('<html class="print2">');
    html += ('<head>');
    html += ('<link rel="stylesheet" href="/Content/bootstrap.min.css" type="text/css" />');
    html += ('<link rel="stylesheet" href="/Content/hungpvCustom.css" type="text/css" />');
    html += ('<link rel="stylesheet" href="/Content/hungpvprint.css" type="text/css" />');
    html += ('<link rel="stylesheet" href="/Content/billPrint.css" type="text/css" />');
    html += ('</head>');
    html += ('<body class="container-fluid">');
    html += ('<button class="button-print btn btn-primary noprint" onclick="window.print();"><i class="glyphicon glyphicon-print"></i></button>');
    html += ('<div class="row">');

    html += $("#modalDetails .modal-body-content").html();

    html += ("</div>");
    html += ('</body>');
    html += ('</html>');
    return html;
}

function getInfoSave() {
    return {
        Name: $("#NameCheckout").val(),
        Phone: $("#PhoneCheckout").val(),
        Identity: $("#IdentifyNumberCheckout").val(),
        Email: $("#EmailCheckout").val(),
        Company: $("#CompanyCheckout").length > 0 ? $("#CompanyCheckout").val() : "",


        khunggio: $("#dllKhungGio").val(), // khung giờ
        Deduction: $("#Deduction").val(), //Giảm trừ 

        Adult: $("#Number_People").val(), // người lớn
        Children: $("#Number_Children").val(), // trẻ em

        Discount: $("#Discount").val(), //Giảm giá
        DiscountType: $("#dllDiscountType").val(), //Giảm giá

        Deposit: $("#Deposit").val(), // trả trước

        UpPrice: $("#upPrice").length > 0 ? $("#upPrice").val() : 0, //Tăng giá ngày lễ
        UpPriceType: $("#dllUpPrice").length > 0 ? $("#dllUpPrice").val() : 0, //Tăng giá ngày lễ

        Description: $("#Description").val(), // ghi chú
        Payment_Type_ID: $("#dllPaymentType").val(), // phuong thuc thanh toan 

        TravelId: $("#dllTravel").val(), // đại lý
    };
}

// lưu thông tin thanh toán
function saveCheckInServer() {
    if (!($("#formInforDetailCustomer").valid())) return;
    bootbox.confirm("Bạn có chắc chắn muốn lưu thông tin hóa đơn này không?", function (result) {
        if (!result) return;
        showDialogLoading();
        $.post("/RoomCheckOut/SaveCheckOut",
            { checkInid: parseInt($("#CheckInID").val()), leaveDate: $("#Leave_DatePlan").val(), temp: getInfoSave() },
            function (rs) {
                hideDialogLoading();
                bootboxLamlt.alert(rs.Message);
            });
    });
}

function getDatachangeprice($e) {
    var $tr = $e.closest("tr.tpitem");
    var price = {
        pricelevelId: 0,
        ischange: true,
        title: $tr.data("title"),
        price: $e.val(),
        type: $tr.data("typeprice"),//$("#typeprice").val(),        
        typetitle: $tr.data("titleprice"),//$("#dllKhungGio option:selected").text(),
        khunggia: $("#dllKhungGio").val(),

        quantiy: $tr.data("quantiy"),

        datefrom: Sv.DateToString($tr.data("dtfrom"), "DD/MM/YYYY HH:mm"),
        dateto: Sv.DateToString($tr.data("dtto"), "DD/MM/YYYY HH:mm"),
        CheckInId: $("#CheckInID").val(),
        RoomId: $tr.data("roomid"),
    };
    temp = getInfoSave();
    return {
        pricelevel: price,
        temp: temp,
        tdate: $("#Leave_DateCheckOut").val(),
    };
}

function changePriceItem(e) {
    var $e = $(e.target);
    var model = getDatachangeprice($e);
    showDialogLoading();
    $.post("/RoomCheckOut/SavePriveLevelCheckOut", model, function (rs) {
        hideDialogLoading();
        //console.log(rs);
        if (rs.Status == 1) {
            bindThongtinThanhToan(rs);
            setMaxHeight();
            g_Utils.SetAmount();
        }
        else {
            bootboxLamlt.alert(rs.Message);
        }
    });
}

function changePrintOption() {
    $("#printType").off("change").on('change', function () {
        var v = $(this).val();
        var data = getPrintData();
        printCtr.load(v, data);
    });
}

function paymentChangecurrency() {
    var tCurrency = $("#dllCurrency").val();
    var rate = 1;
    if ($("#dllCurrency").length > 0)
        rate = parseFloat($("#dllCurrency option:selected").data("rate"));

    $("#txtTotalPrice").val(PayMoney.tongtien / rate);
    $("#txtTotalPrice").next().html(tCurrency);
    $("#txtDeposit").val(PayMoney.tratruoc / rate);
    $("#txtDeposit").next().html(tCurrency);
    $("#txtDeduction").val(PayMoney.giamtru / rate);
    $("#txtDeduction").next().html(tCurrency);
    if (PayMoney.giamgiaType == 0) {
        $("#txtGiamGia").val(PayMoney.giamgia / rate);
        $("#txtGiamGia").next().html(tCurrency);
    } else {
        $("#txtGiamGia").val(PayMoney.giamgia);
    }
    if (PayMoney.tanggiaType == 0) {
        $("#txtTangGia").val(PayMoney.tanggia / rate);
        $("#txtTangGia").next().html(tCurrency);
    } else {
        $("#txtTangGia").val(PayMoney.tanggia);
    }
    $("#txtTotalPay").val(PayMoney.thanhtoan / rate);
    $("#txtTotalPay").next().html(tCurrency);
    if (document.getElementById("txtTax")) $("#txtTax").val((10*PayMoney.tongtien / rate)/100);

}


// đổi phòng
function doiphong() {
    (new ReservationRoom()).doiphong($("#Roomid").val());

}

function isServiceinOrder() {
    var item = $(".listService.box-item").find(".bdbtsv");
    var itemOther = $(".listServiceOrther.box-item").find(".bdbtsv");
    return item.length > 0 || itemOther.length > 0;
}

// thanh toán tiền dịch vụ
function PayService() {

    var check = isServiceinOrder();
    if (check) {
        showDialogLoading();
        var obj = {
            checkInid: $("#CheckInID").val(),
            roomId: $("#Roomid").val(),
        }

        $("#modalDetailsDp").find(".modal-dialog").removeClass("modal-lg").removeClass("modal-md").addClass("modal-lg");
        $("#modalDetailsDp").off('show.bs.modal');
        $("#modalDetailsDp .modal-title").html("Xác nhận thanh toán dịch vụ");
        $("#modalDetailsDp").on('show.bs.modal', function () {
            $("#modalDetailsDp .modal-body-content").html('<p>loading..</p>');
            $.post("/RoomCheckOut/ViewServiceInOrder", obj, function (data) {
                hideDialogLoading();
                $("#modalDetailsDp .modal-body-content").html(data);

                $("#modalDetailsDp #btnSaveDp").html("Xác nhận");

                $("#modalDetailsDp #btnSaveDp").off("click").click(function () {
                    showDialogLoading();
                    var objSubmit = {
                        checkInid: $("#CheckInID").val(),
                        roomId: $("#Roomid").val(),
                        payment_type: $("#modalDetailsDp #dllPaymentTypeRoom").val()
                    }
                    $.post("/RoomCheckOut/PayServiceInOrder", objSubmit, function (rs) {
                        hideDialogLoading();
                        if (rs.IntStatus == 1) {
                            $('#modalDetailsDp').on('hidden.bs.modal', function () {
                                bootboxLamlt.alert(rs.Message);
                                refreshHash();
                            })
                            $("#modalDetailsDp").modal("hide");
                        }
                        else {
                            alert(rs.Message);
                        }
                    });
                });
            });
        });
        $("#modalDetailsDp").modal("show");

    } else {
        alert("Không có dịch vụ sử dụng trong hóa đơn này.");
    }
}
