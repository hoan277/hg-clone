﻿function api_doanhthu() {
    var self = this;
    this.tableName = "doanhthu";
    this.idModal = "modaldoanhthu";
    this.$table;
  
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_apidoanhthu').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/Lists",
                "data": function (d) {
                    delete d.columns;
                    //d.search = $("#search_apidoanhthu_title").val();
                    //d.cpid = $("#search_film_users_log_sub_type").val();
                    //d.status = $("#search_film_users_log_sub_type").val();
                    //d.catalogid = $("#search_film_users_log_catalogid").val();
                   // d.sub_type = $("#search_film_users_log_sub_type").val();
                  //  d.statusUserLog = $("#search_film_users_log_status").val();
                    //d.sub_state = $("#search_film_users_log_sub_state").val();
                    d.start_time = $("#start_time").val();
                    d.end_time = $("#end_time").val();
                    console.log(d.start_time);
                    console.log(d.end_time);
                    //console.log(d.sub_type);
                   // console.log(d.status);
                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'datecreated'}
                    , { 'data': 'countphoneactive' }
                    , { 'data': 'countphone' }
                    , { 'data': 'countCancel' }
                    , { 'data': 'giahan' }
                    , { 'data': 'countSucceed' }
                    , { 'data': 'countfailed' }
                    , { 'data': 'tlghtc' }
                    , { 'data': 'totaldoanhthu' }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }

}