﻿function film_product() {
    var self = this;
    this.tableName = "film_product";
    this.idModal = "modalfilm_product";
    this.$table;
    $("#tatca").on("click", function () {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "tatca"
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    });
    $("#dachon").on("click", function () {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "dachon",
                list_id: selected_id.toString()
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    });
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/film_product/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.edit_tagDialog = function (id) {
        window.open("/film_product_excel/Edit/" + id);
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.exportDialog = function (id) {
        $.ajax({
            type: 'POST',
            url: "/film_product/export_list/" + id,
            success: function (data) {
                hideDialogLoading();
                if (data != null) {
                    window.open(data.data);
                }
                else {
                    alert("Có lỗi khi tạo :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    var data_null = "<strong style='color:red'>Chưa cập nhật</strong>";
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_product').DataTable({
            fixedHeader: {
                header: true
                //footer: true
            },
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "rowId": "id",
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = String($("#s_title").val());
                    d.time_range = String($('#s_timerange').val());
                    stt = 1;
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = "";
                            var info = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                                '</a>  ';
                            var edit = '<a href="javascript:void(0)" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.edit_tagDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                                '</a>  ';
                            var delete1 = '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                '</a>  ';
                            var active = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-check" aria-hidden="true" style="color:#8BC34A" title="Duyệt"></span>' +
                                '</a>  ';
                            var convert = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.convertDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-sound-stereo" aria-hidden="true" style="color:#8BC34A" title="Convert"></span>' +
                                '</a>  ';
                            var export_excel = '<a title="Export" style="color: red;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.exportDialog(' + row.id + ')">' +
                                '<i class="fa fa-file-excel-o" aria-hidden="true"></i>' +
                                '</a>  ';
                            htmlMenu = info + edit + delete1 + convert + export_excel;
                            if (row.status != 1) {
                                htmlMenu = info + edit + delete1 + active + export_excel;
                            }
                            return htmlMenu;
                        }
                    }
                    , {
                        'data': null, render: function (data, type, row) {
                            var check = 0;
                            var rs = "";
                            rs += check == 0 ? "{'STT':'" + stt + "'," : ",{'STT':'" + stt + "',";
                            rs += "'Channel':'" + row.channel + "'}";
                            check++;
                            stt++;
                            var check_box = '<div class="checkbox"><label><input class="export_selected" type="checkbox" id="export_selected" name="export_selected" type="checkbox" value="' + rs + '">' + row.id + '</label></div >';
                            var html1 = check_box;
                            return html1;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var path = "";
                            if (row.path != null) {
                                //path = '<iframe' +' src=' + '"' + row.path + '"' +' title=' + '"' +row.title+ '"'+'>'+'</iframe>';
                                path = '<video width="192" controls><source src="' + row.path + '" type="video/mp4">' + row.title + '</video>';
                            }
                            return path;
                        }
                    }
                    , { 'data': 'title' }
                    //, { 'data': 'content' }
                    , { 'data': 'channel_title' }
                    , { 'data': 'upload_title' }
                    //, { 'data': 'description' }
                    , { 'data': 'path' }
                    , {
                        'data': null, render: function (row) {
                            var datecreated = "";
                            if (row.datecreated != null) {
                                datecreated = moment(row.datecreated).format('DD/MM/YYYY HH:mm:ss');
                            }
                            return datecreated;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var dateupdated = "";
                            if (row.dateupdated != null) {
                                dateupdated = moment(row.dateupdated).format('DD/MM/YYYY HH:mm:ss');
                            }
                            return dateupdated;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var convert_status = "";
                            if (row.convert_status == 0) { convert_status = "Chưa convert"; }
                            if (row.convert_status == 1) { convert_status = "Đang convert"; }
                            if (row.convert_status == 2) { convert_status = "Đã convert xong"; }
                            if (row.convert_status == 3) { convert_status = "Convert lỗi"; }
                            return convert_status;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var status = "";
                            if (row.status == 1) { status = "Đã duyệt"; }
                            else { status = "Chưa duyệt"; }
                            return status;
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '<hr/>';
        html += '       <button type="button" id="btnSave' + self.tableName + '" class="btn btn-warning" >Lưu</button> <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.activeDialog = function activeDialog(id) {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/UpdateStatus/" + id,
            success: function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    $("#" + self.idModal).modal("hide"); self.searchGrid();
                }
                else {
                    alert("Có lỗi khi xử lý :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
    //================================================= xuất excel ======================================================================
    this.exportAll = function exportAll() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "tatca",
                search: String($("#s_title").val()),
                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    this.exportList = function exportList() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "dachon",
                list_id: selected_id.toString(),
                search: String($("#s_title").val()),
                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    //================================================= xuất excel ======================================================================
    this.get_pro = function get_pro(id) {
        $.ajax({
            type: 'GET',
            url: '/film_product_processing/list_by_product_id/' + id,
            data: {
                product_id: id
            },
            success: function (data) {
                $("#list_pro_processing").html(data);
            }
        });
    }
    this.convertDialog = function convertDialog(id) {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/convert/" + id,
            success: function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    $("#" + self.idModal).modal("hide"); self.searchGrid();
                    alert("Đã tạo lệnh convert");
                }
                else {
                    alert("Có lỗi khi xử lý :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
    this.updateOrInsert = function (id) {
        if (!$("#form" + self.tableName).valid()) return;
        var pdata = getFormData($("#form" + self.tableName));
        console.log(pdata);
        showDialogLoading();
        $.post("/film_product/update", { obj: pdata }, function (data) {
            hideDialogLoading();
            if (data.result > 0) {
                alert("Thao tác thành công");
                //setTimeout(function () {
                //    history.back();
                //}, 1000);
            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });
    }
    this.gen_video_processing = function (id) {
        $.ajax({
            type: 'POST',
            url: '/film_product_processing_video/gen_now/' + id,
            data: {
                product_id: id
            },
            success: function (data) {
                $.ajax({
                    type: 'GET',
                    url: '/film_product_processing_video/list_by_product_id/' + id,
                    data: {
                        product_id: id
                    },
                    success: function (data) {
                        $("#list_video_processing").html(data);
                    }
                });
            }
        });
    }
    this.gen_video_first = function (id) {
        $.ajax({
            type: 'GET',
            url: '/film_product_processing_video/list_by_product_id/' + id,
            data: {
                product_id: id
            },
            success: function (data) {
                $("#list_video_processing").html(data);
            }
        });
    }
}