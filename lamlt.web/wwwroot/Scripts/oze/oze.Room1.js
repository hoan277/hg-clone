﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}

function editDialog(id) {
    //debugger;
    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        $("#myModal .modal-body-content").load("/Room1/Edit/" + id);
        $("#myModal button#btnSave").css("display", "inline");
        $("#btnSave").off("click");
        $("#btnSave").click(function () {
            setupValidate();
            if (!$("#dummyRoom").valid()) return;
            var pdata = getFormData($("#dummyRoom"));
            showDialogLoading();

            $.post("/Room1/update", { obj: pdata }, function (data) {
                hideDialogLoading();
                //closeDlgLoadingData();
                if (data.result > 0) {
                    bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchRoom(); });
                }
                else {
                    alert("Có lỗi khi cập nhật phòng");
                }
            });
        });
    });
    $("#myModal").modal("show");
}
function deleteDialog(id) {
    if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
        if (!result) return;
        $.post("/Room1/delete", { id: id }, function (data) {
            if (data.result > 0) {
                bootboxLamlt.alert("Thao tác thành công", function () { searchRoom(); });
    } else {
                if (data.result == -1) {
                    bootbox.alert(data.mess);
    } else
                    alert("Có lỗi khi xóa Phòng:" + data.mess);
    }
    });
    }));
}
function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/Room1/GetDetail/" + id);
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}
function setupValidate() {
    var form = $('#dummyRoom').on();
    form.validate({
        rules: {
            Name : {
                required: true
            },
            RoomType_ID: {
                required: true
            }
        },
        messages: {
            Name: {
                required: 'Vui lòng nhập tên phòng'
            },
            RoomType_ID: {
                required : 'Vui lòng chọn hàng phòng'
            }
        }
    });
}