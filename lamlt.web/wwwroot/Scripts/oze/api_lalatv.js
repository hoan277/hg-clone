﻿function api_lalatv() {
    var self = this;
    this.tableName = "api_lalatv";
    this.idModal = "modalapi_lalatv";
    this.$table;
    // total : 14 function
    this.editDialog = function (id) {

        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>Đang tải..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                // console.log(pdata);
                $.post("/api_lalatv/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {

                        /*debugger;
                        console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                        imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                        //console.log($("input[type='checkbox'][name='showhome']").val());
                        //console.log(imgs)
                        self.UpdateCheckbox(data.result, imgs);
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
				/*
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data)
					{
						hideDialogLoading();
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});*/
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    // updateEdit view edit
    this.updateEdit = function (id) {
        if (!$("#form" + self.tableName).valid()) return;
        var pdata = getFormData($("#form" + self.tableName));
        showDialogLoading();
        // console.log(pdata);
        $.post("/api_lalatv/update", { obj: pdata }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {

                /*debugger;
                console.log($("input[type='checkbox'][name='catalog_cb[]']").val());*/
                imgs = $("input[type='checkbox'][name='catalog_cb[]']:checked").map(function () { return this.value; }).get();
                //console.log(imgs)
                self.UpdateCheckbox(data.result, imgs);
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); });
            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });
    }
    //  chuyển trạng thái status rồi update trạng thái
    this.updateStatus = function (id, status) {
        // debugger;
        $.post("/api_lalatv/updatestatus", {
            obj: { id: id, status: status }
        }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {
                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            }
            else {
                alert("Có lỗi khi tạo :" + data.mess);
            }
        });

    }
    // update anh trong bang film_video_image
    this.UpdateImage = function (url, filmid) {
        //debugger;
        $.post("/api_lalatv/UpdateImage", {
            //obj: { id: id, url: url }
            url: url, filmid: filmid
        }, function (data) {
            hideDialogLoading();
            self.getImageupdate(filmid);
            //if (data.result > 0) {
            //    bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            //}
            //else {
            //    alert("Có lỗi khi tạo :" + data.mess);
            //}
        });
    }
    // select hinh anh theo filmid trong ban film_video_image
    this.getImageupdate = function (filmid) {
        $.get("/api_lalatv/getImageupdate?id=" + filmid, function (data) {
            hideDialogLoading();
            $("#list_image").html("");
            for (var index in data.result) {
                if (data.result[index].url != undefined) {
                    $("#list_image").append("<div style= 'width:20%;display:inline-grid;margin:5px'><img src='" + data.result[index].url + "' width=100px/><a href='javascript:new film_video().delimage(" + data.result[index].id + "," + data.result[index].filmid + ")'>'<img src='/images/icon/icon-delete.png' style='border: none;' title='Xóa'></a>'</div>");
                }
            }
        });
    }
    // xóa hình ảnh trong film_video_image theo id
    this.delimage = function (id, filmid) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/deteleimage/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { self.getImageupdate(filmid); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    // checkbox
    this.UpdateCheckbox = function (filmid, catalog_id) {

        $.post("/api_lalatv/UpdateCheckbox", {
            //obj: { catalog_id: catalog_id, filmid: film_catalog_film.filmid }
            catalog_id: catalog_id, filmid: filmid
        }, function (data) {
        });
    }
    this.edits = function (id) {
        window.location.href = "/" + self.tableName + "/Edits/" + id;
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.exportData = function exportData() {
        $.ajax({
            url: "/" + self.tableName + "/export/",
            success: function (data) {
                $("#dvjson").excelexportjs({
                    containerid: "dvjson",
                    datatype: 'json',
                    dataset: JSON.parse(data.result),
                    worksheetName: "Sheet 1",
                    columns: getColumns(JSON.parse(data.result))
                });
            }
        });
    }
    var data_null = "<strong style='color:red'>Chưa cập nhật</strong>";
    this.setupGrid = function () {
        showDialogLoading();
        var stt = 1;
        self.$table = $('#example_api_lalatv').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "dom": 'Bfrtip',
            "buttons": ['excel', 'pdf'],
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "fixedHeader": { "header": true },
            "scrollX": true,
            "rowId": "id",
            "dom": '<"top"i>rt<"bottom"flp><"clear">',
            "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_video_title").val();
                    d.cpid = $("#search_film_video_cpid").val();
                    d.status = $("#search_film_video_status").val();
                    d.catalogid = $("#search_film_video_catalogid").val();
                    if (d.publish_year != "") { d.action_type = "search"; d.publish_year = $("#search_film_video_year").val(); }
                    if (d.publish_countryid != "") { d.action_type = "search"; d.publish_countryid = $("#publish_countryid").val(); }
                    if (d.film_type != "") { d.action_type = "search"; d.film_type = $("#film_type").val(); }
                    if (d.search != "") { d.action_type = "search"; }
                    stt = 1;
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = "";
                            var info = '<a style="color: #ffc107; font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#ffc107" title="Xem"></span>' +
                                '</a> ';
                            var edit = '<a href="javascript:void(0)" style="color: #28a745; font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#28a745" title="Sửa"></span>' +
                                '</a> ';
                            var delete1 = '<a href="javascript:void(0)" style="color: #dc3545;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:#dc3545" title="Xóa"></span>' +
                                '</a> ';
                            var active = '<a href="javascript:void(0)" style="color: #007bff;font-weight: bold;" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#007bff" title="Duyệt"></span>' +
                                '</a> ';
                            htmlMenu = info + edit + delete1;
                            if (row.status != 1) {
                                htmlMenu = htmlMenu + active;
                            }
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'title' }
                    , { 'data': 'desc' }
                    , { 'data': 'link' }
                    , { 'data': 'method' }
                    , { 'data': 'param' }
                    , { 'data': 'param_type' }
                    , { 'data': 'result' }
                    , { 'data': 'note' }
                    , {
                        'data': null, render: function (data, type, row) {
                            return (row.status == 1) ? "Đã duyệt" : "Chưa duyệt";
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }

}
