﻿function encodeHTML(str) {
    str = str.replace(/¢/g, "&cent;");
    str = str.replace(/¥/g, "&yen;");
    str = str.replace(/£/g, "&pound;");
    str = str.replace(/€/g, "&euro;");
    str = str.replace(/©/g, "&copy;");
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
}
function formatTime(seconds) {
    return [
        parseInt(seconds / 60 / 60),
        parseInt(seconds / 60 % 60),
        parseInt(seconds % 60)
    ]
        .join(":")
        .replace(/\b(\d)\b/g, "0$1")
}
function has_any_spaces(str) {
    if (/^\S+$/g.test(str) || str === '') {
        return false;
    }
    return true;
}
function film_video_processing_transcript() {
    var self = this;
    this.tableName = "film_video_processing_transcript";
    this.idModal = "modalfilm_video_processing_transcript";
    this.$table;
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var transcript_id = $("#transcript_id").val();
                //console.log(ownership);
                var pdata = getFormData($("#form" + self.tableName));
                pdata.video_processing_id = $("#Id").val();
                pdata.transcript_id = transcript_id;
                //console.log(pdata);
                showDialogLoading();
                $.post("/film_video_processing_transcript/UpdateByFilm", { obj: pdata, array_transcript: String(transcript_id) }, function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo : Đã tồn tại quyền");
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.addDialog = function (video_processing_id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + video_processing_id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var transcript_id = $("#transcript_id").val();
                //console.log(ownership);
                var pdata = getFormData($("#form" + self.tableName));
                pdata.video_processing_id = $("#Id").val();
                pdata.transcript_id = transcript_id;
                //console.log(pdata);
                showDialogLoading();
                $.post("/film_video_processing_transcript/UpdateByFilm", { obj: pdata, array_transcript: String(transcript_id) }, function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo : Đã tồn tại quyền");
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.convertDialog = function convertDialog(code) {
        var id = code.split("_")[0];
        var lang = code.split("_")[1];
        $.ajax({
            type: 'POST',
            url: "/film_video_processing/Convert_Sub",
            data: {
                list_selected: id,
                from: lang
            },
            success: function (data) {
                alert(data);
            },
            error: function (xhr) {
                console.log(xhr.responseText);
            },
        });
    }
    this.live_editDialog = function live_editDialog(id) {
        localStorage.setItem("live_edit", true);
        $("#srt").hide();
        $("#live_edit").show();
        let selected_sub = $("tr#" + id + " td textarea").val();
        let file_live_edit = $("tr#" + id + " td:nth-child(3) a").html();
        $("#file_live_edit").val(file_live_edit);
        $("#live_edit").val(selected_sub);
        var dulieu_srt = "";
        if ($("#live_edit").val() != "") {
            var datas = $("#live_edit").val().split("\n\n");
            datas.forEach(function (item) {
                if (item != "") {
                    let line = item.split("\n");
                    let time = line[1].split("-->");
                    let batdau = time[0];
                    let ketthuc = time[1];
                    let stt = line[0];
                    let content = line[2];
                    if (has_any_spaces(stt)) { stt = stt.trim(); }
                    if (has_any_spaces(content)) { content = content.trim(); }
                    if (has_any_spaces(batdau)) { batdau = batdau.trim(); }
                    if (has_any_spaces(ketthuc)) { ketthuc = ketthuc.trim(); }
                    dulieu_srt += "<tr class='d-flex " + batdau.replace(",000", "").replace(/:/g, "_") + "' id='" + stt + "'><td class='col-xs-1'><a id='" + stt + "'><i class='fa fa-plus'></i></a><input type='text' class='stt form-control' value='" + stt + "'></td><td class='col-xs-2'><input type='text' class='form-control' value='" + batdau + "'></td><td class='col-xs-2'><input type='text' class='form-control' value='" + ketthuc + "'></td><td class='col-xs-7'><input type='text' class='form-control' tabindex='" + stt + "' value='" + encodeHTML(content) + "'></td></tr>";
                }
            });
            var currentText;
            var nextThis;
            $("#tblSrt > tbody").html("");
            $("#tblSrt > tbody").append(dulieu_srt);
            $("#tblSrt tr td.col-xs-1 a").click(function () {
                $("tr#" + $(this).attr("id")).after("<tr class='d-flex newtr' id=''><td class='col-xs-1'><a id=''></a><input type='text' class='stt form-control' value=''></td><td class='col-xs-2'><input type='text' class='form-control' value=''></td><td class='col-xs-2'><input type='text' class='form-control' value=''></td><td class='col-xs-7'><input type='text' class='form-control'></td></tr>");
            });
            $("tr").click(function () {
                currentText = $(this).children("td:nth-child(4)").children("input");
                $("tbody tr").removeClass("active");
                $(this).addClass("active");
                nextThis = $(this).attr("id");
                let batdau = $(this).children("td:nth-child(2)").children("input").val();
                let noidung = $(this).children("td:nth-child(4)").children("input").val();
                $("#preview_sub").html(noidung);
                if (batdau) {
                    document.getElementById("editvideo").currentTime = srtToSeconds(batdau);
                }
            });
            $("input").keypress(function (e) {
                if (e.keyCode == 9) {
                    $("tbody tr").removeClass("active");
                    $(this).addClass("active");
                }
                let noidung = $('tr.active').children("td:nth-child(4)").children("input").val();
                $("#preview_sub").html(noidung);
            });
            $("input").focusout(function (e) {
                let noidung = $('tr.active').children("td:nth-child(4)").children("input").val();
                $("#preview_sub").html(noidung);
            });
            $("#btnRebuild").hide();
            $("#btnRebuildLive").show();
            $("#btnRebuildLive").attr("data-id", id);
        }
    }
    this.setupGrid = function () {
        var video_processing_id = $("#video_processing_id").val();
        self.$table = $('#example_film_video_processing_transcript').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            //"processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
            },
            "dom": 'lrtip',
            "ajax": {
                "url": "/film_video_processing_transcript/getListByvideo_processing_id/" + video_processing_id,
                "data": function (d) {
                    d.search = $("#search_film_video_processing_transcript_title").val();
                    delete d.columns;
                }
            },
            "paging": false,
            "rowId": "id",
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = "1";
                            let code = row.transcript_code;
                            var convert = '<a style="color: #03a9f4;font-weight: bold;" href="javascript:void(0)" onclick="javascript:ofilm_video_processing_transcript' + '.convertDialog(\'' + code + '\')">' +
                                '<span class="glyphicon glyphicon-subtitles" aria-hidden="true" style="color:#03a9f4" title="Add Hard Sub"></span>' + ' Hard sub' +
                                '</a>  <br />';
                            var live_edit = '<a style="color: #673AB7;font-weight: bold;" href="javascript:void(0)" onclick="javascript:ofilm_video_processing_transcript' + '.live_editDialog(\'' + row.id + '\')">' +
                                '<span class="glyphicon glyphicon-flash" aria-hidden="true" style="color:#673AB7" title="Sửa trực tiếp"></span>' + ' Sửa trực tiếp' +
                                '</a>  <br />';
                            var info = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:ofilm_transcript' + '.editDialog(' + row.transcript_id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Sửa sub"></span>' + ' Sửa sub' +
                                '</a>  <br />';
                            var edit = '<a href="javascript:void(0)" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + 'film_video_processing_transcript' + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Thêm sub"></span>' + ' Thêm sub ' +
                                '</a>  <br />';
                            var delete1 = '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + 'film_video_processing_transcript' + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' + ' Xoá' +
                                '</a>  <br />';
                            var active = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + 'film_video_processing_transcript' + '.activeDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-check" aria-hidden="true" style="color:#8BC34A" title="Duyệt"></span>' + ' Duyệt' +
                                '</a>  ';
                            htmlMenu = live_edit + convert + info + edit + delete1;
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'transcript_title' }
                    //, {
                    //    'data': null, render: function (row) {
                    //        return '<a target="_blank" href="' + row.transcript_file + '">' + row.transcript_file + '</a>';
                    //    }
                    //}
                    , {
                        'data': null, render: function (row) {
                            return '<a target="_blank" href="' + row.file_thanh_pham + '">' + row.file_thanh_pham + '</a>';
                        }
                    }
                    , { 'data': 'status' }
                    , {
                        'data': null, render: function (row) {
                            return '<textarea class="form-control" style="width:100%">' + row.transcript_desc + '</textarea>';
                        }
                    }

                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.activeDialog = function activeDialog(id) {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/active/" + id,
            success: function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    $("#" + self.idModal).modal("hide"); self.searchGrid();
                }
                else {
                    alert("Có lỗi khi xử lý :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
}