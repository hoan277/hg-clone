﻿function film_product_processing() {
    var self = this;
    this.tableName = "film_product_processing";
    this.idModal = "modalfilm_product_processing";
    this.$table;
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/film_product_processing/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.addDialog = function (product_id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/0");
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                //var ownership = $("#ownership_id").val();
                //console.log(ownership);
                var pdata = getFormData($("#form" + self.tableName));
                pdata.product_id = $("#productid").val();
                console.log(pdata);
                //pdata.ownership_id = ownership;
                //console.log(pdata);
                showDialogLoading();
                $.post("/film_product_processing/UpdateByProduct", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        //alert("ok");
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        //console.log(data);
                        alert("Có lỗi khi tạo : Đã tồn tại quyền");
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    var data_null = "<strong style='color:red'>Chưa cập nhật</strong>";
    this.setupGrid = function () {
        var productid = $("#productid").val();
        console.log(productid);
        showDialogLoading();
        self.$table = $('#example_film_product_processing').DataTable({
            fixedHeader: {
                header: true
                //footer: true
            },
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "rowId": "id",
            "ajax": {
                "url": "/" + "film_product_processing" + "/getListByProductId/" + productid,
                "data": function (d) {
                    delete d.columns;
                    //d.search = String($("#s_title").val());
                    //d.time_range = String($('#s_timerange').val());
                    //stt = 1;
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = "";
                            var info = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                                '</a>  ';
                            var edit = '<a href="javascript:void(0)" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                                '</a>  ';
                            var delete1 = '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                '</a>  ';
                            var active = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-check" aria-hidden="true" style="color:#8BC34A" title="Duyệt"></span>' +
                                '</a>  ';
                            htmlMenu = info + edit + delete1;
                            return htmlMenu;
                        }
                    }
                    ,{ 'data': 'catalog_title' }
                    ,{ 'data': 'catalog_time' }
                    ,{ 'data': 'catalog_position' }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '<hr/>';
        html += '       <button type="button" id="btnSave' + self.tableName + '" class="btn btn-warning" >Lưu</button> <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.activeDialog = function activeDialog(id) {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/active/" + id,
            success: function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    $("#" + self.idModal).modal("hide"); self.searchGrid();
                }
                else {
                    alert("Có lỗi khi xử lý :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
    //================================================= xuất excel ======================================================================
    this.exportAll = function exportAll() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "tatca",

                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    this.exportList = function exportList() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "dachon",
                list_id: selected_id.toString(),
                search: String($("#s_title").val()),

                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    //================================================= xuất excel ======================================================================

}