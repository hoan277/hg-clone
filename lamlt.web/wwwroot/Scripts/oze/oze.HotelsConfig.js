﻿$(document).ready(function () {
    Sv.SetupInputMask();
    $("#btnSave").click(function() {
        var pdata = getFormData($("#formData"));
        if ($("#sysFuncType").length > 0) {
            var v = $("#sysFuncType").val(); 
            pdata.sysFuncType = v ? v.join(';') : "";
        }
        if ($("#roompricetype_hide").length > 0) {
            var v = $("#roompricetype_hide").val();
            pdata.roompricetype_hide = v ? v.join(';') : "";
        }
        

        Sv.Post({
            url: "/HotelsConfig/UpdateOrInsert",
            data: { obj: pdata }
        }, function (data) {
            if (data.result > 0) {
                bootbox.alert("Lưu cấu hình thành công", function () { });
            }
            else {
                alert("lỗi");
            }
        });
    });
});


