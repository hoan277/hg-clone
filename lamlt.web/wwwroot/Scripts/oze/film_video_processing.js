﻿$(document).ready(function () {
    $(document).on({
        click: function () {
            $('#modalfilm_video_processing').remove();
            $('.modal-backdrop').remove();
        }
    }, ".close");
    $(document).on({
        mouseenter: function () {
            $('#modalfilm_video_processing').remove();
            $('.modal-backdrop').remove();
        }
    }, "#example_film_video_processing");
});
function film_video_processing() {
    var self = this;
    this.tableName = "film_video_processing";
    this.idModal = "modalfilm_video_processing";
    this.$table;
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                $.get("/film_video_processing/get?title=" + $('#title').val() + "&code=" + $('#code').val(), function (data) {
                    if (data.length > 0) {
                        alert("Dữ liệu tên/mã code đã tồn tại vui lòng sử dụng tên/mã code khác");
                    } else {
                        var pdata = getFormData($("#form" + self.tableName));
                        showDialogLoading();
                        $.post("/film_video_processing/update", { obj: pdata }, function (data) {
                            hideDialogLoading();
                            //closeDlgLoadingData();
                            if (data.result > 0) {
                                bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                            }
                            else {
                                alert("Có lỗi khi tạo :" + data.mess);
                            }
                        });
                    }
                })
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.editsDialog = function editsDialog(id) {
        window.location.href = "/" + self.tableName + "/Edits/" + id;
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    var data_null = "<strong style='color:red'>Chưa cập nhật</strong>";

    const getURLParameters = url => (url.match(/([^?=&]+)(=([^&]*))/g) || []).reduce((a, v) => ((a[v.slice(0, v.indexOf('='))] = v.slice(v.indexOf('=') + 1)), a), {});
    let search_video_type = "";
    let video_type = getURLParameters(window.location.href);
    if (video_type) {
        switch (video_type.video_type) {
            case "film":
                search_video_type = "";
                break;
            case "other":
                search_video_type = "other";
                break;
            case "denoiser":
                search_video_type = "denoiser";
                break;
        }
    }

    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_video_processing').DataTable({
            fixedHeader: {
                header: true
                //footer: true
            },
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": false,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top"<"clear">>rt<"bottom"lip<"clear">>',
            "rowId": "id",
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.length = 50;
                    d.search = String($("#s_title").val());
                    d.time_range = String($('#s_timerange').val());
                    stt = 1;
                    d.search_video_type = search_video_type;
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = "";
                            var info = '<a style="color: #03A9F4;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#03A9F4" title="Xem"></span>' +
                                ' Thông tin</a><br />';
                            var edit = '<a href="javascript:void(0)" style="color: #8BC34A;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editsDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#8BC34A" title="Chi tiết"></span>' +
                                ' Chi tiết</a><br />';
                            var delete1 = '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                ' Xóa</a>';
                            var active = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-check" aria-hidden="true" style="color:#8BC34A" title="Duyệt"></span>' +
                                '</a>  '
                            
                            htmlMenu = info + edit + delete1 ;
                            if (row.status != 1) {
                                htmlMenu = info + edit + delete1 ;
                            }
                            return htmlMenu;
                        }
                    }
                    , {
                        'data': null, render: function (data, type, row) {
                            var check = 0;
                            var rs = "";
                            rs += check == 0 ? "{'STT':'" + stt + "'," : ",{'STT':'" + stt + "',";
                            rs += "'upload_file':'" +  + "'}";
                            check++;
                            stt++;
                            var check_box = '<div class="checkbox"><label><input class="export_selected" type="checkbox" id="export_selected" name="export_selected" type="checkbox" value="' + row.upload_file + '">' + row.id + '</label></div >';
                            var html1 = check_box;
                            return html1;
                        }
                    }
                    , { 'data': 'title' }
                    , { 'data': 'code' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.desc == null) return "";
                            if (row.desc.length <= 20) return row.desc;
                            return row.desc.substr(0, 19) + "...";
                        }
                    }
                    , { 'data': 'status' }
                    //, { 'data': 'upload_file' }
                    //, {
                    //    'data': null, render: function (row) {
                    //        var upload_file = "";
                    //        if (row.upload_file != null) {
                    //            upload_file = row.upload_file.replace("https://files.lalatv.com.vn:9090/", "http://27.76.152.255:13000/");
                    //        }
                    //        var info = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                    //            '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span> Xem</a>  ';
                    //        return info;
                    //    }
                    //}
                    //, { 'data': 'catalog_title' }
                    //, { 'data': 'duration' }
                    //, {
                    //    'data': null, render: function (row) {
                    //        var datecreated = "";
                    //        if (row.datecreated != null) {
                    //            datecreated = moment(row.datecreated).format('DD/MM/YYYY HH:mm:ss');
                    //        }
                    //        return datecreated;
                    //    }
                    //}
                    //, {
                    //    'data': null, render: function (row) {
                    //        var dateupdated = "";
                    //        if (row.dateupdated != null) {
                    //            dateupdated = moment(row.dateupdated).format('DD/MM/YYYY HH:mm:ss');
                    //        }
                    //        return dateupdated;
                    //    }
                    //}
                   
                ]
        });
        
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '<hr/>';
        html += '       <button type="button" id="btnSave' + self.tableName + '" class="btn btn-warning" >Lưu</button> <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#modalfilm_video_processing').remove();
    }
    this.activeDialog = function activeDialog(id) {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/UpdateStatus/" + id,
            success: function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    $("#" + self.idModal).modal("hide"); self.searchGrid();
                }
                else {
                    alert("Có lỗi khi xử lý :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
    //================================================= xuất excel ======================================================================
    this.exportAll = function exportAll() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "tatca",
                search: String($("#s_title").val()),
                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    this.exportList = function exportList() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "dachon",
                list_id: selected_id.toString(),
                search: String($("#s_title").val()),
                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    //================================================= xuất excel ======================================================================

    //================================================= nhập excel ======================================================================
    this.importExcel = function importExcel() {
        var fileExtension = ['xls', 'xlsx'];
        var filename = $('#import_file').val();
        if (filename.length == 0) {
            alert("Vui lòng chọn file tải lên.");
            return false;
        }
        else {
            var extension = filename.replace(/^.*\./, '');
            if ($.inArray(extension, fileExtension) == -1) {
                alert("Vui lòng chọn file có định dạng .xls, xlsx");
                return false;
            }
        }
        var fdata = new FormData();
        var fileUpload = $("#import_file").get(0);
        var files = fileUpload.files;
        fdata.append(files[0].name, files[0]);
        console.log(fdata);
        $.ajax({
            type: "POST",
            url: "/" + self.tableName + "/importExcel",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: fdata,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.length == 0)
                    alert('Có lỗi trong quá trình upload');
                else {
                    alert('Upload thành công');
                    oreport.searchGrid();
                    console.log(response);
                }
            },
            error: function (e) {
                alert('Vui lòng reload lại trang');
            }
        });
    }
    //================================================= nhập excel ======================================================================
    // updateEdit view edit
    this.updateEdit = function (id) {
        if (!$("#form" + self.tableName).valid()) return;
        $.get("/film_video_processing/get?title=" + $('#title').val() + "&code=" + $('#code').val(), function (data) {
            if (data.length > 0) {
                alert("Dữ liệu tên/mã code đã tồn tại vui lòng sử dụng tên/mã code khác");
            } else {
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $.post("/film_video_processing/Update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            }
        })
        
    }
    this.upload_file = function upload_file() {
        $(".progress").show();
        var fd = new FormData();
        var files = $('#upload_file1')[0].files[0];
        fd.append('file', files);
        $.ajax({
            url: "/" + self.tableName + "/upload_file",
            data: fd,
            contentType: false,
            processData: false,
            type: 'post',
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var progress = Math.round((evt.loaded / evt.total) * 100);
                    }
                    $("#progressbar-5").css("width", progress + "%").attr("aria-valuenow", progress);
                    $("#progressbar-5").html(progress + "%");
                }, false);
                return xhr;
            },
            success: function (data) {
                $("#upload_file").val(data.file);
                $("#upload_file").select();
                $("#progressbar-5").css("width", "100%").attr("aria-valuenow", 100);
                $("#progressbar-5").addClass("bg-success");
                $("#progressbar-5").html('<i class="fa fa-check"></i> Đã upload thành công');
            },
            error: function (data) {
                alert("Có lỗi khi upload");
            }
        });
    }
    this.LoadProgressBar = function LoadProgressBar(result) {
        var progressbar = $("#progressbar-5");
        var progressLabel = $(".progress-label");
        progressbar.show();
        $("#progressbar-5").progressbar({
            //value: false,
            change: function () {
                progressLabel.text(
                    progressbar.progressbar("value") + "%");  // Showing the progress increment value in progress bar
            },
            complete: function () {
                progressLabel.text("Loading Completed!");
                progressbar.progressbar("value", 0);  //Reinitialize the progress bar value 0
                progressLabel.text("");
                progressbar.hide(); //Hiding the progress bar
                var markup = "<tr><td>" + result + "</td><td><a href='#' onclick='DeleteFile(\"" + result + "\")'><span class='glyphicon glyphicon-remove red'></span></a></td></tr>"; // Binding the file name
                $("#ListofFiles tbody").append(markup);
                $('#Files').val('');
                $('#FileBrowse').find("*").prop("disabled", false);
            }
        });
        function progress() {
            var val = progressbar.progressbar("value") || 0;
            progressbar.progressbar("value", val + 1);
            if (val < 99) {
                setTimeout(progress, 25);
            }
        }
        setTimeout(progress, 100);
    }
    this.get_transcript = function get_pro(id) {
        $.ajax({
            type: 'GET',
            url: '/film_video_processing_transcript/list_by_video/' + id,
            data: {
                video_processing_id: id
            },
            success: function (data) {
                $(".list_video_processing_transript").html(data);
            }
        });
    }
}