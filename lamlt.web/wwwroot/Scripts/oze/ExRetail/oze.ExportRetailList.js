﻿
var $table;

$(document)
    .ready(function () {
        reportDate();
        setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

        function searchGrid() {
            showDialogLoading();
            if ($table)
                $table.destroy();
            $table = $("#example")
            .DataTable({
                fixedHeader: {
                    header: true,
                    footer: true
                },
                language: {
                    "sProcessing": "Đang xử lý...",
                    "sLengthMenu": "Xem _MENU_ mục",
                    "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                    "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                    "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                    "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                    "sInfoPostFix": "",
                    "sSearch": "Tìm:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Đầu",
                        "sPrevious": "Trước",
                        "sNext": "Tiếp",
                        "sLast": "Cuối"
                    }
                },
                "processing": true,
                "serverSide": true,
                "dom": '<"top">rt<"bottom" lpi><"clear">',
                "ajax": {
                    "url": "/ExportRetail/ListData",
                    "data": function (d) {
                        d.search = "";
                        d.FromDate = $("input[name='FromDate']").val();
                        d.ToDate = $("[name='ToDate']").val();
                        d.columns = "";
                    }
                },
                "initComplete": function (settings, json) {
                    hideDialogLoading();
                },
                "columns":
                [
                    {
                        "data": null,
                        render: function (data, type, row, infor) {
                            return $table.page.info().page + infor.row + 1;
                        }
                    },
                      {
                          "data": "ordercode",
                      },

                      {
                          "data": "CreatorName",
                      },
                      {
                          "data": "CustomerName",
                      },

                      {
                          "data": "CustomerPhone",
                      },

                      {
                          "data": null,
                          render: function (data, type, row, infor) {
                              return Sv.DateToString(row.DateCreated, "DD/MM/YYYY HH:mm");
                          }
                      },

                      {
                          "data": null,
                          render: function (data, type, row, infor) {
                              return Sv.NumberToString(row.TotalAmount);
                          }
                      },


                      {
                          "data": null,
                          render: function (data, type, row, infor) {
                              var htmlMenu = ('<div class="input-table" onclick="javascript:viewOrderInfo(' + row.id + ')">' +
                                          '<a  class="btn btn-primary btn-flat" title="Chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                      '</div>');
                              if (!isLT()) {
                                  htmlMenu += ('<div class="input-table" onclick="javascript:removeOrder(' + row.id + ',' + row.TotalAmount + ')">' +
                                         '<a  class="btn btn-danger btn-flat" title="Xóa" ><i class="fa fa-times"></i></a>' +
                                     '</div>');
                              }
                              return htmlMenu;
                          }
                      },

                ],
                "footerCallback": function (row, data, start, end, display) {
                }
            });

        }
        function exportExcel() {
            window.open("/Report/ExportExcelDoanhThu");
            //$.ajax({
            //    url: "/Report/ExportExcelDoanhThu", success: function (result) {    	            
            //    }
            //});
        }
        $("#btnSearch")
            .click(function () {
                searchGrid();
            });

        $("#btnRefresh")
        .click(function () {
            rsdata();
            searchGrid();
        });

        $("#keyword").keypress(function (e) {
            if (e.which == 13) {
                searchGrid();
            }
        });


    });

function viewOrderInfo(id) {
    Sv.SetupModal({
        modalId: "ExportRetail_viewOrderInfo",
        title: "Chi tiết",
        url: "/ExportRetail/Details",
        data: { id: id },
        modalclass: "modal-lg",
        button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
    }, function () {
    }, function () {
    }, function () {
    });
}

function removeOrder(id, total) {
    confirm("Bạn chắc chắn muốn xóa ghi nhận bán lẻ này không?<br/> Tổng giá trị: " + Sv.NumberToString(total), function (rs) {
        if (!rs) return;
        Sv.AjaxPost({
            url: "/ExportRetail/Delete",
            data: { id: id },
        }, function (e) {       
            bootboxLamlt.alert(e.Message, function () {
                if (e.Status === '1') {
                }
            });
        });
    })
}

function rsdata() {
    if (!daysView) daysView = -30;
    Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(daysView)));
    Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
}