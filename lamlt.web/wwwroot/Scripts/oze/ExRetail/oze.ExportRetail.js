﻿
var UnitCtrl = function () {
    var base = this;
    this.ArrayProduct = [];
    this.$table = $('#detailTbl');
    this.$countProduct = $('#countProduct');
    this.productSelect = $('#productList');
    this.$btnAddProduct = $('#btnAddProduct');
    this.$btnAddProduct2 = $('#btnAddProduct2');
    this.$detailPay = $('#deteilPay');
    this.PayLeft = 0;
    this.Surchage = 0;
    this.$totalAmount = $('#totalMoney');
    this.$surcharge = $('#surcharge');
    this.$totalAmountLeft = $('#totalAmountLeft');
    this.$totalAmountPay = $('#totalPay');
    this.$groupButton = $('#groupButton');
    this.AddToTable = function (obj) {
        var result = $.grep(base.ArrayProduct, function (e) { return e.Id == obj.Id; });
        if (result.length > 0) {
            var currentObj = result[0];
            base.PushToArray(currentObj, obj);
        } else {
            obj.Total = parseInt(obj.DPrice) * parseInt(obj.Count);
            base.ArrayProduct.push(obj);
            base.AppendTable(obj);
        }
        if (base.ArrayProduct.length === 0) {
            base.$table.slideUp().find('tbody').empty();
            //base.$detailPay.slideUp();
            base.$groupButton.hide();
        } else {
            base.$table.slideDown();
            //base.$detailPay.show();
            base.$groupButton.show();
        }
        base.CalPay();
    }
    this.AddToTable2 = function (obj) {
        var result = $.grep(base.ArrayProduct, function (e) { return e.DName.toLowerCase() == obj.DName.toLowerCase(); });
        if (result.length > 0) {
            var currentObj = result[0];
            base.PushToArray(currentObj, obj);
        } else {
            obj.Total = parseInt(obj.DPrice) * parseInt(obj.Count);
            base.ArrayProduct.push(obj);
            base.AppendTable(obj);
        }
        if (base.ArrayProduct.length === 0) {
            base.$table.slideUp().find('tbody').empty();
            //base.$detailPay.slideUp();
            base.$groupButton.hide();
        } else {
            base.$table.slideDown();
            //base.$detailPay.show();
            base.$groupButton.show();
        }
        base.CalPay();
    }
    this.$btnPrint = $('#btnPrint');
    this.SelectProduct = function () {
        var v = base.$countProduct.val();
        if (v.length == 0 || parseInt(v) <= 0) {
            alert("Vui lòng nhập số lượng");
            return;
        }
        var p = $("#total").val();
        if (p.length == 0 || parseFloat(p) <= 0) {
            alert("Vui lòng nhập đơn giá");
            return;
        }
        var option = base.productSelect.find('option:selected');
        //console.log(option);
        var dataSelect = {
            Id: option.attr('value'),
            DUnit: option.attr('dUnit'),
            DDescription: option.attr('dDescription') == undefined ? "" : option.attr('dDescription'),
            DPrice: $("#total").val(),
            DName: option.attr('dName'),
            Count: v,
            Total: 0
        };
        base.AddToTable(dataSelect);
    }
    this.SelectProduct2 = function () {
        var v = $("#product2").val();
        if (v.length == 0) {
            alert("Vui lòng nhập dịch vụ");
            return;
        }
        var p = $("#totalProduct2").val();
        if (p.length == 0 || parseFloat(p) <= 0) {
            alert("Vui lòng tổng tiền");
            return;
        }
        var dataSelect = {
            Id: (new Date()).getTime(),
            DUnit: "",
            DDescription: v,
            DPrice: p,
            DName: v,
            Count: 1,
            Total: 0
        };
        base.AddToTable2(dataSelect);

    }
    this.$groupExport = $('#groupExport');
    this.PushToArray = function (currentObj, obj) {
        var newCount = parseInt(obj.Count) + parseInt(currentObj.Count);
        var newObj = currentObj;
        newObj.Count = newCount;
        newObj.Total = parseInt(obj.DPrice) * newCount;
        var indexOfItem = base.ArrayProduct.indexOf(currentObj);
        base.ArrayProduct.splice(indexOfItem, 1);
        base.ArrayProduct.push(newObj);
        base.UpdateRow(newObj);
    }
    this.$ErCode = $('#txtCodeEr');
    this.$CustomerName = $('#txtCustomerName');
    this.$CustomerPhone = $('#txtPhoneNumber');
    this.$Time = $('#txtDate');
    this.$btnSubmit = $('#btnSubmit');
    this.AppendTable = function (obj) {
        var rowIndex = base.$table.find('tbody tr').length + 1;
        var html = "<tr pid=" + obj.Id + ">" +
            "<td class='rIndex'>" + rowIndex + "</td>" +
            "<td>" + obj.DUnit + "</td>" +
            "<td>" + obj.DName + "</td>" +
            "<td class='dPrice'>" + base.FormatCurency(parseInt(obj.DPrice)) + "</td>" +
            "<td class='dCount'>" + obj.Count + "</td>" +
            "<td class='dTotal'>" + base.FormatCurency(parseInt(obj.Total)) + "</td>" +
            "<td class='dbutton'>" + "<button class='btn btn-sm btn-danger' onclick='unit.removeProduct(" + obj.Id + ")'>Xóa</button>" + "</td>" +
            "</tr>";
        base.$table.find('tbody').append(html);
    }
    this.removeProduct = function (id) {
        base.$table.find('tbody tr[pid=' + id + ']').remove();
        base.ArrayProduct = base.ArrayProduct.filter(function (obj) {
            return obj.Id != id;
        });
    }

    this.Error = "error";
    this.$notifyDiv = $('.notify-popup');
    this.UpdateRow = function (obj) {
        var row = base.$table.find('tr[pid=' + obj.Id + ']');
        row.find('.dCount').text(obj.Count);
        row.find('.dTotal').text(base.FormatCurency(parseInt(obj.Total)));
    }
    this.$btnSave = $('#btnSave');
    this.$btnPay = $('#btnPay');
    this.CalPay = function () {
        var totalAmount = 0;
        for (var i = 0; i < base.ArrayProduct.length; i++) {
            totalAmount += parseInt(base.ArrayProduct[i].Total);
        }
        var totalPay = (totalAmount + base.Surchage) - base.PayLeft;
        base.$totalAmount.text(base.FormatCurency(totalAmount));
        base.$surcharge.text(base.Surchage);
        base.$totalAmountLeft.text(base.FormatCurency(base.PayLeft));
        base.$totalAmountPay.text(base.FormatCurency(totalPay));

    }
    this.$form = $('#boxCustomer');
    this.FormatCurency = function (num) {
        var p = num.toFixed(2).split(".");
        return p[0].split("").reverse().reduce(function (acc, num, i, orig) {
            return num == "-" ? acc : num + (i && !(i % 3) ? "." : "") + acc;
        }, "");
    }

    this.PrintPage = function (data) {
        $('.form-horizontal').hide();
        $('.rowProduct').hide();
        window.print();
        //window.close();
        $('.form-horizontal').show();
        $('.rowProduct').show();
        return true;
    }

    this.ExportWord = function () {
        var listToAction = [];
        var totalAmount = 0;
        for (var i = 0; i < base.ArrayProduct.length; i++) {
            totalAmount += parseInt(base.ArrayProduct[i].Total);
            var obj = {
                ProductId: base.ArrayProduct[i].Id,
                Unit: base.ArrayProduct[i].DUnit,
                Description: base.ArrayProduct[i].DDescription,
                Note: "",
                Quantity: base.ArrayProduct[i].Count,
                Price: base.ArrayProduct[i].DPrice,
                ProductName: base.ArrayProduct[i].DName
            }
            listToAction.push(obj);
        }
        var objData = {
            SurchargeAmount: 0,
            PayLeft: 0,
            TotalAmount: totalAmount,
            ListRetailDetail: listToAction,
            ErCode: base.$ErCode.val(),
            CustomerName: base.$CustomerName.val(),
            SPayTime: base.$Time.val(),
            CustomerPhone: base.$CustomerPhone.val()
        }
        $.ajax({
            url: "/ExportRetail/SaveSession",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(objData),
            beforeSend: function () {
                showDialogLoading('Đang xử lý ...');
            },
            complete: function () {
                hideDialogLoading();
            },
            success: function (base64) {
                base.$btnSubmit.click();
            }

        });
    }

    this.ExportPDF = function () {
        var listToAction = [];
        var totalAmount = 0;
        for (var i = 0; i < base.ArrayProduct.length; i++) {
            totalAmount += parseInt(base.ArrayProduct[i].Total);
            var obj = {
                ProductId: base.ArrayProduct[i].Id,
                Unit: base.ArrayProduct[i].DUnit,
                Description: base.ArrayProduct[i].DDescription,
                Note: "",
                Quantity: base.ArrayProduct[i].Count,
                Price: base.ArrayProduct[i].DPrice,
                ProductName: base.ArrayProduct[i].DName
            }
            listToAction.push(obj);
        }
        var objData = {
            SurchargeAmount: 0,
            PayLeft: 0,
            TotalAmount: totalAmount,
            ListRetailDetail: listToAction,
            ErCode: base.$ErCode.val(),
            CustomerName: base.$CustomerName.val(),
            SPayTime: base.$Time.val(),
            CustomerPhone: base.$CustomerPhone.val()
        }
        $.ajax({
            url: "/ExportRetail/ExportPdf",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(objData),
            beforeSend: function () {
                showDialogLoading('Đang xử lý ...');
            },
            complete: function () {
                hideDialogLoading();
            },
            success: function (base64) {
                var sampleArr = base64ToArrayBuffer(base64);
                saveByteArray("Export Retail Detail PDF", sampleArr, '.pdf');
            }

        });
    }

    this.SaveExRetail = function () {
        var listToAction = [];
        var totalAmount = 0;
        for (var i = 0; i < base.ArrayProduct.length; i++) {
            totalAmount += parseInt(base.ArrayProduct[i].Total);
            var obj = {
                ProductId: base.ArrayProduct[i].Id,
                Unit: base.ArrayProduct[i].DUnit,
                Description: base.ArrayProduct[i].DDescription,
                Note: "",
                Quantity: base.ArrayProduct[i].Count,
                Price: base.ArrayProduct[i].DPrice,
                ProductName: base.ArrayProduct[i].DName
            }
            listToAction.push(obj);
        };
        console.log(listToAction);
        var objData = {
            SurchargeAmount: 0,
            PayLeft: 0,
            TotalAmount: totalAmount,
            ListRetailDetail: listToAction,
            ErCode: base.$ErCode.val(),
            CustomerName: base.$CustomerName.val(),
            SPayTime: base.$Time.val(),
            CustomerPhone: base.$CustomerPhone.val()
        };
        Sv.AjaxPost({
            url: "/ExportRetail/SaveExRetail",
            data: { model: objData },
        }, function (e) {
            if (e.Status === '1') {
                bootboxLamlt.alert(e.Message, function () {
                    window.setTimeout(function () {
                        location.hash = '#/ExportRetail/List';
                        //location.reload();
                    }, 200);
                });
            } else {
                bootboxLamlt.alert(e.Message);
            }
        });
        //$.ajax({
        //    url: "/ExportRetail/SaveExRetail",
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    data: JSON.stringify(objData),
        //    beforeSend: function () {
        //        showDialogLoading('Đang xử lý ...');
        //    },
        //    complete: function () {
        //        hideDialogLoading();
        //    },
        //    success: function (e) {
        //        if (e.status === '00') {
        //            base.ShowMessage(e.message, '');
        //        } else {
        //            base.ShowMessage(e.message, base.Error);
        //        }
        //        window.setTimeout(function () {
        //            location.reload();
        //        }, 3000);
        //    }
        //});
    }

    this.PayExRetail = function () {
        var listToAction = [];
        var totalAmount = 0;
        for (var i = 0; i < base.ArrayProduct.length; i++) {
            totalAmount += parseInt(base.ArrayProduct[i].Total);
            var obj = {
                ProductId: base.ArrayProduct[i].Id,
                Unit: base.ArrayProduct[i].DUnit,
                Description: base.ArrayProduct[i].DDescription,
                Note: "",
                Quantity: base.ArrayProduct[i].Count,
                Price: base.ArrayProduct[i].DPrice,
                ProductName: base.ArrayProduct[i].DName
            }
            listToAction.push(obj);
        }
        var continute = listToAction.length > 0;
        if (continute) {
            var objData = {
                SurchargeAmount: 0,
                PayLeft: 0,
                TotalAmount: totalAmount,
                ListRetailDetail: listToAction,
                ErCode: base.$ErCode.val(),
                CustomerName: base.$CustomerName.val(),
                SPayTime: base.$Time.val(),
                CustomerPhone: base.$CustomerPhone.val()
            }
            $.ajax({
                url: "/ExportRetail/SaveExRetail",
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(objData),
                beforeSend: function () {
                    showDialogLoading('Đang xử lý ...');
                },
                complete: function () {
                    hideDialogLoading();
                },
                success: function (e) {
                    if (e.status === '00') {
                        base.ShowMessage(e.message, '');
                    } else {
                        base.ShowMessage(e.message, base.Error);
                    }
                    window.setTimeout(function () {
                        location.reload();
                    }, 3000);
                }
            });
        } else {
            base.ShowMessage('Bạn chưa chọn sản phẩm', base.Error);
        }

    }

    this.ShowMessage = function (str, type) {
        var strClass = "alert-success";
        if (type === "error")
            strClass = "alert-danger";
        var html = "<div style='display:none' class=\"alert " + strClass + "\">" +
            "" + str +
            "</div>";
        base.$notifyDiv.empty().append(html);
        base.$notifyDiv.find('.alert').show(5000).slideUp();
    }

    this.SetUpValidate = function () {
        var $form = $("#boxCustomer");
        $form.validate({
            rules: {
                txtCodeEr: {
                    required: true
                },
                txtCustomerName: {
                    required: true
                },
                //txtPhoneNumber: {
                //    required: true
                //},
                txtDate: {
                    required: true
                }
            },
            messages: {
                txtCodeEr: {
                    required: "Bạn chưa nhập mã hoá đơn"
                },
                txtCustomerName: {
                    required: "Bạn chưa nhập tên khách hàng"
                }
                ,
                //txtPhoneNumber: {
                //    required: "Bạn chưa nhập SĐT khách hàng"
                //},
                txtDate: {
                    required: "Bạn chưa nhập ngày thanh toán"
                }
            }
        });
    }
}
var unit = null;
$(document).ready(function () {
    toggleMenu("menu_letan");
    Sv.SetupDatePicker([{ e: $('#input-datetime'), format: "DD/MM/YYYY HH:mm", defaultDate: moment(new Date()) }]);
    $("#productList").select2();
    unit = new UnitCtrl();
    unit.$btnAddProduct.click(function () {
        unit.SelectProduct();
    });
    unit.$btnAddProduct2.click(function () {
        unit.SelectProduct2();
    });
    unit.$btnPrint.click(function () {
        unit.PrintPage();
    });
    unit.$groupExport.change(function () {
        var value = $(this).val();
        if (value === 'pdf') {
            unit.ExportPDF();
        }
        if (value === 'word') {
            unit.ExportWord();
        }
    });
    unit.$btnSave.click(function () {
        if (unit.$form.valid()) {
            unit.SaveExRetail();
        }
    });
    unit.$btnPay.click(function () {
        if (unit.$form.valid()) {
            unit.PayExRetail();
        }
    });
    unit.SetUpValidate();

    $("#productList").change(function () {
        var option = $("#productList").find('option:selected');
        var dataSelect = {
            Id: option.attr('value'),
            DUnit: option.attr('dUnit'),
            DDescription: option.attr('dDescription') == undefined ? "" : option.attr('dDescription'),
            DPrice: option.attr('dPrice'),
            DName: option.attr('dName'),
            Total: 0
        };
        $("#total").val(dataSelect.DPrice);
    });
    Sv.SetupInputMask();
    $(".btn-changetype-sp").click(function () {
        if ($(".div-sp").is(":visible")) {
            $(".div-sp").hide();
            $(".div-phuthu").show();
            $(".btn-changetype-sp").html("<span class=\"fa fa-refresh\"></span> Sản phẩm");
        } else {
            $(".div-phuthu").hide();
            $(".div-sp").show();
            $(".btn-changetype-sp").html("<span class=\"fa fa-refresh\"></span> Dịch vụ - Phụ thu");
        }
    });


});

base64ToArrayBuffer = function (base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

saveByteArray = function (reportName, bytes, fileType) {
    var blob = new Blob([bytes]);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var timeNow = new Date();
    var month = timeNow.getMonth() + 1;
    var fileName = reportName + fileType;
    link.download = fileName;
    link.click();
};
