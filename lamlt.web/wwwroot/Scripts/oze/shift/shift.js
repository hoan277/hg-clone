﻿
var $table;

$(document).ready(function () {

    reportDate();
    setTimeout(function () { $("#btnRefresh").trigger("click"); }, 1);

    function rsdata() {
        Sv.SetDateTime($("input[name='FromDate']"), Sv.Set00InDay(Sv.AddDays(-30)));
        Sv.SetDateTime($("input[name='ToDate']"), Sv.Set59InDay(new Date()));
        $("#userId").val(-1);
        $("#confirmUserId").val(-1);
    }

    $("#btnSearch")
        .click(function () {
            searchGrid();
        });
    $("#btnRefresh")
       .click(function () {
           rsdata();
           searchGrid();
       });

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
        }
    });

    showDialogLoading();
    var tableOption = {
        "ajax": {
            "url": "/Shift/List",
            "data": function (d) {
                d.search = "";
                d.columns = "";
                d.FromDate = $("#FromDate input[name='FromDate']").val();
                d.ToDate = $("#ToDate input[name='ToDate']").val();
                d.userId = $("#userId").val();
                d.confirmUserId = $("#confirmUserId").val();
            }
        },
        "columns":
        [
            {
                "data": null, render: function (data, type, row, infor) {
                    console.log(data);
                    return $table.page.info().page + infor.row + 1;
                }
            },
            { "data": "title" },
            { "data": "user_FullName" },
            {
                "data": null, render: function (data, type, row, infor) {
                    return Sv.DateToString(row.startDate, "DD/MM/YYYY HH:mm");
                }
            },
            {
                "data": null, render: function (data, type, row, infor) {
                    return Sv.DateToString(row.endDate, "DD/MM/YYYY HH:mm");
                }
            },
            { "data": "userTo_FullName" },
           
            //{
            //    "data": null, render: function (data, type, row, infor) {
            //        return Sv.NumberToString(row.totalAmount);
            //    }
            //},
            //{
            //    "data": null, render: function (data, type, row, infor) {
            //        return Sv.NumberToString(row.totalAmountOld);
            //    }
            //},
            //{
            //    "data": null, render: function (data, type, row, infor) {
            //        return Sv.NumberToString(row.totalExpensive);
            //    }
            //},
            //{
            //    "data": null, render: function (data, type, row, infor) {
            //        return Sv.NumberToString(row.totalTransfer);
            //    }
            //},
            {
                "data": null, render: function (data, type, row, infor) {
                    var total = row.status == 1  ? row.totalFinal
                                            : row.totalAmountOld + row.totalAmount - row.totalTransfer - row.totalExpensive;
                    return Sv.NumberToString(total);
                }
            },
            {
                "data": "statusName" 
            },
            {
                "data": null, render: function (data, type, row, infor) {
                    var htmlMenu = ('<div class="input-table" onclick="javascript:shiftOpen(' + row.id + ')">' +
                                    '<a  class="btn btn-primary" title="Chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                '</div>');
                    return htmlMenu;
                }
            },
        ],
        "initComplete": function (settings, json) {
            hideDialogLoading();
        },
    };
    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));
});


function searchGrid() {
    $table.ajax.reload(function (data) {
        //setTableFooter($table, data.recordsTotal, data.totalAmount, data.total);
    });
}

function shiftOpen(id) {
 
    //thông tin  order 
    
        $("#modalDetails").off('show.bs.modal').on('show.bs.modal', function () {
            $("#TittleBox").html("Thông tin giao ca");
            $("#modalDetails .modal-body-content").html('<p>loading..</p>');
            Sv.AjaxPost({
                url: "/Shift/ViewShift",
                data: { id: id }
            }, function (rs) {
                console.log(rs);
                $("#modalDetails .modal-body-content").html(rs);
                Sv.SetupNumberMask([{ e: $(".amount-mask") }]);
                Sv.SetScrollHeight($(".box-item"));
                Sv.SetupInputMask();
            });
        }).modal("show");
  


}