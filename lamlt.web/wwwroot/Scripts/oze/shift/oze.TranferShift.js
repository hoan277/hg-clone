﻿
var ExpenseEdit = function () {
    var base = this;
    this.ResetForm = function (form) {

        if (form.length) {
            form.find("input, textarea, select")
                .each(function (index) {
                    var input = $(this);
                    if (input.is(":radio, :checkbox")) {
                        input.prop("checked", this.defaultChecked);
                    } else if (input.is("select")) {
                        input.val("-1");
                    } else {
                        input.val("");
                    }
                });
        }

    };

    this.ClosedShift = function () {
        confirm("Bạn có chắc chắn muốn đóng ca?",
            function () {
                var id = $("#frmTransferShift input[name=shiftid]").val();
                var final = $("#frmTransferShift input[name=final]").val();
                Sv.AjaxPost({
                    Url: "/Shift/ClosedShift",
                    Data: { id: id, totalFinal: final, }
                }, function (rs) {
                    console.log(rs);
                    if (rs.result > 0) {
                        bootboxLamlt.alert("Đóng ca thành công! Tài khoản sẽ thực hiện thao tác trên ca mới!");

                        $("#btnClosedShift").prop("disabled", true);
                        $("#btnTransferShift").prop("disabled", true);
                        setTimeout(function () { window.location.reload(), 3000})
                    } else {
                        alert("Đóng ca không thành công!");
                    }
                });
            });
        return;
    };

    this.TransferShift = function () {
        var final = $("#frmTransferShift input[name=final]").val();
        var userId = $("#frmTransferShift select[name=userTransfer]").val();
        var userName = $("#frmTransferShift select[name=userTransfer] :selected").text();
        var title = $("#title").val();

        if (userId == 0) {
            alert("Vui lòng chọn nhân viên giao ca");
            return;
        }
        confirm("Bạn có chắc chắn muốn giao ca cho <b>" + userName + "</b>, số tiền bàn giao: <b>" + convert2Money(final) + "</b> ?",
            function () {
                var obj = {
                    Id: $("#frmTransferShift [name=shiftid]").val(),
                    title: title,
                    userTo: userId,
                    userFrom: userName,
                    totalFinal: final,
                };
                Sv.AjaxPost({
                    Url: "/Shift/TransferShift",
                    Data: obj
                }, function (rs) {
                    if (rs.IntStatus == 1) {
                        $("#btnClosedShift").prop("disabled", true);
                        $("#btnTransferShift").prop("disabled", true);
                        alert(rs.Message, function () {
                            window.location.href = "/Accounts/Logout";
                        });       
                    } else {
                        bootboxLamlt.alert(rs.Message);
                    }
                });
            });

    };

    this.InitCbbUser = function () {
        $.get("/Shift/GetAvailableUser", function (data) {
            $.each(data.result, function (i, value) {
                console.log(i, value);
                var newRowContent =
                    '<option value="' + value.Id + '">' + value.FullName + '</option>';
                $("#userTransfer").append(newRowContent);
            });
        });
    }

    this.SetUpStringNumber = function () {
        $("#div_AmountOld").html(MoneyToString($("#totalAmountOld").val()));
        $("#div_Amount").html(MoneyToString($("#totalAmount").val()));
        
        $("#div_Expensive").html(MoneyToString($("#totalExpensive").val()));
        //$("#div_TempFinal").html(MoneyToString($("#totalFinal").val()));
        $("#div_Transfer").html(MoneyToString($("#totalTransfer").val()));
        $("#div_Final").html(MoneyToString($("#final").val()));
    }
};
$(document)
    .ready(function () {
        var unit = new ExpenseEdit();
        unit.InitCbbUser();
        unit.SetUpStringNumber();
        $("#btnClosedShift")
            .click(function () {
                unit.ClosedShift();
            });
        $("#btnTransferShift").click(function () {
            unit.TransferShift();
        });
    });