﻿function film_video_image()
{
    var self = this;
    this.tableName = "film_video_image";
    this.idModal = "modalfilm_video_image";
    this.$table;

    this.editDialog = function (id)
    {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function ()
        {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/"+ self.tableName +"/Edit/" + id);
            $("#" + self.idModal + " button#btnSave"+ self.tableName ).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function ()
            {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
				
                $.post("/film_video_image/update", { obj: pdata }, function (data)
                {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0)
                    {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :"+data.mess);
                    }
                });
				/*
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data) 
					{ 
						hideDialogLoading();						
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});*/
            });
            $("#"+self.idModal).off('show.bs.modal');
        });
        $("#"+self.idModal).modal("show");
    }
    this.deleteDialog = function (id)
    {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result)
        {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/"+id,
                success: function (data)
                {
						hideDialogLoading();
						if (data.result > 0) {
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                        }
                        else {
							            alert("Có lỗi khi tạo :" + data.mess);
                        }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id)
    {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#"+self.idModal).off('show.bs.modal');
        $("#"+self.idModal+" .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function ()
        {
            $("#"+self.idModal+" .modal-body-content").load("/"+ self.tableName +"/GetViewDetail/" + id);
            $("#"+self.idModal+" button#btnSave" + self.tableName).css("display", "none");
        });
        $("#"+self.idModal).modal("show");
    }
    this.setupGrid = function ()
    {
        showDialogLoading();
        self.$table = $('#example_film_video_image').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json)
            {
                hideDialogLoading();               
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/"+ self.tableName+"/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
					delete d.columns;
					
                }
            },
            "columns":
                [{ 'data': 'id' }
                    , { 'data': 'filmid' }
                    , {
                        "data": null, render: function (data, type, row) {

                            var htmlMenu = '<img src ="' + row.url + '" alt = "image" width = "100" height = "100"/>';
                            return htmlMenu;
                        }
                    }
                    , { 'data': null, render: function (data, type, row, infor) { return row.datecreated === null ? '' : moment(new Date(parseInt(row.datecreated.slice(6, -2)))).format('DD-MM-YYYY HH:mm'); } },
                {
                    "data": null, render: function (data, type, row)
                    {
                        var htmlMenu =
                                '<a style="color: #00a65a;font-weight: bold;" href="#" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                                ' Xem </a> ' +
                                '<a href="#" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                                ' Sửa </a> ' +
                                '<a href="#" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                ' Xóa </a> ';
                            return htmlMenu;
                    }
                }
                ]
        });
    }
    this.searchGrid=function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal)
    {
        var html = '<div id="'+idModal+'" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">'+self.tableName+'</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName+ '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal= function (idModal)
    {
        $('#'+idModal).modal('hide');
    }
}