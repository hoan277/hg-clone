﻿/*in hóa đơn*/
function openPrintDinh(htmlxxxxx) {
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write(htmlPrintAll(htmlxxxxx));
    mywindow.document.getElementById("printButon").remove();
    setTimeout(function () { mywindow.print(); }, 400);  // nhỏ hơn 400 thì tèo ~ 100 thì tèo để 350 cho khủng
    mywindow.onfocus = function () {
        setTimeout(function () {
            mywindow.close();
            $("#modalPrintDinh").modal("hide");
        }, 410);
    }
}

function htmlPrintAll(htmlxxxxx) {

    var html = "";
    html += ('<html>');
    html += ('<head>');
    html += ('<link rel="stylesheet" href="/Content/bootstrap.min.css" type="text/css" />');
    html += ('<link rel="stylesheet" href="/Content/print/hotel_print.css" type="text/css" />');
    html += ('</head>');
    html += ('<body class="container-fluid">');
    html += ('<div id="printCust">');
    if (htmlxxxxx) {
        html += htmlxxxxx;
    } else {
        html += $("#printCust").html();
    }
    html += ("</div>");
    html += ('</body>');
    html += ('</html>');
    return html;
}

// print 

function printDinhHotel(action) {
    var modalId = "modalPrintDinh";
    updateInformation();
    var data = {
        checkinID: parseInt($("#CheckInID").val()),
        tDate: $("#Leave_DateCheckOut").val(),
        temp: information
    }

    // sau thanh toán
    if (action == 101) {
        showDialogLoading();
        $.post("/Print/ViewPrint_Dinh2", data, function (rs) {
            hideDialogLoading();
            openPrintDinh(rs);
            setTimeout(function () {
                hashChangeSodophong();
            }, 200);
        });
    } else {
        Sv.SetupModal({
            modalId: modalId,
            title: "In hóa đơn",
            url: "/Print/ViewPrint_Dinh",
            data: data,
            modalclass: "modal-default",
            button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
        }, function () { 
        }, function () {
        }, function () {
        });
    }

}

function printDinhHotel_bc(orderId, checkinId) {
    var modalId = "modalPrintDinh";

    Sv.SetupModal({
        modalId: modalId,
        title: "In hóa đơn",
        url: "/Print/ViewPrint_Dinh2",
        data: { orderId: orderId, checkinID: checkinId },
        modalclass: "modal-default",
        button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
    }, function () {
    }, function () {
    }, function () {
    });

}








/*in hóa đơn nguyễn anh*/
function openPrintOrder80(htmlxxxxx) {
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write(htmlPrintAll(htmlxxxxx));
    mywindow.document.getElementById("printButon").remove();
    setTimeout(function () { mywindow.print(); }, 400);  // nhỏ hơn 400 thì tèo ~ 100 thì tèo để 350 cho khủng
    mywindow.onfocus = function () {
        setTimeout(function () {
            mywindow.close();
            $("#modal_printOrder80").modal("hide");
        }, 410);
    }
}

// print   
function printOrder80(action) {
    var modalId = "modal_printOrder80";
    updateInformation();
    var data = {
        checkinID: parseInt($("#CheckInID").val()),
        tDate: $("#Leave_DateCheckOut").val(),
        temp: information
    }

    // sau thanh toán
    if (action == 101) {
        showDialogLoading();
        $.post("/Print/ViewPrint802", data, function (rs) {
            hideDialogLoading();
            openPrintOrder80(rs);
            setTimeout(function () {
                hashChangeSodophong();
            }, 200);
        });
    } else {
        Sv.SetupModal({
            modalId: modalId,
            title: "In hóa đơn",
            url: "/Print/ViewPrint80",
            data: data,
            modalclass: "modal-default",
            button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
        }, function () {
        }, function () {
        }, function () {
        });
    }

}

function printOrder80_bc(orderId, checkinId) {
    var modalId = "modal_printOrder80";

    Sv.SetupModal({
        modalId: modalId,
        title: "In hóa đơn",
        url: "/Print/ViewPrint802",
        data: { orderId: orderId, checkinID: checkinId },
        modalclass: "modal-default",
        button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
    }, function () {
    }, function () {
    }, function () {
    });

}



















/*in hóa đơn 2*/
function openPrintOrder_type2(htmlxxxxx) {
    var mywindow = window.open('', '', 'height=768,width=1024,scrollbars=yes');
    mywindow.document.write(htmlPrintAll(htmlxxxxx));
    mywindow.document.getElementById("printButon").remove();
    setTimeout(function () { mywindow.print(); }, 400);  // nhỏ hơn 400 thì tèo ~ 100 thì tèo để 350 cho khủng
    mywindow.onfocus = function () {
        setTimeout(function () {
            mywindow.close();
            $("#modal_printOrder_type2").modal("hide");
        }, 410);
    }
}
   
// print   
function printOrder_type2(action) {
    var modalId = "modal_printOrder_type2";
    updateInformation();
    var data = {
        checkinID: parseInt($("#CheckInID").val()),
        tDate: $("#Leave_DateCheckOut").val(),
        temp: information
    }

    // sau thanh toán
    if (action == 101) {
        showDialogLoading();
        $.post("/Print/ViewPrint_type2_bc", data, function (rs) {
            hideDialogLoading();
            openPrintOrder_type2(rs);
            setTimeout(function () {
                hashChangeSodophong();
            }, 200);
        });
    } else {
        Sv.SetupModal({
            modalId: modalId,
            title: "In hóa đơn",
            url: "/Print/ViewPrint_type2",
            data: data,
            modalclass: "modal-default",
            button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
        }, function () {
        }, function () {
        }, function () {
        });
    }

}

function printOrder_type2_bc(orderId, checkinId) {
    var modalId = "modal_printOrder_type2";

    Sv.SetupModal({
        modalId: modalId,
        title: "In hóa đơn",
        url: "/Print/ViewPrint_type2_bc",
        data: { orderId: orderId, checkinID: checkinId },
        modalclass: "modal-default",
        button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
    }, function () {
    }, function () {
    }, function () {
    });

}
