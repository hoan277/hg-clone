﻿
var printCtr = {

    load: function (v, data) {
        var html = [];
        var total = 0;
        var d = data.Deposit;
        var d2 = data.Deduction;
        switch (v) {
            case "":
                html = printCtr.htmlTp(data.ListEstimate, false);
                var htmldc = printCtr.htmlDv(data.ListCustomerServices, false, data.ListEstimate.length);
                html = html.concat(htmldc);
                //tổng tiền
                total = printCtr.getTotal(data.ListEstimate, 'tp') + printCtr.getTotal(data.ListCustomerServices, 'dv');
                break;
            case "tp":
                html = printCtr.htmlTp(data.ListEstimate, false);
                //tổng tiền
                total = printCtr.getTotal(data.ListEstimate, 'tp');
                break;
            case "dv":

                html = printCtr.htmlDv(data.ListCustomerServices, false, 0);
                //tổng tiền
                total = printCtr.getTotal(data.ListCustomerServices, 'dv');
                // giảm trừ giảm giá theo tiền phòng
                d = 0; d2 = 0;
                break;
            case "tp-sum":
                html = printCtr.htmlTp(data.ListEstimate, true);
                var htmldc = printCtr.htmlDv(data.ListCustomerServices, false, 0);
                html = html.concat(htmldc);
                //tổng tiền
                total = printCtr.getTotal(data.ListEstimate, 'tp') + printCtr.getTotal(data.ListCustomerServices, 'dv');
                break;
            case "dv-sum":
                html = printCtr.htmlTp(data.ListEstimate, false);
                var htmldc = printCtr.htmlDv(data.ListCustomerServices, true, 0);
                html = html.concat(htmldc);
                //tổng tiền
                total = printCtr.getTotal(data.ListEstimate, 'tp') + printCtr.getTotal(data.ListCustomerServices, 'dv');
                break;
            case "sum":
                html = printCtr.htmlTp(data.ListEstimate, true);
                var htmldc = printCtr.htmlDv(data.ListCustomerServices, true, 0);
                html = html.concat(htmldc);
                //tổng tiền
                total = printCtr.getTotal(data.ListEstimate, 'tp') + printCtr.getTotal(data.ListCustomerServices, 'dv');
                break;
        }
        // grid dữ liệu
        $("#gridPrint tbody").html(html.join(''));

        // tổng tiền thanh toán 
        var totalPay = total - (d + d2);
        printCtr.htmlTotal(total, d, d2, totalPay);

    },

    getTotal: function (data, type) {
        var total = 0;
        if (type == "tp")
            for (var i = 0; i < data.length; i++) { total += data[i].price; }
        else if (type == "dv")
            for (var i = 0; i < data.length; i++) { total += data[i].TotalSale; }
        return total;
    },

    htmlTp: function (data, isSum) {

        var html = ['<tr>', '<th class="alw" style="text-align: left" colspan="8">Tiền phòng</th>', '</tr>', ];
        if (data.length > 0) {
            if (isSum) {
                var total = 0;
                for (var i = 0; i < data.length; i++) { total += data[i].price; }
                var htmlsum = ['<tr>', '<th class="alw" style="text-align: left" colspan="8">Tiền phòng: ' + Sv.NumberToString(total) + ' VND </th>', '</tr>'];
                //html = html.concat(htmlsum);
                return htmlsum;
            } else {

                for (var i = 0; i < data.length; i++) {
                    var v = data[i];
                    var htmli = [
                        '<tr>',
                        '<td>' + (i + 1) + '</td>',
                        '<td>' + v.roomName + '</td>',
                        '<td>' + (Sv.DateToString(v.dtFrom, 'DD/MM/YYYY HH:mm') + ' - ' + Sv.DateToString(v.dtTo, 'DD/MM/YYYY HH:mm')) + '</td>',
                        '<td>' + (Sv.NumberToString(v.quantiy) + " " + v.titlePrice) + '</td>',
                        '<td class="text-right">' + Sv.NumberToString(v.price) + '</td>',
                        '<td class="text-right">' + Sv.NumberToString(v.quantiy) + '</td>',
                        '<td class="text-right">' + Sv.NumberToString(v.price) + '</td>',
                        '<td>' + Sv.NumberToString(v.pricePolicyName) + '</td>',
                        '</tr>'
                    ];
                    html = html.concat(htmli);
                };
                return html;
            }
        } else {
            //html.concat(['<tr>', '<th style="text-align: left" colspan="8">Tiền phòng: Không có dữ liệu</th>', '</tr>']);
            //return html;
            return ['<tr>', '<th class="alw" style="text-align: left" colspan="8">Tiền phòng: Không có dữ liệu</th>', '</tr>'];
        }
    },

    htmlDv: function (data, isSum, starti) {
        var html = ['<tr>', '<th class="alw" style="text-align: left" colspan="8">Tiền dịch vụ</th>', '</tr>', ]
        if (data.length > 0) {
            if (isSum) {
                var total = 0;
                for (var i = 0; i < data.length; i++) { total += data[i].TotalSale; }
                var htmlsum = (['<tr>', '<th class="alw" style="text-align: left" colspan="8">Tiền dịch vụ: ' + Sv.NumberToString(total) + ' VND </th>', '</tr>']);
                //html = html.concat(htmlsum);
                return htmlsum;
            } else {
                for (var i = 0; i < data.length; i++) {
                    var v = data[i];
                    var htmli = [
                        '<tr>',
                        '<td>' + (i + 1 + starti) + '</td>',
                        '<td>' + v.RoomName + '</td>',
                        '<td>' + v.Name + '</td>',
                        '<td>' + v.UnitName + '</td>',
                        '<td class="text-right">' + Sv.NumberToString(v.SalePrice) + '</td>',
                        '<td class="text-right">' + Sv.NumberToString(v.Quantity) + '</td>',
                        '<td class="text-right">' + Sv.NumberToString(v.TotalSale) + '</td>',
                        '<td>' + '' + '</td>',
                        '</tr>'
                    ];
                    html = html.concat(htmli);
                };
                return html;
            }
        } else {
            //html.concat(['<tr>', '<th style="text-align: left" colspan="8">Không có dữ liệu</th>', '</tr>']); 
            //return html;
            return ['<tr>', '<th class="alw" style="text-align: left" colspan="8">Tiền dịch vụ: 0</th>', '</tr>'];
        }
    },

    htmlTotal: function (total, deposit, deduction, totalPay) {
        $("#ptotal").html(Sv.NumberToString(total) + " VND");
        $("#pdeposit").html(Sv.NumberToString(deposit) + " VND");
        $("#pdeduction").html(Sv.NumberToString(deduction) + " VND");
        $("#ptotalpay").html(Sv.NumberToString(totalPay) + " VND");
    },

}