﻿
var $table;  

$(document).ready(function () {
    $("#btnAdd").click(function () {
        editMenu(0);
    });
    
    showDialogLoading();
    var tableOption = {
        "ajax": {
            "url": "/Setting/MenuList",
            "data": function (d) {
                d.search = "";
            }
        },
        "columns":
            [
                { "data": "Id", },
                { "data": "Name", },
                { "data": "Link", },
                { "data": "ParentID", },
                {
                    "data": "Status",
                    render: function (data, type, row, infor) {
                        return data == 1 ? "Hoạt động" : "Khóa";
                    }
                },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        var htmlMenu = ('<div class="input-table" onclick="javascript:viewMenu(' + row.Id + ')">' +
                                    '<a  class="btn btn-info btn-flat" title="Chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                '</div>');
                        htmlMenu += ('<div class="input-table" onclick="javascript:editMenu(' + row.Id + ')">' +
                                    '<a  class="btn btn-primary btn-flat" title="Sửa" ><i class="fa fa-edit"></i></a>' +
                                '</div>');
                        htmlMenu += ('<div class="input-table" onclick="javascript:removeMenu(' + row.Id + ')">' +
                                    '<a  class="btn btn-danger btn-flat" title="Xóa" ><i class="fa fa-times"></i></a>' +
                                '</div>');
                        return htmlMenu;
                    }
                },
            ],
        "initComplete": function (settings, json) { hideDialogLoading(); },
    };
    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));
});

function viewMenu(id) {
    Sv.SetupModal({
        modalId: "viewMenuModalId",
        title: "Xem menu",
        url: "/Setting/MenuEdit",
        data: { id },
        modalclass: "modal-default",
        button: '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>',
    }, function () { 
    }, function () { 
    }, function () { 
    });
}
 
function editMenu(id) {
    Sv.SetupModal({
        modalId: "editMenuModalId",
        title: "Sửa menu",
        url: "/Setting/MenuEdit",
        data: { id },
        modalclass: "modal-default",
    }, function () {
    }, function () {
        var data = getFormData($("#frmMenuEdit"));
        Sv.Post({ url: "/Setting/MenuEditUpdate", data: { obj: data } }, function (rs) {
            if (rs.IntStatus > 0) {
                $("#editMenuModalId").modal("hide");
                bootboxLamlt.alert(rs.Message);
                searchGrid();
            } else {
                alert(rs.Message);
            }
        });
    }, function () {
    });
}

function removeMenu(id) {
    confirm("Bạn chắc chắn muốn xóa menu này không", function (cf) {
        if (cf) {
            Sv.Post({ url: "/Setting/MenuDelete", data: { id } }, function (rs) {
                if (rs.IntStatus > 0) { 
                    bootboxLamlt.alert(rs.Message);
                    searchGrid();
                } else {
                    alert(rs.Message);
                }
            });
        }
    })
}

function searchGrid() {
    $table.ajax.reload(function (data) { });
}
