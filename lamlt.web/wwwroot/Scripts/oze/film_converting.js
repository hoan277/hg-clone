﻿function film_converting() {
    var self = this;
    this.tableName = "film_converting";
    this.idModal = "modalfilm_converting";
    this.$table;
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_converting/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.quatity = function (id) {
        $.post('/film_converting/quatity', {
            id: id
        }, function (data) {

        });
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_converting').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
                    delete d.columns;
                    d.search = $("#search_film_converting_title").val();
                    d.status = $("#search_status").children("option:selected").val();
                    console.log(d.status);
                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'title' }
                    , { 'data': 'path' }
                    , {
                        'data': null, render: function (row) {
                            if (row.start_time != null) {
                                var dateObj = new Date(row.start_time);
                                var date = $.datepicker.formatDate('dd/mm/yy', dateObj);
                                var hours = (dateObj.getHours() < 10) ? "0" + dateObj.getHours() : dateObj.getHours();
                                var minutes = (dateObj.getMinutes() < 10) ? "0" + dateObj.getMinutes() : dateObj.getMinutes();
                                var seconds = (dateObj.getSeconds() < 10) ? "0" + dateObj.getSeconds() : dateObj.getSeconds();
                                var time = hours + ":" + minutes + ":" + seconds;
                                return dateObj_str = date + " " + time;
                            }
                            return "";
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            if (row.date_time != null) {
                                var dateObj = new Date(row.date_time);
                                var date = $.datepicker.formatDate('dd/mm/yy', dateObj);
                                var hours = (dateObj.getHours() < 10) ? "0" + dateObj.getHours() : dateObj.getHours();
                                var minutes = (dateObj.getMinutes() < 10) ? "0" + dateObj.getMinutes() : dateObj.getMinutes();
                                var seconds = (dateObj.getSeconds() < 10) ? "0" + dateObj.getSeconds() : dateObj.getSeconds();
                                var time = hours + ":" + minutes + ":" + seconds;
                                return dateObj_str = date + " " + time;
                            }
                            return "";
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            if (row.status == 2 && row.date_time != null && row.start_time != null) {
                                var start_time = new Date(row.start_time).getTime();
                                var date_time = new Date(row.date_time).getTime();
                                var distance = date_time - start_time;
                                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                days = (days < 1) ? "" : days + " ngày ";
                                hours = (hours < 1) ? "" : hours + " giờ ";
                                minutes = (minutes < 1) ? "" : minutes + " phút ";
                                seconds = (seconds < 1) ? "Chưa đầy 1 phút" : seconds + " giây ";
                                return days + hours + minutes + seconds;
                            }
                            return "";
                        }
                    }
                    , {
                        'data': null, render: function (data, type, row) {
                            if (row.status == 5) return "Đang convert";
                            else if (row.status == 1) return "Đã convert xong";
                            else if (row.status == 6) return "Convert lỗi";
                            else { return "" }
                        }
                    }
                    , { 'data': 'cmd_title' }
                    , {
                        'data': null, render: function (data, type, row) {
                            if (row.process_id > 0) return row.process_id + '<br /><a id="create_pid" data-pid="' + row.process_id + '" class="btn btn-block btn-danger" href="javascript:void(0)" title="Stop convert">Stop</a>';
                            else { return "" }
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}