﻿var idInEdit = 0;
var productInEdit = { 'Name': 0, 'UnitID': 0 }; 
    var currentListPrice = []; 
function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function GetDataSubmit(form) {
    var obj = [];

    form.find("tr.item").each(function (index) {
        var trItem = $(this);
        var itemObj =
        {
            Index: index,
            Price: trItem.find(".productvalue").text().replace(/\./g, ''),
            UnitId: trItem.find(".unit").attr('UnitID').trim(),
            UnitMiniId: trItem.find(".unitMini").attr('UnitIDMin').trim(),
            vol1: trItem.find(".vol").attr('V1').trim(),
            vol2: trItem.find(".vol").attr('V2').trim(),
            vol3: trItem.find(".vol").attr('V3').trim(),
            Quantity: trItem.find(".Quantity").attr('quantity').trim(),
            weightShip: trItem.find(".weightShip").attr('weightShip').trim()

        }
        obj.push(itemObj);
    });
    return obj;
}
function GetFormDataTable(form) {
    var obj = new FormData();
    form.find("tr.item").each(function (index) {
        var trItem = $(this);
        obj.append("Index", index);
        obj.append("Price", trItem.find(".productvalue").text().replace('.', ''));
        obj.append("UnitId", trItem.find(".unit").attr('UnitID').trim());
        obj.append("UnitIDMin", trItem.find(".unitMini").attr('UnitIDMin').trim());
        obj.append("vol1", trItem.find(".vol").attr('V1').trim());
        obj.append("vol2", trItem.find(".vol").attr('V2').trim());
        obj.append("vol3", trItem.find(".vol").attr('V3').trim());
        obj.append("Quantity", trItem.find(".Quantity").attr('quantity').trim());
        obj.append("Indexs", trItem.find(".indexName").attr('index').trim());

        obj.append("weightShip", trItem.find(".weightShip").attr('weightShip').trim());

    });
    return obj;
}
function GetFormData() {
    var $form = $("#frmDetail");
    var obj = new FormData();
    var content = CKEDITOR.instances.txtContent.getData();
    var description = CKEDITOR.instances.txtDecription.getData();
    obj.append("ProductGroupID", $form.find("select[id='selectGroupPro']").val());
    obj.append("SupplierID", $form.find("select[id='selectSupplier']").val());
    obj.append("UnitID", $form.find("select[id='selectUnit']").val());
    obj.append("UnitIdMini", $form.find("select[id='selectUnitMini']").val());
    obj.append("Status", $form.find("select[id='selectStatus']").val());
    obj.append("ID", $form.find("input[id='txtID']").val());
    obj.append("Name", $form.find("input[id='txtName']").val());
    obj.append("PriceOrder", $form.find("input[id='txtPriceOrder']").val());
    obj.append("SalePrice", $form.find("input[id='txtPriceOrder']").val());
    obj.append("QuotaMinimize", $form.find("input[id='txtQuantity']").val());
    obj.append("v1", $form.find("input[id='txtv1']").val());
    obj.append("v2", $form.find("input[id='txtv2']").val());
    obj.append("v3", $form.find("input[id='txtv3']").val());
    obj.append("DungTich_Trong", $form.find("input[id='txtDungTich_Trong']").val());
    obj.append("QuantityDVT", $form.find("input[id='txtQuantityDVT']").val());
    obj.append("Description", content);
    obj.append("Contents", description);
    //var files = $form.find("input[id='imgInp']")[0].files;
    //for (var i = 0; i < files.length; i++) {
    //    obj.append("file" + i, files[i]);
    //}
    console.log('str', JSON.stringify(GetFormDataTable($("#frmDetail"))));
    var files = $form.find("input[id='imgInp']").get(0).files;
    for (var i = 0; i < files.length; i++) {
        obj.append("filename" + i, files[i]);
    }
    return obj;
}
function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}
function TemplateView(index, obj) {

    var template =
        '<tr class="item" data-index="' + index + '">' +
            ' <td> <p class="indexName" index="' + index + '" id="index-[' + index + ']" name="index-[' + index + ']">' + index + '</p> </td>' +
            ' <td> <p class="productname" productID="' + obj.ProductId + '" id="productname-[' + index + ']" name="productname-[' + index + ']">' + obj.Name + '</p> </td>' +
            ' <td> <p class="unit" UnitID="' + obj.UnitID + ' " id="unit' + index + '" name="unit' + index + '">' + obj.UnitName + '</p> </td>' +
            ' <td style="text-align: right"> <p class="productvalue" id="productvalue-[' + index + ']" name="productvalue-[' + index + ']">' + obj.Price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + '</p> </td>' +
            //' <td style="text-align: right">' + obj.UnitMinName.toString() + ' </td> ' +
            ' <td> <p class="unitMini" UnitIDMin="' + obj.UnitIDMin + ' " id="UnitIDMin' + index + '" name="UnitIDMin' + index + '">' + obj.UnitMinName + '</p> </td>' +
            //' <td style="text-align: right">' + obj.Quantity + '</td>' +
            ' <td style="text-align: right"> <p class="Quantity" Quantity="' + obj.Quantity + ' " id="Quantity' + index + '" name="Quantity' + index + '">' + obj.Quantity + '</p> </td>' +
            //' <td style="text-align: right"> <p class="PriceShipping" id="PriceShipping-[' + index + ']" name="PriceShipping-[' + index + ']">' + obj.vol1 + 'x' + obj.vol2 + 'x' + obj.vol3 + '</p> </td>' +
            ' <td style="text-align: right"> <p class="vol" id="v1-[' + index + ']" V1="' + obj.vol1 + ' " V2="' + obj.vol2 + ' " V3="' + obj.vol3 + ' " name="PriceShipping-[' + index + ']">' + obj.vol1 + 'x' + obj.vol2 + 'x' + obj.vol3 + '</p> </td>' +

            ' <td> <p class="weightShip" weightShip="' + obj.weightShip + ' " id="weightShip' + index + '" name="weightShip' + index + '">' + obj.weightShip + '</p> </td>' +

            ' <td class="text-center"><span class="btn btn-sm btnEdit" title="Sửa" id="btn-edit-[' + index + ']"><i class="fa fa-pencil"></i></span> <span class="btn btn-sm btnRemove" title="Xoá"  id="btn-remove-[' + index + ']"><i class="fa fa-trash-o"></i></span> </td>  </tr>' +
            '</tr>'; 
    return template;
};
function TemplateViewNotEditAndRemove(index, obj) {

    var strPrice = '';
    if (obj.IdPromotion != null) {
        strPrice += '<span class="promotionprice">' + obj.Price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + '</span><span>' + obj.PromotionPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + '</span>';
    }
    else {
        strPrice += obj.Price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }

    var template =
        '<tr class="item" data-index="' + index + '">' +
            ' <td> <p class="indexName" index="' + index + '" id="index-[' + index + ']" name="index-[' + index + ']">' + index + '</p> </td>' +
            ' <td> <p class="productname" productID="' + obj.ProductId + '" id="productname-[' + index + ']" name="productname-[' + index + ']">' + obj.Name + '</p> </td>' +
            ' <td> <p class="unit" UnitID="' + obj.UnitID + ' " id="unit' + index + '" name="unit' + index + '">' + obj.UnitName + '</p> </td>' +
            ' <td style="text-align: right"> <p class="productvalue" id="productvalue-[' + index + ']" name="productvalue-[' + index + ']">' + strPrice + '</p> </td>' +
            //' <td style="text-align: right">' + obj.UnitMinName.toString() + ' </td> ' +
            ' <td> <p class="unitMini" UnitIDMin="' + obj.UnitIDMin + ' " id="UnitIDMin' + index + '" name="UnitIDMin' + index + '">' + obj.UnitMinName + '</p> </td>' +
            //' <td style="text-align: right">' + obj.Quantity + '</td>' +
            ' <td style="text-align: right"> <p class="Quantity" Quantity="' + obj.Quantity + ' " id="Quantity' + index + '" name="Quantity' + index + '">' + obj.Quantity + '</p> </td>' +
            //' <td style="text-align: right"> <p class="PriceShipping" id="PriceShipping-[' + index + ']" name="PriceShipping-[' + index + ']">' + obj.vol1 + 'x' + obj.vol2 + 'x' + obj.vol3 + '</p> </td>' +
            ' <td style="text-align: right"> <p class="vol" id="v1-[' + index + ']" V1="' + obj.vol1 + ' " V2="' + obj.vol2 + ' " V3="' + obj.vol3 + ' " name="PriceShipping-[' + index + ']">' + obj.vol1 + 'x' + obj.vol2 + 'x' + obj.vol3 + '</p> </td>' +

            ' <td> <p class="weightShip" weightShip="' + obj.weightShip + ' " id="weightShip' + index + '" name="weightShip' + index + '">' + obj.weightShip + '</p> </td>' +

            '  </tr>' +
            '</tr>';
    return template;
};
function DisibaleForm() {
    var $form = $("#frmDetail").on();
    $('input,select').attr('disabled', "disabled");
    $('input,select').prop('disabled', true);
    //$("#frmDetail :input").attr("disabled", "disabled");
    //$form.find("select[id='selectGroupPro']").prop('disabled', true);
    //$form.find("select[id='selectSupplier']").prop('disabled', true);
    //$form.find("select[id='selectUnit']").prop('disabled', true);
    //$form.find("select[id='selectUnitMini']").prop('disabled', true);
    //$form.find("select[id='selectStatus']").prop('disabled', true);

}
function loadTable(id) {
    $.post("/Product/GetPriceDetail", { productId: id }, function (data) {

        var index = 0;
        var tbody = $("#tableData tbody");
        var str = "";
        for (var i = 0; i < data.length; i++) {
            index = index + 1; 
            tbody.append(TemplateView(index, data[i]));
            currentListPrice.push(data[i]);
        }
        hideDialogLoading();
        g_Utils.SetAmount();
        g_Utils.SetDateDefaultAdd();
    });
}
function loadTableView(id) {
    $.post("/Product/GetPriceDetail", { productId: id }, function (data) {

        var index = 0;
        var tbody = $("#tableData tbody");
        for (var i = 0; i < data.length; i++) {
            index = index + 1;
            tbody.append(TemplateViewNotEditAndRemove(index, data[i]));
            currentListPrice.push(data[i]);
        }
        hideDialogLoading();
        g_Utils.SetAmount();
        g_Utils.SetDateDefaultAdd();
    });
}
function bindView(obj) {
    var tbody = $("#tableData tbody");
    {
        currentListPrice.push(obj);
        tbody.append(TemplateView(currentListPrice.length, obj));
    }
}
function bindViewEdit(obj) {
    var tbody = $("#tableData tbody");
    var item = $('#tableData tr.item[data-index=' + idInEdit + ']');
    item.find('.unit').text(obj.UnitName);
    item.find('.unit').attr('unitid', obj.UnitID);
    item.find('.productvalue').text(obj.Price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'));
    item.find('.unitMini').text(obj.UnitMinName);
    item.find('.unitMini').attr('unitidmin', obj.UnitIDMin);
    item.find('.Quantity').text(obj.Quantity);
    item.find('.Quantity').attr('quantity', obj.Quantity);
    var txtvol = obj.vol1 + 'x' + obj.vol2 + 'x' + obj.vol3;
    txtvol = txtvol.replace(/ /g, '');
    item.find('.vol').text(txtvol);
    item.find('.vol').attr('v1', obj.vol1);
    item.find('.vol').attr('v2', obj.vol2);
    item.find('.vol').attr('v3', obj.vol3);
    item.find('.weightShip').text(obj.weightShip);
    item.find('.weightShip').attr('weightship', obj.weightShip);
}
$("#myModal").on("click", ".btnRemove", function (e) {
    var index = $(e.target).parents("tr.item").data("index");
    var item = $('#tableData tr.item[data-index=' + index + ']');
    var productname = item.find('.productname').text().trim();
    var unitid = item.find('.unit').attr('unitid').toString().trim() * 1;
    delproductPriceInlist(productname, unitid);
    // xóa dòng đó
    
    if (item.next().hasClass("tr-my-error")) {
        item.next().remove();
    }
    $('#tableData tr.item[data-index=' + index + ']').remove();

});
$("#myModal").on("click", ".btnEdit", function (e) {
    var index = $(e.target).parents("tr.item").data("index");
    idInEdit = index;
    //// xóa dòng đó
    var item = $('#tableData tr.item[data-index=' + index + ']');
    var productname = item.find('.productname').text().trim();
    var productId = item.find('.productname').attr('productid').toString().trim() * 1;
    var unitname = item.find('.unit').text().trim();
    var unitid = item.find('.unit').attr('unitid').toString().trim() * 1;
    var price = item.find('.productvalue').text().trim();
    price = price.toString().replace(/\./g, '');
    var unitminname = item.find('.unitMini').text().trim();
    var unitminId = item.find('.unitMini').attr('unitidmin');
    var quantity = item.find('.Quantity').attr('quantity').toString().replace(/\./g, '');
    var v1 = item.find('.vol').attr('v1');
    var v2 = item.find('.vol').attr('v2');
    var v3 = item.find('.vol').attr('v3');
    var weight = item.find('.weightShip').attr('weightship').toString().trim();
    $('#txtPriceOrder').val(price);
    $('#selectUnit').val(unitid * 1);
    $('#selectUnitMin').val(unitminId * 1);
    $('#txtQuantityMin').val(quantity);
    $('#txtv1').val(v1);
    $('#txtv2').val(v2);
    $('#txtv3').val(v3);
    $('#txtWeightShip').val(weight);
    $('#btnAddPrice').addClass('modeEdit');
    $('#btnAddPrice').find('i').removeClass('fa-plus-square');
    productInEdit = { 'Name': productname, 'UnitID': unitid };

});
function delproductPriceInlist(name, unitId) {
    var istep = -1;
    for (var index in currentListPrice) {
        var obj1 = currentListPrice[index];
        if (obj1.Name == name && obj1.UnitID == unitId) {
            istep = index;
            break;
        }
    }
    if (istep > -1) {
        currentListPrice.splice(istep, 1);
    }
}


function createEditorBuidl(languageCode, id) {
    var editor = CKEDITOR.replace(id, {
        language: languageCode
    });
}
//---------------Img-----------------
function GetListImage(proId) {

    $.post("/ProductOze/GetImagePro", { proId: proId },
    function (data) {
        $("#listImage").html(data);
    });
}
function DeleteImg(fileId, name) {
    //var cf = confirm("Bạn có muốn xóa ảnh này");
    //if (cf==true) {
    var id = $("#txtID").val();
    setTimeout(function () {
        $.post("/ProductOze/DeleteImg", { fileId: fileId, name: name },
            function (data) {
                //hideDialogLoading();
                if (data.result > 0) {
                    //bootboxLamlt.alert("Thao tác thành công", function () {
                    GetListImage(id);
                    //});
                } else {
                    alert("Có lỗi khi xóa ảnh");
                }
            });
    }, 50);
}
//}