﻿var $table;
$(document).ready(function () {
    toggleMenu("menu_kho");
    showDialogLoading();
    $table = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "initComplete": function (settings, json) {
            hideDialogLoading();
        },
        /*bFilter: false, bInfo: false,*/
        "dom": '<"top">rt<"bottom" lpi><"clear">',
        "ajax": {
            "url": "/Dinhluong/ListProductDinhLuong",
            "data": function (d) {
                delete d.columns;
                d.search = $("#txtSearch").val();
            }
        },
        "columns":
            [
            {
                "data": null, "orderable": "false",
                render: function (data, type, row, infor) {
                    return $table.page.info().page + infor.row + 1;
                }
            },
            { "data": "Name", "orderable": "false" },
            { "data": "UnitName", "orderable": "false" },
            {
                "data": "SalePrice", "orderable": "false", className: 'text-right',
                render: function (data, type, row, infor) {
                    return Sv.NumberToString(row.SalePrice);
                }
            },
            {
                "data": "PriceOrder", "orderable": "false", className: 'text-right',
                render: function (data, type, row, infor) {
                    return Sv.NumberToString(row.PriceOrder);
                }
            },
            {
                "data": null, render: function (data, type, row) {
                    var htmlMenu =
                    '<div class="edit-delete-table">' +
                        '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:viewDialog(' + row.ID + ',' + row.UnitID + ')">' +
                                   '<img src="/images/icon/icon-view.png" style=" border: none;" title="Thông tin">' +
                     '</div>' +
                        '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:viewDialogDinhLuong(' + row.ID + ',' + row.UnitID + ')">' +
                                   '<i style="color:#f27c2e; font-size:19px" class="fa fa-archive" title="Cập nhật định lượng"></i>' +
                     '</div>';
                    return htmlMenu;
                }
            }]
    });

    $("#txtSearch").keypress(function (e) {
        if (e.which == 13) {
            searchGrid();
        }
    });

    $("#btnSearch").off().on("click", function (e) {
        searchGrid();
    });

    $("#btnReset").off().on("click", function (e) {
        resetFrom();
    });


});

function searchGrid() {
    $table.ajax.reload();
}

function resetFrom() {
    $("#txtSearch").val("");
    searchGrid();
}

function setupValidate() {
    var form = $('#dummyProduct').on();
    form.validate({
        rules: {
            Name: {
                required: true
            },
            QuotaMinimize: {
                required: true
            },
            SalePrice: {
                required: true
            },
            PriceOrder: {
                required: true
            },
            txtQuantityMin: {
                required: true
            },
            txtWeightShip: {
                required: true
            }
        },
        messages: {
            Name: {
                required: 'Vui lòng nhập tên sản phẩm'
            },
            QuotaMinimize: {
                required: 'Vui lòng nhập định mức tối thiểu'
            },
            SalePrice: {
                required: 'Vui lòng nhập giá nhập'
            },
            PriceOrder: {
                required: 'Vui lòng nhập giá bán'
            },
            txtQuantityMin: {
                required: 'Vui lòng nhập quy các đóng gói'
            },
            txtWeightShip: {
                required: 'Vui lòng nhập trọng lượng'
            }
        }
    });
}

function viewDialog(id, unitid) {
    viewDialogDinhLuong(id, unitid, function () {
        $("#myModal2").find("select").prop("disabled", true);
        $("#myModal2").find("input").prop("disabled", true);
        $("#myModal2").find("#btnSaveDl").hide();
        $("#myModal2").find(".btn-danger").hide();
        $("#myModal2 #adddl1").hide();
    })
}

function viewDialogDinhLuong(id, unitid, callback) {
    $("#myModal2").off('show.bs.modal');
    $("#myModal2").on('show.bs.modal', function () {
        $("#myModal2 .modal-body-content").html('<p>loading...</p>');
        Sv.Post({
            url: "/Dinhluong/Edit",
            data: { id: id, unitid: unitid }
        }, function (htxxxxx) {
            $("#myModal2 .modal-body-content").html(htxxxxx);

            $("#myModal2 button#btnSaveDl").css("display", "inline");
            var $t = $("#tbl_prodl");
            $("#myModal2 button#btnSaveDl").off("click").click(function (e) {
                adddinhluong.save(id, $t, e);
            });

            if (lstDinhluong.length > 0)
                $.each(lstDinhluong, function (id1, d1) {
                    adddinhluong.setup(id, $t, d1);
                    $t.find("tr:last").find(".dl_pro").val(d1.productSource);
                    $t.find("tr:last").find(".dl_unit").val(d1.unitSource);
                    $t.find("tr:last").find(".dl_val").val(d1.value);
                });

            // add sản phẩm
            $("#myModal2 #adddl1").on().show();
            $("#adddl1").on().off("click").click(function () {
                adddinhluong.setup(id, $t);
                //adddinhluong.addProduct(id, $t);
                //// change sản phâm
                //$("#myModal2").on().find(".dl_pro").off("change").change(function (e) {
                //    // load lại unit khi change product
                //    adddinhluong.changePro(id, $t, e);
                //});
                //// xóa
                //$("#myModal2").on().find(".btn-danger").off("click").click(function (e) {
                //    adddinhluong.xoa(id, $t, e);
                //});
            });

            if (typeof callback === "function") {
                callback();
            }
        });
    });
    $("#myModal2").modal("show");
}

var adddinhluong = {
    setup: function (id, $t, prod) {
        adddinhluong.addProduct(id, $t, prod);
        // change sản phâm
        $("#myModal2").on().find(".dl_pro").off("change").change(function (e) {
            // load lại unit khi change product
            adddinhluong.changePro(id, $t, e);
        });
        // xóa
        $("#myModal2").on().find(".btn-danger").off("click").click(function (e) {
            adddinhluong.xoa(id, $t, e);
        });
    },
    getUnit: function (product) {
        var unit = [];
        //unit.push({
        //    Id: product.unitDefault,
        //    Name: product.unitDefaultName
        //});
        $.each(product.lstUnit, function (i1, x) {
            unit.push({
                Id: x.unitid,
                Name: x.unitName
            });
        });
        return unit;
    },
    addProduct: function (id, $t, prod) {
        var sourceSv = $("#handlabars_griditem").html();
        var templateS = Handlebars.compile(sourceSv);
        var index = 1;
        var p = proSource[0];
        if (prod) {
            var p1 = proSource.filter(function (x) {
                return prod.productSource == x.Id;
            });
            p = p1[0];
        }
        var unit = adddinhluong.getUnit(p);
        var obj = {
            i: index,
            pro: proSource,
            unit: unit,
            ID: id,
            value: 0
        };
        var htmSv = templateS(obj);
        $t.find("tbody").append(htmSv);
    },

    changePro: function (id, $t, e) {
        var $tr = $(e.target).closest("tr");
        var proid = $tr.find(".dl_pro").val();
        var pro = proSource.filter(function (x) {
            return x.Id == proid;
        });
        var unint = adddinhluong.getUnit(pro[0]);
        var html = "";
        $.each(unint, function (i, x) {
            html += "<option value='" + x.Id + "'>" + x.Name + "</option>";
        })
        $tr.find(".dl_unit").html(html);
    },
    xoa: function (id, $t, e) {
        var $tr = $(e.target).closest("tr");
        $tr.remove();
    },
    getdata: function (id, $t, e) {
        var data = [];
        var ck = false;
        var mgs = "";
        $t.find("tbody tr").each(function (i, x) {
            var p = $(x).find(".dl_pro").val();
            var u = $(x).find(".dl_unit").val();
            var v = $(x).find(".dl_val").val();
            var value = parseFloat(v ? v : 0);
            var pId = parseInt(p ? p : 0);
            var uId = parseInt(u ? u : 0);
            // check
            if (data.length > 0) {
                $.each(data, function (ip, xp) {
                    if (xp.pro == pId)
                        ck = true;
                });
                if (ck) {
                    mgs = ("Không thể tồn tại một nguyên liệu hai lần");
                    return;
                }
            }
            //
            if (value < 0) {
                mgs = ("Trọng số phải lớn hơn hoặc bằng 0");
                ck = true;
                return;
            }
            data.push({
                pro: pId,
                unit: uId,
                value: value
            });
        });
        return { data: data, ck: ck, mgs: mgs };
    },

    save: function (id, $t, e) {
        var obj = adddinhluong.getdata(id, $t, e);
        if (obj.ck) {
            alert(obj.mgs);
            return;
        }
        var data = {
            proid: $("#myModal2").find("[name=Id]").val(),
            unitId: $("#myModal2").find("[name=unitId]").val(),
            data: obj.data
        }
        Sv.Post({
            url: "/Dinhluong/Update",
            data: { obj: data }
        }, function (rs) {
            if (rs.IntStatus == 1) {
                $("#myModal2").modal("hide");
                bootboxLamlt.alert(rs.Message);
            } else {
                alert(rs.Message);
            }
        });

    }
}