﻿var chartDatPhong = function () {
    var _this = this;

    this.FromDate = $("[name='dpDateFrom']");
    this.ToDate = $("[name='dpDateTo']");
    this.datetype = $("[name='datetype']");
    this.btnSearch = $("#btnSearch");
    this.barElement = $("#barChart");

    // change thời gian
    _this.OnChangeDateType = function () {
        // _this.datetype.on("change", _this.ChangeDateType);
    };

    _this.OnSearch = function () {
        _this.btnSearch.on("click", _this.Search);
    };

    // change date set up date
    _this.ChangeDateType = function () {
        var type = _this.datetype.val();
        switch (type) {
            case "day":
                _this.SetUpdateDay();
                break;
            case "week":
                _this.SetUpdateWeek();
                break;
            case "month":
                _this.SetUpdateMonth();
                break;
            case "quarter":
                _this.SetUpdateQuarter();
                break;
            case "year":
                _this.SetUpdateYear();
                break;
            default:
                bootboxLamlt("Không xác định được thời gian");
                break;
        }
    };
    // setup date day
    _this.SetUpdateDay = function () {

        _this.FromDate.datetimepicker({
            format: "DD/MM/YYYY",
            viewMode: 'days',
            defaultDate: Sv.AddDays(-29),
            showTodayButton: true,
            showClose: true,
            sideBySide: true,
            showClear: true,
        });
        _this.ToDate.datetimepicker({
            format: "DD/MM/YYYY",
            viewMode: 'days',
            defaultDate: Sv.AddDays(0),
            showTodayButton: true,
            showClose: true,
            showClear: true,
        });
    };
    _this.SetUpdateWeek = function () { };
    _this.SetUpdateMonth = function () { };
    _this.SetUpdateQuarter = function () { };
    _this.SetUpdateYear = function () { };

    // search
    _this.Search = function () {

        var type = _this.datetype.val();
        var d1 = _this.FromDate.val();
        var d2 = _this.ToDate.val();
        var typeReport = 1;
        switch (type) {
            case "day":
                typeReport = 1;
                break;
            case "week":
                typeReport = 2;
                break;
            case "month":
                typeReport = 3;
                break;
            case "quarter":
                typeReport = 4;
                break;
            case "year":
                typeReport = 5;
                break;
            default:
                bootboxLamlt("Không xác định được thời gian");
                return;
                break;
        }

        _this.AjaxPost("/ReportChart/DatPhongReport", { fromDate: d1, toDate: d2, type: typeReport });
    }
    // ajax search
    _this.AjaxPost = function (url, data) {
        Sv.AjaxPost({
            url: url,
            data: data
        }, function (respon) {
            if (respon.Status == "1") {
                _this.ChartUpdate(respon.Data);
            } else {
                bootboxLamlt.alert(respon.Message);
            }
        });
    };



    //-------------
    //- BAR CHART -
    //-------------
    _this.barChartOptions = {
        title: {
            display: true,
            text: "Thống kê: đặt phòng - nhận phòng đã đặt"
        },
        scales: {
            yAxes: [{ 
                gridLines: {
                    display: true,
                    //color: "rgba(255,99,132,0.2)"  
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Số lượng'
                }

            }],
            xAxes: [{
                gridLines: {
                    display: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Thời gian'
                }
            }]
        },
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false
        },
    };
    _this.barChartCanvas = _this.barElement;
    _this.barChart = undefined;
    _this.barChartData = {
        labels: [""],       
        datasets: [
          {
              label: "Số lượng đặt phòng",
              backgroundColor: "rgba(242, 125, 46, 1)",
              data: [65, 59, 80, 81, 56, 55, 40],
              borderWidth: 0
          },
          {
              label: "Số lượng khách nhận phòng đã đặt",
              backgroundColor: "rgba(0,80,117,1)",
              data: [28, 48, 40, 19, 86, 27, 90],
              borderWidth: 0
          }
        ]
    };
    
    _this.ChartUpdate = function (data) {
        if (_this.barChart !== undefined) {
            _this.barChart.destroy();
        }

        _this.barChartData.labels = data.labels;
        _this.barChartData.datasets[0].data = data.datasets[0].data;
        _this.barChartData.datasets[1].data = data.datasets[1].data;

        _this.barChart = new Chart(_this.barChartCanvas, {
            type: 'bar',
            data: _this.barChartData,
            options: _this.barChartOptions
        });

        //_this.barChartAddLabel(_this.barChartData);
    };
    _this.barChartAddLabel = function (data) {
        if (data == undefined) {
            $p.html("");
            return;
        }
        var $p = _this.barElement.parent().find(".chart-label");
        var html = '<label style="display:block;">' + data.datasets[0].label + '<i class="color-bar" style="background:' + data.datasets[0].backgroundColor + ';"></i></label>';
        html += '<label style="display:block;">' + data.datasets[1].label + '<i class="color-bar" style="background:' + data.datasets[1].backgroundColor + ';"></i></label>';
        $p.html(html);
    }

    // init
    _this.Init = function () {
        _this.OnChangeDateType();
        _this.OnSearch();
    };
}

$(document).ready(function () {
    var ctrl = new chartDatPhong();
    ctrl.SetUpdateDay();
    ctrl.Init();
});
