﻿function film_users_log() {
    var self = this;
    this.tableName = "film_users_log";
    this.idModal = "modalfilm_users_log";
    this.$table;
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_users_log_viettel').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "lengthMenu": [100000, 200000, 300000],
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + "film_users_log_viettel" + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_users_log_title").val();
                    d.sub_type = $("#search_film_users_log_sub_type").val();
                    d.statusUserLog = $("#search_film_users_log_status").val();
                    d.sub_state = $("#search_film_users_log_sub_state").val();
                    d.start_time = $("#start_time").val();
                    d.end_time = $("#end_time").val();
                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'username' }
                    , { 'data': 'email' }
                    , { 'data': 'phone' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.sub_type == 1) return "Gói ngày";
                            if (row.sub_type == 2) return "Gói tuần";
                            if (row.sub_type == 5) return "Gói tháng";
                            return "Unknown";
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            if (row.desc != null) {
                                var words = row.desc.split(',');
                                var date_dk_str = words[0].split(":")[0];
                                var date_dk = date_dk_str.substring(6, 8) + "/" + date_dk_str.substring(4, 6) + "/" + date_dk_str.substring(0, 4) + " " + date_dk_str.substring(8, 10) + ":" + date_dk_str.substring(10, 12) + ":" + date_dk_str.substring(12, 14);
                                return date_dk;
                            }
                            return "";
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            if (row.date_hb != null) {
                                var dateObj = new Date(row.date_hb);
                                var date = $.datepicker.formatDate('dd/mm/yy', dateObj);
                                var hours = (dateObj.getHours() < 10) ? "0" + dateObj.getHours() : dateObj.getHours();
                                var minutes = (dateObj.getMinutes() < 10) ? "0" + dateObj.getMinutes() : dateObj.getMinutes();
                                var seconds = (dateObj.getSeconds() < 10) ? "0" + dateObj.getSeconds() : dateObj.getSeconds();
                                var time = hours + ":" + minutes + ":" + seconds;
                                return dateObj_str = date + " " + time;
                            }
                            return "";
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            return row.datecreated === null ? '' :
                                $.datepicker.formatDate("dd/mm/yy", new Date(row.datecreated));
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.sub_state == 1) return "Đăng ký";
                            if (row.sub_state == 0) return "Không đăng ký";
                            return "";
                        }
                    }, {
                        "data": null, render: function (data, type, row) {
                            var cuphap = "Unknown";
                            if (row.desc != "" && row.desc != null) {
                                if (row.desc == "fulcmd") {
                                    return "fullcmd";
                                }
                                cuphap = row.desc.split(",")[8];
                            }
                            return cuphap;
                        }
                    }

                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.exportData = function exportData() {
        var search = $("#search_film_users_log_title").val();
        var sub_type = $("#search_film_users_log_sub_type").val();
        var sub_state = $("#search_film_users_log_sub_state").val();
        var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
        $.ajax({
            type: 'POST',
            url: "/" + "film_users_log_viettel" + "/export_data",
            data: {
                type: 0,
                search: search,
                sub_type: sub_type,
                sub_state: sub_state,
                start_time: start_time,
                end_time: end_time
            },
            success: function (data) {
                window.open(data.result);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
}