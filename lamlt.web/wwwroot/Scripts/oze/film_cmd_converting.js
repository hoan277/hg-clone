﻿function film_cmd_converting() {
    $(".ajaxInProgress").show();
    var self = this;
    this.tableName = "film_cmd_converting";
    this.idModal = "modalfilm_cmd_converting";
    this.$table;
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_cmd_converting/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.quatity = function (id) {
        $.post('/film_cmd_converting/quatity', {
            id: id
        }, function (data) {

        });
    }
    this.playDialog = function playDialog(id) {
        var myPlayer = WowzaPlayer.get('playerElement');
        if (myPlayer != null) {
            myPlayer.destroy();
        }
        if (!document.getElementById(self.idModal)) {
            self.initModal(self.idModal);
        }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/play_stream/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        self.$table = $('#example_film_cmd_converting').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            }, "pageLength": 25,
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "rowId": "id",
            "dom": '<"top"ilp<"clear">>rt<"bottom"ilp<"clear">>',
            "ajax": {
                "url": "/film_cmd_converting/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
                    delete d.columns;
                    d.search = $("#search_film_video_title").val();
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (row) {
                            return '<div class="checkbox"><label><input name="checkbox_film_video" type="checkbox" value="' + row.id + '" class="video_Checkbox" /> ' + row.id + "</label></div>";
                        }
                    }
                    , { 'data': 'title' }
                    //, { 'data': 'upload_file' }
                    , {
                        'data': null, render: function (row) {
                            if (row.upload_file != null) {
                                var upload = row.upload_file;
                                var fileName = upload.split("/");
                                var last_ele = fileName[fileName.length - 1];
                                return last_ele;
                            }
                            else {
                                return "null";
                            }
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var btn_player = '<button type="button" class="btn btn-outline-primary" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.playDialog(' + row.id + ')"><span style="color:#3f51b5">Play</span></button>';
                            return btn_player;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            return row.datecreated === null ? '' :
                                $.datepicker.formatDate("dd/mm/yy", new Date(row.datecreated));
                        }
                    }
                ]
        });
    }
    this.searchGrid = function () {
        self.$table.ajax.reload();
    }

    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default close_media" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}

function getCheckedCheckboxesFor(checkboxName) {
    var inputFolder = $("#inputFolder").val();
    var checkboxes = document.querySelectorAll('input[name="' + checkboxName + '"]:checked'),
        values = [];
    if (inputFolder != '') {
        Array.prototype.forEach.call(checkboxes, function (el) {
            values.push("+" + el.value + ";" + inputFolder);
        });
    }
    else {
        Array.prototype.forEach.call(checkboxes, function (el) {
            values.push("+" + el.value);
        });
    }


    // check da tich vao lua chon chua
    if (values.length > 0) {
        var valuesString = values.toString();
        var valuesSplit = valuesString.replace(/,/g, "");

        var fav = [];
        $.each($("input[name='checkbox_film_video']:checked"), function () {
            fav.push($(this).val());
        });
        if (fav.length > 0) {
            console.log(fav);
            //console.log(valuesSplit);
            $.ajax({
                url: "/film_cmd_converting/Update",
                type: "POST",
                data: {
                    listVideo: fav,
                    idconverting: values
                },
                dataType: "json",
                success: function () {
                    alert("Thực hiện đẩy lệnh convert thành công");
                },
                error: function () {
                    alert("Có lỗi trong quá trình xử lý");
                }
            });
        }
        else {
            setTimeout(function () {
                alert("Vui lòng chọn video bạn muốn convert");
            }, 500);
        }
    }
    else {
        alert("Vui lòng chọn kiểu bạn muốn convert");
    }
}
function getCheckedCheckboxesFor_OutSide(checkboxName) {
    var inputFolder = $("#inputFolder").val();
    var checkboxes = document.querySelectorAll('input[name="' + checkboxName + '"]:checked'),
        values = [];
    if (inputFolder != '') {
        Array.prototype.forEach.call(checkboxes, function (el) {
            values.push("+" + el.value + ";" + inputFolder);
        });
    }
    else {
        Array.prototype.forEach.call(checkboxes, function (el) {
            values.push("+" + el.value);
        });
    }


    // check da tich vao lua chon chua
    if (values.length > 0) {
        var valuesString = values.toString();
        var valuesSplit = valuesString.replace(/,/g, "");

        var fav = [];
        $.each($("input[name='checkbox_film_video']:checked"), function () {
            fav.push($(this).val());
        });
        if (fav.length > 0) {
            console.log(fav);
            //console.log(valuesSplit);
            $.ajax({
                url: "/film_cmd_converting/UpdateOutside",
                type: "POST",
                data: {
                    listVideo: fav,
                    idconverting: values
                },
                dataType: "json",
                success: function () {
                    alert("Thực hiện đẩy lệnh convert thành công");
                },
                error: function () {
                    alert("Có lỗi trong quá trình xử lý");
                }
            });
        }
        else {
            setTimeout(function () {
                alert("Vui lòng chọn video bạn muốn convert");
            }, 500);
        }
    }
    else {
        alert("Vui lòng chọn kiểu bạn muốn convert");
    }
}


var list_selected = [];
var selected_id = [];
$(document).on({
    click: function () {
        document.getElementById("checkAll").checked = false;
        GetLiveStreaming();
    }
}, "button.mdl-button");
$("#checkAll").click(function () {
    $('table input:checkbox').not(this).prop('checked', this.checked);
    if (this.checked) {
        //Add selected id to list
        $('.video_Checkbox:checked').each(function () {
            list_selected.push($(this)[0].value);
            selected_id.push($(this).parent().parent().parent().parent().attr('id'));
        });
        //Show selected id
        $("#list_selected").html(selected_id.toString());
        $("#count_selected").html(selected_id.length);
        //Active current selected
        $('tr').addClass("active");
    }
    else {
        //Remove all
        $("#list_selected").html("");
        selected_id = [];
        list_selected = [];
        //Remove class .active
        $('tr').removeClass("active");
        $("#count_selected").html(selected_id.length);
    }
});
$(document).on('change', '.video_Checkbox', function () {
    var tr_id = $(this).parent().parent().parent().parent();
    if (this.checked) {
        $(tr_id).addClass("active");
        list_selected.push(this.value);
        selected_id.push(tr_id.attr('id'));
    } else {
        $(tr_id).removeClass("active");
        list_selected.splice($.inArray(this.value, list_selected), 1);
        selected_id.splice($.inArray(this.value, selected_id), 1);
    }
    $("#list_selected").html(selected_id.toString());
    $("#count_selected").html(selected_id.length);
});
function GetLiveStreaming() {
    setTimeout(function () {
        $.ajax({
            url: '/GetLiveStreaming',
            type: "GET",
            success: function (data) {
                //for (var i = 0; i < data.length; i++) {
                //    var current_streaming = data[i].key; // 400_Unwilling.mp4.txt
                //    var film_id = current_streaming.match(/\d+_/g).toString().replace('_', ''); // 400
                //    var process_id = data[i].value;
                //    console.log(film_id);
                //    console.log(process_id);
                //    $(".live_" + film_id).hide();
                //    $('.process_' + film_id).show();
                //    $('.process_' + film_id).attr('data-process', process_id);
                //}
            },
            //error: function (xhr) { alert("Lỗi này là lỗi gì nhỉ? Google phát đi nhé: " + xhr.responseText); }
        })
    }, 3000);
}
$(document).on('click', '.addtolive', function () {
    var currentClick = $(this);
    var key = prompt("Truy cập https://youtube.com/live_dashboard để lấy key nhé!\nVui lòng nhập key live stream của Youtube:", 'f2qk-2w4q-p525-0s6c')
    if (key == null) {
        return;
    } else {
        var currentId = $(this).attr('id');
        $.ajax({
            url: '/CreateLiveStream',
            type: 'POST',
            data: {
                id: currentId,
                link: $(this).data('link'),
                mp4: $(this).data('mp4'),
                key: key
            },
            success: function (data) {
                if (data != 0) {
                    currentClick.hide();
                    alert("Đã thêm lệnh livestream, đang cập nhật tiến trình streamming, xin mời vào Youtube bật phát trực tiếp!");
                    $("a.process_" + currentId).attr("id", data);
                    $("a.process_" + currentId).show();
                    setInterval(function () {
                        GetLiveStreaming();
                    }, 5000);
                }
                //alert(data == 1 ? "Đang live rồi đó vào Youtube xem thử đi :D" : "Lỗi gì đó rồi, sửa lại đê")
            },
            error: function (xhr) { alert("Lỗi này là lỗi gì nhỉ? Google phát đi nhé: " + xhr.responseText); }
        });
    }
});
$(document).on('click', '.kill', function () {
    var currentClick = $(this);
    if (bootbox.confirm("Bạn có muốn tắt tiến trình live của video này không?", function (result) {
        if (!result) return;
        var currentId = $(currentClick).data('id');
        console.log($(currentClick).data('mp4'));
        /*Đổi tên mp4 thành process để xóa file process trên host*/
        var file_process = $(currentClick).data('mp4').replace('.mp4.txt', '.process').replace('.mov.txt', '.process').replace('.MP4.txt', '.process')
        $.ajax({
            url: '/KillProcessLive',
            type: 'POST',
            data: {
                file_txt: $(currentClick).data('mp4'),
                file_process: file_process,
                process_id: $(currentClick).data('process'),
            },
            success: function (data) {
                if (data != 0) {
                    currentClick.hide();
                    bootbox.alert("Đã tắt tiến trình live trên máy chủ.\nBạn phải vào https://www.youtube.com/live_dashboard tắt livestream nữa nhé!");
                    //window.open("https://www.youtube.com/live_dashboard");
                    $("a.live_" + currentId).show();
                    $("a.process_" + currentId).hide();
                }
            },
            error: function (xhr) { alert("Lỗi này là lỗi gì nhỉ? Google phát đi nhé: " + xhr.responseText); }
        });
    }));
});
$(document).ready(function () {
    GetLiveStreaming();
    // Live playlist video
    $(document).on('click', 'button#dachon', function () {
        if (selected_id.length <= 0) {
            alert("Chưa chọn video nào mà :(");
        } else {
            var list = [];
            console.log(selected_id);
            for (var j = 0; j < selected_id.length; j++) {
                list.push($('tr#' + selected_id[j] + ' a.upload_file_' + selected_id[j]).attr('href'));
            }
            //var key = prompt("Truy cập https://youtube.com/live_dashboard để lấy key nhé!\nVui lòng nhập key live stream của Youtube:", 'f2qk-2w4q-p525-0s6c');
            //if (key == null) {
            //    return;
            //} else {
            console.log(list);
            if (localStorage.getItem('list_selected') != '') {
                localStorage.clear();
            }
            localStorage.setItem('list_selected', list);
            console.log(localStorage.getItem('list_selected'));

            window.location.href = '/livestreaming/'
            // Code này là phiên bản đầu tiên, tuy chạy được nhưng không hiện process id, để tạm đây đã, không được xóa
            //$.ajax({
            //    url: "/CreatePlaylist",
            //    method: "POST",
            //    data: {
            //        key: key,
            //        playlist: list
            //    },
            //    success: function (data) {
            //        if (data != 0) {
            //            for (var i = 0; i < selected_id.length; i++) {
            //                $('tr#' + selected_id[i] + 'a.live_' + selected_id[i]).hide();
            //            }
            //            //bootbox.alert("Đã tắt tiến trình live trên máy chủ.\nBạn phải vào https://www.youtube.com/live_dashboard tắt livestream nữa nhé!");
            //            ////window.open("https://www.youtube.com/live_dashboard");
            //            //$("a.live_" + currentId).show();
            //            //$("a.process_" + currentId).hide();
            //        }
            //    },
            //    error: function (xhr) { alert("Lỗi này là lỗi gì nhỉ? Google phát đi nhé: " + xhr.responseText); }
            //});

            //}
        }
    });
});

var ofilm_cmd_converting = new film_cmd_converting();
$(document).ready(function () {
    ofilm_cmd_converting.setupGrid();
    $(".close_media").on("click", function () {
        ofilm_cmd_converting.close_media();
    });
});

$("#search_film_video_title").keypress(function (e) {
    if (e.which == 13) {
        $(".ajaxInProgress").show();
        ofilm_cmd_converting.searchGrid();
    }
});
