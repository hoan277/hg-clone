﻿function Expense() {
    var self = this;
    this.tableName = "Expense";
    this.objName = "oExpense";
    this.idModal = "modalExpense";
    this.$table;
    var addDays = function () {
        var dat = new Date();
        dat.setDate(dat.getDate() + parseInt(-30));

        return new Date(dat);
    }
    Sv.SetupDatePicker([{ e: $("#FromDate"), defaultDate: Sv.AddDays(-30) }, { e: $("#ToDate") }]);

    //$(".FromDate").datetimepicker({
    //    defaultDate: moment(addDays)
    //});

    this.editDialog = function (id) {
        location.href = "#/Expense/Edit?id=" + id;
    }

    this.updateDialog = function updateDialog(id) {
        showDialogLoading();
        $.get("/Expense/GetExpenseById", { id: id }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result != null) {
                $("#updateModal #id").val(data.result.Id);
                $("#updateModal #code").val(data.result.code);
                $("#updateModal #status").val(data.result.status);
                $("#updateModal").modal("show");
            }
            else {
                alert("Có lỗi khi lấy thông tin Ghi nhận tiền chi:", 1);
            }
        });

    }

    this.detail = function Detail(id) {
        location.href = "#/Expense/GetDetail?id=" + id;
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lpi><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    d.FromDate = $("input[name='FromDate']").val();
                    d.ToDate = $("[name='ToDate']").val();
                    d.search = $("#txtSearch").val();
                }
            },
            "columns":
                [
                {
                    "data": null, className: 'smallCol', render: function (data, type, row, infor) {
                        return self.$table.page.info().page + infor.row + 1;
                    }
                },
                { "data": "code", className: 'hidden-xs', "orderable": "false" },
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        if (row.datecreated != null) {
                            return moment(new Date(parseInt(row.datecreated.slice(6, -2)))).format("DD-MM-YYYY");
                        }
                        return "";
                    }
                },
                 {
                     "data": null,
                     render: function (data, type, row, infor) {
                         return convert2Money(row.amount.toString());
                     }
                 },
                { className: 'hidden-xs', "data": "note", "orderable": "false" },
                //{ "data": "status", "orderable": "false" },
                {
                    "data": null,
                    render: function (data, type, row, infor) {

                        if (row.status == 0) {
                            return "Chưa xác nhận";
                        }
                        if (row.status == 1) {
                            return "Đã xác nhận";
                        }
                        if (row.status == 2) {
                            return "Hủy";
                        }
                        return "";
                    }
                },
                {
                    "data": null, className: 'fixWidth', render: function (data, type, row) {
                        var htmlMenu = "";
                        console.log(row);
                        if (row.status == 0 && row.userid == userId) {
                            htmlMenu += ('<div class="input-table" onclick="javascript:' + self.objName + '.editDialog(' + row.Id + ')">' +
                                              '<a class="btn btn-primary" title="Chỉnh sửa" ><i class="fa fa-pencil-square-o"></i></a>' +
                                          '</div>');
                        }
                        htmlMenu += ('<div class="input-table" onclick="javascript:' + self.objName + '.detail(' + row.Id + ')">' +
                                              '<a class="btn btn-primary" title="Thông tin chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                                          '</div>');
                        htmlMenu += ('<div class="input-table" onclick="javascript:' + self.objName + '.print(' + row.Id + ')">' +
                                          '<a class="btn btn-primary" title="In" ><i class="fa fa-search-plus"></i></a>' +
                                      '</div>');
                        if (row.status == 0 && ql == 1) {
                            htmlMenu += ('<div class="input-table" onclick="javascript:' + self.objName + '.approval(' + row.Id + ', 1)">' +
                                              '<a class="btn btn-primary" title="Duyệt" ><i class="fa fa-check-square"></i></a>' +
                                          '</div>');
                        }
                        if (row.status == 0 && ql == 1) {
                            htmlMenu += ('<div class="input-table" onclick="javascript:' + self.objName + '.approval(' + row.Id + ', 2)">' +
                                                  '<a class="btn btn-primary" title="Không duyệt" ><i class="fa fa-times"></i></a>' +
                                              '</div>');
                        }
                        return htmlMenu;
                    }
                }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    $("#btnSearch").click(function () {
        self.searchGrid();
    });
    $("#btnSave").click(function () {
        var pdata = {
            Id: $("#updateModal #id").val(),
            status: $("#updateModal #status").val(),
        }
        showDialogLoading();
        $.post("/Expense/UpdateStatus", { obj: pdata }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {
                self.hideModal("updateModal");
                bootboxLamlt.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
            }
            else {
                alert("Có lỗi khi duyệt phiếu chi tiền:" + data.mess, 1);
            }
        });
    });
    this.approval = function (expenseID, status) {
        var title = "Bạn có chắc chắn muốn huỷ phiếu chi này không?";
        if (status == 1)
            title = "Bạn có chắc chắn muốn duyệt phiếu chi này không?";
        bootbox.confirm(title, function (result) {
            if (!result) return;
            var pdata = {
                Id: expenseID,
                status: status,
            }
            showDialogLoading();
            $.post("/Expense/UpdateStatus", { obj: pdata }, function (data) {
                hideDialogLoading();
                //closeDlgLoadingData();
                if (data.result > 0) {
                    bootboxLamlt.alert("Thao tác thành công", function () { self.searchGrid(); });
                }
                else {
                    alert("Có lỗi khi duyệt phiếu chi tiền:" + data.mess, 1);
                }
            });
        });
    };
    this.print = function (expenseID) {
        window.open("/Expense/ExpensePrint/" + expenseID, "", "width=300,height=300");
    };

    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}