﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}

function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}

//lich sử đặt phong của khách hàng
function viewDialogHistory(id) {
    $("#modalDetails").off('show.bs.modal').on('show.bs.modal', function () {
        $("#TittleBox").html("Lịch sử khách hàng");
        Sv.Post({
            url: "/CustomerManage/GetHistoryDetail",
            data: { id: id }
        }, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            $("#modalDetails button#btnUpdateDetail").css("display", "none");
        });
        
    }).modal("show");
}

//thông tin checkout gần nhất
function viewInfoDialog(customerId) {
    $("#modalDetails").off('show.bs.modal').on('show.bs.modal', function () {
        $("#TittleBox").html("Thông tin hóa đơn");        
        $("#modalDetails .modal-body-content").html('<p>loading..</p>');
        Sv.Post({
            url: "/CustomerCheckOut/CheckOutEdit",
            data: { customerId: customerId }
        }, function (rs) {
            $("#modalDetails .modal-body-content").html(rs);
            Sv.SetupNumberMask([{ e: $(".amount-mask") }]);
            Sv.SetupInputMask();
            $("#modalDetails button#btnUpdateDetail").css("display", "inline-block");
            
        });
    }).modal("show");
}


function notifyClientviewOrderInfo(id, checkinId) {
    console.log(id, checkinId);
    Sv.SetupModalPost({
        id: "myModal",
        url: "/CustomerCheckOut/ViewSaleOrder",
        data: { orderId: id, checkinId: checkinId },
        title: "Chi tiết hóa đơn"
    }, function () { $("#btnUpdateDetail").hide(); }, // open
        function () { }); // save

}
