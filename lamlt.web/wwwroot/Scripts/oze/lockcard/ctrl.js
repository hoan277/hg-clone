﻿
function LockCardControl() {
    var _this = this;
    _this.url = "http://localhost:7877";

    this.connect = function (data) { 
        var sttringData = "?type=sdk&locktype=" + data.lockType;
        _this.post(_this.url, sttringData);
    }

    this.make = function (data) { 
        var sttringData = "?type=make&locktype=" + data.lockType + "&roomno=" + data.lockNo + "&datefrom=" + data.dateFrom + "&dateto=" + data.dateTo;
        _this.post(_this.url, sttringData);
    }

    this.clear = function (data) { 
        var sttringData = "?type=clear&locktype=" + data.lockType;
        _this.post(_this.url, sttringData);
    }

    this.read = function (dataPost) { 
        var sttringData = "?type=read&locktype=" + dataPost.lockType;
        _this.post(_this.url, sttringData, function (result) {
            if (!result.data || result.data === "") {
                alert("Không có thông tin thẻ!");
            } else {
                var data = JSON.parse(result.data);
                var str = "Phòng thao tác: <b>" + dataPost.roomName + "</b></br>"; 
                str += "Số thẻ: " + data.CardNo + "</br>";
                str += "Mã phòng: " + data.LockNo + "</br>";
                str += "Từ ngày: " + data.DateFrom + "</br>";
                str += "Tới ngày: " + data.DateTo;
                alert(str);
            }
        });
    }

    this.post = function (urlservice, sttringData, callback) {
        if (urlservice)
            _this.url = urlservice;
        showDialogLoading();
        $.ajax({
            type: "post",
            url: _this.url + "/service" + sttringData,
            dataType: "json"
        }).done(function (data) {
            hideDialogLoading();
            if (data.result == 1) {
                if (typeof callback === "function")
                    callback(data);
                else
                    alert(data.mgs)
            }
            else {
                alert(data.mgs);
            }
        }).fail(function (xhr, status, error) {
            hideDialogLoading();
            alert("Không kết nối được tới máy chủ. Vui lòng kiểm tra lại!");
        });
    }

    this.showModal = function (url, callback,typeCheckout) {
        var htmlBtn = "";
        htmlBtn += '<button type="button" id="connectCard" class="btn btn-info" > Kết nối </button> ';
        htmlBtn += '<button type="button" id="addCard" class="btn btn-info" > Lưu thông tin </button> ';
        htmlBtn += '<button type="button" id="removeCard" class="btn btn-info" > Xóa thông tin </button>';
        htmlBtn += '<button type="button" id="infoCard" class="btn btn-info" > Thông tin </button>';
        htmlBtn += '<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';

        function getData() {
            return {
                dateFrom: $("#lockCard [name=FromDate]").val(),
                dateTo: $("#lockCard [name=ToDate]").val(),
                lockNo: $("#lockCard [name=lockNo]").val(),
                lockType: $("#lockCard [name=lockType]").val(),
                roomName: $("#lockCard [name=roomName]").val(),
            }
        }

        Sv.SetupModal({
            modalId: 'lockCard',
            modalclass: "",
            title: "Mở - Khóa thẻ từ",
            url: url,
            button: htmlBtn
        }, function ()
            {
            $("#lockCard").off('hide.bs.modal').on('hide.bs.modal', function () { if (typeof callback === 'function') callback(); });
            $("#lockCard").css("z-index", 1060);
            Sv.SetupDatePicker([{ e: $("#lockCard input[name=FromDate]"), format: "DD/MM/YYYY HH:mm:ss" },
                                { e: $("#lockCard input[name=ToDate]"), format: "DD/MM/YYYY HH:mm:ss" }]);

            if ($("#lockCard input[name=lockNo]").val() != "") {
                $("#lockCard #addCard").show();
                $("#lockCard #removeCard").show();
                $("#lockCard #infoCard").show();
                $("#lockCard #connectCard").hide();
            } else {
                $("#lockCard #addCard").hide();
                $("#lockCard #removeCard").hide();
                $("#lockCard #infoCard").hide();
                $("#lockCard #connectCard").hide();
            }

            //Thêm thông tin vào thẻ
            $("#lockCard #addCard").off("click").click(function () { _this.make(getData()); });
            //Xóa thông tin thẻ
            $("#lockCard #removeCard").off("click").click(function () { _this.clear(getData()); });
            //Thông tin thẻ
            $("#lockCard #infoCard").off("click").click(function () { _this.read(getData()); });
            //Thông tin thẻ
            $("#lockCard #connectCard").off("click").click(function () { _this.connect(getData()); });

            if (typeCheckout == 1)
            {
                _this.read(getData()); 
            }
        });
    }

}
