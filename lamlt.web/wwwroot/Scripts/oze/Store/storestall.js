﻿function searchGrid() { 
    $table.ajax.reload();
}

function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}

function validateInput() {
    var $form = $("#frmDetail");
    $form.validate({
        rules: {

        },
        messages: {

        }
    });
    $('.LimitCheck').each(function () {
        $(this).rules('add', {
            number: true,
            messages: {
                number: 'Nhập sai định dạng',
            }
        });
    });
}

function editDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        var ac = "Add";
        if (id == 0) {
            $(this).find('.modal-title').html("Thêm mới kho quầy");
        } else {
            $(this).find('.modal-title').html("Cập nhật kho quầy");
            ac = "Edit";
        } 
        $.ajax({
            cache: false,
            type: 'POST',
            url: '/StoreStall/ShowModal/',
            data: { id: id, action: ac },
            success: function (data) {
                $("#myModal .modal-body-content").html(data);
            }
        });
        //validateInput();
        $("#myModal button#btnSave").css("display", "inline");
        $("#myModal #btnSave").off("click");
        $("#myModal #btnSave").click(function () {
            if (!$("#frmDetail").valid()) return;
            var arr = [];
            if ($("#tblProduct tr input:checkbox:checked").length > 0) {
                $.each($("#tblProduct tr input:checkbox:checked"), function (i, x) {

                    var $p = $(x).closest("tr");
                    var productId = $p.find('input[name^="chkSelect"]').val();
                    var limit = $p.find('input[name^="txtLimit"]').val();
                    var note = $p.find('input[name^="txtNote"]').val();
                    if (productId != 0 && limit != 0 && productId != 'undefined' && limit != 'undefined') {
                        var item = {
                            ProductId: productId,
                            Limit: limit,
                            Note: note
                        };
                        //var queryStr = { item };
                        arr.push(item);
                    }
                });
            }
            $("#tblProduct tr").each(function (i) {
                var $fieldset = $(this);
              
                if ($('input:checkbox:eq(0)', $fieldset).is(":checked")) {
                    var productId = ($('input:checkbox:eq(0)', $fieldset).val());
                    var limit = ($('input:text:eq(0)', $fieldset).val());
                    var note = ($('input:text:eq(1)', $fieldset).val());

                    if (productId != 0 && limit != 0 && productId != 'undefined' && limit != 'undefined') {
                        var item = {
                            ProductId: productId,
                            Limit: limit,
                            Note: note
                        };
                        //var queryStr = { item };
                        arr.push(item);
                    }
                }
            });
            var name = $("#frmDetail").find("#txtName").val();
            var storeId = $("#frmDetail").find("#txtStoreId").val();
            var stallid = $("#frmDetail").find("#cboSelectStall").val();
            var pdata = { Name: name, StallId: stallid, Item: arr, Id: storeId };
            showDialogLoading();
            setTimeout(function () {
                $.post("/StoreStall/Update", { model: pdata, values: JSON.stringify(arr) }, function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                    }
                    else {
                        if (data.result == -100) {
                            alert("Quầy này đã có kho. Hãy chọn quầy khác");
                        } else {
                            alert("Có lỗi khi tạo kho");
                        }
                    }
                });
            }, 50);
        });
    });
    $("#myModal").modal("show");
}

function deleteDialog(id) { 
    confirm("Bạn có chắc chắn muốn xóa kho này không?", function (c) {
        if (!c) return;
        showDialogLoading();
        $.post("/StoreStall/Delete", { id: id }, function (data) {
            hideDialogLoading();
            //closeDlgLoadingData();
            if (data.result > 0) {
                bootboxLamlt.alert("Thao tác thành công", function () { $("#myModalDelete").modal("hide"); searchGrid(); });
            } else {
                alert("Có lỗi khi xóa kho");
            }
        });
    }) 
}

function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal .modal-content").find('.modal-title').html("Xem chi tiết");
    $("#myModal").on('show.bs.modal', function () {
        $.ajax({
            cache: false,
            type: 'POST',
            url: '/StoreStall/ShowModal/',
            data: { id: id, action: "View" },
            success: function (data) {
                $("#myModal .modal-body-content").html(data);
                $("#myModal").find("#cboSelectStall").prop('disabled', true);
            }
        });
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}
