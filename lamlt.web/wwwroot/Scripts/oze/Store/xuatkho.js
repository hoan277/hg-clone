﻿var Products = [];
var index = 0;
function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

//Array.prototype.remove = function () {
//    var what, a = arguments, L = a.length, ax;
//    while (L && this.length) {
//        what = a[--L];
//        while ((ax = this.indexOf(what)) !== -1) {
//            this.splice(ax, 1);
//        }
//    }
//    return this;
//};



function RemoveItem(el) {
    Dialog.ConfirmCustom("Xác nhận",
        "Bạn có chắc xóa sản phẩm này?",
        function () {
            var elm = $(el).closest('tr');
            var id = elm.data('id');
            elm.remove();
            for (var i = 0; i < Products.length ; i++) {
                if (Products[i].ID === id)
                    removeA(Products, Products[i]);
            }
        });
}
var Input = function () {
    var base = this;
    this.$table = $("#table");
    this.ResetForm = function (form) {

        if (form.length) {
            form.find("input, textarea, select")
                .each(function (index) {
                    var input = $(this);
                    if (input.is(":radio, :checkbox")) {
                        input.prop("checked", this.defaultChecked);
                    } else if (input.is("select")) {
                        input.val("");
                    } else if (input.parent().hasClass("date-add-picker")) {                        
                        input.parent().data("DateTimePicker").date(new Date());                         
                    } else {
                        input.val('');
                    }
                });
        }
        Sv.SetupInputMask();
    };
   
    this.Save = function () {

        if ($("#FrmOrder").valid()) {
            if (Products.length == 0) {
                alert("Vui lòng nhập sản phẩm");
                return;
            }
            Dialog.ConfirmCustom("Xác nhận", "Bạn có chắc chắn xuất kho đơn hàng này?", function () {
                var order = {};
                order.SoPhieu = $("#SoPhieu").val();
                order.SoChungTu = $("#SoChungTu").val();
                order.NgayNhapHD = $("input[name=NgayNhapHD]").val();
                order.NgayChungTu = $("input[name=NgayChungTu]").val();
                order.StoreId = $("#StoreId").val();
                Sv.AjaxPost({
                    Url: "/kho/ThemPhieuXuat",
                    Data: { order: order, orderdetails: Products }
                },
                   function (rs) {
                       console.log(rs);
                       if (rs.ResponseCode === "01") {
                           Dialog.Alert(rs.Message, Dialog.Success, function () {
                               location.href = "danhsachphieuxuat";
                           });
                       } else {
                           Dialog.Alert(rs.Message, Dialog.Error);
                       }
                   });
            })
        }
        return;
    };
     
    this.ChangeProduct = function () {
        $("#ProductCode").val("");
        $("#NhaCC").val("");
        $("#NhomDv").val("");
        $("#Unit").val("");
        $("#DonGia").val("");
        var id = $("#Product").val() != "" ? $("#Product").val() : "-1";
        Sv.AjaxPost({
            Url: "/kho/GetProductInfo",
            Data: { ProductId: id }
        },
            function (rs) {                 
                if (rs) {
                    $("#ProductCode").val(rs.Code);
                    $("#NhaCC").val(rs.SupplierID);
                    $("#NhomDv").val(rs.ProductGroupID);
                    $("#Unit").val(rs.UnitID);
                    $("#DonGia").val(rs.PriceOrder);
                } else {

                }
                var validator = $("#formProduct").validate();
                validator.element("#Product");
                validator.element("#DonGia");
            });
    };

    this.AddProduct = function () {
        if ($("#formProduct").valid()) {
            index++;
            var obj = {};
            obj.ID = index;
            obj.ProductId = $("#Product").val();
            obj.ProductCode = $("#ProductCode").val();
            obj.ProductName = $("#Product :selected").text();
            obj.CateId = $("#NhomDv").val();
            obj.CateName = $("#NhomDv :selected").text();

            obj.Price = $("#DonGia").val();
            obj.SupplierId = $("#NhaCC").val();
            obj.SupplierName = $("#NhaCC :selected").text();

            obj.Quantity = $("#SoLuongNhap").val();
            obj.UnitId = $("#Unit").val();
            obj.UnitName = $("#Unit :selected").text();
            obj.NgaySanXuat = $("input[name=NgaySanXuat]").val();
            obj.HanSuDung = $("input[name=HanSuDung]").val();        

            base.checkAddSp(obj);
             
        }
    };



    this.Uploadfile = function () {
        var data = new FormData();
        var obj = {},
            $form = $("#formadd").on();
        var files = $form.find("input[name='UploadFile']")[0].files;
        for (var i = 0; i < files.length; i++) {
            data.append("file" + i, files[i]);
        }
        Sv.AjaxPostFile({
            Url: '/Kho/UploadFile_Export',// '/Store/UploadFileExcel'
            Data: data
        }, function (rs) {
            if (rs.IntStatus == 1) {
                var htmlError = "<table id=\"examplexxxx\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">" +
                                "<thead>" +
                                    "<tr>" +
                                        " <th>STT</th>" + 
                                        " <th>Mã SP</th>" +
                                        " <th>Tên SP</th>" +
                                        " <th>ĐVT</th>" +
                                        " <th>Số lượng</th>" +
                                        " <th>Đơn giá</th>" +
                                        " <th>Lỗi</th>" +
                                    " </tr>" +
                                " </thead>";
                var htmlAlert = "<table id=\"examplexxxx\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">" +
                               "<thead>" +
                                   "<tr>" +
                                       " <th>STT</th>" +
                                       " <th>Mã SP</th>" + 
                                       " <th>Thông báo</th>" +
                                   " </tr>" +
                               " </thead>";
                var err = 0; 
                for (var j = 0; j < rs.Data.length; j++) {
                    if (rs.Data[j].Status) {
                        base.checkAddSp(rs.Data[j], function (objxxxxxxx) { 
                            bootboxLamlt.clear();
                            bootboxLamlt.alert("Cập nhật dữ liệu thành công!");
                            //bootboxLamlt.alert("Update dữ liệu sản phẩm: " + objxxxxxxx.ProductName + "!");
                        });
                    } else {
                        err = 1;
                        htmlError += base.UploadFileError(rs.Data[j]);
                    }
                }
                if (err == 1) {
                    htmlError += "</table>";
                    alert(htmlError);
                }
                 
            } else {
                Dialog.Alert(rs.Message, (rs.Status == "01" ? Dialog.Success : Dialog.Errsor), function () { });
            }
        }, function () {
            Dialog.Alert("Có lỗi xảy ra trong quá trình xử lý", Dialog.Error);
        });
    };

    this.checkAddSp = function (obj, alertbox) {
        var kiemtrasp = 0;
        // var q = 0;
        $.each(Products, function (i, x) {
            if (x.ProductId == obj.ProductId) {
                kiemtrasp = 1;
                // q += parseFloat(x.Quantity);
            }
        }); 
        // update dữ liệu sp
        if (kiemtrasp == 1) {
            Products = Products.filter(function (x) { return !(x.ProductId == obj.ProductId) }); 
            // var newq = parseFloat(obj.Quantity) + q;
            // obj.Quantity = newq;
            Products.push(obj);
            if (typeof alertbox == "function") {
                alertbox(obj);
            } else { 
                bootboxLamlt.clear();
                bootboxLamlt.alert("Update dữ liệu xuất kho sản phẩm: " + obj.ProductName + "!");
            }
            base.updateProductHtml(obj);
        }
            // thêm mới sp
        else {
            Products.push(obj);
            base.addProductHtml(obj);
        }
        base.footerTable();
        base.ResetForm($("#formProduct"));
        $("#Product").trigger('change.select2');
        index++;
    }

    this.UploadFileError = function (obj) {
        console.log(obj);
        var newRowContent =
                   "<tr data-id='" + obj.ID + "'> " +
                       "<td> " + obj.ID + " </td> " +
                       "<td> " + obj.ProductCode + " </td> " +
                       "<td> " + obj.ProductName + " </td> " + 
                       "<td> " + obj.UnitName + " </td> " +
                       "<td class=\"text-right\"> " + (obj.Quantity ? obj.Quantity : "") + " </td> " +
                       "<td class=\"text-right\"> " + (obj.PriceStr ? obj.PriceStr : "") + " </td> " +
                       "<td> " + obj.mgs + "</td> " +
                   "</tr>";
        return newRowContent;
    };

    this.AddProductFromSv = function (obj) {
        obj.ID = index;
        var p = obj.Price ? obj.Price : 0;
        var t = obj.Tax ? obj.Tax : 0;
        var q = obj.Quantity ? obj.Quantity : 0;

        obj.Total = base.getTotal(q, p, t);

        Products.push(obj); 
         
        base.addProductHtml(obj);
        base.footerTable();
        index++;
    };
    this.getTotal = function (quantity, price, tax) {
        var total = Math.round(price * quantity) + Math.round((tax / 100) * price * quantity);
        return total;
    }

    this.addProductHtmlContent = function (obj) {       
        var newRowContent = "<td> " +
                  obj.ProductName +
                  " </td> <td> " +
                  obj.ProductCode +
                  "</td> <td> " +
                  obj.SupplierName +
                  " </td> <td> " +
                  obj.CateName +
                  " </td>  <td> " +
                  (obj.NgaySanXuat ? obj.NgaySanXuat : "") +
                  " </td> <td> " +
                  (obj.HanSuDung ? obj.HanSuDung : "") +
                  "  </td> <td class='text-right'> " +
                  Sv.NumberToString2(obj.Quantity) +
                  " </td> <td>" +
                  obj.UnitName +
                  " </td> <td class='text-right'> " +
                  Sv.NumberToString2(obj.Price) +
                  "  </td>" +
                 "<td><i class='fa fa-times' onclick='unit.removeProduct(" + obj.ID + "," + obj.ProductId + "," + obj.StoreId + ")' aria-hidden='true'></i></td>";
                   
        return newRowContent;

    };
    this.addProductHtml = function (obj) { 
        var newRowContent =
                    "<tr data-id='" + obj.ID + "' data-product=" + obj.ProductId + " data-store=" + obj.StoreId + "> " +
                       base.addProductHtmlContent(obj) +
                    "</tr>";
        $("#product-table tbody").append(newRowContent);
    }

    this.updateProductHtml = function (obj) {
        var $e = $("[data-product=" + obj.ProductId + "]"); 
        $e.html(base.addProductHtmlContent(obj));
    } 

    this.footerTable = function () {
        //var total = 0;
        //$.each(Products, function (i, obj) { total += obj.Total; });
        //$("#product-table  tbody> tr").each(function (i, e) {
        //    $(e).find("td:first").html(i + 1);
        //});
        //$("#product-table tfoot").empty();
        //$("#product-table tfoot").append("<tr><td colspan=\"8\" class=\"text-right\">Tổng tiền</td><td colspan=\"2\" class=\"text-right\">" + Sv.NumberToString(total) + "</td></tr>");
    }

    this.removeProduct = function (index, id, storeid) {
        Dialog.ConfirmCustom("Xác nhận", "Bạn có chắc xóa sản phẩm này?", function () {
            $("[data-product=" + id + "]").remove();
            Products = Products.filter(function (x) { return !(x.ProductId == id ) });
            base.footerTable();
        });
    }
};
var unit = null;
$(document)
    .ready(function () {
        unit = new Input();
        $("#Product").select2({
            width: '100%'
        });

        Sv.SetupInputMask();

        Sv.AjaxPost({
            Url: "/kho/MaPhieuXuat"
        },
         function (rs) {
             $("#SoPhieu").val(rs);
         });

        $("#Product") .change(function () {
                unit.ChangeProduct();
            });
        $("#btnOk")
            .click(function () {
                unit.AddProduct();
            });
        $("#btnSave").click(function () {
            unit.Save();
        });


        $("#btnDowloadFile").click(function (e) {
            window.location = "/Kho/DownloadTemplateExport";
        });

        $("#btnAddFile").click(function () {
            document.getElementById("fileupload").value = "";
            $('#fileupload').trigger('click');
        }); 

        $('#fileupload').on('change', function () {
            var $form = $("#formadd").on();
            if ($form.valid()) {
                unit.Uploadfile();
            }
        });

    });

