﻿function film_video_view() {
    var self = this;
    this.tableName = "film_video_view";
    this.idModal = "modalfilm_video_view";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_video_view/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_video_view').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_film_video_view_title").val();
                    d.film_id = $("#search_film_video_view_film_id").val();
                    d.range_time = $("#search_film_video_view_range_time").val();
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu =
                                '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                                ' Xem </a> ' +
                                '<a href="javascript:void(0)" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                                ' Sửa </a> ' +
                                '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                ' Xóa </a> ';
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'id' }
                    , { 'data': 'videoid' }
                    , { 'data': 'userid' }
                    , { 'data': 'time_start' }
                    , { 'data': 'time_end' }
                    , {
                        'data': null, render: function (row) {
                            return row.datecreated === null ? '' :
                                $.datepicker.formatDate("dd/mm/yy", new Date(row.datecreated));
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            return row.dateupdated === null ? '' :
                                $.datepicker.formatDate("dd/mm/yy", new Date(row.dateupdated));
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var status = (row.status == 1) ? "Đã duyệt" : "Chưa duyệt";
                            return status;
                        }
                    }

                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}

function view_video() {
    $(".ajaxInProgress").show();
    var self = this;
    this.tableName = "view_video";
    this.idModal = "modalview_video";
    this.$table;
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_cmd_converting/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.quatity = function (id) {
        $.post('/film_cmd_converting/quatity', {
            id: id
        }, function (data) {

        });
    }
    this.playDialog = function playDialog(id) {
        var myPlayer = WowzaPlayer.get('playerElement');
        if (myPlayer != null) {
            myPlayer.destroy();
        }
        if (!document.getElementById(self.idModal)) {
            self.initModal(self.idModal);
        }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/play_stream/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        self.$table = $('#example_view_video').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "pageLength": 25,
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "rowId": "id",
            "dom": '<"top"ilp<"clear">>rt<"bottom"ilp<"clear">>',
            "ajax": {
                "url": "/view_video/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_title").val();
                    d.status = $("#search_status").val();
                    d.film_type = $("#search_type").val();
                    d.catalogid = $("#search_catalog").val();
                    d.range_time = $("#search_range_time").val();
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (row) {
                            return '<div class="checkbox"><label><input name="checkbox_film_video" type="checkbox" value="' + row.id + '" class="video_Checkbox" /> ' + row.film.id + "</label></div>";
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row, thumb_file) {
                            var htmlMenu = "";
                            var image = '<img src ="' + data.film.thumb_file + '" alt = "image" width = "96" height = "54"/>';
                            htmlMenu = image;
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'film.title' }
                    , { 'data': 'film.publish_year' }
                    , {
                        'data': null, render: function (data, type, row) {
                            var status = "";
                            switch (row.film.status) {
                                case 1: status = "Chờ duyệt"; break;
                                case 2: status = "Đã duyệt"; break;
                                case 3: status = "Từ chối duyệt"; break;
                                case 4: status = "Đã cập nhật ảnh"; break;
                                case 5: status = "Đang convert"; break;
                                case 6: status = "Cập nhật ảnh lỗi"; break;
                                case 7: status = "Quá hạn"; break;
                                default: status = "Nháp";
                            }
                            return status;
                        }
                    }
                    , { 'data': 'view_day' }
                    , { 'data': 'view_week' }
                    , { 'data': 'view_month' }
                    , { 'data': 'view_all' }
                ]
        });
    }
    this.searchGrid = function () {
        self.$table.ajax.reload();
    }

    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default close_media" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.export_all = function export_all() {
        alert("Quá trình này có thể mất một vài phút");
        $.ajax({
            method: 'POST',
            url: '/view_video/export',
            data: {
                type: "all",
                search: $("#search_title").val(),
                status: $("#search_status").val(),
                film_type: $("#search_type").val(),
                catalogid: $("#search_catalog").val(),
                range_time: $("#search_range_time").val(),
            },
            success: function (data) {
                window.open(data.result);
            },
            error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
}