﻿function searchGrid() {
    //$table.bootstrapTable('refresh');
    $table.ajax.reload();
}
function getParams() {
    return { s: $("#txtSearch").val(), offset: $table.bootstrapTable('getOptions').pageNumber, limit: $table.bootstrapTable('getOptions').pageSize }
}
function editDialog1(id) {
    BootstrapDialog.show({
        title: 'Chỉnh sửa đơn vị',
        message: $('<div></div>').load('/Unit/Edit/' + id),
        buttons: [{
            label: 'Lưu',
            action: function (dialog) {
                if (!$("#dummyUnit").valid()) return;
                var pdata = getFormData($("#dummyUnit"));
                showDialogLoading();
                $.post("/Unit/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                    }
                    else {
                        if (data.result == -2) {
                            alert("Đơn vị tính không được phép thay đổi");
                        } else {
                            alert("Có lỗi khi tạo sản phẩm");
                        }
                    }
                });
            }
        }, {
            label: 'Close',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}
function editDialog(id) {

    $("#myModal").off('show.bs.modal');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").html('<p>loading..</p>');
        $("#myModal .modal-body-content").load("/Unit/Edit/" + id);
        $("#myModal button#btnSave").css("display", "inline");
        $("#btnSave").off("click");
        $("#btnSave").click(function () {
            setupValidate();
            if (!$("#dummyUnit").on().valid()) return;
            var pdata = getFormData($("#dummyUnit"));

            showDialogLoading();
            $.post("/Unit/update", { obj: pdata }, function (data) {
                hideDialogLoading();
                //closeDlgLoadingData();
                if (data.result > 0) {
                    bootboxLamlt.alert("Thao tác thành công", function () { $("#myModal").modal("hide"); searchGrid(); });
                }
                else {
                    if (data.result == -7) {
                        alert("Mã đơn vị đã tồn tại");
                    } else if (data.result == -8) {
                        alert("Tên sản phẩm đơn vị đã tồn tại");
                    } else if (data.result == -2) {
                        alert("Đơn vị tính không được phép thay đổi");
                    } else {
                        alert("Có lỗi khi tạo sản phẩm");
                    }
                }
            });
        });
    });
    $("#myModal").modal("show");
}
function deleteDialog(id) {
    if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
        if (!result) return;
        $.post("/Unit/delete", { id: id }, function (data) {
            if (data.result > 0) {
                bootboxLamlt.alert("Thao tác thành công", function () { searchGrid(); });
    }
    else {
            if (data.result == -1) {
                bootbox.alert("Đơn vị đang được sử dụng trong danh mục sản phẩm, kiểm tra lại");
    } else {
                alert("Có lỗi khi xóa đơn vị sản phẩm:" + data.mess);
    }
    }
    });
    }));
}
function viewDialog(id) {
    $("#myModal").off('show.bs.modal');
    $("#myModal .modal-body-content").html('<p>loading..</p>');
    $("#myModal").on('show.bs.modal', function () {
        $("#myModal .modal-body-content").load("/Unit/GetDetail/" + id);
        $("#myModal button#btnSave").css("display", "none");
    });
    $("#myModal").modal("show");
}


function setupValidate() {
    var form = $('#dummyUnit').on();
    form.validate({
        rules: {
            Name1: {
                required: true
            },
            Value: {
                required: true,
                priceMin: true,
                number: true
            },
            Description: {
                required: true
            }
        },
        messages: {
            Name1: {
                required: 'Vui lòng nhập tên đơn vị'
            },
            Value: {
                required: 'Vui lòng nhập trọng số',
                priceMin: 'Trọng số không được phép âm',
                number: 'Trọng số không đúng định dạng'
            },
            Description: {
                required: 'Vui lòng nhập mô tả cho đơn vị'
            }
        }
    });
}