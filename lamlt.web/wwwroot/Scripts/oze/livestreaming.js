﻿function livestreaming() {
    var self = this;
    this.tableName = "livestreaming";
    this.idModal = "modallivestreaming";
    this.$table;
    $("#tatca").on("click", function () {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "tatca"
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    });
    $("#dachon").on("click", function () {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "dachon",
                list_id: selected_id.toString()
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    });
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/livestreaming/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
				/*
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data)
					{
						hideDialogLoading();
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});*/
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_livestreaming').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "rowId": "id",
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = $("#search_livestreaming_title").val();
                    d.live_status = $("#search_livestreaming_status").val();
                    d.live_type = $("#search_livestreaming_type").val();
                    d.start_time = $("#start_time").val();
                    d.end_time = $("#end_time").val();
                    d.userid = $("#search_livestreaming_userid").val();
                    d.title = $("#search_livestreaming_title").val();
                    d.type = $("#search_livestreaming_type").val();
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = '';
                            var htmlMenu =
                                '<div class="checkbox"><span class="id">ID: <label><input class="export_selected" type="checkbox" id="export_selected" name="export_selected" type="checkbox">' + row.id + '</label></span></div><div class="tools"><a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                                ' Xem</a>' +
                                '<a style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                                ' Sửa</a>' +
                                '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                ' Xóa</a></div>';
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'title' }
                    , { 'data': 'processid' }
                    //, { 'data': 'cmd' }
                    , { 'data': 'video' }
                    , { 'data': 'resolution' }
                    , { 'data': 'type' }
                    , { 'data': 'username' }
                    , {
                        'data': null, render: function (data, type, row) {
                            var status_html = "";
                            switch (row.status) {
                                case "living":
                                    status_html = '<a data-id="' + row.id + '" class="kill kill_' + row.processid + '" style="color:red;font-weight:bold;" href="javascript:void(0)" title="Tắt tiến trình đang live của video này?" data-process="' + row.processid + '"><img style="width:50px" src="/live.gif"> <br /><i class="fa fa-pause"></i> Stop</a>';
                                    break;
                                case "download":
                                    status_html = '<img style="width:70px" src="/download.gif" />';
                                    break;
                                case "downloading":
                                    status_html = '<img style="width:30px" src="/downloading.gif" />';
                                    break;
                                case "wait":
                                    status_html = '<span class="label label-warning">Đang đợi</span>'; break;
                                case "stop":
                                    status_html = '<span class="label label-danger">Đã dừng</span><hr />';
                                    status_html += '<a id="startcmd" data-id="' + row.id + '" class="btn btn-block btn-success" href="javascript:void(0)" title="Bật livetream">Start</a>';
                                    break;
                                default:
                                    if (row.status.includes('/')) {
                                        var percent = row.status.split('/');
                                        var cur = percent[0] != 'undefined' ? "Đã tải " + percent[0] : '0';
                                        if (percent[0] == "0") {
                                            cur = "Đang tải 1";
                                        }
                                        var all = percent[1] != 'undefined' ? percent[1] : '0';
                                        status_html = '<div class="btn-group-vertical"><button type="button btn-block" class="btn btn-success">Tổng ' + all + '</button><button type="button btn-block" class="btn btn-warning">' + cur + '</button></div>';
                                    } else {
                                        status_html = '<span class="label label-warning">Đã tải xong, chuẩn bị live</span>'
                                    }
                                    break;

                            }
                            return status_html;
                        }
                    }
                    , { 'data': 'datecreated' }
                    , { 'data': 'note' }
                    , { 'data': 'live_catalog_title' }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}