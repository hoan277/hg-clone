﻿function film_contact() {
    var self = this;
    this.tableName = "film_contact";
    this.idModal = "modalfilm_contact";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_contact/Update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.activeDialog = function activeDialog(id) {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/active/" + id,
            success: function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    $("#" + self.idModal).modal("hide"); self.searchGrid();
                }
                else {
                    alert("Có lỗi khi xử lý :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.quatity = function (id) {
        $.post('/film_contact/quatity', {
            id: id
        }, function (data) {

        });
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_contact').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    //d.search = $("#txtSearch").val();
                    delete d.columns;
                    d.search = $("#search_film_contact_title").val();
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var info2 =
                                '<div><button type="button" class="btn btn-outline-primary tool_btn tool_btnfirst" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')"><span style="color:#3f51b5">Chi tiết</span></button></div>';
                            var edit2 =
                                '<div><button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')"><span style="color:#f39c12">Sửa</span></button></div>';
                            var delete2 =
                                '<div><button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')"><span style="color:#761c19">Xóa</span></button></div>';
                            var active2 =
                                '<div><button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')"><span style="color:#f17c2d">Duyệt</span></button></div>';
                            var update_episode =
                                '<div><button type="button" class="btn btn-outline-primary tool_btn" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.updateDialog(' + row.id + ')"><span style="color:#f17c2d">Cập nhật tập</span></button></div>';
                            var htmlMenu = ""
                            if (row.status == 1) {
                                htmlMenu = info2 + edit2 + delete2;
                            } else {
                                htmlMenu = info2 + edit2 + delete2 + active2;
                            }
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'id' }
                    , { 'data': 'userid' }
                    , { 'data': 'full_name' }
                    , { 'data': 'phone' }
                    , { 'data': 'email' }
                    , { 'data': 'content' }
                    , {
                        'data': null, render: function (row) {
                            return row.datecreated === null ? '' :
                                $.datepicker.formatDate("dd/mm/yy", new Date(row.datecreated));
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '           <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button> <button type="button" id="btnSave' + self.tableName + '" class="btn btn-success" >Lưu</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}
