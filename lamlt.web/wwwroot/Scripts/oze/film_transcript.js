﻿function film_transcript() {
    var self = this;
    this.tableName = "film_transcript";
    this.idModal = "modalfilm_transcript";
    this.$table;
    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "inline");
            $("#" + self.idModal + " button#btnGenSrt" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();
                $("button#btnGenSrt").trigger("click");
                $.post("/film_transcript/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        showToast("Thao tác thành công", "success", 10000, "top-end");
                        $("#" + self.idModal).modal("hide");
                    }
                    else {
                        showToast("Có lỗi: " + data.mess, "success", 10000, "top-end");
                    }
                });
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }
        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
            $("#" + self.idModal + " button#btnReport" + self.tableName).css("display", "none");
            $("#" + self.idModal + " button#btnGenSrt" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    var data_null = "<strong style='color:red'>Chưa cập nhật</strong>";
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_transcript').DataTable({
            fixedHeader: {
                header: true
                //footer: true
            },
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": 'lp<"top" rt><"bottom" lip><"clear">',
            "rowId": "id",
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    delete d.columns;
                    d.search = String($("#s_title").val());
                    d.time_range = String($('#s_timerange').val());

                    stt = 1;
                }
            },
            "columns":
                [
                    {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = "";
                            var info = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="color:#8BC34A" title="Xem"></span>' +
                                '</a>  ';
                            var edit = '<a href="javascript:void(0)" style="color: #dd4b39;font-weight: bold;" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#dd4b39" title="Sửa"></span>' +
                                '</a>  ';
                            var delete1 = '<a href="javascript:void(0)" style="color: red;font-weight: bold;" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red" title="Xóa"></span>' +
                                '</a>  ';
                            var active = '<a style="color: #00a65a;font-weight: bold;" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')">' +
                                '<span class="glyphicon glyphicon-check" aria-hidden="true" style="color:#8BC34A" title="Duyệt"></span>' +
                                '</a>  ';
                            htmlMenu = info + edit + delete1;
                            if (row.status != 1) {
                                htmlMenu = info + edit + delete1 + active;
                            }
                            return htmlMenu;
                        }
                    }
                    , { 'data': 'title' }
                    , {
                        "data": null, render: function (data, type, row) {
                            if (row.desc == null) return "";
                            if (row.desc.length <= 20) return row.desc;
                            return row.desc.substr(0, 19) + "...";
                        }
                    }
                    , { 'data': 'file' }
                    , { 'data': 'code' }
                    , { 'data': 'language' }
                    , {
                        'data': null, render: function (row) {
                            var datecreated = "";
                            if (row.datecreated != null) {
                                datecreated = moment(row.datecreated).format('DD/MM/YYYY HH:mm:ss');
                            }
                            return datecreated;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var dateupdated = "";
                            if (row.dateupdated != null) {
                                dateupdated = moment(row.dateupdated).format('DD/MM/YYYY HH:mm:ss');
                            }
                            return dateupdated;
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            var status = "";
                            if (row.status == 1) { status = "Đã duyệt"; }
                            else { status = "Chưa duyệt"; }
                            return status;
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '<hr/>';
        html += '       <button type="button" id="btnSave' + self.tableName + '" class="btn btn-warning" >Lưu và Gen lại Srt</button> <button type="button" class="btn btn-success" id="btnGenSrt">Gen lại srt </button><button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
    this.activeDialog = function activeDialog(id) {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/UpdateStatus",
            data: {
                id: id,
                status: "Đã duyệt"
            },
            success: function (data) {
                hideDialogLoading();
                if (data.result > 0) {
                    $("#" + self.idModal).modal("hide"); self.searchGrid();
                }
                else {
                    alert("Có lỗi khi xử lý :" + data.mess);
                }
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
    //================================================= xuất excel ======================================================================
    this.exportAll = function exportAll() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "tatca",
                search: String($("#s_title").val()),
                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    this.exportList = function exportList() {
        $.ajax({
            type: 'POST',
            url: "/" + self.tableName + "/exportvialib",
            data: {
                type: "dachon",
                list_id: selected_id.toString(),
                search: String($("#s_title").val()),
                time_range: String($('#s_timerange').val()),
            },
            success: function (data) {
                window.open(data.data);
            }, error: function () {
                alert("Lỗi không xuất được file!");
            }
        });
    }
    //================================================= xuất excel ======================================================================
    this.upload_file = function upload_file() {
        var fd = new FormData();
        var files = $('#file1')[0].files[0];
        fd.append('file', files);
        $.ajax({
            url: "/" + self.tableName + "/upload_file",
            data: fd,
            contentType: false,
            processData: false,
            type: 'post',
            success: function (data) {
                $("#formfilm_transcript #desc").val(data).select();
                $("#formfilm_transcript #desc").scrollTop($("#formfilm_transcript #desc").scrollHeight);
                showToast("Đã thay thế nội dung file sub thành công", "success", 10000, "top-end");
                var input = $("#formfilm_transcript input[type='file']");
                input.html(input.html()).val('');
            },
            error: function (data) {
                alert("Có lỗi khi upload");
            }
        });
    }
    this.LoadProgressBar = function LoadProgressBar(result) {
        var progressbar = $("#progressbar-5");
        var progressLabel = $(".progress-label");
        progressbar.show();
        $("#progressbar-5").progressbar({
            //value: false,
            change: function () {
                progressLabel.text(
                    progressbar.progressbar("value") + "%");  // Showing the progress increment value in progress bar
            },
            complete: function () {
                progressLabel.text("Loading Completed!");
                progressbar.progressbar("value", 0);  //Reinitialize the progress bar value 0
                progressLabel.text("");
                progressbar.hide(); //Hiding the progress bar
                var markup = "<tr><td>" + result + "</td><td><a href='#' onclick='DeleteFile(\"" + result + "\")'><span class='glyphicon glyphicon-remove red'></span></a></td></tr>"; // Binding the file name
                $("#ListofFiles tbody").append(markup);
                $('#Files').val('');
                $('#FileBrowse').find("*").prop("disabled", false);
            }
        });
        function progress() {
            var val = progressbar.progressbar("value") || 0;
            progressbar.progressbar("value", val + 1);
            if (val < 99) {
                setTimeout(progress, 25);
            }
        }
        setTimeout(progress, 100);
    }
}