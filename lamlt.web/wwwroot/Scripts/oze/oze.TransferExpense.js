﻿// chốt sổ giao ca - lễ tân
var $table;

$(document).ready(function () {

    //Sv.SetupInputMask();
    Sv.SetupDatePicker([{ e: $("input[name='FromDate']"), defaultDate: Sv.AddDays(-30) }, { e: $("input[name='ToDate']") }]);

    $("#btnSearch").click(function () { $table.ajax.reload(); });

    $("#btnRefresh").click(function () {
        Sv.SetDateTime($("input[name='FromDate']"), Sv.AddDays(-30));
        Sv.SetDateTime($("input[name='ToDate']"), null);
    });

    showDialogLoading();
    var tableOption = {
        "ajax": {
            "url": "/TransferExpense/List",
            "data": function (d) {
                d.columns = "";
                d.FromDate = $("input[name='FromDate']").val();
                d.ToDate = $("input[name='ToDate']").val();
                d.userId = $("select[name='userId']").val();
                d.confirmUserId = $("select[name='confirmUserId']").val();
                d.search = "";
            }
        },
        "columns":
            [
        // STT
                {
                    "data": null,
                    render: function (data, type, row, infor) {
                        return $table.page.info().page + infor.row + 1;
                    }
                },
        // Nhân viên
                {
                    "data": "transferUserName",
                },
        // ngay thanh toan
            {
                "data": "transferDate",
                render: function (data, type, row, infor) {
                    return Sv.DateToString(row.transferDate, "DD/MM/YYYY HH:ss");
                }
            },
        // Nhân viên
            {
                "data": "confirmUserName",
            },
      // ngay thanh toan
            {
                "data": "confirmDate",
                render: function (data, type, row, infor) {
                    return Sv.DateToString(row.confirmDate, "DD/MM/YYYY HH:ss");
                }
            },
            // thao tác
            {
                "data": "money",
                render: function (data, type, row, infor) {
                    return Sv.NumberToString(row.money);
                }
            },
             {
                 "data": "statusName",

             },
        // thao tác
            {
                "data": null,
                render: function (data, type, row, infor) {
                    var htmlMenu = "";

                    htmlMenu += ('<div class="input-table" onclick="javascript:openView(' + row.Id + ')">' +
                                '<a href="javascript:void()" class="btn btn-primary" title="Chi tiết" ><i class="fa fa-info-circle"></i></a>' +
                            '</div>');
                    // quản lý được phép xác nhận
                    if (isManage() && row.status == 0) {
                        htmlMenu += ('<div class="input-table" onclick="javascript:openConfirm(' + row.Id + ')">' +                                
                                '<a href="javascript:void()" class="btn btn-primary" title="Xác nhận"><i class="fa fa-check-square-o"></i></a>' +
                            '</div>');
                    }
                    // lễ tân được phép sửa xóa nếu trạng thái == 0
                    if (row.status == 0) {
                        htmlMenu += ('<div class="input-table input-table2" onclick="javascript:openEdit(' + row.Id + ')">' +
                                   '<a href="javascript:void()" class="btn btn-primary" title="Sửa"><i class="fa fa-pencil-square-o"></i></a>' +
                               '</div>' +
                               '<div class="input-table input-table2" onclick="javascript:openRemove(' + row.Id + ')">' +
                                   '<a href="javascript:void()" class="btn btn-primary" title="Xóa"><i class="fa fa-trash"></i></a>' +
                               '</div>');
                    }
                    return htmlMenu;
                }
            }
            ],
        "initComplete": function (settings, json) {
            hideDialogLoading(); hideDialogLoading();
        },
    };

    $table = Sv.SetUpDataTable($('#example'), Sv.DataTableOption(tableOption));

    // add
    $("#btnAdd").click(openAdd);

});

function isManage() {
    var v = $("[name=isManage]").val();
    if (v === "true")
        return true;
    else
        return false;
}
// add
function openAdd() {
    Sv.SetupModal({
        id: "modalDetails_openAdd",
        url: "/TransferExpense/ViewAdd",
        title: "Nộp tiền"
    },
        function () {           
            $("#btnSave").html("<i class='fa fa-save'></i> Lưu").show();
        }, // open
        function () { // save
            if ($("#formAddTransferExpense").valid()) {
                var data = Sv.FormGetData($("#formAddTransferExpense"));
                //console.log(data, t);
                Sv.AjaxPost({
                    url: "/TransferExpense/SaveAdd",
                    data: { model: data }
                }, function (respon) {
                    if (respon.IntStatus == 1) {
                        bootboxLamlt.alert("Thao tác thành công");
                        $("#modalDetails_openAdd").modal("hide");
                        $table.ajax.reload();
                    }
                    else {
                        bootboxLamlt.alert("Có lỗi:" + respon.Message);
                    }
                });
            }
        });
}
// view
function openView(id) {
    Sv.SetupModal({
        id: "modalDetails_openView",
        url: "/TransferExpense/ViewEdit/" + id,
        title: "Thông tin nộp tiền"
    },
        function () {
            // disable
            $("[name=transferUserId]").prop("disabled", true);
            $("[name=money]").prop("disabled", true);
            $("[name=note]").prop("disabled", true);
            $("#btnSave").hide();
        }, // open
        function () {  
        });
}
// xác nhận
function openConfirm(id) {
    Sv.SetupModal({
        id: "modalDetails_cf",
        url: "/TransferExpense/ViewConfirm/" + id,
        title: "Xác nhận nộp tiền"
    },
        function () {
            // disable
            $("[name=transferUserId]").prop("disabled", true);
            $("[name=money]").prop("disabled", true);
            $("[name=note]").prop("disabled", true);
            $("#btnSave").html("<i class='fa fa-check'></i> Xác nhận").show();
        }, // open
        function () { // save
            if ($("#formAddTransferExpense").valid()) {
                var data = Sv.FormGetData($("#formAddTransferExpense"));
                //console.log(data, t);
                Sv.AjaxPost({
                    url: "/TransferExpense/SaveConfirm",
                    data: { model: data }
                }, function (respon) {
                    if (respon.IntStatus == 1) {
                        bootboxLamlt.alert("Thao tác thành công");
                        $("#modalDetails_cf").modal("hide");
                        $table.ajax.reload();
                    }
                    else {
                        bootboxLamlt.alert("Có lỗi:" + respon.Message);
                    }
                });
            }
        });
}
// edit
function openEdit(id) {
    Sv.SetupModal({
        id: "modalDetails_openEdit",
        url: "/TransferExpense/ViewEdit/" + id,
        title: "Sửa nộp tiền"
    },
        function () {
            // disable
            //$("[name=transferUserId]").prop("disabled", true);
            //$("[name=money]").prop("disabled", true);
            //$("[name=note]").prop("disabled", true);
            $("#btnSave").html("<i class='fa fa-check'></i> Lưu").show();
        },
        function () { // save
            if ($("#formAddTransferExpense").valid()) {
                var data = Sv.FormGetData($("#formAddTransferExpense")); 
                Sv.AjaxPost({
                    url: "/TransferExpense/SaveEdit",
                    data: { model: data }
                }, function (respon) {
                    if (respon.IntStatus == 1) {
                        bootboxLamlt.alert("Thao tác thành công");
                        $("#modalDetails_openEdit").modal("hide");
                        $table.ajax.reload();
                    }
                    else {
                        bootboxLamlt.alert("Có lỗi:" + respon.Message);
                    }
                });
            }
        });
}
// xóa
function openRemove(id) {
    Dialog.ConfirmCustom("Xác nhận xóa", "Bạn chắc chắn muốn xóa thông tin nộp tiền này ko?", function () {
        Sv.AjaxPost({
            url: "/TransferExpense/RemoveItem",
            data: { id: id }
        }, function (respon) {
            if (respon.Status == "1") {
                bootboxLamlt.alert("Thao tác thành công", function () {
                    $("#modalDetails").modal("hide");
                    $table.ajax.reload();
                });
            }
            else {
                bootboxLamlt.alert("Có lỗi:" + respon.Message);
            }
        });
    });
}