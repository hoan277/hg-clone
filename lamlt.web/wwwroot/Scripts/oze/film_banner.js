﻿function film_banner() {
    var self = this;
    this.tableName = "film_banner";
    this.idModal = "modalfilm_banner";
    this.$table;

    this.editDialog = function (id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/Edit/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "inline");
            $("#btnSave" + self.tableName).off("click");
            $("#btnSave" + self.tableName).click(function () {
                if (!$("#form" + self.tableName).valid()) return;
                var pdata = getFormData($("#form" + self.tableName));
                showDialogLoading();

                $.post("/film_banner/update", { obj: pdata }, function (data) {
                    hideDialogLoading();
                    //closeDlgLoadingData();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                });
				/*
				$.ajax({
					type: 'POST',
					url: "/"+ self.tableName +"/update",
					data: JSON.stringify (pdata),
					success: function(data) 
					{ 
						hideDialogLoading();						
						if (data.result > 0)
						{
							bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
						}
						else {
							alert("Có lỗi khi tạo :"+data.mess);
						}
					},
					contentType: "application/json",
					dataType: 'json'
				});*/
            });
            $("#" + self.idModal).off('show.bs.modal');
        });
        $("#" + self.idModal).modal("show");
    }
    this.deleteDialog = function (id) {
        if (bootbox.confirm("Bạn chắc chắn muốn xóa", function (result) {
            if (!result) return;
            $.ajax({
                type: 'DELETE',
                url: "/" + self.tableName + "/Delete/" + id,
                success: function (data) {
                    hideDialogLoading();
                    if (data.result > 0) {
                        bootbox.alert("Thao tác thành công", function () { $("#" + self.idModal).modal("hide"); self.searchGrid(); });
                    }
                    else {
                        alert("Có lỗi khi tạo :" + data.mess);
                    }
                },
                contentType: "application/json",
                dataType: 'json'
            });
        }));
    }
    this.viewDialog = function viewDialog(id) {
        if (!document.getElementById(self.idModal)) { self.initModal(self.idModal); }

        $("#" + self.idModal).off('show.bs.modal');
        $("#" + self.idModal + " .modal-body-content").html('<p>loading..</p>');
        $("#" + self.idModal).on('show.bs.modal', function () {
            $("#" + self.idModal + " .modal-body-content").load("/" + self.tableName + "/GetViewDetail/" + id);
            $("#" + self.idModal + " button#btnSave" + self.tableName).css("display", "none");
        });
        $("#" + self.idModal).modal("show");
    }
    this.setupGrid = function () {
        showDialogLoading();
        self.$table = $('#example_film_banner').DataTable({
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function (settings, json) {
                hideDialogLoading();
            },
            "dom": '<"top">rt<"bottom" lip><"clear">',
            "ajax": {
                "url": "/" + self.tableName + "/List",
                "data": function (d) {
                    d.search = String($("#s_title").val());
                    d.arr_display = String($("#s_display").val());
                    d.arr_catalogId = String($("#s_catalogId").val());
                    d.arr_typeClient = String($("#s_typeClient").val());
                    d.arr_search_date = String($('#s_timerange').val());
                    delete d.columns;
                }
            },
            "columns":
                [
                    { 'data': 'id' }
                    , { 'data': 'title' }
                    , { 'data': 'position' }
                    , { 'data': 'prior' }
                    , {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = '<img src ="' + row.file + '" alt = "image" width = "100" height = "100"/>';
                            return htmlMenu;
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            var htmlMenu = 'http://lalatv.com.vn' + row.html;
                            return '<a href="' + htmlMenu + '" target="_blank">' + htmlMenu + '</a>';
                            return htmlMenu;
                        }
                    }
                    , {
                        'data': null, render: function (data, type, row) {
                            var str_display = "";
                            switch (row.display) {
                                case -1: str_display ="Trang chủ" ;break;
                                case 7: str_display ="Phim truyện" ;break;
                                case 5: str_display ="Truyện cổ tích" ;break;
                                case 8: str_display ="Truyền hình" ;break;
                                case 6: str_display ="Ca nhạc thiếu nhi" ;break;
                                case 11: str_display ="Phim Âu Mỹ" ;break;
                                case 9: str_display ="Phim Việt Nam" ;break;
                                case 10: str_display ="Phim Trung Quốc" ;break;
                                case 4: str_display ="Giáo dục" ;break;
                                case 3: str_display ="Hoạt hình Việt Nam" ;break;
                                case 100: str_display ="Live" ;break;
                                case 17: str_display ="Hoạt hình nước ngoài" ;break;
                                case 21: str_display ="Nhạc trẻ" ;break;
                                case 22: str_display ="Nhạc cover" ;break;
                                case 23: str_display ="Nhạc Acountic" ;break;
                                case 24: str_display ="Nhạc dân ca" ;break;
                                case 25: str_display ="Hoạt hình" ;break;
                                case 26: str_display = "Ca nhạc"; break;
                                default: str_display = str_display; break;
                            }
                            return str_display;
                        }
                    }
                    , { 'data': 'film_catalog_title' }
                    , {
                        "data": null, render: function (data, type, row) {
                            var type_client_str = "";
                            if (row.type_client != null) {
                                if (row.type_client == -1) {
                                    type_client_str = "Tất cả";
                                }
                                else if (row.type_client == 1) {
                                    type_client_str = "Website";
                                }
                                else if (row.type_client == 2) {
                                    type_client_str = "Mobile";
                                }
                                else if (row.type_client == 3) {
                                    type_client_str = "Tablet";
                                }
                                else {
                                    type_client_str = "Không hiện";
                                }
                            }
                            return type_client_str;
                        }
                    }
                    , { 'data': 'desc' }
                    , {
                        'data': null, render: function (row) {
                            return row.datecreated === null ? '' :
                                $.datepicker.formatDate("dd/mm/yy", new Date(row.datecreated));
                        }
                    }
                    , {
                        'data': null, render: function (row) {
                            return row.dateupdated === null ? '' :
                                $.datepicker.formatDate("dd/mm/yy", new Date(row.dateupdated));
                        }
                    }
                    , {
                        "data": null, render: function (data, type, row) {
                            var info =
                                '<div><button type="button" class="btn btn-outline-primary" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.viewDialog(' + row.id + ')"><span style="color:#3f51b5">Chi tiết</span></button></div>';
                            var edit =
                                '<button type="button" class="btn btn-outline-primary" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.editDialog(' + row.id + ')"><span style="color:#f39c12">Sửa</span></button></div>';
                            var delete1 =
                                '<div><button type="button" class="btn btn-outline-primary" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.deleteDialog(' + row.id + ')"><span style="color:#761c19">Xóa</span></button></div>';
                            var active =
                                '<div><button type="button" class="btn btn-outline-primary" href="javascript:void(0)" onclick="javascript:o' + self.tableName + '.activeDialog(' + row.id + ')"><span style="color:#f17c2d">Duyệt</span></button></div>';
                            var htmlMenu = info + edit + delete1;
                            return htmlMenu;
                        }
                    }
                ]
        });
    }
    this.searchGrid = function searchGrid() {
        self.$table.ajax.reload();
    }
    this.initModal = function (idModal) {
        var html = '<div id="' + idModal + '" class="modal fade" role="dialog">';
        html += '<div class="modal-dialog modal-lg">';
        html += '    <div class="modal-content">';
        html += '        <div class="modal-header">';
        html += '            <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '            <h4 class="modal-title">' + self.tableName + '</h4>';
        html += '        </div>';
        html += '        <div class="modal-body">';
        html += '            <div class="modal-body-content">';
        html += '                <p>Loading...</p>';
        html += '            </div>';
        html += '</div>';
        html += '        <div class="modal-footer">';
        html += '            <button type="button" id="btnSave' + self.tableName + '" class="btn btn-default" >Lưu</button> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
        $("#" + idModal).modal();
    }
    this.hideModal = function (idModal) {
        $('#' + idModal).modal('hide');
    }
}