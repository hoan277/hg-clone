﻿var cookieNameNotify = "NotifyObject"; 

function setupViewNotification() { 
    var $e = $("#divNotification");
    $e.load("/Notification/setupviewNotify" );
}

function viewNotification(e, typeNotification, obj) { 
    Cookie.create(cookieNameNotify, JSON.stringify(obj));
    // post
    $.post("/Notification/viewNotification", { type: typeNotification });
    // load notify
    var $total = $("#divNotification .notify-total");
    var $totalItem = $("#divNotification .typenotification" + typeNotification);
    var total = parseInt($total.text());
    var totalItem = parseInt($totalItem.data("totalitem"));
    // tổng
    $total.text(total - totalItem);
    $("#divNotification .notify-total2").text(total - totalItem);
    // remove item
    $totalItem.parent().remove();
    if (window.location.href == $(e.target).attr("href")) {
        notifyLazyEvent();
    } 
}

function notifyLazyEvent() { 
    var objStr = Cookie.get(cookieNameNotify); 
    if (objStr == undefined || objStr.length == 0)
        return;
    try {
        var obj = JSON.parse(objStr);
        var check = viewNotifyClient(obj)
        if (check == true)
            Cookie.create(cookieNameNotify, null, 0);
        else
            viewNotifyClientAuto();
    } catch (e) {
        return;
    }
}

function viewNotifyClient(obj) {
    switch (obj.Type) {
        case 1:
            // chi tiết hủy phòng
            if (obj.Details == true) {
                if (typeof viewCheckInDetails !== "function") return false;
                viewCheckInDetails(obj.Item.dataid);
                return true
            } else
                return true;
            break;
        case 2:
            // Sửa hóa đơn của khách đã đi
            if (obj.Details == true) {
                if (typeof notifyClientviewOrderInfo !== "function") return false;
                var data = JSON.parse(obj.Data);
                notifyClientviewOrderInfo(data.Id, data.CheckInID);
                return true
            } else
                return true;
            break;
        case 5:
            // giá phòng
            if (obj.Details == true) {
                if (typeof viewDetailDialog !== "function") return false;
                viewDetailDialog(obj.Item.dataid);

                return true
            } else
                return true;
            break;
        case 6:
            // hủy đặt phòng - danh sách
            if (obj.Details == false) {
                $("#ddlStatus").val(4);
                $("#btnSearch").trigger("click");
                return true
            } else
                return true;
            break;
        case 8:
            // chi tiết thanh toán
            if (obj.Details == true) {
                if (typeof viewOrderInfo !== "function") return false;
                var data = JSON.parse(obj.Data);
                viewOrderInfo(data.Id, data.CheckInID);
                return true
            } else
                return true;
            break;
        case 9:
            // nhận phòng
            if (obj.Details == true) {
                if (typeof viewInfoDialogCheckIn !== "function") return false;
                viewInfoDialogCheckIn(obj.Item.dataid);
                return true
            } else
                return true;
            break;
        default:
            return true;
    }
}
var timeoutAuto = -1;
function viewNotifyClientAuto() {
    if (timeoutAuto)
        clearTimeout(timeoutAuto);
    timeoutAuto = setTimeout(notifyLazyEvent, 1000);
}
