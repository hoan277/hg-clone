﻿var idTimeOut; 
startKeepSessionAlive();

function startKeepSessionAlive() {
    if (idTimeOut) clearInterval(idTimeOut);
    if (navigator.onLine) {
        idTimeOut = setInterval(KeepSessionAlive, 60000)
    } else {
        idTimeOut = setInterval(function () { console.log("offline") }, 1000)
    }
}

function KeepSessionAlive() {
    if (navigator.onLine) {
        var url = "/KeepSessionAlive.ashx";
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.send();
        xhr.onreadystatechange = function () {
            if (navigator.onLine && xhr.readyState == XMLHttpRequest.DONE) {
                if (xhr.responseText == 0) {
                    clearInterval(idTimeOut);
                    if ($("#modalLogin").length > 0) {
                        $("#modalLogin").modal("show");
                    } else {
                        intLogin("modalLogin");
                    }
                }
            }
        }
    } else {
        return "OffLine";
    }
}

function intLogin(id) {
    var html = '<div id="' + id + '" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">' +
                '   <div class="modal-dialog modal-sm modal-login">' +
                '       <div class="modal-content">' +
                '           <div class="modal-body">' +
                '               <form method="post">' +
                    '               <div class="login-logo"><i class="logo-icon"></i></div>' +
                    '               <div class="form-group has-feedback has-feedback-input">' +
                    '                   <input style="padding-left:0px !important;" ' +
                    '                                       id="UserName" name="UserName" type="text"  class="form-controls"  ' +
                    '                                       placeholder="TÊN ĐĂNG NHẬP" autocomplete="off" required="true">' +
                    '               </div>' +
                    '               <div class="form-group has-feedback has-feedback-input">' +
                    '                   <input style="padding-left:0px !important;" ' +
                                                                'type="password" id="Password" name="Password" class="form-controls"  ' +
                                                                'placeholder="MẬT KHẨU" autocomplete="off" required="true">' +
                    '               </div>' +

                    '               <div class="form-group has-feedback">' +
                    '                   <label class="err-mgs"></label>' +
                    '               </div>' +
                    '               <div class="row has-feedback-input">' +
                    '                   <div class="col-xs-12">' +
                    '                       <button type="button" name="command" value="SignInSub" id="btnLoginModal" class="btn btn-block btn-info btn-line-height btn-oze-login">ĐĂNG NHẬP</button>' +
                    '                   </div>' +
                    '               </div>' +
                '               </form>' +
                '           </div>' +
                '       </div>' +
                '    </div>' +
                '</div>';
    // add html in body
    $("body").append(html);
    var $f = $("#modalLogin").find("form");
    //$('<input />').attr('type', 'hidden')
    //  .attr('name', "__RequestVerificationToken")
    //  .attr('value', $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val())
    //  .appendTo('#modalLogin form');
    // load css
    Sv.loadCss("cssLogin", "/Content/Identity/IdentityStype.css");

    //validate

    $f.validate({
        highlight: function (element, error, valid) {
            $(element).css("border-bottom", "1px solid red");
            //$(element).focus();
        },
        unhighlight: function (element) {
            $(element).css("border-bottom", "1px solid #a6a6a6");
        },
    });

    //init btn login
    $("#btnLoginModal").off("click").on("click", function () {
        loginPost($f, $("#modalLogin"));
    });
    $f.keypress(function (e) {
        if (e.which == 13) {
            loginPost($f, $("#modalLogin"));
        }
    });

    // open modal
    $("#modalLogin").modal("show");
}

function loginPost($f, $modal) {
    // submit
    if ($f.valid()) {
        Sv.AjaxToken(function (token) {
            var data = {
                username: $f.find("[name=UserName]").val(),
                password: $f.find("[name=Password]").val(),
                __RequestVerificationToken: token,
            };
            Sv.Post({
                url: "/Accounts/LoginModal",
                data: data
            }, function (mgs) {
                // login thành công => clear modal, startKeepSessionAlive();
                if (mgs == "001") {
                    $modal.modal("hide");
                    startKeepSessionAlive();
                }
                else if (mgs == "002") {
                    var path = "/Home/Index";
                    if (path == window.location.pathname)
                        location.reload();
                    else
                        window.location.href = window.location.host + path; // window.location.host + "/Home/Index";
                }
                    // login thất bại
                else {
                    Dialog.Alert(mgs);
                }

            });
        });
    }

}