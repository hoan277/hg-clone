﻿var sleep_hashchange = 0;
$(document).ready(function () {
    window.addEventListener("hashchange", function () {
        if (sleep_hashchange > 0)
        {
            sleep_hashchange = 0;
            return false;
        }

        flag = 1;
        var x = location.hash;
        if (x === "" && location.href.indexOf("/Home/Index") !== -1 && location.href.indexOf("/Home/Index#") === -1) {
            loadMainContent(x);
            return false;
        }
        else if (x === "" || x === "#") {
            (new Utils()).init();
            return false;
        }
        else if (location.href.indexOf("#") === -1) {
            flag = 1;
            (new Utils()).init();
            return false;
        }
        else if (x !== "#") {
            var u = x.split("#");
            loadMainContent(u[1]);
            return false;
        }
    });
    if (flag == 0) {
        var x = location.href;
        if (x.indexOf("#") > -1) {
            //window.stop();
            var u = x.split("#")
            flag = 1;
            loadMainContent(u[1]);
            return false;
        }
    }
});

// click vào menu
function loadTagahashchange(url, callback) {
    sleep_hashchange = 1;
    loadMainContent(url,callback)
}

function loadMainContent(url, callback) {
    if (typeof callback === "string" && callback === "menu")
    {
        sleep_hashchange = 1;
    }
    $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () { $("#main-content").html("<div class='page-loading'>loading...<img src='/images/load.gif'/></div>"); Sv.RequestStart(); },
        complete: function () { Sv.RequestEnd(); },
        success: function (d) {
            $('#main-content').html(d);
            (new Utils()).init();
            if (typeof callback === "function")
                callback();
        }
    });
}
function hashChangeSodophong() {
    if (location.href.indexOf("#/Sodophong/Index") > -1) {
        window.location.reload();
    } else {
        window.location.hash = "#/Sodophong/Index";
    }
}

function hashChangeKaraoke () {
    if (location.href.indexOf("#/Karaoke/Sodophong/Index") > -1) {
        window.location.reload();
    } else {
        window.location.hash = "#/Karaoke/Sodophong/Index";
    }
}

function hashChangeSpa() {
    if (location.href.indexOf("#/Spa/Sodophong/Index") > -1) {
        window.location.reload();
    } else {
        window.location.hash = "#/Spa/Sodophong/Index";
    }
} 