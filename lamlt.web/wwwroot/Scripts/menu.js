﻿$(document).ready(function () {
    if (localStorage.getItem("tab", "#tab_2")) {
        $('.nav.nav-tabs li#tabManager').removeClass('active');
        $('#tab_1').removeClass('active');

        $('.nav.nav-tabs li#tabTran').addClass('active');
        $('#tab_2').addClass('active');
    }
    $('.sidebar-menu').mouseenter(function () {
        localStorage.removeItem("tab");
    });
    var current_url = window.location.pathname;
    if (current_url.includes("Edits/0")) {
        $(".col_srt").hide();
        $(".col_preview").hide();
    }
    if (current_url.includes("Edits") || current_url.includes("Edit")) {
        current_url = current_url.replace("Edits", "");
        current_url = current_url.replace("Edit", "");
        current_url = current_url.replace("/", "");
        current_url = current_url.replace("/", "");
        current_url = current_url.replace(current_url.split("/").pop(), "");

        $("li#film_video_processing_tran").removeClass("active");
        $("li#film_video_processing").removeClass("active");
    }
    if (!current_url.includes("Edits") && !current_url.includes("Edit")) {
        current_url = current_url.replace("/", "").replace("/", "");
       // console.log("current_url: " + current_url);
        $("#" + current_url).addClass("active");
        $("#" + current_url + " a").css("color", "#fff");
        $("#" + current_url + " a").css("font-weight", "bold");
        //Set active menu parrent
        var parrent_first = $("#" + current_url).parent().parent().attr('id');
        var parrent_second = $("#" + current_url).parent().attr('id');
        $($("#" + parrent_first)).addClass("active");
        $($("#" + parrent_second)).addClass("menu-open");
        $(window).keydown(function (event) {
            if (event.keyCode == 66) {// Ctrl + B
                $('body').toggleClass("sidebar-collapse");
            }
        });
    }
    var current_time = new Date();
    //console.log(current_time.toLocaleDateString("vi-vn"));
    //$(".sidebar-menu").mouseenter(function () {
    //    $('body').removeClass("sidebar-collapse");
    //});
    //$(".sidebar-menu").mouseleave(function () {
    //    $('body').addClass("sidebar-collapse");
    //});
    var annoConfig = new Anno({
        target: '.skin_config',
        position: 'left',
        content: "<b>----- Cập nhật mới -----</b><br /><br />1. Hệ thống đổi giao diện",
    });

    if (localStorage.getItem("skin") == null || localStorage.getItem("skin") == "") {
        localStorage.setItem("skin", "skin-blue");
        location.reload();
    }
    if (localStorage.getItem("skin_config") == null) {
        //annoConfig.show();
        $(".skin_config").css("background", "transparent");
    }
    const getURLParameters = url => (url.match(/([^?=&]+)(=([^&]*))/g) || []).reduce((a, v) => ((a[v.slice(0, v.indexOf('='))] = v.slice(v.indexOf('=') + 1)), a), {});
    let video_type = getURLParameters(window.location.href);
    if (video_type.video_type != undefined) {
        console.log(video_type.video_type);
        switch (video_type.video_type) {
            case "film":
                $(".divdenoiser").hide();
                localStorage.setItem("video_type", "film");
                $("li#film_video_processing").removeClass("active");
                $("li#film_video_processing_tran").addClass("active");
                //$("#tab_1 .col3").removeClass("col-md-4");
                //$("#tab_1 .col3").addClass("col-md-3");
                $(".other").show();
                //$(".st2").show();
                $("#tabTran").show();
                break;
            case "other":
                $(".divdenoiser").hide();
                localStorage.setItem("video_type", "other");
                $("li#film_video_processing").addClass("active");
                $("li#film_video_processing_tran").removeClass("active");
                //$("#tab_1 .col3").removeClass("col-md-3");
                //$("#tab_1 .col3").addClass("col-md-4");
                $(".other").hide();
                $("#tabTran").hide();
                break;
            case "denoiser":
                $("#tabTran").hide();
                localStorage.setItem("video_type", "denoiser");
                $("li#film_video_processing_denoiser").addClass("active");
                $("li#film_video_processing_tran").removeClass("active");
                $("li#film_video_processing").removeClass("active");
                //$("#tab_1 .col3").removeClass("col-md-3");
                //$("#tab_1 .col3").addClass("col-md-4");
                $(".other").hide();
                //$(".st2").show();
                break;
        }
        $('#film_video_processing_tran a').removeAttr('style');
        $('#film_video_processing a').removeAttr('style'); 
    }
    current_url = window.location.pathname;
    if (current_url.includes("Edits") || current_url.includes("Edit")) {
        $("li#film_video_processing_tran").removeClass("active");
        $("li#film_video_processing").removeClass("active");
        $('#film_video_processing_tran a').removeAttr('style'); 
        $('#film_video_processing a').removeAttr('style'); 
    }
});