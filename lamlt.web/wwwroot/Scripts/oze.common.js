﻿
var Cookie = {
    create: function (name, value, days) {
        var expires = "";
        if (!days) days = 30;
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    get: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    },
    clear: function (name) {
        Cookie.create(name, "", -1);
    }
}


var ChuSo = new Array(" không ", " một ", " hai ", " ba ", " bốn ", " năm ", " sáu ", " bảy ", " tám ", " chín ");
var Tien = new Array("", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ");
var bootboxLamlt = {
    alert: function (msg, callback) {
        $.notify(msg);
        if ($.isFunction(callback)) callback.call();
    },
    clear: function () {
        $("[data-notify=container]").remove();
    }
};

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function changeCurency(id) {
    showDialogLoading("Thay đổi đơn vị tiền tệ...");
    $.get("/Common/changeCurency?id=" + id, function (data) {
        hideDialogLoading();
        //result = 1, id = newId, lst = lst
        if (data.result == 1) {
            curencyActive(data.id, data.lst);
            // set active
            refreshHash();
        }
    });
}

function changeHotelSession(hotelid) {
    if (navigator.onLine) {
        showDialogLoading("Thay đổi khách sạn...");
        $.get("/Common/ChangeSessionByHotelID?hotelid=" + hotelid, function (data) {
            Cookie.create('isFirstLogin', '1', 1);
            hideDialogLoading();
            window.location.href = data.Url;
        });
    } else {
        $("#SysHotelIDCurrent").val(window.hotelId);
        // $('#SysHotelIDCurrent').trigger("chosen:updated");
        alert("Không thể thay đổi khách sạn, vui lòng kiểm tra kết nối mạng.");
    }
}

function curencyActive(id, lst) {

    // set list  
    var html = "";
    for (var i = 0; i < lst.length; i++) {
        var it = lst[i];
        if (it.Id != id) {
            html += '<li role="presentation"><a id="curencyItemHotel_' + it.Id + '" href="javascript:changeCurency(' + it.Id + ')"> ' + it.toCurrency + '</a></li>'
        } else {
            window.currency = it;
        }
    }
    $("#curencyHotelList").html(html);
    // set text label  
    $("#curencyHotel").html(window.currency.toCurrency);
}

function refreshHash() {
    var h = window.location.hash;
    if (h != "") {
        loadMainContent(h.replace('#', ''));
    } else {
        window.location.reload();
    }
}

function refreshChangeHotel() {
    window.location.href = '/Home/Index';
}

function refreshCurrentHash() {
    var x = location.hash;
    if (x === "") { window.location.reload(); return; }
    if (x === "#") return;
    if (x !== "#") x = x.substring(1);
    $.ajax({
        type: "GET",
        url: x,
        beforeSend: function () { },
        complete: function () { },
        success: function (d) {
            $('#main-content').html(d);
            g_Utils = new Utils();
            g_Utils.init();
            g_Utils.SetAmount();
        }
    });
}
function alert(msg, callback) {
    bootbox.alert({
        title: "Thông báo",
        message: msg,
        callback: function () {
            if (callback) callback.call();
        }
    });
}

function confirm(msg, callback) {
    bootbox.confirm({
        size: "",
        title: "Xác nhận",
        closeButton: false,
        message: msg,
        callback: callback
    });
}
var dialogLoadin1g;
function showDialogLoading(msg) {
    if (!msg) msg = "";
    if ($("body > div.ajaxInProgress").length <= 0) {
        var str = '<div class="ajaxInProgress"><div class="loading-ct" >' +
            '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' +
            ' </div> </div>';
        $("body").append(str);
    }
    // add div
    if ($("body > div.ajaxInProgress div.load-msg").length <= 0) {
        $("body > div.ajaxInProgress > div.loading-ct").append('<div class="load-msg"></div>');
    }
    //ađ text
    $("body > div.ajaxInProgress div.load-msg").html(msg);


    $("body > div.ajaxInProgress").show();
}
function hideDialogLoading() {
    if ($("body > div.ajaxInProgress").length > 0)
        $("body > div.ajaxInProgress").hide();
}
function genMenu(id, suffixDialog) {
    return
    '<div class="edit-delete-table">' +
        '<div class="edit-table" data-toggle="modal" data-backdrop="static"  onclick="javascript:edit' + jsFunction + '"(' + id + ')">'
    '<img src="/images/icon/icon-edit.png" style=" border: none;" alt="Chỉnh sửa">' +
    '</div>' +
    '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:view' + jsFunction + '"(' + id + ')">' +
        '<img src="/images/icon/icon-view.png" style=" border: none;">' +
    '</div>' +
    '<div class="delete-table" data-toggle="modal" data-backdrop="static" onclick="javascript:view' + jsFunction + '"(' + id + ')">' +
               '<img src="/images/icon/icon-view.png" style=" border: none;">' +
    '</div>'
    '</div>';
}


function toggleMenu(idmenu) {
    $(".sidebar>ul.sidebar-menu > li").removeClass("active")
    $("#" + idmenu).addClass("active");
    updateWrapperHeight();
    sidebarOpen("mini");
}

function updateWrapperHeight() {
    var sidebar_height = $(".sidebar").height();
    var window_height = $(window).height();
    var neg = $('.main-header').outerHeight() + $('.main-footer').outerHeight();
    var postSetWidth;

    if (window_height >= sidebar_height) {
        postSetWidth = window_height - neg;
    } else {
        postSetWidth = sidebar_height;
    }
    //Fix for the control sidebar height   
    if (sidebar_height > postSetWidth)
        postSetWidth = sidebar_height;

    $(".content-wrapper, .right-side").css('min-height', postSetWidth);
}

function loadRoomByLevelRoom(idType, idRoom, idRoomSelect, callback) {
    $.get("/Common/GetAllRoomByRoomTypeID?roomtypeid=" + idType, function (data) {
        $("#" + idRoom).html('');
        $("#" + idRoom).append('<option value="0">--Chọn phòng--</option>');
        if (data.result) {
            $.each(data.result, function (i, obj) {
                var sselect = '';
                if (obj.Id > 0 && obj.Id == idRoomSelect) sselect = 'selected';
                $("#" + idRoom).append('<option value="' + obj.Id + '" ' + sselect + '>' + obj.Name + '</option>');
            });
        }
        if (callback)
            callback();
    });
}

function MoneyToString(money, perfix) {

    return DocSo(money, perfix != undefined ? perfix : "");
}

//1. Hàm đọc số có ba chữ số;
function DocSo3ChuSo(baso) {
    var tram;
    var chuc;
    var donvi;
    var KetQua = "";
    tram = parseInt(baso / 100);
    chuc = parseInt((baso % 100) / 10);
    donvi = baso % 10;
    if (tram == 0 && chuc == 0 && donvi == 0) return "";
    if (tram != 0) {
        KetQua += ChuSo[tram] + " trăm ";
        if ((chuc == 0) && (donvi != 0)) KetQua += " linh ";
    }
    if ((chuc != 0) && (chuc != 1)) {
        KetQua += ChuSo[chuc] + " mươi";
        if ((chuc == 0) && (donvi != 0)) KetQua = KetQua + " linh ";
    }
    if (chuc == 1) KetQua += " mười ";
    switch (donvi) {
        case 1:
            if ((chuc != 0) && (chuc != 1)) {
                KetQua += " mốt ";
            } else {
                KetQua += ChuSo[donvi];
            }
            break;
        case 5:
            if (chuc == 0) {
                KetQua += ChuSo[donvi];
            } else {
                KetQua += " lăm ";
            }
            break;
        default:
            if (donvi != 0) {
                KetQua += ChuSo[donvi];
            }
            break;
    }
    return KetQua;
}

//2. Hàm đọc số thành chữ (Sử dụng hàm đọc số có ba chữ số)
function DocSo(stien, perfix) {
    var SoTien = Math.abs(stien);

    var lan = 0;
    var i = 0;
    var so = 0;
    var KetQua = "";
    var tmp = "";
    var ViTri = new Array();
    if (SoTien < 0) return "";
    if (SoTien == 0) return "Không " + perfix;
    if (SoTien > 0) {
        so = SoTien;
    } else {
        so = -SoTien;
    }
    if (SoTien > 8999999999999999) {
        //SoTien = 0;
        return "Số quá lớn!";
    }
    ViTri[5] = Math.floor(so / 1000000000000000);
    if (isNaN(ViTri[5]))
        ViTri[5] = "0";
    so = so - parseFloat(ViTri[5].toString()) * 1000000000000000;
    ViTri[4] = Math.floor(so / 1000000000000);
    if (isNaN(ViTri[4]))
        ViTri[4] = "0";
    so = so - parseFloat(ViTri[4].toString()) * 1000000000000;
    ViTri[3] = Math.floor(so / 1000000000);
    if (isNaN(ViTri[3]))
        ViTri[3] = "0";
    so = so - parseFloat(ViTri[3].toString()) * 1000000000;
    ViTri[2] = parseInt(so / 1000000);
    if (isNaN(ViTri[2]))
        ViTri[2] = "0";
    ViTri[1] = parseInt((so % 1000000) / 1000);
    if (isNaN(ViTri[1]))
        ViTri[1] = "0";
    ViTri[0] = parseInt(so % 1000);
    if (isNaN(ViTri[0]))
        ViTri[0] = "0";
    if (ViTri[5] > 0) {
        lan = 5;
    } else if (ViTri[4] > 0) {
        lan = 4;
    } else if (ViTri[3] > 0) {
        lan = 3;
    } else if (ViTri[2] > 0) {
        lan = 2;
    } else if (ViTri[1] > 0) {
        lan = 1;
    } else {
        lan = 0;
    }
    for (i = lan; i >= 0; i--) {
        tmp = DocSo3ChuSo(ViTri[i]);
        KetQua += tmp;
        if (ViTri[i] > 0) KetQua += Tien[i];
        if ((i > 0) && (tmp.length > 0)) KetQua += ','; //&& (!string.IsNullOrEmpty(tmp))
    }
    if (KetQua.substring(KetQua.length - 1) == ',') {
        KetQua = KetQua.substring(0, KetQua.length - 1);
    }
    //console.log(stien);
    if (stien < 0)
        return "Âm " + KetQua + " " + perfix;
    KetQua = KetQua.substring(1, 2).toUpperCase() + KetQua.substring(2);
    return KetQua + " " + perfix; //.substring(0, 1);//.toUpperCase();// + KetQua.substring(1);
}

function convert2Money(str) {
    return str.replace(".", ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}

function grid($e, option) {
    var options = {
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "processing": true,
        "serverSide": true,
        "initComplete": function (settings, json) {
            hideDialogLoading();
        },
        "dom": '<"top">rt<"bottom" lpi><"clear">'
    };
    return $e.DataTable($.extend(options, option));
}
function loadDistrictByProvince(provinceid, idDistrictControl, idDistrictSelect) {
    $.get("/Common/GetAllDistrictByProvinceID?provinceid=" + provinceid, function (data) {
        $("#" + idDistrictControl).html('');
        $("#" + idDistrictControl).append('<option value="0">--Chọn--</option>');
        if (data.result) {
            $.each(data.result, function (i, obj) {
                var sselect = '';
                if (obj.Id == idDistrictSelect) sselect = 'selected';
                $("#" + idDistrictControl).append('<option value="' + obj.Id + '" ' + sselect + '>' + obj.name + '</option>');
            });
        }
    });
}
function loadRewardByDistrict(districtid, idRewardControl, idRewardSelect) {
    $.get("/Common/GetAllRewardByDistrictID?districtid=" + districtid, function (data) {
        $("#" + idRewardControl).html('');
        $("#" + idRewardControl).append('<option value="0">--Chọn--</option>');
        if (data.result) {
            $.each(data.result, function (i, obj) {
                var sselect = '';
                if (obj.Id == idRewardSelect) sselect = 'selected';
                $("#" + idRewardControl).append('<option value="' + obj.Id + '" ' + sselect + '>' + obj.name + '</option>');
            });
        }
    });
}

function sdpSetClass() {
    if ((location.href.toLowerCase().indexOf("/home/index") > -1 && location.hash === "") || location.hash.indexOf("/Sodophong/Index") > -1) {
        $(".content-wrapper").addClass("sdp-view");
    } else {
        $(".content-wrapper").removeClass("sdp-view");
    }
}

function reportDate() {
    var _e1 = $("input[name='FromDate']"), _e2 = $("input[name='ToDate']");
    Sv.SetupDatePicker([{
        e: _e1,
        format: "DD/MM/YYYY HH:mm",
    }, {
        e: _e2,
        format: "DD/MM/YYYY HH:mm",
    }]);
}


function timespan(diff) {
    var totalhours = Math.floor(diff / (1000 * 60 * 60));
    var totalmins = Math.floor(diff / (1000 * 60));
    var totalseconds = Math.floor(diff / (1000));


    var days = Math.floor(diff / (1000 * 60 * 60 * 24));
    diff -= days * (1000 * 60 * 60 * 24);

    var hours = Math.floor(diff / (1000 * 60 * 60));
    diff -= hours * (1000 * 60 * 60);

    var mins = Math.floor(diff / (1000 * 60));
    diff -= mins * (1000 * 60);

    var seconds = Math.floor(diff / (1000));
    diff -= seconds * (1000);
    return {
        days: days,
        hours: hours,
        mins: mins,
        seconds: seconds,
        totalhours: totalhours,
        totalmins: totalmins,
        totalseconds: totalseconds,
    };
}

function openghichu(e) {
    var $e = $(e.target);
    $e.blur();
    var modalId = "modal_ghichu";
    var $modalId = $("#" + modalId);
    if ($modalId.length == 0) {
        Sv.InitModal({
            modalId: "modal_ghichu",
            title: "Ghi chú",
            modalclass: "modal-dialog"
        });
        $modalId = $("body #" + modalId);
        $modalId.find(".modal-body-content").html('<textarea type="text"  class="form-control" id="temp_ghichu" name="temp_ghichu" rows="7"></textarea>');

    } else { }

    $modalId.modal("show").on('shown.bs.modal', function () {
        $modalId.find("#temp_ghichu").focus().val($e.val());
        var $btn = $('#' + modalId + ' #' + 'btnSave_' + modalId);
        $btn.off("click").click(function () {
            $e.val($modalId.find("#temp_ghichu").val());
            $modalId.modal("hide");
        });
    })


}

function setValueCurrency(value) {
    var s = value / window.currency.rateCurrency;
    return s;
}

function getValueCurrency(value) {
    var g = value * window.currency.rateCurrency;
    return g;
}

// override val
// get vnd
(function ($) {
    jQuery.fn.currency = function (value) {
        if (!window.currency || !window.currency.rateCurrency)
            return this.val(arguments);
        // set
        if (value != undefined) {
            var set = value / window.currency.rateCurrency;
            return this.val(set);
        }
            // get
        else {
            var get = parseFloat(this.val()) * window.currency.rateCurrency;
            return get;
        }
    };
    jQuery.fn.setAddon = function () {
        var $p = this.parent();
        if ($p.hasClass("input-group")) {
            var $e = $p.find(".input-group-addon");
            $e.html(window.currency.toCurrency);
        }
    }
})(jQuery);


//function marqueeShow() {
//    if ($("#pms_notification").length > 0)
//        $("#pms_notification").modal("show");
//}
function sidebarOpen(type) {
    if (type == "mini")
        $("body").addClass("sidebar-collapse");
    else if (type == "open")
        $("body").removeClass("sidebar-collapse");
}


function autoCreateData() {
    bootbox.confirm({
        title: "Tạo dữ liệu",
        message: "Tự động tạo dữ liệu mẫu trải nghiệm hệ thống!",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Xác nhận',
                className: 'btn-primary',
            },
            cancel: {
                label: 'Hủy',
                className: 'btn-danger',
            },
        },
        callback: function (type) {
            if (type) {
                // có
                Sv.Post({ url: "/Common/AutoCreateData", data: { isCreate: type } })
                    .then(function () {
                        window.location.reload();
                    });
            } else {
                // ko
                $.post("/Common/AutoCreateData", { isCreate: false })
            }
        }
    });
}

function getSaleOrderNumber(saleOrder) {
    try {
        return saleOrder.substring(saleOrder.length - 6);  
    } catch (e) {
        return "000000";
    }
}

function isQL() {
    return window.RightCode == "QUANLY"
}
function isSP() {
    return window.RightCode == "SUPERADMIN"
}
function isSale() {
    return window.RightCode == "SALE"
}
function isLT() {
    return window.RightCode == "LETAN"
}
function isKT() {
    return window.RightCode == "KETOAN"
}
function isBP() {
    return window.RightCode == "BUONGPHONG"
}






var offLineFlag = 0;
// offline
function onOffLine() {
    setInterval(function () {
       if (navigator.onLine) {    //on line
            // kiểm tra login sau khi kết nối mạng
            if (offLineFlag == 1) {
                offLineFlag = 0;
                KeepSessionAlive();
                // auto post lên server
                if (typeof syncDataLocalStorage === "function")
                    syncDataLocalStorage();
            }

            if ($("body > div.page_offline").length > 0)
                $("body > div.page_offline").hide();

            offLineFlag = 0;
        } else {    //off line 
            offLineFlag = 1;

            if ($("body > div.page_offline").length <= 0) {
                var str = '<div class="page_offline"> Không kết nối mạng </div>';
                $("body").append(str);
            }
            $("body > div.page_offline").show();

        }
    }, 100);
}
onOffLine();




// những ai không bị khóa
function isPermissionRemove() {
    return !(window.hotelId == 2556 && window.RightCode == "LETAN");
}
