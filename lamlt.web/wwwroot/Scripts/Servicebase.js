﻿
// longld


var Dialog = {
    Success: 'success',
    Warning: 'warning',
    Error: 'error',
    CONFIRM: 'primary',
    /*  Description: Dialog thông báo
                     - message: Thông tin thông báo cần hiển thị.
                     - status: Trạng thái dialog (Success: Thành công, Warning: Cảnh báo, Error: Thất bại).
                     - callbackFuntion: Function Callback thực hiện sau khi ấn nút xác nhận form thông báo.
        Author: vivu  */
    Alert: function (message, status, callbackFuntion, hideModalFuntion) {
        var typeDialog = this._getTypeDialog(status);
        bootbox.dialog({
            message: message,
            title: typeDialog.title,
            closeButton: false,
            className: typeDialog.className,
            buttons: {
                success: {
                    label: "<i class='fa fa-check'></i>" + " Đóng",
                    className: typeDialog.buttonClass,
                    callback: callbackFuntion
                }
            }
        }).on('shown.bs.modal', function () {
            $('.bootbox').find('button:first').focus();
        });
        //    .on('hidden.bs.modal', function () {
        //    var p = $("body").css('padding-right');
        //    var p1 = parseInt(p) - 17;
        //    if (p1 >= 0)
        //        $("body").css('padding-right', p1);
        //    hideModalFuntion == undefined ? function () { } : hideModalFuntion();
        //});
    },
    /*  Description: Dialog Config custom
                    - message: Thông tin thông báo cần hiển thị.
                    - callbackFuntion: Function Callback thực hiện sau khi ấn nút xác nhận form thông báo.
        Author: vivu  */
    ConfirmCustom: function (title, message, callbackFuntion, showModalFuntion) {
        var typeDialog = this._getTypeDialog(this.CONFIRM);
        bootbox.dialog({
            message: message,
            title: title ? title : typeDialog.title,// title ? typeDialog.title : title,
            closeButton: false,
            className: typeDialog.className,
            buttons: {
                success: {
                    label: "<i class='fa fa-check'></i>" + " Xác nhận",
                    className: typeDialog.buttonClass,
                    callback: callbackFuntion
                },
                cancel: {
                    label: "<i class='fa fa-reply'></i>" + " Đóng",
                    className: "btn btn-df"
                }
            }
        }).on('shown.bs.modal', showModalFuntion == undefined ? function () {
            //$('.bootbox').find('button:first').focus();
        } : showModalFuntion);
    },
    /*  Description: Hàm xác định kiểu của Dialog
        Author: vivu  */
    _getTypeDialog: function (status) {
        var type = {};
        switch (status) {
            case 'success':
                type = {
                    title: "Thành công",
                    className: 'my-modal-success',
                    buttonClass: 'btn btn-primary'
                };
                break;
            case 'warning':
                type = {
                    title: "Thông báo",
                    className: 'my-modal-warning',
                    buttonClass: 'btn btn-df'
                };
                break;
            case 'error':
                type = {
                    title: "Lỗi",
                    className: 'my-modal-error',
                    buttonClass: 'btn btn-df'
                };
                break;
            case 'primary':
                type = {
                    title: " Xác nhận",
                    className: 'my-modal-primary',
                    buttonClass: 'btn btn-primary'
                };
                break;
        }
        return type;
    }
}

var Service = function () {
    var base = this;
    OzeBase.apply(this, arguments);

    var dialogLoadingSv;
    function showDialogLoading(msg) {
        ////if (!msg) msg = "<img src='/images/load.gif' />";
        //if (!msg) msg = "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i>";
        //dialogLoadin1g = bootbox.dialog({
        //    message: '<p class="text-center">' + msg + '</p>',
        //    closeButton: false,
        //    className: 'dialog-loading',
        //}).on('hidden.bs.modal', function () {
        //    var p = $("body").css('padding-right');
        //    var p1 = parseInt(p) - 17;
        //    if (p1 >= 0)
        //        $("body").css('padding-right', p1);
        //});
        if (!msg) msg = "";
        if ($("body > div.ajaxInProgress").length <= 0) {
            var str = '<div class="ajaxInProgress"><div class="loading-ct" >' +
                ////'<i class="fa fa-spinner fa-pulse"></i>' +
                //'<img src="/Assets/Admin/images/ajax-loader.gif">' +
                '<div>' + msg + '</div>' +
                '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' +
                ' </div> </div>';
            $("body").append(str);
        }
        $("body > div.ajaxInProgress").show();
    }
    function hideDialogLoading() {
        //if (dialogLoadin1g) dialogLoadin1g.modal('hide');
        if ($("body > div.ajaxInProgress").length > 0)
            $("body > div.ajaxInProgress").hide();
    }

    this.ObjectNotNull = function (obj) {
        if (obj && Object.keys(obj).length > 0)
            return true;
        return false;
    };

    this.RequestStart = function () {
        //showDialogLoading();
        if ($("body > div.ajaxInProgress").length <= 0) {
            var str = '<div class="ajaxInProgress"><div class="loading-ct" >' +
                ////'<i class="fa fa-spinner fa-pulse"></i>' +
                //'<img src="/Assets/Admin/images/ajax-loader.gif">' +
                //'<div>' + "Đang tải" + '</div>' +
                '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' +
                ' </div> </div>';
            $("body").append(str);
        }
        $("body > div.ajaxInProgress").show();
    }

    this.RequestEnd = function () {
        //hideDialogLoading();
        if ($("body > div.ajaxInProgress").length > 0)
            $("body > div.ajaxInProgress").hide();
    }

    this.CheckAuthen = function (option, fnSuccess, fnError) {
        //bỏ check đăng nhập
        var obj = {};
        obj.IsAuthen = true;
        fnSuccess(obj);
    }

    this.Post = function (option, fnSuccess, fnError) {
        var obj = {
            url: option.url || option.Url,
            type: 'Post',
            data: option.data || option.Data,
            contentType: (option.contentType != undefined ? option.contentType : 'application/x-www-form-urlencoded; charset=UTF-8'),
            beforeSend: function () {
                if (typeof option.beforeSend == "function") {
                    option.beforeSend();
                } else {
                    base.RequestStart();
                }
            },
            async: (option.async == undefined ? true : option.async),
            complete: function () {
                if (typeof option.complete == "function") {
                    option.complete();
                } else {
                    base.RequestEnd();
                }
            },
            success: function (rs) {
                if (typeof fnSuccess === "function")
                    fnSuccess(rs);
            },
            error: function (e) {
                if (!fnError)
                    Dialog.Alert("Có lỗi trong quá trình xử lý. Vui lòng thử lại", Dialog.Error);
                if (typeof fnError === "function")
                    fnError(e);
            }
        };
        return $.ajax(obj);
    }

    this.AjaxPost = function (option, fnSuccess, fnError) {
        base.AjaxToken(function (token) {
            var pdata = option.Data || option.data;
            if (pdata) {
                pdata.__RequestVerificationToken = token;
            }
            else {
                pdata = { __RequestVerificationToken: token }
            }
            base.Post(option, fnSuccess, fnError);
        });
    }

    this.AjaxPostFile = function (option, fnSuccess, fnError) {
        $.ajax({
            url: option.Url || option.url,
            type: 'Post',
            data: option.Data || option.data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            beforeSend: function () {
                base.RequestStart();
            },
            async: (option.async == undefined ? true : option.async),
            complete: function () {
                base.RequestEnd();
            },
            success: function (rs) {
                if (typeof fnSuccess === "function")
                    fnSuccess(rs);
            },
            error: function (e) {
                if (typeof fnError === "function")
                    fnError(e);
                else {
                    console.log(e);
                    Dialog.Alert("Có lỗi trong quá trình xử lý, vui lòng thử lại!", Dialog.Error);
                }
            }
        });
    }

    this.AjaxGet = function (option, fnSuccess, fnError) {
        if (option.Data) {
            option.Data.__RequestVerificationToken = base.requestToken();
        }
        else {
            option.Data = { __RequestVerificationToken: base.requestToken() };
        }
        if (option.CheckAuthen != false) {
            base.CheckAuthen({
                Url: "/Index/CheckAuthen",
            }, function (rs) {
                if (!rs.IsAuthen) {
                    Dialog.Alert("Phiên làm việc của bạn đã hết, vui lòng đăng nhập lại!", Dialog.Error, "Session Timedout", function () {
                        window.location = "/login?ReturnUrl=" + window.location.href;
                    });
                } else {
                    $.ajax({
                        url: option.Url,
                        type: 'Get',
                        data: option.Data,
                        beforeSend: function () {
                            base.RequestStart();
                        },
                        complete: function () {
                            base.RequestEnd();
                        },
                        success: function (rs) {
                            if (typeof fnSuccess === "function")
                                fnSuccess(rs);
                        },
                        error: function (e) {
                            if (typeof fnError === "function")
                                fnError(e);
                        }
                    });
                }
            }, function (e) {
            });
        } else {
            $.ajax({
                url: option.Url,
                type: 'Get',
                data: option.Data,
                beforeSend: function () {
                    base.RequestStart();
                },
                complete: function () {
                    base.RequestEnd();
                },
                success: function (rs) {
                    if (typeof fnSuccess === "function")
                        fnSuccess(rs);
                },
                error: function (e) {
                    if (typeof fnError === "function")
                        fnError(e);
                }
            });
        }
    }

    this.AjaxPostForm = function (option, fnSuccess, fnError) {
        base.AjaxToken(function (token) {
            var pdata = option.Data || option.data;
            if (pdata) {
                pdata.__RequestVerificationToken = token;
            }
            else {
                pdata = { __RequestVerificationToken: token }
            }
            var obj = {
                url: option.url || option.Url,
                type: 'Post',
                data: option.data || option.Data,
                contentType: (option.contentType != undefined ? option.contentType : 'application/x-www-form-urlencoded; charset=UTF-8'),
                processData: false,
                beforeSend: function () {
                    if (typeof option.beforeSend == "function") {
                        option.beforeSend();
                    } else {
                        base.RequestStart();
                    }
                },
                async: (option.async == undefined ? true : option.async),
                complete: function () {
                    if (typeof option.complete == "function") {
                        option.complete();
                    } else {
                        base.RequestEnd();
                    }
                },
                success: function (rs) {
                    if (typeof fnSuccess === "function")
                        fnSuccess(rs);
                },
                error: function (e) {
                    if (!fnError)
                        Dialog.Alert("Có lỗi trong quá trình xử lý. Vui lòng thử lại", Dialog.Error);
                    if (typeof fnError === "function")
                        fnError(e);
                }
            };
            $.ajax(obj);
        });
    }
    //this.AjaxPost = function (option, fnSuccess, fnError) {
    //    $.ajax({
    //        url: option.Url,
    //        type: 'Post',
    //        data: option.Data,

    //        beforeSend: function () {
    //            base.RequestStart();
    //        },
    //        async: (option.async == undefined ? true : option.async),
    //        complete: function () {
    //            base.RequestEnd();
    //        },
    //        success: function (rs) {
    //            if (typeof fnSuccess === "function")
    //                fnSuccess(rs);
    //        },
    //        error: function (e) {
    //            if (!fnError)
    //                Dialog.Alert("Có lỗi trong quá trình xử lý. Vui lòng thử lại", Dialog.Error);
    //            if (typeof fnError === "function")
    //                fnError(e);
    //        }
    //    });
    //}

    this.DateTimeNow = function () {
        var dfFormDate = new Date();
        var dfToDate = new Date();
        dfFormDate.setHours(0);
        dfFormDate.setMinutes(0);
        dfFormDate.setSeconds(0);
        dfToDate.setHours(23);
        dfToDate.setMinutes(59);
        dfToDate.setSeconds(59);
        return {
            FormDate: dfFormDate,
            ToDate: dfToDate,
            MomentFromDate: moment(dfFormDate),
            MomentToDate: moment(dfToDate)
        }
    }
    this.Set59InDay = function (dat) {
        dat.setHours(23);
        dat.setMinutes(59);
        dat.setSeconds(59);
        return dat;
    }
    this.Set00InDay = function (dat) {
        dat.setHours(0);
        dat.setMinutes(0);
        dat.setSeconds(0);
        return dat;
    }

    this.AddDays = function (days) {
        var dat = new Date();
        dat.setDate(dat.getDate() + parseInt(days));
        return new Date(dat);
    }

    this.AddMonth = function (days) {
        var dat = new Date();
        dat.setMonth(dat.getMonth() + parseInt(days));
        return new Date(dat);
    }

    this.AddYear = function (year) {
        var dat = new Date();
        dat.setYear(dat.getYear() + parseInt(year));
        return new Date(dat);
    }

    this.JoinObject = function (oldObj, newObj) {
        if (typeof oldObj === "object" && oldObj != undefined && oldObj != null
           && typeof newObj === "object" && newObj != undefined && newObj != null) {
            for (var key in newObj) {
                if (newObj.hasOwnProperty(key)) {
                    oldObj[key] = newObj[key];
                }
            }
        }
        return oldObj;
    }

    this.NumberToString = function (value) {
        return value != null ? value.toString().replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') : "0";
    };
    this.NumberToString2 = function (value) {
        var v = Math.round(value * 100) / 100;
        return v != null ? v.toString().replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') : "0";
    };

    //json date to string
    this.DateToString = function (value, fomart) {
        if (value == null || value.length <= 0)
            return "";
        if (fomart == undefined)
            fomart = "DD/MM/YYYY";
        return moment(new Date(parseInt(value.slice(6, -2)))).format(fomart);
    };

    //==================================================================
    //	Description:  Bootstrap Table					 Option, function
    //	Author: vivu
    //==================================================================
    this.Sprintf = function (str) {
        var args = arguments,
            flag = true,
            i = 1;

        str = str.replace(/%s/g, function () {
            var arg = args[i++];

            if (typeof arg === 'undefined') {
                flag = false;
                return '';
            }
            return arg;
        });
        return flag ? str : '';
    };

    this.BootstrapTableOption = function (option) {
        var obj = {
            locale: 'en-US',
            classes: 'table table table-striped table-bordered dataTable no-footer', // table-hover
            cache: false,
            pagination: true,
            pageSize: 15,
            pageList: [15, 20, 30, 50, 100],
            formatLoadingMessage: function () {
                return '<div class="ajaxInProgress"> <div class="loading-ct" >' +
                    '<img src="/Assets/Admin/images/ajax-loader.gif">' +
                '<div>' + "Đang tải" + '</div>' +
                    '</div> </div>';
            },
            method: 'post',
            sidePagination: 'server',
            queryParams: function (params) {
                return params;
            },
            responseHandler: function (res) {
                return {
                    total: res.total,
                    rows: res.data
                };
            },
        };
        var xx = base.JoinObject(obj, option);
        return xx;
    }

    this.DateTimeToString = function (value) {
        return value ? moment(new Date(parseInt(value.slice(6, -2)))).format("DD/MM/YYYY HH:mm:ss") : "";
    }

    this.BootstrapTableColumn = function (type, option) {
        var align = "";
        var formatFn;
        var className = "";
        if (typeof type === "function")
            type = type();
        switch (type) {
            case "Number":
                align = "right";
                className = "row-number";
                formatFn = function (value) {
                    return value ? value.toString().replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') : 0;
                }
                break;
                /* vivu - Dự án này áp dụng luôn 1 kiêu date format */
                //case "Date0":
                //    align = "center";
                //    className = "row-date";
                //    formatFn = function (value) {
                //        return value ? moment(new Date(parseInt(value.slice(6, -2)))).format('DD/MM/YYYY') : "";
                //    }
                //    break;
                //case "DateTime0":
                //    align = "center";
                //    className = "row-date";
                //    formatFn = function (value) {
                //        return value ? moment(new Date(parseInt(value.slice(6, -2)))).format('DD/MM/YYYY HH:mm') : "";
                //    }
                //    break;
            case "DateTimeFull":
            case "Date":
            case "DateTime":
                align = "center";
                className = "row-date";
                formatFn = function (value) {
                    return value ? moment(new Date(parseInt(value.slice(6, -2)))).format("DD/MM/YYYY HH:mm:ss") : "";
                }
                break;
            default:
                align = "left";
                className = "row-string";
                formatFn = function (value) {
                    return value ? value : "";
                }
                break;
        }
        var obj = {
            align: align,
            valign: "middle",
            width: "100px",
            'class': className,
            formatter: formatFn,
        };
        return base.JoinObject(obj, option);
    }

    this.BootstrapTableSTT = function ($table, index) {
        var option = $table.bootstrapTable('getOptions');
        if (option.sidePagination == "server") {
            var i = (option.pageNumber - 1) * option.pageSize;
            return i + index + 1;
        } else {
            return index + 1;
        }
    }

    this.ResponseHandlerFooter = function (res) {
        var obj = {
            total: res.total,
            rows: res.data != null ? res.data : [],
        };
        obj.rows.Footer = res.footer;
        return obj;
    }

    this.ResponseHandlerSearch = function (res, $modalSearch, $table) {
        //$modalSearch.modal("hide");
        if (res.total == 0) {
            $("body").css('padding-right', 0);
            $table.bootstrapTable('removeAll');
            Dialog.Alert("Không tìm thấy dữ liệu theo điều kiện tìm kiếm!", Dialog.Warning);
        }
        return {
            total: res.total,
            rows: res.data
        };
    },
    this.LoadTableSearch = function ($table, url, showDialog) {

        $table.bootstrapTable('refreshOptions', {
            url: url,
            responseHandler: function (res) {
                if (res.Status) {
                    if (res.Status == "URL") {
                        window.location.assign(res.Message);

                        return false;
                    }
                    if (res.Status == "Login") {
                        window.location = "/login?ReturnUrl=" + window.location.href;
                        return false;
                    }
                    return base.ResponseHandler($table, showDialog, res.Data);
                } else {
                    return base.ResponseHandler($table, showDialog, res);
                }
            },
            sidePagination: 'server',
        });
    }

    //this.CheckAuthen = function (callback) {
    //    Sv.AjaxPost({
    //        Url: "/Index/CheckAuthen",
    //    }, function (rs) {
    //        if (!rs.IsAuthen) {
    //            Dialog.Alert( "Phiên làm việc của bạn đã hết, vui lòng đăng nhập lại!", Dialog.Error,  "Phiên làm việc của bạn đã hết, vui lòng đăng nhập lại!"Title, function () {
    //                window.location = "/Admin/Login?ReturnUrl=" + window.location.href;
    //            });
    //            return false;
    //        } else {
    //            if (typeof callback === "function")
    //                return callback();
    //            else
    //                return true;
    //        }

    //    }, function (e) {
    //        Dialog.Alert("Có lỗi trong quá trình xử lý. Vui lòng thử lại", Dialog.Error);
    //        return false;
    //    });
    //}

    this.ResponseHandler = function ($table, showDialog, res) {
        if (res.total == 0) {
            $table.bootstrapTable('removeAll');
            if (res.total == 0 && showDialog)
                Dialog.Alert("Không tìm thấy dữ liệu theo điều kiện tìm kiếm!", Dialog.Error);
        }
        return { total: res.total, rows: res.data };
    }

    //vivu - ResetView Table
    //vivu - ResetView Table
    this.ResetViewTable = function () {
        try {
            for (var i = 0; i < arguments.length; i++) {
                arguments[i].bootstrapTable('resetView');
            }
        } catch (e) { }
        return;
    };

    //vivu -  set date
    this.DefaultDate = function () {
        var dfFormDate = new Date();
        var dfToDate = new Date();
        var dfMax = new Date();
        dfFormDate.setHours(0);
        dfFormDate.setMinutes(0);
        dfFormDate.setSeconds(0);
        dfFormDate.setMilliseconds(000);
        dfToDate.setHours(23);
        dfToDate.setMinutes(59);
        dfToDate.setSeconds(59);
        dfToDate.setMilliseconds(000);
        dfMax.setHours(23);
        dfMax.setMinutes(59);
        dfMax.setSeconds(59);
        dfMax.setMilliseconds(999);
        var obj = {
            FormDate: dfFormDate,
            ToDate: dfToDate,
            //Maxdate dfMax,
            MomentFromDate: moment(dfFormDate),
            MomentToDate: moment(dfToDate),
            MomentMaxDate: moment(dfMax)
        }
        return obj;
    }

    this.BindPopup = function (url, model, callback) {
        base.AjaxPost({
            Url: url,
            Data: model
        }, function (rs) {
            if (rs.Status && rs.Status == "Login") {
                window.location = "/login?ReturnUrl=" + window.location.href;
            } else if (rs.Status && rs.Status == "URL") {
                window.location = "/login?ReturnUrl=" + window.location.href;
            } else if (rs.Status && rs.Status === "00") {
                Dialog.Alert(rs.Message, Dialog.Error);
            } else {
                if (typeof callback == "function")
                    callback(rs);
            }
        }, function () {
            Dialog.Alert("Có lỗi trong quá trình xử lý. Vui lòng thử lại", Dialog.Error);
        });
    }
    //vivu
    this.BindPopupOTP = function (callback) {
        base.AjaxPost({
            Url: "/Admin/Common/BindPopupOTP",
            Data: {}
        }, function (rs) {
            if (rs.Status && rs.Status == "Login") {
                window.location = "/login?ReturnUrl=" + window.location.href;
            } else if (rs.Status && rs.Status == "URL") {
                window.location = "/login?ReturnUrl=" + window.location.href;
            } else if (rs.Status && rs.Status === "00") {
                Dialog.Alert(rs.Message, Dialog.Error);
            } else {
                if (typeof callback == "function")
                    callback(rs);
            }
        }, function () {
            Dialog.Alert("Có lỗi trong quá trình xử lý. Vui lòng thử lại", Dialog.Error);
        });
    }

    var s = null; // Giây
    var timeout = null; // Timeout

    this.start = function ($time, callback) {
        /*BƯỚC 1: LẤY GIÁ TRỊ BAN ĐẦU*/
        if (s === null) {
            s = parseInt($time.html());
        }



        // Nếu số giờ = -1 tức là đã hết giờ, lúc này:
        //  - Dừng chương trình
        if (s == 0) {
            clearTimeout(timeout);
            $time.text("0");

            return false;
        }

        /*BƯỚC 1: GIẢM PHÚT XUỐNG 1 GIÂY VÀ GỌI LẠI SAU 1 GIÂY */
        timeout = setTimeout(function () {
            s--;
            base.start($time);
            $time.text("" + s + "");
        }, 1000);

        return true;
    }

    //this.DocSo = function ($e, val) {
    //    if (val > 0) {
    //        $e.html(language.MoneyToString(val));
    //    } else {
    //        $e.html("");
    //    }
    //}

    //==================================================================
    //	Description:  tạo ra các button thêm sửa xóa
    //	Author: vivu
    //==================================================================
    this.BindBtnCtrl = function ($e) {
        var name = $e.data("name");
        var type = $e.data("type");
        $e.attr("type", "button");
        $e.attr("title", name);
        $e.addClass("btn btn-primary btn-page-action");
        if (type.toUpperCase() == "SEARCH") {
            $e.html('<i class="fa fa-search"></i>' + name);
            return;
        }
        if (type.toUpperCase() == "ADD") {
            $e.html('<i class="fa fa-plus-square-o"></i>' + name);
            return;
        }
        if (type.toUpperCase() == "EXPORT") {
            $e.html('<i class="fa fa-print"></i>' + name);
            return;
        }
        if (type.toUpperCase() == "RESET") {
            $e.html('<i class="fa fa-refresh"></i>' + name);
            return;
        }
        if (type.toUpperCase() == "CLOSE") {
            $e.removeClass().addClass("btn btn-df");
            $e.html('<i class="fa fa-reply"></i>' + name);
            return;
        }
        return;
    }

    this.TypeaheadConfig = function ($e, option) {
        var optionDefault = {
            onSelect: function (item) {
                $e.data("seletectedValue", item.value);
                $e.data("seletectedText", item.text);
                if (item.object !== undefined)
                    $e.data("seletectedObject", JSON.parse(item.object));
                if (typeof (option.onSelect) == "function")
                    option.onSelect(item);
            },
            onNotSelect: option.onNotSelect,
            istext: option.istext != undefined ? option.istext : false,
            displayField: option.displayField,
            valueField: option.valueField,
            ajax: {
                url: option.ajax.url,
                triggerLength: option.ajax.triggerLength ? option.ajax.triggerLength : 2,
                preDispatch: (option.ajax.preDispatch == undefined ? function (query) {
                    return {
                        search: query
                    }
                } : option.ajax.preDispatch),
                preProcess: (option.ajax.preProcess == undefined ? function (data) {
                    if (data.success === false) {
                        return false;
                    }
                    return data;
                } : option.ajax.preProcess)
            }
        };

        $e.typeahead(optionDefault);
    };

    this.SetTypeaheadValue = function ($e, item) {
        if (item != null) {
            $e.data("seletectedValue", item.SrcCustomerID);
            $e.data("seletectedText", item.SrcCustomerView);
        }
    }

    this.AlertJsonRs = function (rs, callback) {
        Dialog.Alert(rs.Message, Dialog.Error, function () {
            if (rs.Status && rs.Status == "Login") {
                window.location = "/login?ReturnUrl=" + window.location.href;
            } else if (rs.Status && rs.Status == "URL") {
                window.location = "/login?ReturnUrl=" + window.location.href;
            } else {
                if (typeof callback == "function")
                    callback();
            }
        });
    }

    this.ReturnJsonRs = function (rs, successFn, callback) {
        if (rs.Status && rs.Status == "01") {
            if (typeof successFn == "function")
                successFn();
            else
                Dialog.Alert(rs.Message, Dialog.Success);
        } else {
            base.AlertJsonRs(rs, callback);
        }
    }

    //this.ValidateNote = function ($e, validate, mgs1, mgs2) {
    //    $e.rules("remove");
    //    if (validate) {
    //        $("#myinput").rules("add", {
    //            required: true,
    //            maxlength: 500,
    //            messages: {
    //                required: mgs1 ? mgs1 : language.Validate_Description_Required,
    //                minlength: mgs2 ? mgs2 : language.Validate_Description_Maxlength
    //            }
    //        });
    //    }
    //}
    //hoanglt add validate file
    //this.AddValidateFile = function () {
    //    $(".required-file").rules("add", {
    //        required: true,
    //        messages: {
    //            required: language.AllocatingFinancialTransactions.Validate_File_Required
    //        }
    //    });
    //}
    //--ResetForm
    this.ResetForm = function ($form, $fdate, $todate) {
        var validator = $form.validate();
        validator.resetForm();
        $form.each(function () {
            this.reset();
        });
        $form.find($fdate).data("DateTimePicker").date(Sv.DefaultDate().MomentFromDate);
        $form.find($todate).data("DateTimePicker").date(Sv.DefaultDate().MomentToDate);
    }
    this.ResetFormOnly = function ($form) {
        var validator = $form.validate();
        validator.resetForm();
        $form.each(function () {
            this.reset();
        });
    }
    this.unhighlight = function ($form) {
        $form.find("input,select,textarea").each(function (i, element) {
            if ($(element).hasClass("chosen")) { // input-group-addon
                $(element).next().css("border", "1px solid #c3cbd9");
            } else if ($(element).hasClass("input-group-addon")) {

            } else {
                $(element).css("border", "1px solid #c3cbd9");
            }

        });
    }

    // cấu hình input mask
    this.SetupNumberMask = function (arr) {
        if (arr != undefined && arr.length > 0)
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].e != undefined) {
                    base.isAndroidFn(
                        function () { arr[i].e.prop('type', 'number'); },
                        function () {
                            if (arr[i].alias === "percentage") {
                                arr[i].e.inputmask("percentage", {
                                    placeholder: '',
                                    radixPoint: ",",
                                    autoUnmask: true,

                                });
                            } else if (arr[i].alias === undefined) {
                                arr[i].e.inputmask({
                                    alias: 'decimal',
                                    groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
                                    radixPoint: ",", /* Ký tự phân cách với phần thập phân */
                                    autoGroup: true,
                                    digits: 0, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
                                    autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
                                    allowMinus: true, /* Không cho nhập dấu trừ */
                                    allowPlus: false, /* Không cho nhập dấu cộng */
                                    integerDigits: 11
                                });
                            } else {
                                arr[i].e.inputmask(arr[i].alias, {
                                    groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
                                    radixPoint: ",", /* Ký tự phân cách với phần thập phân */
                                    autoGroup: true,
                                    autoUnmask: true
                                });
                            }
                        });
                }
            }
    }
    this.SetupInputMask = function () {
        base.isAndroidFn(
            function () {
                $("body").find(".percent-mask").on().prop('type', 'number');
                $("body").find(".number-mask").on().prop('type', 'number');
                $("body").find(".number-mask2").on().prop('type', 'number');
                $("body").find(".float-mask").on().prop('type', 'number');
            },
            function () {
                $("body").find(".percent-mask").on().inputmask("percentage", {
                    placeholder: '',
                    radixPoint: ",",
                    autoUnmask: true,
                });

                $("body").find(".number-mask").on().inputmask({
                    alias: 'decimal',
                    groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
                    radixPoint: ",", /* Ký tự phân cách với phần thập phân */
                    autoGroup: true,
                    digits: 0, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
                    autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
                    allowMinus: true, /* Không cho nhập dấu trừ */
                    allowPlus: false, /* Không cho nhập dấu cộng */
                    integerDigits: 16,

                });
                $("body").find(".number-mask2").on().inputmask({
                    alias: 'decimal',
                    groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
                    radixPoint: ",", /* Ký tự phân cách với phần thập phân */
                    autoGroup: true,
                    digits: 2, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
                    autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
                    allowMinus: true, /* Không cho nhập dấu trừ */
                    allowPlus: false, /* Không cho nhập dấu cộng */
                    integerDigits: 16,

                });

                $("body").find(".float-mask").on().inputmask({
                    alias: 'decimal',
                    groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
                    radixPoint: ",", /* Ký tự phân cách với phần thập phân */
                    autoGroup: true,
                    digits: 2, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
                    autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
                    allowMinus: true, /* Không cho nhập dấu trừ */
                    allowPlus: false, /* Không cho nhập dấu cộng */
                    integerDigits: 16,

                });
            });
    }

    // cau hình datepicker
    this.SetupDateTime = function (input1, input2) {
        input1.datetimepicker({
            format: "DD/MM/YYYY",
            showTodayButton: true,
            defaultDate: base.DefaultDate().MomentFromDate,
            sideBySide: true
        });
        input2.datetimepicker({
            format: "DD/MM/YYYY",
            showTodayButton: true,
            defaultDate: base.DefaultDate().MomentToDate,
            showClose: true,
            sideBySide: true
        });
    }
    // datetime full
    this.SetupDateTimeFull = function (input1, input2) {
        input1.datetimepicker({
            format: "DD/MM/YYYY HH:mm:ss",
            showTodayButton: true,
            defaultDate: base.DefaultDate().MomentFromDate,
            sideBySide: true
        });
        input2.datetimepicker({
            format: "DD/MM/YYYY HH:mm:ss",
            showTodayButton: true,
            defaultDate: base.DefaultDate().MomentToDate,
            showClose: true,
            sideBySide: true
        });
    }// datetime full
    this.SetupDateTimeFull_Null = function (input1, input2) {
        input1.datetimepicker({
            format: "DD/MM/YYYY HH:mm",
            showTodayButton: true,
            defaultDate: "",
            sideBySide: true
        });
        input2.datetimepicker({
            format: "DD/MM/YYYY HH:mm",
            showTodayButton: true,
            defaultDate: "",
            showClose: true,
            sideBySide: true
        });
    }

    this.SetupDateTimeFullWithTime = function (input1, input2, time1, time2) {
        input1.datetimepicker({
            format: "DD/MM/YYYY HH:mm:ss",
            showTodayButton: true,
            defaultDate: time1,
            sideBySide: true
        });
        input2.datetimepicker({
            format: "DD/MM/YYYY HH:mm:ss",
            showTodayButton: true,
            defaultDate: time2,
            showClose: true,
            sideBySide: true
        });
    }
    this.SetupDateTimeFullSingle = function (input1, time1) {
        input1.datetimepicker({
            format: "DD/MM/YYYY HH:mm:ss",
            showTodayButton: true,
            defaultDate: time1,
            sideBySide: true
        });
    }

    this.SetDateTime = function ($e, date) {
        if (date == null)
            date = base.DefaultDate().FormDate;
        var f = "DD/MM/YYYY HH:mm";       
        if ($e.data("DateTimePicker"))    {
            f = $e.data("DateTimePicker").options().format;
            $e.data("DateTimePicker").date(moment(date).format(f));
        } else {        
            $e.datetimepicker({
                format: f,
                showTodayButton: true,
                defaultDate: moment(date),
                sideBySide: true
            });
            $e.data("DateTimePicker").date(moment(date).format(f));
        }
    }

    this.SetUpTimePicker = function ($e) {

        $e.timepicker({
            minuteStep: 1,
            showSeconds: false,
            showMeridian: false,
        });
        $e.inputmask("h:s", { placeholder: "__:__" });

    }


    this.SetupDatePicker = function (arr) {
        if (arr.length > 0)
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].e != undefined && arr[i].e.length > 0) {
                    var $e = arr[i].e
                    var df = base.DefaultDate().MomentFromDate;
                    if (arr[i].defaultDate == undefined)
                        df = base.DefaultDate().MomentFromDate;
                    else if (Object.prototype.toString.call(arr[i].defaultDate) == "[object Date]")
                        df = moment(arr[i].defaultDate);
                    else
                        df = arr[i].defaultDate;

                    var f = arr[i].format != undefined ? arr[i].format : "DD/MM/YYYY";
                    var opt = {
                        format: f,
                        defaultDate: df,
                        showTodayButton: true,
                        showClose: true,
                        sideBySide: true,
                        showClear: true,
                    };

                    if (arr[i].max)
                        opt.maxDate = arr[i].max;

                    $e.datetimepicker(opt);

                    var $eMask = null;
                    if ($e[0] != undefined && $e[0].tagName != "INPUT") {
                        $eMask = $e.find("input");
                    } else {
                        $eMask = $e;
                    }

                    var format = arr[i].format != undefined ? arr[i].format : "DD/MM/YYYY";
                    if (format == "DD/MM/YYYY") {
                        $eMask.inputmask("date", {
                            placeholder: "__/__/____",
                        });
                    } else if (format == "DD/MM/YYYY HH:mm") {
                        $eMask.inputmask("datetime", {
                            placeholder: "__/__/____ __:__",
                        });
                    }
                }
            }
    }

    // DataTable
    this.DataTableOption = function (option) {
        var obj = {
            language: {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            processing: true,
            serverSide: true,
            initComplete: function (settings, json) {
                hideDialogLoading();
            },
            paging: true,
            data: [],
            ajax: null,
            dom: '<"top">rt<"bottom" lpi><"clear">',
            fnDrawCallback: function (o) {
                var $t = $("#" + o.sTableId);
                var countCurrentItems = o.aiDisplay.length; // số bản ghi đang hiển thị
                var minSelect = o.aLengthMenu[0]; // giá trị nhỏ nhất của select "Xem 10 mục"
                var currentSelect = o._iDisplayLength; // giá trị đang chon của select "Xem ? mục"
                var total = (o._iRecordsTotal != undefined && o._iRecordsTotal > 0) ? o._iRecordsTotal : 0; // tổng số bản ghi
                if (countCurrentItems > minSelect) {
                    $t.parent().find(".bottom").show();
                    //if (countCurrentItems > currentSelect) {
                    //    $t.parent().find(".dataTables_paginate").show();
                    //} else {
                    //    $t.parent().find(".dataTables_paginate").hide();
                    //}
                } else {
                    if (total > minSelect) {
                        $t.parent().find(".bottom").show();
                    } else {
                        $t.parent().find(".bottom").hide();
                    }
                }
            },
            columns: []
        };
        var xx = base.JoinObject(obj, option);
        return xx;
    }
    // setupdate data table
    this.SetUpDataTable = function ($e, option) {
        return $e.DataTable(base.DataTableOption(option));
    }
    // reset modal -- add class modal-open in body
    this.ResetModalOpen = function () {
        var findModalOpen = $('body').find(".modal.in");
        if (findModalOpen.length > 0)
            if (!($("body").hasClass("modal-open")))
                $("body").addClass("modal-open");
            else
                $("body").removeClass("modal-open");
    }

    this.loadCss = function (cssId, path) {
        if (!document.getElementById(cssId)) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.id = cssId;
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = path;
            link.media = 'all';
            head.appendChild(link);
        }
    }

    this.requestToken = function () {
        return ('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
        //return $.get('/Common/AjaxAntiForgeryForm', function (html) {
        //    $(html).find('input[name=__RequestVerificationToken]').val();
        //});
    }
    this.AjaxToken = function (callback) {
        $.post("/Common/AjaxAntiForgeryForm", function (html) {
            var token = $(html).find('input[name=__RequestVerificationToken]').val();
            callback(token);
        })
    }

    this.RedirectUrl = function (url) {
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function () {
                $("#main-content").html("loading...<img src='/images/load.gif'/>");
                Sv.RequestStart();
            },
            complete: function () {
                Sv.RequestEnd();
            },
            success: function (d) {
                $('#main-content').html(d);
                $.getScript("/Scripts/Servicebase.js");
                g_Utils = new Utils();
                g_Utils.init();
                g_Utils.SetAmount();
            }
        });
    }

    // setup open modal
    this.SetupModal = function (option, callbackOpen, callbackSave, callback) {
        if (option.id)
            option.modalId = option.id;
        if (!option.modalId)
            option.modalId = "modalOze";
        var init = 0;
        $modal = $("#" + option.modalId);
        if ($modal.length == 0) {
            init = 1;
            Sv.InitModal(option);
            $modal = $("#" + option.modalId);
        } else {
            if (option.button) {
                $("#" + option.modalId).find(".modal-footer").html(option.button);
            }
        }

        var $btn = $modal.find("#btnSave");
        if ($btn.length == 0) {
            $btn = $modal.find("#btnSave_" + option.modalId);
        }
        $modal.off('show.bs.modal').on('show.bs.modal', function () {
            if (init == 0 && option.title) {
                $modal.find(".modal-title").find("label").html(option.title);
            }
            $modal.find(".modal-body").html('<p>loading..</p>');
            if (option.htmlcontent != undefined) {
                $modal.find(".modal-body").html(option.htmlcontent);
                bindModal(option.htmlcontent);
            } else if (option.data != undefined) {
                $.post(option.url, option.data, function (d) {
                    $modal.find(".modal-body").html(d);
                    bindModal(d);
                });
            } else {
                $modal.find(".modal-body").load(option.url, function (d) {
                    bindModal(d);
                });
            }
        });

        function bindModal(d) {
            Sv.SetupInputMask();
            if (typeof callbackOpen === "function") callbackOpen(d);

            $btn.off("click").click(function (e) {
                if (typeof callbackSave === "function") callbackSave(d, e);
            });
            if (callback) if (typeof callback === "function") callback();
        }

        $modal.modal("show");
    }

    // setup open modal
    this.SetupModalPost = function (option, callbackOpen, callbackSave) {
        if (!option.modalId)
            option.modalId = "modalDetails";
        var init = 0;

        $modal = $("#" + option.modalId);
        if ($modal.length == 0) {
            init = 1;
            Sv.InitModal(option);
            $modal = $("#" + option.modalId);
        }
        var $btn = $modal.find("#" + option.modalId + " #btnSave");
        if ($btn.length == 0) {
            $btn = $modal.find("#btnSave_" + option.modalId);
        }

        $modal.off('show.bs.modal');
        $modal.on('show.bs.modal', function () {
            if (option.title && init == 0) {
                $modal.find(".modal-title").find("label").html(option.title);
            }
            $modal.find(".modal-body").html('<p>loading..</p>');
            base.AjaxPost({
                url: option.url,
                data: option.data
            }, function (d) {
                $modal.find(".modal-body").html(d);
                Sv.SetupInputMask();
                if (typeof callbackOpen === "function") callbackOpen(d);
                $btn.off("click").click(function (e) {
                    if (typeof callbackSave === "function") callbackSave(d, e);
                });
            });
        });
        $modal.modal("show");
    }

    // init
    this.InitModal = function (option) {
        var html = '';
        html += '<div id="' + option.modalId + '" class="modal fade" role="dialog">';
        html += '   <div class="modal-dialog ' + (option.modalclass != undefined ? option.modalclass : "modal-lg") + '">';
        html += '       <div class="modal-content">';
        html += '           <div class="modal-header">';
        html += '               <button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '               <h4 class="modal-title btn-header"><label>' + option.title + '</label></h4>';
        html += '           </div>';
        html += '           <div class="modal-body">';
        html += '               <div class="modal-body-content">';
        html += '                   <p>Loading...</p>';
        html += '               </div>';
        html += '           </div>';
        html += '           <div class="modal-footer">';

        if (option.button) {
            html += option.button;
        } else {
            html += '          <button type="button" id="btnSave_' + option.modalId + '" class="btn btn-info" > Lưu</button> ';
            html += '          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        }

        html += '           </div>';
        html += '       </div>';
        html += '   </div>';
        html += '</div>';
        $("body").append(html);
    }

    // lấy dữ liệu trong orm
    this.FormGetData = function ($parent, callback) {
        var $f = $parent;
        if ($parent[0].tagName !== 'FORM') {
            $f = $parent.find('form');
        }
        if ($f.length <= 0)
            return [];
        var obj = {};
        $.each($f[0].elements, function (index, elem) {
            var name = elem.name;
            var value = elem.value;
            if (name.length > 0) {
                // ckeditor
                if (elem.classList.contains('ckeditor')) {
                    value = CKEDITOR.instances[name].getData();
                }
                // checkbox
                if (elem.tagName.toLowerCase() == 'input' && elem.type == 'checkbox') {
                    value = elem.checked;
                }
                // file
                if (elem.tagName.toLowerCase() == 'input' && elem.type == 'file') {
                    value = elem.files[0];
                }
                obj[name] = value;
            }
        });
        if (typeof callback == "function") {
            return callback(obj);
        } else {
            return obj;
        }
    }

    // Hàm thiết lập Cookie -- default 30p
    this.setCookie = function (cookieName, cookieValue, exhours) {
        if (exhours === undefined)
            exhours = 24;
        var d = new Date();
        d.setTime(d.getTime() + (exhours * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cookieName + "=" + cookieValue + "; " + expires;
    }

    // Hàm lấy Cookie
    this.getCookie = function (cookieName) {
        var name = cookieName + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1);

            if (c.indexOf(name) === 0)
                return c.substring(name.length, c.length);
        }
        return "";
    }

    this.SetScrollHeight = function (box) {
        if (box.length > 0) {
            for (var i = 0; i < box.length; i++) {
                var h = $(box[i]).height();
                if (h > 250) {
                    $(box[i]).addClass('box-item-scroll');
                }
            }

        }
    }


    //
    this.chosenSetup = function ($e, config) {
        var obj = {
            width: "100%",
        };
        var objCf = {};
        if (config && config != null) {
            objCf = base.JoinObject(obj, config);
        } else {
            objCf = obj;
        }
        var ch = $e.chosen(objCf);
        if (typeof config.onchange === "function") {
            ch.change(config.onchange);
        }
        return ch;
    }


};

var Sv = new Service();

$(document).ready(function () {
    /*  Description: Cấu hình datetimepicker
                Author: vivu  */
    if ($('.input-date').length > 0)
        $('.input-date').datetimepicker({
            //locale: "en",
            format: 'DD/MM/YYYY',
            showTodayButton: true,
            showClose: true,

        });

    /*  Description: Cấu hình datetimepicker (hiển thị cả giờ phút)
        Author: vivu  */
    if ($('.input-datetime').length > 0) $('.input-datetime').datetimepicker({
        //  locale: "en",
        format: 'DD/MM/YYYY HH:mm:ss',
        showTodayButton: true,
        showClose: true,
        sideBySide: true
    });

    /*  Description: Cấu hình Mask cho các control nhập số tiền (ko có phần thập phân) */

    if ($('.price-mask').length > 0 || $('.price-mask-digits').length > 0 || $('.discounted-mask').length > 0)
        Sv.isAndroidFn(function () {
            $('.price-mask').on().prop('type', 'number');
            $('.price-mask-digits').on().prop('type', 'number');
            $('.discounted-mask').on().prop('type', 'number');
        }, function () {
            $('.price-mask').inputmask({
                alias: 'decimal',
                groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
                radixPoint: ",", /* Ký tự phân cách với phần thập phân */
                autoGroup: true,
                digits: 0, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
                autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
                allowMinus: false, /* Không cho nhập dấu trừ */
                allowPlus: false, /* Không cho nhập dấu cộng */
                integerDigits: 9
            });

            /*  Description: Cấu hình Mask cho các control nhập số tiền
                Author: vivu  */
            $('.price-mask-digits').inputmask({
                alias: 'decimal',
                groupSeparator: '.', /* Ký tự phân cách giữa phần trăm, nghìn... */
                radixPoint: ",", /* Ký tự phân cách với phần thập phân */
                autoGroup: true,
                digits: 2, /* Lấy bao nhiêu số phần thập phân, mặc định của inputmask là 2 */
                autoUnmask: true, /* Tự động bỏ mask khi lấy giá trị */
                allowMinus: false, /* Không cho nhập dấu trừ */
                allowPlus: false, /* Không cho nhập dấu cộng */
                integerDigits: 9
            });

            /*  Description: Cấu hình Mask cho các control nhập chiết khấu
                Author: vivu  */
            $('.discounted-mask').inputmask("percentage", {
                placeholder: '',
                radixPoint: ",",
                autoUnmask: true
            });
        });
    /*  Description: Cấu hình Validation các control
        Author: vivu  */
    if ($('form.formValid').length > 0) $('form.formValid').each(function () {
        $(this).validate({
            ignore: '',
            errorPlacement: function (error, element) {
                var tagParent = element.parent();
                /* vivu: Đoạn kiểm tra nếu như thẻ div bọc chưa button và input
                            thì add class lỗi lên thẻ div cha cao hơn */
                if (tagParent.hasClass('input-group')) {
                    tagParent.parent().append(error);
                } else {
                    tagParent.append(error);
                }
                error.addClass('css-error');
            },
            onfocusout: false,
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
    });

    /*  Description: Cấu hình Validation định dạng ngày tháng
        Author: vivu  */
    $.validator.addMethod("dateFormat", function (value, element, params) {
        var isDate = function (valueDate) {
            var currVal = valueDate;
            if (currVal === "") return false;
            var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
            var dtArray = currVal.match(rxDatePattern);
            if (dtArray == null) return false;
            var dtDay = dtArray[1];
            var dtMonth = dtArray[3];
            var dtYear = dtArray[5];
            if (dtMonth < 1 || dtMonth > 12)
                return false;
            else if (dtDay < 1 || dtDay > 31)
                return false;
            else if ((dtMonth === 4 || dtMonth === 6 || dtMonth === 9 || dtMonth === 11) && dtDay === 31)
                return false;
            else if (dtMonth === 2) {
                var isleap = (dtYear % 4 === 0 && (dtYear % 100 !== 0 || dtYear % 400 === 0));
                if (dtDay > 29 || (dtDay === 29 && !isleap))
                    return false;
            }
            return true;
        };
        return this.optional(element) || isDate(value);
    }, 'Message Null'); //vivu - ko show buộc phải customer đa ngôn ngữ (hoặc phải customer câu thông báo này) -- Định dạng ngày không hợp lệ, vui lòng kiểm tra lại

    /*  Description: Cấu hình Validation báo lỗi khi mật khấu chứa các ký tự unicode như â, ă...
        Author: vivu  */
    $.validator.addMethod("password-regex", function (value, element) {
        return this.optional(element) || /^[A-Za-z0-9\s`~!@#$%^&*()+={}|;:'",.<>\/?\\-]+$/.test(value);
    }, 'Message Null'); //vivu - Mật khẩu chứa ký tự không hợp lệ (ă, â, đ, ...), vui lòng kiểm tra lại

    /*  Description: Cấu hình Validation báo lỗi khi chứa ký tự khoảng trắng.
        Author: vivu  */
    $.validator.addMethod("nospace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "Message Null"); //vivu - Không chưa ký tự khoảng trắng

    /*  Description: Cấu hình Validation chỉ nhập chữ và báo lỗi khi nhập số hoặc ký tự đặc biệt như !@#$%^&*()_+-=...
        Author: vivu  */
    $.validator.addMethod("spacecharacters", function (value, element) {
        return this.optional(element) || !/[~`!@#$%\^&*()_+=\-\[\]\\';,./{}|\\":<>\?0-9]/g.test(value);
    }, 'Message Null'); //vivu -Không chứa ký tự đặc biệt, vui lòng kiểm tra lại

    if ($(".date-picker").length > 0) $(".date-picker").datetimepicker({
        // locale: "en",
        format: 'MM/DD/YYYY HH:mm:ss',
        showTodayButton: true,
        //Maxdate: new Date(),
        defaultDate: new Date(),
        showClose: true, sideBySide: true
    });

    if ($(".date-mask").length > 0) $('.date-mask').inputmask({
        alias: 'MM/DD/YYYY',
        placeholder: ""
    });

    /*  Description: Cấu hình Mask datetime
        Author: vivu  */
    if ($(".datetime-mask").length > 0) $('.datetime-mask').inputmask("datetime", { "placeholder": "" });

    //==================================================================
    //	Description:  bind button
    //	Author: vivu
    //==================================================================
    if ($("button[binding]").length > 0) {
        for (var i = 0; i < $("button[binding]").length ; i++) {
            Sv.BindBtnCtrl($($("button[binding]")[i]));
        }
    }

    $('body').on('hidden.bs.modal', function () {
        var findModalOpen = $('body').find(".modal.in");
        if (findModalOpen.length > 0)
            if (!($("body").hasClass("modal-open")))
                $("body").addClass("modal-open");
            else
                $("body").removeClass("modal-open");
        var p = $("body").css('padding-right');
        var p1 = parseInt(p) - 17;
        if (p1 >= 0)
            $("body").css('padding-right', p1);
    });


    if ($("#SysHotelIDCurrent").length > 0 && window.chosenSelectKs == null) {

        //window.chosenSelectKs = Sv.chosenSetup($("#SysHotelIDCurrent"), {
        //    placeholder_text: "Lựa chọn khách sạn",
        //    onchange: function (e) { 
        //        //onchange = "javascript: changeHotelSession(this.value);"
        //        changeHotelSession($("#SysHotelIDCurrent").val());
        //    }
        //});
        $("#SysHotelIDCurrent").select2({
            width: '100%'
        });
        $("#SysHotelIDCurrent").change(function () {
            changeHotelSession($("#SysHotelIDCurrent").val());
        });
    }
});


function gettoken() {
    var token = '@Html.AntiForgeryToken()';
    token = $(token).val();
    return token;
}

// fix select2 hiện tại chưa dùng thay =   dropdownParent;           $("#SupplierID").select2({ width: "100%", dropdownParent: $("#myModal") });
//$.fn.modal.Constructor.prototype.enforceFocus = function () { };


var SESSION = {
    IsSp: window.RightCode == "SUPERADMIN",
    IsKeToan: window.RightCode == "KETOAN",
    IsLeTan: window.RightCode == "LETAN",
    IsQuanLy: (window.RightCode == "QUANLY"),
    IsSale: (window.RightCode == "SALE"),
    IsThuNgan: window.RightCode == "RMS_TELLER",
    IsNhaBep: window.RightCode == "RMS_COOK",
    IsChayBan: window.RightCode == "RMS_WAITER",

}

window.mobilecheck = function () {
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};
window.mobileAndTabletcheck = function () {
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};
window.detectmob =function() {
    if (navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
    ) {
        return true;
    }
    else {
        return false;
    }
}