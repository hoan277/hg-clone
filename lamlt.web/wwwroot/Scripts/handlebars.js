﻿Handlebars.registerHelper("img", function (data) {
    if (data)
        return data;// escape(data);
    else
        return "/Areas/Cafe/Images/Icons/images-empty.png";
});
Handlebars.registerHelper("numberToString", function (value) {
    return value != null ? value.toString().replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') : 0;
});

Handlebars.registerHelper("datejsonToString", function (value, fomart) {
    if (value == null || value.length <= 0)
        return "";
    if (fomart == undefined)
        fomart = "DD/MM/YYYY";
    return moment(new Date(parseInt(value.slice(6, -2)))).format(fomart);
})

