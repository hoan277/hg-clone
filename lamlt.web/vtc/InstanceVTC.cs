﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web;

namespace Golaco.bank
{
    public class InstanceVTC : IGatewayPay
    {

        public static string Security_Key = "Abc1234567890123456!";//"Abc1234567890123456!";//"Aa123456789012345!";//;// "Aa123456789012345!";// ConfigurationManager.AppSettings["pay_SecretKey"];
        public static string url_run = "https://vtcpay.vn/bank-gateway/checkout.html";
        //public static string url_run = ;/"http://alpha1.vtcpay.vn/portalgateway/checkout.html";//"https://vtcpay.vn/bank-gateway/checkout.html";//"http://alpha1.vtcpay.vn/portalgateway/checkout.html";// ConfigurationManager.AppSettings["pay_url_run"];
        public static string receiver_account = "0394244165";//"0916992864";// "0916992864";// "0963465816";// ConfigurationManager.AppSettings["pay_receiver_account"];
        public static string websiteid = "8763";//"150286";//"40088";// ConfigurationManager.AppSettings["pay_websiteid"];

        //public static string Security_Key = "Abc1234567890123456!";//"Abc1234567890123456!";//"Aa123456789012345!";//;// "Aa123456789012345!";// ConfigurationManager.AppSettings["pay_SecretKey"];
        //public static string url_run = "http://alpha1.vtcpay.vn/portalgateway/checkout.html";//"https://vtcpay.vn/bank-gateway/checkout.html";//"http://alpha1.vtcpay.vn/portalgateway/checkout.html";// ConfigurationManager.AppSettings["pay_url_run"];
        //public static string receiver_account = "0916992864";//"0916992864";// "0916992864";// "0963465816";// ConfigurationManager.AppSettings["pay_receiver_account"];
        //public static string websiteid = "150286";//"150286";//"40088";// ConfigurationManager.AppSettings["pay_websiteid"];

        public static IGatewayPay getInstance()
        {
            return new InstanceVTC();
        }
        public string getURLToBankNetToCharge(VTCHolder holder)
        {
            return holder.urlRedirect();
        }
        public film_transactions CheckTransactionIsProcessed(Microsoft.AspNetCore.Http.HttpContext context)
        {
            StringValues reference_number;
            context.Request.Query.TryGetValue("reference_number", out reference_number);

            var transactionService = new film_transactionsService();
            var trans = transactionService.GetByTxtId(reference_number);
            if (trans != null && (trans.statusBanking != "0") && (trans.statusBanking != "2"))
            //if (trans != null && (trans.statusBanking != "0") && (trans.statusBanking != "2"))
            {
                return trans;
                //return new GatewayResponse() { status = -101, describe = GetStatusMessage(-21), txtid = reference_number };
            }
            return null;
        }
        public GatewayResponse ExcuteResultRedirectUrl(Microsoft.AspNetCore.Http.HttpContext context)
        {
            StringValues reference_number;
            context.Request.Query.TryGetValue("reference_number", out reference_number);
            StringValues amount;
            context.Request.Query.TryGetValue("amount", out amount);
            StringValues website_id;
            context.Request.Query.TryGetValue("website_id", out website_id);
            StringValues payment_type;
            context.Request.Query.TryGetValue("payment_type", out payment_type);
            StringValues sstatus;
            context.Request.Query.TryGetValue("status", out sstatus);
            StringValues trans_ref_no;
            context.Request.Query.TryGetValue("trans_ref_no", out trans_ref_no);
            StringValues ssignature;
            context.Request.Query.TryGetValue("signature", out ssignature);
            StringValues message;
            context.Request.Query.TryGetValue("message", out message);

            int status = int.Parse(sstatus);
            //string signature =context.Server.HtmlDecode(context.Request.QueryString["signature"].ToString().Replace(" ", "+"));
            string signature = System.Net.WebUtility.HtmlDecode(ssignature.ToString().Replace(" ", "+"));
            object[] arrParamReturn = new object[] { amount, message, payment_type, reference_number, status, trans_ref_no, website_id };
            string textSign = string.Join("|", arrParamReturn) + "|" + Security_Key;

            string merchantSign = SecurityVTC.SHA256encrypt(textSign);
            bool isVerify = (merchantSign == signature);

            if (isVerify)
            {
                return new GatewayResponse() { status = status, describe = GetStatusMessage(status), txtid = reference_number, urlPath = context.Request.QueryString.ToString() };
            }
            else
            {
                //LogService.logItem("HTTP GET.Sai chu ky. Text:" + textSign
                //    + Environment.NewLine + "Sign:" + merchantSign, "handleFromGateway");
                //lblVerify.Text = "Chu ky sai";
                return new GatewayResponse() { status = -101, describe = GetStatusMessage(-101), txtid = reference_number, urlPath = context.Request.QueryString.ToString() };
            }
            //return new VTCResponse() { status = 1, describe = context.Request.QueryString.ToString() };
        }

        private string GetStatusMessage(int status)
        {
            string message = string.Empty;
            switch (status)
            {
                case 1:
                    message = "Giao dịch thành công";
                    break;
                case 0:
                    message = "Giao dịch ở trạng thái khởi tạo";
                    break;
                case -1:
                    message = "Giao dịch thất bại";
                    break;
                case -9:
                    message = "Khách hàng tự hủy giao dịch";
                    break;
                case -3:
                    message = "Quản trị VTC hủy giao dịch";
                    break;
                case -4:
                    message = "Thẻ/tài khoản không đủ điều kiện giao dịch (Đang bị khóa, chưa đăng ký thanh toán online …)";
                    break;
                case -5:
                    message = "Số dư thẻ/tài khoản khách hàng không đủ để thực hiện giao dịch";
                    break;
                case -6:
                    message = "Lỗi giao dịch tại VTC";
                    break;
                case -7:
                    message = "Khách hàng nhập sai thông tin thanh toán ( Sai thông tin tài khoản hoặc sai OTP)";
                    break;
                case -8:
                    message = "Quá hạn mức giao dịch trong ngày";
                    break;
                case -22:
                    message = "Số tiền thanh toán đơn hàng quá nhỏ";
                    break;
                case -24:
                    message = "Đơn vị tiền tệ thanh toán đơn hàng không hợp lệ";
                    break;
                case -25:
                    message = "Tài khoản VTC Pay nhận tiền của Merchant không tồn tại.";
                    break;
                case -28:
                    message = "Thiếu tham số bắt buộc phải có trong một đơn hàng thanh toán online";
                    break;
                case -29:
                    message = "Tham số context.Request không hợp lệ";
                    break;
                case -21:
                    message = "Trùng mã giao dịch, Có thể do xử lý duplicate không tốt nên bị trùng, mạng chậm hoặc khách hàng nhấn F5, hoặc cơ chế sinh mã GD của đối tác không tốt, đối tác cần kiểm tra lại để biết thời gian, số tiền và trạng thái của giao dịch này tại VTC";
                    break;
                case -23:
                    message = "WebsiteID không tồn tại";
                    break;
                case -99:
                    message = "Lỗi chưa rõ nguyên nhân và chưa biết trạng thái giao dịch. Cần kiểm tra để biết giao dịch thành công hay thất bại";
                    break;
                case -101:
                    message = "Chữ ký không hợp lệ";
                    break;
                case -102:
                    message = "Dữ liệu không hợp lệ";
                    break;
                default:
                    message = "Lỗi chưa rõ nguyên nhân và chưa biết trạng thái giao dịch. Cần kiểm tra để biết giao dịch thành công hay thất bại";
                    break;
            }
            return message;
        }



        private string getMessageForCharge(string vpc_ResponseCode)
        {

            if (vpc_ResponseCode == "-1") return "Lỗi bảo mật hệ thống";
            if (vpc_ResponseCode == "0") return "Giao dịch thành công";
            if (vpc_ResponseCode == "1") return " Ngân hàng từ chối thanh toán: thẻ/tài khoản bị khóa";
            if (vpc_ResponseCode == "2") return "Thông tin thẻ không hợp lệ";
            if (vpc_ResponseCode == "3") return "Thẻ hết hạn";
            if (vpc_ResponseCode == "4") return "Lỗi người mua hàng: Quá số lần cho phép. (Sai OTP, quá hạn mức trong ngày)";
            if (vpc_ResponseCode == "5") return "Không có trả lời của Ngân hàng";
            if (vpc_ResponseCode == "6") return "Lỗi giao tiếp với Ngân hàng";
            if (vpc_ResponseCode == "7") return "Tài khoản không đủ tiền";
            if (vpc_ResponseCode == "8") return "Lỗi checksum dữ liệu";
            if (vpc_ResponseCode == "9") return "Kiểu giao dịch không được hỗ trợ";
            if (vpc_ResponseCode == "10") return "Lỗi Không Xác định";
            if (vpc_ResponseCode == "11") return "Giao dịch chưa được xác thực OTP";

            if (vpc_ResponseCode == "12") return "Giao dịch không thành công, thẻ vượt quá hạn mức trong ngày";
            if (vpc_ResponseCode == "13") return "Thẻ chưa đăng ký dịch vụ giao dịch qua internet";
            if (vpc_ResponseCode == "14") return "Sai OTP";
            if (vpc_ResponseCode == "15") return "Sai mật khẩu";
            if (vpc_ResponseCode == "16") return "Sai tên chủ thẻ";
            if (vpc_ResponseCode == "17") return "Sai số thẻ";

            if (vpc_ResponseCode == "18") return "Sai ngày hiệu lực thẻ (sai ngày phát hành)";
            if (vpc_ResponseCode == "19") return "Sai ngày hiệu lực thẻ (sai ngày hết hạn)";
            if (vpc_ResponseCode == "20") return "OTP Timeout";
            if (vpc_ResponseCode == "21") return "Chưa xác thực thông tin thẻ";
            if (vpc_ResponseCode == "22") return "Không đủ điều kiện thanh toán (thẻ/tài khoản không hợp lệ hoặc TK không đủ số dư).";
            if (vpc_ResponseCode == "23") return "Giao dịch không thành công, số tiền giao dịch vượt quá hạn mức 1 lần thanh toán.";
            if (vpc_ResponseCode == "24") return "Giao dịch không thành công, số tiền giao dịch vượt hạn mức thanh toán.";
            if (vpc_ResponseCode == "25") return "Giao dịch chờ xác nhận từ Ngân hàng.";
            if (vpc_ResponseCode == "26") return "Sai thông tin xác thực ( áp dụng cho các NH thực hiện xác thực qua Internet Banking của NH)";
            if (vpc_ResponseCode == "27") return " Timeout giao dịch";
            if (vpc_ResponseCode == "28") return "Lỗi xử lý giao dịch tại hệ thống NHPH";
            return "Lỗi không xác định";
        }
        private string getMessageForRefund(string vpc_ResponseCode)
        {
            if (vpc_ResponseCode == "-1") return "Lỗi bảo mật hệ thống";
            if (vpc_ResponseCode == "0") return "Giao dịch thành công";

            if (vpc_ResponseCode == "2") return "Không được phép hoàn tiền với số tiền yêu cầu";
            if (vpc_ResponseCode == "3") return "Không tìm thấy giao dịch gốc";
            if (vpc_ResponseCode == "4") return "Không được phép hoàn giao dịch với số tiền lớn hơn số tiền giao dịch gốc.";
            if (vpc_ResponseCode == "5") return "Giao dịch đã được hoàn tiền.";
            if (vpc_ResponseCode == "9") return "Không được phép hoàn giao dịch này";
            if (vpc_ResponseCode == "99") return "Lỗi khác";

            return "Lỗi không xác định";
        }
        private string getMessageForQueryData(string vpc_ResponseCode)
        {
            if (vpc_ResponseCode == "-1") return "Lỗi bảo mật hệ thống";
            if (vpc_ResponseCode == "0") return "Giao dịch thành công";
            if (vpc_ResponseCode == "2") return "Giao dịch không thành công";
            return "Lỗi không xác định";
        }
        private string getMD5Input(NameValueCollection coll)
        {
            // Get ArrayList 
            ArrayList arrColl1 = getFieldsKey(coll);
            StringBuilder sb = new StringBuilder();
            foreach (object obj in arrColl1)
            {
                if (obj != null)
                {
                    sb.Append(getValuesKey(coll.GetValues(obj.ToString())));
                }
            }
            return sb.ToString();
        }

        private ArrayList getFieldsKey(NameValueCollection coll)
        {
            ArrayList myArrRet = new ArrayList();
            string[] arr1 = coll.AllKeys;
            for (int loop1 = 0; loop1 < arr1.Length; loop1++)
            {
                if (arr1[loop1] == null) continue;
                string vpc_name = HttpUtility.HtmlEncode(arr1[loop1]).Trim();
                vpc_name = vpc_name.Equals("vpc_SecureHash") ? "" : vpc_name;
                myArrRet.Add(vpc_name);
            }
            //myArrRet.Sort();
            return myArrRet;
        }

        private string getValuesKey(string[] arr2)
        {
            string strRet = "";
            if (arr2 == null) return strRet;
            for (int loop2 = 0; loop2 < arr2.Length; loop2++)
            {
                strRet = HttpUtility.HtmlEncode(arr2[loop2]);
            }
            return strRet;
        }

        private void PrintValues(IEnumerable myList)
        {
            foreach (Object obj in myList)
            {
                //Response.Write(obj+"<br>");
            }
        }
    }

}
