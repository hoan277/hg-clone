﻿namespace Golaco.bank
{
    /// <summary>
    ///1:vpc_MerchTxnRef,2:amount,3:information about services,4:card type,5: url to return
    /// </summary>
    /// <param name="values">url will redirect to</param>
    /// <returns></returns>
    public class VTCHolder
    {
        public string amount { get; set; }
        public string currency { get; set; }
        public string receiver_account = InstanceVTC.receiver_account;    // Tài khoản hứng tiền của đối tác tại VTC
        public string reference_number { get; set; }           // Mã đơn hàng của đối tác, VTC và đối tác dùng đơn hàng này làm cơ sở đối soát
        public string transaction_type = "sale";
        public string website_id = InstanceVTC.websiteid;//txtWebsiteID.Text.Trim();

        public string plaintext()
        {
            return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}", amount, currency, receiver_account, reference_number, transaction_type, website_id, InstanceVTC.Security_Key);
        }
        public string signature()
        {
            return SecurityVTC.SHA256encrypt(plaintext());
        }

        public string listparam()
        {
            return string.Format("website_id={0}&amount={1}&receiver_account={2}&reference_number={3}&currency={4}&signature={5}&transaction_type={6}",
            website_id, amount, receiver_account, reference_number, currency, signature(), transaction_type);
        }
        public string urlRedirect()
        {

            string urlRedirect = string.Format("{0}?{1}", InstanceVTC.url_run, listparam());
            return urlRedirect;
            //Golaco.Log.LogItem("url context.Request full: " + urlRedirect);
        }
    }
    public class GatewayResponse
    {
        public int status { get; set; }
        public string describe { get; set; }
        public string txtid { get; set; }
        public string urlPath { get; set; }
        public override string ToString()
        {
            return "{status:" + status.ToString()
                + ",describe:\"" + describe + "\""
                + ",txtid:\"" + txtid + "\""
                + ",urlPath:\"" + urlPath + "\""
                + "}";
        }

    }
}
