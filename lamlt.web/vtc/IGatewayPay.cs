﻿namespace Golaco.bank
{
    public interface IGatewayPay
    {
        //GatewayResponse ExcuteResultNotifyFromVTCPay(HttpContext context);
        GatewayResponse ExcuteResultRedirectUrl(Microsoft.AspNetCore.Http.HttpContext context);
        string getURLToBankNetToCharge(VTCHolder holder);
    }
}