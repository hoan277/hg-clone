﻿namespace lamlt.web.Models
{
    public class PagingModel
    {
        public int offset;
        public int limit;
        public string search;
        public int cpid;
        public int status;
        public int statusUserLog;
        public int catalogid;
        public string start_time;
        public string end_time;
        public string cp_title;
        public int sub_state;
        public int sub_type;
        public int publish_year;
        public int publish_countryid;
        public int film_type;
        public string action_type;// Dùng cho trước khi xuất Excel film_video_processing
        public string typeName;// Dùng cho film_transactions
        public int type_client;// Dùng cho film_transactions

        /*
         * dùng cho search link upload_file;
         */
        public string upload_file;
        public string search_video_type;
        // Dùng cho livestreaming
        public string live_type;
        public string live_status;
        public string live_userid;
        public string range_time;
        public int film_id;
        public int video_processing_id;
        public int product_id; // Dùng cho get list product process by product id
        public string arr_status;
        public string arr_search_date;
        public string arr_typeName;// Dùng cho film_transactions
        public string arr_display;// Dùng cho film_banner
        public string arr_catalogId;// Dùng cho film_banner
        public string arr_typeClient;// Dùng cho film_banner
    }
}
