﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_transactiondetailService : LamltService
    {

        public film_transactiondetailService()
        {

        }
        public List<film_transactiondetail> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactiondetail>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<film_transactiondetail> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_transactiondetail> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transactiondetail>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.txtid_transactions.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_film_transactiondetail> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactiondetail>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public film_transactiondetail GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactiondetail>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_transactiondetail GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transactiondetail>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(film_transactiondetail obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_transactiondetail>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.transid = obj.transid;
                        //objUpdate.txtid = obj.txtid;
                        objUpdate.address = obj.address;
                        objUpdate.amount1 = obj.amount1;
                        objUpdate.amount2 = obj.amount2;
                        objUpdate.currency1 = obj.currency1;
                        objUpdate.currency2 = obj.currency2;
                        objUpdate.status_url = obj.status_url;
                        objUpdate.qrcore_url = obj.qrcore_url;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.rateWS = obj.rateWS;
                        objUpdate.statusWS = obj.statusWS;
                        objUpdate.hashWS = obj.hashWS;
                        objUpdate.idRef = obj.idRef;
                        objUpdate.keyRef = obj.keyRef;
                        objUpdate.typeTrans = obj.typeTrans;
                        objUpdate.rateUSD1 = obj.rateUSD1;
                        objUpdate.rateUSD2 = obj.rateUSD2;
                        objUpdate.addressWS = obj.addressWS;
                        objUpdate.memberid = obj.memberid;
                        objUpdate.txthash = obj.txthash;
                        objUpdate.statusWS1 = obj.statusWS1;
                        objUpdate.memberidTo = obj.memberidTo;
                        objUpdate.txtHash1 = obj.txtHash1;
                        objUpdate.statusETH = obj.statusETH;
                        objUpdate.username = obj.username;
                        objUpdate.note = obj.note;
                        objUpdate.bankid = obj.bankid;
                        objUpdate.statusBanking = obj.statusBanking;
                        objUpdate.fullname = obj.fullname;
                        objUpdate.phone = obj.phone;
                        objUpdate.email = obj.email;
                        objUpdate.urlTemp = obj.urlTemp;
                        objUpdate.statusBankingResult = obj.statusBankingResult;
                        objUpdate.orderIdRef = obj.orderIdRef;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_transactiondetail>()
                        .Where(e => e.Id == obj.transid).Select(e => e.transid);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.transid = obj.transid;
                    objUpdate.address = obj.address;
                    objUpdate.amount1 = obj.amount1;
                    objUpdate.amount2 = obj.amount2;
                    objUpdate.currency1 = obj.currency1;
                    objUpdate.currency2 = obj.currency2;
                    objUpdate.status_url = obj.status_url;
                    objUpdate.qrcore_url = obj.qrcore_url;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.rateWS = obj.rateWS;
                    objUpdate.statusWS = obj.statusWS;
                    objUpdate.hashWS = obj.hashWS;
                    objUpdate.idRef = obj.idRef;
                    objUpdate.keyRef = obj.keyRef;
                    objUpdate.typeTrans = obj.typeTrans;
                    objUpdate.rateUSD1 = obj.rateUSD1;
                    objUpdate.rateUSD2 = obj.rateUSD2;
                    objUpdate.addressWS = obj.addressWS;
                    objUpdate.memberid = obj.memberid;
                    objUpdate.txthash = obj.txthash;
                    objUpdate.statusWS1 = obj.statusWS1;
                    objUpdate.memberidTo = obj.memberidTo;
                    objUpdate.txtHash1 = obj.txtHash1;
                    objUpdate.statusETH = obj.statusETH;
                    objUpdate.username = obj.username;
                    objUpdate.note = obj.note;
                    objUpdate.bankid = obj.bankid;
                    objUpdate.statusBanking = obj.statusBanking;
                    objUpdate.fullname = obj.fullname;
                    objUpdate.phone = obj.phone;
                    objUpdate.email = obj.email;
                    objUpdate.urlTemp = obj.urlTemp;
                    objUpdate.statusBankingResult = obj.statusBankingResult;
                    objUpdate.orderIdRef = obj.orderIdRef;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactiondetail>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_transactiondetail InitEmpty()
        {
            var obj = new film_transactiondetail();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_film_transactiondetail
		as
			select film_transactiondetail.Id,film_transactiondetail.title,film_transactiondetail.desc,film_transactiondetail.datecreated,film_transactiondetail.userid,film_transactiondetail.catalogid   ,film_transactiondetail.title as catalogid_title  from film_transactiondetail   inner join film_transactiondetail on film_transactiondetail.catalogid=film_transactiondetail.id  
		*/
    }
}