﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class live_catalogService : LamltService
    {

        public live_catalogService()
        {

        }
        public List<live_catalog> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<live_catalog>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<live_catalog> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_live_catalog> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_live_catalog>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_live_catalog> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<live_catalog>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public live_catalog GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<live_catalog>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_live_catalog GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_live_catalog>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        //public string GetAllCatalogNameByFilmId(int filmid, System.Data.IDbConnection db)
        //{
        //    //using (var db = _connectionData.OpenDbConnection())
        //    {
        //        var q = db.From<live_catalog>()
        //        .Join<live_catalog_film>((ca, ca_film) => ca.Id == ca_film.catalogid)
        //        .Where<live_catalog_film>(fcf => fcf.filmid == filmid);
        //        live_catalog[] live_catalog = db.Select(q).ToArray();
        //        string rs = "";
        //        foreach (var item in live_catalog)
        //        {
        //            if (string.IsNullOrEmpty(rs)) rs += item.title;
        //            else rs = rs + ", " + item.title;
        //        }
        //        return rs;
        //    }
        //}

        public int quantity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(live_catalog obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<live_catalog>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.desc = obj.desc;
                        objUpdate.title = obj.title;
                        objUpdate.quantity = obj.quantity;
                        objUpdate.code = obj.code;
                        objUpdate.user_id = comm.GetUserId();
                        //objUpdate.date_created = obj.date_created;
                        objUpdate.date_updated = DateTime.Now;
                        objUpdate.catalogid = obj.catalogid;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<live_catalog>()
                    .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.desc = obj.desc;
                    objUpdate.user_id = comm.GetUserId();
                    objUpdate.catalogid = obj.catalogid;
                    objUpdate.date_created = DateTime.Now;
                    objUpdate.date_updated = DateTime.Now;
                    objUpdate.title = obj.title;
                    objUpdate.quantity = obj.quantity;
                    objUpdate.code = obj.code;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<live_catalog>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public live_catalog InitEmpty()
        {
            var obj = new live_catalog();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_live_catalog
		as
			select live_catalog.Id,live_catalog.title,live_catalog.desc,live_catalog.datecreated,live_catalog.userid,live_catalog.catalogid   ,live_catalog.title as catalogid_title  from live_catalog   inner join live_catalog on live_catalog.catalogid=live_catalog.id  
		*/
    }
}