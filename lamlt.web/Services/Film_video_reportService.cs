﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_reportService : LamltService
    {

        public film_video_reportService()
        {

        }
        // forcus
        public List<vw_film_video_report> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_video_report>();
                if (comm.IsCP()) query = query.Where(e => e.cp_id == comm.GetCPId());
                query.OrderByDescending(x => x.video_id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.film_title.Contains(page.search)));

                if (page.cpid != -1) query = query.Where(e => e.cp_id == page.cpid);
                query.Where(e => e.dateview >= dt1 && e.dateview <= dt2);
                List<vw_film_video_report> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_video_report>();
                if (comm.IsCP()) query = query.Where(e => e.cp_id == comm.GetCPId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.film_title.Contains(page.search)));

                if (page.cpid != -1) query = query.Where(e => e.cp_id == page.cpid);
                query.Where(e => e.dateview >= dt1 && e.dateview <= dt2);
                return db.Count(query);
            }
        }
        public film_video GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_video_report GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_report>().Where(e => e.video_id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
    }
}