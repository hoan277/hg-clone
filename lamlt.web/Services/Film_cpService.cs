﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_cpService : LamltService
    {

        public film_cpService()
        {

        }
        public List<film_cp> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cp>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<film_cp> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_cp> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cp>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<vw_film_cp> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cp>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cp>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public film_cp GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cp>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_cp GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cp>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_cp obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_cp>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.shortname = obj.shortname;
                        objUpdate.address = obj.address;
                        objUpdate.rep = obj.rep;
                        objUpdate.email = obj.email;
                        objUpdate.contractname = obj.contractname;
                        objUpdate.contractphone = obj.contractphone;
                        objUpdate.contractemail = obj.contractemail;
                        objUpdate.desc = obj.desc;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.status = obj.status;
                        objUpdate.scan = obj.scan;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_cp>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.shortname = obj.shortname;
                    objUpdate.address = obj.address;
                    objUpdate.rep = obj.rep;
                    objUpdate.email = obj.email;
                    objUpdate.contractname = obj.contractname;
                    objUpdate.contractphone = obj.contractphone;
                    objUpdate.contractemail = obj.contractemail;
                    objUpdate.desc = obj.desc;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.status = obj.status;
                    objUpdate.scan = obj.scan;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cp>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_cp InitEmpty()
        {
            var obj = new film_cp();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_film_cp
		as
			select film_cp.Id,film_cp.title,film_cp.shortname,film_cp.address,film_cp.rep,film_cp.email,film_cp.contractname,film_cp.contractphone,film_cp.contractemail,film_cp.desc,film_cp.datecreated,film_cp.userid,film_cp.status  from film_cp  
		*/
    }
}