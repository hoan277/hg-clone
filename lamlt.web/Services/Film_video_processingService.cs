﻿using lamlt.data;
using lamlt.web.Models;
using NPOI.SS.UserModel;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace lamlt.web.Services
{
    public class film_video_processingService : LamltService
    {
        public film_video_processingService()
        {
        }

        public List<film_video_processing> GetAllItemByCatalogId(PagingModel page, int catalogId)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.catalog_id == catalogId);
                query = query.Skip(offset);
                List<film_video_processing> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_video_processing> GetViewAllItemByCatalogId(PagingModel page, int catalogId)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.catalog_id == catalogId);
                query = query.Skip(offset);
                List<vw_film_video_processing> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_video_processing> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Where(e => e.video_type == page.search_video_type);
                query = query.Skip(offset).Take(limit);
                List<vw_film_video_processing> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Where(e => e.video_type == page.search_video_type);
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public film_video_processing GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public film_video_processing GetByTitle(string title, string code)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>().Where(e => e.title == title && e.code == code);
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_video_processing GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public List<film_video_processing> GetInfo(int id, string title, string code, string upload_file)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>();
                if (id > 0) query = query.Where(e => e.Id == id);
                if (!string.IsNullOrEmpty(title)) query = query.Where(e => e.title == title);
                if (!string.IsNullOrEmpty(code)) query = query.Where(e => e.code == code);
                if (!string.IsNullOrEmpty(upload_file)) query = query.Where(e => e.upload_file == upload_file || e.title.Contains(upload_file));
                return db.Select(query).ToList();
            }
        }

        public int UpdateOrInsert(film_video_processing obj)
        {
            film_video_processing_transcriptService film_video_processing_transcriptSV = new film_video_processing_transcriptService();
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video_processing>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.desc = obj.desc;
                        objUpdate.duration = obj.duration;
                        objUpdate.upload_file = obj.upload_file;
                        objUpdate.catalog_id = obj.catalog_id;
                        objUpdate.status = obj.status;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.datecreated = !string.IsNullOrEmpty(obj.video_type) ? obj.datecreated : DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.actor = obj.actor;
                        objUpdate.director = obj.director;
                        objUpdate.beat = obj.beat;
                        objUpdate.lyric = obj.lyric;
                        objUpdate.json = obj.json;
                        objUpdate.srt = obj.srt;
                        objUpdate.code = obj.code;
                        objUpdate.master = obj.master;
                        objUpdate.video_type = !string.IsNullOrEmpty(obj.video_type) ? obj.video_type : objUpdate.video_type;
                        objUpdate.denoiser_step = obj.denoiser_step;
                        objUpdate.denoiser_result = obj.denoiser_result;
                        objUpdate.userid = comm.GetUserId();
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var query = db.From<film_video_processing>().Where(e => e.title == obj.title);
                    var obj_old = db.Select(query).FirstOrDefault();
                    var objUpdate = InitEmpty();
                    if (obj_old != null)
                    {
                        objUpdate = obj_old;
                    }
                    objUpdate.title = obj.title;
                    objUpdate.desc = obj.desc;
                    objUpdate.duration = obj.duration;
                    objUpdate.upload_file = obj.upload_file;
                    objUpdate.catalog_id = obj.catalog_id;
                    objUpdate.status = obj.status;
                    objUpdate.actor = obj.actor;
                    objUpdate.director = obj.director;
                    objUpdate.beat = obj.beat;
                    objUpdate.lyric = obj.lyric;
                    objUpdate.json = obj.json;
                    objUpdate.srt = obj.srt;
                    objUpdate.code = obj.code;
                    objUpdate.master = obj.master;
                    objUpdate.video_type = !string.IsNullOrEmpty(obj.video_type) ? obj.video_type : objUpdate.video_type;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.denoiser_step = obj.denoiser_step;
                    objUpdate.denoiser_result = obj.denoiser_result;
                    int result_update = 0;
                    if (obj_old != null)
                    {
                        result_update = db.Update(objUpdate);
                        if (result_update > 0) return objUpdate.Id;
                    }
                    else
                    {
                        result_update = (int)db.Insert(objUpdate, selectIdentity: true);
                    }

                    List<film_video_processing_transcript> list_video_processing_transcript = getListVideoProcessingTranscript();
                    foreach (var item in list_video_processing_transcript)
                    {
                        item.video_processing_id = result_update;
                        film_video_processing_transcriptSV.UpdateOrInsert(item);
                    }
                    if (result_update > 0) return result_update;
                    return -1;
                }
            }
        }

        public List<film_video_processing_transcript> getListVideoProcessingTranscript()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing_transcript>().Where(e => e.video_processing_id == 0);
                List<film_video_processing_transcript> rows = db.Select(query).Skip(0).ToList();
                return rows;
            }
        }

        internal string UpdateLog(int id, string status)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>().Where(e => e.Id == id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = status;
                    var newline = Environment.NewLine;
                    var oldLog = objUpdate.log;
                    objUpdate.log = "------------" + DateTime.Now + ": " + status + " ------------" + newline + oldLog;
                    int a = db.Update(objUpdate);
                    if (a > 0) return "Đã cập nhật xong";
                }
                return "Lỗi cập nhật API UpdateLog";
            }
        }

        public int UpdateStatus(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>().Where(e => e.Id == Id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    //objUpdate.status = "Đang xử lý";
                    int a = db.Update(objUpdate);
                    if (a > 0) return objUpdate.Id;
                }
                return -1;
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_video_processing InitEmpty()
        {
            var obj = new film_video_processing();
            obj.Id = 0;
            return obj;
        }

        //public List<vw_film_video_processing> GetViewAllItemByCatalogId(PagingModel page, int catalog_id)
        //{
        //    var random = new Random();

        //    if (page == null) page = new PagingModel() { offset = 0 };
        //    if (page.search == null) page.search = "";
        //    if (page.arr_search_date == null) { }
        //    using (var db = _connectionData.OpenDbConnection())
        //    {
        //        var query = db.From<vw_film_video_processing>();
        //        query = query.Where(e => e.catalog_id == catalog_id);
        //        query.OrderByRandom();
        //        List<vw_film_video_processing> rows = db.Select(query).ToList();
        //        return rows;
        //    }
        //}
        public List<vw_film_video_processing> GetViewAllItemByCatalogIdRanDom(PagingModel page, int catalog_id)
        {
            var random = new Random();

            if (page == null) page = new PagingModel() { offset = 0 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing>();
                query.OrderByRandom();
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // time range
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Where(e => e.catalog_id == catalog_id);
                query = query.Skip(offset).Take(5);
                //query = query.Skip(offset).Take(limit);
                List<vw_film_video_processing> rows = db.Select(query).ToList();
                return rows;
            }
        }

        // random 1
        public vw_film_video_processing GetViewItemByCatalogIdRanDom(PagingModel page, int catalog_id)
        {
            var random = new Random();
            if (page == null) page = new PagingModel() { offset = 0 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing>();
                query.OrderByRandom();
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // time range
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Where(e => e.catalog_id == catalog_id);
                query = query.Skip(offset).Take(1);
                return db.Select(query).FirstOrDefault();
                //return rows.FirstOrDefault();
            }
        }

        public film_video_processing ImportExcel(IRow row)
        {
            CultureInfo cultureVN = CultureInfo.GetCultureInfo("vi-VN");

            #region vi tri

            int index_code = row.FirstCellNum + 1; //code
            int index_upload = row.FirstCellNum + 2; // upload
            int index_title = row.FirstCellNum + 3; // title
            int index_director = row.FirstCellNum + 4; // direct
            int index_actor = row.FirstCellNum + 5; // actor
            int index_catalog_title = row.FirstCellNum + 6; //  catalog
            int index_publish_year = row.FirstCellNum + 7; // Năm sản xuất
            int index_publish_country = row.FirstCellNum + 8; // Quốc Gia
            int index_desc = row.FirstCellNum + 9; // Giới thiệu phim
            int index_lyric = row.FirstCellNum + 10; // Lyric(Lời bài hát)

            #endregion vi tri

            string code = ""; string upload = ""; string title = ""; string director = ""; string actor = ""; string catalog_title = "";
            string publish_year = ""; string publish_country = ""; string desc = ""; string lyric = "";
            code = row.GetCell(index_code).ToString();
            upload = row.GetCell(index_upload).ToString();
            title = row.GetCell(index_title).ToString();
            director = row.GetCell(index_director).ToString();
            actor = row.GetCell(index_actor).ToString();
            catalog_title = row.GetCell(index_catalog_title).ToString();
            publish_year = row.GetCell(index_publish_year).ToString();
            publish_country = row.GetCell(index_publish_country).ToString();
            desc = row.GetCell(index_desc).ToString();
            lyric = row.GetCell(index_lyric).ToString();
            film_video_processing video_process = new film_video_processing();
            //http://files.lalatv.com.vn:9090/content_origin/20191129/Ashes of Love 47.mp4
            if (!string.IsNullOrEmpty(code)) video_process.code = code;
            if (!string.IsNullOrEmpty(upload)) video_process.upload_file = "http://files.lalatv.com.vn:9090/content_origin/hg_upload/" + upload;
            if (!string.IsNullOrEmpty(title)) video_process.title = title;
            if (!string.IsNullOrEmpty(director)) video_process.director = director;
            if (!string.IsNullOrEmpty(actor)) video_process.actor = actor;
            using (var db = _connectionData.OpenDbConnection())
            {
                if (!string.IsNullOrEmpty(catalog_title))
                {
                    film_catalog current_catalog = db.Select(db.From<film_catalog>().Where(e => e.title.ToLower() == catalog_title.ToLower())).SingleOrDefault();
                    if (current_catalog != null) video_process.catalog_id = current_catalog.Id;
                };
                if (!string.IsNullOrEmpty(publish_country))
                {
                    film_country current_country = db.Select(db.From<film_country>().Where(e => e.title.ToLower() == publish_country.ToLower())).SingleOrDefault();
                    if (current_country != null) video_process.publish_countryid = current_country.Id;
                };
            }

            if (!string.IsNullOrEmpty(publish_year)) video_process.publish_year = int.Parse(publish_year);
            if (!string.IsNullOrEmpty(desc)) video_process.desc = desc;
            if (!string.IsNullOrEmpty(lyric)) video_process.lyric = lyric;
            return video_process;
        }

        #region _____updateByExcel_____

        public int UpdateOrInsertByExcel(film_video_processing obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video_processing>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.code = obj.code;
                        objUpdate.upload_file = obj.upload_file;
                        objUpdate.title = obj.title;
                        objUpdate.director = obj.director;
                        objUpdate.actor = obj.actor;
                        objUpdate.catalog_id = obj.catalog_id;
                        objUpdate.publish_year = obj.publish_year;
                        objUpdate.publish_countryid = obj.publish_countryid;
                        objUpdate.desc = obj.desc;
                        objUpdate.lyric = obj.lyric;
                        objUpdate.dateupdated = DateTime.Now;
                        int a = db.Update(objUpdate);
                        if (a > 0) return obj.Id;
                        return 0;
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.code = obj.code;
                    objUpdate.upload_file = obj.upload_file;
                    objUpdate.title = obj.title;
                    objUpdate.director = obj.director;
                    objUpdate.actor = obj.actor;
                    objUpdate.catalog_id = obj.catalog_id;
                    objUpdate.publish_year = obj.publish_year;
                    objUpdate.publish_countryid = obj.publish_countryid;
                    objUpdate.desc = obj.desc;
                    objUpdate.lyric = obj.lyric;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        #endregion _____updateByExcel_____
    }
}