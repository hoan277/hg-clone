﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_statisticService : LamltService
    {

        public film_video_statisticService()
        {

        }
        public List<film_video> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == -1) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));

                query = query.Where(e => (e.cpid == page.cpid));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                List<film_video> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        // forcus
        public List<vw_film_video_statistic> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_video_statistic>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));

                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                if (page.status != -1)
                {
                    if (page.status == 0)
                    {
                        query.Where(e => e.dateupdate1 >= dt1 && e.dateupdate1 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 1)
                    {
                        query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2 && e.status == page.status);
                    }
                    if (page.status == 2)
                    {
                        query.Where(e => e.dateupdate2 >= dt1 && e.dateupdate2 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 3)
                    {
                        query.Where(e => e.dateupdate3 >= dt1 && e.dateupdate3 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 4)
                    {
                        query.Where(e => e.dateupdate4 >= dt1 && e.dateupdate4 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 5)
                    {
                        query.Where(e => e.dateupdate5 >= dt1 && e.dateupdate5 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 6)
                    {
                        query.Where(e => e.dateupdate6 >= dt1 && e.dateupdate6 <= dt2 && e.status == page.status);
                    }
                }
                else
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                //if (page.status == 0) { query.Where(e => e.dateupdate1 >= dt1 && e.dateupdate1 <= dt2); }
                //if (page.status == 1) { query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2); }
                //if (page.status == 2) { query.Where(e => e.dateupdate2 >= dt1 && e.dateupdate2 <= dt2); }
                //if (page.status == 3) { query.Where(e => e.dateupdate3 >= dt1 && e.dateupdate3 <= dt2); }
                //if (page.status == 4) { query.Where(e => e.dateupdate4 >= dt1 && e.dateupdate4 <= dt2); }
                //if (page.status == 5) { query.Where(e => e.dateupdate5 >= dt1 && e.dateupdate5 <= dt2); }
                //if (page.status == 6) { query.Where(e => e.dateupdate6 >= dt1 && e.dateupdate6 <= dt2); }
                if (page.catalogid != -1) query = query.Where(e => e.catalog_id == page.catalogid);
                List<vw_film_video_statistic> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));

                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                if (page.status != -1)
                {
                    if (page.status == 0)
                    {
                        query.Where(e => e.dateupdate1 >= dt1 && e.dateupdate1 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 1)
                    {
                        query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2 && e.status == page.status);
                    }
                    if (page.status == 2)
                    {
                        query.Where(e => e.dateupdate2 >= dt1 && e.dateupdate2 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 3)
                    {
                        query.Where(e => e.dateupdate3 >= dt1 && e.dateupdate3 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 4)
                    {
                        query.Where(e => e.dateupdate4 >= dt1 && e.dateupdate4 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 5)
                    {
                        query.Where(e => e.dateupdate5 >= dt1 && e.dateupdate5 <= dt2 && e.status == page.status);
                    }
                    if (page.status == 6)
                    {
                        query.Where(e => e.dateupdate6 >= dt1 && e.dateupdate6 <= dt2 && e.status == page.status);
                    }
                }
                else
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                if (page.catalogid != -1) query = query.Where(e => e.catalog_id == page.catalogid);
                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_video_statistic>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));

                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                //if (page.status != -1) query = query.Where(e => e.status == page.status);
                if (page.status == 0) { query.Where(e => e.dateupdate1 >= dt1 && e.dateupdate1 <= dt2); }
                if (page.status == 1) { query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2); }
                if (page.status == 2) { query.Where(e => e.dateupdate2 >= dt1 && e.dateupdate2 <= dt2); }
                if (page.status == 3) { query.Where(e => e.dateupdate3 >= dt1 && e.dateupdate3 <= dt2); }
                if (page.status == 4) { query.Where(e => e.dateupdate4 >= dt1 && e.dateupdate4 <= dt2); }
                if (page.status == 5) { query.Where(e => e.dateupdate5 >= dt1 && e.dateupdate5 <= dt2); }
                if (page.status == 6) { query.Where(e => e.dateupdate6 >= dt1 && e.dateupdate6 <= dt2); }
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                return db.Count(query);
            }
        }
        public film_video GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_video_statistic GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_statistic>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        /*
		genvie here
		create view vw_film_video_statistic
		as
			select film_video_statistic.Id,film_video_statistic.title,film_video_statistic.desc,film_video_statistic.datecreated,film_video_statistic.userid,film_video_statistic.seriid,film_video_statistic.imdb,film_video_statistic.format,film_video_statistic.sub_type,film_video_statistic.publish_year,film_video_statistic.publish_countryid,film_video_statistic.duration,film_video_statistic.actor,film_video_statistic.director,film_video_statistic.film_type,film_video_statistic.episode,film_video_statistic.episode_current,film_video_statistic.contract_copyright,film_video_statistic.contract_exprired,film_video_statistic.contract_appendix,film_video_statistic.copyright_appendix,film_video_statistic.copyright_expired,film_video_statistic.catalog_id,film_video_statistic.upload_file,film_video_statistic.thumb_file,film_video_statistic.exclusive,film_video_statistic.price,film_video_statistic.status,film_video_statistic.cpid   ,film_country.title as publish_countryid_title  ,film_catalog.title as catalog_id_title  ,film_cp.title as cpid_title  from film_video_statistic   inner join film_country on film_video_statistic.publish_countryid=film_country.id    inner join film_catalog on film_video_statistic.catalog_id=film_catalog.id    inner join film_cp on film_video_statistic.cpid=film_cp.id  
		*/
    }
}