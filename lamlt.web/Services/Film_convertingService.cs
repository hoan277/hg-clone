﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace lamlt.web.Services
{
    public class film_convertingService : LamltService
    {

        public film_convertingService()
        {

        }
        public List<film_converting> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_converting>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<film_converting> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_converting> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_converting>();
                query.OrderByDescending(x => x.start_time);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                if (page.status == 0)
                {
                    query = query.Where(e => e.path.Contains(page.search));
                }
                else
                {
                    query = query.Where(e => e.path.Contains(page.search) && e.status == page.status);
                }

                List<vw_film_converting> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_converting>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }

        public Dictionary<object, object> update_pid(int id, int pid, string type)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_converting>();
                if (id > 0) query.Where(e => e.Id == id);
                if (pid > 0) query.Where(e => e.process_id == pid);

                film_converting obj = db.Select(query).SingleOrDefault();

                if (obj != null)
                {
                    int rs = db.Update(new film_converting()
                    {
                        Id = obj.Id,
                        title = obj.title,
                        path = obj.path,
                        status = obj.status,
                        start_time = obj.start_time,
                        date_time = obj.date_time,
                        duration = obj.duration,
                        cmd_id = obj.cmd_id,
                        cmd_title = obj.cmd_title,
                        process_id = type == "create" ? pid : 0
                    });
                    if (rs > 0)
                    {
                        dict.Add("code", "success");
                        dict.Add("message", "Cập nhật thành công process id: " + pid);
                    }
                    else
                    {
                        dict.Add("code", "error");
                        dict.Add("message", "Lỗi không cập nhật được vào cơ sở dữ liệu");
                    }
                }
                else
                {
                    dict.Add("code", "error");
                    dict.Add("message", "Không tìm thấy id của phim đang thực hiện convert");
                }
            }
            return dict;
        }

        public Dictionary<object, object> create_pid_txt(int pid)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string folderPath = "/home/amnhacsaigon/film_converting/";
            if (isDev)
            {
                folderPath = "C://";
            }

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fileName = folderPath + pid + ".txt";
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            using (StreamWriter sw = new StreamWriter(fileName, true, Encoding.ASCII))
            {
                sw.Write(pid);
                sw.Flush();
                sw.Close();
            }
            dict.Add("code", "success");
            dict.Add("message", "Tạo file pid thành công");
            return dict;
        }

        public film_converting GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_converting>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_converting GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_converting>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateByPath(string path, int status, string title)
        {
            // status 5: bắt đầu convert, 6: convert lỗi, 1: convert xong
            LogService.logItem("============================= /api/film_converting/update --> UpdateByPath =============================");
            LogService.logItem("path: " + path);
            LogService.logItem("status: " + status);
            LogService.logItem("title: " + title);

            using (var db = _connectionData.OpenDbConnection())
            {
                // get top 1 cmd_converting
                var query_cmd = db.From<film_cmd_converting>();
                query_cmd.OrderByDescending(x => x.Id);
                List<film_cmd_converting> rows = db.Select(query_cmd).Skip(0).Take(1).ToList();
                var query = db.From<film_converting>().Where(e => e.path == path);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.title = title;
                    objUpdate.path = path;
                    objUpdate.status = status;
                    if (status == 5)
                    {
                        objUpdate.date_time = null; objUpdate.start_time = DateTime.Now;
                    }
                    else
                    {
                        objUpdate.date_time = DateTime.Now;
                    }
                    //objUpdate.cmd_id = rows[0].Id;
                    //objUpdate.cmd_title = rows[0].idconverting;
                    return db.Update(objUpdate);
                }
                else
                {
                    objUpdate = InitEmpty();
                    objUpdate.title = title;
                    objUpdate.path = path;
                    objUpdate.start_time = DateTime.Now;
                    objUpdate.status = status;
                    //objUpdate.cmd_id = rows[0].Id;
                    //objUpdate.cmd_title = rows[0].idconverting;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int UpdatePathTrainer(string path, int status, string title, string trainer)
        {
            if (trainer != null)
            {
                LogService.logItem("============================= /api/film_converting/update --> UpdateByPath =============================");
                LogService.logItem("path: " + path);
                LogService.logItem("status: " + status);
                LogService.logItem("title: " + title);
                LogService.logItem("trainer: " + trainer);

                using (var db = _connectionData.OpenDbConnection())
                {
                    var query_video = db.From<film_video>().Where(e => e.path == path);
                    var objVideo = db.Select(query_video).SingleOrDefault();

                    if (objVideo != null)
                    {
                        objVideo.trainer_url = trainer;
                        db.Update(objVideo);
                    }
                    var query = db.From<film_converting>().Where(e => e.path == path);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.title = title;
                        objUpdate.path = path;
                        objUpdate.status = status;
                        if (status == 5)
                        {
                            objUpdate.date_time = null; objUpdate.start_time = DateTime.Now;
                        }
                        else
                        {
                            objUpdate.date_time = DateTime.Now;
                        }
                        return db.Update(objUpdate);
                    }
                    else
                    {
                        objUpdate = InitEmpty();
                        objUpdate.title = title;
                        objUpdate.path = path;
                        objUpdate.start_time = DateTime.Now;
                        objUpdate.status = status;
                        return (int)db.Insert(objUpdate, selectIdentity: true);

                    }
                }
            }
            else
            {
                LogService.logItem("============================= /api/film_converting/update --> UpdateByPath =============================");
                LogService.logItem("path: " + path);
                LogService.logItem("status: " + status);
                LogService.logItem("title: " + title);

                using (var db = _connectionData.OpenDbConnection())
                {
                    var query = db.From<film_converting>().Where(e => e.path == path);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.title = title;
                        objUpdate.path = path;
                        objUpdate.status = status;
                        if (status == 5)
                        {
                            objUpdate.date_time = null; objUpdate.start_time = DateTime.Now;
                        }
                        else
                        {
                            objUpdate.date_time = DateTime.Now;
                        }
                        return db.Update(objUpdate);
                    }
                    else
                    {
                        objUpdate = InitEmpty();
                        objUpdate.title = title;
                        objUpdate.path = path;
                        objUpdate.start_time = DateTime.Now;
                        objUpdate.status = status;
                        return (int)db.Insert(objUpdate, selectIdentity: true);
                    }
                }
            }
        }

        public int UpdateOrInsert(film_converting obj)
        {
            //LogService.logItem("============================= film_converting/Update --> UpdateOrInsert =============================");
            //LogService.logItem("obj.Id: " + obj.Id);
            //LogService.logItem("obj.title: " + obj.title);
            //LogService.logItem("obj.path: " + obj.path);
            //LogService.logItem("obj.start_time: " + obj.start_time);
            //LogService.logItem("obj.date_time: " + obj.date_time);
            //LogService.logItem("obj.status: " + obj.status);

            using (var db = _connectionData.OpenDbConnection())
            {
                //update
                if (obj.Id > 0)
                {
                    var query = db.From<film_converting>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (obj.status == 1) objUpdate.start_time = DateTime.Now;
                    objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.path = obj.path;
                    objUpdate.date_time = obj.date_time;
                    objUpdate.status = obj.status;
                    objUpdate.process_id = obj.process_id;
                    return db.Update(objUpdate);
                }
                else
                {
                    //insert
                    var objUpdate = InitEmpty();
                    if (obj.status == 1) objUpdate.start_time = DateTime.Now;
                    objUpdate.title = obj.title;
                    objUpdate.path = obj.path;
                    objUpdate.date_time = obj.date_time;
                    objUpdate.status = obj.status;
                    objUpdate.process_id = obj.process_id;
                    return (int)db.Insert(objUpdate, selectIdentity: true);

                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_converting>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_converting InitEmpty()
        {
            var obj = new film_converting();
            obj.Id = 0;
            return obj;
        }
    }
}