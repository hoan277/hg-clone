﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_landing_page_prService : LamltService
    {

        public film_landing_page_prService()
        {

        }
        public List<film_landing_page_pr> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == -1) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_landing_page_pr>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.source.Contains(page.search)));
                List<film_landing_page_pr> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        // forcus
        public List<vw_film_landing_page_pr> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_landing_page_pr>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.source.Contains(page.search)));
                if (page.status != -1)
                {
                    if (page.status == 0)
                    {
                        query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                    }
                }
                else
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                List<vw_film_landing_page_pr> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<film_landing_page_pr>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.source.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_landing_page_pr>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.source.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                return db.Count(query);
            }
        }





        public film_landing_page_pr GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_landing_page_pr>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_landing_page_pr GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_landing_page_pr>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_landing_page_pr>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(film_landing_page_pr obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_landing_page_pr>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.guidid = obj.guidid;
                        objUpdate.source = obj.source;
                        objUpdate.ip = obj.ip;
                        objUpdate.datecreated = DateTime.Now;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_landing_page_pr>()
                        .Where(e => e.source == obj.source).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.guidid = obj.guidid;
                    objUpdate.source = obj.source;
                    objUpdate.ip = obj.ip;
                    objUpdate.datecreated = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_landing_page_pr>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_landing_page_pr InitEmpty()
        {
            var obj = new film_landing_page_pr();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_film_landing_page_pr
		as
			select film_landing_page_pr.Id,film_landing_page_pr.title,film_landing_page_pr.desc,film_landing_page_pr.datecreated,film_landing_page_pr.userid,film_landing_page_pr.catalogid   ,film_landing_page_pr.title as catalogid_title  from film_landing_page_pr   inner join film_landing_page_pr on film_landing_page_pr.catalogid=film_landing_page_pr.id  
		*/
    }
}