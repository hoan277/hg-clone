﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace lamlt.web.Services
{
    public class translateService : LamltService
    {

        public translateService()
        {

        }
        public List<translate> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<translate>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.title.Contains(page.search));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<translate> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_translate> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_translate>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_translate> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        internal object GetInfo(int id, string title, string status)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<translate>();
                if (id > 0) query = query.Where(e => e.Id == id);
                if (!string.IsNullOrEmpty(title)) query = query.Where(e => e.title == title);
                if (!string.IsNullOrEmpty(status)) query = query.Where(e => e.status == status);
                query.OrderByDescending(x => x.Id);
                return db.Select(query).ToList();
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<translate>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_translate>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public translate GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<translate>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_translate GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_translate>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(translate obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<translate>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = string.IsNullOrEmpty(obj.title) ? objUpdate.title : obj.title;
                        objUpdate.image = obj.image;
                        objUpdate.result_orc = obj.result_orc;
                        objUpdate.result = obj.result;
                        objUpdate.status = obj.status;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    //var queryCount = db.From<translate>().Where(e => e.title == obj.title).Select(e => e.Id);
                    //var objCount = db.Count(queryCount);
                    //if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.image = obj.image;
                    objUpdate.result_orc = obj.result_orc;
                    objUpdate.result = obj.result;
                    objUpdate.status = obj.status;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            string applicationPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            System.Console.WriteLine(applicationPath + "/wwwroot/translate_file_result/" + Id);
            if (Directory.Exists(applicationPath + "/wwwroot/translate_file_result/" + Id))
            {
                Directory.Delete(applicationPath + "/wwwroot/translate_file_result/" + Id, true);
            }
            using (var db = _connectionData.OpenDbConnection())
            {

                var query = db.From<translate>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public translate InitEmpty()
        {
            var obj = new translate();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_translate
		as
			select translate.Id,translate.title,translate.image,translate.result,translate.status  from translate  
		*/
    }
}