﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_imageService : LamltService
    {

        public film_video_imageService()
        {

        }
        public List<film_video_image> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.url.Contains(page.search)));
                /**/


                List<film_video_image> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.url.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_image>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.url.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public List<film_video_image> updateImg(int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.filmid == filmid);
                return db.Select(query).ToList<film_video_image>();
            }
        }
        //public film_video_image getImageupdate(int fil)
        //{
        //    using (var db = _connectionData.OpenDbConnection())
        //    {
        //        var query = db.From<film_video_image>().Where(e => e.Id == id);
        //        return db.Select(query).SingleOrDefault();
        //    }

        //}
        public film_video_image GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_video_image GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_image>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_video_image obj, string pathImage)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_video_image>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.filmid = obj.filmid;
                        if (obj.url != null && !obj.url.StartsWith("http"))
                        {
                            objUpdate.url = pathImage + obj.url;
                        }
                        objUpdate.datecreated = DateTime.Now;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_video_image>()
                        .Where(e => e.url == obj.url).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    // objUpdate.Id = obj.Id;
                    objUpdate.filmid = obj.filmid;
                    if (obj.url != null && !obj.url.StartsWith("http"))
                    {
                        objUpdate.url = pathImage + obj.url;
                    }
                    objUpdate.datecreated = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_video_image InitEmpty()
        {
            var obj = new film_video_image();
            obj.Id = 0;
            obj.url = "";
            return obj;
        }
        /*
		public List<vw_film_video_image> GetViewAllItem(PagingModel page) 
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

          //  ServiceStackHelper.Help();
          //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
			var query = db.From<vw_film_video_image>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
				query=query.Where(e => (e.title.Contains(page.search)));
				
				
				
                List<vw_film_video_image> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;                
            }
        }
		genvie here
		create view vw_film_video_image
		as
			select film_video_image.Id,film_video_image.filmid,film_video_image.url,film_video_image.datecreated  from film_video_image  
		*/
    }
}