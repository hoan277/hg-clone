﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_contactService : LamltService
    {

        public film_contactService()
        {

        }
        public List<film_contact> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_contact>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<film_contact> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_contact> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_contact>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<vw_film_contact> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_contact>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_contact>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public film_contact GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_contact>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_contact GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_contact>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_contact obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_contact>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.userid = obj.userid;
                        objUpdate.full_name = obj.full_name;
                        objUpdate.phone = obj.phone;
                        objUpdate.email = obj.email;
                        objUpdate.title = obj.title;
                        objUpdate.content = obj.content;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid_created = obj.userid_created;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.userid = obj.userid;
                    objUpdate.full_name = obj.full_name;
                    objUpdate.phone = obj.phone;
                    objUpdate.email = obj.email;
                    objUpdate.title = obj.title;
                    objUpdate.content = obj.content;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid_created = obj.userid_created;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_contact>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_contact InitEmpty()
        {
            var obj = new film_contact();
            obj.Id = 0;
            return obj;
        }
    }
}