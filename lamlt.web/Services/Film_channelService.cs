﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_channelService : LamltService
    {
        public film_channelService()
        {
        }

        public List<film_channel> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_channel>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Skip(offset).Take(limit);
                List<film_channel> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_channel> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_channel>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Skip(offset).Take(limit);
                List<vw_film_channel> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_channel>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_channel>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public film_channel GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_channel>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public film_channel GetByTitle(string title)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_channel>().Where(e => e.title == title || e.title.Contains(title));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_channel GetViewById(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_channel>().Where(e => e.Id == id);
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_channel GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_channel>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_channel obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_channel>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.description = obj.description;
                        objUpdate.username = obj.username;
                        objUpdate.password = obj.password;
                        objUpdate.ip = obj.ip;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.status = obj.status;
                        objUpdate.userid = comm.GetUserId();
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.title = obj.title;
                    objUpdate.description = obj.description;
                    objUpdate.username = obj.username;
                    objUpdate.password = obj.password;
                    objUpdate.ip = obj.ip;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.status = obj.status;
                    objUpdate.userid = comm.GetUserId();
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int UpdateStatus(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_channel>().Where(e => e.Id == Id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = 1;
                    int a = db.Update(objUpdate);
                    if (a > 0) return objUpdate.Id;
                }
                return -1;
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_channel>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_channel InitEmpty()
        {
            var obj = new film_channel();
            obj.Id = 0;
            return obj;
        }
    }
}