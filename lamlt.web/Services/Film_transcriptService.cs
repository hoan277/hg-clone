﻿using lamlt.data;
using lamlt.web.Models;
using NPOI.SS.Formula.Functions;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_transcriptService : LamltService
    {
        public film_transcriptService()
        {
        }

        public List<film_transcript> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Skip(offset).Take(limit);
                List<film_transcript> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_transcript> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Skip(offset).Take(limit);
                List<vw_film_transcript> rows = db.Select(query).ToList();
                return rows;
            }
        }

        internal object GetInfo(int id, string title)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>();
                query.OrderByDescending(x => x.Id);
                if (id > 0) { query = query.Where(e => e.Id == id); }
                if (!string.IsNullOrEmpty(title)) { query = query.Where(e => e.title == title); }
                List<film_transcript> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        internal Dictionary<object, object> gen_srt(string filesrt, string content)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "", code = "";
            string filePath = "/home/www/data/data/saigonmusic/hg_project_sub/" + filesrt;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>().Where(e => e.file.Contains(filesrt));
                film_transcript obj = db.Select(query).FirstOrDefault();
                if (obj != null)
                {
                    obj.desc = content;
                    obj.dateupdated = DateTime.Now;
                    db.Update(obj);
                }
            }
            using (StreamWriter sw = new StreamWriter(filePath))
            {
                sw.Write(content);
                code = "success";
                message = "Cập nhật file srt thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public film_transcript GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_transcript GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transcript>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_transcript obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_transcript>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.file = obj.file;
                        objUpdate.code = obj.code;
                        objUpdate.catalogid = obj.catalogid;
                        objUpdate.language = obj.language;
                        objUpdate.keywords = obj.keywords;
                        objUpdate.desc = obj.desc;
                        objUpdate.quatity = obj.quatity;
                        objUpdate.position = obj.position;
                        objUpdate.status = obj.status;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.isview = obj.isview;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    var query = db.From<film_transcript>().Where(e => e.title == obj.title);
                    objUpdate.title = obj.title;
                    objUpdate.file = obj.file;
                    objUpdate.code = obj.code;
                    objUpdate.catalogid = obj.catalogid;
                    objUpdate.language = obj.language;
                    objUpdate.keywords = obj.keywords;
                    objUpdate.desc = obj.desc;
                    objUpdate.quatity = obj.quatity;
                    objUpdate.position = obj.position;
                    objUpdate.status = obj.status;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.isview = obj.isview;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int UpdateStatus(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>().Where(e => e.Id == Id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = 1;
                    int a = db.Update(objUpdate);
                    if (a > 0) return objUpdate.Id;
                }
                return -1;
            }
        }

        internal film_transcript GetSubProcessingID(string processingId, string lang)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>().Where(e => e.code == (processingId + "_" + lang));
                var obj = db.Select(query).LastOrDefault();
                if (obj != null)
                {
                    return obj;
                }
                return null;
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transcript>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_transcript InitEmpty()
        {
            var obj = new film_transcript();
            obj.Id = 0;
            return obj;
        }
    }
}