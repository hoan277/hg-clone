﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class SupplierService : LamltService
    {
        //OrmLiteConnectionFactory _connectionData;
        public SupplierService()
        {
            //OrmLiteConfig.DialectProvider = SqlServerDialect.Provider;
            //OrmLiteConfig.DialectProvider.UseUnicode = true; // nhập tiếng việt
            //_connectionData = new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["ConnectionString_OzeGeneral"].ConnectionString, SqlServerOrmLiteDialectProvider.Instance);
        }
        public List<tbl_Supplier> getAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<tbl_Supplier>();
                //query = query.Where(e => e.SysHotelID == comm.GetHotelId() || e.isGlobal == 1);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                List<tbl_Supplier> rows = db.Select(query)
                    .Where(e => (e.Name ?? "").Contains(page.search))
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long countAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<tbl_Supplier>();
                //query = query.Where(e => e.SysHotelID == comm.GetHotelId() || e.isGlobal == 1);
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                return db.Count(query.Where(e => e.Name.Contains(page.search)));
            }
        }

        public tbl_Supplier GetSupplierByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<tbl_Supplier>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsertSupplier(tbl_Supplier obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<tbl_Supplier>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Address = obj.Address;
                        objUpdate.Code = DateTime.Now.Ticks.ToString();
                        objUpdate.CodeFake = obj.CodeFake;
                        objUpdate.Name = obj.Name;
                        objUpdate.Contact_Person_Email = obj.Contact_Person_Email;
                        objUpdate.Contact_Person_Mobile = obj.Contact_Person_Mobile;
                        objUpdate.Contact_Person_Name = obj.Contact_Person_Name;
                        objUpdate.Contact_Person_Phone = obj.Contact_Person_Phone;
                        objUpdate.Email = obj.Email;

                        objUpdate.sup_type = obj.sup_type;
                        objUpdate.sup_sign = obj.sup_sign;
                        objUpdate.sup_note = obj.sup_note;

                        db.Update(objUpdate);
                        return objUpdate.Id;
                    }
                    return -1;
                }
                else
                {
                    obj.Code = DateTime.Now.Ticks.ToString(); ;
                    var queryCount = db.From<tbl_Supplier>().Where(e => e.Name == obj.Name).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0)
                    {
                        var queryUpdate = db.Single<tbl_Supplier>(e => e.Code == obj.Code);

                        db.Update(queryUpdate);
                        return queryUpdate.Id;// comm.ERROR_EXIST;
                    }
                    obj.SysHotelID = comm.GetUserId();
                    return (int)db.Insert(obj, selectIdentity: true);
                }
            }
        }
        public int DeleteSupplier(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // Kiểm tra nếu nhà cung cấp được sử dụng trong bảng sản phẩm thì không cho xóa, trả về -1
                if (db.Exists(db.From<tbl_Product>().Where(p => p.SupplierID == Id)))
                {
                    return -1;
                }
                var query = db.From<tbl_Supplier>().Where(e => e.Id == Id);
                //var objUpdate = db.Select(query).SingleOrDefault();
                return db.Delete(query);
            }
        }
        public tbl_Supplier InitEmpty()
        {
            var obj = new tbl_Supplier();
            obj.Id = 0;
            obj.Address = "";
            obj.Code = "";
            obj.Contact_Person_Email = "";
            obj.Contact_Person_Mobile = "";
            obj.Contact_Person_Name = "";
            obj.Contact_Person_Phone = "";
            obj.Email = "";
            obj.Code = DateTime.Now.Ticks.ToString();
            return obj;
        }
    }


}