﻿using Microsoft.Extensions.Configuration;
using ServiceStack.OrmLite;
using System;
using System.IO;

namespace lamlt.web.Services
{
    public class LamltService
    {
        protected OrmLiteConnectionFactory _connectionData;

        public LamltService()
        {
            OrmLiteConfig.DialectProvider = MySqlDialect.Provider;
            //OrmLiteConfig.DialectProvider. = true;
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string cnn = isDev ? "server" : "local";
            _connectionData = new OrmLiteConnectionFactory(GetConnectionString(cnn), OrmLiteConfig.DialectProvider);
        }
        public static IConfigurationRoot Configuration;
        public static string GetConnectionString(string server)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("conf/appsettings.json");
            Configuration = builder.Build();
            return Configuration[server];
        }
    }
}