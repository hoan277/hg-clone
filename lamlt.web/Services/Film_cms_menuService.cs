﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_cms_menuService : LamltService
    {

        public film_cms_menuService()
        {

        }
        public List<film_cms_menu> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cms_menu>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<film_cms_menu> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_cms_menu> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cms_menu>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<vw_film_cms_menu> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cms_menu>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cms_menu>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public film_cms_menu GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cms_menu>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_cms_menu GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cms_menu>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_cms_menu obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_cms_menu>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.desc = obj.desc;
                        objUpdate.url = obj.url;
                        objUpdate.position = obj.position;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.status = obj.status;
                        objUpdate.parent_id = obj.parent_id;
                        objUpdate.userid = obj.userid;
                        objUpdate.str_menu_id = obj.str_menu_id;
                        objUpdate.menu_icon = obj.menu_icon;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.title = obj.title;
                    objUpdate.desc = obj.desc;
                    objUpdate.url = obj.url;
                    objUpdate.position = obj.position;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.status = obj.status;
                    objUpdate.parent_id = obj.parent_id;
                    objUpdate.userid = obj.userid;
                    objUpdate.str_menu_id = obj.str_menu_id;
                    objUpdate.menu_icon = obj.menu_icon;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cms_menu>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_cms_menu InitEmpty()
        {
            var obj = new film_cms_menu();
            obj.Id = 0;
            return obj;
        }

        // get list by position
        public List<vw_film_cms_menu> GetListViewByPosition(PagingModel page, int position)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100000 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cms_menu>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.position == position);
                query = query.Where(e => (e.title.Contains(page.search)));
                List<vw_film_cms_menu> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        // get list by parent_id
        public List<vw_film_cms_menu> GetListViewByParentId(PagingModel page, int parent_id)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100000 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cms_menu>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                if (parent_id != -1)
                {
                    query = query.Where(e => e.parent_id == parent_id);
                }
                query = query.Where(e => (e.title.Contains(page.search)));
                List<vw_film_cms_menu> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public film_cms_menu GetViewByTitle(string title)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cms_menu>().Where(e => e.title.Contains(title));
                return db.Select(query).SingleOrDefault();
            }
        }
    }
}