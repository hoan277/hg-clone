﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_catalog_filmService : LamltService
    {

        public film_catalog_filmService()
        {

        }
        public List<film_catalog_film> GetAllCatalog()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //SELECT fc.id, fc.title
                //FROM film_catalog_film fcf
                //LEFT JOIN film_catalog fc
                //ON fcf.filmid = 232 where fc.id = fcf.catalogid

                //var q = db.From<film_catalog_film>()
                //.Join<film_catalog_film, film_catalog>((fcf, fc) => fcf.filmid == filmid && fc.Id == fcf.catalogid);
                //return db.Select(q).ToList();
                var query = db.From<film_catalog_film>();
                List<film_catalog_film> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<film_catalog_film> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog_film>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query=query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<film_catalog_film> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<film_catalog_film> FilmId(int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog_film>().Where(e => e.filmid == filmid);
                List<film_catalog_film> row = db.Select(query);
                return row;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog_film>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query=query.Where(e => (e.title .Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_catalog_film>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query=query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public film_catalog_film GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog_film>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_catalog_film GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_catalog_film>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_catalog_film obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_catalog_film>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.catalogid = obj.catalogid;
                        objUpdate.filmid = obj.filmid;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    /*var queryCount = db.From<film_catalog_film>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;*/
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.catalogid = obj.catalogid;
                    objUpdate.filmid = obj.filmid;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_catalog_film>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_catalog_film InitEmpty()
        {
            var obj = new film_catalog_film();
            obj.Id = 0;
            return obj;
        }
        /*
        public List<vw_film_catalog_film> GetViewAllItem(PagingModel page) 
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

          //  ServiceStackHelper.Help();
          //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
            var query = db.From<vw_film_catalog_film>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query=query.Where(e => (e.title.Contains(page.search)));
                
                
                
                List<vw_film_catalog_film> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;                
            }
        }
        genvie here
        create view vw_film_catalog_film
        as
            select film_catalog_film.Id,film_catalog_film.catalogid,film_catalog_film.filmid,film_catalog_film.datecreated,film_catalog_film.userid   ,film_catalog.title as catalogid_title  ,film_video.title as filmid_title  from film_catalog_film   inner join film_catalog on film_catalog_film.catalogid=film_catalog.id    inner join film_video on film_catalog_film.filmid=film_video.id  
        */
    }
}
