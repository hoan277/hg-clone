﻿using lamlt.data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class Film_contractService : LamltService
    {
        public Film_contractService()
        {
        }

        public Dictionary<object, object> GetByFilmID_List(int film_id)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "", scode = "";
            List<film_contract> data = new List<film_contract>();
            if (film_id > 0)
            {
                using (var db = _connectionData.OpenDbConnection())
                {
                    data = db.Select(db.From<film_contract>().Where(e => e.film_id == film_id)).ToList();
                    if (data != null)
                    {
                        scode = "success";
                        message = "Lấy dữ liệu thành công";
                        dict.Add("data", data);
                    }
                    else
                    {
                        scode = "error";
                        message = "Không có dữ liệu phù hợp";
                    }
                }
            }
            else
            {
                scode = "error";
                message = "Code không được dể trống";
            }

            dict.Add("code", scode);
            dict.Add("message", message);
            return dict;
        }

        internal int UpdateOrInsert(film_contract obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_contract>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.film_id = obj.film_id;
                        objUpdate.number = obj.number;
                        objUpdate.appendix = obj.appendix;
                        objUpdate.censorship = obj.censorship;
                        objUpdate.copyright = obj.copyright;
                        objUpdate.date_sign = obj.date_sign;
                        objUpdate.date_expired = obj.date_expired;
                        objUpdate.partner = obj.partner;
                        objUpdate.file = obj.file;
                        objUpdate.type = obj.type;
                        objUpdate.date_created = obj.date_created != null ? obj.date_created : DateTime.Now;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_contract>()
                    .Where(e => e.type == obj.type).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = new film_contract();
                    objUpdate.Id = 0;
                    objUpdate.film_id = obj.film_id;
                    objUpdate.number = obj.number;
                    objUpdate.appendix = obj.appendix;
                    objUpdate.censorship = obj.censorship;
                    objUpdate.copyright = obj.copyright;
                    objUpdate.date_sign = obj.date_sign;
                    objUpdate.date_expired = obj.date_expired;
                    objUpdate.partner = obj.partner;
                    objUpdate.file = obj.file;
                    objUpdate.type = obj.type;
                    objUpdate.date_created = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        internal int UpdateOrInsert_API(film_contract obj)
        {
            int rs = 0;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_contract>().Where(e => e.type == obj.type && e.film_id == obj.film_id);
                var objUpdate = db.Select(query).LastOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.Id = objUpdate.Id;
                    objUpdate.film_id = obj.film_id;
                    objUpdate.number = obj.number;
                    objUpdate.appendix = obj.appendix;
                    objUpdate.censorship = obj.censorship;
                    objUpdate.copyright = obj.copyright;
                    objUpdate.date_sign = obj.date_sign;
                    objUpdate.date_expired = obj.date_expired;
                    objUpdate.partner = obj.partner;
                    objUpdate.file = obj.file;
                    objUpdate.type = obj.type;
                    objUpdate.date_created = obj.date_created != null ? obj.date_created : DateTime.Now;
                    rs = db.Update(objUpdate);
                }
                else
                {
                    var queryCount = db.From<film_contract>()
                    .Where(e => e.type == obj.type && e.film_id == obj.film_id).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    objUpdate = new film_contract
                    {
                        Id = 0,
                        film_id = obj.film_id,
                        number = obj.number,
                        appendix = obj.appendix,
                        censorship = obj.censorship,
                        copyright = obj.copyright,
                        date_sign = obj.date_sign,
                        date_expired = obj.date_expired,
                        partner = obj.partner,
                        file = obj.file,
                        type = obj.type,
                        date_created = DateTime.Now
                    };
                    rs = (int)db.Insert(objUpdate, selectIdentity: true);
                }
                return rs;
            }
        }

        public List<film_contract> GetByFilmId(int film_id)
        {
            List<film_contract> data = new List<film_contract>();
            if (film_id > 0)
            {
                using (var db = _connectionData.OpenDbConnection())
                {
                    data = db.Select(db.From<film_contract>().Where(e => e.film_id == film_id)).ToList();
                }
            }
            return data;
        }
    }
}