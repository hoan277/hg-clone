﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_bannerService : LamltService
    {

        public film_bannerService()
        {

        }
        public List<film_banner> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_banner>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /**/


                List<film_banner> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public List<vw_film_banner> Get_Vw_Film_Banners(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.arr_display == "null") { page.arr_display = ""; }
            if (page.arr_catalogId == "null") { page.arr_catalogId = ""; }
            if (page.arr_typeClient == "null") { page.arr_typeClient = ""; }
            if (page.arr_search_date == "null") { page.arr_search_date = ""; }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                // display
                if (!string.IsNullOrEmpty(page.arr_display))
                {
                    string[] arrdisplay = page.arr_display.Split(",");
                    query = query.Where(e => Sql.In(e.display.ToString(), arrdisplay));
                }
                // categoryId
                if (!string.IsNullOrEmpty(page.arr_catalogId))
                {
                    string[] arrcatalog = page.arr_catalogId.Split(",");
                    query = query.Where(e => Sql.In(e.film_catalog_id.ToString(), arrcatalog));
                }
                // typeClient
                if (!string.IsNullOrEmpty(page.arr_typeClient))
                {
                    string[] arrtypeClient = page.arr_typeClient.Split(",");
                    query = query.Where(e => Sql.In(e.type_client.ToString(), arrtypeClient));
                }
                query = query.Where(e => e.title.Contains(page.search) || e.html.Contains(page.search)).Skip(offset).Take(limit);
                List<vw_film_banner> rows = db.Select(query).ToList();
                return rows;
            }
        }



        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.arr_display == "null") { page.arr_display = ""; }
            if (page.arr_catalogId == "null") { page.arr_catalogId = ""; }
            if (page.arr_typeClient == "null") { page.arr_typeClient = ""; }
            if (page.arr_search_date == "null") { page.arr_search_date = ""; }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                // display
                if (!string.IsNullOrEmpty(page.arr_display))
                {
                    string[] arrdisplay = page.arr_display.Split(",");
                    query = query.Where(e => Sql.In(e.display.ToString(), arrdisplay));
                }
                // categoryId
                if (!string.IsNullOrEmpty(page.arr_catalogId))
                {
                    string[] arrcatalog = page.arr_catalogId.Split(",");
                    query = query.Where(e => Sql.In(e.film_catalog_id.ToString(), arrcatalog));
                }
                // typeClient
                if (!string.IsNullOrEmpty(page.arr_typeClient))
                {
                    string[] arrtypeClient = page.arr_typeClient.Split(",");
                    query = query.Where(e => Sql.In(e.type_client.ToString(), arrtypeClient));
                }
                query = query.Where(e => e.title.Contains(page.search) || e.html.Contains(page.search)).Skip(offset).Take(limit);
                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }
        public film_banner GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_banner>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_banner GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_banner>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_banner obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_banner>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.position = obj.position;
                        objUpdate.prior = obj.prior;
                        objUpdate.file = obj.file;
                        //objUpdate.file = "http://image.lalatv.com.vn/" + obj.file;
                        objUpdate.html = obj.html;
                        objUpdate.typeid = obj.typeid;
                        objUpdate.desc = obj.desc;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.type_client = obj.type_client;
                        objUpdate.show_home = obj.show_home;
                        objUpdate.display = obj.display;
                        if (obj.film_id != -1)
                        {
                            objUpdate.desc = obj.desc;
                            objUpdate.film_id = obj.film_id;
                        }
                        else
                        {
                            objUpdate.film_id = -1;
                            objUpdate.desc = "";
                        }
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.typeid = obj.typeid;
                    objUpdate.title = obj.title;
                    objUpdate.position = obj.position;
                    objUpdate.prior = obj.prior;
                    objUpdate.file = obj.file;
                    objUpdate.html = obj.html;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.type_client = obj.type_client;
                    objUpdate.show_home = obj.show_home;
                    objUpdate.display = obj.display;
                    if (obj.film_id != -1)
                    {
                        objUpdate.desc = obj.desc;
                        objUpdate.film_id = obj.film_id;
                    }
                    else
                    {
                        objUpdate.film_id = -1;
                        objUpdate.desc = "";
                    }
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_banner>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_banner InitEmpty()
        {
            var obj = new film_banner();
            obj.Id = 0;
            obj.file = "";
            return obj;
        }

    }
}