﻿using DocumentFormat.OpenXml.Wordprocessing;
using lamlt.data;
using lamlt.web.Models;
using Microsoft.AspNetCore.Razor.Language.Extensions;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_product_processing_videoService : LamltService
    {

        public film_product_processing_videoService()
        {

        }
        public List<film_product_processing_video> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing_video>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.product_id.ToString().Contains(page.search)));
                query = query.Skip(offset).Take(limit);
                List<film_product_processing_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public List<vw_film_product_processing_video> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product_processing_video>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.product_id.ToString().Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Skip(offset).Take(limit);
                List<vw_film_product_processing_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing_video>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.product_id.ToString().Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product_processing_video>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.product_id.ToString().Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }
        public film_product_processing_video GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_product_processing_video GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product_processing_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_product_processing_video obj)
        {
            film_video_processingService film_video_processingSV = new film_video_processingService();
            film_video_processing video = film_video_processingSV.GetByID(obj.video_processing_id.ToString());
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_product_processing_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.product_id = obj.product_id;
                        objUpdate.video_processing_id = obj.video_processing_id;
                        objUpdate.catalog_id = video.catalog_id;
                        objUpdate.status = obj.status;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.product_id = obj.product_id;
                    objUpdate.video_processing_id = obj.video_processing_id;
                    objUpdate.catalog_id = video.catalog_id;
                    objUpdate.status = obj.status;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int UpdateStatus(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing_video>().Where(e => e.Id == Id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = 1;
                    int a = db.Update(objUpdate);
                    if (a > 0) return objUpdate.Id;
                }
                return -1;
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing_video>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_product_processing_video InitEmpty()
        {
            var obj = new film_product_processing_video();
            obj.Id = 0;
            return obj;
        }
        public film_product_processing_video InitEmptyByProductId(int productid)
        {
            var obj = new film_product_processing_video();
            obj.Id = 0;
            obj.product_id = productid;
            return obj;
        }

        // for product edit
        public List<vw_film_product_processing_video> GetViewAllItemByProductId(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product_processing_video>();
                query.OrderBy(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.product_id == page.product_id).Skip(0);
                List<vw_film_product_processing_video> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public long CountViewAllItemByProductId(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product_processing_video>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.product_id == page.product_id).Skip(0);
                return db.Count(query);
            }
        }

        public int DeleteByProductId(int product_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing_video>().Where(e => e.product_id == product_id);
                List<film_product_processing_video> list = db.Select(query);
                foreach(var item in list)
                {
                    db.Delete(item);
                }
                return 1;
            }
        }
        public int GenVideoProcessingRandom_Lamlt(PagingModel page)
        {
            // goi den cac service
            film_product_processingService film_product_processingSV = new film_product_processingService();
            film_productService film_productSV = new film_productService();
            film_video_processingService film_video_processingSV = new film_video_processingService();
            List<vw_film_product_processing> list_product_processing = film_product_processingSV.GetViewAllItemByProductId(page);
            vw_film_product product = film_productSV.GetViewById(page.product_id);
            // duyet cac pro_processing de lay ra nhung catalog co cung product
            film_product_processing_video product_Procesing_Video = new film_product_processing_video();
            foreach (var item in list_product_processing) //danh sach pro_processing
            { //1 2 3
                int catalog_id = item.catalog_id.Value;
                string catalog_title = item.catalog_title;
                int catalog_time = item.catalog_time.Value; //600
                int catalog_posision = item.catalog_position.Value;
                List<vw_film_video_processing> list_video = film_video_processingSV.GetViewAllItemByCatalogIdRanDom(null, item.catalog_id.Value);
                foreach (var item1 in list_video)
                {
                    product_Procesing_Video.video_processing_id = item1.Id;
                    product_Procesing_Video.product_id = product.Id;
                    product_Procesing_Video.catalog_id = catalog_id;
                    UpdateOrInsert(product_Procesing_Video);
                }
            }
            return 1;
        }
        public int GenVideoProcessingRandom(PagingModel page)
        {
            // goi den cac service
            film_product_processingService film_product_processingSV = new film_product_processingService();
            film_productService film_productSV = new film_productService();
            film_video_processingService film_video_processingSV = new film_video_processingService();
            List<vw_film_product_processing> list_product_processing = film_product_processingSV.GetViewAllItemByProductId(page);
            vw_film_product product = film_productSV.GetViewById(page.product_id);
            // duyet cac pro_processing de lay ra nhung catalog co cung product
            film_product_processing_video product_Procesing_Video = new film_product_processing_video();
            //duyệt qua các folder của sản phẩm này
            foreach (var item in list_product_processing) //danh sach pro_processing
            { 
                //1 2 3
                int catalog_id = item.catalog_id.Value;
                string catalog_title = item.catalog_title;
                int catalog_time = item.catalog_time.Value; //600
                int catalog_posision = item.catalog_position.Value;
                //lấy danh sách các file video theo từng folder
                List<vw_film_video_processing> list_video1= film_video_processingSV.GetViewAllItemByCatalogId(null, item.catalog_id.Value);
                List<vw_film_video_processing> list_video=getRandomByTotalTime(list_video1, catalog_time);
                foreach (var item1 in list_video)
                {
                    product_Procesing_Video.video_processing_id = item1.Id;
                    //product_Procesing_Video.product_id = product.Id;
                    product_Procesing_Video.product_id = page.product_id;
                    product_Procesing_Video.catalog_id = catalog_id;
                    UpdateOrInsert(product_Procesing_Video);
                }
            }
            return 1;
        }

        private List<vw_film_video_processing> getRandomByTotalTime(List<vw_film_video_processing> list_video1, int catalog_time)
        {
            List<vw_film_video_processing> list_videoResult = new List<vw_film_video_processing>();
            TimeSpan totalSpan = new TimeSpan();
            totalSpan = TimeSpan.FromMinutes(catalog_time);// chuyển đổi thành phút
            TimeSpan currentTimeSpan = TimeSpan.FromMinutes(0);

            list_video1.OrderBy(e => System.Guid.NewGuid().ToString());
            for (int jx = 0; jx <= list_video1.Count - 1; jx++)
            {
                list_videoResult.Add(list_video1[jx]);
                int total_seconds = list_video1[jx].duration ?? 0;
                currentTimeSpan=currentTimeSpan.Add(TimeSpan.FromSeconds(total_seconds));
                if (totalSpan.CompareTo(currentTimeSpan) < 0) break;                
            }
            return list_videoResult;
        }

        #region last_random
        // lay ra tong thoi gian
        public int GetTotalDurationByProductId(PagingModel page)
        {
            int total_duration = 0;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product_processing_video>().Where(e => e.product_id == page.product_id);
                List<vw_film_product_processing_video> list = db.Select(query);
                foreach (var item in list)
                {
                    total_duration = total_duration + item.video_processing_duration.Value;
                }
                return total_duration;
            }
        }
        public int GenVideoProcessingRandom1(PagingModel page)
        {
            // goi den cac service
            film_productService film_productSV = new film_productService(); // product service
            film_product_processingService film_product_processingSV = new film_product_processingService(); // product_processing service
            film_video_processingService film_video_processingSV = new film_video_processingService(); // film_video_processing service
            List<vw_film_product_processing> list_product_processing = film_product_processingSV.GetViewAllItemByProductId(page); // product_procesisng service
            vw_film_product product = film_productSV.GetViewById(page.product_id); // product



            foreach (var product_processing in list_product_processing) //danh sach pro_processing
            { 
                int catalog_id = product_processing.catalog_id.Value; // cata id
                string catalog_title = product_processing.catalog_title; // cata title
                int catalog_time = product_processing.catalog_time.Value; // cata duration 600
                int catalog_posision = product_processing.catalog_position.Value; // cata possiton
                vw_film_video_processing vw_video = new vw_film_video_processing();
                List<vw_film_video_processing> list_video = film_video_processingSV.GetViewAllItemByCatalogId(page, catalog_id);
                var random = new Random();
                //while (catalog_time>0)
                //{
                //    vw_video = film_video_processingSV.GetViewItemByCatalogIdRanDom(page, catalog_id);
                //    if (list_video.Count > 0)
                //    {
                //        foreach(var current_video in list_video)
                //        {
                //            if(vw_video.Id!= current_video.Id)
                //            {
                //                catalog_time = catalog_time - vw_video.duration.Value;
                //                list_video.Add(vw_video);
                //            }
                //        }
                //    }
                //    else
                //    {
                //        catalog_time = catalog_time - vw_video.duration.Value;
                //        list_video.Add(vw_video);
                //    }
                //}
                //foreach(var item111 in list_video)
                //{

                //}
            }
            return 1;
        }
        #endregion last_random
    }
}