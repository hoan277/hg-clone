﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class user_linkService : LamltService
    {

        public user_linkService()
        {

        }
        public List<user_link> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/


                List<user_link> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_user_link> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/


                List<vw_user_link> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_user_link>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(name == "-1") ||  string.IsNullOrEmpty(name)) query=query.Where(e=>e.name == page.name);
*/

                return db.Count(query);
            }
        }
        public user_link GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_user_link GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_user_link>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(user_link obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<user_link>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.userid = obj.userid;
                        objUpdate.name = obj.name;
                        objUpdate.typekid = obj.typekid;
                        objUpdate.datecreated = obj.datecreated;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<user_link>()
                        .Where(e => e.name == obj.name).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.userid = obj.userid;
                    objUpdate.name = obj.name;
                    objUpdate.typekid = obj.typekid;
                    objUpdate.datecreated = obj.datecreated;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<user_link>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public user_link InitEmpty()
        {
            var obj = new user_link();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_user_link
		as
			select user_link.Id,user_link.userid,user_link.name,user_link.typekid,user_link.datecreated  from user_link  
		*/
    }
}