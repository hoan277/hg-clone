﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_media_streamService : LamltService
    {

        public film_media_streamService()
        {

        }
        public List<film_media_stream> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == -1) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_media_stream>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                List<film_media_stream> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<film_media_stream> GetAllItemForCmd(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == -1) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_media_stream>();
                query.OrderBy(x => x.stt);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                List<film_media_stream> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public string getResult(film_media_stream data)
        {
            string dataCode = data.code.ToString();
            var framerate = (!data.framerate.HasValue) ? "" : ("-r " + data.framerate + " ");
            var audiobitrate = string.IsNullOrEmpty(data.audiobitrate) ? "" : ("-b:a " + data.audiobitrate + " ");
            var fbs = string.IsNullOrEmpty(data.fbs) ? "" : ("-s " + data.fbs + " ");
            var bitrate = string.IsNullOrEmpty(data.bitrate) ? "" : ("-b:v " + data.bitrate + " ");
            /*
            if (data.framerate == null)
            {
                framerate = null;
            }
            if (data.audiobitrate == null) { audiobitrate = null; }
            if (data.fbs == null) { fbs = null; }
            if (data.bitrate == null) { bitrate = null; }
            */
            string dataResult = framerate + audiobitrate + fbs + bitrate + data.other;
            //object dataCode = dataResult;
            return dataResult;
        }
        // forcus
        public List<vw_film_media_stream> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_media_stream>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e =>/* e.name.Contains(page.search) ||*/ e.code.ToString().Contains(page.search));
                if (page.status != -1)
                {
                    if (page.status == 0)
                    {
                        query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                    }
                }
                else
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                List<vw_film_media_stream> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<film_media_stream>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.name.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_media_stream>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                return db.Count(query);
            }
        }





        public film_media_stream GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_media_stream>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_media_stream GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_media_stream>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_media_stream>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(film_media_stream obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_media_stream>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.stt = obj.stt;
                        objUpdate.Id = obj.Id;
                        objUpdate.code = obj.code;
                        objUpdate.name = obj.name;
                        objUpdate.bitrate = obj.bitrate;
                        objUpdate.audiobitrate = obj.audiobitrate;
                        objUpdate.deinterface = obj.deinterface;
                        objUpdate.fbs = obj.fbs;
                        objUpdate.framerate = obj.framerate;
                        objUpdate.input_dir = obj.input_dir;
                        objUpdate.input_origin = obj.input_origin;
                        objUpdate.outputFolder = obj.outputFolder;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.other = obj.other;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_media_stream>()
                        .Where(e => e.name == obj.name).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.stt = obj.stt;
                    objUpdate.code = obj.code;
                    objUpdate.name = obj.name;
                    objUpdate.bitrate = obj.bitrate;
                    objUpdate.audiobitrate = obj.audiobitrate;
                    objUpdate.deinterface = obj.deinterface;
                    objUpdate.fbs = obj.fbs;
                    objUpdate.framerate = obj.framerate;
                    objUpdate.input_dir = obj.input_dir;
                    objUpdate.input_origin = obj.input_origin;
                    objUpdate.outputFolder = obj.outputFolder;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.other = obj.other;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_media_stream>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_media_stream InitEmpty()
        {
            var obj = new film_media_stream();
            obj.Id = 0;
            return obj;
        }
        public film_media_stream GetByCode(string code)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_media_stream>().Where(e => e.code == code);
                return db.Select(query).SingleOrDefault();
            }
        }
        public List<film_media_stream> GetListByCode(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == -1) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_media_stream>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                List<film_media_stream> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

    }
}