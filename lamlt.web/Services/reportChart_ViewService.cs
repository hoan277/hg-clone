﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class reportChart_ViewService : LamltService
    {

        public reportChart_ViewService()
        {

        }
        public List<package> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                query = query.Where(e => (e.title.Contains(page.search)));
                List<package> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_report_view>();
                if (comm.IsCP()) query = query.Where(e => e.video_id == comm.GetCPId());

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.film_title.Contains(page.search)));
                if (page.cpid != -1) query = query.Where(e => e.video_id == page.cpid);
                return db.Count(query);
            }
        }
        public film_video GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_report_view GetViewByID(int videoid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_report_view>().Where(e => e.video_id == videoid);
                var f = db.Select(query).SingleOrDefault();
                return f;
            }
        }

        public List<vw_report_view> GetListVideoByDate(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }

            if (page.cpid == -1) { }

            using (var db = _connectionData.OpenDbConnection())
            {

                var query = db.From<vw_report_view>();
                if (comm.IsCP()) query = query.Where(e => e.video_id == comm.GetCPId());
                query.GroupBy(x => x.video_id).OrderByDescending(x => x.video_id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.film_title.Contains(page.search));

                List<vw_report_view> rows = db.Select(query)
                    .Skip(offset).Take(100).ToList();
                return rows;
            }
        }

        public long CountViewOfVideoByDate(int video_id, PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.cpid == -1) { }

            using (var db = _connectionData.OpenDbConnection())
            {

                var query = db.From<vw_report_view>().Where(e => e.video_id == video_id);
                query.GroupBy(x => x.video_id).OrderByDescending(x => x.video_id);
                return db.Count(query);
            }
        }
        // hàm lấy package
        public List<package> GetPackages(int package_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                if (comm.IsCP()) query = query.Where(e => e.Id == package_id);
                query.OrderByDescending(x => x.Id);

                List<package> rows = db.Select(query).ToList();
                return rows;
            }
        }
        // hàm tính tổng doanh thu
        public long TongDoanhthu(int package_id, int video_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // DTCP = Dti + DTii
                // DTi = pi * qi *30% , DTii = (SLSB/SLSALL)* DTGX * 30%
                // pila 
                List<package> pi = new List<package>();
                foreach (var item in pi)
                {

                }
                return 1;
            }
        }
        public List<film_video> GetAllItemFilmvideo(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                if (comm.IsCP()) query = query.Where(e => e.Id == page.cpid);
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                query = query.Where(e => (e.title.Contains(page.search)));
                List<film_video> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
    }
}