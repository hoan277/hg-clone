﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_renderService : LamltService
    {
        public film_renderService()
        {
        }

        public List<film_render> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_render>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                List<film_render> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public List<vw_film_render> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_render>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                List<vw_film_render> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public Dictionary<object, object> get(int id, string title, string status)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_render>();
                query.OrderByDescending(x => x.Id);
                if (id > 0) { query = query.Where(e => e.Id == id); }
                if (!string.IsNullOrEmpty(title)) { query = query.Where(e => e.title == title); }
                if (!string.IsNullOrEmpty(status)) { query = query.Where(e => e.status == status); }
                return new Dictionary<object, object>
                {
                    { "data", db.Select(query).ToList() }
                }; ;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_render>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_render>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }

        public film_render GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_render>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_render GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_render>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_render obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_render>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = string.IsNullOrEmpty(obj.title) ? objUpdate.title : obj.title;
                        objUpdate.list_file = obj.list_file;
                        objUpdate.effect = obj.effect;
                        objUpdate.thumb = obj.thumb;
                        objUpdate.channel_id = obj.channel_id;
                        objUpdate.yt_title = obj.yt_title;
                        objUpdate.yt_desc = obj.yt_desc;
                        objUpdate.yt_mode = obj.yt_mode;
                        objUpdate.yt_schedule = obj.yt_schedule;
                        objUpdate.status = obj.status;
                        objUpdate.date_created = obj.date_created;
                        objUpdate.date_update = DateTime.Now;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_render>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.list_file = obj.list_file;
                    objUpdate.effect = obj.effect;
                    objUpdate.thumb = obj.thumb;
                    objUpdate.channel_id = obj.channel_id;
                    objUpdate.yt_title = obj.yt_title;
                    objUpdate.yt_desc = obj.yt_desc;
                    objUpdate.yt_mode = obj.yt_mode;
                    objUpdate.yt_schedule = obj.yt_schedule;
                    objUpdate.status = obj.status;
                    objUpdate.date_created = obj.date_created;
                    objUpdate.date_update = DateTime.Now;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_render>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_render InitEmpty()
        {
            var obj = new film_render();
            obj.Id = 0;
            return obj;
        }

        /*
		genvie here
		create view vw_film_render
		as
			select film_render.Id,film_render.title,film_render.list_file,film_render.effect,film_render.thumb,film_render.channel_id,film_render.yt_title,film_render.yt_desc,film_render.yt_mode,film_render.yt_schedule,film_render.status,film_render.date_created,film_render.date_update  from film_render
		*/
    }
}