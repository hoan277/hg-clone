﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace lamlt.web.Services
{
    public class film_cmd_convertingService : LamltService
    {

        public film_cmd_convertingService()
        {

        }
        public List<film_cmd_converting> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cmd_converting>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<film_cmd_converting> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_cmd_converting> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cmd_converting>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.idconverting.Contains(page.search)));
                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_film_cmd_converting> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        //public int WriteDataToText(string[] idconverting, string[] listVideo)
        //{
        //    string[] listVideo ={
        //                    "Sample Line 1",
        //                    "Sample Line 2",
        //                    "Sample Line 3",
        //                    "Sample Line 4",
        //                    "Sample Line 5"
        //                  };
        //    string[] myArray =
        //        {
        //        "asdtung",
        //        "asdd",
        //        "dds"
        //    };
        //    for (int i = 0; i < listVideo.Length; i++)
        //    {
        //        string path = @"F:\New folder\" + listVideo[i] + ".txt";
        //        //if (!File.Exists(path))
        //        //{
        //        // Create a file to write to.
        //        using (StreamWriter sw = File.CreateText(path))
        //        {
        //            for (int x = 0; x < myArray.Length; x++)
        //            {
        //                sw.WriteLine(myArray[x]);
        //            }
        //            //sw.WriteLine("Hello");
        //            //sw.WriteLine("And");
        //            //sw.WriteLine("Welcome");
        //            //}
        //        }
        //    }

        //}

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cmd_converting>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /*
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public film_cmd_converting GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cmd_converting>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_cmd_converting GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_cmd_converting>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(film_cmd_converting obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_cmd_converting>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.idconverting = obj.idconverting;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.status = obj.status;
                        objUpdate.userid = obj.userid;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {

                    var objUpdate = InitEmpty();
                    objUpdate.Id = obj.Id;
                    objUpdate.idconverting = obj.idconverting;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.status = obj.status;
                    objUpdate.userid = obj.userid;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cmd_converting>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_cmd_converting InitEmpty()
        {
            var obj = new film_cmd_converting();
            obj.Id = 0;
            return obj;
        }
        public void updateTransactionObj(film_transactions trans)
        {
            new film_transactionsService().UpdateOrInsert(trans);
        }
        private film_cmd_converting GetItemById(int film_cmd_convertingid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cmd_converting>().Where(e => e.Id == film_cmd_convertingid);
                film_cmd_converting rows = db.Select(query).FirstOrDefault();
                return rows;
            }
        }
        public List<film_cmd_converting> Getfilm_cmd_convertings()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cmd_converting>();
                return db.Select(query); ;
            }
        }
        public List<film_cmd_converting> FilmId(int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_cmd_converting>().Where(e => e.userid == filmid);
                List<film_cmd_converting> row = db.Select(query);
                return row;
            }
        }

        private static readonly string[] VietNamChar = new string[]
        {
            "aAeEoOuUiIdDyY",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
        };
        public string LocDau(string str)
        {
            //Thay thế và lọc dấu từng char
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            return str;
        }

        public List<film_video_processing> GetFilm_Videos(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 1000 };
            if (page.search == null) page.search = "";

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)) || e.upload_file.Contains(page.search));

                List<film_video_processing> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAllVideo(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 100;
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                return db.Count(query);
            }
        }
        public List<KeyValuePair<string, string>> GetLiveStreaming()
        {
            var list = new List<KeyValuePair<string, string>>();
            foreach (var item in Directory.GetFiles("/home/amnhacsaigon/livestream/running", "*.txt"))
            {
                var current = new LiveStream();
                var current_item = item.Replace("/home/amnhacsaigon/livestream/running/", "");
                var current_item_noext = item.Replace("/home/amnhacsaigon/livestream/running/", "").Replace(".mp4.txt", "").Replace(".mov.txt", "").Replace(".MP4.txt", "");
                LogService.logItem("current_item_noext=" + current_item_noext);
                foreach (var item_process in Directory.GetFiles("/home/amnhacsaigon/livestream/running/", "*.process"))
                {
                    var current_process = item_process.Replace("/home/amnhacsaigon/livestream/running/", "");
                    var current_process_noext = item_process.Replace("/home/amnhacsaigon/livestream/running/", "").Replace(".process", "");
                    LogService.logItem("current_process_noext=" + current_process_noext);
                    if (current_item_noext == current_process_noext)
                    {
                        list.Add(new KeyValuePair<string, string>(current_item, File.ReadAllText(item_process)));
                        LogService.logItem("list=" + list);
                        LogService.logItem("ReadAllText=" + File.ReadAllText(item_process));
                    }
                }
            }
            return list;
        }
        public int KillProcessLive(string file_txt, string file_process, string process_id)
        {
            string fileTxt = "/home/amnhacsaigon/livestream/running/" + file_txt;
            string fileProcess = "/home/amnhacsaigon/livestream/running/" + file_process;
            if (File.Exists(fileTxt) && File.Exists(fileProcess))
            {
                File.Delete(fileTxt);
                File.Delete(fileProcess);
                using (StreamWriter sw = new StreamWriter("/home/amnhacsaigon/livestream/list.kill", false, Encoding.UTF8))
                {
                    sw.Write(process_id);// ghi lệnh ra file list.kill để python tự đọc lệnh và chạy lệnh tắt tiến trình
                    sw.Close();
                }
                return 1;
            }
            return 0;
        }
    }
    public class LiveStream
    {
        public string link;
        public string process;
        public string mp4;

    }
}
