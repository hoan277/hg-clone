﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class denoiserService : LamltService
    {

        public denoiserService()
        {

        }
        public List<denoiser> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<denoiser>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
+ " 
   if(!(mp4 == "-1") ||  string.IsNullOrEmpty(mp4)) query=query.Where(e=>e.mp4 == page.mp4);
*/


                List<denoiser> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_denoiser> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_denoiser>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
+ " 
   if(!(mp4 == "-1") ||  string.IsNullOrEmpty(mp4)) query=query.Where(e=>e.mp4 == page.mp4);
*/


                List<vw_denoiser> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<denoiser>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
+ " 
   if(!(mp4 == "-1") ||  string.IsNullOrEmpty(mp4)) query=query.Where(e=>e.mp4 == page.mp4);
*/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_denoiser>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
+ " 
   if(!(mp4 == "-1") ||  string.IsNullOrEmpty(mp4)) query=query.Where(e=>e.mp4 == page.mp4);
*/

                return db.Count(query);
            }
        }

        internal object GetInfo(int id, string title)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<denoiser>();
                if (id > 0) query = query.Where(e => e.Id == id);
                if (!string.IsNullOrEmpty(title)) query = query.Where(e => e.title == title || e.title.Contains(title));
                return db.Select(query).ToList();
            }
        }

        public denoiser GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<denoiser>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_denoiser GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_denoiser>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(denoiser obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<denoiser>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.mp4 = obj.mp4;
                        objUpdate.step = obj.step;
                        objUpdate.result = obj.result;
                        objUpdate.created = obj.created;
                        objUpdate.updated = obj.updated;
                        objUpdate.status = obj.status;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<denoiser>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.mp4 = obj.mp4;
                    objUpdate.step = obj.step;
                    objUpdate.result = obj.result;
                    objUpdate.created = DateTime.Now;
                    objUpdate.updated = DateTime.Now;
                    objUpdate.status = obj.status;

                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<denoiser>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public denoiser InitEmpty()
        {
            var obj = new denoiser();
            obj.Id = 0;
            return obj;
        }
        public int UpdateStatus(int Id, string status)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<denoiser>().Where(e => e.Id == Id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = status;
                    int a = db.Update(objUpdate);
                    if (a > 0) return objUpdate.Id;
                }
                return -1;
            }
        }
        /*
		genvie here
		create view vw_denoiser
		as
			select denoiser.Id,denoiser.title,denoiser.mp4,denoiser.step,denoiser.result,denoiser.created,denoiser.updated,denoiser.status  from denoiser  
		*/
    }
}