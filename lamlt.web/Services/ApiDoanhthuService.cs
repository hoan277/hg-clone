﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.Legacy;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace lamlt.web.Services1
{
    public class ApiDoanhthuService : LamltService1
    {
        //select db
        public List<mvm_bao_cao_doanhthu_day> GetViewAllItem_Range(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (string.IsNullOrEmpty(page.start_time))
            {
                { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            }

            if (string.IsNullOrEmpty(page.end_time))
            {
                { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            }
            using (var db = _connectionData1.OpenDbConnection())
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                DateTime date = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", provider);
                DateTime date1 = DateTime.ParseExact(page.end_time, "yyyy-MM-dd", provider);
                var query = db.From<mvm_bao_cao_doanhthu_day>();
                query.OrderByDescending(x => x.Id);
                // if (comm.IsCP()) query = query.Where(e => e.Id == comm.GetCPId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;

                try { limit = page.limit; }
                catch { }
                //query = query.Where(x => (x.datecreated >= date && x.datecreated <= date1));
                if ((!string.IsNullOrEmpty(page.start_time)) && (!string.IsNullOrEmpty(page.end_time)))
                {
                    query.Where(e => (e.datecreated >= date && e.datecreated <= date1));
                }
                // query = query.Where(e => e.Id > 0 );
                // query = query.Where(e => (e.Id.Contains(page.search)));
                //if (!String.IsNullOrEmpty(page.start_time))
                //{
                //   
                //}
                //sub_type

                List<mvm_bao_cao_doanhthu_day> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;

            }
        }
        public List<mvm_bao_cao_doanhthu_day> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            CultureInfo provider = CultureInfo.InvariantCulture;

            DateTime dtStart = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", provider);
            DateTime dtEnd = DateTime.ParseExact(page.end_time, "yyyy-MM-dd", provider);

            DateTime dtTmp = dtStart;
            List<string> lst = new List<string>();

            while (dtTmp.CompareTo(dtEnd) <= 0)
            {
                lst.Add(dtTmp.ToString("yyyy-MM-dd"));
                dtTmp = dtTmp.AddDays(1);
            }

            ApiDoanhthuService svrDoanhthu = (new ApiDoanhthuService());
            List<mvm_bao_cao_doanhthu_day> lst_result = new List<mvm_bao_cao_doanhthu_day>();

            //using (var db = _connectionData1.OpenDbConnection())
            {
                foreach (string s in lst)
                {
                    int result = new ApiDoanhthuService().UpdateOrInsert(s);
                    List<mvm_bao_cao_doanhthu_day> data = svrDoanhthu.GetViewAllItem_Range(new PagingModel() { offset = 0, limit = 50, search = "", end_time = s, start_time = s });
                    lst_result.AddRange(data);
                }


                return lst_result;
            }

        }
        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData1.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time, "yyyy-MM-dd", null);
                var query = db.From<mvm_bao_cao_doanhthu_day>();
                if (comm.IsCP()) query = query.Where(e => e.Id == comm.GetCPId());

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }

                // query = query.Where(e => (e.Id.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                //sub_type

                return db.Count(query);
            }
        }


        /// <summary>
        ///tinh tong so phone active
        /// </summary>
        /// <returns></returns>
        public long CountPhoneActive(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                //string time1 = DateTime.Now.ToString("yyyy-MM-dd");

                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                //DateTime dt1 = DateTime.Today.AddDays(-1);
                //DateTime dt1 = DateTime.ParseExact("2019-11-04", "yyyy-MM-dd", null); ;
                var state1 = 1;
                var query = db.From<mvm_user_subscription_logs>().Where(e => e.state == state1 && e.created_at >= dt1 && e.created_at <= dt2);
                return db.Count(query);
            }
        }
        /// <summary>
        /// tính tổng số phone đăng ký
        /// </summary>
        /// <returns></returns>
        public long countphone(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                // DateTime dt1 = DateTime.Today.AddDays(-1);
                //DateTime dt1 = DateTime.ParseExact("2019-11-04", "yyyy-MM-dd", null); ;
                var type1 = 1;
                var query = db.From<mvm_user_subscription_logs>().Where(e => e.type == type1 && e.created_at >= dt1 && e.created_at <= dt2);
                return db.Count(query);
            }
        }
        /// <summary>
        /// tổng tb hủy
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public long phonecancel(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                // DateTime dt1 = DateTime.Today.AddDays(-1);
                //DateTime dt1 = DateTime.ParseExact("2019-11-04", "yyyy-MM-dd", null); ;
                var type1 = 2;
                var query = db.From<mvm_user_subscription_logs>().Where(e => e.type == type1 && e.created_at >= dt1 && e.created_at <= dt2);

                return db.Count(query);
            }
        }
        /*
         * select action_date, package_code,msisdn,`channel`,reason,price,original_price from vascloud_cdr where action_date >= "2020-02-01" and action_date <= "2020-02-29"
         */
        internal List<vascloud_cdr> GetReportVnpt(string start = "2020-01-01", string end = "2020-01-30")
        {
            if (start == null) start = "2020-01-01";
            if (end == null) end = "2020-01-30";
            LogService.logItem(start);
            LogService.logItem(end);
            using (var db = _connectionData1.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(start, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(end + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vascloud_cdr>().Where(e => e.action_date >= dt1 && e.action_date <= dt2);
                List<vascloud_cdr> list = db.Select(query);
                LogService.logItem(list);
                return list;
            }
        }

        /// <summary>
        /// tổng thuê bao gia hạn
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public long giahan(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                //DateTime dt1 = DateTime.Today.AddDays(-1);
                //DateTime dt1 = DateTime.ParseExact("2019-11-04", "yyyy-MM-dd", null); ;
                var type1 = 3;
                var query = db.From<mvm_user_subscription_logs>().Where(e => e.type == type1 && e.created_at >= dt1 && e.created_at <= dt2);

                return db.Count(query);
            }
        }
        /// <summary>
        /// tính tổng thuê bao đk thành công
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public long countSucceed(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                //DateTime dt1 = DateTime.Today.AddDays(-1);
                // DateTime dt1 = DateTime.ParseExact("2019-11-04", "yyyy-MM-dd", null); ;
                var state1 = 1;
                var query = db.From<mvm_user_subscription_logs>().Where(e => e.type == state1 && e.created_at >= dt1 && e.created_at <= dt2);
                return db.Count(query);
            }
        }
        /// <summary>
        /// tính tổng thuê bao thất bại
        /// </summary>
        /// <returns></returns>
        public long countfailed(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                // DateTime dt1 = DateTime.Today.AddDays(-1);
                //DateTime dt1 = DateTime.ParseExact("2019-11-04", "yyyy-MM-dd", null); ;
                var state1 = 0;
                var query = db.From<mvm_user_subscription_logs>().Where(e => e.type == state1);
                query.Where(x => x.created_at >= dt1 && x.created_at <= dt2);
                return db.Count(query);
            }
        }
        /// <summary>
        /// tính tỉ lệ gia hạn
        /// </summary>
        /// <returns></returns>
        public int GHTC(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                // gia han 1 lấy kết quả gia hạn của ngày hôm trước
                DateTime dt_strart = DateTime.ParseExact(s, "yyyy-MM-dd", null); // lay ngay ban dau
                DateTime dt1 = dt_strart.AddDays(-1);
                string dtt = dt1.ToString("yyyy-MM-dd");
                DateTime dt2 = DateTime.ParseExact(dtt + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var type1 = 3;
                var query = db.From<mvm_user_subscription_logs>().Where(e => e.type == type1 && e.created_at >= dt1 && e.created_at <= dt2);
                long count1 = db.Count(query);
                long count2 = giahan(s);
                long count3 = countSucceed(s);
                //   int c = (int)gh1 / (int)gh2;
                int gh1 = Convert.ToInt32(count1);
                int gh2 = Convert.ToInt32(count2);
                int gh3 = Convert.ToInt32(count3);
                int sohang1 = (gh2 - gh3) * 100;
                int sohang2 = gh1;
                if (gh1 == 0 || gh2 == 0)
                {
                    double c = 0;
                    int result = Convert.ToInt32(c);
                    return result;
                }
                else
                {
                    double c = (sohang1 / sohang2);
                    int result = Convert.ToInt32(c);
                    return result;
                }
                //object giahan1 = giahan1.CompareTo("count(user_mobile)", "type1 ='3' && created_at ='"+ dt1 +"'");
                //Query = giahan()/giahan

            }
        }
        /// <summary>
        /// tính tổng doanh thu
        /// </summary>
        /// <returns></returns>
        public int sum(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                var gia1 = 2000;
                var gia2 = 3000;
                var gia3 = 5000;
                var gia4 = 9000;
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);

                var query = db.From<mvm_user_subscription_logs>().Where(e => e.price == gia1 && e.created_at >= dt1 && e.created_at <= dt2);
                var query1 = db.From<mvm_user_subscription_logs>().Where(e => e.price == gia2 && e.created_at >= dt1 && e.created_at <= dt2);
                var query2 = db.From<mvm_user_subscription_logs>().Where(e => e.price == gia3 && e.created_at >= dt1 && e.created_at <= dt2);
                var query3 = db.From<mvm_user_subscription_logs>().Where(e => e.price == gia4 && e.created_at >= dt1 && e.created_at <= dt2);
                long price1 = db.Count(query); // goi ngay 2000
                long price2 = db.Count(query1); // goi ngay 3000
                long price3 = db.Count(query2); // goi tuan 5000
                long price4 = db.Count(query3); // goi thang 9000
                int sum1 = Convert.ToInt32(price1) * gia1;
                int sum2 = Convert.ToInt32(price2) * gia2;
                int sum3 = Convert.ToInt32(price3) * gia3;
                int sum4 = Convert.ToInt32(price4) * gia4;
                int total = sum1 + sum2 + sum3 + sum4;

                return total;
            }
        }



        public int UpdateOrInsert(string s)
        {
            using (var db = _connectionData1.OpenDbConnection())
            {
                // check dk neu form mvm_user_subcription_logs co du lieu tra ve ngay hom do thi update
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query1 = db.From<mvm_user_subscription_logs>().Where(e => e.created_at >= dt1 && e.created_at <= dt2);
                var dt = db.Count(query1);

                if (dt > 0)
                {
                    //string json = new WebClient().DownloadString("http://115.146.121.190:5003/api/doanhthu/List");
                    //mvm_bao_cao_doanhthu_day items = JsonConvert.DeserializeObject<mvm_bao_cao_doanhthu_day>(json);
                    //Console.WriteLine (items.datecreated);
                    ApiDoanhthuService svrDoanhthu = (new ApiDoanhthuService());
                    DateTime oDate = Convert.ToDateTime(s);
                    var query = db.From<mvm_bao_cao_doanhthu_day>()
                        .Where(e => (e.datecreated == dt1));
                    //.Where(e=>(e.datecreated.Day== dt1.Day) && (e.datecreated.Month == dt1.Month)&&(e.datecreated.Year == dt1.Year));
                    var objUpdate = db.Select(query).FirstOrDefault();

                    if (objUpdate != null)
                    {

                        objUpdate.countphoneactive = (int)svrDoanhthu.CountPhoneActive(s);
                        objUpdate.countphone = (int)svrDoanhthu.countphone(s);
                        objUpdate.countCancel = (int)svrDoanhthu.phonecancel(s);
                        objUpdate.giahan = (int)svrDoanhthu.giahan(s);
                        objUpdate.countSucceed = (int)svrDoanhthu.countSucceed(s);
                        objUpdate.countfailed = (int)svrDoanhthu.countfailed(s);
                        objUpdate.TLGHTC = (int)svrDoanhthu.GHTC(s);
                        objUpdate.totaldoanhthu = (int)svrDoanhthu.sum(s);

                        return (int)db.Update(objUpdate);
                        //return -1;
                    }
                    else
                    {
                        objUpdate = InitEmpty();
                        objUpdate.datecreated = oDate;
                        objUpdate.countphoneactive = (int)svrDoanhthu.CountPhoneActive(s);
                        objUpdate.countphone = (int)svrDoanhthu.countphone(s);
                        objUpdate.countCancel = (int)svrDoanhthu.phonecancel(s);
                        objUpdate.giahan = (int)svrDoanhthu.giahan(s);
                        objUpdate.countSucceed = (int)svrDoanhthu.countSucceed(s);
                        objUpdate.countfailed = (int)svrDoanhthu.countfailed(s);
                        objUpdate.TLGHTC = (int)svrDoanhthu.GHTC(s);
                        objUpdate.totaldoanhthu = (int)svrDoanhthu.sum(s);
                        return (int)db.Insert(objUpdate, selectIdentity: true);
                    }
                }
                else
                {
                    return -1;
                }
            }
        }







        public mvm_bao_cao_doanhthu_day InitEmpty()
        {
            var obj = new mvm_bao_cao_doanhthu_day();
            obj.Id = 0;
            return obj;
        }
        //domain
        //public class doanhthu
        //{
        //    public int id { get; set; }
        //    public int? price { get; set; }
        //    public int? count { get; set; }
        //}
        //public class vw_doanhthu
        //{
        //    public int id { get; set; }
        //    public int? price { get; set; }
        //    public int? count { get; set; }
        //}


    }
}
