﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_processing_transcriptService : LamltService
    {
        public film_video_processing_transcriptService()
        {
        }

        public List<film_video_processing_transcript> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.video_processing_id.ToString().Contains(page.search)));
                query = query.Skip(offset).Take(limit);
                List<film_video_processing_transcript> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_video_processing_transcript> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.video_processing_title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Skip(offset).Take(limit);
                List<vw_film_video_processing_transcript> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.video_processing_id.ToString().Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing_transcript>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.video_processing_title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public film_video_processing_transcript GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing_transcript>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_video_processing_transcript GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing_transcript>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_video_processing_transcript obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video_processing_transcript>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.video_processing_id = obj.video_processing_id;
                        objUpdate.transcript_id = obj.transcript_id > 0 ? obj.transcript_id : objUpdate.transcript_id;
                        objUpdate.status = obj.status;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.isview = obj.isview;
                        objUpdate.file_thanh_pham = obj.file_thanh_pham;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.video_processing_id = obj.video_processing_id;
                    objUpdate.transcript_id = obj.transcript_id;
                    objUpdate.status = obj.status;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.isview = obj.isview;
                    objUpdate.file_thanh_pham = obj.file_thanh_pham;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public Dictionary<object, object> UpdateStatus(int Id, string status)
        {
            var dict = new Dictionary<object, object>();
            string message = "", code = "";

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing_transcript>().Where(e => e.Id == Id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = status;
                    int a = db.Update(objUpdate);
                    code = a > 0 ? "success" : "error";
                    message = a > 0 ? "Cập nhật trạng thái thành công" : "Thất bại";
                }
                dict.Add("code", code);
                dict.Add("message", message);
            }
            return dict;
        }
        public int DelTranscript(int transcript_id)
        {
            try
            {
                using (var db = _connectionData.OpenDbConnection())
                {
                    var query = db.From<film_transcript>().Where(e => e.Id == transcript_id);
                    return db.Delete(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return 1;
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_processing_transcript>().Where(e => e.Id == Id);
                var rs = db.Select(query);
                if (rs.Count > 0)
                {
                    DelTranscript((int)rs.LastOrDefault().transcript_id);
                }
                return db.Delete(query);
            }
        }

        public film_video_processing_transcript InitEmpty()
        {
            var obj = new film_video_processing_transcript();
            obj.Id = 0;
            return obj;
        }

        // for product edit
        public List<vw_film_video_processing_transcript> GetViewAllItemByvideo_processing_id(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing_transcript>();
                query.OrderBy(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.video_processing_id == page.video_processing_id).Skip(0);
                List<vw_film_video_processing_transcript> rows = db.Select(query).ToList();
                return rows;
            }
        }

        internal vw_film_video_processing_transcript GetIDByProcesingID(int processingID, int transcript_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_processing_transcript>();
                query = query.Where(e => e.video_processing_id == processingID && e.transcript_id == transcript_id);
                return db.Select(query).LastOrDefault();
            }
        }

        public long CountViewAllItemByvideo_processing_id(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product_processing_video>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.video_processing_id == page.video_processing_id).Skip(0);
                return db.Count(query);
            }
        }
    }
}