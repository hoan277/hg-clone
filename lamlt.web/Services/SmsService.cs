﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class SmsService : LamltService
    {


        //public class film_user_log
        //{
        //    public string desc;
        //}

        public List<film_users_log> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_users_log>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.cpid);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.desc.Contains(page.search)));
                /**/


                List<film_users_log> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_users_log>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                // query = query.Where(e => (e.desc.Contains(page.search)));
                /**/

                return db.Count(query);
            }
        }

        public int GetAll(string desc, int userid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {

                var query = db.From<film_users_log>().Where(e => e.Id == userid);

                //lặp qua mảng id
                foreach (var descs in desc)
                {
                    var obj = new vw_film_users_log();
                    obj.desc = desc;

                    db.Insert<vw_film_users_log>(obj);
                }
                return -1;
            }
        }
    }
}
