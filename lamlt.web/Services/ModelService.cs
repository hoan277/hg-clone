﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace lamlt.web.Services
{
    public class modelService : LamltService
    {

        public modelService()
        {

        }
        public List<model> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<model>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<model> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_model> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_model>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_model> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<model>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public model GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<model>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_model GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_model>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(model obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<model>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.description = obj.description;
                        objUpdate.decrypt = obj.description;
                        objUpdate.datecreated = DateTime.Now;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<model>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.description = obj.description;
                    objUpdate.decrypt = obj.description;
                    objUpdate.datecreated = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<model>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public string Train(int Id)
        {
            HttpClient http = new HttpClient();
            http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("APIKEY");
            var url = "http://115.146.121.190:5005/recommendation/train?modelid=" + Id;
            string data = http.GetAsync(url).Result.Content.ReadAsStringAsync().Result;
            return data;
        }


        public model InitEmpty()
        {
            var obj = new model();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_model
		as
			select model.Id,model.title,model.desc,model.datecreated,model.userid,model.catalogid   ,model.title as catalogid_title  from model   inner join model on model.catalogid=model.id  
		*/
    }
}