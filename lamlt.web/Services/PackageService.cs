﻿using Golaco.bank;
using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class packageService : LamltService
    {

        public packageService()
        {

        }
        public List<package> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<package> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_package> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_package>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_package> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public package GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_package GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_package>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(package obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<package>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.description = obj.description;
                        objUpdate.price = obj.price;
                        objUpdate.note = obj.note;
                        objUpdate.typepayment = obj.typepayment;
                        objUpdate.code = obj.code;
                        objUpdate.provider_id = obj.provider_id;
                        objUpdate.date_created = DateTime.Now;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<package>()
                        .Where(e => e.title == obj.title).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.description = obj.description;
                    objUpdate.price = obj.price;
                    objUpdate.note = obj.note;
                    objUpdate.code = obj.code;
                    objUpdate.provider_id = obj.provider_id;
                    objUpdate.typepayment = obj.typepayment;

                    objUpdate.date_created = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public package InitEmpty()
        {
            var obj = new package();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_package
		as
			select package.Id,package.title,package.desc,package.datecreated,package.userid,package.catalogid   ,package.title as catalogid_title  from package   inner join package on package.catalogid=package.id  
		*/
        public string create_transaction(int packageid, int memberid)
        {
            packageService pack = new packageService();
            package itemPackage = pack.GetItemById(packageid);
            //var user_id = Comm.GetUserId();

            film_transactions trans = new film_transactions();
            trans.txtid = System.Guid.NewGuid().ToString();
            trans.datecreated = DateTime.Now;
            trans.amount1 = itemPackage.price;
            trans.currency1 = "VND";
            trans.idRef = packageid.ToString();
            trans.keyRef = "0";
            trans.statusBanking = "0";

            trans.memberid = memberid;
            int result = new film_transactionsService().UpdateOrInsert(trans);
            if (result > 0)
            {
                //call sang ngân hàng
                VTCHolder vtcHolder = new VTCHolder();
                vtcHolder.amount = trans.amount1.ToString();
                vtcHolder.currency = trans.currency1.ToString();

                vtcHolder.receiver_account = InstanceVTC.receiver_account;  // Tài khoản hứng tiền của đối tác tại VTC
                vtcHolder.reference_number = trans.txtid;           // Mã đơn hàng của đối tác, VTC và đối tác dùng đơn hàng này làm cơ sở đối soát
                vtcHolder.transaction_type = "sale";
                vtcHolder.website_id = InstanceVTC.websiteid;//txtWebsiteID.Text.Trim();

                //cập nhật vào db để đối soát
                string urlTemp = vtcHolder.urlRedirect();
                trans.urlTemp = urlTemp;
                trans.statusBanking = "2";
                trans.keyRef = "2";
                updateTransactionObj(trans);
                return trans.urlTemp;
            }
            else return "";
        }
        public void updateTransactionObj(film_transactions trans)
        {
            new film_transactionsService().UpdateOrInsert(trans);
        }
        private package GetItemById(int packageid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>().Where(e => e.Id == packageid);
                package rows = db.Select(query).FirstOrDefault();
                return rows;
            }
        }
        public List<package> GetPackages()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<package>();
                return db.Select(query); ;
            }
        }
    }
}