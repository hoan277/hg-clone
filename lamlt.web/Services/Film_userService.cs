﻿using lamlt.data;
using lamlt.web.Models;
using Microsoft.AspNetCore.Http;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_userService : LamltService
    {
        public film_userService()
        {
        }

        public List<film_user> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.fullname.Contains(page.search)));
                /**/
                List<film_user> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public List<vw_film_user> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_user>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.fullname.Contains(page.search) || e.username.Contains(page.search) || e.email.Contains(page.search) || e.desc.Contains(page.search) || e.phone.Contains(page.search));
                if (page.sub_state >= 0)
                {
                    query = query.Where(e => e.sub_state == page.sub_state);
                }
                List<vw_film_user> rows = db.Select(query).Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public film_user CheckLogin(string user, string pass, StreamWriter writer)
        {
            DateTime time = DateTime.Now;
            writer.WriteLine("__________________________ start service______");
            using (var db = _connectionData.OpenDbConnection())
            {
                writer.WriteLine("__________________________ start connecttion______");
                var query = db.From<film_user>();
                query = query.Where(e => (e.username == user));
                film_user oUser = db.Select(query).FirstOrDefault();
                writer.WriteLine(time.ToString() + " film_user " + oUser);
                if (oUser != null)
                {
                    writer.WriteLine("__________________________ username: " + user.ToString() + ", password: " + pass.ToString());
                    if (user == oUser.username && MD5.md5(pass).ToLower() == oUser.password)
                    {
                        writer.WriteLine("__________________________ password md5:" + MD5.md5(pass).ToLower());
                        writer.WriteLine("__________________________ user.username: " + oUser.username.ToString() + ", user.password: " + oUser.password.ToString());
                        var queryRole = db.From<film_role>();
                        queryRole = queryRole.Where(e => (e.Id == oUser.roleid));
                        writer.WriteLine("__________________________ user.roleid: " + oUser.roleid.ToString());
                        film_role oRole = db.Select(queryRole).FirstOrDefault();
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_USERID, oUser.Id);
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_CPID, oUser.cpid ?? 0);
                        System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_USERNAME, oUser.username.ToString());
                        if (oRole != null) System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_ROLE, oRole.code);
                        return oUser;
                    }
                    if (false) //(user == oUser.username && pass == oUser.password)
                    {
                        writer.WriteLine("__________________________ false");
                        film_role oRole = new film_role() { Id = 1, title = "BT", code = "BT" };

                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_USERID, oUser.Id);
                        System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_USERNAME, oUser.username.ToString());
                        System.Web.HttpContext.Current.Session.SetInt32(CConfig.SESSION_CPID, oUser.cpid ?? 0);
                        if (oRole != null) System.Web.HttpContext.Current.Session.SetString(CConfig.SESSION_ROLE, oRole.code);
                        return oUser;
                    }
                }
                writer.WriteLine("__________________________ return null");
                return null;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_user>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.fullname.Contains(page.search) || e.username.Contains(page.search) || e.email.Contains(page.search) || e.desc.Contains(page.search) || e.phone.Contains(page.search));
                if (page.sub_state >= 0)
                {
                    query = query.Where(e => e.sub_state == page.sub_state);
                }
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_user>();
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.fullname.Contains(page.search)));
                return db.Count(query);
            }
        }

        public film_user GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_user GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_user>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_user obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_user>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.username = obj.username;
                        objUpdate.password = MD5.md5(obj.password);
                        objUpdate.email = obj.email;
                        objUpdate.phone = obj.phone;
                        objUpdate.fullname = obj.fullname;
                        objUpdate.desc = obj.desc;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.cpid = obj.cpid;
                        objUpdate.roleid = obj.roleid;
                        objUpdate.sub_state = 0;
                        objUpdate.sub_type = 0;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_user>()
                        .Where(e => e.username == obj.username).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.username = obj.username;
                    objUpdate.password = MD5.md5(obj.password);
                    objUpdate.email = obj.email;
                    objUpdate.phone = obj.phone;
                    objUpdate.fullname = obj.fullname;
                    objUpdate.desc = obj.desc;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.cpid = obj.cpid;
                    objUpdate.roleid = obj.roleid;
                    objUpdate.sub_state = 0;
                    objUpdate.sub_type = 0;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_user InitEmpty()
        {
            var obj = new film_user();
            obj.Id = 0;
            return obj;
        }

        public film_user GetById(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_user>().Where(e => e.Id == id);
                return db.Select(query).SingleOrDefault();
            }
        }
    }
}