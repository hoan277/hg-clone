﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class detailService : LamltService
    {

        public detailService()
        {

        }
        public List<detail> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<detail>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);
                //query.OrderByDescending(x => x.userid);
                //query.OrderByDescending(x => x.videoid);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<detail> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_detail> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_detail>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                //query.OrderByDescending(x => x.Id);
                query.OrderBy(x => x.userid).OrderBy(x => x.videoid);
                //query.GroupBy(x => x.userid);
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.videoid.ToString().Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_detail> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<detail>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                //query = query.Where(e => (e.title.Contains(page.search)));

                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public detail GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<detail>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_detail GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_detail>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(detail obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<detail>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.videoid = obj.videoid;
                        objUpdate.userid = obj.userid;
                        objUpdate.modelid = obj.modelid;
                        objUpdate.count = obj.count;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<detail>()
                        .Where(e => e.videoid == obj.videoid).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.videoid = obj.videoid;
                    objUpdate.userid = obj.userid;
                    objUpdate.modelid = obj.modelid;
                    objUpdate.count = obj.count;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<detail>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public detail InitEmpty()
        {
            var obj = new detail();
            obj.Id = 0;
            return obj;
        }

        /*
		genvie here
		create view vw_detail
		as
			select detail.Id,detail.title,detail.desc,detail.datecreated,detail.userid,detail.catalogid   ,detail.title as catalogid_title  from detail   inner join detail on detail.catalogid=detail.id  
		*/
    }
}