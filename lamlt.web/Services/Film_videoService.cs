﻿using ClosedXML.Excel;
using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace lamlt.web.Services
{
    public class film_videoService : LamltService
    {
        public film_videoService()
        {
        }

        public List<film_video> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 1000 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));

                query = query.Where(e => (e.cpid == page.cpid));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

                List<film_video> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public List<vw_film_video_cms> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_cms>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search) || e.upload_file.Contains(page.search) || e.code.Contains(page.search)));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                if (page.status != -1) query = query.Where(e => e.status == page.status);
                if (page.catalogid != -1) query = query.Where(e => e.catalog_id == page.catalogid);
                if (page.film_type != -1) query = query.Where(e => e.film_type == page.film_type);
                query.Skip(offset).Take(limit);
                List<vw_film_video_cms> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

                if (page.status != -1) query = query.Where(e => e.status == page.status);
                if (page.catalogid != -1) query = query.Where(e => e.catalog_id == page.catalogid);
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_cms>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                // int cpid = -1;
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

                /*
  if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
*/

                return db.Count(query);
            }
        }

        public film_video GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_video GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_video_cms GetViewCMS(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_cms>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }


        public int UpdateByPath(string path, int status, int film_type)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                string path1 = "http://118.68.218.182/:1935/vod/_definst/" + path + "/playlist.m3u8";
                var query = db.From<film_video>().Where(e => e.title == path);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.title = path;
                    objUpdate.upload_file = path1;
                    objUpdate.status = status;
                    objUpdate.datecreated = DateTime.Now;
                    switch (status)
                    {
                        case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                        case 1: objUpdate.datecreated = DateTime.Now; break;
                        case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                        case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                        case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                        case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                        case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                    }
                    return db.Update(objUpdate);
                }
                else
                {
                    objUpdate = InitEmpty();
                    objUpdate.title = path;
                    objUpdate.upload_file = path1;
                    objUpdate.status = status;
                    objUpdate.datecreated = DateTime.Now;
                    switch (status)
                    {
                        case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                        case 1: objUpdate.datecreated = DateTime.Now; break;
                        case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                        case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                        case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                        case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                        case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                    }
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int UpdateVideo(string path, int status, string title = "", string directory = "", string code = "", string actor = "", string director = "", string publish_year = "", string desc = "", string catalog = "", string tags = "", string trainer = "", string duration = "")
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                string path_streaming = "https://stream.lalatv.com.vn/vod/_definst_/smil:automedia/" + path + "/playlist.m3u8";
                var query = db.From<film_video>().Where(e => e.title == title);
                var objUpdate = db.Select(query).FirstOrDefault();
                if (objUpdate != null)
                {
                    #region ============== START SPLIT CATEGORY BY ; ====================

                    /*=================================== START SPLIT CATEGORY BY ; ====================================*/
                    if (catalog != null)
                    {
                        objUpdate.catalog = catalog;// Fix cứng catalog bằng string. Ví dụ: Kinh dị, hoạt hình, hài hước
                        LogService.logItem("Update SPLIT catalog=" + catalog);
                        String[] strlist = catalog.Split(",");
                        List<int> list = new List<int>();
                        foreach (var lst in strlist)
                        {
                            var queryCatalog = db.From<film_catalog>().Where(e => e.title == lst);
                            var objCatalog = db.Select(queryCatalog).SingleOrDefault();
                            if (objCatalog != null) list.Add(objCatalog.Id);
                            LogService.logItem("Update lst catalog=" + lst);
                        }
                        var debug = objUpdate.Id;
                        //code test fix cứng video id = 19
                        int tesst_rs = new film_videoService().UpdateCheckbox(list.ToArray(), objUpdate.Id);
                    }
                    /*=================================== END SPLIT CATEGORY BY ; ====================================*/

                    #endregion ============== START SPLIT CATEGORY BY ; ====================

                    objUpdate.upload_file = path_streaming;
                    objUpdate.status = status;
                    objUpdate.datecreated = DateTime.Now;
                    if (title != "") objUpdate.title = title;
                    if (code != "") objUpdate.code = code;
                    if (actor != "") objUpdate.actor = actor;
                    if (director != "") objUpdate.director = director;
                    if (publish_year != "") objUpdate.publish_year = (int)float.Parse(publish_year);
                    if (tags != "") objUpdate.tags = tags;
                    if (desc != "") objUpdate.desc = desc;
                    if (duration != "") objUpdate.duration = duration;
                    if (trainer != null) objUpdate.trainer_url = trainer;
                    if (path != null) objUpdate.path = path;
                    objUpdate.cpid = objUpdate.cpid != null ? objUpdate.cpid : 5;
                    objUpdate.publish_countryid = objUpdate.publish_countryid != null ? objUpdate.publish_countryid : 4;
                    switch (status)
                    {
                        case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                        case 1: objUpdate.datecreated = DateTime.Now; break;
                        case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                        case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                        case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                        case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                        case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                    }
                    LogService.logItem(objUpdate);
                    return db.Update(objUpdate);
                }
                else
                {
                    objUpdate = InitEmpty();
                    var queryCatalog = db.From<film_catalog>().Where(e => e.title == catalog);
                    var objCatalog = db.Select(queryCatalog).SingleOrDefault();
                    if (objCatalog != null) objUpdate.catalog_id = objCatalog.Id;
                    else objUpdate.catalog_id = -1;
                    objUpdate.catalog = catalog;
                    objUpdate.upload_file = path_streaming;
                    objUpdate.status = status;
                    objUpdate.datecreated = DateTime.Now;
                    if (title != "") objUpdate.title = title;
                    if (code != "") objUpdate.code = code;
                    if (actor != "") objUpdate.actor = actor;
                    if (director != "") objUpdate.director = director;
                    if (publish_year != "") objUpdate.publish_year = (int)float.Parse(publish_year);
                    if (tags != "") objUpdate.tags = tags;
                    if (desc != "") objUpdate.desc = desc;
                    if (duration != "") objUpdate.duration = duration;
                    objUpdate.cpid = objUpdate.cpid != null ? objUpdate.cpid : 5;
                    objUpdate.publish_countryid = objUpdate.publish_countryid != null ? objUpdate.publish_countryid : 4;
                    switch (status)
                    {
                        case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                        case 1: objUpdate.datecreated = DateTime.Now; break;
                        case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                        case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                        case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                        case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                        case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                    }

                    #region ============== START SPLIT CATEGORY BY ; ====================

                    /*=================================== START SPLIT CATEGORY BY ; ====================================*/

                    int rs = (int)db.Insert(objUpdate, selectIdentity: true);
                    if (catalog != null)
                    {
                        objUpdate.catalog = catalog;// Fix cứng catalog bằng string. Ví dụ: Kinh dị, hoạt hình, hài hước
                        LogService.logItem("Update SPLIT catalog=" + catalog);
                        String[] strlist = catalog.Split(",");
                        List<int> list = new List<int>();
                        foreach (var lst in strlist)
                        {
                            var queryCatalog1 = db.From<film_catalog>().Where(e => e.title == lst);
                            var objCatalog1 = db.Select(queryCatalog1).SingleOrDefault();
                            if (objCatalog1 != null) list.Add(objCatalog1.Id);
                            LogService.logItem("Update lst catalog=" + lst);
                        }
                        int tesst_rs = new film_videoService().UpdateCheckbox(list.ToArray(), rs);
                    }
                    LogService.logItem(objUpdate);
                    /*=================================== END SPLIT CATEGORY BY ; ====================================*/

                    #endregion ============== START SPLIT CATEGORY BY ; ====================

                    return rs;
                }
            }
        }

        public int UpdateImageAuto(string file_name, int status)
        {
            string code = file_name.Split('.')[0];
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.code == code);
                var objUpdate = db.Select(query).FirstOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = status;
                    objUpdate.thumb_file = "http://image.lalatv.com.vn/automedia/" + file_name;
                    LogService.logItem("objUpdate.thumb_file=" + objUpdate.thumb_file);
                    return db.Update(objUpdate);
                }
            }
            return -1;
        }

        public int UpdateOrInsert(film_video obj, string pathVideo, string pathImage)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.desc = obj.desc;
                        objUpdate.datecreated = DateTime.Now;
                        switch (objUpdate.status)
                        {
                            case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                            case 1: objUpdate.datecreated = DateTime.Now; break;
                            case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                            case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                            case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                            case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                            case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                        }
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.seriid = comm.GetUserId();
                        objUpdate.imdb = obj.imdb;
                        objUpdate.format = obj.format;
                        objUpdate.sub_type = obj.sub_type;
                        objUpdate.publish_year = obj.publish_year;
                        objUpdate.publish_countryid = obj.publish_countryid;
                        objUpdate.duration = obj.duration;
                        objUpdate.actor = obj.actor;
                        objUpdate.director = obj.director;
                        objUpdate.film_type = obj.film_type;
                        objUpdate.episode = obj.episode;
                        objUpdate.episode_current = obj.episode_current;
                        objUpdate.contract_copyright = obj.contract_copyright;
                        objUpdate.contract_exprired = DateTime.Now;
                        objUpdate.contract_appendix = obj.contract_appendix;
                        objUpdate.copyright_appendix = obj.copyright_appendix;
                        objUpdate.copyright_expired = obj.copyright_expired;
                        //if (obj.copyright_expired != null)
                        //{
                        //    objUpdate.copyright_expired = obj.copyright_expired;
                        //}
                        objUpdate.catalog_id = obj.catalog_id;

                        if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                        {
                            objUpdate.upload_file = pathVideo + obj.upload_file + "/playlist.m3u8";
                        }
                        else { objUpdate.upload_file = obj.upload_file; }
                        //objUpdate.thumb_file = "http://image.lalatv.com.vn/" + obj.thumb_file;
                        if (obj.thumb_file != null && !obj.thumb_file.StartsWith("http"))
                        {
                            objUpdate.thumb_file = pathImage + obj.thumb_file;
                        }
                        if (obj.trainer_url != null)
                        {
                            objUpdate.trainer_url = obj.trainer_url;
                        }
                        objUpdate.exclusive = obj.exclusive != null ? 1 : 0;
                        objUpdate.showhome = obj.showhome != null ? 1 : 0;
                        objUpdate.price = obj.price;
                        objUpdate.status = obj.status;
                        //if (obj.copyright_expired != null)
                        //{
                        //    if (obj.copyright_expired < DateTime.Now)
                        //    {
                        //        objUpdate.status = 7;
                        //    }
                        //}
                        objUpdate.cpid = obj.cpid;
                        objUpdate.code = obj.code;
                        objUpdate.tags = obj.tags;
                        objUpdate.episode_id = obj.episode_id;
                        objUpdate.isview = obj.isview;
                        objUpdate.filter = obj.filter;
                        //int a = db.Update(objUpdate);
                        return db.Update(objUpdate);
                        //if (a > 0) return obj.Id;
                        //return 0;
                    }
                    return -1;
                }
                else
                {
                    //var queryCount = db.From<film_video>()
                    //	.Where(e => e.title == obj.title).Select(e => e.Id);
                    //var objCount = db.Count(queryCount);
                    //if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.desc = obj.desc;
                    objUpdate.datecreated = DateTime.Now;
                    switch (objUpdate.status)
                    {
                        case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                        case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                        case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                        case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                        case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                        case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                    }
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.seriid = comm.GetUserId();
                    objUpdate.imdb = obj.imdb;
                    objUpdate.format = obj.format;
                    objUpdate.sub_type = obj.sub_type;
                    objUpdate.publish_year = obj.publish_year;
                    objUpdate.publish_countryid = obj.publish_countryid;
                    objUpdate.duration = obj.duration;
                    objUpdate.actor = obj.actor;
                    objUpdate.director = obj.director;
                    objUpdate.film_type = obj.film_type;
                    objUpdate.episode = obj.episode;
                    objUpdate.episode_current = obj.episode_current;
                    objUpdate.contract_copyright = obj.contract_copyright;
                    objUpdate.contract_exprired = DateTime.Now;
                    objUpdate.contract_appendix = obj.contract_appendix;
                    //objUpdate.copyright_appendix = obj.copyright_appendix;
                    objUpdate.copyright_expired = obj.copyright_expired;

                    objUpdate.catalog_id = obj.catalog_id;

                    if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                    {
                        objUpdate.trainer_url = obj.trainer_url;
                    }
                    else { objUpdate.upload_file = obj.upload_file; }
                    if (obj.thumb_file != null && !obj.thumb_file.StartsWith("http"))
                    {
                        objUpdate.thumb_file = pathImage + obj.thumb_file;
                    }
                    if (obj.trainer_url != null)
                    {
                        objUpdate.trainer_url = pathVideo + obj.trainer_url + "/playlist.m3u8";
                    }
                    objUpdate.exclusive = obj.exclusive != null ? 1 : 0;
                    objUpdate.showhome = obj.showhome != null ? 1 : 0;
                    objUpdate.price = obj.price;
                    objUpdate.status = obj.status;
                    //if (obj.copyright_expired != null)
                    //{
                    //    if (obj.copyright_expired < DateTime.Now)
                    //    {
                    //        objUpdate.status = 7;
                    //    }
                    //}
                    objUpdate.cpid = obj.cpid;
                    objUpdate.code = obj.code;
                    objUpdate.tags = obj.tags;
                    objUpdate.episode_id = obj.episode_id;
                    objUpdate.isview = obj.isview;
                    objUpdate.filter = obj.filter;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int UpdateStatus(film_video obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.status = obj.status;
                        objUpdate.datecreated = DateTime.Now;
                        switch (objUpdate.status)
                        {
                            case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                            case 1: objUpdate.datecreated = DateTime.Now; break;
                            case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                            case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                            case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                            case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                            case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                        }
                        return db.Update(objUpdate);
                    }
                }
            }
            return -1;
        }

        public int UpdateCheckbox(int[] catalog_id, int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //xóa hết catalog id của film id
                var query = db.From<film_catalog_film>().Where(e => e.filmid == filmid);
                db.Delete(query);

                //lặp qua mảng id
                foreach (var catalogid in catalog_id)
                {
                    var obj = new film_catalog_film();
                    obj.filmid = filmid;
                    obj.catalogid = catalogid;
                    db.Insert<film_catalog_film>(obj);
                }
                return -1;
            }
        }

        public int UpdateImage(string url, int filmid, string pathImage)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //inset img vào film_video_image
                var query = db.From<film_video_image>().Where(e => e.filmid == filmid);
                var obj = new film_video_image();

                obj.url = pathImage + url;
                obj.filmid = filmid;
                db.Insert<film_video_image>(obj);
                // Console.WriteLine("hello world.1..");

                var query1 = db.From<film_video>().Where(e => e.Id == filmid);
                // Console.WriteLine("hello world.2..");
                var objUpdate = db.Select(query1).SingleOrDefault();
                if (objUpdate.thumb_file == null)
                {
                    objUpdate.thumb_file = pathImage + url;
                    db.Update<film_video>(objUpdate);
                }
                // neu thumb_file != null thi xoa tat ca thumb_file trong film_video di roi update file url moi hien thi trong anh dai dien giao dien admin
                else
                {
                    var delete = db.From<film_video>().Where(e => e.thumb_file == url);
                    db.Delete(delete);
                    objUpdate.thumb_file = pathImage + url;
                    db.Update<film_video>(objUpdate);
                }
            }
            return -1;
        }

        public List<film_video_image> getupdateImg(int Filmid, string path)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.filmid == Filmid);
                return db.Select(query).ToList<film_video_image>();
            }
        }
        public int deletedrmDialog(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.Id == id);
                return db.Delete(query);
            }
        }
        public int deteleimage(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.Id == id);
                return db.Delete(query);
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_video InitEmpty()
        {
            var obj = new film_video();
            obj.Id = 0;
            obj.thumb_file = "";
            obj.upload_file = "";

            return obj;
        }

        /*
		genvie here
		create view vw_film_video
		as
			select film_video.Id,film_video.title,film_video.desc,film_video.datecreated,film_video.userid,film_video.seriid,film_video.imdb,film_video.format,film_video.sub_type,film_video.publish_year,film_video.publish_countryid,film_video.duration,film_video.actor,film_video.director,film_video.film_type,film_video.episode,film_video.episode_current,film_video.contract_copyright,film_video.contract_exprired,film_video.contract_appendix,film_video.copyright_appendix,film_video.copyright_expired,film_video.catalog_id,film_video.upload_file,film_video.thumb_file,film_video.exclusive,film_video.price,film_video.status,film_video.cpid   ,film_country.title as publish_countryid_title  ,film_catalog.title as catalog_id_title  ,film_cp.title as cpid_title  from film_video   inner join film_country on film_video.publish_countryid=film_country.id    inner join film_catalog on film_video.catalog_id=film_catalog.id    inner join film_cp on film_video.cpid=film_cp.id
		*/

        public List<film_video> getAllVideo(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));

                //query = query.Where(e => (e.cpid == page.cpid));
                //if (page.cpid != 0) query = query.Where(e => e.cpid == page.cpid);
                query = query.Skip(offset).Take(limit);
                List<film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<film_video> GetListVideoByFilmEpisodeTitle(string filmEpisodeTitle)
        {
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                query.OrderByDescending(x => x.Id);
                query = query.Where(e => (e.title.Contains(filmEpisodeTitle)));
                List<film_video> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public int getEpisodeCurrentByTitleVideo(string videoTitle)
        {
            String[] title_split = videoTitle.Split(" ");
            string str = "Tập";
            string tap_title = "";
            for (int x = 0; x < title_split.Length; x++)
            {
                if (title_split[x].ToLower() == str.ToLower())
                {
                    tap_title = title_split[x + 1];
                }
                if (tap_title.ToLower() == "cuối")
                {
                    tap_title = "0";
                }
            }
            string[] numbers = Regex.Split(tap_title, @"\D+");
            int tap_int = int.Parse(numbers[0]);
            return tap_int;
            //string[] numbers = Regex.Split(videoTitle, @"\D+");
            //int i = int.Parse(numbers[numbers.Length - 1]);
            //return i;
        }

        #region ======================= RECONVERT ====================

        /// <summary>
        /// Chuyển đổi lại video
        /// </summary>
        /// <param name="file_upload">File muốn chuyển đổi</param>
        /// <returns>Đúng hoặc sai</returns>
        public bool Reconvert(string file_upload)
        {
            bool rs = false;
            string from = @"/home/www/data/data/saigonmusic/lalatv_origin/" + file_upload;
            string to = @"/home/www/data/data/saigonmusic/lalatv";
            try
            {
                if (!Directory.Exists(to))
                {
                    Directory.CreateDirectory(to);
                }
                if (File.Exists(from))
                {
                    File.Move(from, to + "/" + file_upload);
                    rs = true;
                }
                else
                {
                    LogService.logItem("---------------- LOG RE-CONVERT----------------------");
                    LogService.logItem("Không tìm thấy file: " + from);
                }
            }
            catch (Exception e)
            {
                LogService.logItem("---------------- LOG RE-CONVERT----------------------");
                LogService.logItem("fileName: " + from);
                LogService.logItem("Exception: " + e.Message);
            }
            return rs;
        }

        #endregion ======================= RECONVERT ====================
        public int GetVideoByPath(string path)
        {
            if (path == null)
            {
                return -1;
            }
            using (var db = _connectionData.OpenDbConnection())
            {
                if (path != null)
                {
                    string upload_file = "https://stream.lalatv.com.vn/vod/_definst_/smil:automedia/" + path + "/playlist.m3u8";
                    var query = db.From<film_video>().Where(e => e.upload_file == upload_file);
                    var objUpdate = db.Select(query).FirstOrDefault();
                    if (objUpdate != null)
                    {
                        return 1;
                    }
                    return 0;
                }
                return -1;
            }
        }


        #region ===== EPISODE =====
        public int UpdateEpisode(film_video obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.episode = obj.episode;
                        objUpdate.film_type = 1;
                        objUpdate.episode_current = obj.episode_current;
                        objUpdate.episode_id = obj.episode_id;
                        int a = db.Update(objUpdate);
                        if (a > 0) return obj.Id;
                        return 0;
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.episode = obj.episode;
                    objUpdate.film_type = 1;
                    objUpdate.episode_current = obj.episode_current;
                    objUpdate.episode_id = obj.episode_id;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        #endregion ===== EPISODE =====

        #region ===== TRAINER =====
        public int UpdateVideoTrainer(string path, string trainer, int status)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (path != null)
                {
                    string upload_file = "https://stream.lalatv.com.vn/vod/_definst_/smil:automedia/" + path + "/playlist.m3u8";
                    var query = db.From<film_video>().Where(e => e.upload_file == upload_file);
                    var objUpdate = db.Select(query).FirstOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.status = status;
                        objUpdate.datecreated = DateTime.Now;
                        if (trainer != null) objUpdate.trainer_url = trainer;
                        if (path != null) objUpdate.path = path;
                        objUpdate.cpid = objUpdate.cpid != null ? objUpdate.cpid : 5;
                        objUpdate.publish_countryid = objUpdate.publish_countryid != null ? objUpdate.publish_countryid : 4;
                        switch (status)
                        {
                            case 0: objUpdate.dateupdate1 = DateTime.Now; break;
                            case 1: objUpdate.datecreated = DateTime.Now; break;
                            case 2: objUpdate.dateupdate2 = DateTime.Now; break;
                            case 3: objUpdate.dateupdate3 = DateTime.Now; break;
                            case 4: objUpdate.dateupdate4 = DateTime.Now; break;
                            case 5: objUpdate.dateupdate5 = DateTime.Now; break;
                            case 6: objUpdate.dateupdate6 = DateTime.Now; break;
                        }
                        LogService.logItem(objUpdate);
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                return -1;
            }
        }
        #endregion ===== TRAINER =====
        public List<vw_film_video_cms> GetViewAllItemBanner(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100000 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_cms>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 100000;
                try { limit = page.limit; }
                catch { }
                query = query.Skip(offset).Take(limit);
                List<vw_film_video_cms> rows = db.Select(query).ToList();
                return rows;
            }
        }

        #region ===== For Film_video_view =====
        public List<vw_film_video_cms> GetFilmForFilmVideoView(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 1000 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_cms>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Skip(offset).Take(limit);
                List<vw_film_video_cms> rows = db.Select(query).ToList();
                return rows;
            }
        }
        #endregion ===== For Film_video_view =====


        #region export excel to upload youtube
        public string export_upload_youtube(List<vw_film_video_cms> obj)
        {
            DataTable dt = new DataTable
            {
                TableName = "Phim"
            };
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("file_dir", typeof(string));
            dt.Columns.Add("filename", typeof(string));
            dt.Columns.Add("title", typeof(string));
            dt.Columns.Add("description", typeof(string));
            dt.Columns.Add("keyword", typeof(string));
            dt.Columns.Add("privacy_status", typeof(string));
            //Add Rows in DataTable
            int stt = 1;
            for (int i = 0; i < obj.Count; i++)
            {
                dt.Rows.Add(
                    stt,
                    "/home/www/data/data/media/automedia",
                    obj[i].upload_file.Split("/")[6].Split(".")[0] + ".mp4",
                    obj[i].title,
                    obj[i].desc,
                    obj[i].tags,
                    "private"
                    );
                stt++;
            }
            dt.AcceptChanges();
            //dt.DefaultView.Sort = "Tên Phim ASC";
            //Name of File

            //string folderPath = "H:\\amnhacsaigon\\banquyen\\lalatv-acs-be\\lamlt.web\\wwwroot\\export_upload\\";
            string folderPath = "/home/amnhacsaigon/python/youtube/upload_youtube/";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fileName = "course_info" + ".xlsx";
            //string fileName = "upload_youtube" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                var ws = wb.Worksheets.Add(dt, "upload_list");
                ws.Style.Font.Bold = true;
                ws.Row(1).Height = 18;
                ws.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                ws.Row(1).Style.Font.FontColor = XLColor.Red;
                ws.Row(1).Style.Font.SetFontSize(12);
                ws.Row(1).Height = 30;
                ws.Rows().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                ws.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Rows().Style.Alignment.WrapText = true;
                ws.Rows().AdjustToContents();
                wb.SaveAs(folderPath + fileName);
                string rs = "/export_video/" + fileName;
                return rs;
            }
        }
        #endregion export excel to upload youtube
    }
}