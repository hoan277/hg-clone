﻿using lamlt.data;
using lamlt.web.Services;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;

namespace lamlt.webservice.Services
{
    public class film_key_logService : LamltService
    {
        public int GetTime(string key)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key_log>().Where(e => (e.key == key));
                return db.Select(query).LastOrDefault().time;
            }
        }
        public List<film_key_log> Get(int id, string key)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key_log>();
                if (id > 0) { query = query.Where(e => e.id == id); }
                if (!string.IsNullOrEmpty(key)) { query = query.Where(e => e.key == key); }
                return db.Select(query).ToList();
            }
        }

        internal object Delete(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key_log>();
                if (id > 0) { query = query.Where(e => e.id == id); }
                return db.Delete(query);
            }
        }

        public int UpdateBykey(film_key_log obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key_log>().Where(e => (e.key == obj.key));
                film_key_log config = db.Select(query).LastOrDefault();
                if (config == null)
                {
                    config = new film_key_log
                    {

                        key = obj.key,
                        time = obj.time,
                        created = DateTime.Now,
                        note = obj.note,
                    };
                    return (int)db.Insert(config, selectIdentity: true);
                }
                config.time = obj.time;
                config.note = obj.note;
                if (db.Update(config) > 0)
                {
                    return obj.id;
                }
                return 0;
            }
        }
    }
}
