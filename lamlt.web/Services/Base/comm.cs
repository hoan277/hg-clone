﻿using lamlt.web.Models;
using Microsoft.AspNetCore.Http;
using ServiceStack;
using System;
using System.Text.RegularExpressions;

namespace lamlt.web.Services
{
    [Serializable()]
    public static class comm
    {
        public static readonly int ERROR_NOT_EXIST = -3;
        public static readonly int ERROR_EXIST = -2;
        public static readonly int ERROR_GENERAL = -1;

        public static int GetUserId()
        {
            if (System.Web.HttpContext.Current == null) return -1;
            if (System.Web.HttpContext.Current.Session == null) return -1;
            if (System.Web.HttpContext.Current.Session.Get(CConfig.SESSION_USERID) == null) return -1;

            int? x = System.Web.HttpContext.Current.Session.GetInt32(CConfig.SESSION_USERID);
            return x.Value;
        }
        public static int GetCPId()
        {
            if (System.Web.HttpContext.Current == null) return -1;
            if (System.Web.HttpContext.Current.Session == null) return -1;

            if (System.Web.HttpContext.Current.Session.Get(CConfig.SESSION_CPID) == null) return -1;

            int? x = System.Web.HttpContext.Current.Session.GetInt32(CConfig.SESSION_CPID);
            // return -1
            return x.Value;
        }

        public static String GetUserName()
        {
            if (System.Web.HttpContext.Current == null) return "";
            if (System.Web.HttpContext.Current.Session.Get(CConfig.SESSION_USERNAME) == null) return "";

            return (System.Web.HttpContext.Current.Session.GetString(CConfig.SESSION_USERNAME));
        }

        public static string GetRightCode()
        {
            if (System.Web.HttpContext.Current == null) return "";
            if (System.Web.HttpContext.Current.Session.Get(CConfig.SESSION_ROLE) == null) return "";
            string sCode = (System.Web.HttpContext.Current.Session.GetString(CConfig.SESSION_ROLE));
            return sCode.ToUpper();
        }
        public static int GetFlagSupperAdmin()
        {
            if (System.Web.HttpContext.Current == null) return -1;
            if (System.Web.HttpContext.Current.Session.Get(CConfig.SESSION_SUPPERADMIN_FLAG) == null) return -1;
            return int.Parse(System.Web.HttpContext.Current.Session.Get(CConfig.SESSION_SUPPERADMIN_FLAG).ToString());

        }
        public static bool IsAllHotel()
        {
            return (GetFlagSupperAdmin() <= 0);
        }
        public static bool IsSuperAdminCode()
        {
            return (GetRightCode().ToUpper() == "SUPERADMIN");
        }
        public static bool IsCP()
        {
            return (GetRightCode().ToUpper() == "CP");
        }
        // SUPPER ADMIN
        public static bool IsSuperAdmin()
        {
            return (GetRightCode() == "SUPERADMIN");
        }
        public static bool IsBT()
        {
            return (GetRightCode() == "BT");
        }
        public static bool IsBQ()
        {
            return (GetRightCode() == "BQ");
        }
        public static bool IsTT()
        {
            return (GetRightCode() == "TT");
        }

        public static bool IsKeToan()
        {
            return (GetRightCode() == "KETOAN");
        }
        public static bool IsLeTan()
        {
            return (GetRightCode() == "LETAN");
        }
        public static bool IsQuanLy()
        {
            return (GetRightCode() == "QUANLY");
        }
        public static bool IsSale()
        {
            return (GetRightCode() == "SALE");
        }
        public static bool IsThuNgan()
        {
            return (GetRightCode() == "RMS_TELLER");
        }
        public static bool IsNhaBep()
        {
            return (GetRightCode() == "RMS_COOK");
        }
        public static bool IsChayBan()
        {
            return (GetRightCode() == "RMS_WAITER");
        }
        public static bool IsSL()
        {
            return IsSale();
        }

        public static bool IsKT()
        {
            return IsKeToan();
        }
        public static bool IsLT()
        {
            return IsLeTan();
        }
        public static bool IsQL()
        {
            return IsQuanLy();
        }
        public static bool IsBp()
        {
            return IsBuongPhong();
        }
        public static bool IsBuongPhong()
        {
            return (GetRightCode() == "BUONGPHONG");
        }
        // HIỂN THỊ MENU VỚI TÀI KHOẢN SUPPER ADMIN
        public static bool IsSP()
        {
            return (GetRightCode() == "SUPERADMIN");
        }
        public static bool IsSelected()
        {
            var admin = (GetRightCode() == "SUPERADMIN");
            var flag = GetFlagSupperAdmin();
            if (admin == true && flag > -1) return true;
            else if (admin == false && flag == -1) return true;
            else return false;
        }
        public static string SerializeObject(object value)
        {
            string sb = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            string replacement = Regex.Replace(sb, @"\\t|\\n|\\r", "");
            return replacement.ToString();
        }

    }
}
