﻿using lamlt.data;
using lamlt.web.Models;
using Newtonsoft.Json;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace lamlt.web.Services
{
    public class livestreamingService : LamltService
    {
        public livestreamingService()
        {
        }

        public List<livestreaming> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.video.Contains(page.search)));
                /*
  if(!(video == "-1") ||  string.IsNullOrEmpty(video)) query=query.Where(e=>e.video == page.video);
*/

                List<livestreaming> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public List<vw_livestreaming> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) { page.search = ""; }
            if (page.live_status == null) { page.live_status = ""; }
            if (page.live_type == null) { page.live_type = ""; }
            if (page.start_time == null) { page.start_time = "2020-01-01"; }
            if (page.end_time == null) { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.live_userid == null) { page.live_userid = comm.GetUserId().ToString(); }

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);

                var query = db.From<vw_livestreaming>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e =>
                    e.title.Contains(page.search) &&
                    e.status.Contains(page.live_status) &&
                    e.type.Contains(page.live_type)
                );
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                if (page.live_userid != "-1")
                {
                    query.Where(e => e.userid == int.Parse(page.live_userid));
                }
                /*
  if(!(video == "-1") ||  string.IsNullOrEmpty(video)) query=query.Where(e=>e.video == page.video);
*/

                List<vw_livestreaming> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();

                return rows;
            }
        }

        internal int UpdateWait(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (id < 0) return -1;
                var query = db.From<livestreaming>().Where(e => e.Id == id);
                var objUpdate = new livestreaming();
                var obj = GetByID(id.ToString());
                if (obj != null)
                {
                    objUpdate.Id = obj.Id;
                    objUpdate.processid = obj.processid;
                    objUpdate.cmd = obj.cmd;
                    objUpdate.video = obj.video;
                    objUpdate.status = "wait";
                    objUpdate.type = obj.type;
                    objUpdate.userid = obj.userid;
                    objUpdate.title = obj.title;
                    objUpdate.note = obj.note;
                    objUpdate.resolution = obj.resolution;
                    objUpdate.live_catalog_id = obj.live_catalog_id;
                    objUpdate.datecreated = obj.datecreated != null ? obj.datecreated : DateTime.Now;
                    objUpdate.starttime = obj.starttime != null ? obj.starttime : DateTime.Now;
                    objUpdate.stoptime = obj.stoptime != null ? obj.stoptime : DateTime.Now.AddDays(1);
                    return db.Update(objUpdate);
                }
                return -1;
            }
        }

        public List<vw_livestreaming> GetViewAllItem_Export()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                List<vw_livestreaming> rows = db.Select(db.From<vw_livestreaming>()).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(video == "-1") ||  string.IsNullOrEmpty(video)) query=query.Where(e=>e.video == page.video);
*/

                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_livestreaming>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                /*
  if(!(video == "-1") ||  string.IsNullOrEmpty(video)) query=query.Where(e=>e.video == page.video);
*/

                return db.Count(query);
            }
        }

        public livestreaming GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_livestreaming GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_livestreaming>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(livestreaming obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<livestreaming>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.processid = obj.processid;
                        objUpdate.cmd = obj.cmd;
                        objUpdate.video = obj.video;
                        objUpdate.status = obj.status;
                        objUpdate.type = obj.type;
                        objUpdate.userid = obj.userid;
                        objUpdate.title = obj.title;
                        objUpdate.note = obj.note;
                        objUpdate.resolution = obj.resolution;
                        objUpdate.live_catalog_id = obj.live_catalog_id;
                        objUpdate.datecreated = obj.datecreated != null ? obj.datecreated : DateTime.Now;
                        objUpdate.starttime = obj.starttime.ToString().Contains("0001") || obj.starttime == null ? null : obj.starttime;
                        objUpdate.stoptime = obj.stoptime.ToString().Contains("0001") || obj.stoptime == null ? null : obj.stoptime;
                        //LogService.logItem("Ghi log livestream update");
                        //LogService.logItem(objUpdate);
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    //var queryCount = db.From<livestreaming>()
                    //    .Where(e => e.video == obj.video).Select(e => e.Id);
                    //var objCount = db.Count(queryCount);
                    //if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.processid = obj.processid;
                    //string youtube_id = GetYouTubeId(obj.cmd);
                    //livestreaming obj_old = Check_Exist_Link(youtube_id);
                    //if (obj_old != null)
                    //{
                    //    obj.cmd = obj.cmd.Replace(obj.video, obj_old.video);
                    //    obj.video = obj.video.Replace(obj.video, obj_old.video);
                    //}
                    objUpdate.cmd = obj.cmd;
                    objUpdate.video = obj.video;
                    objUpdate.status = obj.status;
                    objUpdate.note = obj.note;
                    objUpdate.type = obj.type;
                    objUpdate.userid = obj.userid;
                    objUpdate.title = obj.title;
                    objUpdate.resolution = obj.resolution;
                    objUpdate.live_catalog_id = obj.live_catalog_id;
                    objUpdate.datecreated = obj.datecreated != null ? obj.datecreated : DateTime.Now;
                    objUpdate.starttime = obj.starttime.ToString().Contains("0001") ? null : obj.starttime;
                    objUpdate.stoptime = obj.stoptime.ToString().Contains("0001") ? null : obj.stoptime;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public static string GetYouTubeId(string url)
        {
            var youtubeMatch =
               new Regex(@"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)").Match(url);
            return youtubeMatch.Success ? youtubeMatch.Groups[1].Value : string.Empty;
        }

        public livestreaming Check_Exist_Link(string link)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>().Where(e => e.cmd.Contains(link));
                return db.Select(query).SingleOrDefault();
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public livestreaming InitEmpty()
        {
            var obj = new livestreaming();
            obj.Id = 0;
            return obj;
        }

        internal int KillProcessLive(string id, string process_id)
        {
            int rs = -1;
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string path = "/home/amnhacsaigon/livestream_v2/";
            if (isDev)
            {
                path = "C://";
            }
            using (StreamWriter sw = new StreamWriter(path + process_id + ".process", false, Encoding.UTF8))
            {
                sw.Write(JsonConvert.SerializeObject(GetViewByID(id)));
                sw.Close();
                rs = 1;
            }
            return rs;
        }

        internal int StopProcess(int id)
        {
            if (id <= 0) return -1;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<livestreaming>().Where(e => e.Id == id);
                livestreaming old_obj = db.Select(query).SingleOrDefault();
                var update_obj = new livestreaming()
                {
                    Id = old_obj.Id,
                    processid = old_obj.processid,
                    cmd = old_obj.cmd,
                    status = "stop",
                    resolution = old_obj.resolution,
                    video = old_obj.video,
                    note = old_obj.note,
                    title = old_obj.title,
                    type = old_obj.type,
                    userid = old_obj.userid,
                    datecreated = old_obj.datecreated,
                    live_catalog_id = old_obj.live_catalog_id,
                    starttime = null,
                    stoptime = null,
                };
                return db.Update(update_obj);
            }
        }

        /*
    genvie here
    create view vw_livestreaming
    as
    select livestreaming.Id,livestreaming.processid,livestreaming.cmd,livestreaming.video,livestreaming.status,livestreaming.note,livestreaming.datecreated  from livestreaming
    */
    }
}