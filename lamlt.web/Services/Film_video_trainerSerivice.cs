﻿using lamlt.data;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_trainerService : LamltService
    {
        public film_video_trainerService()
        {
        }

        //public List<film_video_trainer> GetAllItem(PagingModel page)
        //{
        //    if (page == null) page = new PagingModel() { offset = 0, limit = 1000 };
        //    if (page.search == null) page.search = "";
        //    using (var db = _connectionData.OpenDbConnection())
        //    {
        //        var query = db.From<film_video_trainer>();
        //        query.OrderByDescending(x => x.Id);

        //        int offset = 0; try { offset = page.offset; }
        //        catch { }

        //        int limit = 10;//int.Parse(Request.Params["limit"]);
        //        try { limit = page.limit; }
        //        catch { }
        //        query = query.Where(e => (e.title.Contains(page.search)));

        //        query = query.Where(e => (e.cpid == page.cpid));
        //        if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

        //        List<film_video_trainer> rows = db.Select(query)
        //            .Skip(offset).Take(limit).ToList();
        //        return rows;
        //    }
        //}

        //public List<vw_film_video_trainer> GetViewAllItem(PagingModel page)
        //{
        //    if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
        //    if (page.search == null) page.search = "";
        //    //  ServiceStackHelper.Help();
        //    //  LicenseUtils.ActivatedLicenseFeatures();
        //    //search again
        //    using (var db = _connectionData.OpenDbConnection())
        //    {
        //        var query = db.From<vw_film_video_trainer>();
        //        if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
        //        query.OrderByDescending(x => x.Id);

        //        int offset = 0; try { offset = page.offset; }
        //        catch { }

        //        int limit = 10;//int.Parse(Request.Params["limit"]);
        //        try { limit = page.limit; }
        //        catch { }
        //        query = query.Where(e => (e.title.Contains(page.search) || e.upload_file.Contains(page.search) || e.code.Contains(page.search)));
        //        //if(page.search !=null) query = query.Where(e=>e.upload_file.Contains(page.search));
        //        if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
        //        if (page.status != -1) query = query.Where(e => e.status == page.status);
        //        if (page.catalogid != -1) query = query.Where(e => e.catalog_id == page.catalogid);
        //        if (page.film_type != -1) query = query.Where(e => e.film_type == page.film_type);
        //        /*
        //          if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
        //        */

        //        List<vw_film_video_trainer> rows = db.Select(query)
        //            .Skip(offset).Take(limit).ToList();
        //        return rows;
        //    }
        //}

        //public long CountAll(PagingModel page)
        //{
        //    if (page.search == null) page.search = "";
        //    using (var db = _connectionData.OpenDbConnection())
        //    {
        //        var query = db.From<film_video_trainer>();
        //        if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
        //        //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
        //        int offset = 0; try { offset = page.offset; }
        //        catch { }

        //        int limit = 10;//int.Parse(Request.Params["limit"]);
        //        try { limit = page.limit; }
        //        catch { }

        //        query = query.Where(e => (e.title.Contains(page.search)));
        //        if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

        //        if (page.status != -1) query = query.Where(e => e.status == page.status);
        //        if (page.catalogid != -1) query = query.Where(e => e.catalog_id == page.catalogid);

        //        /*
        //          if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
        //        */

        //        return db.Count(query);
        //    }
        //}

        //        public long CountViewAll(PagingModel page)
        //        {
        //            if (page.search == null) page.search = "";
        //            using (var db = _connectionData.OpenDbConnection())
        //            {
        //                var query = db.From<vw_film_video_trainer>();
        //                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
        //                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
        //                int offset = 0; try { offset = page.offset; }
        //                catch { }

        //                int limit = 10;//int.Parse(Request.Params["limit"]);
        //                try { limit = page.limit; }
        //                catch { }

        //                query = query.Where(e => (e.title.Contains(page.search)));
        //                // int cpid = -1;
        //                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

        //                /*
        //  if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
        //*/

        //                return db.Count(query);
        //            }
        //        }

        public film_video_trainer GetById(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_trainer>().Where(e => e.Id == id);
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_video_trainer GetViewById(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_trainer>().Where(e => e.Id == id);
                return db.Select(query).SingleOrDefault();
            }
        }


        public int UpdateOrInsert(film_video_trainer obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video_trainer>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.desc = obj.desc;
                        objUpdate.trainer_url = obj.trainer_url;
                        objUpdate.trainer_thumbnail = obj.trainer_thumbnail;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.status = obj.status;
                        int a = db.Update(objUpdate);
                        if (a > 0) return obj.Id;
                        return 0;
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.title = obj.title;
                    objUpdate.desc = obj.desc;
                    objUpdate.trainer_url = obj.trainer_url;
                    objUpdate.trainer_thumbnail = obj.trainer_thumbnail;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.status = obj.status;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int UpdateStatus(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_trainer>().Where(e => e.Id == id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = 1;
                    return db.Update(objUpdate);
                }
                return -1;
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_trainer>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_video_trainer InitEmpty()
        {
            var obj = new film_video_trainer();
            obj.Id = 0;
            return obj;
        }
    }
}