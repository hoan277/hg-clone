﻿using lamlt.data;
using lamlt.web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.webservice.Services
{
    public class film_keyService : LamltService
    {
        public string GetBykey(string key)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key>().Where(e => (e.key == key));
                return db.Select(query).LastOrDefault().allow;
            }
        }
        public film_key Get(int id, string key)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key>();
                if (id > 0) { query = query.Where(e => e.id == id); }
                if (!string.IsNullOrEmpty(key)) { query = query.Where(e => e.key == key); }
                return db.Select(query).LastOrDefault();
            }
        }
        public Dictionary<object, object> GetTotalTime(string key)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key>().Where(e => e.key == key);
                film_key fk = db.Select(query).LastOrDefault();
                if (fk != null)
                {
                    dict.Add("key", key);
                    dict.Add("allow", int.Parse(!string.IsNullOrEmpty(fk.allow) ? fk.allow : "0"));
                    var query_log = db.From<film_key_log>().Where(e => e.key == key);
                    List<film_key_log> fkl = db.Select(query_log).ToList();
                    if (fkl.Count > 0)
                    {
                        int total_time = 0;
                        foreach (var item in fkl)
                        {
                            total_time += item.time > 0 ? item.time : 0;
                        }
                        dict.Add("total", total_time);
                    }
                }
                return dict;
            }
        }
        public List<film_key> GetAll(int id, string key)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key>();
                return db.Select(query).ToList();
            }
        }

        internal object Delete(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key>();
                if (id > 0) { query = query.Where(e => e.id == id); }
                return db.Delete(query);
            }
        }

        public int UpdateBykey(film_key obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_key>().Where(e => (e.key == obj.key));
                film_key config = db.Select(query).LastOrDefault();
                if (config == null)
                {
                    string key = Guid.NewGuid().ToString();
                    config = new film_key
                    {
                        key = key,
                        allow = obj.allow,
                        title = obj.title,
                        status = obj.status,
                        note = obj.note,
                    };
                    return (int)db.Insert(config, selectIdentity: true);
                }
                config.allow = !string.IsNullOrEmpty(obj.allow) ? obj.allow : config.allow;
                config.title = !string.IsNullOrEmpty(obj.title) ? obj.title : config.title;
                config.status = !string.IsNullOrEmpty(obj.status) ? obj.status : config.status;
                config.note = !string.IsNullOrEmpty(obj.note) ? obj.note : config.note;
                if (db.Update(config) > 0)
                {
                    return obj.id;
                }
                return 0;
            }
        }

        public string CheckKey()
        {
            string serial = LamltService.GetConnectionString("key");
            string api_key = LamltService.GetConnectionString("api_key");
            string api_key_log = LamltService.GetConnectionString("api_key_log");
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (isDev)
            {
                api_key = api_key.Replace("183.81.35.24", "localhost");
                api_key_log = api_key_log.Replace("183.81.35.24", "localhost");
            }
            var request = new RestRequest(Method.GET);
            IRestResponse response;
            IRestResponse response_log;
            try
            {
                var url = api_key + serial;
                response = new RestClient(url).Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JObject jobj = JObject.Parse(response.Content);
                    Dictionary<string, string> dictObj = jobj.ToObject<Dictionary<string, string>>();
                    dictObj.TryGetValue("key", out string key);
                    dictObj.TryGetValue("id", out string id);
                    dictObj.TryGetValue("value", out string value);
                    if (int.Parse(value) > 0 || !string.IsNullOrEmpty(key))
                    {
                        // Check Key log
                        response_log = new RestClient(api_key_log + serial).Execute(request);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            List<film_key_log> lstLog = JsonConvert.DeserializeObject<List<film_key_log>>(response_log.Content);
                            int total_time = 0;
                            foreach (var item in lstLog)
                            {
                                total_time += item.time;
                            }
                            if (total_time < int.Parse(value))
                            {
                                return "success";
                            }
                            else
                            {
                                return "timeout";//Thời gian sử dụng đã hết, vui lòng liên hệ quản trị viên
                            }
                        }
                        else
                        {
                            return "key_log_error";//Lỗi không kiểm tra lịch sử sử dụng của key
                        }
                    }
                    else
                    {
                        return "no_key";//Chưa cài đặt key kích hoạt bản quyền, vui lòng liên hệ quản trị viên
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "error";
            }
            return "";
        }
    }
}
