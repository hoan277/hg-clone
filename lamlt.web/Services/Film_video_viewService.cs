﻿using ClosedXML.Excel;
using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_viewService : LamltService
    {

        public film_video_viewService() { }

        public List<film_video_view> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_view>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                List<film_video_view> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_video_view> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) { page.search = ""; }

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_view>();
                query.OrderByDescending(x => x.Id);
                if (!string.IsNullOrEmpty(page.range_time))
                {
                    string[] array = page.range_time.Split("-");
                    DateTime dt1 = DateTime.ParseExact(array[0].Trim() + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime dt2 = DateTime.ParseExact(array[1].Trim() + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= dt1 && e.dateupdated <= dt2);
                }
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.film_title.Contains(page.search)));
                if (page.film_id != -1)
                {
                    query = query.Where(e => e.videoid == page.film_id);
                }
                query = query.Skip(offset).Take(limit);
                List<vw_film_video_view> rows = db.Select(query).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) { page.search = ""; }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_view>();
                query.OrderByDescending(x => x.Id);
                if (!string.IsNullOrEmpty(page.range_time))
                {
                    string[] array = page.range_time.Split("-");
                    DateTime dt1 = DateTime.ParseExact(array[0].Trim() + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime dt2 = DateTime.ParseExact(array[1].Trim() + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= dt1 && e.dateupdated <= dt2);
                }
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.film_title.Contains(page.search)));
                if (page.film_id != -1)
                {
                    query = query.Where(e => e.videoid == page.film_id);
                }
                query = query.Skip(offset).Take(limit);
                return db.Count(query);
            }
        }
        public film_video_view GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_view>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_video_view GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video_view>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
            }
            return -1;
        }
        public int UpdateOrInsert(film_video_view obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_video_view>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.videoid = obj.videoid;
                        objUpdate.userid = obj.userid;
                        objUpdate.status = obj.status;
                        objUpdate.view = obj.view;
                        objUpdate.custom_duration = obj.custom_duration;
                        objUpdate.time_start = obj.time_start;
                        objUpdate.time_end = obj.time_end;
                        objUpdate.datecreated = DateTime.Now;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_video_view>()
                    .Where(e => e.videoid == obj.videoid).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    objUpdate.videoid = obj.videoid;
                    objUpdate.userid = obj.userid;
                    objUpdate.status = obj.status;
                    objUpdate.view = obj.view;
                    objUpdate.custom_duration = obj.custom_duration;
                    objUpdate.time_start = obj.time_start;
                    objUpdate.time_end = obj.time_end;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_view>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_video_view InitEmpty()
        {
            var obj = new film_video_view();
            obj.Id = 0;
            return obj;
        }

        public List<view_video> GetVideoView(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) { page.search = ""; }
            using (var db = _connectionData.OpenDbConnection())
            {
                var queryVideo = db.From<vw_film_video>();
                queryVideo.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                queryVideo = queryVideo.Where(e => (e.title.Contains(page.search)));
                if (page.status != -1)
                {
                    queryVideo = queryVideo.Where(e => (e.status == page.status));
                }
                if (page.film_type != -1)
                {
                    queryVideo = queryVideo.Where(e => (e.film_type == page.film_type));
                }
                if (page.catalogid != -1)
                {
                    queryVideo = queryVideo.Where(e => (e.catalog_id == page.catalogid));
                }
                queryVideo = queryVideo.Skip(offset).Take(limit);
                List<vw_film_video> listVideo = db.Select(queryVideo).ToList();
                List<view_video> list_view_video = new List<view_video>();
                foreach (var video in listVideo)
                {
                    var queryView = db.From<vw_film_video_view>();
                    var queryDay = db.From<vw_film_video_view>();
                    var queryWeek = db.From<vw_film_video_view>();
                    var queryMonth = db.From<vw_film_video_view>();
                    queryView.OrderByDescending(x => x.Id);
                    queryView = queryView.Where(e => e.videoid == video.Id);
                    // all date
                    if (!string.IsNullOrEmpty(page.range_time))
                    {
                        string[] array = page.range_time.Split("-");
                        DateTime dt1 = DateTime.ParseExact(array[0].Trim() + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                        DateTime dt2 = DateTime.ParseExact(array[1].Trim() + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                        queryView = queryView.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                    }
                    queryView = queryView.Skip(offset);
                    DateTime today = DateTime.Now;
                    // 48h ago
                    DateTime twoday_ago = today.AddDays(-2);
                    // 7 ngay
                    DateTime week_ago = today.AddDays(-7);
                    // 30 ngay
                    DateTime month_ago = today.AddMonths(-1);

                    queryDay = queryView.Where(x => x.datecreated >= twoday_ago && x.datecreated <= today);
                    queryWeek = queryView.Where(x => x.datecreated >= week_ago && x.datecreated <= today);
                    queryMonth = queryView.Where(x => x.datecreated >= month_ago && x.datecreated <= today);
                    view_video view = new view_video()
                    {
                        film = video,
                        view_all = Convert.ToInt32(db.Count(queryView)),
                        view_day = Convert.ToInt32(db.Count(queryDay)),
                        view_week = Convert.ToInt32(db.Count(queryWeek)),
                        view_month = Convert.ToInt32(db.Count(queryMonth))
                    };
                    list_view_video.Add(view);
                };
                return list_view_video;
            }
        }
        public long CountVideoView(PagingModel page)
        {
            if (page.search == null) { page.search = ""; }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 100;
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                return db.Count(query);
            }
        }


        public string export_data(List<view_video> obj)
        {
            string folderPath = "/home/amnhacsaigon/dotnet/cms-lalatv-viettel-v1/wwwroot/bien-ban/";
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (isDev)
            {
                folderPath = @"D:\excel\";
            }
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fileName = "Bien-ban-doi-soat " + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            // table 2
            DataTable dt = new DataTable
            {
                TableName = "Chi tiết"
            };
            dt.Columns.Add("STT", typeof(string));
            dt.Columns.Add("Video", typeof(string));
            dt.Columns.Add("Năm xuất bản", typeof(string));
            dt.Columns.Add("Tổng lượt xem 48h trước", typeof(string));
            dt.Columns.Add("Tổng lượt xem/ tuần", typeof(string));
            dt.Columns.Add("Tổng lượt xem/ tháng", typeof(string));
            int stt = 1;
            int sum_day = 0;
            int sum_week = 0;
            int sum_month = 0;
            if (obj.Count != 0)
            {
                for (int i = 0; i < obj.Count; i++)
                {
                    dt.Rows.Add(stt, obj[i].film.title, obj[i].film.publish_year, obj[i].view_day, obj[i].view_week, obj[i].view_month);
                    stt++;
                    sum_day = sum_day + obj[i].view_day;
                    sum_week = sum_week + obj[i].view_week;
                    sum_month = sum_month + obj[i].view_month;
                }
                dt.AcceptChanges();
            }
            dt.Rows.Add(" ", " ", " ", sum_day, sum_week, sum_month);

            using (XLWorkbook wb = new XLWorkbook())
            {
                //sheet 2

                var ws2 = wb.Worksheets.Add("Chi tiết hàng tháng");
                ws2.Style.Font.FontName = "Times New Roman";
                ws2.Style.Font.FontSize = 12;
                var table2 = ws2.Cell(1, 1).InsertTable(dt);

                table2.Theme = XLTableTheme.None;
                table2.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                table2.Cells().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws2.Columns().AdjustToContents();

                // saves
                wb.SaveAs(folderPath + fileName);
                string rs = "/bien-ban-doi-soat/" + fileName;
                return rs;
            }
        }


    }
}