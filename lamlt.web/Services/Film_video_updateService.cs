﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_video_updateService : LamltService
    {
        public film_video_updateService()
        {

        }
        public List<film_video> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.status == 0) { };
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                if (!comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Where(e => (e.status == page.status));
                if (page.status == 0) query = query.Where(e => e.status == page.status);
                //query = query.Where(e => (e.cpid == page.cpid));
                // if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);



                List<film_video> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_video> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            //if (page.search == null) page.search = "";
            if (page.status == 0) ;
            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                if (page.status == 0) query = query.Where(e => e.status == page.status);
                //if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                //query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Where(e => e.status == page.status);
                // if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                //if (page.status != 0) query = query.Where(e => e.status == page.status);
                /*
  if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
*/


                List<vw_film_video> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            //if (page.cpid == -1) page.cpid = 0;
            if (page.status == 0) ;
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Where(e => e.status == page.status);
                // if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

                /*
  if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
*/

                return db.Count(query);
            }
        }
        public long CountViewAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>();
                // if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.title.Contains(page.search)));
                // int cpid = -1;
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);

                /*
  if(!(cpid == "-1") ||  string.IsNullOrEmpty(cpid)) query=query.Where(e=>e.cpid == page.cpid);
*/

                return db.Count(query);
            }
        }
        public film_video GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_video GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_video>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_video obj, string pathImage, string pathVideo)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.desc = obj.desc;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.userid = comm.GetUserId();
                        objUpdate.seriid = comm.GetUserId();
                        objUpdate.imdb = obj.imdb;
                        objUpdate.format = obj.format;
                        objUpdate.sub_type = obj.sub_type;
                        objUpdate.publish_year = obj.publish_year;
                        objUpdate.publish_countryid = obj.publish_countryid;
                        objUpdate.duration = obj.duration;
                        objUpdate.actor = obj.actor;
                        objUpdate.director = obj.director;
                        objUpdate.film_type = obj.film_type;
                        objUpdate.episode = obj.episode;
                        objUpdate.episode_current = obj.episode_current;
                        objUpdate.contract_copyright = obj.contract_copyright;
                        objUpdate.contract_exprired = DateTime.Now;
                        objUpdate.contract_appendix = obj.contract_appendix;
                        objUpdate.copyright_appendix = obj.copyright_appendix;
                        if (obj.copyright_expired != null)
                        {
                            objUpdate.copyright_expired = obj.copyright_expired;
                        }
                        objUpdate.catalog_id = obj.catalog_id;

                        if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                        {
                            //objUpdate.upload_file = "http://image.lalatv.com.vn/" + obj.upload_file;
                            objUpdate.upload_file = pathVideo + "smil:" + obj.upload_file + "/playlist.m3u8";
                        }
                        else { objUpdate.upload_file = obj.upload_file; }
                        //objUpdate.thumb_file = "http://image.lalatv.com.vn/" + obj.thumb_file;
                        if (obj.thumb_file != null && !obj.thumb_file.StartsWith("http"))
                        {
                            objUpdate.thumb_file = pathImage + "/smil:" + obj.thumb_file;
                        }
                        objUpdate.exclusive = obj.exclusive;
                        objUpdate.price = obj.price;
                        objUpdate.status = obj.status;
                        objUpdate.cpid = obj.cpid;
                        objUpdate.tags = obj.tags;
                        objUpdate.code = obj.code;
                        int a = db.Update(objUpdate);
                        if (a > 0) return obj.Id;
                        return 0;
                    }
                    return -1;
                }
                else
                {
                    //var queryCount = db.From<film_video>()
                    //	.Where(e => e.   == obj.title).Select(e => e.Id);
                    //var objCount = db.Count(queryCount);
                    //if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.title = obj.title;
                    objUpdate.desc = obj.desc;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.userid = comm.GetUserId();
                    objUpdate.seriid = comm.GetUserId();
                    objUpdate.imdb = obj.imdb;
                    objUpdate.format = obj.format;
                    objUpdate.sub_type = obj.sub_type;
                    objUpdate.publish_year = obj.publish_year;
                    objUpdate.publish_countryid = obj.publish_countryid;
                    objUpdate.duration = obj.duration;
                    objUpdate.actor = obj.actor;
                    objUpdate.director = obj.director;
                    objUpdate.film_type = obj.film_type;
                    objUpdate.episode = obj.episode;
                    objUpdate.episode_current = obj.episode_current;
                    objUpdate.contract_copyright = obj.contract_copyright;
                    objUpdate.contract_exprired = DateTime.Now;
                    objUpdate.contract_appendix = obj.contract_appendix;
                    //objUpdate.copyright_appendix = obj.copyright_appendix;
                    if (obj.copyright_expired != null)
                    {
                        objUpdate.copyright_expired = obj.copyright_expired;
                    }
                    objUpdate.catalog_id = obj.catalog_id;
                    //objUpdate.upload_file = "http://image.lalatv.com.vn/" + obj.upload_file;

                    if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                    {
                        //objUpdate.upload_file = "http://image.lalatv.com.vn/" + obj.upload_file;
                        objUpdate.upload_file = pathVideo + "smil:" + obj.upload_file + "/playlist.m3u8";
                    }
                    else { objUpdate.upload_file = obj.upload_file; }
                    if (obj.thumb_file != null && !obj.thumb_file.StartsWith("http"))
                    {
                        objUpdate.thumb_file = pathImage + "smil:" + obj.thumb_file;
                    }

                    objUpdate.exclusive = obj.exclusive;
                    objUpdate.price = obj.price;
                    objUpdate.status = obj.status;
                    objUpdate.cpid = obj.cpid;
                    objUpdate.tags = obj.tags;
                    objUpdate.code = obj.code;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int update_video(film_video obj, string pathVideo)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();

                    if (objUpdate != null)
                    {
                        if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                        {
                            //objUpdate.upload_file = "http://image.lalatv.com.vn/" + obj.upload_file;
                            objUpdate.upload_file = pathVideo + "smil:" + obj.upload_file + "/playlist.m3u8";
                        }
                        else { objUpdate.upload_file = obj.upload_file; }

                        objUpdate.status = obj.status;
                        int a = db.Update(objUpdate);
                        if (a > 0) return obj.Id;
                        return 0;
                    }
                    return -1;
                }
                else
                {
                    var StringFile = pathVideo + "smil:/cong/" + obj.upload_file + "/playlist.m3u8";
                    var nameFile = db.From<film_video>().Where(e => e.upload_file == StringFile).Select(e => e.Id);

                    var objCount = db.Count(nameFile);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();

                    if (obj.upload_file != null && !obj.upload_file.StartsWith("http"))
                    {
                        objUpdate.upload_file = pathVideo + "smil:/cong/" + obj.upload_file + "/playlist.m3u8";
                    }
                    else { objUpdate.upload_file = obj.upload_file; }

                    objUpdate.cpid = comm.GetCPId();
                    objUpdate.status = obj.status;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }

            }
        }
        public int UpdateStatus(film_video obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_video>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.status = obj.status;
                        return db.Update(objUpdate);
                    }

                }
            }
            return -1;
        }
        public int UpdateCheckbox(int[] catalog_id, int filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //xóa hết catalog id của film id
                var query = db.From<film_catalog_film>().Where(e => e.filmid == filmid);
                db.Delete(query);

                //lặp qua mảng id
                foreach (var catalogid in catalog_id)
                {
                    var obj = new film_catalog_film();
                    obj.filmid = filmid;
                    obj.catalogid = catalogid;
                    db.Insert<film_catalog_film>(obj);
                }
                return -1;
            }
        }
        public int UpdateImage(string url, int filmid, string pathImage)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //inset img vào film_video_image
                var query = db.From<film_video_image>().Where(e => e.filmid == filmid);
                var obj = new film_video_image();
                obj.url = pathImage + url;
                obj.filmid = filmid;
                db.Insert<film_video_image>(obj);
                // Console.WriteLine("hello world.1..");

                var query1 = db.From<film_video>().Where(e => e.Id == filmid);
                // Console.WriteLine("hello world.2..");
                var objUpdate = db.Select(query1).SingleOrDefault();
                if (objUpdate.thumb_file == null)
                {
                    objUpdate.thumb_file = pathImage + url;
                    db.Update<film_video>(objUpdate);
                }
                // neu thumb_file != null thi xoa tat ca thumb_file trong film_video di roi update file url moi hien thi trong anh dai dien giao dien admin
                else
                {
                    var delete = db.From<film_video>().Where(e => e.thumb_file == url);
                    db.Delete(delete);
                    objUpdate.thumb_file = pathImage + url;
                    db.Update<film_video>(objUpdate);
                }
            }
            return -1;

        }
        public film_cp getcpScan(int cpid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                //var query = db.From<film_cp>().Where(e => e.scan == id);
                //if (comm.IsCP())
                var query = db.From<film_cp>();
                query = query.Where(e => e.Id == cpid);
                return db.Select(query).SingleOrDefault();
            }
        }
        public List<film_video_image> getupdateImg(int Filmid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.filmid == Filmid);
                return db.Select(query).ToList<film_video_image>();
            }
        }
        public int deteleimage(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video_image>().Where(e => e.Id == id);
                return db.Delete(query);
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_video>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_video InitEmpty()
        {
            var obj = new film_video();
            obj.Id = 0;
            obj.thumb_file = "";
            obj.upload_file = "";

            return obj;
        }
        /*
		genvie here
		create view vw_film_video
		as
			select film_video.Id,film_video.title,film_video.desc,film_video.datecreated,film_video.userid,film_video.seriid,film_video.imdb,film_video.format,film_video.sub_type,film_video.publish_year,film_video.publish_countryid,film_video.duration,film_video.actor,film_video.director,film_video.film_type,film_video.episode,film_video.episode_current,film_video.contract_copyright,film_video.contract_exprired,film_video.contract_appendix,film_video.copyright_appendix,film_video.copyright_expired,film_video.catalog_id,film_video.upload_file,film_video.thumb_file,film_video.exclusive,film_video.price,film_video.status,film_video.cpid   ,film_country.title as publish_countryid_title  ,film_catalog.title as catalog_id_title  ,film_cp.title as cpid_title  from film_video   inner join film_country on film_video.publish_countryid=film_country.id    inner join film_catalog on film_video.catalog_id=film_catalog.id    inner join film_cp on film_video.cpid=film_cp.id
		*/
    }
}
