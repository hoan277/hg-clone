﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_drm_service : LamltService
    {
        public List<film_drm> Getfilm_drms(PagingModel page)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_drm>();
                List<film_drm> all_film_drms = db.Select(query);
                return all_film_drms;
            }
        }
        public List<vw_film_drm> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_drm>();
                query.OrderByDescending(x => x.id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => e.name.Contains(page.search) || e.userid.ToString().Contains(page.search));
                List<vw_film_drm> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public int Delete(int drm_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_drm>().Where(x => x.id == drm_id);
                return db.Delete(query);
            }
        }
        public film_drm GetByID(int drm_id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_drm>().Where(x => x.id == drm_id);
                return db.Select(query).SingleOrDefault();
            }
        }
        public int UpdateOrInsert(film_drm objDrm)
        {
            int result = 0;
            using (var db = _connectionData.OpenDbConnection())
            {
                if (objDrm.id > 0)
                {
                    var query = db.From<film_drm>().Where(e => e.id == objDrm.id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.id = objDrm.id;
                        objUpdate.name = objDrm.name;
                        objUpdate.metadata = objDrm.metadata;
                        objUpdate.key = objDrm.key;
                        objUpdate.datecreate = objDrm.datecreate;
                        objUpdate.datesubcribe = objDrm.datesubcribe;
                        objUpdate.note = objDrm.note;
                        objUpdate.userid = objDrm.userid;
                        result = db.Update(objUpdate);
                    }
                }
                else
                {
                    var queryCount = db.From<film_drm>().Where(e => e.name == objDrm.name).Select(e => e.id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objInsert = InitEmpty();
                    objInsert.id = objDrm.id;
                    objInsert.name = objDrm.name;
                    objInsert.metadata = objDrm.metadata;
                    objInsert.key = objDrm.key;
                    objInsert.datecreate = objDrm.datecreate;
                    objInsert.datesubcribe = objDrm.datesubcribe;
                    objInsert.note = objDrm.note;
                    objInsert.userid = objDrm.userid;
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_drm>();
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                return db.Count(query);
            }
        }
        public vw_film_drm GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_drm>().Where(e => e.id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public film_drm InitEmpty()
        {
            var obj = new film_drm();
            obj.id = 0;
            return obj;
        }

    }
}
