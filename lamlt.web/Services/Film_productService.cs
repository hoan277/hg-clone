﻿using lamlt.data;
using lamlt.web.Models;
using Newtonsoft.Json;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_productService : LamltService
    {
        public film_productService()
        {
        }

        public List<film_product> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                query = query.Skip(offset).Take(limit);
                List<film_product> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public List<vw_film_product> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                query = query.Skip(offset).Take(limit);
                List<vw_film_product> rows = db.Select(query).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 10 };
            if (page.search == null) page.search = "";
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.title.Contains(page.search)));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.dateupdated >= time_start && e.dateupdated <= time_end);
                }
                return db.Count(query);
            }
        }

        public film_product GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_product GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_product GetViewById(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_product>().Where(e => e.Id == id);
                return db.Select(query).SingleOrDefault();
            }
        }

        public int UpdateOrInsert(film_product obj)
        {
            film_product_processingService film_product_processingSV = new film_product_processingService();
            film_product_processing_videoService film_product_processing_videoSV = new film_product_processing_videoService();
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_product>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.title = obj.title;
                        objUpdate.content = obj.content;
                        objUpdate.channel_id = obj.channel_id;
                        objUpdate.upload_title = obj.upload_title;
                        objUpdate.description = obj.description;
                        objUpdate.thumbnail = obj.thumbnail;
                        objUpdate.transcript = obj.transcript;
                        objUpdate.tags = obj.tags;
                        objUpdate.privacy_status = obj.privacy_status;
                        objUpdate.dateupdated = DateTime.Now;
                        objUpdate.status = obj.status;
                        objUpdate.effect_id = obj.effect_id;
                        objUpdate.scene_change_id = obj.scene_change_id;
                        objUpdate.convert_status = obj.convert_status;
                        objUpdate.path = obj.path;
                        objUpdate.code = obj.code;
                        objUpdate.upload_ftp = obj.upload_ftp;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.title = obj.title;
                    objUpdate.content = obj.content;
                    objUpdate.channel_id = obj.channel_id;
                    objUpdate.upload_title = obj.upload_title;
                    objUpdate.description = obj.description;
                    objUpdate.thumbnail = obj.thumbnail;
                    objUpdate.transcript = obj.transcript;
                    objUpdate.tags = obj.tags;
                    objUpdate.privacy_status = obj.privacy_status;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.dateupdated = DateTime.Now;
                    objUpdate.status = obj.status;
                    objUpdate.effect_id = obj.effect_id;
                    objUpdate.scene_change_id = obj.scene_change_id;
                    objUpdate.convert_status = obj.convert_status;
                    objUpdate.path = obj.path;
                    objUpdate.code = obj.code;
                    objUpdate.upload_ftp = obj.upload_ftp;
                    //if (obj.upload_ftp == 1)
                    //{
                    //    int re = CreateJsonProductFile(obj.title, obj.channel_id.Value);
                    //    if (re == -1)
                    //    {
                    //        return -1;
                    //    }
                    //}
                    int ab = (int)db.Insert(objUpdate, selectIdentity: true);
                    List<film_product_processing> list_product_processing = getListProductProcessing();
                    List<film_product_processing_video> list_product_processing_video = getListProductProcessingVideo();
                    foreach (var item in list_product_processing)
                    {
                        item.product_id = ab;
                        film_product_processingSV.UpdateOrInsert(item);
                    }
                    foreach (var item1 in list_product_processing_video)
                    {
                        item1.product_id = ab;
                        film_product_processing_videoSV.UpdateOrInsert(item1);
                    }
                    if (ab > 0)
                    {
                        // Update mã isrc cuối cùng
                        new film_isrcService().UpdateOrInsert(
                            new film_isrc()
                            {
                                current_code = int.Parse(obj.code.Replace("VNA0D" + DateTime.Now.ToString("yy"), "")),
                                product_id = ab
                            });
                        return ab;
                    }
                    return -1;
                }
            }
        }

        public List<film_product_processing> getListProductProcessing()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing>().Where(e => e.product_id == 0);
                List<film_product_processing> rows = db.Select(query)
                    .Skip(0).ToList();
                return rows;
            }
        }

        public List<vw_film_video_processing> getAllVideoProcessingByProductId(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                List<vw_film_video_processing> lstProcessing = new List<vw_film_video_processing>();
                var query = db.From<vw_film_product_processing_video>().Where(e => e.product_id == id);
                var lstProcessVideo = db.Select(query).ToList();
                if (lstProcessVideo.Count > 0)
                {
                    foreach (var item in lstProcessVideo)
                    {
                        lstProcessing.Add(db.Select(db.From<vw_film_video_processing>().Where(e => e.Id == item.video_processing_id)).SingleOrDefault());
                    }
                }
                return lstProcessing;
            }
        }

        public List<film_product_processing_video> getListProductProcessingVideo()
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product_processing_video>().Where(e => e.product_id == 0);
                List<film_product_processing_video> rows = db.Select(query).Skip(0).ToList();
                return rows;
            }
        }

        public int UpdateStatus(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product>().Where(e => e.Id == Id);
                var objUpdate = db.Select(query).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.status = 1;
                    int a = db.Update(objUpdate);
                    if (a > 0) return objUpdate.Id;
                }
                return -1;
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_product InitEmpty()
        {
            var obj = new film_product();
            obj.Id = 0;
            return obj;
        }

        // GEN KEY
        public int convert_product(PagingModel page)
        {
            vw_film_product product = GetViewById(page.product_id);
            film_product_processing_videoService film_product_processing_videoSV = new film_product_processing_videoService();
            List<vw_film_product_processing_video> list_ex = film_product_processing_videoSV.GetViewAllItemByProductId(page);
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string file_name = product.title + ".txt";
            string fileDirect = @"/home/amnhacsaigon/dotnet/hg-project/wwwroot/file_upload/";
            string mode = GetConnectionString("mode");
            if (isDev)
            {
                fileDirect = @"C:\tung_test\";
            }
            if (mode == "docker") fileDirect = "/app/wwwroot/file_upload/";
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            string filePath = fileDirect + file_name;
            film_effectService filmService = new film_effectService();
            film_effect effect = filmService.GetByID(product.effect_id.ToString());
            if (effect == null) effect = new film_effect();//fix null item

            scene_change effectScene = new scene_changeService().GetByID(product.scene_change_id.ToString());
            if (effectScene == null) effectScene = new scene_change();//fix null item
            string scene_change_upload_file = "";
            if (product.scene_change_id > 0)
            {
                var scene = new scene_changeService().GetViewByID(product.scene_change_id.ToString());
                if (scene != null)
                {
                    scene_change_upload_file = scene.upload_file;
                }
            }
            using (StreamWriter sw = new StreamWriter(filePath, false, System.Text.Encoding.UTF8))
            {
                sw.WriteLine("title:" + product.title);
                sw.WriteLine("content:" + product.content);
                sw.WriteLine("channel_title:" + product.channel_title);
                sw.WriteLine("upload_title:" + product.upload_title);
                sw.WriteLine("description:" + product.description);
                sw.WriteLine("thumbnail:" + product.thumbnail);
                sw.WriteLine("tags:" + product.tags);
                sw.WriteLine("code:" + product.code);
                sw.WriteLine("privacy_status:" + product.privacy_status);
                sw.WriteLine("scene_change_file:" + scene_change_upload_file);
                sw.WriteLine("effect_file:" + effect.file);
                for (int x = 0; x <= list_ex.Count - 1; x++)
                {
                    sw.WriteLine("video:" + list_ex[x].video_processing_upload_file);
                }
            }
            return 1;
        }

        public int UpdateByConvert(string path, int status)
        {
            // status 5: bắt đầu convert, 6: convert lỗi, 1: convert xong
            LogService.logItem("============================= /api/film_converting/update --> UpdateByConvert =============================");
            LogService.logItem("path: " + path);
            LogService.logItem("status: " + status);
            if (string.IsNullOrEmpty(path)) { return -2; };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_product>().Where(e => e.title == path.Replace(".txt", ""));
                var objUpdate = db.Select(query).LastOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.convert_status = status;
                    objUpdate.path = "http://27.76.152.255:13000/hg_project_product/" + path.Replace(".txt", "") + ".mp4";
                    //objUpdate.path = "https://files.lalatv.com.vn:9090/hg_project_product/" + path.Replace(".txt", "") + ".mp4";
                    if (objUpdate.upload_ftp == 1 && status == 2)
                    {
                        int re = CreateJsonProductFile(objUpdate.title, objUpdate.channel_id.Value);
                        LogService.logItem("trang thai ghi json: " + re);
                    }
                    int a = db.Update(objUpdate);
                    if (a > 0)
                    {
                        LogService.logItem(objUpdate.Id);
                        return objUpdate.Id;
                    };
                }
                LogService.logItem("cap nhat that bai");
                return -1;
            }
        }

        public int CreateJsonProductFile(string title, int channel_id)
        {
            film_channelService film_channelSV = new film_channelService();
            vw_film_channel channel = film_channelSV.GetViewById(channel_id);
            if (channel != null)
            {
                if (string.IsNullOrEmpty(channel.title) || string.IsNullOrEmpty(channel.ip) || string.IsNullOrEmpty(channel.password))
                {
                    return -1;
                }
                bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string fileDirect = "/home/www/data/data/saigonmusic/hg_project_json_upload_ftp/";
                if (isDev)
                {
                    fileDirect = @"C:\tung_test\";
                }
                string mode = GetConnectionString("mode");
                if (mode == "docker") fileDirect = "/app/wwwroot/hg_project_json_upload_ftp/";
                if (!Directory.Exists(fileDirect))
                {
                    Directory.CreateDirectory(fileDirect);
                }
                string file_path = fileDirect + title + ".json";
                var _data = new product_json()
                {
                    ftp_ip = channel.ip,
                    ftp_username = channel.username,
                    ftp_password = channel.password,
                    ftp_folder = "/home/www/data/data/saigonmusic/thetung_test", // from server
                    client_file = "/home/www/data/data/saigonmusic/hg_project_product/" + title + ".mp4",
                    file_name = title + ".mp4"
                };
                using (StreamWriter file = File.CreateText(file_path))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    serializer.Serialize(file, _data);
                    return 1;
                };
            }
            return -1;
        }
    }

    // EXTEND CLASS
    public class video_processing_extend
    {
        public int catalog_id { get; set; }
        public string catalog_title { get; set; }
        public List<vw_film_video_processing> list_vw_video_processing { get; set; }
        public List<film_video_processing> list_video_processing { get; set; }
    }
}