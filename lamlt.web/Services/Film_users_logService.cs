﻿using ClosedXML.Excel;
using lamlt.data;
using lamlt.web.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_users_logService : LamltService
    {
        public film_users_logService()
        {
        }

        public List<film_users_log> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            if (page.cpid == -1) { };
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_users_log>();
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.username.Contains(page.search)));

                query = query.Where(e => (e.cpid == page.cpid));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                List<film_users_log> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        // forcus
        public List<vw_film_users_log> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_users_log>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.username.Contains(page.search)));
                //sub_type
                if (page.sub_type == -1)
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                };
                if (page.sub_type != -1)
                {
                    query.Where(e => e.sub_type == page.sub_type);
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                //status
                if (page.statusUserLog == -1)
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                if (page.statusUserLog != -1)
                {
                    query = query.Where(e => e.status == page.statusUserLog);
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                List<vw_film_users_log> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }

        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<film_users_log>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.username.Contains(page.search)));
                //sub_type
                if (page.sub_type == -1)
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                };
                if (page.sub_type != -1)
                {
                    query.Where(e => e.sub_type == page.sub_type);
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                //status
                if (page.statusUserLog == -1)
                {
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                if (page.statusUserLog != -1)
                {
                    query = query.Where(e => e.status == page.statusUserLog);
                    query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                }
                return db.Count(query);
            }
        }

        public long CountViewAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "2019-01-01"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };
            if (page.sub_type == -1) { };
            if (page.sub_state == -1) { };

            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query = db.From<vw_film_users_log>();
                if (comm.IsCP()) query = query.Where(e => e.cpid == comm.GetCPId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.username.Contains(page.search)));
                if (page.cpid != -1) query = query.Where(e => e.cpid == page.cpid);
                return db.Count(query);
            }
        }

        public film_users_log GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_users_log>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        public vw_film_users_log GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_users_log>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }

        // get film_users_log_viettel
        public List<vw_film_users_log> GetVw_Film_Users_Logs_Viettel(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100000 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "01/01/2019"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("dd/MM/yyyy"); }
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "dd/MM/yyyy", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                var query = db.From<vw_film_users_log>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // search
                query = query.Where(e => (e.username.Contains(page.search)) || (e.email.Contains(page.search)) || (e.phone.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2); // start-end time
                if (page.sub_type != -1) { query.Where(e => e.sub_type == page.sub_type); } // sub_type
                if (page.sub_state != -1) { query.Where(e => e.sub_state == page.sub_state); } // sub_state
                query.Where(e => e.desc.Contains("xna") || e.desc.Contains("XNA"));
                // number vt
                string[] vietel_number = { "096", "097", "098", "032", "033", "034", "035", "036", "037", "038", "039", "086", "96", "97", "98", "32", "33", "34", "35", "36", "37", "38", "39", "86", "+8496", "+8497", "+8498", "+8432", "+8433", "+8434", "+8435", "+8436", "+8437", "+8438", "+8486" };
                List<vw_film_users_log> rows = db.Select(query).ToList();
                List<vw_film_users_log> row_last = new List<vw_film_users_log>();
                foreach (var item in rows)
                {
                    foreach (string s in vietel_number)
                    {
                        if (item.username.StartsWith(s))
                        {
                            row_last.Add(item);
                        };
                    }
                }
                return row_last;
            }
        }

        public long CountVw_Film_users_log_viettel(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "01/01/2019"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("dd/MM/yyyy"); }
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "dd/MM/yyyy", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                var query = db.From<vw_film_users_log>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // search
                query = query.Where(e => (e.username.Contains(page.search)) || (e.email.Contains(page.search)) || (e.phone.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2); // start-end time
                if (page.sub_type != -1) { query.Where(e => e.sub_type == page.sub_type); } // sub_type
                if (page.sub_state != -1) { query.Where(e => e.sub_state == page.sub_state); } // sub_state
                query.Where(e => e.desc.Contains("xna") || e.desc.Contains("XNA"));
                // number vt
                string[] vietel_number = { "096", "097", "098", "032", "033", "034", "035", "036", "037", "038", "039", "086", "96", "97", "98", "32", "33", "34", "35", "36", "37", "38", "39", "86", "+8496", "+8497", "+8498", "+8432", "+8433", "+8434", "+8435", "+8436", "+8437", "+8438", "+8486" };
                List<vw_film_users_log> rows = db.Select(query).ToList();
                List<vw_film_users_log> row_last = new List<vw_film_users_log>();
                foreach (var item in rows)
                {
                    foreach (string s in vietel_number)
                    {
                        if (item.username.StartsWith(s))
                        {
                            row_last.Add(item);
                        };
                    }
                }
                return row_last.Count;
            }
        }

        #region ===== export data =====

        // get list
        public List<vw_film_users_log> GetVw_Film_Users_Logs_Viettel_Export(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100000 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = "01/01/2019"; }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("dd/MM/yyyy"); }
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "dd/MM/yyyy", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                var query = db.From<vw_film_users_log>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 1000000;
                try { limit = page.limit; }
                catch { }
                // search
                query = query.Where(e => (e.username.Contains(page.search)) || (e.email.Contains(page.search)) || (e.phone.Contains(page.search)));
                query.Where(e => e.datecreated >= dt1 && e.datecreated <= dt2); // start-end time
                if (page.sub_type != -1) { query.Where(e => e.sub_type == page.sub_type); } // sub_type
                if (page.sub_state != -1) { query.Where(e => e.sub_state == page.sub_state); } // sub_state
                // number vt
                string[] vietel_number = { "096", "097", "098", "032", "033", "034", "035", "036", "037", "038", "086", "96", "97", "98", "32", "33", "34", "35", "36", "37", "38", "86", "+8496", "+8497", "+8498", "+8432", "+8433", "+8434", "+8435", "+8436", "+8437", "+8438", "+8486" };
                List<vw_film_users_log> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                List<vw_film_users_log> row_last = new List<vw_film_users_log>();
                foreach (var item in rows)
                {
                    foreach (string s in vietel_number)
                    {
                        if (item.username.StartsWith(s))
                        {
                            row_last.Add(item);
                        };
                    }
                }
                return row_last;
            }
        }

        // export
        public string ExportData(List<vw_film_users_log> data)
        {
            string folderPath = "/home/amnhacsaigon/dotnet/cms-lalatv-viettel-v1/wwwroot/export_viettel/";
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (isDev)
            {
                folderPath = @"D:\";
            }
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fileName = "Chi tiết Viettel " + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";

            // table 2
            DataTable dt2 = new DataTable
            {
                TableName = "Chi tiết Viettel"
            };
            dt2.Columns.Add("STT", typeof(string));
            dt2.Columns.Add("Username", typeof(string));
            dt2.Columns.Add("Email", typeof(string));
            dt2.Columns.Add("Phone", typeof(string));
            dt2.Columns.Add("Gói cước", typeof(string));
            dt2.Columns.Add("Thời gian đăng ký", typeof(string));
            dt2.Columns.Add("Thời gian gia hạn tiếp tục", typeof(string));
            dt2.Columns.Add("Thời gian tạo", typeof(string));
            dt2.Columns.Add("Trạng thái", typeof(string));
            dt2.Columns.Add("Cú pháp", typeof(string));
            int stt = 1;
            if (data.Count != 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    var sub_state_str = "";
                    switch (data[i].sub_state)
                    {
                        case 1: sub_state_str = "Đăng ký"; break;
                        case 0: sub_state_str = "Không đăng ký"; break;
                        default: sub_state_str = ""; break;
                    }

                    var sub_type_str = "";
                    switch (data[i].sub_type)
                    {
                        case 1: sub_type_str = "Gói ngày"; break;
                        case 2: sub_type_str = "Gói tuần"; break;
                        case 3: sub_type_str = "Gói tháng"; break;
                        default: sub_state_str = ""; break;
                    }
                    var date_dk = "";
                    if (data[i].desc != null)
                    {
                        string[] words = data[i].desc.Split(',');
                        string date_dk_str = words[0].Split(":")[0];
                        date_dk = date_dk_str.Substring(6, 2) + "/" + date_dk_str.Substring(4, 2) + "/" + date_dk_str.Substring(0, 4) + " " + date_dk_str.Substring(8, 2) + ":" + date_dk_str.Substring(10, 2) + ":" + date_dk_str.Substring(12, 2);
                    }
                    var date_hb = "";
                    if (data[i].date_hb != null)
                    {
                        date_hb = data[i].date_hb.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    var datecreated = "";
                    if (data[i].datecreated != null)
                    {
                        datecreated = data[i].datecreated.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    var cuphap = "Unknown";
                    if (!string.IsNullOrEmpty(data[i].desc))
                    {
                        var scuphap = data[i].desc.Split(",");
                        cuphap = !string.IsNullOrEmpty(scuphap[8]) ? scuphap[8] : "Unknown";
                    }
                    dt2.Rows.Add(stt, data[i].username, data[i].email, data[i].phone, sub_type_str,
                                date_dk, date_hb, datecreated, sub_state_str, cuphap);
                    stt++;
                }
                dt2.AcceptChanges();
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                //sheet 2
                var ws2 = wb.Worksheets.Add("Chi tiết Viettel");
                ws2.Style.Font.FontName = "Times New Roman";
                ws2.Style.Font.FontSize = 12;
                var table2 = ws2.Cell(1, 1).InsertTable(dt2);

                table2.Theme = XLTableTheme.None;
                table2.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                table2.Cells().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                ws2.Columns().AdjustToContents();

                // saves
                wb.SaveAs(folderPath + fileName);
                return "/export_viettel/" + fileName;
            }
        }

        #endregion ===== export data =====
    }
}