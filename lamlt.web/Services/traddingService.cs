﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class traddingService : LamltService
    {

        public traddingService()
        {

        }
        public List<tradding> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<tradding>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/
                List<tradding> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_tradding> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";

            //  ServiceStackHelper.Help();
            //  LicenseUtils.ActivatedLicenseFeatures();           
            //search again
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_tradding>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                query.OrderByDescending(x => x.Id);

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }
                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/


                List<vw_tradding> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<tradding>();
                //if (!comm.IsSuperAdmin()) query = query.Where(e => e.SysHotelID == comm.GetHotelId());
                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;//int.Parse(Request.Params["limit"]);
                try { limit = page.limit; }
                catch { }

                query = query.Where(e => (e.name.Contains(page.search)));
                /* 
  if(!(title == "-1") ||  string.IsNullOrEmpty(title)) query=query.Where(e=>e.title == page.title);
*/

                return db.Count(query);
            }
        }
        public tradding GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<tradding>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_tradding GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_tradding>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }



        public int UpdateOrInsert(tradding obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<tradding>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.name = obj.name;
                        objUpdate.revenue = obj.revenue;
                        objUpdate.discount = obj.discount;

                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<tradding>()
                        .Where(e => e.name == obj.name).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.name = obj.name;
                    objUpdate.revenue = obj.revenue;
                    objUpdate.discount = obj.discount;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<tradding>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public tradding InitEmpty()
        {
            var obj = new tradding();
            obj.Id = 0;
            return obj;
        }
    }
}