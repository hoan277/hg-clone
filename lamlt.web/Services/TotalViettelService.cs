﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace lamlt.web.Services
{
    public class TotalViettelService : LamltService
    {
        public List<vw_totalviettel> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            CultureInfo provider = CultureInfo.InvariantCulture;

            DateTime dtStart = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", provider);
            DateTime dtEnd = DateTime.ParseExact(page.end_time, "yyyy-MM-dd", provider);

            DateTime dtTmp = dtStart;
            List<string> lst = new List<string>();

            while (dtTmp.CompareTo(dtEnd) <= 0)
            {
                lst.Add(dtTmp.ToString("yyyy-MM-dd"));
                dtTmp = dtTmp.AddDays(1);
            }

            TotalViettelService svrtotal = (new TotalViettelService());
            List<vw_totalviettel> lst_result = new List<vw_totalviettel>();

            using (var db = _connectionData.OpenDbConnection())
            {
                foreach (string s in lst)
                {
                    int result = new TotalViettelService().UpdateOrInsert(s);
                    List<vw_totalviettel> data = svrtotal.GetViewAllItem_Range(new PagingModel() { offset = 0, limit = 50, search = "", end_time = s, start_time = s });
                    lst_result.AddRange(data);
                }
                return lst_result;
            }

        }
        public List<vw_totalviettel> GetViewAllItem_Range(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (string.IsNullOrEmpty(page.start_time))
            {
                { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            }

            if (string.IsNullOrEmpty(page.end_time))
            {
                { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            }
            using (var db = _connectionData.OpenDbConnection())
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                DateTime date = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", provider);
                DateTime date1 = DateTime.ParseExact(page.end_time, "yyyy-MM-dd", provider);
                var query = db.From<vw_totalviettel>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                if ((!string.IsNullOrEmpty(page.start_time)) && (!string.IsNullOrEmpty(page.end_time)))
                {
                    query.Where(e => (e.date >= date && e.date <= date1));
                }
                List<vw_totalviettel> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null)
            { page.search = ""; }
            if (page.start_time == null)
            { page.start_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.end_time == null)
            { page.end_time = DateTime.Now.ToString("yyyy-MM-dd"); }
            if (page.cpid == -1) { };

            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(page.start_time, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(page.end_time, "yyyy-MM-dd", null);
                var query = db.From<vw_totalviettel>();
                if (comm.IsCP()) query = query.Where(e => e.Id == comm.GetCPId());

                int offset = 0; try { offset = page.offset; }
                catch { }

                int limit = 10;
                try { limit = page.limit; }
                catch { }
                query.Where(e => e.date >= dt1 && e.date <= dt2);
                return db.Count(query);
            }
        }
        public int UpdateOrInsert(string s)
        {
            int rs;
            using (var db = _connectionData.OpenDbConnection())
            {
                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);
                var query1 = db.From<film_users_log>().Where(e => e.datecreated >= dt1 && e.datecreated <= dt2);
                var dt = db.Count(query1);
                //var timeinput = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                //  var timeout = DateTime.ParseExact(s + " 21:00:00", "yyyy - MM - dd HH: mm:ss", null);

                if (dt > 0)
                {
                    TotalViettelService tvService = (new TotalViettelService());
                    DateTime oDate = Convert.ToDateTime(s);
                    var query = db.From<vw_totalviettel>()
                        .Where(e => (e.date == dt1));
                    var objUpdate = db.Select(query).FirstOrDefault();

                    if (objUpdate != null)
                    {
                        //var delete = db.From<totalviettel>().Where(e => e.date == dt1);
                        //db.Delete(delete);
                        objUpdate.total = (int)tvService.total(s);
                        return (int)db.Update(objUpdate);
                    }
                    else
                    {
                        objUpdate = InitEmpty();
                        objUpdate.date = oDate;
                        objUpdate.total = (int)tvService.total(s);
                        rs = (int)db.Insert(objUpdate, selectIdentity: true);
                        return rs;

                    }
                }
                else
                {
                    return -1;
                }
            }
        }
        public int total(string s)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var gia1 = 3000; // gói ngày
                var gia2 = 10000; // gói tuần
                var gia3 = 30000; // gói tháng

                DateTime dt1 = DateTime.ParseExact(s, "yyyy-MM-dd", null);
                DateTime dt2 = DateTime.ParseExact(s + " 23:59:59", "yyyy-MM-dd HH:mm:ss", null);

                var query = db.From<film_users_log>().Where(e => e.sub_type == 1 && e.datecreated >= dt1 && e.datecreated <= dt2);
                var query1 = db.From<film_users_log>().Where(e => e.sub_type == 2 && e.datecreated >= dt1 && e.datecreated <= dt2);
                var query2 = db.From<film_users_log>().Where(e => e.sub_type == 5 && e.datecreated >= dt1 && e.datecreated <= dt2);
                long price1 = db.Count(query); // goi ngay 2000
                long price2 = db.Count(query1); // goi ngay 3000
                long price3 = db.Count(query2); // goi tuan 5000
                int sum1 = Convert.ToInt32(price1) * gia1;
                int sum2 = Convert.ToInt32(price2) * gia2;
                int sum3 = Convert.ToInt32(price3) * gia3;
                int total = sum1 + sum2 + sum3;

                return total;
            }
        }
        public vw_totalviettel InitEmpty()
        {
            var obj = new vw_totalviettel();
            obj.Id = 0;
            return obj;
        }
    }
}
