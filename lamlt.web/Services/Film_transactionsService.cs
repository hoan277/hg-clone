﻿using lamlt.data;
using lamlt.web.Models;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_transactionsService : LamltService
    {

        public film_transactionsService()
        {

        }
        public List<film_transactions> GetAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.search == null) page.search = "";
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactions>();
                query.OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                List<film_transactions> rows = db.Select(query)
                    .Skip(offset).Take(limit).ToList();
                return rows;
            }
        }
        public List<vw_film_transactions> GetViewAllItem(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.limit == 0) { page.limit = 10000; };
            if (page.search == null) page.search = "";
            if (page.arr_status == "null") { page.arr_status = ""; }
            if (page.arr_typeName == "null") { page.arr_typeName = ""; }
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transactions>().OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // title
                query = query.Where(e => e.txtid.Contains(page.search) || e.phone.Contains(page.search) || e.user.Contains(page.search) || e.username.Contains(page.search) || e.txtHash1.Contains(page.search));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.datecreated >= time_start && e.datecreated <= time_end);
                }
                // status
                if (!string.IsNullOrEmpty(page.arr_status))
                {
                    string[] arrstatus = page.arr_status.Split(",");
                    query = query.Where(e => Sql.In(e.statusBanking.ToString(), arrstatus));
                }
                // typeName
                if (!string.IsNullOrEmpty(page.arr_typeName))
                {
                    string[] arr_typeName = page.arr_typeName.Split(",");
                    query = query.Where(e => Sql.In(e.typeName.ToString(), arr_typeName));
                }
                query = query.Where(e => e.txtHash1 != "msidn");
                query = query.Skip(offset).Take(limit);
                List<vw_film_transactions> rows = db.Select(query);
                return rows;
            }
        }
        public long CountAll(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.limit == 0) { page.limit = 10000; };
            if (page.search == null) page.search = "";
            if (page.arr_status == "null") { page.arr_status = ""; }
            if (page.arr_typeName == "null") { page.arr_typeName = ""; }
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transactions>().OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // title
                query = query.Where(e => e.txtid.Contains(page.search) || e.phone.Contains(page.search) || e.user.Contains(page.search) || e.username.Contains(page.search));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.datecreated >= time_start && e.datecreated <= time_end);
                }
                // status
                if (!string.IsNullOrEmpty(page.arr_status))
                {
                    string[] arrstatus = page.arr_status.Split(",");
                    query = query.Where(e => Sql.In(e.statusBanking.ToString(), arrstatus));
                }
                // typeName
                if (!string.IsNullOrEmpty(page.arr_typeName))
                {
                    string[] arr_typeName = page.arr_typeName.Split(",");
                    query = query.Where(e => Sql.In(e.typeName.ToString(), arr_typeName));
                }
                query = query.Where(e => e.txtHash1 != "msidn");
                return db.Count(query);
            }
        }
        public film_transactions GetByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactions>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public vw_film_transactions GetViewByID(string id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transactions>().Where(e => e.Id == int.Parse(id));
                return db.Select(query).SingleOrDefault();
            }
        }
        public int quatity(int id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                // var query = db.Count<id>.From<film_video>().Where(e => e.catalog_id == id);
                // return db.Update(query);

            }
            return -1;
        }
        public int UpdateOrInsert(film_transactions obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {

                    var query = db.From<film_transactions>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.txtid = obj.txtid;
                        objUpdate.address = obj.address;
                        objUpdate.amount1 = obj.amount1;
                        objUpdate.amount2 = obj.amount2;
                        objUpdate.currency1 = obj.currency1;
                        objUpdate.currency2 = obj.currency2;
                        objUpdate.status_url = obj.status_url;
                        objUpdate.qrcore_url = obj.qrcore_url;
                        objUpdate.datecreated = DateTime.Now;
                        objUpdate.idRef = obj.idRef;
                        objUpdate.keyRef = obj.keyRef;
                        objUpdate.typeTrans = obj.typeTrans;
                        objUpdate.rateUSD1 = obj.rateUSD1;
                        objUpdate.rateUSD2 = obj.rateUSD2;
                        objUpdate.addressWS = obj.addressWS;
                        objUpdate.memberid = obj.memberid;
                        objUpdate.txthash = obj.txthash;
                        objUpdate.statusWS1 = obj.statusWS1;
                        objUpdate.txtHashFee = obj.txtHashFee;
                        objUpdate.statusFee = obj.statusFee;
                        objUpdate.memberidTo = obj.memberidTo;
                        objUpdate.addressTo = obj.addressTo;
                        objUpdate.addressFrom = obj.addressFrom;
                        objUpdate.typeName = obj.typeName;
                        objUpdate.txtHash1 = obj.txtHash1;
                        objUpdate.username = obj.username;
                        objUpdate.note = obj.note;
                        objUpdate.bankid = obj.bankid;
                        objUpdate.statusBanking = obj.statusBanking;
                        objUpdate.fullname = obj.fullname;
                        objUpdate.phone = obj.phone;
                        objUpdate.email = obj.email;
                        objUpdate.urlTemp = obj.urlTemp;
                        objUpdate.statusBankingResult = obj.statusBankingResult;
                        objUpdate.orderIdRef = obj.orderIdRef;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var queryCount = db.From<film_transactions>()
                        .Where(e => e.txtid == obj.txtid).Select(e => e.Id);
                    var objCount = db.Count(queryCount);
                    if (objCount > 0) return comm.ERROR_EXIST;
                    var objUpdate = InitEmpty();
                    //objUpdate.Id = obj.Id;
                    objUpdate.txtid = obj.txtid;
                    objUpdate.address = obj.address;
                    objUpdate.amount1 = obj.amount1;
                    objUpdate.amount2 = obj.amount2;
                    objUpdate.currency1 = obj.currency1;
                    objUpdate.currency2 = obj.currency2;
                    objUpdate.status_url = obj.status_url;
                    objUpdate.qrcore_url = obj.qrcore_url;
                    objUpdate.datecreated = DateTime.Now;
                    objUpdate.idRef = obj.idRef;
                    objUpdate.keyRef = obj.keyRef;
                    objUpdate.typeTrans = obj.typeTrans;
                    objUpdate.rateUSD1 = obj.rateUSD1;
                    objUpdate.rateUSD2 = obj.rateUSD2;
                    objUpdate.addressWS = obj.addressWS;
                    objUpdate.memberid = obj.memberid;
                    objUpdate.txthash = obj.txthash;
                    objUpdate.statusWS1 = obj.statusWS1;
                    objUpdate.txtHashFee = obj.txtHashFee;
                    objUpdate.statusFee = obj.statusFee;
                    objUpdate.memberidTo = obj.memberidTo;
                    objUpdate.addressTo = obj.addressTo;
                    objUpdate.addressFrom = obj.addressFrom;
                    objUpdate.typeName = obj.typeName;
                    objUpdate.txtHash1 = obj.txtHash1;
                    objUpdate.username = obj.username;
                    objUpdate.note = obj.note;
                    objUpdate.bankid = obj.bankid;
                    objUpdate.statusBanking = obj.statusBanking;
                    objUpdate.fullname = obj.fullname;
                    objUpdate.phone = obj.phone;
                    objUpdate.email = obj.email;
                    objUpdate.urlTemp = obj.urlTemp;
                    objUpdate.statusBankingResult = obj.statusBankingResult;
                    objUpdate.orderIdRef = obj.orderIdRef;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }
        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactions>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }
        public film_transactions InitEmpty()
        {
            var obj = new film_transactions();
            obj.Id = 0;
            return obj;
        }
        /*
		genvie here
		create view vw_film_transactions
		as
			select film_transactions.Id,film_transactions.title,film_transactions.desc,film_transactions.datecreated,film_transactions.userid,film_transactions.catalogid   ,film_transactions.title as catalogid_title  from film_transactions   inner join film_transactions on film_transactions.catalogid=film_transactions.id
		*/
        public film_transactions GetByTxtId(string txtid)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_transactions>().Where(e => e.txtid == txtid);
                return db.Select(query).FirstOrDefault();
            }
        }

        public double GetSumAmount(PagingModel page)
        {
            if (page == null) page = new PagingModel() { offset = 0, limit = 100 };
            if (page.limit == 0) { page.limit = 10000; };
            if (page.search == null) page.search = "";
            if (page.arr_status == "null") { page.arr_status = ""; }
            if (page.arr_typeName == "null") { page.arr_typeName = ""; }
            if (page.arr_search_date == null) { }
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<vw_film_transactions>().OrderByDescending(x => x.Id);
                int offset = 0; try { offset = page.offset; }
                catch { }
                int limit = 10;
                try { limit = page.limit; }
                catch { }
                // title
                query = query.Where(e => e.txtid.Contains(page.search) || e.phone.Contains(page.search) || e.user.Contains(page.search) || e.username.Contains(page.search) || e.txtHash1.Contains(page.search));
                // timerange
                if (!string.IsNullOrEmpty(page.arr_search_date))
                {
                    string[] arrtime = page.arr_search_date.Replace(" ", "").Split("-");
                    DateTime time_start = DateTime.ParseExact(arrtime[0] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime time_end = DateTime.ParseExact(arrtime[1] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                    query = query.Where(e => e.datecreated >= time_start && e.datecreated <= time_end);
                }
                // status
                if (!string.IsNullOrEmpty(page.arr_status))
                {
                    string[] arrstatus = page.arr_status.Split(",");
                    query = query.Where(e => Sql.In(e.statusBanking.ToString(), arrstatus));
                }
                // typeName
                if (!string.IsNullOrEmpty(page.arr_typeName))
                {
                    string[] arr_typeName = page.arr_typeName.Split(",");
                    query = query.Where(e => Sql.In(e.typeName.ToString(), arr_typeName));
                }
                query = query.Where(e => e.txtHash1 != "msidn");
                query = query.Skip(offset).Take(limit);
                List<vw_film_transactions> rows = db.Select(query);
                double total = rows.Sum(e => e.amount1.Value);
                return total;
            }
        }
    }
}