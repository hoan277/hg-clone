﻿using lamlt.data;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace lamlt.web.Services
{
    public class film_isrcService : LamltService
    {
        public film_isrcService()
        {
        }

        public List<film_isrc> get(int id, int product_id, int curent_code)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<film_isrc>();
                if (id > 0) query = query.Where(e => e.Id == id);
                if (product_id > 0) query = query.Where(e => e.product_id == product_id);
                if (curent_code > 0) query = query.Where(e => e.current_code == curent_code);
                query = query.OrderByDescending(e => e.Id);
                return db.Select(query).ToList();
            }
        }

        public int UpdateOrInsert(film_isrc obj)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                if (obj.Id > 0)
                {
                    var query = db.From<film_isrc>().Where(e => e.Id == obj.Id);
                    var objUpdate = db.Select(query).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = obj.Id;
                        objUpdate.product_id = obj.product_id;
                        objUpdate.current_code = obj.current_code;
                        return db.Update(objUpdate);
                    }
                    return -1;
                }
                else
                {
                    var objUpdate = InitEmpty();
                    objUpdate.product_id = obj.product_id;
                    objUpdate.current_code = obj.current_code;
                    return (int)db.Insert(objUpdate, selectIdentity: true);
                }
            }
        }

        public int Delete(int Id)
        {
            using (var db = _connectionData.OpenDbConnection())
            {
                var query = db.From<ads>().Where(e => e.Id == Id);
                return db.Delete(query);
            }
        }

        public film_isrc InitEmpty()
        {
            var obj = new film_isrc();
            obj.Id = 0;
            return obj;
        }
    }
}