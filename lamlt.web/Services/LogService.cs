﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace lamlt.web.Services
{
    public class LogService
    {
        public static void logItem(string content, string file = "log")
        {
            file = "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            using (StreamWriter w = new StreamWriter(file, true, Encoding.UTF8))
            {
                w.WriteLine(DateTime.Now.ToString("HH:mm:ss") + "\t" + content);
            }
        }
        public static void logItem(Object obj)
        {
            var method = new StackTrace().GetFrame(1).GetMethod().Name;
            string file = "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            using (StreamWriter w = new StreamWriter(file, true, Encoding.UTF8))
            {
                w.WriteLine("============================= " + DateTime.Now.ToString("HH:mm:ss") + " ============================= function: " + method + " =============================");
                w.WriteLine(nameof(obj) + ":\n" + JsonConvert.SerializeObject(obj, Formatting.Indented));
            }
        }
    }
}
