﻿using SmartIT.Employee.MockDB;
using System.Collections.Generic;

namespace EmployeeJquery.Ui
{
    public class ServiceDB
    {
        public List<Employee> getAll()
        {
            List<Employee> lst = new List<Employee>();
            for (int jx = 0; jx <= 10; jx++)
            {
                lst.Add(new Employee() { Id = 1, DepartmentId = 1, Name = "Employee" + jx.ToString() });
            }
            return lst;
        }
    }
}
