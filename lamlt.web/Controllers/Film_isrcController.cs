﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.Formula.Functions;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/Film_isrc")]
    public class Film_isrcController : Controller
    {
        [HttpGet]
        [Route("~/film_isrc/get")]
        public JsonResult get(int id, int product_id, int current_code)
        {
            var obj = new film_isrcService().get(id, product_id, current_code);
            return Json(obj);
        }
    }
}