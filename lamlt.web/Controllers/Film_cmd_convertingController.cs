﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_cmd_converting")]
    public class film_cmd_convertingController : Controller
    {
        // GET: film_cmd_convertings
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_cmd_converting/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_cmd_convertingService svrfilm_cmd_converting = (new film_cmd_convertingService());
            List<film_video_processing> data = svrfilm_cmd_converting.GetFilm_Videos(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_cmd_converting.CountAllVideo(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            //try { draw = int.Parse(valueDraw); } catch { }
            //return PartialView("List", data);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_cmd_converting/Edit/{id}")]
        public PartialViewResult film_cmd_convertingEdit([FromRoute] string id)
        {
            film_cmd_converting obj = (new film_cmd_convertingService()).GetByID(id);
            if (obj == null) obj = (new film_cmd_convertingService()).InitEmpty();
            return PartialView("film_cmd_convertingEdit", obj);
        }
        [HttpPost]
        [Route("~/film_cmd_converting/Update")]
        public ActionResult Update(string[] idconverting, string[] listVideo)
        {
            var service = new film_cmd_convertingService();
            film_cmd_converting obj = service.InitEmpty();
            obj.idconverting = string.Join(" ", idconverting);
            var myArray = new List<string>();
            for (int i = 0; i < idconverting.Length; i++)
            {
                myArray.Add(idconverting[i]);
            }
            myArray.Add("i confirm to convert...");
            string sDirTo = @"/home/www/data/data/saigonmusic/content_origin/";
            for (int i = 0; i < listVideo.Length; i++)
            {
                film_video_processing film_Video = new film_video_processingService().GetByID(listVideo[i]);
                string[] arr_file_name = film_Video.upload_file.Split('/');
                string title_film = arr_file_name[arr_file_name.Length - 1];
                title_film = title_film.Split('.')[0];
                string locdau_title = service.LocDau(title_film);//film_Video.title
                string locdau_1 = locdau_title;//.ToLower();
                //locdau_1 = locdau_1.Replace(" ", "");
                locdau_1 = locdau_1.Replace("|", "_");
                locdau_1 = locdau_1.Replace(":", "_");
                locdau_1 = locdau_1.Replace("/", "_");
                locdau_1 = locdau_1.Replace("?", "_");
                locdau_1 = locdau_1.Replace("*", "_");
                locdau_1 = locdau_1.Replace("<", "_");
                locdau_1 = locdau_1.Replace(">", "_");

                string path = sDirTo + locdau_1 + ".txt";

                if (myArray.Contains("+lalatv")) path = sDirTo + locdau_1 + ".txt.lalatv";
                if (myArray.Contains("+live")) path = sDirTo + locdau_1 + ".txt.live";

                //id suy ra title
                //biến title ko dấu
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.ASCII))
                {
                    for (int x = 0; x <= myArray.Count - 1; x++)
                    {
                        sw.WriteLine(myArray[x]);
                    }
                }
            }
            int result = service.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/film_cmd_converting/UpdateOutside")]
        public ActionResult UpdateOutside(string[] idconverting, string[] listVideo)
        {
            var service = new film_cmd_convertingService();
            film_cmd_converting obj = service.InitEmpty();
            obj.idconverting = string.Join(" ", idconverting);
            var myArray = new List<string>();
            for (int i = 0; i < idconverting.Length; i++)
            {
                myArray.Add(idconverting[i]);
            }
            myArray.Add("i confirm to convert...");
            //var myArray = myList.ToArray();
            string sDirTo = @"/home/www/data/data/saigonmusic/wait/all.txt";
            using (StreamWriter sw = new StreamWriter(sDirTo, false, System.Text.Encoding.ASCII))
            {
                for (int x = 0; x <= myArray.Count - 1; x++)
                {
                    sw.WriteLine(myArray[x]);
                }
            }
            /*
            for (int i = 0; i < listVideo.Length; i++)
            {
                //string path = "/home/www/data/data/saigonmusic/wait/" + listVideo[i] + ".txt";
                film_video_processing film_Video = new film_video_processingService().GetByID(listVideo[i]);
                string[] arr_file_name = film_Video.upload_file.Split('/');
                string title_film = arr_file_name[arr_file_name.Length - 1];
                title_film = title_film.Split('.')[0];
                string locdau_title = service.LocDau(title_film);//film_Video.title
                string locdau_1 = locdau_title;//.ToLower();
                //locdau_1 = locdau_1.Replace(" ", "");
                locdau_1 = locdau_1.Replace("|", "_");
                locdau_1 = locdau_1.Replace(":", "_");
                locdau_1 = locdau_1.Replace("/", "_");
                locdau_1 = locdau_1.Replace("?", "_");
                locdau_1 = locdau_1.Replace("*", "_");
                locdau_1 = locdau_1.Replace("<", "_");
                locdau_1 = locdau_1.Replace(">", "_");
                string path = sDirTo + locdau_1 + ".txt";
                //id suy ra title
                //biến title ko dấu
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.ASCII))
                {
                    for (int x = 0; x <= myArray.Count - 1; x++)
                    {
                        sw.WriteLine(myArray[x]);
                    }
                }
            }
            */
            int result = service.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_cmd_converting/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_cmd_convertingService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_cmd_converting/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_cmd_converting obj = new film_cmd_convertingService().GetByID(id);
            return PartialView("film_cmd_convertingDetail", obj);
        }
        [HttpGet]
        [Route("~/film_cmd_converting/GetViewDetail/{id}")]
        public PartialViewResult film_cmd_convertingDetail(string id)
        {
            vw_film_cmd_converting obj = new film_cmd_convertingService().GetViewByID(id);
            return PartialView("film_cmd_convertingDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_cmd_converting/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_cmd_convertingService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        public static string getValueInURL(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }
        [HttpPost]
        [Route("~/CreateLiveStream")]
        public int CreateLiveStream(int id, string link, string mp4, string key)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string path = "/home/amnhacsaigon/livestream/";
            if (isDev)
            {
                path = "C://";
            }
            string file_name = path + id + "_" + mp4 + ".txt";
            string content = "./ffmpeg -re -stream_loop -1 -i " + link + " -b:v:1 2500k -r:v:1 30 -c copy -vcodec libx264 -f flv -s 1280x720 rtmp://a.rtmp.youtube.com/live2/" + key;
            using (StreamWriter sw = new StreamWriter(file_name, true, Encoding.ASCII))
            {
                sw.Write(content.Replace("'", "\""));
                try
                {
                    using (Process p = new Process())
                    {
                        p.StartInfo.UseShellExecute = false;
                        p.StartInfo.CreateNoWindow = true;
                        p.StartInfo.RedirectStandardOutput = true;
                        p.StartInfo.FileName = "/home/ubuntu/bin/ffmpeg";
                        p.StartInfo.Arguments = content.Replace("'", "\"");
                        p.Start();
                        p.WaitForExit();
                        p.StandardOutput.ReadToEnd();
                        return p.Id;
                    }
                }
                catch (Exception ex)
                {
                    LogService.logItem(ex.Message);
                }
                return 1;
            }

        }
        [HttpPost]
        [Route("~/CreatePlaylist")]
        public int CreatePlaylist(string[] playlist, string key)
        {
            string path = "/home/amnhacsaigon/livestream/";
            string file_name = path + "all.playlist";
            using (StreamWriter sw = new StreamWriter(file_name, true, Encoding.ASCII))
            {
                foreach (string item in playlist)
                {
                    sw.WriteLine("file '" + item + "'");
                }
                sw.Flush();
                sw.Close();
            }
            string content = "./ffmpeg -re -stream_loop -1 -f concat -safe 0 -protocol_whitelist file,http,https,tcp,tls,crypto -i " + file_name + " -b:v:1 2500k -r:v:1 30 -c copy -vcodec libx264 -f flv -s 1280x720 rtmp://a.rtmp.youtube.com/live2/" + key;
            using (StreamWriter sw = new StreamWriter(path + "play.list", true, Encoding.ASCII))
            {
                sw.Write(content.Replace("'", "\""));
                sw.Close();
                return 1;
            }
        }

        //[HttpGet]
        //[Route("~/GetLiveStreaming")]
        //public JsonResult GetLiveStreaming()
        //{
        //    LogService.logItem("hoan");
        //    LogService.logItem(new film_cmd_convertingService().GetLiveStreaming());
        //    return Json(new film_cmd_convertingService().GetLiveStreaming());
        //}
        [HttpPost]
        [Route("~/KillProcessLive")]
        public int KillProcessLive(string file_txt, string file_process, string process_id)
        {
            return new film_cmd_convertingService().KillProcessLive(file_txt, file_process, process_id);
        }


        #region =====Player=====
        [HttpGet]
        [Route("~/film_cmd_converting/play_stream/{id}")]
        public PartialViewResult film_cmd_convertingPlayer(string id)
        {
            vw_film_video_processing video_Processing = new film_video_processingService().GetViewByID(id);
            return PartialView("film_cmd_convertingPlayer", video_Processing);
        }
        #endregion =====Player=====
    }
}
