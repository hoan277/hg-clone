﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video_view")]
    public class film_video_viewController : Controller
    {
        // GET: film_video_views
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_video_view/List")]
        public ActionResult List(int length, int start, string search, int film_id, string range_time)
        {
            film_video_viewService svFilmView = new film_video_viewService();
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                range_time = range_time,
                film_id = film_id
            };
            List<vw_film_video_view> data = svFilmView.GetViewAllItem(page);
            int recordsTotal = (int)svFilmView.CountAll(page);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpGet]
        [Route("~/film_video_view/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video_view obj = (new film_video_viewService()).GetByID(id);
            if (obj == null) obj = (new film_video_viewService()).InitEmpty();
            return PartialView("film_video_viewEdit", obj);
        }
        [HttpPost]
        [Route("~/film_video_view/Update")]
        public ActionResult Update(film_video_view obj)
        {
            int result = new film_video_viewService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/film_video_view/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video_view obj = new film_video_viewService().GetByID(id);
            return PartialView("film_video_viewDetail", obj);
        }

        [HttpGet]
        [Route("~/film_video_view/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_video_view obj = new film_video_viewService().GetViewByID(id);
            return PartialView("film_video_viewDetail", obj);
        }

        [HttpDelete]
        [Route("~/film_video_view/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_video_viewService().Delete(int.Parse(id));
            return Json(new { result = result });
        }


        #region ===== other =====

        [HttpGet]
        [Route("~/film_video_view/list_video")]
        public PartialViewResult viewVideo()
        {
            return PartialView("viewVideo");
        }

        [HttpGet]
        [Route("~/view_video/List")]
        public ActionResult ListView(int length, int start, string search, int status, int film_type, int catalogid, string range_time)
        {
            film_video_viewService film_viewSV = new film_video_viewService();
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                status = status,
                film_type = film_type,
                catalogid = catalogid,
                range_time = range_time
            };
            List<view_video> data = film_viewSV.GetVideoView(page);
            int recordsTotal = (int)film_viewSV.CountVideoView(page);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }


        [HttpGet]
        [Route("~/film_video_view/chart")]
        public PartialViewResult chart_video_view()
        {
            return PartialView("chart_video_view");
        }

        [HttpPost]
        [Route("~/view_video/export")]
        public ActionResult export(string type, int length, int start, string search, int status, int film_type, int catalogid, string range_time)
        {
            film_video_viewService view_videoSV = new film_video_viewService();
            // check exit
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = 10000,
                search = search,
                status = status,
                film_type = film_type,
                catalogid = catalogid,
                range_time = range_time
            };
            List<view_video> list_view_video = new List<view_video>();
            if (!string.IsNullOrEmpty(type))
            {
                if (type == "all")
                {
                    list_view_video = view_videoSV.GetVideoView(page);
                }
            }
            string result = view_videoSV.export_data(list_view_video);
            return Json(new { result = result });
        }

        #endregion ===== other =====
    }
}