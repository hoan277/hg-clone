﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_landing_page_pr")]
    public class film_landing_page_prController : Controller
    {
        // GET: film_landing_page_prs
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_landing_page_pr/List")]
        public ActionResult List(int length, int start, string search, int cpid, int status, int catalogid, string start_time, string end_time)
        {
            film_landing_page_prService svrfilm_landing_page_pr = (new film_landing_page_prService());
            List<vw_film_landing_page_pr> data = svrfilm_landing_page_pr.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, start_time = start_time, end_time = end_time });
            int recordsTotal = (int)svrfilm_landing_page_pr.CountAll(new PagingModel() { offset = start, limit = length, search = search, start_time = start_time, end_time = end_time });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        //public ActionResult List(int length, int start, string search, int cpid, int status, int catalogid, string start_time, string end_time)
        //{
        //    film_video_statisticService svrfilm_video_statistic = (new film_video_statisticService());
        //    List<vw_film_video_statistic> data = svrfilm_video_statistic.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
        //    int recordsTotal = (int)svrfilm_video_statistic.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
        //    int recordsFiltered = recordsTotal;
        //    int draw = 1;
        //    StringValues valueDraw = "";
        //    HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
        //    try { draw = int.Parse(valueDraw); } catch { }
        //    return Json(new
        //    {
        //        draw,
        //        recordsTotal,
        //        recordsFiltered,
        //        data
        //    });
        //}


        [HttpGet]
        [Route("~/film_landing_page_pr/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_landing_page_pr obj = (new film_landing_page_prService()).GetByID(id);
            if (obj == null) obj = (new film_landing_page_prService()).InitEmpty();
            return PartialView("film_landing_page_prEdit", obj);
        }
        [HttpPost]
        [Route("~/film_landing_page_pr/Update")]
        public ActionResult Update(film_landing_page_pr obj)
        {
            int result = new film_landing_page_prService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_landing_page_pr/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_landing_page_prService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_landing_page_pr/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_landing_page_pr obj = new film_landing_page_prService().GetByID(id);
            return PartialView("film_landing_page_prDetail", obj);
        }
        [HttpGet]
        [Route("~/film_landing_page_pr/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_landing_page_pr obj = new film_landing_page_prService().GetViewByID(id);
            return PartialView("film_landing_page_prDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_landing_page_pr/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_landing_page_prService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}