﻿using ClosedXML.Excel;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using lamlt.web.Services1;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/doanhthu")]
    public class DoanhthuController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("~/doanhthu/Lists")]
        public ActionResult Lists(int length, int start, string search, string start_time, string end_time)
        {
            ApiDoanhthuService svrDoanhthu = (new ApiDoanhthuService());
            if (string.IsNullOrEmpty(start_time)) start_time = DateTime.Now.ToString("yyyy-MM-dd");
            if (string.IsNullOrEmpty(end_time)) end_time = DateTime.Now.ToString("yyyy-MM-dd");



            List<mvm_bao_cao_doanhthu_day> data = svrDoanhthu.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, end_time = end_time, start_time = start_time });
            int recordsTotal = (int)svrDoanhthu.CountAll(new PagingModel() { offset = start, limit = length, search = search, end_time = end_time, start_time = start_time });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpGet]
        [Route("~/api/doanhthu/List")]
        public ActionResult List()
        {
            ApiDoanhthuService svrdoanhthu = (new ApiDoanhthuService());

            string time1 = DateTime.Now.ToString("yyyy-MM-dd");
            int countphoneactive = (int)svrdoanhthu.CountPhoneActive(time1);
            int countphone = (int)svrdoanhthu.countphone(time1);
            int countCancel = (int)svrdoanhthu.phonecancel(time1);
            int giahan = (int)svrdoanhthu.giahan(time1);
            int countSucceed = (int)svrdoanhthu.countSucceed(time1);
            int countfailed = (int)svrdoanhthu.countfailed(time1);
            int TLGHTC = (int)svrdoanhthu.GHTC(time1);
            int totaldoanhthu = (int)svrdoanhthu.sum(time1);
            return Json(new
            {
                time1,
                countphoneactive,
                countphone,
                countCancel,
                giahan,
                countSucceed,
                countfailed,
                TLGHTC,
                totaldoanhthu

            });
        }
        //[HttpPost]
        //[Route("~/doanhthu/update")]
        //public ActionResult Update(mvm_bao_cao_doanhthu_day obj)
        //{
        //    int result = new ApiDoanhthuService().UpdateOrInsert(obj);
        //    return Json(new { result = result });
        //}
        // tổng thuê bao active
        [HttpGet]
        [Route("~/api/doanhthu/phoneactive")]
        public IActionResult CountPhoneActive(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int recordsTotal = (int)api.CountPhoneActive(s);
            return Json(new { result = recordsTotal });
        }
        // tổng thuê bao đk
        [HttpGet]
        [Route("~/api/doanhthu/countphone")]
        public IActionResult CountPhone(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int recordsTotal = (int)api.countphone(s);
            return Json(new { result = recordsTotal });
        }
        // tổng thuê bao hủy
        [HttpGet]
        [Route("~/api/doanhthu/phonecancel")]
        public IActionResult countCancel(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int recordsTotal = (int)api.phonecancel(s);
            return Json(new { result = recordsTotal });
        }
        // tổng thuê bao gia hạn 
        [HttpGet]
        [Route("~/api/doanhthu/giahan")]
        public IActionResult giahan(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int recordsTotal = (int)api.giahan(s);
            return Json(new { result = recordsTotal });
        }
        [HttpGet]
        [Route("~/api/doanhthu/thanhcong")]
        public IActionResult countSucceed(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int recordsTotal = (int)api.countSucceed(s);
            return Json(new { result = recordsTotal });
        }
        [HttpGet]
        [Route("~/api/doanhthu/thatbai")]
        public IActionResult countfailed(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int recordsTotal = (int)api.countfailed(s);
            return Json(new { result = recordsTotal });
        }
        [HttpGet]
        [Route("~/api/doanhthu/GHTC")]
        public IActionResult GHTC(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int recordsTotal = (int)api.GHTC(s);
            return Json(new { result = recordsTotal });
        }
        [HttpGet]
        [Route("~/api/doanhthu/total")]
        public IActionResult sum(string s)
        {
            ApiDoanhthuService api = new ApiDoanhthuService();
            int result = (int)api.sum(s);
            return Json(new { result = result });
        }
        //public PartialViewResult get(string id)
        //{
        //    mvm_user_subscription_logs obj = new ApiDoanhthuService().UpdateByPath(id);
        //    return PartialView("mvm_user_subscription_logs", obj);
        //}
        // GET: api/<controller>
        //public IActionResult getAll()
        //{
        //    return Ok(new[] { "avx","vsds"});
        //}
        //public ApiDoanhthuController(DoanhthuManager<doanhthu> doanhthuManager)
        //{
        //    DoanhthuManager = doanhthuManager;
        //}
        //[HttpPost]
        //public async Task<IActionResult> create([FromBody] doanhthu model)
        //{
        //    var doanhthu1 = new doanhthu() { id = model.id, price = model.price, count = model.count };
        //    var result = await DoanhthuManager.c(doanhthu1);
        //    if (result.Succeeded)
        //    {
        //        return Ok();
        //    }
        //    else
        //    {
        //        return BadRequest();
        //    }
        //}


        // GET api/<controller>/5


        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        /*
         * select action_date, package_code,msisdn,`channel`,reason,price,original_price from vascloud_cdr where action_date >= "2020-02-01" and action_date <= "2020-02-29"
         */
        [HttpGet]
        [Route("~/doanhthu/GetReportVnpt")]
        public JsonResult GetReportVnpt(string start, string end)
        {
            List<vascloud_cdr> obj = new ApiDoanhthuService().GetReportVnpt(start, end);
            LogService.logItem(obj);
            DataTable dt = new DataTable
            {
                TableName = "Báo Cáo"
            };
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("Ngày", typeof(string));
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Số điện thoại", typeof(string));
            dt.Columns.Add("Kênh", typeof(string));
            dt.Columns.Add("Lý do", typeof(string));
            dt.Columns.Add("Giá", typeof(string));
            dt.Columns.Add("Giá gốc", typeof(string));
            //Add Rows in DataTable
            int stt = 1;
            for (int i = 0; i < obj.Count; i++)
            {
                dt.Rows.Add(stt, obj[i].action_date, obj[i].package_code, obj[i].msisdn, obj[i].channel,
                    obj[i].reason, obj[i].price, obj[i].original_price);
                stt++;
            }
            dt.AcceptChanges();
            dt.DefaultView.Sort = "STT ASC";
            //Name of File
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string folderPath = "/home/amnhacsaigon/dotnet/cms-lalatv-viettel-v1/wwwroot/export_vnpt/";
            if (isDev)
            {
                folderPath = "C://export_vnpt/";
            }
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string sheetName = start.Replace("-", "") + "_to_" + end.Replace("-", "");
            string fileName = "Report_VNPT_" + sheetName + ".xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                var ws = wb.Worksheets.Add(dt, sheetName);
                ws.Style.Font.Bold = true;
                // Set the width of all columns in the worksheet
                ws.Column("1").Width = 10;
                ws.Column("2").Width = 25;
                ws.Column("3").Width = 10;
                ws.Column("4").Width = 20;
                ws.Column("5").Width = 10;
                ws.Column("6").Width = 10;
                ws.Column("7").Width = 15;
                ws.Column("8").Width = 20;
                ws.Row(1).Height = 20;
                ws.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                ws.Row(1).Style.Font.FontColor = XLColor.Red;
                ws.Row(1).Style.Font.SetFontSize(12);
                ws.Row(1).Height = 30;
                ws.Rows().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                ws.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                //ws.Rows().Style.Alignment.WrapText = true;
                //ws.Rows().AdjustToContents();
                wb.SaveAs(folderPath + fileName);
                string rs = "/export_vnpt/" + fileName;
                return Json(new { data = obj, file = rs });
            }
        }
    }
}
