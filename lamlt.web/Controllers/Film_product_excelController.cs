﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_product_excel")]
    public class film_product_excelController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        // ACTION LIST
        [HttpGet]
        [Route("~/film_product_excel/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_productService sv_film_product = (new film_productService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_product> data = sv_film_product.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_product.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_product_excel/Edit/{id}")]
        public ActionResult film_product_excelEdit([FromRoute] string id)
        {
            film_product obj = (new film_productService()).GetByID(id);
            if (obj == null)
            {
                obj = (new film_productService()).InitEmpty();
                List<film_isrc> lists = new film_isrcService().get(0, 0, 0);
                int current_code = lists[0].current_code + 1;
                obj.code = "VNA0D" + DateTime.Now.ToString("yy") + current_code;
            }
            return View("film_product_excelEdit", obj);
        }

        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_product_excel/GetViewDetail/{id}")]
        public PartialViewResult film_product_excelDetail([FromRoute] string id)
        {
            vw_film_product obj = new film_productService().GetViewByID(id);
            return PartialView("film_product_excelDetail", obj);
        }

        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_product_excel/Update")]
        public ActionResult Update(film_product obj)
        {
            film_productService film_productSV = new film_productService();
            int result = film_productSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_product_excel/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_productService film_productSV = new film_productService();
            int result = film_productSV.UpdateStatus(id);
            return Json(new { result = result });
        }

        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_product_excel/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_productService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        // ACTION CONVERT
        [HttpPost]
        [Route("~/film_product_excel/convert/{id}")]
        public ActionResult ConvertProduct(int id)
        {
            PagingModel paging = new PagingModel()
            {
                product_id = id
            };
            film_productService film_productSV = new film_productService();
            int result = film_productSV.convert_product(paging);
            return Json(new { result = result });
        }

        // ACTION CONVERT
        [HttpGet]
        [Route("~/film_product_excel/test")]
        public ActionResult ConvertProduct123()
        {
            film_video_processingService film_productSV = new film_video_processingService();
            List<vw_film_video_processing> result = film_productSV.GetViewAllItem(new PagingModel() { offset = 0, limit = 100 });
            List<vw_film_video_processing> result1 = new List<vw_film_video_processing>();
            foreach (var item in result)
            {
                string file = item.upload_file;
                file = file.Replace("http://27.76.152.255:13000/hg_project/", "/home/www/data/data/saigonmusic/hg_project/");
                //file = file.Replace("https://files.lalatv.com.vn:9090/hg_project/", "/home/www/data/data/saigonmusic/hg_project/");
                if (!System.IO.File.Exists(file)) result1.Add(item);
            }
            return Json(new { result = result1 });
        }
    }
}