﻿using ClosedXML.Excel;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/livestreaming")]
    public class livestreamingController : Controller
    {
        // GET: livestreamings
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/livestreaming/List")]
        public ActionResult List(int length, int start, string search, string live_status, string live_type, string start_time, string end_time, string userid)
        {
            livestreamingService svrlivestreaming = (new livestreamingService());
            PagingModel pagingModel = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                live_status = live_status,
                live_type = live_type,
                start_time = start_time,
                end_time = end_time,
                live_userid = userid
            };
            List<vw_livestreaming> data = svrlivestreaming.GetViewAllItem(pagingModel);
            int recordsTotal = (int)svrlivestreaming.CountAll(pagingModel);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try
            {
                draw = int.Parse(valueDraw);
            }
            catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpGet]
        [Route("~/livestreaming/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            livestreaming obj = (new livestreamingService()).GetByID(id);
            if (obj == null) obj = (new livestreamingService()).InitEmpty();
            return PartialView("livestreamingEdit", obj);
        }

        [HttpPost]
        [Route("~/livestreaming/Update")]
        public ActionResult Update(livestreaming obj)
        {
            int result = new livestreamingService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/livestreaming/UpdateWait")]
        public JsonResult UpdateWait(int id)
        {
            return Json(new livestreamingService().UpdateWait(id));
        }

        [HttpGet]
        [Route("~/livestreaming/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            livestreaming obj = new livestreamingService().GetByID(id);
            return PartialView("livestreamingDetail", obj);
        }

        [HttpGet]
        [Route("~/livestreaming/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_livestreaming obj = new livestreamingService().GetViewByID(id);
            return PartialView("livestreamingDetail", obj);
        }

        [HttpDelete]
        [Route("~/livestreaming/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new livestreamingService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/livestreaming/CreatePlaylist")]
        public int CreatePlaylist(string[] playlist, string[] key, string[] type, string repeat, string resolution, string other, string title, int live_catalog_id, DateTime starttime, DateTime stoptime)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string path = "/home/amnhacsaigon/livestream_v2/";
            if (isDev)
            {
                path = "C://";
            }
            var key_youtube = "";
            var key_facebook = "";
            if (key != null)
            {
                foreach (var key_item in key)
                {
                    if (key_item.Contains("youtube="))
                    {
                        key_youtube = key_item.Replace("youtube=", "");
                    }
                    if (key_item.Contains("facebook="))
                    {
                        key_facebook = key_item.Replace("facebook=", "");
                    }
                }
            }
            var file_name = path + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".playlist";
            foreach (var item in type)
            {
                switch (item)
                {
                    case "lalatv": break;
                    case "facebook":
                        file_name = path + "facebook_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".playlist";
                        // Xóa sau đó ghi ra file txt để cho Python đọc và chạy code trong đó
                        if (System.IO.File.Exists(file_name))
                        {
                            System.IO.File.Delete(file_name);
                        }

                        using (StreamWriter sw = new StreamWriter(file_name, true, Encoding.ASCII))
                        {
                            foreach (string list in playlist)
                            {
                                sw.WriteLine("file '" + list + "'");
                            }
                            sw.Flush();
                            sw.Close();
                        }
                        /*
                         ffmpeg.exe -re -stream_loop -1 -i 1.mp4 -acodec libmp3lame  -ar 44100 -b:a 128k -pix_fmt yuv420p -profile:v baseline -s 1280x720 -bufsize 6000k -vb 400k -maxrate 1500k -deinterlace -vcodec libx264 -preset veryfast -g 30 -r 30 -f flv "rtmps://live-api-s.facebook.com:443/rtmp/3680017362069622?s_bl=1&s_ps=1&s_sw=0&s_vt=api-s&a=AbysfT7cMiVza-wF"

                         */

                        // Cập nhật vào cơ sở dữ liệu
                        //var cmd_fb = "sudo ffmpeg " + repeat + other + " -f concat -safe 0 -protocol_whitelist file,http,https,tcp,tls,crypto -i " + file_name + " -b:v:1 2500k -r:v:1 30 -c copy -vcodec libx264 " + size + " -f flv 'rtmps://live-api-s.facebook.com:443/rtmp/" + key_facebook + "'";
                        var cmd_fb = "sudo ffmpeg " + repeat + other + " -f concat -safe 0 -fflags discardcorrupt -protocol_whitelist file,http,https,tcp,tls,crypto -i " + file_name + " -vcodec copy " + resolution + " -f flv 'rtmps://live-api-s.facebook.com:443/rtmp/" + key_facebook + "'";
                        var rs_fb = new livestreamingService().UpdateOrInsert(new livestreaming()
                        {
                            cmd = cmd_fb.Replace("'", "\""),
                            video = string.Join(",", playlist),
                            status = "wait",
                            live_catalog_id = live_catalog_id,
                            type = "facebook",
                            title = title,
                            datecreated = DateTime.Now,
                            userid = comm.GetUserId(),
                            starttime = starttime,
                            stoptime = stoptime
                        });
                        break;
                    // Mặc đinh sẽ là Youtube
                    default:
                        file_name = path + "youtube_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".playlist";
                        // Xóa sau đó ghi ra file txt để cho Python đọc và chạy code trong đó
                        if (System.IO.File.Exists(file_name))
                        {
                            System.IO.File.Delete(file_name);
                        }
                        using (StreamWriter sw = new StreamWriter(file_name, true, Encoding.ASCII))
                        {
                            foreach (string list in playlist)
                            {
                                sw.WriteLine("file '" + list + "'");
                            }
                            sw.Flush();
                            sw.Close();
                        }
                        // Cập nhật vào cơ sở dữ liệu
                        //var cmd_youtube = "./ffmpeg " + repeat + other + " -f concat -safe 0 -protocol_whitelist file,http,https,tcp,tls,crypto -i " + file_name + " -b:v:1 2500k -r:v:1 30 -c copy -vcodec libx264 " + size + " -f flv 'rtmp://a.rtmp.youtube.com/live2/" + key_youtube + "'";
                        var cmd_youtube = "./ffmpeg " + repeat + other + " -f concat -safe 0 -fflags discardcorrupt -protocol_whitelist file,http,https,tcp,tls,crypto -i " + file_name + " -vcodec copy " + resolution + " -f flv 'rtmp://a.rtmp.youtube.com/live2/" + key_youtube + "'";
                        var rs_yt = new livestreamingService().UpdateOrInsert(new livestreaming()
                        {
                            cmd = cmd_youtube.Replace("'", "\""),
                            video = string.Join(",", playlist),
                            status = "wait",
                            type = "youtube",
                            live_catalog_id = live_catalog_id,
                            title = title,
                            datecreated = DateTime.Now,
                            userid = comm.GetUserId(),
                            starttime = starttime,
                            stoptime = stoptime
                        });
                        break;
                }
            }
            return 1;
        }

        [HttpPost]
        [Route("~/livestreaming/KillProcessLive")]
        public int KillProcessLive(string id, string process_id)
        {
            return new livestreamingService().KillProcessLive(id, process_id);
        }

        [HttpPost]
        [Route("~/livestreaming/StopProcess")]
        public int StopProcess(int id)
        {
            return new livestreamingService().StopProcess(id);
        }

        #region =====================================  XUẤT FILE BẰNG CLOSED XML ============================

        [HttpPost]
        [Route("~/livestreaming/exportvialib")]
        public JsonResult exportvialib(string type, string list_id)
        {
            livestreamingService svrfilm_video = (new livestreamingService());
            //PagingModel page = new PagingModel()
            //{
            //    offset = 0,
            //    limit = 1000000,
            //    search = null,
            //    cpid = -1,
            //    status = -1,
            //    catalogid = -1,
            //    publish_year = -1,
            //    publish_countryid = -1,
            //    film_type = -1,
            //    action_type = "search"
            //};
            List<vw_livestreaming> obj = svrfilm_video.GetViewAllItem_Export();
            if (type == "dachon")
            {
                List<vw_livestreaming> selected_obj = new List<vw_livestreaming>();
                var strlist = list_id.Split(",");
                foreach (var all_data in obj)
                {
                    foreach (var selected_id in strlist)
                    {
                        if (selected_id == all_data.Id.ToString())
                        {
                            selected_obj.Add(all_data);
                        }
                    }
                }
                obj = selected_obj;
            }
            DataTable dt = new DataTable
            {
                TableName = "Phim"
            };
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("Tiêu đề", typeof(string));
            dt.Columns.Add("Video", typeof(string));
            dt.Columns.Add("Loại", typeof(string));
            dt.Columns.Add("Danh mục live", typeof(string));
            dt.Columns.Add("Tài khoản", typeof(string));
            dt.Columns.Add("Ngày tạo", typeof(string));
            dt.Columns.Add("Ghi chú", typeof(string));
            //Add Rows in DataTable
            int stt = 1;
            for (int i = 0; i < obj.Count; i++)
            {
                var video = obj[i].video;
                var videos = "";
                if (video.Contains(","))
                {
                    foreach (var item in video.Split(","))
                    {
                        videos += item + Environment.NewLine;
                    }
                    dt.Rows.Add(stt, obj[i].title, videos, obj[i].type, obj[i].live_catalog_title, obj[i].username, obj[i].datecreated, obj[i].note);
                }
                else
                {
                    dt.Rows.Add(stt, obj[i].title, obj[i].video, obj[i].type, obj[i].live_catalog_title, obj[i].username, obj[i].datecreated, obj[i].note);
                }
                stt++;
            }
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Ngày tạo ASC";
            //Name of File
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string folderPath = "/home/amnhacsaigon/dotnet/cms-lalatv-viettel-v1/wwwroot/export_livestream/";
            if (isDev)
            {
                folderPath = "C://";
            }
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fileName = "Export_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                var ws = wb.Worksheets.Add(dt, "Sheet 1");
                ws.Style.Font.Bold = true;
                // Set the width of all columns in the worksheet
                ws.Column("1").Width = 5;
                ws.Column("2").Width = 50;
                ws.Column("3").Width = 100;
                ws.Column("4").Width = 10;
                ws.Column("5").Width = 20;
                ws.Column("6").Width = 15;
                ws.Column("7").Width = 20;
                ws.Column("8").Width = 35;
                ws.Row(1).Height = 20;
                ws.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                ws.Row(1).Style.Font.FontColor = XLColor.Red;
                ws.Row(1).Style.Font.SetFontSize(12);
                ws.Row(1).Height = 30;
                ws.Rows().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                ws.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                //ws.Rows().Style.Alignment.WrapText = true;
                //ws.Rows().AdjustToContents();
                wb.SaveAs(folderPath + fileName);
                string rs = "/export_livestream/" + fileName;
                return Json(new { data = rs });
            }
        }

        #endregion =====================================  XUẤT FILE BẰNG CLOSED XML ============================

        [HttpGet]
        [Route("~/livestreaming/GetVideoDownloaded")]
        public JsonResult GetVideoDownloaded()
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string folderPath = "/home/amnhacsaigon/livestream_v2/downloaded/";
            if (isDev)
            {
                folderPath = "D://Desktop//stream//";
            }
            string[] allFile = Directory.GetFiles(folderPath, "*.*", SearchOption.AllDirectories);
            int countFile = allFile != null ? allFile.Length : 0;
            List<FileDownloaded> lst = new List<FileDownloaded>();
            for (int i = 0; i < allFile.Length; i++)
            {
                var fileInfo = new FileInfo(allFile[i].ToString());
                var objFile = new FileDownloaded
                {
                    fileName = allFile[i].ToString().Replace("/home/amnhacsaigon/livestream_v2/", ""),
                    fileSize = FileDownloaded.GetFileSize(allFile[i].ToString()),
                    fileDatecreated = fileInfo.CreationTime
                };
                lst.Add(objFile);
            }
            return Json(new
            {
                count = countFile,
                list_file = lst
            });
        }

        [HttpPost]
        [Route("~/livestreaming/DeleteFileDownloaded")]
        public string DeleteFileDownloaded(string[] list_delete)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string folderPath = "/home/amnhacsaigon/livestream_v2/downloaded/";
            if (isDev)
            {
                folderPath = "D://Desktop//stream//";
            }
            try
            {
                for (int i = 0; i < list_delete.Length; i++)
                {
                    var file_path = isDev ? list_delete[i] : folderPath.Replace("downloaded/", "") + list_delete[i];
                    if (System.IO.File.Exists(file_path))
                    {
                        System.IO.File.Delete(file_path);
                    }
                }
                return "true";
            }
            catch (IOException ioex)
            {
                return ioex.Message;
            }
        }
    }

    public class FileDownloaded
    {
        public string fileName { get; set; }
        public string fileSize { get; set; }
        public DateTime fileDatecreated { get; set; }

        public static string GetFileSize(string fileName)
        {
            string[] sizes = { "B", "KB", "MB", "GB", "TB" };
            double len = new FileInfo(fileName).Length;
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len /= 1024;
            }
            return String.Format("{0:0.##} {1}", len, sizes[order]);
        }
    }
}