﻿using lamlt.web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeJquery.Ui.Controllers
{
    public class HomeController : Controller
    {
        private AppSettingsModel settings;
        public HomeController(IOptions<AppSettingsModel> app)
        {
            this.settings = app.Value;
        }
        // private IConfiguration configuration;
        // GET: /<controller>/
        public IActionResult Index()
        {
            string dbConn = settings.ConnectionString;
            // string dbConn2 = configuration.GetValue<string>("MySettings:DbConnection");
            return View();
        }
    }
}
