﻿using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace lamlt.web.Controllers
{
    public class TranController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [Route("~/tran/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file_tran(IFormFile file, string fromlang, string tolang, string service)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0)
                return Content("file not selected");
            string fileDirect = @"/home/www/data/data/saigonmusic/hg_project/";
            if (isDev)
            {
                fileDirect = @"C:\data\";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }

            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect);
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            int tranid = 0;

            string renameFile = Convert.ToString("file_" + DateTime.Now.Ticks + "." + fileName.Split('.').Last());
            var fullPath = Path.Combine(path, renameFile);

            try
            {
                tranid = new film_video_processingService().UpdateOrInsert(new data.film_video_processing()
                {
                    Id = 0,
                    title = $"{fileName}",
                    video_type = "denoiser",
                    upload_file = "/hg_project/" + renameFile
                });
            }
            catch (Exception ex)
            {
                return Json(new { data = "Lỗi tạo mới " + ex.Message }); ;
            }
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            return Json(new
            {
                id = tranid,
                file = renameFile,
            }); ;
        }
    }
}
