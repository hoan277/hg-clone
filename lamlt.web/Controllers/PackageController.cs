﻿using Lala.lamlt.webservice;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/package")]
    public class packageController : Controller
    {
        // GET: packages
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/package/List")]
        public ActionResult List(int length, int start, string search)
        {
            packageService svrpackage = (new packageService());
            List<vw_package> data = svrpackage.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrpackage.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/package/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            package obj = (new packageService()).GetByID(id);
            if (obj == null) obj = (new packageService()).InitEmpty();
            return PartialView("packageEdit", obj);
        }
        [HttpPost]
        [Route("~/package/Update")]
        public ActionResult Update(package obj)
        {
            int result = new packageService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/package/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new packageService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/package/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            package obj = new packageService().GetByID(id);
            return PartialView("packageDetail", obj);
        }
        [HttpGet]
        [Route("~/package/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_package obj = new packageService().GetViewByID(id);
            return PartialView("packageDetail", obj);
        }
        [HttpDelete]
        [Route("~/package/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new packageService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/api/AddTransaction/")]
        public ActionResult AddTransaction()
        {
            StringValues str_packageId;
            Request.Query.TryGetValue("packageId", out str_packageId);
            StringValues str_userId;
            Request.Query.TryGetValue("userId", out str_userId);
            //Update to database
            int packageId = int.Parse(str_packageId);
            int userId = int.Parse(str_userId);
            packageService service = new packageService();
            string url = service.create_transaction(packageId, userId);
            //get transaction id
            string guid = getValueInURL(url, "reference_number=", "&currency");
            var trans_id = new film_transactionsService().GetByTxtId(guid);

            return new JsonResult(JsonResponse.success("Đang chuyển hướng ngân hàng", new
            {
                sub_type = 0,
                transaction_obj = trans_id,
                sub_state = 0,
                href = url
            }));
        }
        public static string getValueInURL(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        [HttpGet]
        [Route("~/api/package/getall")]
        public ActionResult GetAll(int length, int start, string search)
        {
            packageService svrpackage = (new packageService());
            List<package> data = svrpackage.GetPackages();
            return Json(new { data });
        }
    }
}