﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/user_link")]
    public class user_linkController : Controller
    {
        // GET: user_links
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/user_link/List")]
        public ActionResult List(int length, int start, string search)
        {
            user_linkService svruser_link = (new user_linkService());
            List<user_link> data = svruser_link.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svruser_link.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/user_link/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            user_link obj = (new user_linkService()).GetByID(id);
            if (obj == null) obj = (new user_linkService()).InitEmpty();
            return PartialView("user_linkEdit", obj);
        }
        [HttpPost]
        [Route("~/user_link/Update")]
        public ActionResult Update(user_link obj)
        {
            int result = new user_linkService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/user_link/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            user_link obj = new user_linkService().GetByID(id);
            return PartialView("user_linkDetail", obj);
        }
        [HttpGet]
        [Route("~/user_link/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_user_link obj = new user_linkService().GetViewByID(id);
            return PartialView("user_linkDetail", obj);
        }
        [HttpDelete]
        [Route("~/user_link/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new user_linkService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}