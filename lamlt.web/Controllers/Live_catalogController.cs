﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/live_catalog")]
    public class live_catalogController : Controller
    {
        // GET: live_catalogs
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/live_catalog/List")]
        public ActionResult List(int length, int start, string search)
        {
            live_catalogService svrlive_catalog = (new live_catalogService());
            List<vw_live_catalog> data = svrlive_catalog.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrlive_catalog.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/live_catalog/Edit/{id}")]
        public PartialViewResult live_catalogEdit([FromRoute] string id)
        {
            live_catalog obj = (new live_catalogService()).GetByID(id);
            if (obj == null) obj = (new live_catalogService()).InitEmpty();
            return PartialView("live_catalogEdit", obj);
        }
        [HttpPost]
        [Route("~/live_catalog/Update")]
        public ActionResult Update(live_catalog obj)
        {
            int result = new live_catalogService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/live_catalog/quantity")]
        public ActionResult quantity(int id)
        {
            int result = new live_catalogService().quantity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/live_catalog/GetDetail/{id}")]
        public PartialViewResult live_catalogDetail([FromRoute] string id)
        {
            live_catalog obj = new live_catalogService().GetByID(id);
            return PartialView("live_catalogDetail", obj);
        }
        [HttpGet]
        [Route("~/live_catalog/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_live_catalog obj = new live_catalogService().GetViewByID(id);
            return PartialView("live_catalogDetail", obj);
        }
        [HttpDelete]
        [Route("~/live_catalog/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new live_catalogService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}