﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_comments")]
    public class film_commentsController : Controller
    {
        // GET: film_commentss
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_comments/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_commentsService svrfilm_comments = (new film_commentsService());
            List<vw_film_comments> data = svrfilm_comments.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_comments.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_comments/Edit/{id}")]
        public PartialViewResult film_commentsEdit([FromRoute] string id)
        {
            film_comments obj = (new film_commentsService()).GetByID(id);
            if (obj == null) obj = (new film_commentsService()).InitEmpty();
            return PartialView("film_commentsEdit", obj);
        }
        [HttpPost]
        [Route("~/film_comments/Update")]
        public ActionResult Update(film_comments obj)
        {
            int result = new film_commentsService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_comments/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_commentsService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_comments/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_comments obj = new film_commentsService().GetByID(id);
            return PartialView("film_commentsDetail", obj);
        }
        [HttpGet]
        [Route("~/film_comments/GetViewDetail/{id}")]
        public PartialViewResult film_commentsDetail(string id)
        {
            vw_film_comments obj = new film_commentsService().GetViewByID(id);
            return PartialView("film_commentsDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_comments/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_commentsService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/film_comments/UpdateStatus")]
        public ActionResult UpdateStatus(film_comments obj)
        {
            int result = new film_commentsService().UpdateStatus(obj);
            return Json(new { result = result });
        }
    }
}