﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/api/film_media_stream")]
    public class Api_film_media_streamController : Controller
    {
        private IConfiguration configuration;
        public Api_film_media_streamController(IConfiguration iconfig)
        {
            configuration = iconfig;
        }
        public class DataResultObject
        {
            public string key { get; set; }
            public string value { get; set; }
            public string path { get; set; }
            public string path_output { get; set; }
            public string path_origin { get; set; }
        }
        [HttpGet]
        [Route("~/api/film_media_stream")]
        public ActionResult GetResult([FromRoute] string code)
        {
            film_media_streamService svfilm_media_stream = new film_media_streamService();

            StringValues s_code;
            Request.Query.TryGetValue("code", out s_code);
            if (s_code == "")
            {
                List<film_media_stream> data = svfilm_media_stream.GetListByCode(null);
                int recordsTotal = (int)svfilm_media_stream.CountAll(null);
                int recordsFiltered = recordsTotal;
                int draw = 1;
                StringValues valueDraw = "";
                HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
                try { draw = int.Parse(valueDraw); } catch { }
                List<object> firstlist = new List<object>();
                for (int i = 0; i < data.Count; i++)
                {
                    DataResultObject dataResultObject = new DataResultObject();
                    string svFilmMedia = svfilm_media_stream.getResult(data[i]);
                    dataResultObject.value = svFilmMedia;
                    dataResultObject.key = data[i].code;
                    dataResultObject.path = data[i].input_dir;
                    dataResultObject.path_output = data[i].outputFolder;
                    dataResultObject.path_origin = data[i].input_origin;
                    firstlist.Add(dataResultObject);
                }
                return Json(new { result = firstlist });
            }
            else
            {

                film_media_stream obj = new film_media_streamService().GetByCode(s_code);
                if (obj == null)
                {
                    return Json(new { result = "Not Found" });
                }
                else
                {
                    string data = svfilm_media_stream.getResult(obj);
                    return Json(new { result = data, path = obj.outputFolder });
                }

            }
        }
    }
}