﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_transcript")]
    public class film_transcriptController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        // ACTION LIST
        [HttpGet]
        [Route("~/film_transcript/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_transcriptService sv_film_transcript = (new film_transcriptService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_transcript> data = sv_film_transcript.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_transcript.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_transcript/get")]
        public JsonResult get(int id, string title)
        {
            var obj = new film_transcriptService().GetInfo(id, title);
            return Json(obj != null ? obj : null);
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_transcript/Edit/{id}")]
        public PartialViewResult film_transcriptEdit([FromRoute] string id)
        {
            film_transcript obj = (new film_transcriptService()).GetByID(id);
            if (obj == null) obj = (new film_transcriptService()).InitEmpty();
            return PartialView("film_transcriptEdit", obj);
        }

        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_transcript/GetViewDetail/{id}")]
        public PartialViewResult film_transcriptDetail([FromRoute] string id)
        {
            vw_film_transcript obj = new film_transcriptService().GetViewByID(id);
            return PartialView("film_transcriptDetail", obj);
        }

        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_transcript/Update")]
        public ActionResult Update(film_transcript obj)
        {
            film_transcriptService film_transcriptSV = new film_transcriptService();
            int result = film_transcriptSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_transcript/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_transcriptService film_transcriptSV = new film_transcriptService();
            int result = film_transcriptSV.UpdateStatus(id);
            return Json(new { result = result });
        }

        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_transcript/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_transcriptService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [Route("~/film_transcript/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file(IFormFile file)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0) return Content("file not selected");
            string sFileWillSave = file.FileName;
            string fileDirect = @"/home/www/data/data/saigonmusic/hg_project_sub/";
            //string fileDirect = @"/home/amnhacsaigon/dotnet/acs-be/wwwroot/copyright/";
            if (isDev)
            {
                fileDirect = @"D:\data\";
            }
            string mode = LamltService.GetConnectionString("mode");
            //if (mode == "docker") fileDirect = "/app/wwwroot/file_transcript/";
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            string srt_content = "";
            using (var reader = new StreamReader(path))
            {
                srt_content = reader.ReadToEnd();
            }
            return Json(srt_content);
            //return Json(new { file = "https://files.lalatv.com.vn:9090/hg_project_sub/" + sFileWillSave });
            //return Json(new { file = "http://27.76.152.255:13000/hg_project_sub/" + sFileWillSave });
        }

        // Gen file *.srt
        [HttpPost]
        [Route("~/film_transcript/gen_srt")]
        public JsonResult gen_srt(string file_srt, string content)
        {
            if (string.IsNullOrEmpty(content)) { return Json("Nội dung không được để trống"); }
            return Json(new film_transcriptService().gen_srt(file_srt, content));
        }
    }
}