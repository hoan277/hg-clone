﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("tbl_tongiao")]
    public class tbl_tongiaoController : Controller
    {
        // GET: tbl_tongiaos
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/tbl_tongiao/List")]
        public ActionResult List(int length, int start, string search)
        {
            tbl_tongiaoService svrtbl_tongiao = (new tbl_tongiaoService());
            List<vw_tbl_tongiao> data = svrtbl_tongiao.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrtbl_tongiao.CountViewAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/tbl_tongiao/Edit/{id}")]
        public PartialViewResult Edit(string id)
        {
            tbl_tongiao obj = (new tbl_tongiaoService()).GetByID(id);
            if (obj == null) obj = (new tbl_tongiaoService()).InitEmpty();
            return PartialView("tbl_tongiaoEdit", obj);
        }
        [HttpPost]
        [Route("~/tbl_tongiao/Update")]
        public ActionResult Update([FromBody] tbl_tongiao obj)
        {
            int result = new tbl_tongiaoService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/tbl_tongiao/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            tbl_tongiao obj = new tbl_tongiaoService().GetByID(id);
            return PartialView("tbl_tongiaoDetail", obj);
        }
        [HttpGet]
        [Route("~/tbl_tongiao/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_tbl_tongiao obj = new tbl_tongiaoService().GetViewByID(id);
            return PartialView("tbl_tongiaoDetail", obj);
        }
        [HttpDelete]
        [Route("~/tbl_tongiao/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new tbl_tongiaoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}