﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;
using System.IO;

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video_update")]
    public class film_video_updateController : Controller
    {
        // config key value
        private IConfiguration webconfig;
        public film_video_updateController(IConfiguration iConfig)
        {
            webconfig = iConfig;
        }
        // end key value
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        [HttpGet]
        [Route("~/film_video_update/List")]
        public ActionResult List(int length, int start, int status, string search)
        {
            film_video_updateService svrfilm_video = (new film_video_updateService());
            List<film_video> data = svrfilm_video.GetAllItem(new PagingModel() { offset = start, limit = length, status = status, search = search });
            int recordsTotal = (int)svrfilm_video.CountAll(new PagingModel() { offset = start, limit = length, status = status, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_video_update/Edits/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video obj = (new film_video_updateService()).GetByID(id);
            if (obj == null) obj = (new film_video_updateService()).InitEmpty();
            return PartialView("Edit", obj);
        }
        [HttpGet]
        [Route("~/film_video_update/getfiles")]
        public ActionResult getfiles()
        {
            string pathVideos = webconfig["pathVideo"];
            int cpid = comm.GetCPId();
            film_cp scans = new film_video_updateService().getcpScan(cpid);
            string scan = scans.scan;
            DirectoryInfo d = new DirectoryInfo(scan);
            FileInfo[] Files = d.GetFiles("*.mp4", SearchOption.TopDirectoryOnly);
            string str = "";
            foreach (FileInfo file in Files)
            {
                str = str + "," + file.Name;
                film_video obj = new film_video();
                obj.upload_file = file.Name;
                obj.status = 0;
                int result = new film_video_updateService().update_video(obj, pathVideos);

            }
            return Json(new { result = str });
        }
        //update 
        [HttpPost]
        [Route("~/film_video_update/Update")]
        public ActionResult Update(film_video obj)
        {
            string pathImages = webconfig["pathImage"];
            string pathVideos = webconfig["pathVideo"];
            int result = new film_video_updateService().UpdateOrInsert(obj, pathImages, pathVideos);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video_update/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_video_updateService().UpdateStatus(obj);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/film_video_update/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute] string id)
        {
            int result = new film_video_updateService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video_update/getImageupdate")]
        public IActionResult getImageupdate(int id)
        {
            List<film_video_image> result = new film_video_updateService().getupdateImg(id);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video_update/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid)
        {
            string pathImages = webconfig["pathImage"];
            int result = new film_video_updateService().UpdateImage(url, filmid, pathImages);
            return Json(new { result = result });
        }
        //  class filmid
        //{
        //    public int[] catatogid { get;set }
        //    int filmid { get; set}
        //}
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/film_video_update/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_video_updateService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video_update/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video obj = new film_video_updateService().GetByID(id);
            return PartialView("film_video_updateDetail", obj);
        }
        [HttpGet]
        [Route("~/film_video_update/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video obj = new film_video_updateService().GetViewByID(id);
            return PartialView("film_video_updateDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_video_update/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_video_updateService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }

}