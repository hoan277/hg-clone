﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_keyupload")]
    public class film_keyuploadController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        // ACTION LIST
        [HttpGet]
        [Route("~/film_keyupload/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_keyuploadService sv_film_keyupload = (new film_keyuploadService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_keyupload> data = sv_film_keyupload.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_keyupload.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_keyupload/Edit/{id}")]
        public PartialViewResult film_keyuploadEdit([FromRoute] string id)
        {
            film_keyupload obj = (new film_keyuploadService()).GetByID(id);
            if (obj == null) obj = (new film_keyuploadService()).InitEmpty();
            return PartialView("film_keyuploadEdit", obj);
        }
        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_keyupload/GetViewDetail/{id}")]
        public PartialViewResult film_keyuploadDetail([FromRoute] string id)
        {
            vw_film_keyupload obj = new film_keyuploadService().GetViewByID(id);
            return PartialView("film_keyuploadDetail", obj);
        }
        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_keyupload/Update")]
        public ActionResult Update(film_keyupload obj)
        {
            film_keyuploadService film_keyuploadSV = new film_keyuploadService();
            int result = film_keyuploadSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_keyupload/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_keyuploadService film_keyuploadSV = new film_keyuploadService();
            int result = film_keyuploadSV.UpdateStatus(id);
            return Json(new { result = result });
        }
        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_keyupload/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_keyuploadService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}