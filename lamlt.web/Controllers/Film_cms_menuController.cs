﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/film_cms_menu")]
    public class film_cms_menuController : Controller
    {
        // GET: film_cms_menus
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_cms_menu/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_cms_menuService svrfilm_cms_menu = (new film_cms_menuService());
            List<film_cms_menu> data = svrfilm_cms_menu.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_cms_menu.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_cms_menu/Edit/{id}")]
        public PartialViewResult film_cms_menuEdit([FromRoute] string id)
        {
            film_cms_menu obj = (new film_cms_menuService()).GetByID(id);
            if (obj == null) obj = (new film_cms_menuService()).InitEmpty();
            return PartialView("film_cms_menuEdit", obj);
        }
        [HttpPost]
        [Route("~/film_cms_menu/Update")]
        public ActionResult Update(film_cms_menu obj)
        {
            int result = new film_cms_menuService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_cms_menu/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_cms_menu obj = new film_cms_menuService().GetByID(id);
            return PartialView("film_cms_menuDetail", obj);
        }
        [HttpGet]
        [Route("~/film_cms_menu/GetViewDetail/{id}")]
        public PartialViewResult film_cms_menuDetail(string id)
        {
            vw_film_cms_menu obj = new film_cms_menuService().GetViewByID(id);
            return PartialView("film_cms_menuDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_cms_menu/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_cms_menuService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}