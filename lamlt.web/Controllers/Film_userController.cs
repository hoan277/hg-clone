﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_user")]
    public class film_userController : Controller
    {
        // GET: film_users
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_user/List")]
        public ActionResult List(int length, int start, string search, int sub_state)
        {
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                sub_state = sub_state
            };
            film_userService svrfilm_user = (new film_userService());
            List<vw_film_user> data = svrfilm_user.GetViewAllItem(paging);
            int recordsTotal = (int)svrfilm_user.CountAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpGet]
        [Route("~/film_user/Edit/{id}")]
        public PartialViewResult film_userEdit([FromRoute] string id)
        {
            film_user obj = (new film_userService()).GetByID(id);
            if (obj == null) obj = (new film_userService()).InitEmpty();
            return PartialView("film_userEdit", obj);
        }

        [HttpPost]
        [Route("~/film_user/Update")]
        public ActionResult Update(film_user obj)
        {
            int result = new film_userService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/film_user/Login")]
        public ActionResult Login(string user, string pass)
        {
            DateTime time = DateTime.Now;
            string api_server = "https://sdk.lalatv.com.vn/check_client?key=ABCDGAdhDAHDAJJADJA1234";
            WebClient web = new WebClient();
            string sResult = web.DownloadString(api_server);
            using (StreamWriter writer = new StreamWriter("user_log" + time.ToString("yyyyMMdd")+".txt", true)) //// true to append data to the file
            {
                
                writer.WriteLine(time.ToString()+" : is kiem tra client sdk "+ sResult + "(1: thành công, còn lại là thất bại)");
                if (sResult == "1")
                {
                    writer.WriteLine("______login___ mysql____");
                    film_user oLogin = new film_userService().CheckLogin(user, pass, writer);
                    //writer.WriteLine(time.ToString() + ": checklogin : " + "user_: "+ user+ ", password: "+pass);
                    int message = (oLogin != null ? 1 : 0);
                    writer.WriteLine(time.ToString() + ": message : " + message+ " (1: thành công, còn lại là thất bại)");
                    return Json(new { message = (oLogin != null ? 1 : 0) });
                    //film_user oLogin = new film_userService().CheckLogin(user, pass);
                    //int status1 = (oLogin != null ? 0 : 1);
                    //return Json(new { result = 1, status = status1, token = System.Guid.NewGuid().ToString(), user = oLogin });
                }
                else return Json(new { result = 1, status = 1, token = "" });
            }
            //film_user oResult1 = new film_userService().CheckLogin(user, pass);
            //return Json(new { message = (oResult != null ? 1 : 0) });
        }

        [HttpGet]
        [Route("~/film_user/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_user obj = new film_userService().GetByID(id);
            return PartialView("film_userDetail", obj);
        }

        [HttpGet]
        [Route("~/film_user/GetViewDetail/{id}")]
        public PartialViewResult film_userDetail(string id)
        {
            vw_film_user obj = new film_userService().GetViewByID(id);
            return PartialView("film_userDetail", obj);
        }

        [HttpDelete]
        [Route("~/film_user/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_userService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}