﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_drm")]
    public class film_drmController : Controller
    {
        [Route("~/film_drm")]
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_drm/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_drm_service drmServiceObj = (new film_drm_service());
            List<vw_film_drm> data = drmServiceObj.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)drmServiceObj.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_drm/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] int id)
        {
            film_drm obj = (new film_drm_service()).GetByID(id);
            if (obj == null) obj = (new film_drm_service()).InitEmpty();
            return PartialView("drmEdit", obj);
        }
        [HttpPost]
        [Route("~/film_drm/Update")]
        public ActionResult Update(film_drm obj)
        {
            return Json(new
            {
                result = new film_drm_service().UpdateOrInsert(obj)
            });
        }

        [HttpGet]
        [Route("~/film_drm/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] int id)
        {
            film_drm obj = new film_drm_service().GetByID(id);
            return PartialView("film_drmDetail", obj);
        }
        [HttpGet]
        [Route("~/film_drm/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_drm obj = new film_drm_service().GetViewByID(id);
            return PartialView("film_drmDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_drm/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_drm_service().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}