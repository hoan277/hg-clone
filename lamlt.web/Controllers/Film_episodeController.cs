﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_episode")]
    public class film_episodeController : Controller
    {
        private IConfiguration webconfig;
        public film_episodeController(IConfiguration iConfig)
        {
            webconfig = iConfig;
        }
        // GET: film_episodes
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_episode/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_episodeService svrfilm_episode = (new film_episodeService());
            List<vw_film_episode> data = svrfilm_episode.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_episode.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_episode/Edit/{id}")]
        public PartialViewResult film_episodeEdit([FromRoute] string id)
        {
            film_episode obj = (new film_episodeService()).GetByID(id);
            if (obj == null) obj = (new film_episodeService()).InitEmpty();
            return PartialView("film_episodeEdit", obj);
        }
        [HttpPost]
        [Route("~/film_episode/Update")]
        public ActionResult Update(film_episode obj)
        {
            int result = new film_episodeService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_episode/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_episodeService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_episode/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_episode obj = new film_episodeService().GetByID(id);
            return PartialView("film_episodeDetail", obj);
        }
        [HttpGet]
        [Route("~/film_episode/GetViewDetail/{id}")]
        public PartialViewResult film_episodeDetail(string id)
        {
            vw_film_episode obj = new film_episodeService().GetViewByID(id);
            return PartialView("film_episodeDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_episode/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_episodeService().Delete(int.Parse(id));
            return Json(new { result = result });
        }


        [HttpPost]
        [Route("~/film_episode/UpdateVideo")]
        public ActionResult UpdateVideo(int id)
        {
            //tìm episode
            film_episode obj = new film_episodeService().GetByID(id.ToString());
            // lấy danh sách video
            List<film_video> listFilmVideo = new film_videoService().GetListVideoByFilmEpisodeTitle(obj.title);
            for (int i = 0; i < listFilmVideo.Count; i++)
            {
                string pathVideos = webconfig["pathVideo"];
                string pathImages = webconfig["pathImage"];
                //lấy video
                film_video video = new film_videoService().GetByID(listFilmVideo[i].Id.ToString());
                int video_episode_current = new film_videoService().getEpisodeCurrentByTitleVideo(video.title);
                if (video_episode_current == 0)
                {
                    video_episode_current = (listFilmVideo.Count);
                }
                video.episode_id = id;
                video.episode = listFilmVideo.Count;
                video.episode_current = video_episode_current;
                // update video
                int resultVideo = new film_videoService().UpdateEpisode(video);
            }
            int result = new film_episodeService().UpdateOrInsert(obj);
            return Json(new { result = obj });
        }

        [HttpPost]
        [Route("~/film_episode/active/{id}")]
        public JsonResult active(int id)
        {
            int result = new film_episodeService().UpdateStatus(id);
            return Json(new { result = result });
        }

    }
}