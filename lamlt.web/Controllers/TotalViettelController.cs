﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/totalviettel")]
    public class TotalViettelController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("~/totalviettel/List")]
        public ActionResult List(int length, int start, string search, string start_time, string end_time)
        {
            TotalViettelService svrtotal = (new TotalViettelService());
            if (string.IsNullOrEmpty(start_time)) start_time = DateTime.Now.ToString("yyyy-MM-dd");
            if (string.IsNullOrEmpty(end_time)) end_time = DateTime.Now.ToString("yyyy-MM-dd");



            List<vw_totalviettel> data = svrtotal.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, end_time = end_time, start_time = start_time });
            int recordsTotal = (int)svrtotal.CountAll(new PagingModel() { offset = start, limit = length, search = search, end_time = end_time, start_time = start_time });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/totalviettel/total")]
        public IActionResult total(string s)
        {
            TotalViettelService lts = new TotalViettelService();
            int result = (int)lts.total(s);
            return Json(new { result = result });
        }
    }
}