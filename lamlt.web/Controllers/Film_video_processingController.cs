﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using lamlt.webservice.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Oze.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video_processing")]
    public class film_video_processingController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            HttpContext.Request.Query.TryGetValue("video_type", out StringValues video_type);
            switch (video_type[0].ToString())
            {
                case "demo":
                    return PartialView("processing_demo");
                case "other":
                    return PartialView("processing_translate");
                case "denoiser":
                    return PartialView("processing_denoiser");
                case "film":
                default:
                    return PartialView("Index");
            }

        }
        // ACTION LIST
        [HttpGet]
        [Route("~/film_video_processing/List")]
        public ActionResult List(int length, int start, string search, string time_range, string search_video_type)
        {
            film_video_processingService sv_film_video_processing = (new film_video_processingService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range,
                search_video_type = search_video_type,
            };
            List<vw_film_video_processing> data = sv_film_video_processing.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_video_processing.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_video_processing/Edit/{id}")]
        public PartialViewResult film_video_processingEdit([FromRoute] string id)
        {
            film_video_processing obj = (new film_video_processingService()).GetByID(id);
            if (obj == null) obj = (new film_video_processingService()).InitEmpty();
            return PartialView("film_video_processingEdit", obj);
        }

        [HttpGet]
        [Route("~/film_video_processing/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            film_video_processing obj = (new film_video_processingService()).GetByID(id);
            if (obj == null) obj = (new film_video_processingService()).InitEmpty();
            return View("Edit", obj);
        }

        [HttpGet]
        [Route("~/film_video_processing/get")]
        public JsonResult get(int id, string title, string code, string upload_file)
        {
            var obj = new film_video_processingService().GetInfo(id, title, code, upload_file);
            return Json(obj != null ? obj : null);
        }

        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_video_processing/GetViewDetail/{id}")]
        public PartialViewResult film_video_processingDetail([FromRoute] string id)
        {
            vw_film_video_processing obj = new film_video_processingService().GetViewByID(id);
            return PartialView("film_video_processingDetail", obj);
        }

        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_video_processing/Update")]
        public ActionResult Update(film_video_processing obj)
        {
            film_video_processingService film_video_processingSV = new film_video_processingService();
            int result = film_video_processingSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // ACTION TRANSCRIPT
        [HttpPost]
        [Route("~/film_video_processing/transcript")]
        public JsonResult transcript(string from, string to, string list_selected)
        {
            if (!string.IsNullOrEmpty(list_selected))
            {
                bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string folder = @"/home/amnhacsaigon/dotnet/hg-clone/wwwroot/translate/";
                if (isDev)
                {
                    folder = @"C:/translate/";
                }
                //string mode = LamltService.GetConnectionString("mode");
                //if (mode == "docker") folder = "/app/wwwroot/translate/";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                foreach (var processingId in list_selected.Split(","))
                {
                    var fileTxt = folder + processingId + "_" + from + "-" + to + ".txt";
                    if (System.IO.File.Exists(fileTxt))
                    {
                        System.IO.File.Delete(fileTxt);
                    }
                    using (StreamWriter sw = new StreamWriter(fileTxt, true, Encoding.ASCII))
                    {
                        var obj = new film_video_processingService().GetViewByID(processingId);
                        if (!string.IsNullOrEmpty(obj.json))
                        {
                            sw.Write(obj.upload_file);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
                return Json("Tạo lệnh dịch thành công");
            }
            else
            {
                return Json("Chưa chọn file nào");
            }
        }
        // ACTION speech to text
        [HttpPost]
        [Route("~/film_video_processing/SpeechToText")]
        public JsonResult SpeechToText(string list_selected, string from = "en", string service = "google")
        {
            string serial = LamltService.GetConnectionString("key");
            if (!string.IsNullOrEmpty(list_selected))
            {
                bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string folder = @"/home/amnhacsaigon/dotnet/hg-clone/wwwroot/speech_to_text/";
                if (isDev)
                {
                    folder = @"C:/speech_to_text/";
                }
                string check_key = new film_keyService().CheckKey();
                switch (check_key)
                {
                    case "success": goto speech2text;
                    case "no_key": return Json("Bạn chưa kích hoạt key bản quyền phần mềm");
                    case "key_log_error": return Json("Lỗi không kiểm tra được lịch sử sử dụng bản quyền phần mềm, vui lòng liên hệ quản trị viên");
                    case "timeout": return Json("Đã hết thời gian sử dụng");
                    case "error":
                    default:
                        return Json("Lỗi không kiểm tra được hệ thống bản quyền phần mềm, vui lòng liên hệ quản trị viên");
                }
            speech2text:
                //string mode = LamltService.GetConnectionString("mode");
                //if (mode == "docker") folder = "/app/wwwroot/speech_to_text/";\
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                try
                {
                    foreach (var processingId in list_selected.Split(","))
                    {
                        var fileTxt = folder + processingId + "_" + from + "_" + service + ".txt";
                        if (System.IO.File.Exists(fileTxt))
                        {
                            System.IO.File.Delete(fileTxt);
                        }
                        using (StreamWriter sw = new StreamWriter(fileTxt, true, Encoding.ASCII))
                        {
                            var obj = new film_video_processingService().GetViewByID(processingId);
                            Dictionary<object, object> dict = new Dictionary<object, object>();
                            if (obj != null)
                            {
                                dict.Add("file", obj.upload_file);
                                dict.Add("video_type", obj.video_type);
                                dict.Add("service", service);
                                dict.Add("step1", "");
                                dict.Add("step2", "");
                                dict.Add("step3", "");
                                dict.Add("key", serial);
                                sw.Write(JsonConvert.SerializeObject(dict));
                                sw.Flush();
                                sw.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("Thất bại: " + ex.Message);
                }

                return Json("Tạo lệnh phân tích thành công");
            }
            else
            {
                return Json("Chưa chọn file nào");
            }
        }

        // ACTION speech to text
        [HttpPost]
        [Route("~/film_video_processing/Convert_Sub")]
        public JsonResult Convert_Sub(string list_selected, string from = "en")
        {
            if (!string.IsNullOrEmpty(list_selected))
            {
                bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string folder = @"/home/amnhacsaigon/dotnet/hg-clone/wwwroot/convert_sub/";
                if (isDev)
                {
                    folder = @"C:/convert_sub/";
                }
                //string mode = LamltService.GetConnectionString("mode");
                //if (mode == "docker") folder = "/app/wwwroot/convert_sub/";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                foreach (var processingId in list_selected.Split(","))
                {
                    var obj = new film_video_processingService().GetViewByID(processingId);
                    if (obj != null)
                    {
                        var fileTxt = folder + obj.title + "_" + from + ".txt";
                        if (System.IO.File.Exists(fileTxt))
                        {
                            System.IO.File.Delete(fileTxt);
                        }
                        using (var sw = new StreamWriter(fileTxt, true, Encoding.UTF8))
                        {
                            var dict = new Dictionary<object, object>();
                            try
                            {
                                dict.Add("id", obj.Id);
                                dict.Add("file_mp4", obj.upload_file.Replace("https://files.lalatv.com.vn:9090/", "/home/www/data/data/saigonmusic/"));
                                Console.WriteLine("=========================== Chạy hàm lấy: GetSubProcessingID: " + processingId + " " + from);
                                var obj_transcript = new film_transcriptService().GetSubProcessingID(processingId, from);
                                Console.WriteLine("================obj_transcript:================");
                                Console.WriteLine(obj_transcript);
                                Console.WriteLine("=========================== Chạy hàm processing transcript : GetIDByProcesingID: " + processingId + " " + obj_transcript.Id);
                                var obj_processing_transcript = new film_video_processing_transcriptService().GetIDByProcesingID(int.Parse(processingId), obj_transcript.Id);
                                Console.WriteLine("================obj_processing_transcript:================");
                                Console.WriteLine(obj_processing_transcript);
                                if (obj_transcript != null)
                                {
                                    var fileName = obj_transcript.file.Replace("https://files.lalatv.com.vn:9090/", "/home/www/data/data/saigonmusic/");
                                    dict.Add("processing_transcript_id", obj_processing_transcript.Id);
                                    dict.Add("file_srt", fileName);
                                }
                                else
                                {
                                    dict.Add("processing_transcript_id", obj_processing_transcript.Id);
                                    dict.Add("file_srt", "null");
                                }
                                sw.Write(JsonConvert.SerializeObject(dict, Formatting.Indented));
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Lỗi không có dữ liệu: " + ex.Message);
                            }
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
                return Json("Tạo lệnh convert sub thành công");
            }
            else
            {
                return Json("Chưa chọn file nào");
            }
        }

        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_video_processing/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_video_processingService film_video_processingSV = new film_video_processingService();
            int result = film_video_processingSV.UpdateStatus(id);
            return Json(new { result = result });
        }

        // ACTION UPDATE STATUS
        [HttpGet]
        [Route("~/film_video_processing/UpdateLog")]
        public ActionResult UpdateLog(int id, string status)
        {
            return Json(new film_video_processingService().UpdateLog(id, status));
        }

        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_video_processing/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_video_processingService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        // ACTION IMPORT EXCEL
        [HttpPost]
        [Route("~/film_video_processing/importExcel")]
        public ActionResult importExcel()
        {
            film_video_processingService film_video_processingSV = new film_video_processingService();
            // check file
            IFormFile file = Request.Form.Files[0];
            // upload file
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0)
            {
                return Content("file not selected");
            }
            string sFileWillSave = file.FileName;
            //string fileDirect = @"/home/amnhacsaigon/dotnet/acs-be/wwwroot/file_upload/";
            string fileDirect = @"/home/amnhacsaigon/dotnet/hg-clone/wwwroot/file_upload/";
            if (isDev)
            {
                fileDirect = @"E:\data\";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
                        sheet = hssfwb.GetSheetAt(0);
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
                        sheet = hssfwb.GetSheetAt(0);
                    }
                    IRow headerRow = sheet.GetRow(0);
                    int cellCount = headerRow.LastCellNum;
                    // get header
                    for (int j = 0; j < cellCount; j++)
                    {
                        NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
                        // check null or space in row 1
                        //if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                    }
                    // get content
                    List<IRow> list_row = new List<IRow>();
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        //list_row.Add(row);
                        film_video_processing video_processing = film_video_processingSV.ImportExcel(row);
                        film_video_processingSV.UpdateOrInsertByExcel(video_processing);
                    }
                    //foreach (var row in list_row)
                    //{
                    //    // check update

                    //    //report rp = reportSV.ImportExcel(row);
                    //    //reportSV.UpdateOrInsertByExcel(rp);
                    //}
                }
            }
            return Json(new { result = "1" });
        }

        [Route("~/film_video_processing/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file(IFormFile file)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0)
                return Content("file not selected");
            string sFileWillSave = file.FileName;
            //sFileWillSave = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + CommService.TrimVietnameseMark(sFileWillSave, true, true);
            string fileDirect = @"/home/www/data/data/saigonmusic/hg_project/";
            if (isDev)
            {
                fileDirect = @"C:\data\";
            }
            else
            {
                //string mode = LamltService.GetConnectionString("mode");
                //if (mode == "docker") fileDirect = "/app/wwwroot/hg_project/";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect);
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            string renameFile = Convert.ToString("file_" + DateTime.Now.Ticks.ToString()) + "." + fileName.Split('.').Last();
            var fullPath = Path.Combine(path, renameFile);
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            return Json(new { file = "/hg_project/" + renameFile });


            //return Json(new { file = "http://27.76.152.255:13000/hg_project/" + sFileWillSave });
            //return Json(new { file = fileDirect + sFileWillSave });
        }
        [Route("~/film_video_processing/upload_file_tran")]
        [HttpPost]
        public async Task<IActionResult> upload_file_tran(IFormFile file, string fromlang, string tolang, string service)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0)
                return Content("file not selected");
            //string sFileWillSave = file.FileName;
            //sFileWillSave = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + CommService.TrimVietnameseMark(sFileWillSave, true, true);
            string fileDirect = @"/home/amnhacsaigon/dotnet/hg-clone/wwwroot/translate_file/";
            if (isDev)
            {
                fileDirect = @"C:\data\";
            }
            else
            {
                //string mode = LamltService.GetConnectionString("mode");
                //if (mode == "docker") fileDirect = "/app/wwwroot/hg_project/";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }

            //var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect);
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            int tranid = 0;
            try
            {
                tranid = new translateService().UpdateOrInsert(new translate()
                {
                    title = $"{Guid.NewGuid().ToString().Substring(0, 6)}_{fileName}",
                    status = "Đang dịch"
                });
            }
            catch (Exception ex)
            {
                return Json(new { data = "Lỗi tạo mới " + ex.Message }); ;
            }

            //string renameFile = Convert.ToString("file_" + DateTime.Now.Ticks.ToString()) + "." + fileName.Split('.').Last();
            string renameFile = Convert.ToString(tranid + "_" + fromlang + "_" + tolang + "_" + service + "." + fileName.Split('.').Last());
            var fullPath = Path.Combine(fileDirect, renameFile);
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            return Json(new { data = "Đặt lệnh thành công" }); ;
        }


        // ACTION speech to text
        [HttpPost]
        [Route("~/film_video_processing/SearchLyric")]
        public JsonResult SearchLyric(string list_selected, string from)
        {
            if (!string.IsNullOrEmpty(list_selected))
            {
                bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string folder = @"/home/amnhacsaigon/dotnet/hg-clone/wwwroot/search_lyric/";
                if (isDev)
                {
                    folder = @"C:/search_lyric/";
                }
                //string mode = LamltService.GetConnectionString("mode");
                //if (mode == "docker") folder = "/app/wwwroot/search_lyric/";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                foreach (var processingId in list_selected.Split(","))
                {
                    var fileTxt = folder + processingId + "_" + from + ".txt";
                    if (System.IO.File.Exists(fileTxt))
                    {
                        System.IO.File.Delete(fileTxt);
                    }
                    using (StreamWriter sw = new StreamWriter(fileTxt, true, Encoding.ASCII))
                    {
                        var obj = new film_video_processingService().GetViewByID(processingId);
                        if (obj != null)
                        {
                            sw.Write(obj.title + " - " + obj.actor + "\n" + obj.upload_file);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
                return Json("Tạo lệnh tìm kiếm Lyric thành công");
            }
            else
            {
                return Json("Chưa chọn file nào");
            }
        }
        // ACTION speech to text
        [HttpPost]
        [Route("~/film_video_processing/SaveLyricGoogle")]
        public JsonResult SaveLyricGoogle(int id, string lyric_google)
        {
            try
            {
                bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string folder = @"/home/www/data/data/saigonmusic/hg_project_convert/file_gseach/";
                if (isDev)
                {
                    folder = @"C:/file_gseach/";
                }
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                var fileTxt = folder + id + "_en.txt";
                if (System.IO.File.Exists(fileTxt))
                {
                    System.IO.File.Delete(fileTxt);
                }
                using (StreamWriter sw = new StreamWriter(fileTxt, true, Encoding.ASCII))
                {
                    sw.Write(lyric_google);
                    sw.Flush();
                    sw.Close();
                }
                return Json("Lưu Lyric thành công");
            }
            catch (Exception ex)
            {
                return Json("Lưu Lyric thất bại: " + ex.Message);
            }
        }



    }
}