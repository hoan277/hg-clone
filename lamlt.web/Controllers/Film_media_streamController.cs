﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_media_stream")]
    public class film_media_streamController : Controller
    {
        // GET: film_media_streams
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_media_stream/List")]
        public ActionResult List(int length, int start, string search, int cpid, int status, int catalogid, string start_time, string end_time)
        {
            film_media_streamService svrfilm_media_stream = (new film_media_streamService());
            List<vw_film_media_stream> data = svrfilm_media_stream.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, start_time = start_time, end_time = end_time });
            int recordsTotal = (int)svrfilm_media_stream.CountAll(new PagingModel() { offset = start, limit = length, search = search, start_time = start_time, end_time = end_time });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        //public ActionResult List(int length, int start, string search, int cpid, int status, int catalogid, string start_time, string end_time)
        //{
        //    film_video_statisticService svrfilm_video_statistic = (new film_video_statisticService());
        //    List<vw_film_video_statistic> data = svrfilm_video_statistic.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
        //    int recordsTotal = (int)svrfilm_video_statistic.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
        //    int recordsFiltered = recordsTotal;
        //    int draw = 1;
        //    StringValues valueDraw = "";
        //    HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
        //    try { draw = int.Parse(valueDraw); } catch { }
        //    return Json(new
        //    {
        //        draw,
        //        recordsTotal,
        //        recordsFiltered,
        //        data
        //    });
        //}


        [HttpGet]
        [Route("~/film_media_stream/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_media_stream obj = (new film_media_streamService()).GetByID(id);
            if (obj == null) obj = (new film_media_streamService()).InitEmpty();
            return PartialView("film_media_streamEdit", obj);
        }
        [HttpPost]
        [Route("~/film_media_stream/Update")]
        public ActionResult Update(film_media_stream obj)
        {
            int result = new film_media_streamService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_media_stream/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_media_streamService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_media_stream/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_media_stream obj = new film_media_streamService().GetByID(id);
            return PartialView("film_media_streamDetail", obj);
        }
        [HttpGet]
        [Route("~/film_media_stream/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_media_stream obj = new film_media_streamService().GetViewByID(id);
            return PartialView("film_media_streamDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_media_stream/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_media_streamService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}