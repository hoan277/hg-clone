﻿using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_converting")]
    public class api_film_convertingController : Controller
    {
        [HttpPost]
        [Route("~/api/film_converting/update")]
        public ActionResult Update([FromBody] film_model obj) //, string Id, string title, DateTime start_time)
        {
            //get params
            // path
            StringValues s_path;
            Request.Query.TryGetValue("path", out s_path);
            // film_type
            StringValues s_film_type;
            Request.Query.TryGetValue("film_type", out s_film_type);
            // status
            StringValues s_status;
            Request.Query.TryGetValue("status", out s_status);
            // title
            StringValues s_title;
            Request.Query.TryGetValue("title", out s_title);
            // trainer
            StringValues s_trainer;
            Request.Query.TryGetValue("trainer", out s_trainer);
            // trainer
            StringValues s_duration;
            Request.Query.TryGetValue("duration", out s_duration);

            string path = s_path;// obj.path;
            if (path == null) { return Json(new { result = -1 }); };
            string film_type = s_film_type;// obj.film_type;
            if (film_type == null) { return Json(new { result = -2 }); }
            string status = s_status;// obj.status;
            string title = s_title;// obj.title;
            string trainer = s_trainer;
            string duration = s_duration;
            // Rename extension path to .smil
            if (path.Contains(".mp4")) { path = path.Replace(".mp4", ".smil"); }
            if (path.Contains(".MP4")) { path = path.Replace(".MP4", ".smil"); }
            if (path.Contains(".mov")) { path = path.Replace(".mov", ".smil"); }
            if (path.Contains(".MOV")) { path = path.Replace(".MOV", ".smil"); }
            if (path.Contains(".mkv")) { path = path.Replace(".mkv", ".smil"); }
            if (path.Contains(".MKV")) { path = path.Replace(".MKV", ".smil"); }

            if (title == null) { title = ""; }
            //else title = obj.title;
            if (obj == null) { obj = new film_model(); };
            if (film_type == "1")
            {
                int result1 = new film_convertingService().UpdateByPath(path, int.Parse(status), path);
                int film_type1 = int.Parse(film_type);
                int result2 = new film_productService().UpdateByConvert(path, int.Parse(status));
                return Json(new { result = result2 });
            }
            else if (film_type == "3")
            {
                int film_type1 = int.Parse(film_type);
                // update trainer
                int result3 = new film_videoService().UpdateVideoTrainer(path, s_trainer, int.Parse(status));
                return Json(new { result = result3 });
            }
            //else if (film_type == "4")
            //{
            //    if (path == null) { return Json(new { result = -1 }); }
            //    else
            //    {
            //        int film_type1 = int.Parse(film_type);
            //        int result5 = new film_videoService().GetVideoByPath(path);
            //        return Json(new { result = result5 });
            //    }
            //}
            else
            {
                return Json(new { result = -1 });
            }

        }

        //[HttpGet]
        [HttpGet]
        [Route("~/api/film_converting/update")]
        public ActionResult Update1([FromBody] film_model obj) //, string Id, string title, DateTime start_time)
        {
            //get params
            StringValues s_status;
            Request.Query.TryGetValue("status", out s_status);
            StringValues s_path;
            Request.Query.TryGetValue("path", out s_path);
            StringValues s_title;
            Request.Query.TryGetValue("title", out s_title);
            StringValues s_film_type;
            Request.Query.TryGetValue("film_type", out s_film_type);
            StringValues s_trainer;
            Request.Query.TryGetValue("trainer", out s_trainer);
            string title = s_title;// obj.title;
            string path = s_path;// obj.path;
            string film_type = s_film_type;// obj.film_type;
            if (film_type == null && path == null)
            {
                return Json(new { result = -4, msg = "Thiếu file_type và path" });
            };
            if (film_type == null) { return Json(new { result = -2, msg = "Thiếu file_type" }); }

            string status = s_status;// obj.status;
            string trainer = s_trainer;
            if (path == null) { return Json(new { result = -1, msg = "Thiếu path" }); };

            // Rename extension path to .smil
            if (path.Contains(".mp4")) { path = path.Replace(".mp4", ".smil"); }
            if (path.Contains(".MP4")) { path = path.Replace(".MP4", ".smil"); }
            if (path.Contains(".mov")) { path = path.Replace(".mov", ".smil"); }
            if (path.Contains(".MOV")) { path = path.Replace(".MOV", ".smil"); }
            if (path.Contains(".mkv")) { path = path.Replace(".mkv", ".smil"); }
            if (path.Contains(".MKV")) { path = path.Replace(".MKV", ".smil"); }

            if (title == null) { title = ""; }
            if (obj == null) { obj = new film_model(); };
            if (film_type == "4")
            {
                int film_type1 = int.Parse(film_type);
                int result5 = new film_videoService().GetVideoByPath(path);
                if (result5 == 1)
                {
                    return Json(new { result = result5, msg = "Tồn tại" });
                }
                else if (result5 == 0)
                {
                    return Json(new { result = result5, msg = "Không tồn tại" });
                }
                else
                {
                    return Json(new { result = result5, msg = "Lỗi" });
                }
            }
            else
            {
                return Json(new { result = -3, msg = "Sai file_type" });
            }

        }
        [HttpGet]
        [Route("~/api/film_converting/update_image")]
        public ActionResult UpdateImage(string path, string status) //, string Id, string title, DateTime start_time)
        {
            int result2 = new film_videoService().UpdateImageAuto(path, int.Parse(status));
            return Json(new { result = result2 });
        }
    }
    public class film_model
    {
        public string path { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public string film_type { get; set; }
        public string catalog { get; set; }
        public string code { get; set; }
        public string year { get; set; }
        public string actor { get; set; }

        public string director { get; set; }
        public string price { get; set; }
        public string tags { get; set; }
        public string directory { get; set; }
        public string desc { get; set; }
        public string trainer { get; set; }

    }
}