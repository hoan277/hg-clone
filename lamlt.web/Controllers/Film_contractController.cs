﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_contract")]
    public class Film_contractController : Controller
    {
        [HttpGet]
        [Route("~/film_contract/GetByFilmId")]
        public JsonResult GetByFilmId(int film_id)
        {
            var obj = new Film_contractService().GetByFilmID_List(film_id);
            return Json(obj);
        }

        [HttpPost]
        [Route("~/film_contract/UpdateAPI")]
        public ActionResult Update(film_contract obj)
        {
            int result = new Film_contractService().UpdateOrInsert_API(obj);
            return Json(new { result = result });
        }
    }
}