﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video_processing_transcript")]
    public class film_video_processing_transcriptController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        // ACTION LIST
        [HttpGet]
        [Route("~/film_video_processing_transcript/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            var sv_film_video_processing_transcript = (new film_video_processing_transcriptService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_video_processing_transcript> data = sv_film_video_processing_transcript.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_video_processing_transcript.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_video_processing_transcript/Edit/{id}")]
        public PartialViewResult film_video_processing_transcriptEdit([FromRoute] string id)
        {
            var obj = (new film_video_processing_transcriptService()).GetByID(id);
            if (obj == null) obj = (new film_video_processing_transcriptService()).InitEmpty();
            return PartialView("film_video_processing_transcriptEdit", obj);
        }

        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_video_processing_transcript/GetViewDetail/{id}")]
        public PartialViewResult film_video_processing_transcriptDetail([FromRoute] string id)
        {
            var obj = new film_video_processing_transcriptService().GetViewByID(id);
            return PartialView("film_video_processing_transcriptDetail", obj);
        }

        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_video_processing_transcript/Update")]
        public ActionResult Update(film_video_processing_transcript obj)
        {
            var film_video_processing_transcriptSV = new film_video_processing_transcriptService();
            int result = film_video_processing_transcriptSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_video_processing_transcript/UpdateStatus")]
        public ActionResult UpdateStatus(int id, string status)
        {
            var film_video_processing_transcriptSV = new film_video_processing_transcriptService();
            return Json(film_video_processing_transcriptSV.UpdateStatus(id, status));
        }

        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_video_processing_transcript/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_video_processing_transcriptService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [Route("~/film_video_processing_transcript/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file(IFormFile file)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0) return Content("file not selected");
            string sFileWillSave = file.FileName;
            string fileDirect = @"/home/amnhacsaigon/dotnet/hg-project/wwwroot/file_effect/";
            //string fileDirect = @"/home/amnhacsaigon/dotnet/acs-be/wwwroot/copyright/";
            if (isDev)
            {
                fileDirect = @"D:\data\";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return Json(new { file = "http://183.81.35.24:5030/file_effect/" + sFileWillSave });
        }

        // for product edit
        [HttpGet]
        [Route("~/film_video_processing_transcript/list_by_video/{video_id}")]
        public PartialViewResult film_video_processing_transcriptListByVideoId([FromRoute] string video_id)
        {
            return PartialView("film_video_processing_transcriptListByVideoId", video_id);
        }

        [HttpGet]
        [Route("~/film_video_processing_transcript/getListByvideo_processing_id/{video_processing_id}")]
        public ActionResult getListByvideo_processing_id(int length, int start, [FromRoute] string video_processing_id)
        {
            var film_video_processing_transcriptSV = (new film_video_processing_transcriptService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                video_processing_id = int.Parse(video_processing_id)
            };
            var data = film_video_processing_transcriptSV.GetViewAllItemByvideo_processing_id(paging);
            int recordsTotal = (int)film_video_processing_transcriptSV.CountViewAllItemByvideo_processing_id(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpPost]
        [Route("~/film_video_processing_transcript/UpdateByFilm")]
        public ActionResult UpdateByFilm(film_video_processing_transcript obj, string array_transcript)
        {
            int result = 1;
            if (!string.IsNullOrEmpty(array_transcript))
            {
                string[] arr = array_transcript.Split(",");
                foreach (var a in arr)
                {
                    obj.transcript_id = int.Parse(a);
                    result = new film_video_processing_transcriptService().UpdateOrInsert(obj);
                }
                return Json(new { result = result });
            }
            return Json(new { result = -1 });
        }
    }
}