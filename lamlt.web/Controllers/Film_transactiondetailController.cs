﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_transactiondetail")]
    public class film_transactiondetailController : Controller
    {
        // GET: film_transactiondetails
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_transactiondetail/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_transactiondetailService svrfilm_transactiondetail = (new film_transactiondetailService());
            List<vw_film_transactiondetail> data = svrfilm_transactiondetail.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_transactiondetail.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_transactiondetail/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_transactiondetail obj = (new film_transactiondetailService()).GetByID(id);
            if (obj == null) obj = (new film_transactiondetailService()).InitEmpty();
            return PartialView("film_transactiondetailEdit", obj);
        }
        [HttpPost]
        [Route("~/film_transactiondetail/Update")]
        public ActionResult Update(film_transactiondetail obj)
        {
            int result = new film_transactiondetailService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_transactiondetail/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_transactiondetailService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_transactiondetail/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_transactiondetail obj = new film_transactiondetailService().GetByID(id);
            return PartialView("film_transactiondetailDetail", obj);
        }
        [HttpGet]
        [Route("~/film_transactiondetail/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_transactiondetail obj = new film_transactiondetailService().GetViewByID(id);
            return PartialView("film_transactiondetailDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_transactiondetail/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_transactiondetailService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}