﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_product")]
    public class film_productController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        // ACTION LIST
        [HttpGet]
        [Route("~/film_product/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_productService sv_film_product = (new film_productService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_product> data = sv_film_product.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_product.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_product/Edit/{id}")]
        public ActionResult film_productEdit([FromRoute] string id)
        {
            film_product obj = (new film_productService()).GetByID(id);
            if (obj == null)
            {
                obj = (new film_productService()).InitEmpty();
                List<film_isrc> lists = new film_isrcService().get(0, 0, 0);
                int current_code = lists[0].current_code + 1;
                obj.code = "VNA0D" + DateTime.Now.ToString("yy") + current_code;
            }
            return View("film_productEdit", obj);
        }

        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_product/GetViewDetail/{id}")]
        public PartialViewResult film_productDetail([FromRoute] string id)
        {
            vw_film_product obj = new film_productService().GetViewByID(id);
            return PartialView("film_productDetail", obj);
        }

        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_product/Update")]
        public ActionResult Update(film_product obj)
        {
            film_productService film_productSV = new film_productService();
            int result = film_productSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_product/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_productService film_productSV = new film_productService();
            int result = film_productSV.UpdateStatus(id);
            return Json(new { result = result });
        }

        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_product/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_productService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        // ACTION CONVERT
        [HttpPost]
        [Route("~/film_product/convert/{id}")]
        public ActionResult ConvertProduct(int id)
        {
            PagingModel paging = new PagingModel()
            {
                product_id = id
            };
            film_productService film_productSV = new film_productService();
            int result = film_productSV.convert_product(paging);
            return Json(new { result = result });
        }

        // ACTION CONVERT
        [HttpGet]
        [Route("~/film_product/test")]
        public ActionResult ConvertProduct123()
        {
            film_video_processingService film_productSV = new film_video_processingService();
            List<vw_film_video_processing> result = film_productSV.GetViewAllItem(new PagingModel() { offset = 0, limit = 100 });
            List<vw_film_video_processing> result1 = new List<vw_film_video_processing>();
            foreach (var item in result)
            {
                string file = item.upload_file;
                file = file.Replace("http://27.76.152.255:13000/hg_project/", "/home/www/data/data/saigonmusic/hg_project/");
                //file = file.Replace("https://files.lalatv.com.vn:9090/hg_project/", "/home/www/data/data/saigonmusic/hg_project/");
                if (!System.IO.File.Exists(file))
                {
                    item.upload_file = file;
                    result1.Add(item);
                }
            }
            return Json(new { result = result1 });
        }

        #region =====================================  XUẤT FILE BẰNG CLOSED XML ============================

        [HttpPost]
        [Route("~/film_product/export_list/{product_id}")]
        public JsonResult export_list(int product_id)
        {
            string fileName = "List_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            List<vw_film_video_processing> obj = new film_productService().getAllVideoProcessingByProductId(product_id);
            if (obj.Count > 0)
            {
                DataTable dt = new DataTable { TableName = "Phim" };
                dt.Columns.Add("STT", typeof(int));
                dt.Columns.Add("Tên Video", typeof(string));
                dt.Columns.Add("Thời lượng", typeof(string));
                dt.Columns.Add("Mã code", typeof(string));
                dt.Columns.Add("Nhạc sĩ", typeof(string));
                dt.Columns.Add("Diễn viên", typeof(string));
                dt.Columns.Add("Ngày tạo", typeof(string));
                //Add Rows in DataTable
                int stt = 1;
                foreach (var item in obj)
                {
                    dt.Rows.Add(stt, item.title, item.duration, item.code, item.director, item.actor, item.datecreated);
                    stt++;
                }
                dt.AcceptChanges();
                dt.DefaultView.Sort = "Tên Video ASC";
                bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string folderPath = "/home/amnhacsaigon/dotnet/hg-project/wwwroot/export_list/";
                if (isDev)
                {
                    folderPath = "C://";
                }
                string mode = LamltService.GetConnectionString("mode");
                if (mode == "docker") folderPath = "/app/wwwroot/export_list/";
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "Sheet 1");
                    ws.Style.Font.Bold = true;
                    // Set the width of all columns in the worksheet
                    ws.Column("1").Width = 5;
                    ws.Column("2").Width = 50;
                    ws.Column("3").Width = 15;
                    ws.Column("4").Width = 40;
                    ws.Column("5").Width = 15;
                    ws.Column("6").Width = 20;
                    ws.Column("7").Width = 15;
                    ws.Row(1).Height = 20;
                    ws.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                    ws.Row(1).Style.Font.FontColor = XLColor.Red;
                    ws.Row(1).Style.Font.SetFontSize(12);
                    ws.Row(1).Height = 30;
                    ws.Rows().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    ws.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    ws.Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    //ws.Rows().Style.Alignment.WrapText = true;
                    //ws.Rows().AdjustToContents();
                    wb.SaveAs(folderPath + fileName);
                    string rs = "/export_list/" + fileName;
                    return Json(new { data = rs });
                }
            }
            return Json(new { data = -1 });
        }

        [HttpPost]
        [Route("~/film_product/exportvialib")]
        public JsonResult exportvialib(string type, string list_id)
        {
            var allProduct = new film_productService().GetViewAllItem(null);
            string fileName = "Product_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string folderPath = "/home/amnhacsaigon/dotnet/hg-project/wwwroot/export_transcript/";
            if (isDev)
            {
                folderPath = "C://";
            }
            string mode = LamltService.GetConnectionString("mode");
            if (mode == "docker") folderPath = "/app/wwwroot/export_transcript/";
            int stt = 1;
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            XLWorkbook wb = new XLWorkbook();
            var ws = wb.Worksheets.Add("Sheet 1");
            ws.Cell("A1").Value = "STT";
            ws.Cell("B1").Value = "Tên Video";
            ws.Cell("C1").Value = "Code";
            ws.Cell("D1").Value = "Link";
            ws.Cell("E1").Value = "Kênh";
            ws.Cell("F1").Value = "Ngày tạo";
            ws.Cell("G1").Value = "Danh sách file";
            ws.Cell("H1").Value = "Duration";
            ws.Cell("I1").Value = "Thời lượng";
            ws.Cell("J1").Value = "ISRC";
            ws.Cell("K1").Value = "Nhạc sĩ";
            ws.Cell("L1").Value = "Diễn viên";
            // Set the width of all columns in the worksheet
            ws.Column("1").Width = 5;
            ws.Column("2").Width = 50;
            ws.Column("3").Width = 15;
            ws.Column("4").Width = 50;
            ws.Column("5").Width = 50;
            ws.Column("6").Width = 20;
            ws.Column("7").Width = 50;
            ws.Column("8").Width = 10;
            ws.Column("9").Width = 20;
            ws.Column("10").Width = 20;
            ws.Column("11").Width = 30;
            ws.Column("12").Width = 30;
            ws.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
            ws.Row(1).Style.Font.SetBold();
            ws.Row(1).Style.Font.SetFontColor(XLColor.Red);
            ws.Row(1).Style.Font.SetFontSize(12);
            ws.Row(1).Height = 30;
            ws.Row(1).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            ws.Rows().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            if (type == "dachon")
            {
                List<vw_film_product> selected_obj = new List<vw_film_product>();
                var strlist = list_id.Split(",");
                foreach (var all_data in allProduct)
                {
                    foreach (var selected_id in strlist)
                    {
                        if (selected_id == all_data.Id.ToString())
                        {
                            selected_obj.Add(all_data);
                        }
                    }
                }
                allProduct = selected_obj;
            }
            int current_row = 0;
            int merge_row = 1;
            List<vw_film_video_processing> lstProcess = new List<vw_film_video_processing>();
            //Add Rows in DataTable
            for (int i = 0; i < allProduct.Count; i++)
            {
                var lists = new film_productService().getAllVideoProcessingByProductId(allProduct[i].Id);
                if (lists.Count > 0)
                {
                    lstProcess.Clear();
                    foreach (var item in lists)
                    {
                        if (item != null)
                        {
                            lstProcess.Add(item);
                        }
                    }
                }
                current_row = merge_row + 1;
                merge_row = merge_row + lstProcess.Count;
                if (ws.Cell("G" + (current_row - 1)).IsEmpty())
                {
                    current_row--;
                    merge_row--;
                }
                if (lstProcess.Count > 1)
                {
                    ws.Cell("A" + current_row).Value = stt;
                    ws.Cell("B" + current_row).Value = allProduct[i].title;
                    ws.Cell("C" + current_row).Value = allProduct[i].code;
                    ws.Cell("D" + current_row).Value = allProduct[i].path;
                    ws.Cell("E" + current_row).Value = allProduct[i].channel_title;
                    ws.Cell("F" + current_row).Value = allProduct[i].datecreated;
                    ws.Range("A" + current_row + ":A" + merge_row).Column(1).Merge();
                    ws.Range("A" + current_row + ":A" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("B" + current_row + ":B" + merge_row).Column(1).Merge();
                    ws.Range("B" + current_row + ":B" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("C" + current_row + ":C" + merge_row).Column(1).Merge();
                    ws.Range("C" + current_row + ":C" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("D" + current_row + ":D" + merge_row).Column(1).Merge();
                    ws.Range("D" + current_row + ":D" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("E" + current_row + ":E" + merge_row).Column(1).Merge();
                    ws.Range("E" + current_row + ":E" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("F" + current_row + ":F" + merge_row).Column(1).Merge();
                    ws.Range("F" + current_row + ":F" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    int currentDuration = 0;
                    for (int j = 0; j < lstProcess.Count; j++)
                    {
                        string duration;
                        currentDuration = lstProcess[j] != null ? lstProcess[j].duration.Value : 0;
                        if (j == 0)
                        {
                            duration = TimeSpan.FromSeconds(double.Parse(lstProcess[j].duration.ToString())).ToString();
                        }
                        else
                        {
                            duration = TimeSpan.FromSeconds(double.Parse(currentDuration.ToString())).ToString();
                        }
                        ws.Cell("G" + (j + current_row)).Value = lstProcess[j].title;
                        ws.Cell("H" + (j + current_row)).Value = lstProcess[j].duration;
                        ws.Cell("I" + (j + current_row)).Value = "'" + duration;
                        ws.Cell("J" + (j + current_row)).Value = lstProcess[j].code;
                        ws.Cell("K" + (j + current_row)).Value = lstProcess[j].director;
                        ws.Cell("L" + (j + current_row)).Value = lstProcess[j].actor;
                        if (j == lstProcess.Count - 1)
                        {
                            ws.Cell("G" + (j + current_row)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                            ws.Cell("H" + (j + current_row)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                            ws.Cell("I" + (j + current_row)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                            ws.Cell("J" + (j + current_row)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                            ws.Cell("K" + (j + current_row)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                            ws.Cell("L" + (j + current_row)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        }
                    }
                }
                else
                {
                    #region ===================== Cập nhật 1 dòng dữ liệu ========================

                    var chua_cap_nhat = "Chưa cập nhật";
                    ws.Cell("A" + current_row).Value = stt;
                    ws.Cell("B" + current_row).Value = allProduct[i].title;
                    ws.Cell("C" + current_row).Value = allProduct[i].code;
                    ws.Cell("C" + current_row).Value = allProduct[i].path;
                    ws.Cell("D" + current_row).Value = allProduct[i].channel_title;
                    ws.Cell("E" + current_row).Value = allProduct[i].datecreated;
                    ws.Range("A" + current_row + ":A" + merge_row).Column(1).Merge();
                    ws.Range("A" + current_row + ":A" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("B" + current_row + ":B" + merge_row).Column(1).Merge();
                    ws.Range("B" + current_row + ":B" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("C" + current_row + ":C" + merge_row).Column(1).Merge();
                    ws.Range("C" + current_row + ":C" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("D" + current_row + ":D" + merge_row).Column(1).Merge();
                    ws.Range("D" + current_row + ":D" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("E" + current_row + ":E" + merge_row).Column(1).Merge();
                    ws.Range("E" + current_row + ":E" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Range("F" + current_row + ":F" + merge_row).Column(1).Merge();
                    ws.Range("F" + current_row + ":F" + merge_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    if (lstProcess.Count == 1)
                    {
                        ws.Cell("G" + current_row).Value = !string.IsNullOrEmpty(lstProcess[0].title) ? lstProcess[0].title : chua_cap_nhat;
                        ws.Cell("H" + current_row).Value = !string.IsNullOrEmpty(lstProcess[0].duration.Value.ToString()) ? lstProcess[0].duration.Value.ToString() : chua_cap_nhat;
                        ws.Cell("I" + current_row).Value = !string.IsNullOrEmpty(lstProcess[0].duration.Value.ToString()) ? TimeSpan.FromSeconds(double.Parse(lstProcess[0].duration.Value.ToString())).ToString() : chua_cap_nhat;
                        ws.Cell("J" + current_row).Value = !string.IsNullOrEmpty(lstProcess[0].code) ? lstProcess[0].code : chua_cap_nhat;
                        ws.Cell("K" + current_row).Value = !string.IsNullOrEmpty(lstProcess[0].director) ? lstProcess[0].director : chua_cap_nhat;
                        ws.Cell("L" + current_row).Value = !string.IsNullOrEmpty(lstProcess[0].actor) ? lstProcess[0].actor : chua_cap_nhat;
                    }
                    else
                    {
                        ws.Cell("G" + current_row).Value = chua_cap_nhat;
                        ws.Cell("H" + current_row).Value = chua_cap_nhat;
                        ws.Cell("I" + current_row).Value = chua_cap_nhat;
                        ws.Cell("J" + current_row).Value = chua_cap_nhat;
                        ws.Cell("K" + current_row).Value = chua_cap_nhat;
                        ws.Cell("L" + current_row).Value = chua_cap_nhat;
                    }
                    string[] cols = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };
                    foreach (var item in cols)
                    {
                        ws.Cell(item + current_row).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    }

                    #endregion ===================== Cập nhật 1 dòng dữ liệu ========================

                    current_row = current_row + 1;
                    merge_row = merge_row + 1;
                }
                lstProcess = new List<vw_film_video_processing>();
                stt++;
            }
            ws.RangeUsed().SetAutoFilter();
            wb.SaveAs(folderPath + fileName);
            string rs = "/export_transcript/" + fileName;
            return Json(new { data = rs });
        }

        #endregion =====================================  XUẤT FILE BẰNG CLOSED XML ============================
    }
}