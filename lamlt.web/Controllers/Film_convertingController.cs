﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_converting")]
    public class film_convertingController : Controller
    {
        // GET: film_convertings
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        [HttpGet]
        [Route("~/film_converting/List")]
        public ActionResult List(int length, int start, string search, int status)
        {
            film_convertingService svrfilm_converting = (new film_convertingService());
            List<vw_film_converting> data = svrfilm_converting.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, status = status });
            int recordsTotal = (int)svrfilm_converting.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_converting/Edit/{id}")]
        public PartialViewResult film_convertingEdit([FromRoute] string id)
        {
            film_converting obj = (new film_convertingService()).GetByID(id);
            if (obj == null) obj = (new film_convertingService()).InitEmpty();
            return PartialView("film_convertingEdit", obj);
        }
        [HttpPost]
        [Route("~/film_converting/Update")]
        public ActionResult Update(film_converting obj)
        {
            int result = new film_convertingService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_converting/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_convertingService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_converting/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_converting obj = new film_convertingService().GetByID(id);
            return PartialView("film_convertingDetail", obj);
        }
        [HttpGet]
        [Route("~/film_converting/GetViewDetail/{id}")]
        public PartialViewResult film_convertingDetail(string id)
        {
            vw_film_converting obj = new film_convertingService().GetViewByID(id);
            return PartialView("film_convertingDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_converting/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_convertingService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_converting/update_pid")]
        public JsonResult update_pid(int id, int pid, string type)
        {
            //if (id < 0) return Json(new { code = "error", message = "ID không đươc để trống" });
            //if (pid < 0) return Json(new { code = "error", message = "Process id không đươc để trống" });
            Dictionary<object, object> dict = new film_convertingService().update_pid(id, pid, type);
            return Json(dict);
        }
        [HttpPost]
        [Route("~/film_converting/create_pid_txt")]
        public JsonResult create_pid_txt(int pid)
        {
            if (pid < 0) return Json(new { code = "error", message = "Process id không đươc để trống" });
            Dictionary<object, object> dict = new film_convertingService().create_pid_txt(pid);
            return Json(dict);
        }
    }
}