﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video_image")]
    public class film_video_imageController : Controller
    {
        private IConfiguration webconfig;
        public film_video_imageController(IConfiguration iConfig)
        {
            webconfig = iConfig;
        }
        // GET: film_video_images
        [HttpGet]
        public ViewResult Index()
        {
            return View("Index");
        }

        [HttpGet]
        [Route("~/film_video_image/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_video_imageService svrfilm_video_image = (new film_video_imageService());
            List<film_video_image> data = svrfilm_video_image.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_video_image.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_video_image/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video_image obj = (new film_video_imageService()).GetByID(id);
            if (obj == null) obj = (new film_video_imageService()).InitEmpty();
            return PartialView("film_video_imageEdit", obj);
        }
        [HttpPost]
        [Route("~/film_video_image/Update")]
        public ActionResult Update(film_video_image obj)
        {
            string pathImages = webconfig["pathImage"];
            int result = new film_video_imageService().UpdateOrInsert(obj, pathImages);
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/film_video_image/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video_image obj = new film_video_imageService().GetByID(id);
            return PartialView("film_video_imageDetail", obj);
        }
        [HttpGet]
        [Route("~/film_video_image/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video_image obj = new film_video_imageService().GetViewByID(id);
            return PartialView("film_video_imageDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_video_image/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_video_imageService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}