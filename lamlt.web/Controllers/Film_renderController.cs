﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using NPOI.XSSF.UserModel;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_render")]
    public class film_renderController : Controller
    {
        // GET: film_renders
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_render/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_renderService svrfilm_render = (new film_renderService());
            List<vw_film_render> data = svrfilm_render.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_render.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_render/get")]
        public Dictionary<object, object> get(int id, string title, string status)
        {
            return new film_renderService().get(id, title, status);
        }

        [HttpGet]
        [Route("~/film_render/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_render obj = (new film_renderService()).GetByID(id);
            if (obj == null) obj = (new film_renderService()).InitEmpty();
            return PartialView("film_renderEdit", obj);
        }

        [HttpPost]
        [Route("~/film_render/Update")]
        public ActionResult Update(film_render obj)
        {
            int result = new film_renderService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/film_render/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_render obj = new film_renderService().GetByID(id);
            return PartialView("film_renderDetail", obj);
        }

        [HttpGet]
        [Route("~/film_render/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_render obj = new film_renderService().GetViewByID(id);
            return PartialView("film_renderDetail", obj);
        }

        [HttpDelete]
        [Route("~/film_render/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_renderService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/film_render/import_excel")]
        public JsonResult import_excel()
        {
            IFormFile file = Request.Form.Files[0];
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string upload_dir = isDev ? @"C:\file_upload" : @"/home/amnhacsaigon/dotnet/hg-project/wwwroot/file_upload/";
            string mode = LamltService.GetConnectionString("mode");
            if (mode == "docker") upload_dir = "/app/wwwroot/file_upload/";
            if (!Directory.Exists(upload_dir)) Directory.CreateDirectory(upload_dir);
            var path = Path.Combine(Directory.GetCurrentDirectory(), upload_dir, file.FileName);
            if (file.Length > 0)
            {
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    ISheet sheet = Path.GetExtension(file.FileName).ToLower() == ".xls" ? new HSSFWorkbook(stream).GetSheetAt(0) : new XSSFWorkbook(stream).GetSheetAt(0);
                    IRow headerRow = sheet.GetRow(0);
                    int cellCount = headerRow.LastCellNum;
                    for (int j = 0; j < cellCount; j++)
                    {
                        ICell cell = headerRow.GetCell(j);
                    }
                    List<IRow> list_row = new List<IRow>();
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        list_row.Add(row);
                    }
                    foreach (var row in list_row)
                    {
                        var channel_id = new film_channelService().GetByTitle(row.Cells[5].ToString()).Id;
                        new film_renderService().UpdateOrInsert(new film_render()
                        {
                            title = row.Cells[1].ToString(),
                            list_file = row.Cells[2].ToString(),
                            effect = int.Parse(row.Cells[3].ToString()),
                            thumb = row.Cells[4].ToString(),
                            channel_id = channel_id > 0 ? channel_id : 0,
                            yt_title = row.Cells[6].ToString(),
                            yt_desc = row.Cells[7].ToString(),
                            yt_mode = row.Cells[8].ToString(),
                            yt_schedule = DateTime.Parse(row.Cells[9].ToString()),
                            status = "render",
                            date_created = DateTime.Now,
                            date_update = DateTime.Now
                        });
                    }
                }
            }
            return Json(new { message = "success" });
        }
    }
}