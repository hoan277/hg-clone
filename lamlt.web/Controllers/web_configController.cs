﻿using lamlt.webservice.Services;
using Microsoft.AspNetCore.Mvc;

namespace lamlt.web.Controllers
{
    public class web_configController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [Route("/web_config/get")]
        [HttpGet]
        public JsonResult get(int id = 0, string key = "")
        {
            return Json(new web_configService().Get(id, key));
        }
        [Route("/web_config/update")]
        [HttpGet]
        public JsonResult update(string key = "", string value = "")
        {
            return Json(new web_configService().UpdateBykey(key, value));
        }
        [Route("/web_config/delete/{id}")]
        [HttpDelete]
        public JsonResult delete(int id)
        {
            return Json(new web_configService().Delete(id));
        }
        
    }
}
