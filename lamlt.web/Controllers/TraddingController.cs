﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/tradding")]
    public class traddingController : Controller
    {
        // GET: tradding
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/tradding/List")]
        public ActionResult List(int length, int start, string search)
        {
            traddingService svrtradding = (new traddingService());
            List<vw_tradding> data = svrtradding.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrtradding.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/tradding/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            tradding obj = (new traddingService()).GetByID(id);
            if (obj == null) obj = (new traddingService()).InitEmpty();
            return PartialView("traddingEdit", obj);
        }

        [HttpPost]
        [Route("~/tradding/Update")]
        public ActionResult Update(tradding obj)
        {
            int result = new traddingService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/tradding/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new traddingService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/tradding/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            tradding obj = new traddingService().GetByID(id);
            return PartialView("TraddingDetail", obj);
        }
        [HttpGet]
        [Route("~/tradding/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_tradding obj = new traddingService().GetViewByID(id);
            return PartialView("TraddingDetail", obj);
        }
        [HttpDelete]
        [Route("~/tradding/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new traddingService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}
