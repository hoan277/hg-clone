﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/ads")]
    public class adsController : Controller
    {
        // GET: ads
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/ads/List")]
        public ActionResult List(int length, int start, string search)
        {
            adsService svrad = (new adsService());
            List<vw_ads> data = svrad.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrad.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/ads/Edit/{id}")]
        public PartialViewResult adsEdit([FromRoute] string id)
        {
            ads obj = (new adsService()).GetByID(id);
            if (obj == null) obj = (new adsService()).InitEmpty();
            return PartialView("adsEdit", obj);
        }
        [HttpPost]
        [Route("~/ads/Update")]
        public ActionResult Update(ads obj)
        {
            int result = new adsService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/ads/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            ads obj = new adsService().GetByID(id);
            return PartialView("adsDetail", obj);
        }
        [HttpGet]
        [Route("~/ads/GetViewDetail/{id}")]
        public PartialViewResult adsDetail([FromRoute] string id)
        {
            vw_ads obj = new adsService().GetViewByID(id);
            return PartialView("adsDetail", obj);
        }
        [HttpDelete]
        [Route("~/ads/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new adsService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}