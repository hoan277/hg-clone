﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video_statistic")]

    public class film_video_statisticController : Controller
    {
        // GET: film_video_statistics
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        [HttpGet]
        [Route("~/film_video_statistic/List")]
        public ActionResult List(int length, int start, string search, int cpid, int status, int catalogid, string start_time, string end_time)
        {
            film_video_statisticService svrfilm_video_statistic = (new film_video_statisticService());
            List<vw_film_video_statistic> data = svrfilm_video_statistic.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
            int recordsTotal = (int)svrfilm_video_statistic.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
    }
}