﻿using ClosedXML.Excel;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/film_users_log_viettel")]
    public class film_users_log_viettelController : Controller
    {
        // GET: film_users_logs
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        #region ===== List film_users_log_viettel =====

        [HttpGet]
        [Route("~/film_users_log_viettel/List")]
        public ActionResult List(int length, int start, string search, int cpid, int sub_state, int sub_type, string start_time, string end_time)
        {
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                cpid = cpid,
                sub_state = sub_state,
                sub_type = sub_type,
                start_time = start_time,
                end_time = end_time
            };
            film_users_logService svrfilm_users_log = (new film_users_logService());
            List<vw_film_users_log> data = svrfilm_users_log.GetVw_Film_Users_Logs_Viettel(page);
            int recordsTotal = (int)svrfilm_users_log.CountVw_Film_users_log_viettel(page);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            string folderPath = "/home/amnhacsaigon/dotnet/cms-lalatv-viettel-v1/wwwroot/export_viettel/";
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (isDev)
            {
                folderPath = @"C:\";
            }
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fileName = "Details_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";

            // table 2
            DataTable dt2 = new DataTable
            {
                TableName = "Chi tiết Viettel"
            };
            dt2.Columns.Add("STT", typeof(string));
            dt2.Columns.Add("Username", typeof(string));
            dt2.Columns.Add("Email", typeof(string));
            dt2.Columns.Add("Phone", typeof(string));
            dt2.Columns.Add("Gói cước", typeof(string));
            dt2.Columns.Add("Thời gian đăng ký", typeof(string));
            dt2.Columns.Add("Thời gian gia hạn tiếp tục", typeof(string));
            dt2.Columns.Add("Thời gian tạo", typeof(string));
            dt2.Columns.Add("Trạng thái", typeof(string));
            int stt = 1;
            if (data.Count != 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    var sub_state_str = "";
                    switch (data[i].sub_state)
                    {
                        case 1: sub_state_str = "Đăng ký"; break;
                        case 2: sub_state_str = "Gia hạn"; break;
                        case 3: sub_state_str = "Hết hạn"; break;
                        case 4: sub_state_str = "Hủy"; break;
                        default: sub_state_str = "Unknown"; break;
                    }

                    var sub_type_str = "";
                    switch (data[i].sub_type)
                    {
                        case 1: sub_type_str = "Gói ngày"; break;
                        case 2: sub_type_str = "Gói tuần"; break;
                        case 3: sub_type_str = "Gói tháng"; break;
                        default: sub_state_str = "Unknown"; break;
                    }
                    var date_dk = "";
                    if (data[i].desc != null)
                    {
                        string[] words = data[i].desc.Split(',');
                        string date_dk_str = words[0].Split(":")[0];
                        date_dk = date_dk_str.Substring(6, 2) + "/" + date_dk_str.Substring(4, 2) + "/" + date_dk_str.Substring(0, 4) + " " + date_dk_str.Substring(8, 2) + ":" + date_dk_str.Substring(10, 2) + ":" + date_dk_str.Substring(12, 2);
                    }
                    var date_hb = "";
                    if (data[i].date_hb != null)
                    {
                        date_hb = data[i].date_hb.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    var datecreated = "";
                    if (data[i].datecreated != null)
                    {
                        datecreated = data[i].datecreated.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    dt2.Rows.Add(stt, data[i].username, data[i].email, data[i].phone, sub_type_str,
                                date_dk, date_hb, datecreated, sub_state_str);
                    stt++;
                }
                dt2.AcceptChanges();
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                //sheet 2

                var ws2 = wb.Worksheets.Add("Chi tiết Viettel");
                ws2.Style.Font.FontName = "Times New Roman";
                ws2.Style.Font.FontSize = 12;
                var table2 = ws2.Cell(1, 1).InsertTable(dt2);

                table2.Theme = XLTableTheme.None;
                table2.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                table2.Cells().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                ws2.Columns().AdjustToContents();

                // saves
                wb.SaveAs(folderPath + fileName);
                string rs = "/export_excel/" + fileName;
            }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        #endregion ===== List film_users_log_viettel =====

        #region ===== export data =====

        [HttpPost]
        [Route("~/film_users_log_viettel/export_data")]
        public JsonResult exportvialib(int length, int start, string search, int cpid, int sub_state, int sub_type, string start_time, string end_time, int type)
        {
            // type =1 đã chon | type = 0 tất cả
            if (type == 0) { length = 1000000; };
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                cpid = cpid,
                sub_state = sub_state,
                sub_type = sub_type,
                start_time = start_time,
                end_time = end_time
            };
            film_users_logService svrfilm_users_log = (new film_users_logService());
            List<vw_film_users_log> data = svrfilm_users_log.GetVw_Film_Users_Logs_Viettel(page);
            string result = svrfilm_users_log.ExportData(data);
            return Json(new { result });
        }

        #endregion ===== export data =====
    }
}