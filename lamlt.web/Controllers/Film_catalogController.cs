﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_catalog")]
    public class film_catalogController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        // ACTION LIST
        [HttpGet]
        [Route("~/film_catalog/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_catalogService sv_film_catalog = (new film_catalogService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_catalog> data = sv_film_catalog.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_catalog.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_catalog/Edit/{id}")]
        public PartialViewResult film_catalogEdit([FromRoute] string id)
        {
            film_catalog obj = (new film_catalogService()).GetByID(id);
            if (obj == null) obj = (new film_catalogService()).InitEmpty();
            return PartialView("film_catalogEdit", obj);
        }
        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_catalog/GetViewDetail/{id}")]
        public PartialViewResult film_catalogDetail([FromRoute] string id)
        {
            vw_film_catalog obj = new film_catalogService().GetViewByID(id);
            return PartialView("film_catalogDetail", obj);
        }
        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_catalog/Update")]
        public ActionResult Update(film_catalog obj)
        {
            film_catalogService film_catalogSV = new film_catalogService();
            int result = film_catalogSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_catalog/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_catalogService film_catalogSV = new film_catalogService();
            int result = film_catalogSV.UpdateStatus(id);
            return Json(new { result = result });
        }
        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_catalog/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_catalogService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}