﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using lamlt.webservice.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Oze.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/denoiser")]
    public class denoiserController : Controller
    {
        // GET: denoisers
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/denoiser/List")]
        public ActionResult List(int length, int start, string search)
        {
            denoiserService svrdenoiser = (new denoiserService());
            List<denoiser> data = svrdenoiser.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrdenoiser.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/denoiser/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            denoiser obj = (new denoiserService()).GetByID(id);
            if (obj == null) obj = (new denoiserService()).InitEmpty();
            return PartialView("denoiserEdit", obj);
        }
        [HttpPost]
        [Route("~/denoiser/Update")]
        public ActionResult Update(denoiser obj)
        {
            int result = new denoiserService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/denoiser/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            denoiser obj = new denoiserService().GetByID(id);
            return PartialView("denoiserDetail", obj);
        }
        [HttpGet]
        [Route("~/denoiser/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_denoiser obj = new denoiserService().GetViewByID(id);
            return PartialView("denoiserDetail", obj);
        }
        [HttpDelete]
        [Route("~/denoiser/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new denoiserService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/denoiser/GenDenoiser")]
        public string GenDenoiser(string obj, string filename)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string path = @"/home/amnhacsaigon/dotnet/hg-clone/wwwroot/denoiser/";
            if (isDev)
            {
                path = @"C:\denoiner\";
            }
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string fileName = path + filename;
           
            string check_key = new film_keyService().CheckKey();
            switch (check_key)
            {
                case "success":
                    string serial = LamltService.GetConnectionString("key");
                    var dict = JsonConvert.DeserializeObject<Dictionary<object, object>>(obj);
                    dict.Add("key", serial);
                    try
                    {
                        System.IO.File.WriteAllText(fileName, JsonConvert.SerializeObject(dict));
                        return "Đặt lệnh thành công!";
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine("Lỗi ghi file để denoiser");
                        Console.WriteLine(ex.Message);
                        return "Lỗi : " + ex.Message;
                    }
                case "no_key": return "Bạn chưa kích hoạt key bản quyền phần mềm";
                case "key_log_error": return "Lỗi không kiểm tra được lịch sử sử dụng bản quyền phần mềm, vui lòng liên hệ quản trị viên";
                case "timeout": return "Đã hết thời gian sử dụng";
                case "error":
                default:
                    return "Lỗi không kiểm tra được hệ thống bản quyền phần mềm, vui lòng liên hệ quản trị viên";
            }
           
            
        }
        [HttpGet]
        [Route("~/denoiser/get")]
        public JsonResult Edits(int id, string title)
        {
            var obj = new denoiserService().GetInfo(id, title);
            return Json(obj != null ? obj : null);
        }
        [Route("~/denoiser/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file(IFormFile file)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0)
                return Content("file not selected");
            string sFileWillSave = file.FileName;
            sFileWillSave = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + CommService.TrimVietnameseMark(sFileWillSave, true, true);
            string fileDirect = @"/home/www/data/data/saigonmusic/hg_project/";
            if (isDev)
            {
                fileDirect = @"C:\data\";
            }
            //else
            //{
            //    string mode = LamltService.GetConnectionString("mode");
            //    if (mode == "docker") fileDirect = "/app/wwwroot/hg_project/";
            //}
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return Json(new { file = "/hg_project/" + sFileWillSave });
        }
        [HttpGet]
        [Route("~/denoiser/UpdateStatus/")]
        public ActionResult UpdateStatus(int id, string status)
        {
            int result = new denoiserService().UpdateStatus(id, status);
            return Json(new { result = result });
        }
    }
}