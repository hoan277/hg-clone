﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_transactions")]
    public class film_transactionsController : Controller
    {
        // GET: film_transactionss
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_transactions/List")]
        public ActionResult List(int length, int start, string search, string arr_status, string time_range, string arr_typeName)
        {
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_status = arr_status,
                arr_search_date = time_range,
                arr_typeName = arr_typeName
            };
            film_transactionsService svrfilm_transactions = new film_transactionsService();
            List<vw_film_transactions> data = svrfilm_transactions.GetViewAllItem(paging);
            int recordsTotal = (int)svrfilm_transactions.CountAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            double total = svrfilm_transactions.GetSumAmount(paging);
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data,
                sum = total
            });
        }
        [HttpGet]
        [Route("~/film_transactions/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_transactions obj = (new film_transactionsService()).GetByID(id);
            if (obj == null) obj = (new film_transactionsService()).InitEmpty();
            return PartialView("film_transactionsEdit", obj);
        }
        [HttpPost]
        [Route("~/film_transactions/Update")]
        public ActionResult Update(film_transactions obj)
        {
            int result = new film_transactionsService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/film_transactions/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new film_transactionsService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_transactions/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_transactions obj = new film_transactionsService().GetByID(id);
            return PartialView("film_transactionsDetail", obj);
        }
        [HttpGet]
        [Route("~/film_transactions/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_transactions obj = new film_transactionsService().GetViewByID(id);
            return PartialView("film_transactionsDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_transactions/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_transactionsService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}