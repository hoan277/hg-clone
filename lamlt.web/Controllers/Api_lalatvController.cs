﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/api_lalatv")]

    public class api_lalatvController : Controller
    {
        private IConfiguration webconfig;
        public api_lalatvController(IConfiguration iConfig)
        {
            webconfig = iConfig;
        }
        // GET: api_lalatv
        #region ===== Partial =====
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        [HttpGet]
        [Route("~/api_lalatv/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            api_lalatv obj = new api_lalatvService().GetByID(id);
            if (obj == null) obj = new api_lalatvService().InitEmpty();
            return PartialView("api_lalatvEdit", obj);
        }
        [HttpGet]
        [Route("~/api_lalatv/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            api_lalatv obj = (new api_lalatvService()).GetByID(id);
            if (obj == null) obj = (new api_lalatvService()).InitEmpty();
            return View("Edit", obj);
        }
        [HttpGet]
        [Route("~/api_lalatv/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] int id)
        {
            vw_api_lalatv obj = new api_lalatvService().GetViewByID(id);
            return PartialView("api_lalatvDetail", obj);
        }

        #endregion ===== Partial =====

        #region ===== Action =====
        [HttpGet]
        [Route("~/api_lalatv/List")]
        public ActionResult List(int length, int start, string search, int status, string start_time, string end_time, int isview)
        {
            api_lalatvService api_LalatvSV = new api_lalatvService();
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                status = status,
                start_time = start_time,
                end_time = end_time
            };
            List<vw_api_lalatv> data = api_LalatvSV.GetViewAllItem(page);
            int recordsTotal = (int)api_LalatvSV.CountAll(page);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpPost]
        [Route("~/api_lalatv/update")]
        public ActionResult Update(api_lalatv obj)
        {
            int result = new api_lalatvService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/api_lalatv/UpdateStatus")]
        public ActionResult UpdateStatus(int Id)
        {
            int result = new api_lalatvService().UpdateStatus(Id);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/api_lalatv/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new api_lalatvService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        #endregion ===== Action =====


    }
}