﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_effect")]
    public class film_effectController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        // ACTION LIST
        [HttpGet]
        [Route("~/film_effect/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_effectService sv_film_effect = (new film_effectService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_effect> data = sv_film_effect.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_effect.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_effect/Edit/{id}")]
        public PartialViewResult film_effectEdit([FromRoute] string id)
        {
            film_effect obj = (new film_effectService()).GetByID(id);
            if (obj == null) obj = (new film_effectService()).InitEmpty();
            return PartialView("film_effectEdit", obj);
        }
        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_effect/GetViewDetail/{id}")]
        public PartialViewResult film_effectDetail([FromRoute] string id)
        {
            vw_film_effect obj = new film_effectService().GetViewByID(id);
            return PartialView("film_effectDetail", obj);
        }
        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_effect/Update")]
        public ActionResult Update(film_effect obj)
        {
            film_effectService film_effectSV = new film_effectService();
            int result = film_effectSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_effect/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_effectService film_effectSV = new film_effectService();
            int result = film_effectSV.UpdateStatus(id);
            return Json(new { result = result });
        }
        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_effect/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_effectService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [Route("~/film_effect/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file(IFormFile file)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0) return Content("file not selected");
            string sFileWillSave = file.FileName;
            string fileDirect = @"/home/www/data/data/saigonmusic/hg_project_effect/";
            //string fileDirect = @"/home/amnhacsaigon/dotnet/acs-be/wwwroot/copyright/";
            if (isDev)
            {
                fileDirect = @"D:\data\";
            }
            string mode = LamltService.GetConnectionString("mode");
            if (mode == "docker") fileDirect = "/app/wwwroot/file_effect/";
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect,sFileWillSave);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            //return Json(new { file = "http://183.81.35.24:5030/file_effect/" + sFileWillSave });
            return Json(new { file = "http://27.76.152.255:13000/hg_project_effect/" + sFileWillSave });
        }
    }
}