﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_role")]
    public class film_roleController : Controller
    {
        // GET: film_roles
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_role/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_roleService svrfilm_role = (new film_roleService());
            List<film_role> data = svrfilm_role.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_role.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_role/Edit/{id}")]
        public PartialViewResult film_roleEdit([FromRoute] string id)
        {
            film_role obj = (new film_roleService()).GetByID(id);
            if (obj == null) obj = (new film_roleService()).InitEmpty();
            return PartialView("film_roleEdit", obj);
        }
        [HttpPost]
        [Route("~/film_role/Update")]
        public ActionResult Update(film_role obj)
        {
            int result = new film_roleService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_role/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_role obj = new film_roleService().GetByID(id);
            return PartialView("film_roleDetail", obj);
        }
        [HttpGet]
        [Route("~/film_role/GetViewDetail/{id}")]
        public PartialViewResult film_roleDetail(string id)
        {
            vw_film_role obj = new film_roleService().GetViewByID(id);
            return PartialView("film_roleDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_role/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_roleService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        #region ===== menu =====
        [HttpGet]
        [Route("~/film_role/menu_edit/{id}")]
        public PartialViewResult film_roleMenuEdit([FromRoute] string id)
        {
            film_role obj = (new film_roleService()).GetByID(id);
            if (obj == null) obj = (new film_roleService()).InitEmpty();
            return PartialView("film_roleMenuEdit", obj);
        }
        [HttpGet]
        [Route("~/film_role/get_list_menu")]
        public ActionResult getListMenu(int length, int start, string search)
        {
            film_cms_menuService film_menuSv = new film_cms_menuService();
            // get parent
            List<vw_film_cms_menu> list_menu_1 = film_menuSv.GetListViewByPosition(null, 1);

            List<menu_json> list_menu_json = new List<menu_json>();
            foreach (var menu in list_menu_1)
            {
                menu_json menu_Json = new menu_json();
                menu_Json.Id = menu.Id;
                menu_Json.title = menu.title;
                // get child
                menu_Json.subs = film_menuSv.GetListViewByParentId(null, menu.Id);

                list_menu_json.Add(menu_Json);
            }
            return Json(new { result = list_menu_json });
        }
        //[HttpPost]
        //[Route("~/film_role/menu_update")]
        //public JsonResult menu_update(string list_menu)
        //{
        //    film_roleService roleService = new film_roleService();
        //    int result = roleService.UpdateMenu(int.Parse(id), list_menu);
        //    if (result > 0)
        //    {
        //        return Json(new { result = 1 });
        //    }
        //    return Json(new { result = -1 });
        //}
        #endregion ===== menu =====
    }
    public class menu_json
    {
        public int Id { get; set; }
        public string title { get; set; }
        public List<vw_film_cms_menu> subs { get; set; }
    }
}