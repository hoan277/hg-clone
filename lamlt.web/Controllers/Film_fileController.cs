﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace lamlt.Controllers
{
    //[Route("~/film_file")]
    public class film_fileController : Controller
    {
        // GET: film_files
        [HttpGet]
        public ViewResult Index([FromQuery(Name = "t")] string t, [FromQuery(Name = "callback")] string callback)
        {
            dynamic mymodel = new System.Dynamic.ExpandoObject();
            mymodel.t = t;
            mymodel.callback = callback;
            return View("Index", mymodel);
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");

            string sFileWillSave = file.Name + "_" + DateTime.Now.Ticks.ToString() + Path.GetExtension(file.FileName);
            var path = Path.Combine(Directory.GetCurrentDirectory(), "/home/www/images/images/uploads", sFileWillSave);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return Json(new { file = "https://image.lalatv.com.vn/uploads/" + sFileWillSave });
        }
    }
}