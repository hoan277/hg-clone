﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.web.Controllers
{
    [Produces("application/json")]
    [Route("~/film_contact")]
    public class film_contactController : Controller
    {
        // GET: film_contacts
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_contact/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_contactService svrfilm_contact = (new film_contactService());
            List<film_contact> data = svrfilm_contact.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_contact.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_contact/Edit/{id}")]
        public PartialViewResult film_contactEdit([FromRoute] string id)
        {
            film_contact obj = (new film_contactService()).GetByID(id);
            if (obj == null) obj = (new film_contactService()).InitEmpty();
            return PartialView("film_contactEdit", obj);
        }
        [HttpPost]
        [Route("~/film_contact/Update")]
        public ActionResult Update(film_contact obj)
        {
            int result = new film_contactService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_contact/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_contact obj = new film_contactService().GetByID(id);
            return PartialView("film_contactDetail", obj);
        }
        [HttpGet]
        [Route("~/film_contact/GetViewDetail/{id}")]
        public PartialViewResult film_contactDetail(string id)
        {
            vw_film_contact obj = new film_contactService().GetViewByID(id);
            return PartialView("film_contactDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_contact/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_contactService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}