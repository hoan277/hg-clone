﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/model")]
    public class modelController : Controller
    {
        // GET: models
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/model/List")]
        public ActionResult List(int length, int start, string search)
        {
            modelService svrmodel = (new modelService());
            List<vw_model> data = svrmodel.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrmodel.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/model/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            model obj = (new modelService()).GetByID(id);
            if (obj == null) obj = (new modelService()).InitEmpty();
            return PartialView("modelEdit", obj);
        }
        [HttpPost]
        [Route("~/model/Update")]
        public ActionResult Update(model obj)
        {
            int result = new modelService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/model/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new modelService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/model/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            model obj = new modelService().GetByID(id);
            return PartialView("modelDetail", obj);
        }
        [HttpGet]
        [Route("~/model/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_model obj = new modelService().GetViewByID(id);
            return PartialView("modelDetail", obj);
        }
        [HttpDelete]
        [Route("~/model/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new modelService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        class Train
        {
            public int status { set; get; }
        }

        [HttpGet]
        [Route("~/model/GetTrain/{id}")]
        public PartialViewResult GetTrain11(string id)
        {
            var data = new modelService().Train(int.Parse(id));
            Train result = JsonConvert.DeserializeObject<Train>(data);
            //List<vw_model> listData = new List<vw_model>();
            //modelService serviceModel = new modelService();
            ViewData["data"] = result.status;
            return PartialView("modelTrain", result);
        }

    }
}