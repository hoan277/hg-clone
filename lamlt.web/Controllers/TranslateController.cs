﻿using System;
using System.Collections.Generic;
using lamlt.web.Services;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using lamlt.web.Models;
using System.Data;
using lamlt.data;

using System.Threading;
using Microsoft.Extensions.Primitives;

namespace lamlt.Controllers
{
	[Produces("application/json")]
	[Route("~/translate")]
    public class translateController : Controller
    {
        // GET: translates
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
		[Route("~/translate/List")]
        public ActionResult List(int length, int start,string search)
        {
            translateService svrtranslate= (new translateService()) ;
            List<translate> data = svrtranslate.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrtranslate.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
			StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); }catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/translate/get")]
        public JsonResult get(int id, string title, string status)
        {
            var obj = new translateService().GetInfo(id, title, status);
            return Json(obj != null ? obj : null);
        }
        [HttpGet]
		[Route("~/translate/Edit/{id}")]
        public PartialViewResult Edit([FromRoute]string id)
        {
            translate obj = (new translateService()).GetByID(id);
            if (obj == null) obj = (new translateService()).InitEmpty();
            return PartialView("translateEdit", obj);
        }       
		[HttpPost]
		[Route("~/translate/Update")]
        public ActionResult Update(translate obj)
        {
            int result = new translateService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
		[HttpGet]
		[Route("~/translate/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute]string id)
        {
			translate obj = new translateService().GetByID(id);
            return PartialView("translateDetail",obj);
        }
		[HttpGet]
		[Route("~/translate/GetViewDetail/{id}")]
		public PartialViewResult GetViewDetail([FromRoute]string id)
        {
			vw_translate obj = new translateService().GetViewByID(id);
            return PartialView("translateDetail",obj);
        }
		[HttpDelete]
		[Route("~/translate/Delete/{id}")]
        public JsonResult Delete([FromRoute]string id)
        {
            int result = new translateService().Delete(int.Parse(id));
            return Json(new { result = result });
        }       
    }
}