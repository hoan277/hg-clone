﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;

namespace lamlt.web.Controllers
{
    public class API_Translate : Controller
    {
        public static IConfigurationRoot Configuration;
        public static string ReadAppSetting(string key)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("conf/appsettings.json");
            Configuration = builder.Build();
            return Configuration[key];
        }
        [HttpPost]
        [Route("/api/translate")]
        public Dictionary<object, object> Translate(string data = "Hello", string direction = "en-vi")
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            string linkAPI = ReadAppSetting("api_translate");
            var request = new RestRequest(Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddJsonBody(new { data, direction });
            IRestResponse response;
            try
            {
                response = new RestClient(linkAPI).Execute(request);
                if (response.StatusCode.ToString() == "OK")
                {
                    code = "sucess";
                    message = "Dữ liệu nhận về từ API dịch: " + response.ErrorMessage;
                    JObject jobj = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(response.Content);
                    dict.Add("data", jobj["data"]);
                }
                else
                {
                    code = "error";
                    message = "Lỗi Request lên API:" + response.Content + " [ERROR] " + response.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                code = "error";
                message = "Lỗi Request lên API: " + ex.Message;
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
    }
}
