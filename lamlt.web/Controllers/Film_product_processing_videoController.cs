﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_product_processing_video")]
    public class film_product_processing_videoController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        // ACTION LIST
        [HttpGet]
        [Route("~/film_product_processing_video/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_product_processing_videoService sv_film_product_processing_video = (new film_product_processing_videoService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_product_processing_video> data = sv_film_product_processing_video.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_product_processing_video.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_product_processing_video/Edit/{productid}/{id}")]
        public PartialViewResult film_product_processing_videoEdit([FromRoute] string productid , [FromRoute] string id)
        {
            film_product_processing_video obj = (new film_product_processing_videoService()).GetByID(id);
            if (obj == null) obj = (new film_product_processing_videoService()).InitEmptyByProductId(int.Parse(productid));
            return PartialView("film_product_processing_videoEdit", obj);
        }
        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_product_processing_video/GetViewDetail/{id}")]
        public PartialViewResult film_product_processing_videoDetail([FromRoute] string id)
        {
            vw_film_product_processing_video obj = new film_product_processing_videoService().GetViewByID(id);
            return PartialView("film_product_processing_videoDetail", obj);
        }
        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_product_processing_video/Update")]
        public ActionResult Update(film_product_processing_video obj)
        {
            film_product_processing_videoService film_product_processing_videoSV = new film_product_processing_videoService();
            int result = film_product_processing_videoSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_product_processing_video/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_product_processing_videoService film_product_processing_videoSV = new film_product_processing_videoService();
            int result = film_product_processing_videoSV.UpdateStatus(id);
            return Json(new { result = result });
        }
        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_product_processing_video/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_product_processing_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        // TEMPLATE getListByPRoductId
        // for product edit
        [HttpGet]
        [Route("~/film_product_processing_video/list_by_product_id/{product_id}")]
        public PartialViewResult film_product_processing_videoListByProductId([FromRoute] string product_id)
        {
            return PartialView("film_product_processing_videoListByProductId", product_id);
        }
        [HttpGet]
        [Route("~/film_product_processing_video/getListByProductId/{product_id}")]
        public ActionResult getListByProductId(int length, int start, [FromRoute] string product_id)
        {
            film_product_processing_videoService film_product_processing_videoSV = new film_product_processing_videoService();
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                product_id = int.Parse(product_id)
            };
            List<vw_film_product_processing_video> data = film_product_processing_videoSV.GetViewAllItemByProductId(paging);
            int recordsTotal = (int)film_product_processing_videoSV.CountViewAllItemByProductId(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // ACTION DELETE LIST BY PRODUCT ID
        [HttpPost]
        [Route("~/film_product_processing_video/gen_now/{product_id}")]
        public ActionResult gen_now(int product_id)
        {
            film_product_processing_videoService film_product_processing_videoSV = new film_product_processing_videoService();
            int result = film_product_processing_videoSV.DeleteByProductId(product_id);
            // gen file
            PagingModel page = new PagingModel
            {
                product_id = product_id
            };
            int result1 = film_product_processing_videoSV.GenVideoProcessingRandom(page);
            return Json(new { result = result1 });
        }
    }
}
