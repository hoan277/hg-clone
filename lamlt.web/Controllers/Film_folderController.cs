﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_folder")]
    public class film_folderController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        // ACTION LIST
        [HttpGet]
        [Route("~/film_folder/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_folderService sv_film_folder = (new film_folderService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_folder> data = sv_film_folder.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_folder.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_folder/Edit/{id}")]
        public PartialViewResult film_folderEdit([FromRoute] string id)
        {
            film_folder obj = (new film_folderService()).GetByID(id);
            if (obj == null) obj = (new film_folderService()).InitEmpty();
            return PartialView("film_folderEdit", obj);
        }
        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_folder/GetViewDetail/{id}")]
        public PartialViewResult film_folderDetail([FromRoute] string id)
        {
            vw_film_folder obj = new film_folderService().GetViewByID(id);
            return PartialView("film_folderDetail", obj);
        }
        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_folder/Update")]
        public ActionResult Update(film_folder obj)
        {
            film_folderService film_folderSV = new film_folderService();
            int result = film_folderSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_folder/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_folderService film_folderSV = new film_folderService();
            int result = film_folderSV.UpdateStatus(id);
            return Json(new { result = result });
        }
        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_folder/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_folderService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}