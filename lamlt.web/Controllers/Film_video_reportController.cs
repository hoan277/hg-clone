﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video_report")]

    public class film_video_reportController : Controller
    {
        // GET: film_video_report
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        [HttpGet]
        [Route("~/film_video_report/List")]
        public ActionResult List(int length, int start, string search, int cpid, int status, int catalogid, string start_time, string end_time)
        {
            film_video_reportService svrfilm_video_report = (new film_video_reportService());
            List<vw_film_video_report> data = svrfilm_video_report.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
            int recordsTotal = (int)svrfilm_video_report.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, catalogid = catalogid, start_time = start_time, end_time = end_time, status = status });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
    }
}