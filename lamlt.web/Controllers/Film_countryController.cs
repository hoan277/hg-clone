﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_country")]
    public class film_countryController : Controller
    {
        // GET: film_countrys
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_country/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_countryService svrfilm_country = (new film_countryService());
            List<film_country> data = svrfilm_country.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_country.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpGet]
        [Route("~/film_country/Edit/{id}")]
        public PartialViewResult film_countryEdit([FromRoute] string id)
        {
            film_country obj = (new film_countryService()).GetByID(id);
            if (obj == null) obj = (new film_countryService()).InitEmpty();
            return PartialView("film_countryEdit", obj);
        }

        [HttpPost]
        [Route("~/film_country/Update")]
        public ActionResult Update(film_country obj)
        {
            int result = new film_countryService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/film_country/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_country obj = new film_countryService().GetByID(id);
            return PartialView("film_countryDetail", obj);
        }

        [HttpGet]
        [Route("~/film_country/GetViewDetail/{id}")]
        public PartialViewResult film_countryDetail(string id)
        {
            vw_film_country obj = new film_countryService().GetViewByID(id);
            return PartialView("film_countryDetail", obj);
        }

        [HttpDelete]
        [Route("~/film_country/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_countryService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/film_country/get")]
        public JsonResult get(int id, string title)
        {
            return Json(new film_countryService().GetDetails(id, title));
        }
    }
}