﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using ServiceStack.DataAnnotations;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/report_chart_view")]

    public class report_chart_viewController : Controller
    {
        // lấy dữ liệu đổ về Index
        [HttpGet]
        public PartialViewResult Index(int length, int start, string search, int cpid)
        {
            reportChart_ViewService svrreport_chart_view = new reportChart_ViewService();
            List<package> data_film = svrreport_chart_view.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
            List<package1> pack = new List<package1>();
            foreach (var item in data_film)
            {
                pack.Add(new package1()
                {
                    packageId = item.Id,
                    package_title = item.title
                });
            }
            return PartialView("Index", data_film);

        }
        public class package1
        {
            public int packageId { get; set; }
            public string package_title { get; set; }
        }
        public class vw_report_views
        {
            public int? video_id { get; set; }
            public string film_title { get; set; }
            public int? cp_id { get; set; }
            public int? package_id { get; set; }
            public string package_title { get; set; }
            [Ignore]
            public long count { get; set; }

            //public string revenue { get; set; }
        }

        // truyền dữ liệu lên service
        [HttpGet]
        [Route("~/report_chart_view/List")]
        public ActionResult List(int length, int start, string search, int cpid, int package_id)
        {
            reportChart_ViewService svrreport_chart_view = new reportChart_ViewService();
            List<vw_report_view> data1 = svrreport_chart_view.GetListVideoByDate(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
            List<vw_report_views> data = new List<vw_report_views>();
            foreach (var item in data1)
            {
                data.Add(new vw_report_views()
                {
                    video_id = item.video_id,
                    film_title = item.film_title,
                    package_id = item.package_Id,
                    package_title = item.package_title,
                    //package_title = "gói ngày",
                    //revenue = "2000",
                    count = svrreport_chart_view.CountViewOfVideoByDate(item.video_id, new PagingModel() { offset = start, limit = length, search = search, cpid = cpid })
                });
            }
            int recordsTotal = (int)svrreport_chart_view.CountViewAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }


    }
}
