﻿using lamlt.data;
using lamlt.webservice.Services;
using Microsoft.AspNetCore.Mvc;

namespace lamlt.web.Controllers
{
    public class film_keyController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [Route("/film_key/get")]
        [HttpGet]
        public JsonResult get(int id = 0, string key = "")
        {
            return Json(new film_keyService().Get(id, key));
        }
        [Route("/film_key/getall")]
        [HttpGet]
        public JsonResult getall(int id = 0, string key = "")
        {
            return Json(new film_keyService().GetAll(id, key));
        }
        [Route("/film_key/getTotal")]
        [HttpGet]
        public JsonResult getTotal(string key = "")
        {
            return Json(new film_keyService().GetTotalTime(key));
        }
        [Route("/film_key/update")]
        [HttpPost]
        public JsonResult update(film_key obj)
        {
            return Json(new film_keyService().UpdateBykey(obj));
        }
        [Route("/film_key/delete/{id}")]
        [HttpDelete]
        public JsonResult delete(int id)
        {
            return Json(new film_keyService().Delete(id));
        }
        
    }
}
