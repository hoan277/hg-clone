﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_product_processing")]
    public class film_product_processingController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        // ACTION LIST
        [HttpGet]
        [Route("~/film_product_processing/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_product_processingService sv_film_product_processing = (new film_product_processingService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_product_processing> data = sv_film_product_processing.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_product_processing.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_product_processing/Edit/{id}")]
        public PartialViewResult film_product_processingEdit([FromRoute] string id)
        {
            film_product_processing obj = (new film_product_processingService()).GetByID(id);
            if (obj == null) obj = (new film_product_processingService()).InitEmpty();
            return PartialView("film_product_processingEdit", obj);
        }

        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_product_processing/GetViewDetail/{id}")]
        public PartialViewResult film_product_processingDetail([FromRoute] string id)
        {
            vw_film_product_processing obj = new film_product_processingService().GetViewByID(id);
            return PartialView("film_product_processingDetail", obj);
        }

        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_product_processing/Update")]
        public ActionResult Update(film_product_processing obj)
        {
            film_product_processingService film_product_processingSV = new film_product_processingService();
            int result = film_product_processingSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_product_processing/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_product_processingService film_product_processingSV = new film_product_processingService();
            int result = film_product_processingSV.UpdateStatus(id);
            return Json(new { result = result });
        }

        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_product_processing/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_product_processingService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        // for product edit
        [HttpGet]
        [Route("~/film_product_processing/list_by_product_id/{product_id}")]
        public PartialViewResult film_product_processingListByProductId([FromRoute] string product_id)
        {
            return PartialView("film_product_processingListByProductId", product_id);
        }

        [HttpGet]
        [Route("~/film_product_processing/getListByProductId/{product_id}")]
        public ActionResult getListByProductId(int length, int start, [FromRoute] string product_id)
        {
            film_product_processingService film_product_processingSV = new film_product_processingService();
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                product_id = int.Parse(product_id)
            };
            List<vw_film_product_processing> data = film_product_processingSV.GetViewAllItemByProductId(paging);
            int recordsTotal = (int)film_product_processingSV.CountViewAllItemByProductId(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }

        [HttpPost]
        [Route("~/film_product_processing/UpdateByProduct")]
        public ActionResult UpdateByProduct(film_product_processing obj)
        {
            int result = new film_product_processingService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/film_product_processing/import_excel/{product_id}")]
        public JsonResult import_excel([FromRoute] int product_id)
        {
            IFormFile file = Request.Form.Files[0];
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string upload_dir = isDev ? @"C:\file_upload" : @"/home/amnhacsaigon/dotnet/hg-project/wwwroot/file_upload/";
            if (!Directory.Exists(upload_dir)) Directory.CreateDirectory(upload_dir);
            var path = Path.Combine(Directory.GetCurrentDirectory(), upload_dir, file.FileName);
            List<film_video_processing> arrProcessing = new List<film_video_processing>();
            if (file.Length > 0)
            {
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    ISheet sheet;
                    if (Path.GetExtension(file.FileName).ToLower() == ".xls")
                    {
                        sheet = new HSSFWorkbook(stream).GetSheetAt(0);
                    }
                    else
                    {
                        sheet = new XSSFWorkbook(stream).GetSheetAt(0);
                    }
                    IRow headerRow = sheet.GetRow(0);
                    int cellCount = headerRow.LastCellNum;
                    for (int j = 0; j < cellCount; j++)
                    {
                        ICell cell = headerRow.GetCell(j);
                    }
                    List<IRow> list_row = new List<IRow>();
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        list_row.Add(row);
                    }
                    foreach (var row in list_row)
                    {
                        var obj = new film_video_processingService().GetByTitle(row.Cells[1].ToString(), row.Cells[2].ToString());
                        if (obj != null)
                        {
                            new film_product_processing_videoService().UpdateOrInsert(new film_product_processing_video()
                            {
                                product_id = product_id,
                                video_processing_id = obj.Id,
                                catalog_id = -1,
                                status = null,
                                datecreated = DateTime.Now,
                                dateupdated = DateTime.Now,
                                userid = comm.GetUserId()
                            });
                        }
                    }
                }
            }
            return Json(arrProcessing);
        }
    }
}