﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_provider")]
    public class film_providerController : Controller
    {
        // GET: film_providers
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_provider/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_providerService svrfilm_provider = (new film_providerService());
            List<film_provider> data = svrfilm_provider.GetAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_provider.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_provider/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_provider obj = (new film_providerService()).GetByID(id);
            if (obj == null) obj = (new film_providerService()).InitEmpty();
            return PartialView("film_providerEdit", obj);
        }
        [HttpPost]
        [Route("~/film_provider/Update")]
        public ActionResult Update(film_provider obj)
        {
            int result = new film_providerService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_provider/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_provider obj = new film_providerService().GetByID(id);
            return PartialView("film_providerDetail", obj);
        }
        [HttpGet]
        [Route("~/film_provider/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_provider obj = new film_providerService().GetViewByID(id);
            return PartialView("film_providerDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_provider/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_providerService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}