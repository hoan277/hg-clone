﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/download")]
    public class downloadController : Controller
    {
        // GET: downloads
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/download/List")]
        public ActionResult List(int length, int start, string search)
        {
            downloadService svrdownload = (new downloadService());
            List<vw_download> data = svrdownload.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrdownload.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/download/Edit/{id}")]
        public PartialViewResult downloadEdit([FromRoute] string id)
        {
            download obj = (new downloadService()).GetByID(id);
            if (obj == null) obj = (new downloadService()).InitEmpty();
            return PartialView("downloadEdit", obj);
        }
        [HttpPost]
        [Route("~/download/Update")]
        public ActionResult Update(download obj)
        {
            int result = new downloadService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/download/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            download obj = new downloadService().GetByID(id);
            return PartialView("downloadDetail", obj);
        }
        [HttpGet]
        [Route("~/download/GetViewDetail/{id}")]
        public PartialViewResult downloadDetail([FromRoute] string id)
        {
            vw_download obj = new downloadService().GetViewByID(id);
            return PartialView("downloadDetail", obj);
        }
        [HttpDelete]
        [Route("~/download/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new downloadService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}