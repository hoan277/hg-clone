﻿using ClosedXML.Excel;
using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_video")]

    public class film_videoController : Controller
    {
        private IConfiguration webconfig;
        public film_videoController(IConfiguration iConfig)
        {
            webconfig = iConfig;
        }
        // GET: film_videos
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_video/List")]
        public ActionResult List(int length, int start, string search, int cpid, int status, int catalogid, string upload_file, int film_type)
        {
            film_videoService svrfilm_video = (new film_videoService());
            PagingModel pagingModel = new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, status = status, catalogid = catalogid, upload_file = upload_file, film_type = film_type };
            List<vw_film_video_cms> data = svrfilm_video.GetViewAllItem(pagingModel);
            int recordsTotal = (int)svrfilm_video.CountAll(pagingModel);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }


        [HttpGet]
        [Route("~/film_video/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return PartialView("film_videoEdit", obj);
        }
        [HttpGet]
        [Route("~/film_video/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return View("Edit", obj);
        }

        [HttpPost]
        [Route("~/film_video/Update")]
        public ActionResult Update(film_video obj)
        {
            string pathVideos = webconfig["pathVideo"];
            string pathImages = webconfig["pathImage"];
            int result = new film_videoService().UpdateOrInsert(obj, pathVideos, pathImages);
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/film_video/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_videoService().UpdateStatus(obj);
            return Json(new { result = result });
        }

        [HttpDelete]
        [Route("~/film_video/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute] string id)
        {
            int result = new film_videoService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }

        [HttpPost]
        [Route("~/film_video/deletedrmDialog")]
        public JsonResult deletedrmDialog(string fileName)
        {
            fileName = Regex.Match(fileName, @"\w.*(.mkv|.mov|.mp4|.smil)").Groups[0].Value.Replace("https://stream.lalatv.com.vn/vod/_definst_/smil:automedia/", "");
            fileName = fileName.Replace("https://stream.lalatv.com.vn/vod/_definst_/smil:automedia/", "");
            fileName = fileName.Replace("/playlist.m3u8", "");

            string file360 = "/home/amnhacsaigon/wowza/mapping/keys/automedia/" + fileName.Split(".")[0] + "-360p.mp4.key";
            string file480 = "/home/amnhacsaigon/wowza/mapping/keys/automedia/" + fileName.Split(".")[0] + "-480p.mp4.key";
            string file720 = "/home/amnhacsaigon/wowza/mapping/keys/automedia/" + fileName.Split(".")[0] + "-720p.mp4.key";
            string fullPath = "/home/amnhacsaigon/wowza/mapping/keys/automedia/" + fileName + ".key";
            List<string> lstFile = new List<string>();
            lstFile.Add(file360);
            lstFile.Add(file480);
            lstFile.Add(file720);
            lstFile.Add(fullPath);
            foreach (var item in lstFile)
            {
                if (System.IO.File.Exists(item))
                {
                    System.IO.File.Delete(item);
                }
            }
            return Json(new { message = "Xóa key thành công" });
        }

        [HttpGet]
        [Route("~/film_video/getImageupdate")]
        public IActionResult getImageupdate(int id, string url)
        {
            string urls = webconfig["pathImage"];
            List<film_video_image> result = new film_videoService().getupdateImg(id, urls);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/film_video/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid, string path)
        {
            string paths = webconfig["pathImage"];
            int result = new film_videoService().UpdateImage(url, filmid, paths);
            return Json(new { result = result });
        }
        //  class filmid
        //{
        //    public int[] catatogid { get;set }
        //    int filmid { get; set}
        //}
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/film_video/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_videoService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video obj = new film_videoService().GetByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpGet]
        [Route("~/film_video/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_video/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_video/reconvert")]
        public JsonResult Reconvert(string file_upload)
        {
            bool result = new film_videoService().Reconvert(file_upload);
            return Json(new { result = result });
        }

        #region ===== Trainer =====
        [HttpGet]
        [Route("~/film_video/get_trainer/{id}")]
        public PartialViewResult film_videoTrainer([FromRoute] string id)
        {
            vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoTrainer", obj);
        }

        // đẩy file
        [Route("~/film_video/upload_trainer")]
        [HttpPost]
        public async Task<IActionResult> upload_trainer(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");
            string sFileWillSave = file.FileName;

            string fileDirect = "/home/www/data/data/media/automedia";
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (isDev)
            {
                fileDirect = @"D:\";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return Json(new { file = sFileWillSave });
        }

        #endregion ===== Trainer =====

        #region =====================================  XUẤT FILE BẰNG CLOSED XML ============================
        [HttpPost]
        [Route("~/film_video/exportvialib")]
        public JsonResult exportvialib(string type, string list_id)
        {
            film_videoService svrfilm_video = (new film_videoService());
            PagingModel page = new PagingModel()
            {
                offset = 0,
                limit = 1000000,
                search = null,
                cpid = -1,
                status = -1,
                catalogid = -1,
                publish_year = -1,
                publish_countryid = -1,
                film_type = -1,
                action_type = "search"
            };
            List<vw_film_video_cms> obj = svrfilm_video.GetViewAllItem(page);
            if (type == "dachon")
            {
                List<vw_film_video_cms> selected_obj = new List<vw_film_video_cms>();
                var strlist = list_id.Split(",");
                foreach (var all_data in obj)
                {
                    foreach (var selected_id in strlist)
                    {
                        if (selected_id == all_data.Id.ToString())
                        {
                            selected_obj.Add(all_data);
                        }
                    }
                }
                obj = selected_obj;
            }
            LogService.logItem("Xuất file theo: " + type);
            LogService.logItem(obj);
            DataTable dt = new DataTable
            {
                TableName = "Phim"
            };
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("Tên Phim", typeof(string));
            dt.Columns.Add("Mã code", typeof(string));
            dt.Columns.Add("Loại nội dung", typeof(string));
            dt.Columns.Add("Mô tả", typeof(string));
            dt.Columns.Add("Thể loại", typeof(string));
            dt.Columns.Add("Năm sản xuất", typeof(string));
            dt.Columns.Add("Quốc gia", typeof(string));
            dt.Columns.Add("Danh mục", typeof(string));
            dt.Columns.Add("Link phim", typeof(string));
            //Add Rows in DataTable
            int stt = 1;
            for (int i = 0; i < obj.Count; i++)
            {
                string filmType = obj[i].film_type == 0 ? "Phim lẻ" : "Phim bộ";
                dt.Rows.Add(
                    stt,
                    obj[i].title,
                    obj[i].code,
                    obj[i].catalog,
                    obj[i].desc,
                    filmType,
                    obj[i].publish_year,
                    obj[i].publish_countryid_title,
                    obj[i].catalog_id_title,
                    obj[i].upload_file
                    );
                stt++;
            }
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Tên Phim ASC";
            //Name of File

            string folderPath = "/home/amnhacsaigon/dotnet/cms-lalatv-viettel-v1/wwwroot/export_video/";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fileName = "Export_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                var ws = wb.Worksheets.Add(dt, "Sheet 1");
                ws.Style.Font.Bold = true;
                // Set the width of all columns in the worksheet
                ws.Column("1").Width = 5;
                ws.Column("2").Width = 50;
                ws.Column("3").Width = 15;
                ws.Column("4").Width = 50;
                ws.Column("5").Width = 50;
                ws.Column("6").Width = 20;
                ws.Column("7").Width = 15;
                ws.Column("8").Width = 20;
                ws.Column("9").Width = 30;
                ws.Column("10").Width = 50;
                ws.Row(1).Height = 20;
                ws.Row(1).Style.Fill.BackgroundColor = XLColor.Yellow;
                ws.Row(1).Style.Font.FontColor = XLColor.Red;
                ws.Row(1).Style.Font.SetFontSize(12);
                ws.Row(1).Height = 30;
                ws.Rows().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                ws.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                //ws.Rows().Style.Alignment.WrapText = true;
                //ws.Rows().AdjustToContents();
                wb.SaveAs(folderPath + fileName);
                string rs = "/export_video/" + fileName;
                return Json(new { data = rs });
            }
        }
        #endregion

        #region export excel to upload youtube
        [HttpPost]
        [Route("~/film_video/export_upload_youtube")]
        public JsonResult export_upload_youtube(string type, string list_id)
        {
            film_videoService film_videoSV = (new film_videoService());
            List<vw_film_video_cms> obj = new List<vw_film_video_cms>();
            if (type == "dachon")
            {
                string[] str_listId = list_id.Split(',');
                if (str_listId.Length > 0)
                {
                    foreach (var item in str_listId)
                    {
                        vw_film_video_cms film = film_videoSV.GetViewCMS(item);
                        obj.Add(film);
                    }
                }
                LogService.logItem("Xuất file theo: " + type);
                LogService.logItem(obj);
                string rs = film_videoSV.export_upload_youtube(obj);
                return Json(new { data = rs });
            }
            return Json(new { data = "" });
        }
    }
    #endregion export excel to upload youtube
}