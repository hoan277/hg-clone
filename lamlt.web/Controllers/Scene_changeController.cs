﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/scene_change")]
    public class scene_changeController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        // ACTION LIST
        [HttpGet]
        [Route("~/scene_change/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            scene_changeService sv_scene_change = (new scene_changeService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_scene_change> data = sv_scene_change.GetViewAllItem(paging);
            int recordsTotal = (int)sv_scene_change.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/scene_change/Edit/{id}")]
        public PartialViewResult scene_changeEdit([FromRoute] string id)
        {
            scene_change obj = (new scene_changeService()).GetByID(id);
            if (obj == null) obj = (new scene_changeService()).InitEmpty();
            return PartialView("scene_changeEdit", obj);
        }
        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/scene_change/GetViewDetail/{id}")]
        public PartialViewResult scene_changeDetail([FromRoute] string id)
        {
            vw_scene_change obj = new scene_changeService().GetViewByID(id);
            return PartialView("scene_changeDetail", obj);
        }
        // ACTION UPDATE
        [HttpPost]
        [Route("~/scene_change/Update")]
        public ActionResult Update(scene_change obj)
        {
            scene_changeService scene_changeSV = new scene_changeService();
            int result = scene_changeSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/scene_change/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            scene_changeService scene_changeSV = new scene_changeService();
            int result = scene_changeSV.UpdateStatus(id);
            return Json(new { result = result });
        }
        // ACTION DELETE
        [HttpDelete]
        [Route("~/scene_change/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new scene_changeService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [Route("~/scene_change/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file(IFormFile file)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0) return Content("file not selected");
            string sFileWillSave = file.FileName;
            string fileDirect = @"/home/www/data/data/saigonmusic/hg_project_scene_change/";
            //string fileDirect = @"/home/amnhacsaigon/dotnet/acs-be/wwwroot/copyright/";
            if (isDev)
            {
                fileDirect = @"D:\data\";
            }
            string mode = LamltService.GetConnectionString("mode");
            if (mode == "docker") fileDirect = "/app/wwwroot/scene_change/";
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return Json(new { file = "http://27.76.152.255:13000/hg_project_scene_change/" + sFileWillSave });
        }
    }
}