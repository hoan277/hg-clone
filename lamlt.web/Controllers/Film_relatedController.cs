﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_related")]
    public class film_relatedController : Controller
    {
        // GET: film_relateds
        [HttpGet]
        public ViewResult Index()
        {
            return View("Index");
        }

        [HttpGet]
        [Route("~/film_related/List")]
        public ActionResult List(int length, int start, string search)
        {
            film_relatedService svrfilm_related = (new film_relatedService());
            List<vw_film_related> data = svrfilm_related.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrfilm_related.CountViewAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_related/Edit/{id}")]
        public PartialViewResult film_relatedEdit(string id)
        {
            film_related obj = (new film_relatedService()).GetByID(id);
            if (obj == null) obj = (new film_relatedService()).InitEmpty();
            return PartialView("film_relatedEdit", obj);
        }
        [HttpPost]
        [Route("~/film_related/Update")]
        public ActionResult Update([FromBody] film_related obj)
        {
            int result = new film_relatedService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_related/GetDetail/{id}")]
        public PartialViewResult GetDetail(string id)
        {
            film_related obj = new film_relatedService().GetByID(id);
            return PartialView("film_relatedDetail", obj);
        }
        [HttpGet]
        [Route("~/film_related/film_relatedDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_film_related obj = new film_relatedService().GetViewByID(id);
            return PartialView("film_relatedDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_related/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new film_relatedService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}