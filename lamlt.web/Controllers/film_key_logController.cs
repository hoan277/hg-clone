﻿using lamlt.data;
using lamlt.webservice.Services;
using Microsoft.AspNetCore.Mvc;

namespace lamlt.web.Controllers
{
    public class film_key_logController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [Route("/film_key_log/get")]
        [HttpGet]
        public JsonResult get(int id = 0, string key = "")
        {
            return Json(new film_key_logService().Get(id, key));
        }
        [Route("/film_key_log/update")]
        [HttpPost]
        public JsonResult update(film_key_log obj)
        {
            return Json(new film_key_logService().UpdateBykey(obj));
        }
        [Route("/film_key_log/delete/{id}")]
        [HttpDelete]
        public JsonResult delete(int id)
        {
            return Json(new film_key_logService().Delete(id));
        }
    }
}
