﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_banner")]
    public class film_bannerController : Controller
    {
        // GET: film_banners
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/film_banner/List")]
        public ActionResult List(int length, int start, string search, string arr_display, string arr_catalogId, string arr_typeClient, string arr_search_date)
        {
            PagingModel page = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_display = arr_display,
                arr_catalogId = arr_catalogId,
                arr_typeClient = arr_typeClient,
                arr_search_date = arr_search_date
            };
            film_bannerService svrfilm_banner = new film_bannerService();
            List<vw_film_banner> data = svrfilm_banner.Get_Vw_Film_Banners(page);
            int recordsTotal = (int)svrfilm_banner.CountAll(page);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/film_banner/Edit/{id}")]
        public PartialViewResult film_bannerEdit([FromRoute] string id)
        {
            film_banner obj = (new film_bannerService()).GetByID(id);
            if (obj == null) obj = (new film_bannerService()).InitEmpty();
            return PartialView("film_bannerEdit", obj);
        }
        [HttpPost]
        [Route("~/film_banner/Update")]
        public ActionResult Update(film_banner obj)
        {
            int result = new film_bannerService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/film_banner/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_banner obj = new film_bannerService().GetByID(id);
            return PartialView("film_bannerDetail", obj);
        }
        [HttpGet]
        [Route("~/film_banner/GetViewDetail/{id}")]
        public PartialViewResult film_bannerDetail([FromRoute] string id)
        {
            vw_film_banner obj = new film_bannerService().GetViewByID(id);
            return PartialView("film_bannerDetail", obj);
        }
        [HttpDelete]
        [Route("~/film_banner/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_bannerService().Delete(int.Parse(id));
            return Json(new { result = result });
        }

        [HttpGet]
        [Route("~/film_banner/get_video")]
        public ActionResult film_bannerVideo(string id)
        {
            vw_film_video film = new film_videoService().GetViewByID(id);
            return Json(new { result = film });
        }

    }
}