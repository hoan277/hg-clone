﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/detail")]
    public class detailController : Controller
    {
        // GET: details
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        [Route("~/detail/List")]
        public ActionResult List(int length, int start, string search)
        {
            detailService svrdetail = (new detailService());
            List<vw_detail> data = svrdetail.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search });
            int recordsTotal = (int)svrdetail.CountAll(new PagingModel() { offset = start, limit = length, search = search });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/detail/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            detail obj = (new detailService()).GetByID(id);
            if (obj == null) obj = (new detailService()).InitEmpty();
            return PartialView("detailEdit", obj);
        }
        [HttpPost]
        [Route("~/detail/Update")]
        public ActionResult Update(detail obj)
        {
            int result = new detailService().UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // đếm số lượng video theo id catalog
        [HttpPost]
        [Route("~/detail/quatity")]
        public ActionResult quatity(int id)
        {
            int result = new detailService().quatity(id);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/detail/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            detail obj = new detailService().GetByID(id);
            return PartialView("detailDetail", obj);
        }
        [HttpGet]
        [Route("~/detail/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail(string id)
        {
            vw_detail obj = new detailService().GetViewByID(id);
            return PartialView("detailDetail", obj);
        }
        [HttpDelete]
        [Route("~/detail/Delete/{id}")]
        public JsonResult Delete(string id)
        {
            int result = new detailService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}