﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/video")]
    public class api_videoController : Controller
    {
        private IConfiguration webconfig;
        public api_videoController(IConfiguration iConfig)
        {
            webconfig = iConfig;
        }
        // GET: film_videos
        [HttpGet]
        public JsonResult Index()
        {
            StringValues access_token;
            Request.Query.TryGetValue("access-token", out access_token);
            return Json(new { data = access_token });
        }

        [HttpGet]
        [Route("~/api/film_video/List")]
        public ActionResult List(int length, int start, string search, int cpid, int film_type)
        {
            film_videoService svrfilm_video = (new film_videoService());
            PagingModel pagingModel = new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, film_type = film_type };
            List<vw_film_video_cms> data = svrfilm_video.GetViewAllItem(pagingModel);
            int recordsTotal = (int)svrfilm_video.CountAll(pagingModel);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        [HttpGet]
        [Route("~/api/film_video/Edit/{id}")]
        public PartialViewResult Edit([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return PartialView("film_videoEdit", obj);
        }
        [HttpGet]
        [Route("~/api/film_video/Edits/{id}")]
        public ActionResult Edits([FromRoute] string id)
        {
            film_video obj = (new film_videoService()).GetByID(id);
            if (obj == null) obj = (new film_videoService()).InitEmpty();
            return View("Edit", obj);
        }
        [HttpPost]
        [Route("~/api/film_video/Update")]
        public ActionResult Update(film_video obj)
        {
            string pathVideos = webconfig["pathVideo"];
            string pathImages = webconfig["pathImage"];
            int result = new film_videoService().UpdateOrInsert(obj, pathVideos, pathImages);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/api/film_video/UpdateStatus")]
        public ActionResult UpdateStatus(film_video obj)
        {
            int result = new film_videoService().UpdateStatus(obj);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/api/film_video/deteleimage/{id}")]
        public JsonResult deteleimage([FromRoute] string id)
        {
            int result = new film_videoService().deteleimage(int.Parse(id));
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/api/film_video/getImageupdate")]
        public IActionResult getImageupdate(int id, string path)
        {
            List<film_video_image> result = new film_videoService().getupdateImg(id, path);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/api/film_video/UpdateImage")]
        public IActionResult UpdateImage(string url, int filmid, string path)
        {
            int result = new film_videoService().UpdateImage(url, filmid, path);
            return Json(new { result = result });
        }
        //  class filmid
        //{
        //    public int[] catatogid { get;set }
        //    int filmid { get; set}
        //}
        [HttpPost]
        //public IActionResult UpdateCheckbox(tmpabc obj)
        [Route("~/api/film_video/UpdateCheckbox")]
        public IActionResult UpdateCheckbox(int[] catalog_id, int filmid)
        {
            int result = new film_videoService().UpdateCheckbox(catalog_id, filmid);
            return Json(new { result = result });
        }
        [HttpGet]
        [Route("~/api/film_video/GetDetail/{id}")]
        public PartialViewResult GetDetail([FromRoute] string id)
        {
            film_video obj = new film_videoService().GetByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpGet]
        [Route("~/api/film_video/GetViewDetail/{id}")]
        public PartialViewResult GetViewDetail([FromRoute] string id)
        {
            vw_film_video obj = new film_videoService().GetViewByID(id);
            return PartialView("film_videoDetail", obj);
        }
        [HttpDelete]
        [Route("~/api/film_video/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_videoService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}