﻿using lamlt.data;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace lamlt.web.Controllers
{
    public class AccountsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            string header = (Request.Headers["CF-Connecting-IP"].FirstOrDefault() ?? Request.Headers["X-Forwarded-For"].FirstOrDefault());
            if (IPAddress.TryParse(header, out IPAddress ip))
            {
                ViewBag.ip1 = ip;
            }
            ViewBag.ip2 = Request.HttpContext.Connection.RemoteIpAddress;
            return View();
        }
        [HttpPost]
        [Route("~/account/check_client")]
        public ActionResult Login(string user, string pass)
        {
            string api_server = "https://sdk.lalatv.com.vn/check_api?key=ABCDGAdhDAHDAJJADJA1234";
            WebClient web = new WebClient();
            string  sResult=web.DownloadString(api_server);
            if (sResult == "1")
            {
                film_user oLogin = new film_userService().CheckLogin(user, pass, null);
                int status1 = (oLogin != null ? 0 : 1);
                return Json(new { result = 1, status = status1, token = System.Guid.NewGuid().ToString(), user = oLogin });
            }
            else  return Json(new { result = 1, status = 1, token = ""});
        }
    }
}
