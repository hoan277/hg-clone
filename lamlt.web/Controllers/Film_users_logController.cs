﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_users_log")]

    public class film_users_logController : Controller
    {
        // GET: film_users_logs
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        [HttpGet]
        [Route("~/film_users_log/List")]
        public ActionResult List(int length, int start, string search, int cpid, int statusUserLog, int sub_state, int sub_type, string start_time, string end_time)
        {
            film_users_logService svrfilm_users_log = (new film_users_logService());
            List<vw_film_users_log> data = svrfilm_users_log.GetViewAllItem(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, sub_state = sub_state, sub_type = sub_type, start_time = start_time, end_time = end_time, statusUserLog = statusUserLog });
            int recordsTotal = (int)svrfilm_users_log.CountAll(new PagingModel() { offset = start, limit = length, search = search, cpid = cpid, sub_state = sub_state, sub_type = sub_type, start_time = start_time, end_time = end_time, statusUserLog = statusUserLog });
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
    }
}