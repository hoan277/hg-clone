﻿using lamlt.data;
using lamlt.web.Models;
using lamlt.web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace lamlt.Controllers
{
    [Produces("application/json")]
    [Route("~/film_channel")]
    public class film_channelController : Controller
    {
        // TEMPLATE INDEX
        [HttpGet]
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        // ACTION LIST
        [HttpGet]
        [Route("~/film_channel/List")]
        public ActionResult List(int length, int start, string search, string time_range)
        {
            film_channelService sv_film_channel = (new film_channelService());
            PagingModel paging = new PagingModel()
            {
                offset = start,
                limit = length,
                search = search,
                arr_search_date = time_range
            };
            List<vw_film_channel> data = sv_film_channel.GetViewAllItem(paging);
            int recordsTotal = (int)sv_film_channel.CountViewAll(paging);
            int recordsFiltered = recordsTotal;
            int draw = 1;
            StringValues valueDraw = "";
            HttpContext.Request.Query.TryGetValue("draw", out valueDraw);
            try { draw = int.Parse(valueDraw); } catch { }
            return Json(new
            {
                draw,
                recordsTotal,
                recordsFiltered,
                data
            });
        }
        // TEMPLATE EDIT
        [HttpGet]
        [Route("~/film_channel/Edit/{id}")]
        public PartialViewResult film_channelEdit([FromRoute] string id)
        {
            film_channel obj = (new film_channelService()).GetByID(id);
            if (obj == null) obj = (new film_channelService()).InitEmpty();
            return PartialView("film_channelEdit", obj);
        }
        // TEMPLATE DETAIL
        [HttpGet]
        [Route("~/film_channel/GetViewDetail/{id}")]
        public PartialViewResult film_channelDetail([FromRoute] string id)
        {
            vw_film_channel obj = new film_channelService().GetViewByID(id);
            return PartialView("film_channelDetail", obj);
        }
        // ACTION UPDATE
        [HttpPost]
        [Route("~/film_channel/Update")]
        public ActionResult Update(film_channel obj)
        {
            film_channelService film_channelSV = new film_channelService();
            int result = film_channelSV.UpdateOrInsert(obj);
            return Json(new { result = result });
        }
        // ACTION UPDATE STATUS
        [HttpPost]
        [Route("~/film_channel/UpdateStatus/{id}")]
        public ActionResult UpdateStatus(int id)
        {
            film_channelService film_channelSV = new film_channelService();
            int result = film_channelSV.UpdateStatus(id);
            return Json(new { result = result });
        }
        // ACTION DELETE
        [HttpDelete]
        [Route("~/film_channel/Delete/{id}")]
        public JsonResult Delete([FromRoute] string id)
        {
            int result = new film_channelService().Delete(int.Parse(id));
            return Json(new { result = result });
        }
    }
}