﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.IO;

namespace lalatv.dbmanager
{
    
    /// <summary>
    /// Summary description for ServiceUtil
    /// </summary>
    public class LogUtil
    {
        public  string dirRoot = "C:\\lalatv\\log\\";
        public static string dirLog = "C:\\lalatv\\log\\";
        bool isNoLog = false;
        public static LogUtil _instance;
        public static LogUtil getInstance()
        {
            if (_instance == null) _instance = new LogUtil();
            return _instance;
        }        
        public LogUtil()
        {
            if (ConfigurationManager.AppSettings["dirLog"] != null) dirRoot = ConfigurationManager.AppSettings["dirLog"];
            if (!Directory.Exists(dirRoot)) Directory.CreateDirectory(dirRoot);
            isNoLog = ConfigurationManager.AppSettings["disableLog"] == "1";
        }       
        public void logSk(string sContent)
        {
            if (isNoLog) return;
            Console.WriteLine(sContent);
            try
            {
                using (StreamWriter str = new StreamWriter(dirRoot + "logSocket_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true))
                {
                    str.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ":" + sContent);
                }
            }
            catch(Exception ex) 
            {
                try
                {
                    using (StreamWriter str = new StreamWriter(dirRoot + "logSocket_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true))
                    {
                        str.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ":" + sContent);
                    }
                }catch{}
            }
        }
        public void logSk(string sContent,string sFile)
        {
            if (isNoLog) return;
            Console.WriteLine(sContent);
            try
            {
                using (StreamWriter str = new StreamWriter(dirRoot + "log" + sFile + "_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true))
                {
                    str.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ":" + sContent);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    using (StreamWriter str = new StreamWriter(dirRoot + "logsFile_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true))
                    {
                        str.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ":" + sContent);
                    }
                }
                catch { }
            }
        }
        public static void logItem(string file, string[] arr)
        {
            //dirLog = ConfigurationManager.AppSettings["dirLog"];
            if (string.IsNullOrEmpty(dirLog)) dirLog = "C://lalatv/log/";
            if (!Directory.Exists(dirLog)) Directory.CreateDirectory(dirLog);
            string sContent = "";
            foreach (string s in arr)
            {
                sContent = sContent + "," + s;
            }
            using (StreamWriter w = new StreamWriter(dirLog + file, true, Encoding.UTF8))
            {
                string sLine = DateTime.Now.ToString("yyyyMMddHHmmss:") + sContent;
                w.WriteLine(sLine);
            }
        }
        public static void logItem(string file, string sContent)
        {
            //dirLog = ConfigurationManager.AppSettings["dirLog"];
            if (string.IsNullOrEmpty(dirLog)) dirLog = "C://lalatv/log/";
            if (!Directory.Exists(dirLog)) Directory.CreateDirectory(dirLog);
            using (StreamWriter w = new StreamWriter(dirLog + file, true, Encoding.UTF8))
            {
                string sLine = DateTime.Now.ToString("yyyyMMddHHmmss:") + sContent;
                w.WriteLine(sLine);
            }
        }
    }

}
