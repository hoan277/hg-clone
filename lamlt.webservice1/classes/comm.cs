﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Mail;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Net;
using System.Threading;

/// <summary>
/// Summary description for common
/// </summary>
namespace lalatv.dbmanager
{
    public class comm
    {
        public static bool validatePhone(string sPhone)
        {
            if (string.IsNullOrEmpty(sPhone)) return false;
            for (int j = 0; j <= sPhone.Length - 1; j++)
            {
                if (!("0123456789").Contains(sPhone.Substring(j, 1)))
                {
                    return false;
                }
            }
            return true;
        }
        public static bool isCouldBeChangeImei()
        {
            return isSuperAdmin() || (IsErp() && isAdmin());
        }
        public comm()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        static string[] email = new string[] { "viethong.gps@gmail.com", "tuyendung@vhc.vn", "makevhcvn@gmail.com", "developvhcvn@gmail.com" };
        static string[] password = new string[] { "viethong123!", "tuyendung123!", "maklamlt123", "devlamlt123" };

        public static string[] FindSendMailGmail()
        {
            try
            {
                int index = 0;
                for (int jx = 0; jx <= email.Length - 1; jx++)
                {
                    index = jx;
                    string sSqlCorrect = "select count(id) from PushMail where left(dateadded,8)=" + comm.to_sqltext(DateTime.Now.ToString("yyyyMMdd")) + " and emailfrom=" + comm.to_sqltext(email[jx]);
                    DataTable dtx = DBUtil.SelectTable(sSqlCorrect);
                    if (int.Parse(dtx.Rows[0][0].ToString()) >= 450)
                    {
                        LogUtil.getInstance().logSk("FindMail:toEmail=" + email[jx] + "===>" + dtx.Rows[0][0].ToString(), "FindMail");
                        continue;
                        //if (jx < email.Length - 1) return new string[] { email[jx + 1], password[jx + 1] };
                        //else return new string[] { email[email.Length - 1], password[email.Length - 1] };
                    }
                    else break;
                    //else return new string[] { email[jx], password[jx] };
                }
                return new string[] { email[index], password[index] };

                //string sSqlCorrect = "select count(id) from PushMail where left(dateadded,8)=" + comm.to_sqltext(DateTime.Now.ToString("yyyyMMdd")) + " and emailfrom=" + comm.to_sqltext("viethong.gps@gmail.com");
                //DataTable dtx = DBUtil.SelectTable(sSqlCorrect);
                //if (int.Parse(dtx.Rows[0][0].ToString()) >= 400)
                //{
                //    return new string[] { email[1], password[1] };
                //}
            }
            catch(Exception ex) 
            {
                LogUtil.getInstance().logSk("FindMail:ex=" + ex.ToString(), "FindMail");
            }
            return new string[] { email[email.Length - 1], password[email.Length - 1] }; 
        }
        public static void SendMailGmail(string toEmail,string subject,string body) 
        {
            try 
            {
                (new Thread(new ThreadStart(
                            delegate
                            {
                                string guid = System.Guid.NewGuid().ToString();
                                try 
                                {
                                    LogUtil.getInstance().logSk("Send Email:toEmail=" + toEmail + ",subject=" + subject + ",Body=" + body, "AutoSendMail");
                                    string[] arrUserPass=FindSendMailGmail();

                                    var fromAddress = new System.Net.Mail.MailAddress(arrUserPass[0], subject == "" ? "Thong Bao Don Hang" : subject);
                                    var toAddress = new System.Net.Mail.MailAddress(toEmail, toEmail);
                                    string fromPassword = arrUserPass[1];// "viethong123!";

                                    
                                    string sSql="INSERT INTO [PushMail]"+
                                    "([emailfrom],[emailto],[subject],[content],[status],[result],[dateadded],[guid])" +
                                    "VALUES(" + comm.to_sqltext(arrUserPass[0]) + "," + comm.to_sqltext(toEmail) + ","
                                              + comm.to_sqltext(subject) + "," + comm.to_sqltext(body) 
                                              + ",0,NULL,"+comm.to_sqlnow()+ ","+comm.to_sqltext(guid)+")";
                                    int nx= DBUtil.Execute(sSql);
                                    LogUtil.getInstance().logSk("===" + sSql + "===>" + nx.ToString(), "AutoSendMail");
                                   
                                    var smtp = new System.Net.Mail.SmtpClient
                                    {
                                        Host = "smtp.gmail.com",
                                        Port = 587,
                                        EnableSsl = true,
                                        DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                                        UseDefaultCredentials = false,
                                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                                    };
                                    using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
                                    {
                                        Subject = subject,
                                        Body = body
                                    })
                                    {
                                        smtp.Send(message);
                                    }
                                    string sSqlEx = "Update [PushMail]" +
                                        " set [status]=1,result=" + comm.to_sqltext("ok") + " where [guid]=" + comm.to_sqltext(guid);
                                    DBUtil.Execute(sSqlEx);
                                }
                                catch (Exception ex) 
                                {
                                    string sSqlEx = "Update [PushMail]" +
                                    " set [status]=-1,result="+comm.to_sqltext(ex.ToString())+" where [guid]=" +comm.to_sqltext(guid);
                                    DBUtil.Execute(sSqlEx);
                                    LogUtil.getInstance().logSk("Send Email:toEmail=" + ex.ToString(), "AutoSendMailException");
                                }

                            })
                      )).Start();
            }
            catch { }
        }
        public static string SendMailNet(string SenderName, string RecieverEmail, string MailTitle, string MailContent, string hostmail, string portmail)
        {
            string SenderEmail = "info@viethonggps.vn";
            string senderEmailPass = "info123!";

            try
            {


                var fromAddress = new System.Net.Mail.MailAddress(SenderEmail, MailTitle);
                var toAddress = new System.Net.Mail.MailAddress(RecieverEmail, RecieverEmail);

                string fromPassword = senderEmailPass;
                string subject = MailTitle;
                string body = MailContent;

                var smtp = new System.Net.Mail.SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                return "1";

            }
            catch (Exception ex) { return ex.ToString(); }

        }
        public static string SendMail(string SenderName, string SenderEmail, string RecieverEmail, string MailTitle, string MailContent)
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.BodyFormat = MailFormat.Html;

                mail.To = RecieverEmail;

                mail.From = SenderEmail;

                mail.Subject = MailTitle;

                mail.Body = MailContent;

                SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];

                SmtpMail.Send(mail);
                return "0";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public static string SendMail(string SenderName, string SenderEmail, string RecieverEmail, string MailTitle, string MailContent, string sSmtpServer)
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.BodyFormat = MailFormat.Html;

                mail.To = RecieverEmail;

                mail.From = SenderEmail;

                mail.Subject = MailTitle;

                mail.Body = MailContent;

                SmtpMail.SmtpServer = sSmtpServer;

                SmtpMail.Send(mail);
                return "0";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public static string getRandomNumber(int ilenght)
        {
            Random rdn = new Random();
            int MaxNumber;
            MaxNumber = 1;
            for (int i = 1; i <= ilenght; i++)
            {
                MaxNumber = MaxNumber * 10;
            }
            String rdnString = (rdn.Next(MaxNumber)).ToString();
            while (rdnString.Length < ilenght)
            {
                rdnString = "0" + rdnString;
            }
            return rdnString;

        }

        public static string to_sql(string value, string datatype)
        {
            if ((value == (null)) || (value == ("")) || (value == ("NULL")))
            {
                return "NULL";
            }
            else
            {
                string temp;
                //temp = datatype;
                temp = (string)datatype.ToLower();
                if (temp.Equals("number"))
                {
                    double Num;
                    bool isNum;
                    //isNum = false;
                    //try{
                    isNum = double.TryParse(value, out Num);
                    //}
                    //catch
                    //{
                    //    isNum = false;
                    //}

                    if (isNum)
                    {
                        return Num.ToString();
                    }
                    else
                    {
                        return "0";
                    }
                }
                else if (temp.Equals("date"))
                {

                    return "Convert([datetime], '" + value + "', 103)";

                }
                else //Luu tru va hien thi theo dinh dang dd/mm/yyyy - code 103
                {
                    return "N'" + value.Replace("'", "''") + "'";
                }


            }
        }
        public static bool IsPtracker()
        {
            {
                if (HttpContext.Current.Session["TypePage"] == null)
                    return false;

                return HttpContext.Current.Session["TypePage"].ToString() == "ptracker";
            }
        }
        public static bool IsErp()
        {
            {
                if (HttpContext.Current.Session["TypePage"] == null)
                    return false;

                return HttpContext.Current.Session["TypePage"].ToString() == "erp";
            }
        }
        public static void checkExpired()
        {
            if (comm.IsExpired())
            {
                HttpContext.Current.Response.End();
            }
        }
        public static bool IsExpired()
        {
            {
                if (HttpContext.Current.Session["IsExpired"] == null)
                    return false;

                return (bool)HttpContext.Current.Session["IsExpired"];
            }
        }
        public static bool IsControl()
        {
            {
                if (HttpContext.Current.Session["iscontrol"] == null)
                    return false;

                return HttpContext.Current.Session["iscontrol"].ToString() == "1";
            }
        }
        public static bool IsDevice()
        {
            {
                if (HttpContext.Current.Session["TypePage"] == null)
                    return false;

                return HttpContext.Current.Session["TypePage"].ToString() == "device";
            }
        }
        public static string GetLimited()
        {
            {
                if (HttpContext.Current.Session["LimitedTimeCond"] == null)
                    return "";

                return HttpContext.Current.Session["LimitedTimeCond"].ToString();
            }
        }
        public static string GetLimitedCond(string field)
        {
            if (string.IsNullOrEmpty(field)) return GetLimited();
            return GetLimited().Replace("dateadded", field);
        }
        //ptracker,device,or erp
        public static string GetTypeMember()
        {  
            if (HttpContext.Current.Session["TypePage"] == null)
                return "";

            return HttpContext.Current.Session["TypePage"].ToString();
        }
        public static string to_sqlnumber(string value)
        {
            return to_sql(value, "number");
        }
        public static string to_sqlnow()
        {
            return to_sql(DateTime.Now.ToString("yyyyMMddHHmmss"), "text");
        }
        public static string to_sqltext(string value)
        {
            return to_sql(value, "text");
        }
        public static string to_sqlforlike(string value)
        {
            if (string.IsNullOrEmpty(value)) return "";
            return value.Replace("'", "''");
        }
        public static string to_sqldate(string value)
        {
            return to_sql(value, "date");
        }
        public static string to_paramurl(object value)
        {
            if (value != null) return value.ToString();
            else return "";
        }
        public static string to_htmlcode(string message)
        {
            if ((message.Equals(null)) || (message.Equals("")))
            {
                return null;
            }
            else
            {
                //return message.Replace("\n\r", "<br>");
                //return message.Replace("\n", "<br>");
                return message.Replace("\r", "<br>");
            }
        }
        public static string to_dataview(string message)
        {
            if ((message.Equals(null)) || (message.Equals("")))
            {
                return null;
            }
            else
            {
                string stemp = message;
                //stemp = stemp.Replace("<", "&lt;");
                //stemp = stemp.Replace(">", "&gt;");
                //stemp = stemp.Replace("\r", "<br>");
                stemp = stemp.Replace("\n", "<br>");
                return stemp;
            }
        }
        public static string to_textcode(string message)
        {
            if (string.IsNullOrEmpty(message)) return "";
            if ((message.Equals(null)) || (message.Equals("")))
            {
                return null;
            }
            else
            {
                return message.Replace("<br>", "\r");
            }
        }

        public static string UnFormatNumber(string st, string stSpace)
        {
            if (st == null) return "";
            return st.Replace(stSpace, "");
        }

        public static string UnFormatNumber(string st)
        {
            return UnFormatNumber(st, ",");
        }

        public static string FormatNumber(string st)
        {
            return FormatNumber(st, 3, ",", 0);
        }
        public static string FormatNumber(string st, int slNhom, string stSpace, int lim)
        {

            if (st == null) return "";
            st = st.Trim();
            int i, sl, vt, cd;
            vt = 0;
            string tgst;
            cd = st.Length;
            if (cd <= slNhom) return st;
            sl = Convert.ToInt32((cd - 1) / slNhom);
            tgst = "";
            for (i = 1; i <= sl; i++)
            {
                vt = cd - slNhom * i;
                if (tgst != "") tgst = stSpace + tgst;
                tgst = st.Substring(vt, slNhom) + tgst;
                if ((lim > 0) && (i == lim + 1))
                {
                    tgst = st.Substring(0, vt) + tgst;
                    return tgst;
                }
            }
            if (vt > 0)
            {
                if (tgst != "") tgst = stSpace + tgst;
                tgst = st.Substring(0, vt) + tgst;
            }
            return tgst;
        }

        public static string fnEncrypt(string password)
        {
            byte[] textBytes = System.Text.Encoding.Default.GetBytes(password);
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    if (a < 16)
                        ret += "0" + a.ToString("x");
                    else
                        ret += a.ToString("x");
                }
                return ret;
            }
            catch
            {
                throw;
            }
        }

        // ------Het cac ham xy ly xau-------

        // ------Cac ham xu ly thoi gian-------

        public static string GetStrTime()
        {
            return GetStrTime("");
        }

        public static string GetStrTime(string format)
        {
            
            string sdatetime, sYear, sMonth, sDay, sHour, sMinute;
            sYear = DateTime.Now.Year.ToString();
            sMonth = DateTime.Now.Month.ToString();
            sDay = DateTime.Now.Day.ToString();
            sHour = DateTime.Now.Hour.ToString();
            sMinute = DateTime.Now.Minute.ToString();
            if (sMonth.Length < 2) sMonth = "0" + sMonth;
            if (sDay.Length < 2) sDay = "0" + sDay;
            if (sHour.Length < 2) sHour = "0" + sHour;
            if (sMinute.Length < 2) sDay = "0" + sMinute;


            switch (format)
            {
                case "yyyymmdd":
                    sdatetime = sYear + "/" + sMonth + "/" + sDay;
                    break;
                case "mmddyyyy":
                    sdatetime = sMonth + "/" + sDay + "/" + sYear;
                    break;
                case "ddmmyyyy":
                    sdatetime = sDay + "/" + sMonth + "/" + sYear;
                    break;
                case "yyyymmddhhmm":
                    sdatetime = sYear + "/" + sMonth + "/" + sDay + " " + sHour + ":" + sMinute;
                    break;
                default:
                    sdatetime = sDay + "/" + sMonth + "/" + sYear;
                    break;
            }

            return sdatetime;

        }
        public static string GetStrTime(DateTime dtRef, string format)
        {

            string sdatetime, sYear, sMonth, sDay, sHour, sMinute, sSecond;
            sYear = dtRef.Year.ToString();
            sMonth = dtRef.Month.ToString();
            sDay = dtRef.Day.ToString();
            sHour = dtRef.Hour.ToString();
            sMinute = dtRef.Minute.ToString();
            sSecond = dtRef.Second.ToString();
            if (sMonth.Length < 2) sMonth = "0" + sMonth;
            if (sDay.Length < 2) sDay = "0" + sDay;
            if (sHour.Length < 2) sHour = "0" + sHour;
            if (sMinute.Length < 2) sMinute = "0" + sMinute;
            if (sSecond.Length < 2) sSecond = "0" + sSecond;

            switch (format)
            {
                case "yyyymmdd":
                    sdatetime = sYear + "/" + sMonth + "/" + sDay;
                    break;
                case "mmddyyyy":
                    sdatetime = sMonth + "/" + sDay + "/" + sYear;
                    break;
                case "ddmmyyyy":
                    sdatetime = sDay + "/" + sMonth + "/" + sYear;
                    break;
                case "hhmmss":
                    sdatetime = sHour + ":" + sMinute + ":" + sSecond;
                    break;
                case "yyyymmddhhmm":
                    sdatetime = sDay + "/" + sMonth + "/" + sYear + " " + sHour + ":" + sMinute + ":00";
                    break;
                case "yyyymmddhhmmss":
                    sdatetime = sDay + "/" + sMonth + "/" + sYear + " " + sHour + ":" + sMinute + ":" + sSecond;
                    break;
                default:
                    sdatetime = sDay + "/" + sMonth + "/" + sYear;
                    break;
            }

            return sdatetime;
        }
        public static DateTime GetDateFromStrVN(string dt)
        {
            string sdatetime, sYear, sMonth, sDay, sHour, sMinute, sSecond;
            string[] arr1 = dt.Split(new string[] { " " }, StringSplitOptions.None);
            string[] arr2 = arr1[0].Split(new string[] { "/" }, StringSplitOptions.None);
            string[] arr3 = arr1[1].Split(new string[] { ":" }, StringSplitOptions.None);

            sYear = arr2[2];
            sMonth = arr2[1];
            sDay = arr2[0];

            sHour = arr3[0];
            sMinute = arr3[1];
            sSecond = arr3[2];

            return new DateTime(int.Parse(sYear), int.Parse(sMonth), int.Parse(sDay), int.Parse(sHour), int.Parse(sMinute), int.Parse(sSecond));
        }
        public static string GetDateFromStrVN(string dt, int i)
        {
            string sdatetime, sYear, sMonth, sDay, sHour, sMinute;
            string[] arr1 = dt.Split(new string[] { " " }, StringSplitOptions.None);
            string[] arr2 = arr1[0].Split(new string[] { "/" }, StringSplitOptions.None);
            string[] arr3 = arr1[1].Split(new string[] { ":" }, StringSplitOptions.None);

            sYear = arr2[2];
            sMonth = arr2[1];
            sDay = arr2[0];

            sHour = arr3[0];
            sMinute = arr3[1];

            return sYear + "-" + sMonth + "-" + sDay + "-" + sHour + "-" + sMinute;
        }
        public static bool isAgency()
        {
            if (HttpContext.Current.Session["isagency"] == null)
                return false;

            return (HttpContext.Current.Session["isagency"].ToString()) == "1";
        }
        public static bool isSuperAdmin()
        {
            if (ConfigurationManager.AppSettings["isTestLocal"] == "1") return true;
            if (HttpContext.Current.Session["issuperadmin"] == null)
                return false;

            return (HttpContext.Current.Session["issuperadmin"].ToString()) == "1";
        }
        public static bool isAdmin()
        {
            if (HttpContext.Current.Session["isadmin"] == null)
                return false;

            return (HttpContext.Current.Session["isadmin"].ToString()) == "1";
        }
        public static bool isReporter()
        {
            if (HttpContext.Current.Session["isreport"] == null)
                return false;

            return (HttpContext.Current.Session["isreport"].ToString()) == "1";
        }
        public static bool isFullproductsreport()
        {
            if (HttpContext.Current.Session["isFullProducts"] == null)
                return false;

            return (HttpContext.Current.Session["isFullProducts"].ToString()) == "1";
        }
        public static bool isLeader()
        {
            if (HttpContext.Current.Session["isleader"] == null)
                return false;

            return (HttpContext.Current.Session["isleader"].ToString()) == "1";
        }
        
        public static bool isKeeper()
        {
            if (HttpContext.Current.Session["iskeeper"] == null)
                return false;

            return (HttpContext.Current.Session["iskeeper"].ToString()) == "1";
        }
        public static bool isModerator()
        {
            if (HttpContext.Current.Session["ismoderator"] == null)
                return false;

            return (HttpContext.Current.Session["ismoderator"].ToString()) == "1";
        }
        public static bool isQTTTSP()
        {
            //return false;
            if (HttpContext.Current.Session["isqtttsp"] == null)
                return false;

            return (HttpContext.Current.Session["isqtttsp"].ToString()) == "1";
        }
        public static bool isPTKD()
        {
            if (HttpContext.Current.Session["isptkd"] == null)
                return false;

            return (HttpContext.Current.Session["isptkd"].ToString()) == "1";
        }
        public static bool isGiamDoc()
        {
            if (HttpContext.Current.Session["isgiamdoc"] == null)
                return false;

            return (HttpContext.Current.Session["isgiamdoc"].ToString()) == "1";
        }        
        public static bool isAccounting()
        {
            //return false;
            if (HttpContext.Current.Session["isaccounting"] == null)
                return false;

            return (HttpContext.Current.Session["isaccounting"].ToString()) == "1";
        }
        public static bool isAccountingVietHong()
        {
            //return false;
            return (isAccounting() && getCompanyId() == 12);
            

            
        }
        public static bool isQTTTSPVietHong()
        {
            //return false;
            return (isQTTTSP() && getCompanyId() == 124);
        }
        public static bool isCTVHong()
        {
            return false;
            //return (getCompanyId() == 137);
        }
        public static bool isProject()
        {
            if (HttpContext.Current.Session["isproject"] == null)
                return false;

            return (HttpContext.Current.Session["isproject"].ToString()) == "1";
        }
        public static bool isSale()
        {
            if (HttpContext.Current.Session["issale"] == null)
                return false;

            return (HttpContext.Current.Session["issale"].ToString()) == "1";
        }
        public static int getAgencyId()
        {
            try
            {
                if (HttpContext.Current.Session["agencyid"] == null)
                    return -1;

                return Convert.ToInt32(HttpContext.Current.Session["agencyid"].ToString());
            }
            catch { return -1; }
        }
        public static int getCompanyId()
        {
            int companyid = -1;
            try
            {
                if (isSuperAdmin()) companyid = getCompanyIdSelected();//kieucb 14042012
                if (companyid > 0) return companyid;

                if (HttpContext.Current.Session["companyid"] == null)
                    return -1;

                return Convert.ToInt32(HttpContext.Current.Session["companyid"].ToString());
            }
            catch { return -1; }
        }
        
        public static string getCompanyByMemberId(string memberid)
        {
            string companyid = "-1";
            try
            {
                if (companyid == "-1" || companyid == "") 
                {
                    string sql = "select companyid from Member where id=" + memberid;
                    object _ob = DBUtil.ExecuteScalar(sql);
                    if (_ob != null)
                    {
                        companyid = _ob.ToString();
                    }
                    else companyid = "-1";
                }
                return companyid;
            }
            catch {  }
            return "-1";
        }

        public static int getCompanyIdSelected() //kieucb 12/04/2012
        {
             try
            {
                if (HttpContext.Current.Session["companyidselected"] == null)
                    return -1;

                return Convert.ToInt32(HttpContext.Current.Session["companyidselected"].ToString());
            }
            catch { return -1; }
            //Session["companyidselected"]
        }
        public static string getCompanyNameMember()
        {
            try
            {
                if (HttpContext.Current.Session["companyname"] == null)
                    return "-1";

                return (HttpContext.Current.Session["companyname"].ToString());
            }
            catch { return "-1"; }
        }

        public static int getCompanyIdMember() //kieucb 12/04/2012
        {
            try
            {
                if (HttpContext.Current.Session["companyid"] == null)
                    return -1;

                return Convert.ToInt32(HttpContext.Current.Session["companyid"].ToString());
            }
            catch { return -1; }
            //Session["companyidselected"]
        }
        public static bool isOfficer()
        {
            if (HttpContext.Current.Session["isofficer"] == null)
                return false;

            return (HttpContext.Current.Session["isofficer"].ToString()) == "1";
        }
        public static bool isSupervisor()
        {
            if (HttpContext.Current.Session["issupervisor"] == null)
                return false;

            return (HttpContext.Current.Session["issupervisor"].ToString()) == "1";
        }
        public static string hideCSSByPtracker() 
        {
            if (IsPtracker() && !isSuperAdmin()) return "style='display:none'";
            return "";
        }
        //public static string getCondByRoleMember() 
        //{            
        //    string sql = "";
        //    if (comm.isSuperAdmin() || comm.isAdmin()) return "";
            
            

        //    return sql;
        //}
        //public static string getCondByRoleMemberSpec()
        //{
        //    string sql = "";
        //    if (comm.isSuperAdmin() || comm.isAdmin()) return "";

        //    if (comm.isSupervisor())
        //    {
        //        sql += " and id in (select memberid from Gps_MembersMembers where supervisorid=" + comm.GetMemberID() + ")";
        //    }

        //    return sql;
        //}
        public static string getCondByMemberNoCompany()
        {
            string sSql = "";
            //return "";

            //if (comm.isSuperAdmin()) sSql += "";
            if (comm.isSuperAdmin()) //kieucb 12/04/2012
            {
                int companyidselected = comm.getCompanyIdSelected();
                if (companyidselected > 0) sSql += " and memberid in (select id from member where companyid=" + companyidselected.ToString() + ")";

                //if (comm.IsDevice()) sSql += " and companyid in (select id from Company where [type]='device') ";//kieucb 22012013
            }
            else if (!comm.isSuperAdmin())
            {
                if (comm.isAccountingVietHong()) return sSql;//Kế toán Việt Hồng có quyền search tất, nhưng không được sửa xóa                

                if (comm.IsDevice()||comm.isOnlyOfficer()) //kieucb 30112012
                {
                    sSql += " and memberid=" + comm.GetMemberID().ToString();
                    return sSql;
                }

                sSql += " and memberid in (select id from member where companyid=" + comm.getCompanyId().ToString() + ")";

                if (comm.isAdmin() || comm.isModerator()) sSql += "";// and memberid in (select id from member where companyid=" + comm.getCompanyId().ToString() + ")";
                else
                {
                    //if (comm.isOfficer()) sSql += " and memberid=" + comm.GetMemberID().ToString();

                    //else sSql += " and companyid =" + comm.getCompanyId().ToString() + "";
                    if (comm.isSupervisor())//nếu là người giám sát thì nó chỉ theo dõi các nhân viên nó giám sát thôi
                        sSql += " and (memberid in (select memberid from Gps_MembersMembers where supervisorid=" + comm.GetMemberID() + ") or memberid=" + comm.GetMemberID().ToString() + ") ";
                    else sSql += " and memberid=" + comm.GetMemberID().ToString();//nếu là nhân viên bình thường thì chỉ theo dõi chính nó thôi
                }
            }

            //thêm điều kiện phân quyền theo role
            //if (!comm.isSuperAdmin() && !comm.isSuperAdmin())
            //{
            //    if (comm.isSupervisor())
            //        sSql += " and memberid in (select memberid from Gps_MembersMembers where supervisorid=" + comm.GetMemberID() + ")";
            //    else sSql = "and memberid=" + comm.GetMemberID().ToString();
            //}

            return sSql;
        }
        public static bool isOnlyOfficer()
        {
            return comm.isOfficer() && (!comm.isSuperAdmin() && !comm.isSupervisor() && !comm.isQTTTSP() && !comm.isAdmin() && !comm.isModerator()) ;
        }
        public static string getCondByMember()
        {
            string sSql = "";
            //return "";

            //if (comm.isSuperAdmin()) sSql += "";
            if (comm.isSuperAdmin()) //kieucb 12/04/2012
            {
                int companyidselected = comm.getCompanyIdSelected();
                if (companyidselected > 0) sSql += " and companyid=" + companyidselected.ToString();
                
                if (comm.IsDevice()) sSql += " and companyid in (select id from Company where [type]='device') ";//kieucb 22012013
            }
            else if (!comm.isSuperAdmin())
            {
                sSql += " and companyid =" + comm.getCompanyId().ToString() + "";//khác superadmin cứ phải thêm điều kiện này cho chắc

                if (comm.IsDevice() || comm.isOnlyOfficer()) //kieucb 30112012
                {
                    sSql += " and memberid=" + comm.GetMemberID().ToString();
                    return sSql;
                }

                if (comm.isAdmin() || comm.isModerator()) sSql += "";// and companyid =" + comm.getCompanyId().ToString() + "";
                else
                {
                    //if (comm.isOfficer()) sSql += " and memberid=" + comm.GetMemberID().ToString();

                    //else sSql += " and companyid =" + comm.getCompanyId().ToString() + "";
                    if (comm.isSupervisor())
                    {
                        //nếu là người giám sát thì nó chỉ theo dõi các nhân viên nó giám sát thôi
                        //oldversion//sSql += " and (memberid in (select memberid from Gps_MembersMembers where supervisorid=" + comm.GetMemberID() + ") or memberid=" + comm.GetMemberID().ToString() + ") ";
                        
                        sSql += " and (memberid in(select memberid from Gps_membersmembers where supervisorid=" + comm.GetMemberID().ToString() + ") or memberid=" + comm.GetMemberID().ToString();
                        //sSql += "  or memberid in (select memberid from Member_Group_GroupMember where groupid in (select groupid from Member_Group_MemberGroup where memberid=" + comm.GetMemberID().ToString() + "))";
                        DataTable dttMembergroup = new DataTable();
                        string memberidInGroup = "-20";
                        foreach (DataRow drr in dttMembergroup.Rows) 
                        {
                            memberidInGroup += "," + drr["memberid"].ToString();
                        }
                        sSql += " or memberid in(" + memberidInGroup + ")";
                        sSql += ")";
                    }
                    else sSql += " and memberid=" + comm.GetMemberID().ToString();//nếu là nhân viên bình thường thì chỉ theo dõi chính nó thôi
                }
            }

            //thêm điều kiện phân quyền theo role
            //if (!comm.isSuperAdmin() && !comm.isSuperAdmin())
            //{
            //    if (comm.isSupervisor())
            //        sSql += " and memberid in (select memberid from Gps_MembersMembers where supervisorid=" + comm.GetMemberID() + ")";
            //    else sSql = "and memberid=" + comm.GetMemberID().ToString();
            //}
            
            return sSql;
        }
        public static string getCondByMemberSpec()
        {
            string sSql = "";
            //return "";
            if (comm.isAccountingVietHong()) return sSql;//Kế toán việt hồng được lấy tất - nhưng không được thêm sửa xóa

            //thằng đại lý cũng có thể lấy tất cả nhưng nó phải nhập điều kiện tìm kiếm
            if (comm.isSuperAdmin())//||comm.isQTTTSP() 
            {
                int companyidselected = comm.getCompanyIdSelected();
                if (companyidselected > 0) sSql += " and companyid=" + companyidselected.ToString();
            }
            else if (!comm.isSuperAdmin())
            {
                sSql += " and companyid =" + comm.getCompanyId().ToString() + "";//ngoài superadmin phải thêm điều kiện này đầu tiên

                if (comm.IsDevice() || comm.isOnlyOfficer())//kieucb 30112012
                {
                    if (comm.isAdmin()) sSql += "";// and companyid =" + comm.getCompanyId().ToString() + "";
                    else sSql += " and id=" + comm.GetMemberID().ToString();
                    return sSql;
                }
                
                if (comm.isAdmin()) sSql += "";// and companyid =" + comm.getCompanyId().ToString() + "";
                else
                {
                    if (comm.isSupervisor())
                    {
                        //sSql += " and (id in (select memberid from Gps_MembersMembers where supervisorid=" + comm.GetMemberID() + ") or id=" + comm.GetMemberID().ToString() + ")";
                        sSql += " and (Member.id in(select memberid from Gps_membersmembers where supervisorid=" + comm.GetMemberID().ToString() + ") or Member.id=" + comm.GetMemberID().ToString();
                        //sSql += "  or Member.id in (select memberid from Member_Group_GroupMember where groupid in (select groupid from Member_Group_MemberGroup where memberid=" + comm.GetMemberID().ToString() + "))";

                        DataTable dttMembergroup = new DataTable();
                        string memberidInGroup = "0";
                        foreach (DataRow drr in dttMembergroup.Rows)
                        {
                            memberidInGroup += "," + drr["memberid"].ToString();
                        }
                        sSql += " or Member.id in(" + memberidInGroup + ")";

                        sSql += ")";
                    }
                    else sSql += " and id=" + comm.GetMemberID().ToString(); 

                    //if (comm.isOfficer()) sSql += " and id=" + comm.GetMemberID().ToString();
                    //else sSql += " and companyid =" + comm.getCompanyId().ToString() + "";
                }
            }

            if (comm.IsDevice()) sSql += " and companyid in (select id from Company where [type]='device') ";//kieucb 22012013

            return sSql;
        }
        public static string getCondByMemberSpecNew()
        {
            string sSql = "";            
            if (comm.isSuperAdmin())
            {
                int companyidselected = comm.getCompanyIdSelected();
                if (companyidselected > 0) sSql += " and Member.companyid=" + companyidselected.ToString();
            }
            else if (!comm.isSuperAdmin())
            {
                sSql += " and Member.companyid =" + comm.getCompanyId().ToString() + "";//ngoài superadmin phải thêm điều kiện này đầu tiên


                if ((comm.IsDevice() || comm.isOnlyOfficer()) && !comm.isAdmin())//kieucb 30112012
                {
                    sSql += " and id=" + comm.GetMemberID().ToString();
                    return sSql;
                }
                if (comm.getCompanyId() == 134)//á âu
                {
                    string member_groupid = HttpContext.Current.Request["member_groupid"];
                    //new version
                    if (comm.isAdmin())
                    {
                        if(string.IsNullOrEmpty(member_groupid))//all group
                        {
                            sSql += "";// and companyid =" + comm.getCompanyId().ToString() + "";
                        }
                        else 
                        {
                            sSql += " and Member.id in (select memberid from Member_Group_GroupMember where groupid=" + comm.to_sqlnumber(member_groupid) + ")";
                        }
                    }
                    else
                    {
                        if (comm.isSupervisor())
                        {
                            //lay danh sach member no quan ly theo nhom no duoc gan
                            if (string.IsNullOrEmpty(member_groupid))
                            {
                                sSql += " and (Member.id in(select memberid from Gps_membersmembers where supervisorid=" + comm.GetMemberID().ToString() + ") or Member.id=" + comm.GetMemberID().ToString();
                                //sSql += "  or Member.id in (select memberid from Member_Group_GroupMember where groupid in (select groupid from Member_Group_MemberGroup where memberid=" + comm.GetMemberID().ToString() + "))";
                                DataTable dttMembergroup = new DataTable();
                                string memberidInGroup = "0";
                                foreach (DataRow drr in dttMembergroup.Rows)
                                {
                                    memberidInGroup += "," + drr["memberid"].ToString();
                                }
                                sSql += " or Member.id in(" + memberidInGroup + ")";

                                sSql += ")";
                            }
                            else if (member_groupid != "-1")
                                sSql += " and Member.id in (select memberid from Member_Group_GroupMember where groupid=" + comm.to_sqlnumber(member_groupid) + ")";
                            else sSql += " and (Member.id in(select memberid from Gps_membersmembers where supervisorid=" + comm.GetMemberID().ToString() + ") or Member.id=" + comm.GetMemberID().ToString() + ")";

                        }
                        else sSql += " and Member.id=" + comm.GetMemberID().ToString();
                    }
                }
                //old version
                else
                {
                    if (comm.isAdmin()) sSql += "";// and companyid =" + comm.getCompanyId().ToString() + "";
                    else
                    {
                        if (comm.isSupervisor())
                        {
                            sSql += " and (Member.id in(select memberid from Gps_membersmembers where supervisorid=" + comm.GetMemberID().ToString() + ")"
                                    + " or Member.id=" + comm.GetMemberID().ToString() + ")";
                        }
                        else sSql += " and Member.id=" + comm.GetMemberID().ToString();
                    }
                }
            }

            if (comm.IsDevice()) sSql += " and Member.companyid in (select id from Company where [type]='device') ";//kieucb 22012013

            return sSql;
        }
        
        public static string getCondByCompany()
        {
            string sSql = "";

            //return "";
            //if (comm.isSuperAdmin()) sSql += "";
            if (comm.isSuperAdmin()) //kieucbok 12/04/2012
            {
                int companyidselected = comm.getCompanyIdSelected();
                if (companyidselected > 0) sSql += " and companyid=" + companyidselected.ToString();

                if (comm.IsDevice()) sSql += " and companyid in (select id from Company where [type]='device') ";//kieucb 22012013
            }
            else sSql += " and companyid=" + comm.getCompanyId().ToString();//" and memberid in (select memberid from Member where companyid =" + comm.getCompanyId().ToString() + ")";


            return sSql;
        }
        public static string getCondByCompanyGroup()
        {
            string sSql = "";

            //return "";
            //if (comm.isSuperAdmin()) sSql += "";
            if (comm.isSuperAdmin()) //kieucbok 12/04/2012
            {
                int companyidselected = comm.getCompanyIdSelected();
                if (companyidselected > 0) sSql += " and companyid=" + companyidselected.ToString();

                if (comm.IsDevice()) sSql += " and companyid in (select id from Company where [type]='device') ";//kieucb 22012013
            }
            else
            {
                if (comm.isAdmin())
                {
                    sSql += " and companyid=" + comm.getCompanyId().ToString();//" and memberid in (select memberid from Member where companyid =" + comm.getCompanyId().ToString() + ")";
                }
                else 
                {
                    if (comm.isSupervisor())
                    {
                        sSql += "  and id in (select groupid from Member_Group_MemberGroup where memberid=" + comm.GetMemberID().ToString() + ")";
                    }
                    else sSql += " and 1=-1 ";
                }
            }
            return sSql;
        }
        public static string getCondByCompanySpec()
        {
            string sSql = "";
            //return "";
            //if (comm.isSuperAdmin()) sSql += "";
            if (comm.isSuperAdmin()) //kieucb 12/04/2012
            {
                int companyidselected = comm.getCompanyIdSelected();
                if (companyidselected > 0) sSql += " and id=" + companyidselected.ToString();
            }
            else sSql += " and id=" + comm.getCompanyId().ToString();//" and memberid in (select memberid from Member where companyid =" + comm.getCompanyId().ToString() + ")";

            if (comm.IsDevice()) sSql += " and [type]='device' ";

            return sSql;
        }
        public static int getDepartmentId()
        {
            if (HttpContext.Current.Session["departmentid"] == null)
                return -1;

            return Convert.ToInt32(HttpContext.Current.Session["departmentid"].ToString());
        }
        // ------Het Cac ham xu ly thoi gian-------
        public static int GetMemberID()
        {
            return 0;
            //return lamdaica.common.commx.GetMemberID();
        }
        public static string GetMemberIweb()
        {

            {
                if (HttpContext.Current.Session["iweb"] == null)
                    return "-1";

                return (HttpContext.Current.Session["iweb"].ToString());
            }
        }
        public static string GetMemberIMEIToManageErp()
        {

            {
                if (HttpContext.Current.Session["listIMEIForERP"] == null)
                    return " and iddevice in ('-1')";

                return (" and iddevice in ("+HttpContext.Current.Session["listIMEIForERP"].ToString()+")");
            }
        }
        public static string GetMemberIMobile()
        {

            {
                if (HttpContext.Current.Session["imobile"] == null)
                    return "-1";

                return (HttpContext.Current.Session["imobile"].ToString());
            }
        }
        public static bool isTrialMember()
        {

            {
                if (HttpContext.Current.Session["trial"] == null)
                    return false;

                return (HttpContext.Current.Session["trial"].ToString() == "1");
            }
        }
        public static bool isFreeMember()
        {

            {
                if (HttpContext.Current.Session["freemember"] == null)
                    return false;

                return (HttpContext.Current.Session["freemember"].ToString() == "1");
            }
        }
        public static string GetMemberIMEI()
        {

            {
                if (HttpContext.Current.Session["imei"] == null)
                    return "-1";

                return (HttpContext.Current.Session["imei"].ToString());
            }
        }
        public static string GetTypeMemberID()
        {

            {
                if (HttpContext.Current.Session["TypeMember"] == null)
                    return "";

                return HttpContext.Current.Session["TypeMember"].ToString();
            }
        }
        public static bool IsNotRightForAction(string sOper)
        {
            return !IsRightForAction(sOper);

        }
        public static bool IsRightForAction(string sOper)
        {
            if (sOper == "del") { return IsRightForDelete(); }
            if (sOper == "edit") { return IsRightForUpdate(); }
            if (sOper == "add") { return IsRightForAdd(); }
            return false;
        }
        public static bool IsRightForDelete()
        {

            {
                string sRight = GetTypeMemberID();

                if (sRight.ToLower() == "admin") { return true; }
                return false;
            }
        }
        public static bool IsRightForUpdate()
        {

            {
                string sRight = GetTypeMemberID();

                if (sRight.ToLower() == "member" || sRight.ToLower() == "admin") { return true; }
                return false;
            }
        }
        public static bool IsRightForAdd()
        {

            {
                string sRight = GetTypeMemberID();

                if (sRight.ToLower() == "admin") { return true; }
                return false;
            }
        }
        public static string GetBussinessID()
        {
            {
                return "";
            }
        }
        public static string GetMemberName()
        {

            {
                if (HttpContext.Current.Session["MemberName"] == null)
                    return "";

                return HttpContext.Current.Session["MemberName"].ToString();
            }
        }
        public static string GetFullName()
        {

            {
                if (HttpContext.Current.Session["FullName"] == null)
                    return "";

                return HttpContext.Current.Session["FullName"].ToString();
            }
        }
        public static string GetMemberUserName()
        {

            {
                if (HttpContext.Current.Session["MemberUserName"] == null)
                    return "";

                return HttpContext.Current.Session["MemberUserName"].ToString();
            }
        }
        public static string GetMemberPassword()
        {

            {
                if (HttpContext.Current.Session["Password"] == null)
                    return "";

                return HttpContext.Current.Session["Password"].ToString();
            }
        }
        public static int GetAdminID()
        {
            {
                if (HttpContext.Current.Session["AdminID"] == null)
                    return -1;

                return Convert.ToInt32(HttpContext.Current.Session["AdminID"].ToString());
            }
        }

        public static string GetAdminName()
        {
            {
                if (HttpContext.Current.Session["AdminName"] == null)
                    return "";

                return HttpContext.Current.Session["AdminName"].ToString();
            }

        }


        //--------End function Identity function---
        //-----------Ham truy cap du lieu------------
        public static string GetFielTextByID(string sfiled, string stable, int id)
        {
            string stsql;
            stsql = "SELECT " + sfiled + " as fText FROM " + sfiled + " WHERE ID=" + to_sql(id.ToString(), "number");
            DataTable dt;
            dt = DBUtil.SelectTable(stsql);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["fText"].ToString();
            }
            return "";
        }
        //-----------Ham truy cap du lieu------------
        public static string GetFieldTextByText(string sfiled, string sfiledC, string stable, string sValue)
        {
            string stsql;
            stsql = "SELECT " + sfiled + " as fText FROM " + stable + " WHERE " + sfiledC + " =" + to_sql(sValue, "text");
            DataTable dt;
            dt = DBUtil.SelectTable(stsql);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["fText"].ToString();
            }
            return "";
        }

        public static string GetVariantFielTextIn(string sfiled)
        {
            string stsql;
            stsql = "SELECT " + sfiled + " as fText FROM Webvariants";
            DataTable dt;
            dt = DBUtil.SelectTable(stsql);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["fText"].ToString();
            }
            return "";
        }
        public static string GetVariantSizeWebContach()
        {
            return GetVariantFielTextIn("SizeWeb");
        }
        public static string GetVariantCSS()
        {
            return GetVariantFielTextIn("CSS");
        }
        public static string GetVariantInfo()
        {
            return GetVariantFielTextIn("Info");
        }
        public static string GetVariantContract()
        {
            return GetVariantFielTextIn("Contract");
        }
        public static string GetVariantMailContach()
        {
            return GetVariantFielTextIn("mailcontach");
        }
        public static int GetVariantPageSize()
        {
            int page_size = 0;
            try
            {
                page_size = Convert.ToInt32(ConfigurationSettings.AppSettings["page_size"]);
                //if (GetVariantFielTextIn("pagesize") == "") return 20;
                //return Convert.ToInt32(GetVariantFielTextIn("pagesize"));
            }
            catch (Exception ex) { page_size = 12; }
            if (page_size <= 0) page_size = 20;
            return page_size;
        }


        //-----------Het ham truy cap du lieu------------
        //KET THUC PHAN TA VIET CODE 
        public static DateTime Now
        {
            get
            {
                DateTime d = DateTime.Now;
                return d;
            }
        }
        // media

        // media
        //player to cut files for admin
        public static string getLayer(string link, string height, string auto)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;
            string width = "100%";// "600px";
            height = "100%"; //"450px";

            //link=repute(link);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/Control/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        //player to cut files for admin
        public static string getLayerToCutFiles(string link, string height, string auto)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;
            string width = "100%";// "600px";
            height = "100%"; //"450px";

            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/admin/Viewer/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/admin/Viewer/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        //player for admin
        public static string getLayerNotAuto(string link, string height, string auto)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;
            string width = "600px";
            height = "450px";

            link = link.Replace(" ~ ", "~");
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/admin/Viewer/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=false'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/admin/Viewer/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        //player split for admin
        public static string getLayerNotAutoSplit(string link, string height, string auto)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;
            string width = "600px";
            height = "450px";

            link = link.Replace(" ~ ", "~");
            link = comm.repute(link);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/admin/Viewer/playerCutFileOffline.swf?file=" + link + "&bufferTime=3&autoStart=false'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src=\"/admin/Viewer/playerCutFileOffline.swf?file=" + link.Replace("%7E", "~") + "&bufferTime=3&autoStart=true\" "
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        public static string getLayer(string link, string height)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;

            string width = "100%";
            height = "100%";

            link = link.Replace(" ~ ", "~");
            link = repute(link);

            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                //+ "  value='player.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + "  value='/Control/Viewer/playerCutFileOnline.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + "  value='player.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <embed src='/Control/Viewer/playerCutFileOnline.swf?file=" + link + "&bufferTime=3&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        public static string getLayerMultiPOffline(string link)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;

            string width = "100%";
            string height = "100%";

            link = link.Replace(" ~ ", "~");
            link = repute(link);

            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/control/Viewer/playerCutFileOffline.swf?file=" + link + "&bufferTime=10&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' /> "
                         + " <embed src='/control/Viewer/playerCutFileOffline.swf?file=" + link + "&bufferTime=10&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        //string _file,
        //string _duration,
        //string sizeLoad,
        //string urlFile,
        //string urlTimeFile,
        //string urlSizeFile
        public static string getLayerMultiPOffline(string[] infor)
        {
            string _file = infor[0];
            string _duration = infor[1];
            string sizeLoad = infor[2];
            string urlFile = repute(infor[3]);
            string urlTimeFile = repute(infor[4]);
            string urlSizeFile = repute(infor[5]);

            //_file
            string link = _file;
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;
            link = link.Replace(" ~ ", "~");
            link = repute(link);

            string width = "100%";
            string height = "100%";

            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " > "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/playerCutFileOffline.swf?file=" + link + "&_duration=" + _duration + "&sizeLoad=" + sizeLoad + "&urlFile=" + urlFile + "&urlTimeFile=" + urlTimeFile + "&sizeFile=" + urlSizeFile + "&bufferTime=10&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' /> "
                         + " <embed src='/Control/Viewer/playerCutFileOffline.swf?file=" + link + "&_duration=" + _duration + "&sizeLoad=" + sizeLoad + "&urlFile=" + urlFile + "&urlTimeFile=" + urlTimeFile + "&sizeFile=" + urlSizeFile + "&bufferTime=10&autoStart=true' allowfullscreen='true' "
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        // version new, can tour
        public static string getLayerMultiPOffline(string link, string _duration, string sizeLoad, string urlFile, string urlTimeFile, string urlSizeFile)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;

            string width = "100%";
            string height = "100%";

            link = link.Replace(" ~ ", "~");
            link = repute(link);

            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/playerCutFileOffline.swf?file=" + link + "&_duration=" + _duration + "&sizeLoad=" + sizeLoad + "&urlFile=" + urlFile + "&urlTimeFile=" + urlTimeFile + "&urlSizeFile=" + urlSizeFile + "&bufferTime=10&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' /> "
                         + " <embed src='/Control/Viewer/playerCutFileOffline.swf?file=" + link + "&_duration=" + _duration + "&sizeLoad=" + sizeLoad + "&urlFile=" + urlFile + "&urlTimeFile=" + urlTimeFile + "&urlSizeFile=" + urlSizeFile + "&bufferTime=10&autoStart=true' allowfullscreen='true' "
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        public static string getPlayerKaraoke(string link, string _duration, string filesave, string filekara)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            filekara = sSite + filekara;

            string width = "100%";
            string height = "100%";

            ////link = link.Replace(" ~ ", "~");
            ////link = repute(link);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/karaoke.swf?serviceUrl=" + link + "&filekara=" + filekara + "&filesave=" + filesave + "'  />"
                         + " <param name='quality'"
                         + "   value='high' /> "
                         + " <embed src='/Control/Viewer/karaoke.swf?serviceUrl=" + link + "&filekara=" + filekara + "&filesave=" + filesave + "' allowfullscreen='true' "
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        public static string getLayerVoiceMessage(string width, string height, string filesave)
        {

            if (width == "") width = "100%";
            if (height == "") height = "100%";
            string link = comm.GetUrlRecord();
            ////link = link.Replace(" ~ ", "~");
            ////link = repute(link);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/VoiceMessage.swf?serviceUrl=" + link + "&filesave=" + filesave + "'  />"
                         + " <param name='quality'"
                         + "   value='high' /> "
                         + " <embed src='/Control/Viewer/VoiceMessage.swf?serviceUrl=" + link + "&filesave=" + filesave + "' allowfullscreen='true' "
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' bgcolor='#FFFFFF' />"
                         + "</object>";
            return HTML;
        }
        public static string getPlayerForVoiceMessage(string width, string height, string filesave)
        {

            if (width == "") width = "100%";
            if (height == "") height = "90%";
            string link = comm.GetUrlRecord();
            ////link = link.Replace(" ~ ", "~");
            ////link = repute(link);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/PlayVoiceMessage.swf?serviceUrl=" + link + "&filesave=" + filesave + "'  />"
                         + " <param name='quality'"
                         + "   value='high' /> "
                         + " <embed src='/Control/Viewer/PlayVoiceMessage.swf?serviceUrl=" + link + "&filesave=" + filesave + "'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' bgcolor='#FFFFFF' allowfullscreen='true' />"
                         + "</object>";
            return HTML;
        }
        // control for smart media
        public static string getLayerPreload(string files)
        {

            string width = "200px";
            string height = "5px";

            string link = comm.GetUrlVideo();

            link = repute(link);

            files = repute(files);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/preLoadFiles.swf.swf?file=" + link + "&urlFile=" + files + "'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/Control/Viewer/preLoadFiles.swf.swf?file=" + link + "&urlFile=" + files + "' "
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' bgcolor='#FFFFFF' />"
                         + "</object>";
            return HTML;
        }
        // get player by id of program
        //player to cut files for admin
        public static string getLayerUser(string link, string height, string auto)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideoUser();
            link = sSite + link;
            string width = "100%";// "600px";
            height = "100%"; //"450px";

            //link=repute(link);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/Control/Viewer/playerNew.swf?file=" + link + "&bufferTime=3&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' bgcolor='#FFFFFF' />"
                         + "</object>";
            return HTML;
        }
        // get player by id of program
        //player to cut files for admin
        public static string getLayerUserShared(string link, string height, string auto)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideoUser();
            link = sSite + link;
            string width = "100%";// "600px";
            height = "100%"; //"450px";

            //link=repute(link);
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/playerEnable.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/Control/Viewer/playerEnable.swf?file=" + link + "&bufferTime=3&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' bgcolor='#FFFFFF' />"
                         + "</object>";
            return HTML;
        }
        public static string GetUploadVideoPath()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["uploadVideoFile"]);
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetConvertVideoPath()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["uploadVideoFile"] + "/WMV");
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUrlSite()
        {
            try
            {
                return ((ConfigurationSettings.AppSettings["sitename"]));
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUrlRoot()
        {
            try
            {
                return ((ConfigurationSettings.AppSettings["urlRoot"]));
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUrlOnline()
        {
            try
            {
                return ((ConfigurationSettings.AppSettings["urlOnline"]));
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUrlBroadCast()
        {
            try
            {
                return ((ConfigurationSettings.AppSettings["urlBroadCast"]));
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUrlRecord()
        {
            try
            {
                return ((ConfigurationSettings.AppSettings["urlRecord"]));
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUrlVideo()
        {
            string rel = "";
            try
            {
                int i = 1;
                while (true)
                {
                    if ((ConfigurationSettings.AppSettings["urlRoot" + i.ToString()]) != null)
                    {
                        if (i == 1) rel = (ConfigurationSettings.AppSettings["urlRoot" + i.ToString()]);
                        else rel = rel + "~" + (ConfigurationSettings.AppSettings["urlRoot" + i.ToString()]);
                        i++;
                    }
                    else break;
                }

            }
            catch (Exception ex) { return ""; }
            return rel;
        }
        public static string GetUploadImagePath()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["uploadImageFile"]);
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUploadVideoTempUser()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["uploadVideoFileTempUser"]);
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUploadVideoUser()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["uploadVideoFileUser"]);
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetUploadVideoTemp()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["uploadVideoFileTemp"]);
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetPathPlayVideo()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["pathPlayVideoFile"]);
            }
            catch (Exception ex) { return ""; }
        }
        public static string GetPathPlayVideoUser()
        {
            try
            {
                return (ConfigurationSettings.AppSettings["pathPlayVideoFileUser"]);
            }
            catch (Exception ex) { return ""; }
        }
        public static string getFileToLay(string slink)
        {
            try
            {
                return GetPathPlayVideo() + slink;
            }
            catch (Exception ex) { return ""; }
        }
        //image to upload
        public static string GetUrlImage()
        {
            try
            {
                return comm.GetUploadImagePath();
            }
            catch (Exception ex) { return ""; }
        }
        //Time span
        public static TimeSpan GetTimeSpanFromStr(string tm)
        {
            string[] arr = tm.Split(new string[] { ":" }, StringSplitOptions.None);
            return new TimeSpan(int.Parse(arr[0]), int.Parse(arr[1]), int.Parse(arr[2]));
        }
        //Time span
        public static TimeSpan GetTimeSpanFromMilis(string tm)
        {
            return TimeSpan.FromMilliseconds(double.Parse(tm));
            //string[] arr = tm.Split(new string[] { ":" }, StringSplitOptions.None);
            //return new TimeSpan(int.Parse(arr[0]), int.Parse(arr[1]), int.Parse(arr[2]));
        }
        //Time span
        public DataTable addSTTToTable(DataTable dt, int pos)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][pos] = (i + 1);
            }
            return dt;
        }
        //Time span
        public static DataTable Join2Tables(DataTable dt1, DataTable dt2)
        {
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dtR = dt1.NewRow();
                dtR.ItemArray = dt2.Rows[i].ItemArray;
                dt1.Rows.Add(dtR);
            }
            dt1.AcceptChanges();
            return dt1;
        }
        // ma hoa
        public static string repute(string inputString)
        {
            string rel = "";
            for (int i = inputString.Length - 1; i >= 0; i--) rel = rel + inputString.Substring(i, 1);
            return rel;
        }
        public static string getLinkFavor(string ID)
        {
            return "<a href=\"javascript: void(0)\" onclick=\"openWindow('/page/AddFavor.aspx?act=favor&edit=1&ID=" + ID + "','Add Favor',500,250,300,300);return false;\"><img src='/images/green_arrows.gif' />Kênh yêu thích</a>";
        }
        public static void checkLogin()
        {
            //if (comm.GetMemberID() < 0)
            //{
            //    HttpContext.Current.Response.Write("<Script LANGUAGE='JavaScript'>top.location.replace(\"../Default.aspx?show=log\");</script>");
            //}  
        }
        public static bool validateEmail(string sMail)
        {
            if (sMail.Length == 0) return false;
            string pattern = @"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$";
            System.Text.RegularExpressions.Match match = Regex.Match(sMail.Trim(), pattern, RegexOptions.IgnoreCase);
            return match.Success;
        }
        public static bool validateUsername(string sUserName)
        {
            string[] arrSkip = new string[] { " ", "-", "" };
            return sUserName.IndexOf(" ") == -1;
        }
        public static string getFormat2Digit(string number)
        {
            if (number.Length == 2) return number;
            else return "0" + number;
        }
        //broadcast webcam   
        //player broadcast webcam   
        public static string getPLayerBroadCast(string idBorad, string height, string auto)
        {
            string sService = comm.GetUrlOnline();
            string width = "100%";
            height = "100%";
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/BroadCast.swf?serviceUrl=" + sService + "&iduser=" + idBorad + "'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/Control/Viewer/BroadCast.swf?serviceUrl=" + sService + "&iduser=" + idBorad + "'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        //broadcast webcam   
        //player broadcast webcam   
        public static string getViewPLayerBroadCast(string idBorad, string height, string auto)
        {
            string sService = comm.GetUrlOnline();
            string width = "100%";
            height = "100%";
            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/Control/Viewer/BroadCastViewer.swf?serviceUrl=" + sService + "&iduser=" + idBorad + "'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + " <embed src='/Control/Viewer/BroadCastViewer.swf?serviceUrl=" + sService + "&iduser=" + idBorad + "'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }

        //end broadcast webcam

        #region PLAYER FOR ADMIN
        public static string getLayerMultiPOffline_admin(string link)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;

            string width = "100%";
            string height = "100%";

            link = link.Replace(" ~ ", "~");
            link = repute(link);

            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/admin/Viewer/playerCutFileOffline.swf?file=" + link + "&bufferTime=10&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' /> "
                         + " <embed src='/admin/Viewer/playerCutFileOffline.swf?file=" + link + "&bufferTime=10&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }
        public static string getLayer_admin(string link, string height)
        {
            string sSite = GetUrlRoot() + GetPathPlayVideo();
            link = sSite + link;

            string width = "100%";
            height = "100%";

            link = link.Replace(" ~ ", "~");
            link = repute(link);

            string HTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'"
                         + " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0'"
                         + " width='" + width + "' height='" + height + "'"
                         + " bgcolor='#FFFFFF'> "
                         + "<param name='movie' "
                         + "  value='/admin/Viewer/playerCutFileOffline.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <param name='quality'"
                         + "   value='high' />"
                         + "  value='player.swf?file=" + link + "&bufferTime=3&autoStart=true'  />"
                         + " <embed src='/admin/Viewer/playerCutFileOffline.swf?file=" + link + "&bufferTime=3&autoStart=true'"
                         + "  quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer'"
                         + " type='application/x-shockwave-flash' width='" + width + "' height='" + height + "' allowFullScreen='true' />"
                         + "</object>";
            return HTML;
        }

        #endregion

        public static string getValLanguage()
        {
            return "";
        }

        public static string getCondLanguage(string p)
        {
            return "1=1";
        }

        public static object getLanguage(string p, string p_2)
        {
            return p;
        }

        public static string GetCurrentYear()
        {
            return DateTime.Now.ToString("yyyy");
        }

        public static string GetCurrentMonth()
        {
            return DateTime.Now.ToString("MM");

        }
        public static int isName(string sInput)
        {
            //return -1;
            string UserChar = "zxcvbnmasdfghjklqwertyuiop1234567890 áàảãạấầẩẫậắằẳẵặéèẻẽẹếềểễệíìỉĩịóòỏõọốồổỗộớờởỡợúùủũụứừửữựýỳỷỹỵâăêôơưđ";
            int idx = -1;
            for (int i = 0; i < sInput.Length; i++)
            {
                idx = UserChar.IndexOf(sInput.ToLower().Substring(i, 1));
                if (idx == -1)
                {
                    return i;
                }
            }
            return -1;
        }
        public static int isUserName(string sInput)
        {
            string UserChar = "zxcvbnmasdfghjklqwertyuiop1234567890_@.";
            int idx = -1;
            for (int i = 0; i < sInput.Length; i++)
            {
                idx = UserChar.IndexOf(sInput.ToLower().Substring(i, 1));
                if (idx == -1)
                {
                    return i;
                }
            }
            return -1;
        }

        public static bool isshowroom()
        {
            if (HttpContext.Current.Session["isshowroom"] == null)
                return false;

            return (HttpContext.Current.Session["isshowroom"].ToString()) == "1";
        }
        public static bool isSaleThuongMai()
        {
            return !(comm.isAgency() || comm.isProject() || comm.isshowroom()
                || comm.isPTKD() || comm.isGiamDoc() || comm.isKeeper() || comm.isAccounting()
                || comm.isModerator()
                );
        }
        public static bool iscustomerservice()
        {
            if (HttpContext.Current.Session["iscustomerservice"] == null)
                return false;

            return (HttpContext.Current.Session["iscustomerservice"].ToString()) == "1";

        }
        public static bool isalarmproduct()
        {
            if (HttpContext.Current.Session["isalarmproduct"] == null)
                return false;

            return (HttpContext.Current.Session["isalarmproduct"].ToString()) == "1";

        }
        public static bool isSmartBrowser(string type) 
        {
            //GETS THE CURRENT USER CONTEXT
            HttpContext context = HttpContext.Current;
            if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                if (type != "") return context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains(type.ToLower());
            }
            return false;
        }
        public static bool isMobileBrowser()
        {
            //GETS THE CURRENT USER CONTEXT
            HttpContext context = HttpContext.Current;

            //FIRST TRY BUILT IN ASP.NT CHECK
            if (context.Request.Browser.IsMobileDevice)
            {
                return true;
            }
            //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
            if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
            {
                return true;
            }
            //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
            if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
                context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
            {
                return true;
            }
            //AND FINALLY CHECK THE HTTP_USER_AGENT 
            //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
            if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                //Create a list of all mobile types
                string[] mobiles =
                    new[]
                {
                    "midp", "j2me", "avant", "docomo", 
                    "novarra", "palmos", "palmsource", 
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/", 
                    "blackberry", "mib/", "symbian", 
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio", 
                    "SIE-", "SEC-", "samsung", "HTC", 
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx", 
                    "NEC", "philips", "mmm", "xx", 
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java", 
                    "pt", "pg", "vox", "amoi", 
                    "bird", "compal", "kg", "voda",
                    "sany", "kdd", "dbt", "sendo", 
                    "sgh", "gradi", "jb", "dddi", 
                    "moto", "iphone","android"
                };

                //Loop through each item in the list created above 
                //and check if the header contains that text
                foreach (string s in mobiles)
                {
                    if (context.Request.ServerVariables["HTTP_USER_AGENT"].
                                                        ToLower().Contains(s.ToLower()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        public static double distanceXY(double lat1, double lon1, double lat2, double lon2)
        {
            /* old
            int R = 6371; // km (change this constant to get miles)

            double dLat = (lat2 - lat1) * Math.PI / 180;

            double dLon = (lon2 - lon1) * Math.PI / 180;

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) *

            Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            double d = R * c;
            return (d);
            */
            return 0;
        }
        public static string getNearestPoint(string fromLatLng, string[] arrPoint) 
        {
            try 
            {
                double lat1 = double.Parse(fromLatLng.Substring(0, fromLatLng.IndexOf(",")));
                double lng1 = double.Parse(fromLatLng.Substring(fromLatLng.IndexOf(",") + 1));

                string nearestPoint = "";
                double minDistance = 0;
                foreach (string s in arrPoint) 
                {
                    double lat2 = double.Parse(s.Substring(0, s.IndexOf(",")));
                    double lng2 = double.Parse(s.Substring(s.IndexOf(",") + 1));

                    double distance = distanceXY(lat1, lng1, lat2, lng2);
                    if (distance < minDistance || minDistance == 0)
                    {
                        minDistance = distance;
                        nearestPoint = s;
                    }
                }
                return nearestPoint;
            }
            catch 
            {
            }
            return "";
        }
        public static string getNearestPointByRadius(string fromLatLng, string[] arrPoint,double radius)
        {
            try
            {
                double lat1 = double.Parse(fromLatLng.Substring(0, fromLatLng.IndexOf(",")), System.Globalization.CultureInfo.InvariantCulture);
                double lng1 = double.Parse(fromLatLng.Substring(fromLatLng.IndexOf(",") + 1), System.Globalization.CultureInfo.InvariantCulture);

                string nearestPoint = "";
                double minDistance = 0;
                foreach (string s in arrPoint)
                {
                    double lat2 = double.Parse(s.Substring(0, s.IndexOf(",")), System.Globalization.CultureInfo.InvariantCulture);
                    double lng2 = double.Parse(s.Substring(s.IndexOf(",") + 1), System.Globalization.CultureInfo.InvariantCulture);

                    double distance = distanceXY(lat1, lng1, lat2, lng2);
                    if (distance < minDistance || minDistance == 0)
                    {
                        minDistance = distance;
                        nearestPoint = s;
                    }
                }
                if (minDistance < radius) return nearestPoint;
                return "";
            }
            catch
            {
            }
            return "";
        }

        public static string getCurrentKeyword() 
        {
            {
                if (HttpContext.Current.Session["currentkeyword"] == null)
                    return "";

                return (HttpContext.Current.Session["currentkeyword"].ToString());
            }
        }
        public static void setCurrentKeyword(string keyword)
        {
            HttpContext.Current.Session["currentkeyword"] = keyword;
        }
        public static bool IsNumber(string s) 
        {
            string n = "0123456789";
            for (int i = 0; i < s.Length; i++) 
            {
                if (n.IndexOf(s.Substring(i, 1)) == -1) return false;
            }
                return true;
        }
        public static string getDBODataName() 
        {

            return "lamdaica.common.commx.getDBODataName()";
        }
        /// <summary>
        /// Merge du lieu cua cac table co cung cau truc du lieu
        /// </summary>
        /// <param name="arrDT"></param>
        /// <returns></returns>
        public static DataTable merge2DataTable(DataTable[] arrDT) 
        {
            if (arrDT.Length == 0) return new DataTable();

            int a = 0;
            int j = 0;
            DataTable dt;
            for (int i = 0; i < arrDT.Length; i++) 
            {
                if (a < arrDT[i].Rows.Count) { a = arrDT[i].Rows.Count; j = i; }
            }
            dt = arrDT[j].Clone();
            foreach (DataTable dt1 in arrDT)
            {
                foreach (DataRow dr in dt1.Rows)
                {
                    DataRow drNew = dt.NewRow();
                    drNew.ItemArray = dr.ItemArray;
                    dt.Rows.Add(drNew);
                    //dt.Rows.Add(dr);
                }
            }
            dt.AcceptChanges();
            return dt;
        }
        public static int GetGroupId()
        {
            try
            {

                if (HttpContext.Current.Session["GroupID"] == null)
                    return -1;

                return Convert.ToInt32(HttpContext.Current.Session["GroupID"].ToString());
            }
            catch { return -1; }
        }
        public static string addFilesToZip(string[] files, string pathFileSave) 
        {
            return "";
        }
        public static string TrimVietnameseMark(string str, bool isLower)
        {
            if (string.IsNullOrEmpty(str)) return "";
            str = str.Replace("ấ", "a");
            str = str.Replace("ầ", "a");
            str = str.Replace("ẩ", "a");
            str = str.Replace("ẫ", "a");
            str = str.Replace("ậ", "a");

            str = str.Replace("Ấ", "A");
            str = str.Replace("Ầ", "A");
            str = str.Replace("Ẩ", "A");
            str = str.Replace("Ẫ", "A");
            str = str.Replace("Ậ", "A");

            str = str.Replace("ắ", "a");
            str = str.Replace("ằ", "a");
            str = str.Replace("ẳ", "a");
            str = str.Replace("ẵ", "a");
            str = str.Replace("ặ", "a");

            str = str.Replace("Ắ", "A");
            str = str.Replace("Ằ", "A");
            str = str.Replace("Ẳ", "A");
            str = str.Replace("Ẵ", "A");
            str = str.Replace("Ặ", "A");

            str = str.Replace("á", "a");
            str = str.Replace("à", "a");
            str = str.Replace("ả", "a");
            str = str.Replace("ã", "a");
            str = str.Replace("ạ", "a");
            str = str.Replace("â", "a");
            str = str.Replace("ă", "a");

            str = str.Replace("Á", "A");
            str = str.Replace("À", "A");
            str = str.Replace("Ả", "A");
            str = str.Replace("Ã", "A");
            str = str.Replace("Ạ", "A");
            str = str.Replace("Â", "A");
            str = str.Replace("Ă", "A");

            str = str.Replace("ế", "e");
            str = str.Replace("ề", "e");
            str = str.Replace("ể", "e");
            str = str.Replace("ễ", "e");
            str = str.Replace("ệ", "e");

            str = str.Replace("Ế", "E");
            str = str.Replace("Ề", "E");
            str = str.Replace("Ể", "E");
            str = str.Replace("Ễ", "E");
            str = str.Replace("Ệ", "E");

            str = str.Replace("é", "e");
            str = str.Replace("è", "e");
            str = str.Replace("ẻ", "e");
            str = str.Replace("ẽ", "e");
            str = str.Replace("ẹ", "e");
            str = str.Replace("ê", "e");

            str = str.Replace("É", "E");
            str = str.Replace("È", "E");
            str = str.Replace("Ẻ", "E");
            str = str.Replace("Ẽ", "E");
            str = str.Replace("Ẹ", "E");
            str = str.Replace("Ê", "E");

            str = str.Replace("í", "i");
            str = str.Replace("ì", "i");
            str = str.Replace("ỉ", "i");
            str = str.Replace("ĩ", "i");
            str = str.Replace("ị", "i");

            str = str.Replace("Í", "I");
            str = str.Replace("Ì", "I");
            str = str.Replace("Ỉ", "I");
            str = str.Replace("Ĩ", "I");
            str = str.Replace("Ị", "I");

            str = str.Replace("ố", "o");
            str = str.Replace("ồ", "o");
            str = str.Replace("ổ", "o");
            str = str.Replace("ỗ", "o");
            str = str.Replace("ộ", "o");

            str = str.Replace("Ố", "O");
            str = str.Replace("Ồ", "O");
            str = str.Replace("Ổ", "O");
            str = str.Replace("Ô", "O");
            str = str.Replace("Ộ", "O");

            str = str.Replace("ớ", "o");
            str = str.Replace("ờ", "o");
            str = str.Replace("ở", "o");
            str = str.Replace("ỡ", "o");
            str = str.Replace("ợ", "o");

            str = str.Replace("Ớ", "O");
            str = str.Replace("Ờ", "O");
            str = str.Replace("Ở", "O");
            str = str.Replace("Ỡ", "O");
            str = str.Replace("Ợ", "O");

            str = str.Replace("ứ", "u");
            str = str.Replace("ừ", "u");
            str = str.Replace("ử", "u");
            str = str.Replace("ữ", "u");
            str = str.Replace("ự", "u");

            str = str.Replace("Ứ", "U");
            str = str.Replace("Ừ", "U");
            str = str.Replace("Ử", "U");
            str = str.Replace("Ữ", "U");
            str = str.Replace("Ự", "U");

            str = str.Replace("ý", "y");
            str = str.Replace("ỳ", "y");
            str = str.Replace("ỷ", "y");
            str = str.Replace("ỹ", "y");
            str = str.Replace("ỵ", "y");

            str = str.Replace("Ý", "Y");
            str = str.Replace("Ỳ", "Y");
            str = str.Replace("Ỷ", "Y");
            str = str.Replace("Ỹ", "Y");
            str = str.Replace("Ỵ", "Y");

            str = str.Replace("Đ", "D");
            str = str.Replace("Đ", "D");
            str = str.Replace("đ", "d");

            str = str.Replace("ó", "o");
            str = str.Replace("ò", "o");
            str = str.Replace("ỏ", "o");
            str = str.Replace("õ", "o");
            str = str.Replace("ọ", "o");
            str = str.Replace("ô", "o");
            str = str.Replace("ơ", "o");

            str = str.Replace("Ó", "O");
            str = str.Replace("Ò", "O");
            str = str.Replace("Ỏ", "O");
            str = str.Replace("Õ", "O");
            str = str.Replace("Ọ", "O");
            str = str.Replace("Ô", "O");
            str = str.Replace("Ơ", "O");

            str = str.Replace("ú", "u");
            str = str.Replace("ù", "u");
            str = str.Replace("ủ", "u");
            str = str.Replace("ũ", "u");
            str = str.Replace("ụ", "u");
            str = str.Replace("ư", "u");

            str = str.Replace("Ú", "U");
            str = str.Replace("Ù", "U");
            str = str.Replace("Ủ", "U");
            str = str.Replace("Ũ", "U");
            str = str.Replace("Ụ", "U");
            str = str.Replace("Ư", "U");

            if (isLower) str = str.ToLower();

            return str;
        }

        //public static string ToJson(System.Collections.Generic.List<autocomplete> lst) 
        public static string ToJson(object lst) 
        {
            //return Jayrock.Json.Conversion.JsonConvert.ExportToString(lst);
            return Newtonsoft.Json.JsonConvert.SerializeObject(lst);
        }
        public static string[] getTimeUsingData()
        {
            try
            {
                if (HttpContext.Current.Session["Timeusing"] == null) return new string[] { "" };
                string timeusing = HttpContext.Current.Session["Timeusing"].ToString();
                string[] arr = timeusing.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                if (arr[2].IndexOf("," + GetMemberUserName() + ",") != -1) return new string[] { "" };
                return arr;
            }
            catch { }
            return new string[] { "" };
        }

        public static bool isCheckValidControl(string sIdDevice)
        {
            return true;
            return comm.isSuperAdmin() 
                   ||(comm.GetMemberIMEI() == sIdDevice);
        }
    }
    public class autocomplete 
    {
        private string _value;
        private string _label;
        public autocomplete() 
        {
        }
        public string value
        {
            get { return _value; }
            set { this._value = value; }
        }
        public string label
        {
            get { return _label; }
            set { this._label = value; }
        }
    }
}
