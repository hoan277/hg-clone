﻿using lalatv.dbmanager;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;

namespace lalatv.dbmanager
{
    public class LalatvUtil
    {
        //subcribe:20190828090505:hongancp,Hongancp!@#45,LALATV_NGAY,975293382,20190828090519,20190828090519,REAL,3000,#FULLSM,20190829000000,
        //getcontent:20190826102309:hongancp,Hongancp!@#45,LALATV_NGAY,867728907,Kta,0,Kta

        public static int update_create_user(string phone,string subtype, string substate, string fullcmd)
        {
            update_create_user_log(phone, subtype, substate, fullcmd);
            phone = common_phone(phone);
            DataTable dtx = check_user(phone);
            if (dtx.Rows.Count == 0)
            {
                string sSql = "INSERT INTO  film_users" +
                "(username ,password ,avatar ,email ,phone ,fullname,`desc`," +
                "datecreated ,userid ,cpid , roleid ,gender , birthday,sub_type,sub_state)" +
                " VALUES "
                + "(" + comm.to_sqltext(phone) + ",'123456' ,''," + comm.to_sqltext(phone)
                + " ," + comm.to_sqltext(phone) + " ," + comm.to_sqltext(phone)
                + "," + comm.to_sqltext(fullcmd) + ","
                + "CURRENT_DATE() ,-1 ,1 , -1 ,-1 , null,"+ comm.to_sqlnumber(subtype) + "," + comm.to_sqlnumber(substate)+")";
                int x=DBUtil.Execute(sSql);
                LogUtil.getInstance().logSk(sSql+"====>"+x.ToString(), "update");
                return x;
            }
            else
            {
                string sSql = "update film_users set fullname=fullname ";
                if (subtype != "-1") sSql = sSql + ",sub_type=" + comm.to_sqlnumber(subtype);
                if (substate != "-1") sSql = sSql + ",sub_state = " + comm.to_sqlnumber(substate);
                sSql= sSql+" where phone=" +comm.to_sqltext(phone);
                int x = DBUtil.Execute(sSql);
                LogUtil.getInstance().logSk(sSql + "====>" + x.ToString(), "update");
                return x;
            }
        }
        public static void update_create_user_log(string phone, string subtype, string substate, string fullcmd)
        {
            phone = common_phone(phone);
            
            string sSql = "INSERT INTO  film_users_log" +
            "(username ,password ,avatar ,email ,phone ,fullname,`desc`," +
            "datecreated ,userid ,cpid , roleid ,gender , birthday,sub_type,sub_state)" +
            " VALUES "
            + "(" + comm.to_sqltext(phone) + ",'123456' ,''," + comm.to_sqltext(phone)
            + " ," + comm.to_sqltext(phone) + " ," + comm.to_sqltext(phone)
            + "," + comm.to_sqltext(fullcmd) + ","
            + "CURRENT_DATE() ,-1 ,1 , -1 ,-1 , null," + comm.to_sqlnumber(subtype) + "," + comm.to_sqlnumber(substate) + ")";
            int x = DBUtil.Execute(sSql);
            //x = x + 1;
            LogUtil.getInstance().logSk(sSql + "====>" + x.ToString(), "update");
        }
        public static DataTable check_user(string phone)
        {
            phone = common_phone(phone);
            string sSql = "select *  from film_users where phone=" + comm.to_sqltext(phone) ;
            return DBUtil.SelectTable(sSql);
        }
        public static string getStateUser(string phone)
        {
            phone = common_phone(phone);
            string sSql = "select * from film_users where phone=" + comm.to_sqltext(phone);
            DataTable dt= check_user(phone);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["sub_state"].ToString();
            }
            return "0";
        }
        public static string getSubTypeUser(string phone)
        {
            phone = common_phone(phone);
            string sSql = "select * from film_users where phone=" + comm.to_sqltext(phone);
            DataTable dt = check_user(phone);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["sub_type"].ToString();
            }
            return "0";
        }
        public static string[] get_user_subcribe(string phone)
        {
            phone = common_phone(phone);
            string sSql = "select sub_type,sub_state from film_users where phone=" + comm.to_sqltext(phone);
            DataTable dt = check_user(phone);
            if (dt.Rows.Count > 0)
            {
                return new string[] { dt.Rows[0]["sub_type"].ToString(),dt.Rows[0]["sub_state"].ToString()};
            }
            return new string[]{ "0","0"};
        }
        public static string common_phone(string phone)
        {
            if (!phone.StartsWith("0")) phone = "0" + phone;           
            return phone;
        }
        public static void call_to_url_api(string phone, string subtype, string substate, string line)
        {
            
            try
            {
                new Thread(new ThreadStart(
                    delegate
                    {
                        call_to_url_api_in_a_thread(phone,  subtype,  substate,  line);
                    }
                    )).Start();
            }
            catch (Exception ex)
            {
                LogUtil.getInstance().logSk("Exception when call api in main thread ====>" + phone.ToString() + "====>" + ex.ToString(), "update_api");
            }
        }
        public static void call_to_url_api_in_a_thread(string phone, string subtype, string substate, string line)
        {
            line = line.Replace("#", "-");
            WebClient web = new WebClient();
            web.Encoding = System.Text.Encoding.UTF8;
            string url = "http://syn.lalatv.com.vn/sms?phone=" + phone;
            url = url + "&subtype=" + subtype;
            url = url + "&substate=" + substate;
            url = url + "&line=" + line;
            url = url + "&timestamp=" + DateTime.Now.Ticks.ToString();
            try
            {
                string ex = web.DownloadString(url);
                LogUtil.getInstance().logSk("Result call to api ====>" + url.ToString() + "====>" + ex.ToString(), "update_api");

            }
            catch (Exception ex)
            {
                LogUtil.getInstance().logSk("Result call to api ====>" + url.ToString() + "====>" + ex.ToString(), "update_api");
            }
        }
        public static string SubRequest(string username, string password, string serviceid, string msisdn, string chargetime, string @params, string mode, int amount, string command, string nextrenew, string cp_request_id)
        {
            LogUtil.logItem("subcribe.txt", new string[] { "====>", username, password, serviceid, msisdn, chargetime, @params, mode, amount.ToString(), command, nextrenew, cp_request_id });
            string s = "0";
            string msg = "Dang ky dich vu lalatv thanh cong,truy cap http://lalatv.com.vn de download va xem video";
            try
            {
                if (!(username == "hongancp" && password == "Hongancp!@#45"))
                {
                    return "301|Sai ten dang nhap hoac mat khau";
                }
                if (string.IsNullOrEmpty(serviceid) ||
                    string.IsNullOrEmpty(msisdn) || string.IsNullOrEmpty(chargetime) ||
                     string.IsNullOrEmpty(@params) || string.IsNullOrEmpty(mode) ||
                      string.IsNullOrEmpty(command) || string.IsNullOrEmpty(nextrenew)
                    )
                {
                    return "300|Sai tham so";
                }
                using (StreamWriter w = new StreamWriter(LogUtil.dirLog + "subcribe.txt", true, Encoding.UTF8))
                {
                    string sLine = DateTime.Now.ToString("yyyyMMddHHmmss:") +
                         username + "," + password + "," + serviceid + "," + msisdn + "," + chargetime + "," + @params + "," + mode + "," + amount.ToString() + "," + command + "," + nextrenew + "," + cp_request_id;
                    w.WriteLine(sLine);
                    string subtype = "0";
                    string substate = "1";
                    if (serviceid == "LALATV_NGAY") subtype = "1";
                    if (serviceid == "LALATV_TUAN") subtype = "2";
                    if (serviceid == "LALATV_THANG") subtype = "3";
                    
                    if (command.ToLower().StartsWith("huy")) subtype = "0";
                    //if (amount == 0) subtype = "0";
                    lalatv.dbmanager.LalatvUtil.update_create_user(msisdn, subtype, substate, sLine);
                    lalatv.dbmanager.LalatvUtil.call_to_url_api(msisdn, subtype, substate, sLine);
                }

            }
            catch (Exception ex)
            {
                LogUtil.logItem("subcribe.txt", new string[] { "Exception:" + ex.ToString() });
                s = "302";
                msg = "Server lalatv dang ban, xin vui long thu lai sau";
            }
            string result = s + "|" + msg;
            LogUtil.logItem("subcribe.txt", "==============================>" + result);
            return result;
        }
    }
}