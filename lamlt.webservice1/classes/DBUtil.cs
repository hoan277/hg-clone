﻿using System;
using System.Data;
using MySql.Data.MySqlClient;// System.Data.SqlClient;
using System.Data.OleDb;
using SqlConnection = MySql.Data.MySqlClient.MySqlConnection;
using SqlCommand = MySql.Data.MySqlClient.MySqlCommand;
using SqlParameter= MySql.Data.MySqlClient.MySqlParameter;
using SqlDataAdapter= MySql.Data.MySqlClient.MySqlDataAdapter;
using SqlTransaction = MySql.Data.MySqlClient.MySqlTransaction;
using System.Configuration;
using System.Web;
using System.IO;

/// <summary>
/// Summary description for dal
/// </summary>
namespace lalatv.dbmanager
{
    public class DBUtil
    {
        public static SqlConnection connSql;

        public static string strConn = "";
        public static string strConnErp = "";


        public DBUtil()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region connection string
        public static void GetConnection()
        {
            try
            {
                strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];

                //if (HttpContext.Current.Application["ConnectionString"] != null)
                //    strConn = HttpContext.Current.Application["ConnectionString"].ToString();
                //else
                //{
                //    strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
                //    HttpContext.Current.Application["ConnectionString"] = strConn;
                //}
            }
            catch (Exception ex)
            {
                //strConn = ConfigurationSettings.AppSettings["ConnectionString"];
                strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
                //HttpContext.Current.Application["ConnectionString"] = strConn;
                //throw ex;
            }
            finally
            {

            }

        }
        public static void GetConnectionErp()
        {
            try
            {
                strConnErp = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
            }
            catch (Exception ex)
            {
                //strConn = ConfigurationSettings.AppSettings["ConnectionString"];
                strConnErp = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
                //HttpContext.Current.Application["ConnectionString"] = strConn;
                //throw ex;
            }
            finally
            {

            }

        }
        #endregion
        #region Open/close
        //public static void cnopen()
        //{
        //    if (connSql == null)
        //    {
        //        //files.writeLog("Hàm cnopen: Mở kết nối");
        //        strConn = ConfigurationSettings.AppSettings["ConnectionString"];
        //        connSql = new SqlConnection(strConn);
        //        connSql.Open();
        //    }
        //    else
        //    {
        //        //files.writeLog("Hàm cnopen: Tại sao connSql khác null");
        //        if (connSql.State != ConnectionState.Open)
        //        {
        //            //files.writeLog("Hàm cnopen: Tại sao connSql khác null mà trạng thái lại khác Open");               
        //            connSql.Open();
        //        }
        //    }
        //}
        //public static void cnclose()
        //{
        //    if (connSql != null)
        //    {
        //        if(connSql.State != ConnectionState.Closed)
        //            connSql.Close();
        //        connSql = null;
        //        //files.writeLog("Hàm cnclose: Đóng kết nối");
        //    }
        //    else
        //    {
        //        //files.writeLog("Hàm cnclose: Tại sao đã đóng kết nối");
        //    }
        //}
        #endregion
        #region execute querry command
        
        public static int Execute(string strSQL)
        {
            //return ExecuteOracle(strSQL);
            int val = 0;
            GetConnection();
            if (ConfigurationManager.AppSettings["poolingsql"] != "0")
            {

                using (SqlConnection connSql = new SqlConnection(strConn))
                {
                    //SqlConnection connSql = new SqlConnection(strConn);
                    try
                    {
                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        cmdSql.CommandTimeout = 1200 * 5;
                        val = cmdSql.ExecuteNonQuery();
                        cmdSql.Dispose();
                    }
                    catch (Exception ex)
                    {
                        logItem("Exception when execute sql In DBManager.DLL:" + strSQL + ":" + ex.ToString());
                        AddToLogToNeedProcess(ex.ToString(), strSQL);
                        //throw ex;
                    }
                    //finally
                    //{
                    //    connSql.Close();
                    //    connSql.Dispose();
                    //}
                }
            }
            else
            {
                SqlConnection connSql = new SqlConnection(strConn);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    cmdSql.CommandTimeout = 5 * 60000;
                    val = cmdSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //System.Data.SqlClient.SqlException: Transaction (Process ID 59) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction
                    logItem("Error Select Sql And Add To Queue In DBManager.DLL:" + strSQL + "===>" + ex.ToString());
                    AddToLogToNeedProcess(ex.ToString(), strSQL);
                }
                finally
                {
                    // SqlConnection.ClearAllPools();
                    connSql.Close();
                    connSql.Dispose();
                }
            }
            return val;
        }
        public static int ExecutePtracker(string strSQL)
        {
            //return ExecuteOracle(strSQL);
            int val = 0;
            GetConnection();
            if (ConfigurationManager.AppSettings["poolingsql"] != "0")
            {

                using (SqlConnection connSql = new SqlConnection(strConnPtracker))
                {
                    //SqlConnection connSql = new SqlConnection(strConn);
                    try
                    {
                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        cmdSql.CommandTimeout = 1200 * 5;
                        val = cmdSql.ExecuteNonQuery();
                        cmdSql.Dispose();
                    }
                    catch (Exception ex)
                    {
                        logItem("Exception when execute sql In DBManager.DLL:" + strSQL + ":" + ex.ToString());
                        AddToLogToNeedProcess(ex.ToString(), strSQL);
                        //throw ex;
                    }
                    //finally
                    //{
                    //    connSql.Close();
                    //    connSql.Dispose();
                    //}
                }
            }
            else
            {
                SqlConnection connSql = new SqlConnection(strConnPtracker);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    cmdSql.CommandTimeout = 5 * 60000;
                    val = cmdSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //System.Data.SqlClient.SqlException: Transaction (Process ID 59) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction
                    logItem("Error Select Sql And Add To Queue In DBManager.DLL:" + strSQL + "===>" + ex.ToString());
                    AddToLogToNeedProcess(ex.ToString(), strSQL);
                }
                finally
                {
                    // SqlConnection.ClearAllPools();
                    connSql.Close();
                    connSql.Dispose();
                }
            }
            return val;
        }
        public static int ExecuteErp(string strSQL)
        {
            //return ExecuteOracle(strSQL);
            int val = 0;
            GetConnection();
            if (ConfigurationManager.AppSettings["poolingsql"] != "0")
            {

                using (SqlConnection connSql = new SqlConnection(strConnErp))
                {
                    //SqlConnection connSql = new SqlConnection(strConn);
                    try
                    {
                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        cmdSql.CommandTimeout = 1200 * 5;
                        val = cmdSql.ExecuteNonQuery();
                        cmdSql.Dispose();
                    }
                    catch (Exception ex)
                    {
                        logItem("Exception when execute sql In DBManager.DLL:" + strSQL + ":" + ex.ToString());
                        AddToLogToNeedProcessErp(ex.ToString(), strSQL);
                        //throw ex;
                    }
                    //finally
                    //{
                    //    connSql.Close();
                    //    connSql.Dispose();
                    //}
                }
            }
            else
            {
                SqlConnection connSql = new SqlConnection(strConnErp);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    cmdSql.CommandTimeout = 5 * 60000;
                    val = cmdSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //System.Data.SqlClient.SqlException: Transaction (Process ID 59) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction
                    logItem("Error Select Sql And Add To Queue In DBManager.DLL:" + strSQL + "===>" + ex.ToString());
                    AddToLogToNeedProcessErp(ex.ToString(), strSQL);
                }
                finally
                {
                    // SqlConnection.ClearAllPools();
                    connSql.Close();
                    connSql.Dispose();
                }
            }
            return val;
        }
        public static void logItem(string s) 
        {
            try
            {
                string dirLog=LogUtil.getInstance().dirRoot;
                //if (!Directory.Exists("D:\\website\\ptrackerall\\log\\")) Directory.CreateDirectory("D:\\website\\ptrackerall\\log\\");
                //using (StreamWriter w = new StreamWriter("D:\\website\\ptrackerall\\log\\logSqlError" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true))
                using (StreamWriter w = new StreamWriter(dirLog+"logSqlError" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true))
                {
                    w.WriteLine(DateTime.Now.ToString("HH:mm") + ":" + s);
                }
            }
            catch { }
        }
        public static void AddToLogToNeedProcess(string ex, string sSql)
        {
            if (ex.ToLower().IndexOf("was deadlocked on lock | communication buffer resources") != -1)
            {
                logItemQueue(sSql);
            }
        }
        public static void logItemQueue(string sSql)
        {
            if (sSql.Split(';').Length >= 5) return;//skip recursive
            string sDirFailOver = @"C://log//";
            if (ConfigurationManager.AppSettings["dirfailover"] != null)
            {
                sDirFailOver = ConfigurationManager.AppSettings["dirfailover"];
            }
            try
            {
                if (!Directory.Exists(sDirFailOver)) Directory.CreateDirectory(sDirFailOver);
                using (StreamWriter w = new StreamWriter(sDirFailOver + "\\ASqlErrorToProcess.txt", true))
                {
                    w.WriteLine(sSql);
                }
            }
            catch { }
        }
        public static void AddToLogToNeedProcessErp(string ex, string sSql)
        {
            if (ex.ToLower().IndexOf("was deadlocked on lock | communication buffer resources") != -1)
            {
                logItemQueueErp(sSql);
            }
        }
        public static void logItemQueueErp(string sSql)
        {
            if (sSql.Split(';').Length >= 5) return;//skip recursive
            string sDirFailOver = @"C://lalatv//log/";
            if (ConfigurationManager.AppSettings["dirfailoverErp"] != null) 
            {
                sDirFailOver = ConfigurationManager.AppSettings["dirfailoverErp"];
            }
            try
            {
                if (!Directory.Exists(sDirFailOver)) Directory.CreateDirectory(sDirFailOver);
                using (StreamWriter w = new StreamWriter(sDirFailOver + "\\ASqlErrorToProcess.txt", true))
                {
                    w.WriteLine(sSql);
                }
            }
            catch { }
        }
        public static int Execute(string strSQL, SqlParameter param)
        {
            int val = 0;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                cmdSql.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
                val = cmdSql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static int Execute(string strSQL, SqlParameter[] param)
        {
            int val = 0;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                for (int i = 0; i < param.Length; i++)
                {
                    cmdSql.Parameters.Add(new SqlParameter(param[i].ParameterName, param[i].Value));
                }
                val = cmdSql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        #endregion

        #region execute Scalar
        public static object ExecuteScalar(string strSQL)
        {
            object val = null;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                val = cmdSql.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static object ExecuteScalar(string strSQL, SqlParameter param)
        {
            object val = null;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                cmdSql.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
                val = cmdSql.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static object ExecuteScalar(string strSQL, SqlParameter[] param)
        {
            object val = null;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                for (int i = 0; i < param.Length; i++)
                    cmdSql.Parameters.Add(new SqlParameter(param[i].ParameterName, param[i].Value));
                val = cmdSql.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connSql.Close();
            }
            return val;
        }
        #endregion

        #region Select table
        public static DataTable SelectTable(string strSQL)
        {
            DataTable val = new DataTable();
            GetConnection();
            //////return SelectTableOracle(strSQL);
            if (ConfigurationManager.AppSettings["poolingsql"] != "0")
            {
                using (SqlConnection connSql = new SqlConnection(strConn))
                {
                    //SqlConnection connSql = new SqlConnection(strConn);
                    try
                    {

                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        cmdSql.CommandTimeout = 1200 * 5;
                        SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                        daSql.Fill(val);
                    }
                    catch (Exception ex)
                    {
                        logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                        //throw ex;
                    }
                    finally
                    {
                        connSql.Dispose();
                    }
                    //finally
                    //{
                    //    connSql.Close();
                    //    connSql.Dispose();
                    //}
                }
            }
            else 
            {
                SqlConnection connSql = new SqlConnection(strConn);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                    daSql.Fill(val);
                }
                catch (Exception ex)
                {
                    logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                    //throw ex;
                }
                finally
                {
                    //SqlConnection.ClearAllPools();
                    connSql.Close();
                    //connSql.Dispose();
                }
            
            }
            return val;
        }
        public static string strConnPtracker = System.Configuration.ConfigurationManager.AppSettings["ConnectionStringPtracker"];
        public static DataTable SelectTablePtracker(string strSQL)
        {
            DataTable val = new DataTable();
            if (ConfigurationManager.AppSettings["poolingsql"] != "0")
            {

                try
                {
                    using (SqlConnection connSql = new SqlConnection(strConnPtracker))
                    {
                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                        daSql.Fill(val);
                    }
                }
                catch (Exception ex)
                {
                    logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                    //throw ex;
                }
            }
            else
            {
                SqlConnection connSql = new SqlConnection(strConnPtracker);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                    daSql.Fill(val);
                }
                catch (Exception ex)
                {
                    logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                    //throw ex;
                }
                finally
                {
                    //SqlConnection.ClearAllPools();
                    connSql.Close();
                    connSql.Dispose();
                }
            }
            return val;
        }
        public static DataTable SelectTableErp(string strSQL)
        {
            DataTable val = new DataTable();
            GetConnectionErp();
            //////return SelectTableOracle(strSQL);
            if (ConfigurationManager.AppSettings["poolingsql"] != "0")
            {
                using (SqlConnection connSql = new SqlConnection(strConnErp))
                {
                    //SqlConnection connSql = new SqlConnection(strConn);
                    try
                    {

                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        cmdSql.CommandTimeout = 1200 * 5;
                        SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                        daSql.Fill(val);
                    }
                    catch (Exception ex)
                    {
                        logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                        //throw ex;
                    }
                    finally
                    {
                        connSql.Dispose();
                    }
                    //finally
                    //{
                    //    connSql.Close();
                    //    connSql.Dispose();
                    //}
                }
            }
            else
            {
                SqlConnection connSql = new SqlConnection(strConnErp);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                    daSql.Fill(val);
                }
                catch (Exception ex)
                {
                    logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                    //throw ex;
                }
                finally
                {
                    //SqlConnection.ClearAllPools();
                    connSql.Close();
                    //connSql.Dispose();
                }

            }
            return val;
        }    
        public static DataTable SelectTable(string strSQL, SqlParameter param)
        {
            DataTable val = new DataTable();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                cmdSql.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
                daSql.Fill(val);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static DataTable SelectTable(string strSQL, SqlParameter[] param)
        {
            DataTable val = new DataTable();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                for (int i = 0; i < param.Length; i++)
                    cmdSql.Parameters.Add(new SqlParameter(param[i].ParameterName, param[i].Value));
                SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                daSql.Fill(val);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        #endregion
        #region Load page
        public static DataTable LoadPage(string strSQL, int startRecord, int page_size)
        {
            //return LoadPageOracle(strSQL, startRecord, page_size);
            DataSet ds = new DataSet();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                cmdSql.CommandTimeout = 1200 * 5;
                SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                daSql.Fill(ds, startRecord, page_size, "result");
                cmdSql.Dispose();
            }
            catch (Exception ex)
            {
               // throw ex;
            }
            finally
            {
                connSql.Close();
                connSql.Dispose();
            }
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            return new DataTable();
        }
        public static DataTable LoadPageLog(string strSQL, int startRecord, int page_size)
        {
            //return LoadPageOracle(strSQL, startRecord, page_size);
            DataSet ds = new DataSet();
            strConnLog= System.Configuration.ConfigurationManager.AppSettings["ConnectionStringLog"];

            SqlConnection connSql = new SqlConnection(strConnLog);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                cmdSql.CommandTimeout = 1200 * 5;
                SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                daSql.Fill(ds, startRecord, page_size, "result");
                cmdSql.Dispose();
            }
            catch (Exception ex)
            {
                // throw ex;
            }
            finally
            {
                connSql.Close();
                connSql.Dispose();
            }
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            return new DataTable();
        }
        public static DataTable LoadPage(string strSQL, int startRecord, int page_size, SqlParameter param)
        {
            DataSet ds = new DataSet();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                cmdSql.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
                daSql.Fill(ds, startRecord, page_size, "result");
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return ds.Tables[0];
        }

        public static DataTable LoadPage(string strSQL, int startRecord, int page_size, SqlParameter[] param)
        {
            DataSet ds = new DataSet();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                for (int i = 0; i < param.Length; i++)
                    cmdSql.Parameters.Add(new SqlParameter(param[i].ParameterName, param[i].Value));
                SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                daSql.Fill(ds, startRecord, page_size, "result");
            }
            catch (Exception ex)
            {
               // throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return ds.Tables[0];
        }
        #endregion

        #region stored procedure
        public static DataTable SelectTableSP(string storedProcedure)
        {
            DataTable val = new DataTable();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(val);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static DataTable SelectTableSP(string storedProcedure, SqlParameter param)
        {
            DataTable val = new DataTable();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedure;
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(val);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static DataTable SelectTableSP(string storedProcedure, SqlParameter[] param)
        {
            DataTable val = new DataTable();
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedure;
                for (int i = 0; i < param.Length; i++)
                    cmd.Parameters.Add(param[i]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(val);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }

        public static int ExecuteSP(string storedProcedure)
        {
            int val = 0;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedure;
                val = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static int ExecuteSP(string storedProcedure, SqlParameter param)
        {
            int val = 0;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedure;
                cmd.Parameters.Add(param);
                val = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        public static int ExecuteSP(string storedProcedure, SqlParameter[] param)
        {
            int val = 0;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            try
            {
                connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedure;
                for (int i = 0; i < param.Length; i++)
                    cmd.Parameters.Add(param[i]);
                val = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                connSql.Close();
            }
            return val;
        }
        #endregion

        public static bool ExecuteTransation(string[] sqlstr)
        {
            bool bRet = true;
            GetConnection();
            SqlConnection connSql = new SqlConnection(strConn);
            SqlTransaction trans;
            connSql.Open();
            trans = connSql.BeginTransaction();
            try
            {
                for (int i = 0; i < sqlstr.Length; i++)
                {
                    SqlCommand cmd = new SqlCommand(sqlstr[i], connSql, trans);
                    cmd.ExecuteNonQuery();
                }
                trans.Commit();

            }
            catch
            {
                trans.Rollback();
                bRet = false;
            }
            finally
            {

                connSql.Close();
            }
            return bRet;
        }
        public static DataTable SelectTable(string strSQL, string sData)
        {
            if (sData.IndexOf("ptrackererp") != -1) return SelectTableErp(strSQL);
            return SelectTable(strSQL);
        }
        public static DataTable SelectTable(string strSQL, string iddevice,string sData)
        {
            return SelectTable(strSQL);
        }
        public static int Execute(string strSQL, string sData,string iddevice)
        {
            return Execute(strSQL);
        }
        public static bool ExecuteTransationErp(string[] sqlstr)
        {
            bool bRet = true;
            GetConnectionErp();
            SqlConnection connSql = new SqlConnection(strConnErp);
            SqlTransaction trans;
            connSql.Open();
            trans = connSql.BeginTransaction();
            try
            {
                for (int i = 0; i < sqlstr.Length; i++)
                {
                    SqlCommand cmd = new SqlCommand(sqlstr[i], connSql, trans);
                    cmd.ExecuteNonQuery();
                }
                trans.Commit();

            }
            catch
            {
                trans.Rollback();
                bRet = false;
            }
            finally
            {

                connSql.Close();
            }
            return bRet;
        }
        public static bool ExecuteTransationLog(string[] sqlstr)
        {
            bool bRet = true;
            strConnLog = ConfigurationManager.AppSettings["ConnectionStringLog"];
            SqlConnection connSql = new SqlConnection(strConnLog);
            SqlTransaction trans;
            connSql.Open();
            trans = connSql.BeginTransaction();
            try
            {
                for (int i = 0; i < sqlstr.Length; i++)
                {
                    SqlCommand cmd = new SqlCommand(sqlstr[i], connSql, trans);
                    cmd.ExecuteNonQuery();
                }
                trans.Commit();

            }
            catch
            {
                trans.Rollback();
                bRet = false;
            }
            finally
            {

                connSql.Close();
            }
            return bRet;
        }
        public static string strConnLog = "";
        public static int ExecuteLog(string strSQL)
        {
            //return Execute(strSQL);
            int val = 0;
            strConnLog = ConfigurationManager.AppSettings["ConnectionStringLog"];
            if (ConfigurationManager.AppSettings["poolingsqllog"] != "0")
            {
                try
                {

                    using (SqlConnection connSql = new SqlConnection(strConnLog))
                    {
                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        cmdSql.CommandTimeout = 5 * 60000;
                        val = cmdSql.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    //System.Data.SqlClient.SqlException: Transaction (Process ID 59) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction
                    logItem("Error Select Sql And Add To Queue In common.DLL:" + strSQL + "===>" + ex.ToString());
                    AddToLogToNeedProcessLog(ex.ToString(), strSQL);
                }
            }
            else
            {
                SqlConnection connSql = new SqlConnection(strConnLog);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    cmdSql.CommandTimeout = 5 * 60000;
                    val = cmdSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //System.Data.SqlClient.SqlException: Transaction (Process ID 59) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction
                    logItem("Error Select Sql And Add To Queue In common.DLL:" + strSQL + "===>" + ex.ToString());
                    AddToLogToNeedProcessLog(ex.ToString(), strSQL);
                }
                finally
                {
                    // SqlConnection.ClearAllPools();
                    connSql.Close();
                    connSql.Dispose();
                }
            }
            return val;
        }
        public static DataTable SelectTableLog(string strSQL)
        {
            DataTable val = new DataTable();
            strConnLog = System.Configuration.ConfigurationManager.AppSettings["ConnectionStringLog"];
            if (ConfigurationManager.AppSettings["poolingsqllog"] != "0")
            {

                try
                {
                    using (SqlConnection connSql = new SqlConnection(strConnLog))
                    {
                        connSql.Open();
                        SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                        SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                        daSql.Fill(val);
                    }
                }
                catch (Exception ex)
                {
                    logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                    //throw ex;
                }
            }
            else
            {
                SqlConnection connSql = new SqlConnection(strConnLog);
                try
                {
                    connSql.Open();
                    SqlCommand cmdSql = new SqlCommand(strSQL, connSql);
                    SqlDataAdapter daSql = new SqlDataAdapter(cmdSql);
                    daSql.Fill(val);
                }
                catch (Exception ex)
                {
                    logItem("Error Select Sql:" + strSQL + "===>" + ex.ToString());
                    //throw ex;
                }
                finally
                {
                    //SqlConnection.ClearAllPools();
                    connSql.Close();
                    connSql.Dispose();
                }
            }
            return val;
        }
        public static void AddToLogToNeedProcessLog(string ex, string sSql)
        {
            if (ex.ToLower().IndexOf("was deadlocked on lock | communication buffer resources") != -1)
            {
                logItemQueueLog(sSql);
            }
        }
        public static void logItemQueueLog(string sSql)
        {
            if (sSql.Split(';').Length >= 5) return;//skip recursive
            string sDirFailOver = @"F:\website\ptrackerall\log\failoverlog\";
            if (ConfigurationManager.AppSettings["dirfailoverlog"] != null)
            {
                sDirFailOver = ConfigurationManager.AppSettings["dirfailoverlog"];
            }
            try
            {
                if (!Directory.Exists(sDirFailOver)) Directory.CreateDirectory(sDirFailOver);
                using (StreamWriter w = new StreamWriter(sDirFailOver + "\\ASqlErrorToProcess.txt", true))
                {
                    w.WriteLine(sSql);
                }
            }
            catch { }
        }

    }
}