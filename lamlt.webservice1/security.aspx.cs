﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace lamlt.webservice1
{
    public partial class security : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sQuery=Request.QueryString.ToString();
            Boolean isValid = true;
            isValid = !string.IsNullOrEmpty(Request.QueryString["userid"]);

            if (!isValid)
            {
                Response.Status = "403 Forbidden";
            }
            else
            {
                Response.AddHeader("Content-Type", "binary/octet-stream");
                Response.AddHeader("Pragma", "nocache");

                String keyStr = "DE51A7254739C0EDF1DCE13BBB308FF0";

                int len = keyStr.Length / 2;
                byte[] keyBuffer = new byte[len];

                for (int i = 0; i < len; i++)
                    keyBuffer[i] = Convert.ToByte(keyStr.Substring(i * 2, 2), 16);

                Response.BinaryWrite(keyBuffer);
                Response.Flush();
                Response.End();
            }
        }
    }
}