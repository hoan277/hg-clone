﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Text;
namespace lamlt.webservice1
{
    /// <summary>
    /// Summary description for subcribe
    /// </summary>
    [WebService(Namespace = "urn:saigonmusic")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class subcribe : System.Web.Services.WebService
    {

        [WebMethod]
        //hongancp,Hongancp!@#45,LALATV_NGAY,975293382,20190814213654,20190814213654,REAL,0,#FULLSM,20190815000000,
        //hongancp,Hongancp!@#45,LALATV_NGAY,975293382,20190828090519,20190828090519,REAL,3000,#FULLSM,20190829000000,
        //hongancp,Hongancp!@#45,LALATV_NGAY,867728907,20190820094208,20190820094208,REAL,0,#FULLSM,,#CP_REQUEST_ID
        //hongancp,Hongancp!@#45,LALATV_NGAY,369196719,20191112155109,0,REAL,0,Dka,20191113000000,
        //username,password,serviceid, msisdn,chargetime,  @params,  mode,  amount,  command,  nextrenew,  cp_request_id

        public string SubRequest(string username, string password, string serviceid, string msisdn, string chargetime, string @params, string mode, int amount, string command, string nextrenew, string cp_request_id)
        {
            return lalatv.dbmanager.LalatvUtil.SubRequest( username,  password,  serviceid,  msisdn,  chargetime,  @params,  mode,  amount,  command,  nextrenew,  cp_request_id);
            LogUtil.logItem("subcribe.txt", new string[] { "====>", username,  password,  serviceid,  msisdn,  chargetime,  @params,  mode,  amount.ToString(),  command,  nextrenew,  cp_request_id });
            string s = "0";
            string msg = "Dang ky dich vu lalatv thanh cong,truy cap http://lalatv.com.vn de download va xem video";
            try
            {
                if (!(username == "hongancp" && password == "Hongancp!@#45"))
                {
                    return "301|Sai ten dang nhap hoac mat khau";
                }
                if (string.IsNullOrEmpty(serviceid) ||
                    string.IsNullOrEmpty(msisdn) || string.IsNullOrEmpty(chargetime) ||
                     string.IsNullOrEmpty(@params) || string.IsNullOrEmpty(mode) ||
                      string.IsNullOrEmpty(command) || string.IsNullOrEmpty(nextrenew)
                    )
                {
                    return "300|Sai tham so";
                }
                using (StreamWriter w = new StreamWriter(LogUtil.dirLog+"subcribe.txt", true, Encoding.UTF8))
                {
                    string sLine = DateTime.Now.ToString("yyyyMMddHHmmss:") +
                         username + "," + password + "," + serviceid + "," + msisdn + "," + chargetime + "," + @params + "," + mode + "," + amount.ToString() + "," + command + "," + nextrenew + "," + cp_request_id;
                    w.WriteLine(sLine);
                    string subtype = "0";
                    string substate = "1";
                    if (serviceid == "LALATV_NGAY") subtype = "1";
                    if (serviceid == "LALATV_TUAN") subtype = "2";
                    if (serviceid == "LALATV_THANG") subtype = "3";
                    if(amount==0) subtype = "0";                   
                    lalatv.dbmanager.LalatvUtil.update_create_user(msisdn,subtype,substate, sLine);
                    lalatv.dbmanager.LalatvUtil.call_to_url_api(msisdn, subtype, substate, sLine);
                }
                
            }
            catch(Exception ex)
            {
                LogUtil.logItem("subcribe.txt", new string[] { "Exception:"+ex.ToString() });
                s = "302";
                msg = "Server lalatv dang ban, xin vui long thu lai sau";
            }
            string result = s + "|" + msg;
            LogUtil.logItem("subcribe.txt","==============================>" +result);
            return result;
        }
    }
}
