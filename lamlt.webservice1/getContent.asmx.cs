﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Text;

namespace lamlt.webservice1
{
    /// <summary>
    /// Summary description for getContent
    /// </summary>
    [WebService(Namespace = "urn:saigonmusic")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class getContent : System.Web.Services.WebService
    {

        [WebMethod]
        public string contentRequest(string username, string password, string serviceid, string msisdn, string @params, int amount, string command)
        {
            LogUtil.logItem("getContent.txt", new string[] { "=====>", username,  password,  serviceid,  msisdn,  @params,  amount.ToString(), command });
            string s = "0";
            string msg = "Dang ky dich vu lalatv thanh cong,truy cap http://lalatv.com.vn de download va xem video";
            string samount = amount.ToString();
            string scommand = command.ToString();

            try
            {
                if (!(username == "hongancp" && password == "Hongancp!@#45"))
                {
                    return "301|Sai ten dang nhap hoac mat khau" + "|" + samount + "|" + scommand;
                }
                if (string.IsNullOrEmpty(serviceid) ||
                    string.IsNullOrEmpty(msisdn) || string.IsNullOrEmpty(@params)
                    )
                {
                    return "300|Sai tham so" + "|" + samount + "|" + scommand;
                }
                
                string sLine = DateTime.Now.ToString("yyyyMMddHHmmss:") +
                        username + "," + password + "," + serviceid + "," + msisdn + "," + @params + "," + amount.ToString() + "," + command;
                LogUtil.logItem("getContent.txt", sLine);
                msg = getMsgToReturn(msg, serviceid, msisdn, command);
            }
            catch (Exception ex)
            {
                LogUtil.logItem("getContent.txt", new string[] { "Exception:" + ex.ToString() });

                s = "302";
                msg = "Server dang ban, xin vui long thu lai sau";
            }
            Console.WriteLine("Test Method Executed!");
            string result = s + "|" + msg + "|" + samount + "|" + scommand;
            LogUtil.logItem("getContent.txt","============>"+ result);
            return result;            
        }

        private static string getMsgToReturn(string msg, string serviceid, string msisdn, string command)
        {
            command = command.ToLower();
            if (command != "kta")
            {
                return msg;
            }
            string sub_stype = lalatv.dbmanager.LalatvUtil.getStateUser(msisdn);
            string sub_title = "";
            if (sub_stype == "1") sub_title = "NGAY";
            if (sub_stype == "2") sub_title = "TUAN";
            if (sub_stype == "3") sub_title = "THANG";

            if (sub_stype != "0")
            {
                msg = "Quy khach dang su dung goi " + sub_title + " DV Data LalaTV. Truy cap ung dung LalaTV de xem KHONG GIOI HAN LUU LUONG DATA cac noi dung giao duc, giai tri hap dan danh cho be yeu tren ung dung LalaTV. Chi tiet soan HDA gui 5282 hoac LH 198 (mien phi).";
            }
            else
            {
                msg = "Quy khach chua dang ky DV Data LalaTV. De dang ky, soan DKA gui 5282 (3.000d/ngay) hoac DKA7 gui 5282 (10.000d/tuan) hoac DKA30 gui 5282 (30.000d/thang). Chi tiet soan HDA gui 5282 hoac LH 198 (mien phi).";
            }
            return msg;
        }
    }
}
