﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace lamlt.webservice1
{
    public class LogUtil
    {
        public static string dirLog = ConfigurationManager.AppSettings["dirLog"];
        public static void logItem(string file, string[] arr)
        {
            //dirLog = ConfigurationManager.AppSettings["dirLog"];
            if (string.IsNullOrEmpty(dirLog)) dirLog = "C://lalatv/log/";
            if (!Directory.Exists(dirLog)) Directory.CreateDirectory(dirLog);
            string sContent = "";
            foreach (string s in arr)
            {
                sContent = sContent + "," + s;
            }
            using (StreamWriter w = new StreamWriter(dirLog+file, true, Encoding.UTF8))
            {
                string sLine = DateTime.Now.ToString("yyyyMMddHHmmss:") + sContent;
                w.WriteLine(sLine);
            }
        }
        public static void logItem(string file, string sContent)
        {
            //dirLog = ConfigurationManager.AppSettings["dirLog"];
            if (string.IsNullOrEmpty(dirLog)) dirLog = "C://lalatv/log/";
            if (!Directory.Exists(dirLog)) Directory.CreateDirectory(dirLog);            
            using (StreamWriter w = new StreamWriter(dirLog + file, true, Encoding.UTF8))
            {
                string sLine = DateTime.Now.ToString("yyyyMMddHHmmss:") + sContent;
                w.WriteLine(sLine);
            }
        }
    }
}