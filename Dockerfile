#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 11000

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["lamlt.web/lamlt.web.csproj", "lamlt.web/"]
COPY ["lamlt1.data/lamlt.data.csproj", "lamlt1.data/"]
RUN dotnet restore "lamlt.web/lamlt.web.csproj"
COPY . .
WORKDIR "/src/lamlt.web"
RUN dotnet build "lamlt.web.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "lamlt.web.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "lamlt.web.dll"]