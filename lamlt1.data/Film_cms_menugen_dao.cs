﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_cms_menu")]
    public partial class film_cms_menu : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string desc { get; set; }
        public string url { get; set; }
        public int? position { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
        public int? parent_id { get; set; }
        public string str_menu_id { get; set; }
        public string menu_icon { get; set; }
    }

    [Alias("vw_film_cms_menu")]
    public partial class vw_film_cms_menu
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string url { get; set; }
        public int? position { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
        public int? parent_id { get; set; }
        public string str_menu_id { get; set; }
        public string menu_icon { get; set; }
    }
}