﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_video_view")]
    public partial class film_video_view : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public int? videoid { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
        public int? view { get; set; }
        public int? custom_duration { get; set; }
        public int? time_start { get; set; }
        public int? time_end { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }

    }
    [Alias("vw_film_video_view")]
    public partial class vw_film_video_view
    {
        public int Id { get; set; }
        public int? videoid { get; set; }
        public int? userid { get; set; }
        public int? time_start { get; set; }
        public int? time_end { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? custom_duration { get; set; }
        public int? view { get; set; }
        public string film_title { get; set; }
        public int? cpid { get; set; }
        public string cp_title { get; set; }
    }
    [Alias("vw_report")]
    public partial class vw_report
    {
        public int Id { get; set; }
        public int? videoid { get; set; }
        public string film_title { get; set; }
        public DateTime? dateview { get; set; }
        public int? cpid { get; set; }
    }

    //extendtion lượt view theo video lẻ
    public partial class view_video
    {
        public vw_film_video film { get; set; }
        public int view_all { get; set; }
        public int view_day { get; set; }
        public int view_week { get; set; }
        public int view_month { get; set; }
    }
}
