﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("user_link")]
    public partial class user_link : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public int? userid { get; set; }
        public string name { get; set; }
        public int typekid { get; set; }
        public DateTime? datecreated { get; set; }

    }
    [Alias("vw_user_link")]
    public partial class vw_user_link
    {
        public int Id { get; set; }
        public int? userid { get; set; }
        public string name { get; set; }
        public int typekid { get; set; }
        public DateTime? datecreated { get; set; }

    }
}