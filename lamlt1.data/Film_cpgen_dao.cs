﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_cp")]
    public partial class film_cp : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string shortname { get; set; }
        public string address { get; set; }
        public string rep { get; set; }
        public string email { get; set; }
        public string contractname { get; set; }
        public string contractphone { get; set; }
        public string contractemail { get; set; }
        public string desc { get; set; }
        public string scan { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
    }

    [Alias("vw_film_cp")]
    public partial class vw_film_cp
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string shortname { get; set; }
        public string address { get; set; }
        public string rep { get; set; }
        public string email { get; set; }
        public string contractname { get; set; }
        public string contractphone { get; set; }
        public string contractemail { get; set; }
        public string desc { get; set; }
        public string scan { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
    }
}