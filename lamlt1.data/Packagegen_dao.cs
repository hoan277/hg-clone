﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{

    [Alias("package")]
    public partial class package : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int price { get; set; }
        public string note { get; set; }
        public int typepayment { get; set; }
        public DateTime date_created { get; set; }
        public int provider_id { get; set; }
        public string code { get; set; }

    }
    [Alias("vw_package")]
    public partial class vw_package
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int price { get; set; }
        public string note { get; set; }
        public int typepayment { get; set; }
        public DateTime date_created { get; set; }
        public int provider_id { get; set; }
        public string provider_name { get; set; }
        public string code { get; set; }
    }

}
