﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("denoiser")]
    public partial class denoiser : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string mp4 { get; set; }
        public string step { get; set; }
        public string result { get; set; }
        public DateTime? created { get; set; }
        public DateTime? updated { get; set; }
        public string status { get; set; }

    }
    [Alias("vw_denoiser")]
    public partial class vw_denoiser
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string mp4 { get; set; }
        public string step { get; set; }
        public string result { get; set; }
        public DateTime? created { get; set; }
        public DateTime? updated { get; set; }
        public string status { get; set; }
    }
}