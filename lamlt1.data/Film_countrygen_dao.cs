﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_country")]
    public partial class film_country : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string desc { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
    }

    [Alias("vw_film_country")]
    public partial class vw_film_country
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
    }
}