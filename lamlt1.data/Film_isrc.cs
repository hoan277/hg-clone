﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;

namespace lamlt.data
{
    [Alias("film_isrc")]
    public partial class film_isrc : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int product_id { get; set; }
        public int current_code { get; set; }
    }
}