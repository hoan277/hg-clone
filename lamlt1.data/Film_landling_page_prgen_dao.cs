﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_landing_page_pr")]
    public partial class film_landing_page_pr : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? guidid { get; set; }
        public string source { get; set; }
        public string ip { get; set; }
        public DateTime? datecreated { get; set; }
    }

    [Alias("vw_film_landing_page_pr")]
    public partial class vw_film_landing_page_pr
    {
        [Required]
        public int Id { get; set; }

        public int? guidid { get; set; }
        public string source { get; set; }
        public string ip { get; set; }
        public DateTime? datecreated { get; set; }
    }
}