﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_related")]
    public partial class film_related : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public int? filmid { get; set; }
        public int? film_related_id { get; set; }
        public DateTime? datecreated { get; set; }


    }
    [Alias("vw_film_related")]
    public partial class vw_film_related
    {
        public int Id { get; set; }
        public int? filmid { get; set; }
        public int? film_related_id { get; set; }
        public DateTime? datecreated { get; set; }
        public string filmid_title { get; set; }

    }
}