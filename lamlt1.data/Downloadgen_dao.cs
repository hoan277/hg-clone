﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("download")]
    public partial class download : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string cmd { get; set; }
        public string status { get; set; }
        public DateTime? datecreated { get; set; }
    }

    [Alias("vw_download")]
    public partial class vw_download
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string cmd { get; set; }
        public string status { get; set; }
        public DateTime? datecreated { get; set; }
    }
}