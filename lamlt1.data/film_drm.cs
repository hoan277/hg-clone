﻿using ServiceStack.DataAnnotations;
using System;

namespace lamlt.data
{
    [Alias("film_drm")]
    public class film_drm
    {
        [Alias("id")]
        [AutoIncrement]
        public int id { get; set; }

        public string name { get; set; }
        public int userid { get; set; }
        public string key { get; set; }
        public string metadata { get; set; }
        public DateTime datecreate { get; set; }
        public DateTime datesubcribe { get; set; }
        public string note { get; set; }
    }

    [Alias("vw_film_drm")]
    public partial class vw_film_drm
    {
        public int id { get; set; }
        public string name { get; set; }
        public int userid { get; set; }
        public string key { get; set; }
        public string metadata { get; set; }
        public DateTime datecreate { get; set; }
        public DateTime datesubcribe { get; set; }
        public string note { get; set; }
    }
}