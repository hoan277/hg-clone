﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    #region ===== film_video_user_log ======
    [Alias("film_video_user_log")]
    public partial class film_video_user_log : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public int? user_id { get; set; }
        public int? film_id { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public int? status { get; set; }
        public int? link_userid { get; set; }

    }
    #endregion ===== film_video_user_log ======

    #region ===== vw_film_video_user_log ===== 
    [Alias("vw_film_video_user_log")]
    public partial class vw_film_video_user_log
    {
        public int Id { get; set; }
        public int? user_id { get; set; }
        public int? film_id { get; set; }
        public string film_title { get; set; }
        public string upload_file { get; set; }
        public string thumb_file { get; set; }
        public string code { get; set; }
        public int? film_view { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public int? status { get; set; }
        public int? link_userid { get; set; }
    }
    #endregion ===== vw_film_video_user_log ===== 
}
