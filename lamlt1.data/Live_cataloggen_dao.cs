﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("live_catalog")]
    public partial class live_catalog : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public int? quantity { get; set; }
        public string code { get; set; }
        public int? catalogid { get; set; }
        public int? position { get; set; }
        public string desc { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public int? status { get; set; }
        public int? user_id { get; set; }

    }
    [Alias("vw_live_catalog")]
    public partial class vw_live_catalog
    {
        public int Id { get; set; }
        public string title { get; set; }
        public int? quantity { get; set; }
        public string code { get; set; }
        public int? catalogid { get; set; }
        public int? position { get; set; }
        public string desc { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public int? status { get; set; }
        public int? user_id { get; set; }
        public string username { get; set; }
    }
}