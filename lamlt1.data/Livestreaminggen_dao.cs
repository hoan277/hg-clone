﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("livestreaming")]
    public partial class livestreaming : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? processid { get; set; }
        public string cmd { get; set; }
        public string video { get; set; }
        public string status { get; set; }
        public string note { get; set; }
        public string resolution { get; set; }
        public string type { get; set; }
        public int userid { get; set; }
        public string title { get; set; }
        public DateTime datecreated { get; set; }
        public DateTime? starttime { get; set; }
        public DateTime? stoptime { get; set; }
        public int live_catalog_id { get; set; }
    }

    [Alias("vw_livestreaming")]
    public partial class vw_livestreaming
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? processid { get; set; }
        public string cmd { get; set; }
        public string video { get; set; }
        public string status { get; set; }
        public string note { get; set; }
        public string resolution { get; set; }
        public string type { get; set; }
        public int userid { get; set; }
        public string title { get; set; }
        public string username { get; set; }
        public DateTime datecreated { get; set; }
        public DateTime? starttime { get; set; }
        public DateTime? stoptime { get; set; }
        public int live_catalog_id { get; set; }
        public string live_catalog_title { get; set; }
    }
}