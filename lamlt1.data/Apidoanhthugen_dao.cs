﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("mvm_user_subscription_logs")]
    public partial class mvm_user_subscription_logs : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? user_id { get; set; }
        public string user_mobile { get; set; }
        public int? type { get; set; }
        public int? sub_type { get; set; }
        public int? sub_case { get; set; }
        public int? price { get; set; }
        public string channel { get; set; }
        public string application { get; set; }
        public string username { get; set; }
        public string user_ip { get; set; }
        public string note { get; set; }
        public int? state { get; set; }
        public int? charging_result { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int? job_state { get; set; }
        public int? cp { get; set; }
    }

    [Alias("vascloud_cdr")]
    public partial class vascloud_cdr
    {
        public DateTime action_date { get; set; }
        public string package_code { get; set; }
        public string msisdn { get; set; }
        public string channel { get; set; }
        public string reason { get; set; }
        public string price { get; set; }
        public string original_price { get; set; }
    }

    [Alias("vw_mvm_user_subscription_logs")]
    public partial class vw_mvm_user_subscription_logs
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? user_id { get; set; }
        public string user_mobile { get; set; }
        public int? type { get; set; }
        public int? sub_type { get; set; }
        public int? sub_case { get; set; }
        public int? price { get; set; }
        public string channel { get; set; }
        public string application { get; set; }
        public string username { get; set; }
        public string user_ip { get; set; }
        public string note { get; set; }
        public int? state { get; set; }
        public int? charging_result { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int? job_state { get; set; }
        public int? cp { get; set; }
    }

    [Alias("mvm_bao_cao_doanhthu_day")]
    public partial class mvm_bao_cao_doanhthu_day
    {
        [AutoIncrement]
        public int Id { get; set; }

        public DateTime? datecreated { get; set; }

        // tổng thuê bao active
        public int? countphoneactive { get; set; }

        //tổng thuê bao đăng ký type =1
        public int? countphone { get; set; }

        // tổng thuê bao hủy type = 2
        public int? countCancel { get; set; }

        //"Tổng thuê bao gia hạn type = 3
        public int? giahan { get; set; }

        // tổng thuê bao đk thành công state = 1
        public int? countSucceed { get; set; }

        // tổng thuê bao dk ko thành công state != 1
        public int? countfailed { get; set; }

        //ti le gia han thanh cong chinh bang tong so gia han hôm nay: số thuê bao hôm trước
        public int? TLGHTC { get; set; }

        //"Tổng doanhthu price * so thue bao dang ky thanh cong theo tung goi
        public int totaldoanhthu { get; set; }
    }
}