﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_provider")]
    public partial class film_provider : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string code { get; set; }
        public DateTime datecreated { get; set; }

    }
    [Alias("vw_film_provider")]
    public partial class vw_film_provider
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string code { get; set; }
        public DateTime datecreated { get; set; }
    }
}