﻿using ServiceStack.DataAnnotations;

namespace lamlt.data
{

    [Alias("vw_report_view")]
    public partial class vw_report_view
    {
        public int video_id { get; set; }
        public string film_title { get; set; }
        public int? quatity_view { get; set; }
        public int? cp_id { get; set; }
        [Ignore]

        public int package_Id { get; set; }
        [Ignore]
        public string package_title { get; set; }
        [Ignore]
        public string revenue { get; set; }
    }
}
