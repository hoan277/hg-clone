﻿
using ServiceStack.DataAnnotations;
using ServiceStack.Model;

namespace lamlt.data
{
    [Alias("tradding")]
    public partial class tradding : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string name { get; set; }
        public string revenue { get; set; }
        public string discount { get; set; }

    }

    [Alias("vw_tradding")]
    public partial class vw_tradding
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string revenue { get; set; }
        public string discount { get; set; }

    }
}