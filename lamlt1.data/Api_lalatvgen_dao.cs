﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("api_lalatv")]
    public partial class api_lalatv : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string desc { get; set; }
        public string link { get; set; }
        public string method { get; set; }
        public string param { get; set; }
        public string param_type { get; set; }
        public string result { get; set; }
        public string note { get; set; }
        public int? status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? isview { get; set; }
    }

    [Alias("vw_api_lalatv")]
    public partial class vw_api_lalatv
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string link { get; set; }
        public string method { get; set; }
        public string param { get; set; }
        public string param_type { get; set; }
        public string result { get; set; }
        public string note { get; set; }
        public int? status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? isview { get; set; }
    }
}