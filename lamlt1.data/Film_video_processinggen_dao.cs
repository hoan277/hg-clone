﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_video_processing")]
    public partial class film_video_processing : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string desc { get; set; }
        public int? duration { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public DateTime? dateupdate1 { get; set; }
        public DateTime? dateupdate2 { get; set; }
        public DateTime? dateupdate3 { get; set; }
        public DateTime? dateupdate4 { get; set; }
        public DateTime? dateupdate5 { get; set; }
        public DateTime? dateupdate6 { get; set; }
        public int? userid { get; set; }
        public int? seriid { get; set; }
        public string imdb { get; set; }
        public int? format { get; set; }
        public int? sub_type { get; set; }
        public int? publish_year { get; set; }
        public int? publish_countryid { get; set; }
        public int? product_id { get; set; }
        public string actor { get; set; }
        public string director { get; set; }
        public int? film_type { get; set; }
        public int? episode { get; set; }
        public int? episode_current { get; set; }
        public string contract_copyright { get; set; }
        public DateTime? contract_exprired { get; set; }
        public string contract_appendix { get; set; }
        public string copyright_appendix { get; set; }
        public DateTime? copyright_expired { get; set; }
        public int? catalog_id { get; set; }
        public string upload_file { get; set; }
        public string trainer { get; set; }
        public string thumb_file { get; set; }
        public int? exclusive { get; set; }
        public float? price { get; set; }
        public string status { get; set; }
        public int? cpid { get; set; }
        public string code { get; set; }
        public string tags { get; set; }
        public int? banner_app { get; set; }
        public string showhome { get; set; }
        public int? episode_id { get; set; }
        public string so_hd { get; set; }
        public string phuluc_hd { get; set; }
        public string giaykiemduyet { get; set; }
        public string giaychungnhan { get; set; }
        public string banquyen { get; set; }
        public DateTime? ngayky_hd { get; set; }
        public DateTime? ngayhethan_hd { get; set; }
        public string doitac { get; set; }
        public string file_hd { get; set; }
        public string language { get; set; }
        public string mv { get; set; }
        public string beat { get; set; }
        public string vocal { get; set; }
        public string master { get; set; }
        public string lyric { get; set; }
        public string contract_singer { get; set; }
        public string contract_author { get; set; }
        public string related_contract { get; set; }
        public string json { get; set; }
        public string srt { get; set; }
        public string log { get; set; }
        public string video_type { get; set; }
        public string denoiser_step { get; set; }
        public string denoiser_result { get; set; }
    }

    [Alias("vw_film_video_processing")]
    public partial class vw_film_video_processing
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public int? duration { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public DateTime? dateupdate1 { get; set; }
        public DateTime? dateupdate2 { get; set; }
        public DateTime? dateupdate3 { get; set; }
        public DateTime? dateupdate4 { get; set; }
        public DateTime? dateupdate5 { get; set; }
        public DateTime? dateupdate6 { get; set; }
        public int? userid { get; set; }
        public int? seriid { get; set; }
        public string imdb { get; set; }
        public int? format { get; set; }
        public int? sub_type { get; set; }
        public int? publish_year { get; set; }
        public int? publish_countryid { get; set; }
        public int? product_id { get; set; }
        public string actor { get; set; }
        public string director { get; set; }
        public int? film_type { get; set; }
        public int? episode { get; set; }
        public int? episode_current { get; set; }
        public string contract_copyright { get; set; }
        public DateTime? contract_exprired { get; set; }
        public string contract_appendix { get; set; }
        public string copyright_appendix { get; set; }
        public DateTime? copyright_expired { get; set; }
        public int? catalog_id { get; set; }
        public string upload_file { get; set; }
        public string trainer { get; set; }
        public string thumb_file { get; set; }
        public int? exclusive { get; set; }
        public float? price { get; set; }
        public string status { get; set; }
        public int? cpid { get; set; }
        public string code { get; set; }
        public string tags { get; set; }
        public int? banner_app { get; set; }
        public string showhome { get; set; }
        public int? episode_id { get; set; }
        public string so_hd { get; set; }
        public string phuluc_hd { get; set; }
        public string giaykiemduyet { get; set; }
        public string giaychungnhan { get; set; }
        public string banquyen { get; set; }
        public DateTime? ngayky_hd { get; set; }
        public DateTime? ngayhethan_hd { get; set; }
        public string doitac { get; set; }
        public string file_hd { get; set; }
        public string catalog_title { get; set; }
        public string language { get; set; }
        public string mv { get; set; }
        public string beat { get; set; }
        public string vocal { get; set; }
        public string master { get; set; }
        public string lyric { get; set; }
        public string contract_singer { get; set; }
        public string contract_author { get; set; }
        public string related_contract { get; set; }
        public string json { get; set; }
        public string srt { get; set; }
        public string log { get; set; }
        public string video_type { get; set; }
        public string denoiser_step { get; set; }
        public string denoiser_result { get; set; }
    }
}