﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_channel")]
    public partial class film_channel : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string description { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string ip { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
    }

    [Alias("vw_film_channel")]
    public partial class vw_film_channel
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string ip { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
    }
}