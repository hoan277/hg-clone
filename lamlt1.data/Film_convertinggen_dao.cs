﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_converting")]
    public partial class film_converting : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string path { get; set; }
        public DateTime? start_time { get; set; }
        public DateTime? date_time { get; set; }
        public int? status { get; set; }
        public int? duration { get; set; }
        public int? cmd_id { get; set; }
        public string cmd_title { get; set; }
        public int process_id { get; set; }
    }

    [Alias("vw_film_converting")]
    public partial class vw_film_converting
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string path { get; set; }
        public DateTime? start_time { get; set; }
        public DateTime? date_time { get; set; }
        public int? status { get; set; }
        public int? duration { get; set; }
        public int? cmd_id { get; set; }
        public string cmd_title { get; set; }
        public int process_id { get; set; }
    }
}