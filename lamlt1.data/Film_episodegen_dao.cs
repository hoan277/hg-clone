﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_episode")]
    public partial class film_episode : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string desc { get; set; }
        public int? catalog_id { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
    }

    [Alias("vw_film_episode")]
    public partial class vw_film_episode
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public int? catalog_id { get; set; }
        public string catalog_title { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
    }
}