﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("scene_change")]
    public partial class scene_change : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string upload_file { get; set; }
        public int? time_show { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }

    }
    [Alias("vw_scene_change")]
    public partial class vw_scene_change
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string upload_file { get; set; }
        public int? time_show { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }

    }
}