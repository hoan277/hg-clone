﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_cmd_converting")]
    public partial class film_cmd_converting : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string idconverting { get; set; }
        public DateTime? datecreated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
    }

    [Alias("vw_film_cmd_converting")]
    public partial class vw_film_cmd_converting
    {
        public int Id { get; set; }
        public string idconverting { get; set; }
        public DateTime? datecreated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
    }
}