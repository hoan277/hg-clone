﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;

namespace lamlt.data
{
    [Alias("detail")]
    public partial class detail : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? videoid { get; set; }
        public int? userid { get; set; }
        public int? modelid { get; set; }
        public int? count { get; set; }
    }

    [Alias("vw_detail")]
    public partial class vw_detail
    {
        public int Id { get; set; }
        public int? videoid { get; set; }
        public int? userid { get; set; }
        public int? modelid { get; set; }
        public int? count { get; set; }
    }
}