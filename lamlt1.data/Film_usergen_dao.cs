﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_users")]
    public partial class film_user : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string fullname { get; set; }
        public string desc { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
        public int? cpid { get; set; }
        public int? roleid { get; set; }
        public int? sub_type { get; set; }
        public int? sub_state { get; set; }
        public string token { get; set; }
        public int payment_type { get; set; }
        public int gender { get; set; }
        public DateTime birthday { get; set; }
        public DateTime? token_expired { get; set; }
        public int link_id { get; set; }
        public int is_required_paid { get; set; }
    }

    [Alias("vw_film_user")]
    public partial class vw_film_user
    {
        public int Id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string fullname { get; set; }
        public string desc { get; set; }
        public int sub_state { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
        public int? cpid { get; set; }
        public int? roleid { get; set; }
        public string cpid_title { get; set; }
        public string roleid_title { get; set; }
    }

    [Alias("film_users_log")]
    public partial class film_users_log : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? userid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string fullname { get; set; }
        public string desc { get; set; }

        // Đăng ký
        public DateTime? datecreated { get; set; }

        // Gia hạn
        public DateTime? date_gh { get; set; }

        // Hết hạn
        public DateTime? date_hh { get; set; }

        // Hủy bỏ
        public DateTime? date_hb { get; set; }

        public int? cpid { get; set; }
        public int? package_id { get; set; }
        public int? roleid { get; set; }
        public int? gender { get; set; }
        public DateTime? birthday { get; set; }
        public int? sub_type { get; set; }
        public int? sub_state { get; set; }
        public int? status { get; set; }
    }

    [Alias("vw_film_users_log")]
    public partial class vw_film_users_log
    {
        public int Id { get; set; }
        public int? userid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string fullname { get; set; }
        public string desc { get; set; }

        // Đăng ký
        public DateTime? datecreated { get; set; }

        // Gia hạn
        public DateTime? date_gh { get; set; }

        // Hết hạn
        public DateTime? date_hh { get; set; }

        // Hủy bỏ
        public DateTime? date_hb { get; set; }

        public int? cpid { get; set; }
        public int? package_id { get; set; }
        public int? roleid { get; set; }
        public int? gender { get; set; }
        public DateTime? birthday { get; set; }
        public int? sub_type { get; set; }
        public int? sub_state { get; set; }
        public int? status { get; set; }
    }
}