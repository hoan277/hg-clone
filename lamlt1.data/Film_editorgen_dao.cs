﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_editor")]
    public partial class film_editor : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string content { get; set; }
        public int? channel_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string thumbnail { get; set; }
        public string transcript { get; set; }
        public string tags { get; set; }
        public string privacy_status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
    }

    [Alias("vw_film_editor")]
    public partial class vw_film_editor
    {
        public int Id { get; set; }
        public string content { get; set; }
        public int? channel_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string thumbnail { get; set; }
        public string transcript { get; set; }
        public string tags { get; set; }
        public string privacy_status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public string channel_title { get; set; }
    }
}