﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_product")]
    public partial class film_product : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public int? channel_id { get; set; }
        public string description { get; set; }
        public string thumbnail { get; set; }
        public string transcript { get; set; }
        public string tags { get; set; }
        public string privacy_status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public string type { get; set; }
        public string upload_title { get; set; }
        public int? effect_id { get; set; }
        public int? scene_change_id { get; set; }
        public int? convert_status { get; set; }
        public string path { get; set; }
        public string code { get; set; }
        public int duration { get; set; }
        public int upload_ftp { get; set; }
    }

    [Alias("vw_film_product")]
    public partial class vw_film_product
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public int? channel_id { get; set; }
        public string description { get; set; }
        public string thumbnail { get; set; }
        public string transcript { get; set; }
        public string tags { get; set; }
        public string privacy_status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public string channel_title { get; set; }
        public string upload_title { get; set; }
        public int? effect_id { get; set; }
        public int? scene_change_id { get; set; }
        public string effect_title { get; set; }
        public string scene_change_title { get; set; }
        public int? convert_status { get; set; }
        public string path { get; set; }
        public string code { get; set; }
        public int duration { get; set; }
        public int upload_ftp { get; set; }
    }
    public class product_json
    {
        public string ftp_ip { get; set; }
        public string ftp_username { get; set; }
        public string ftp_password { get; set; }
        public string ftp_folder { get; set; }
        public string client_file { get; set; }
        public string file_name { get; set; }
    }
}