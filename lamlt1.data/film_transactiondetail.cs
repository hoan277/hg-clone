﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{

    [Alias("film_transactiondetail")]
    public partial class film_transactiondetail : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public int? transid { get; set; }
        public string address { get; set; }
        public double? amount1 { get; set; }
        public double? amount2 { get; set; }
        public string currency1 { get; set; }
        public string currency2 { get; set; }
        public string status_url { get; set; }
        public string qrcore_url { get; set; }
        public DateTime? datecreated { get; set; }
        public double? rateWS { get; set; }
        public int? statusWS { get; set; }
        public string hashWS { get; set; }
        public string idRef { get; set; }
        public string keyRef { get; set; }
        public string typeTrans { get; set; }
        public double? rateUSD1 { get; set; }
        public double? rateUSD2 { get; set; }
        public string addressWS { get; set; }
        public int? memberid { get; set; }
        public string txthash { get; set; }
        public int? statusWS1 { get; set; }
        public int? memberidTo { get; set; }
        public string txtHash1 { get; set; }
        public int? statusETH { get; set; }
        public string username { get; set; }
        public string note { get; set; }
        public string bankid { get; set; }
        public string statusBanking { get; set; }
        public string fullname { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string urlTemp { get; set; }
        public string statusBankingResult { get; set; }
        public string orderIdRef { get; set; }

    }
    [Alias("vw_film_transactiondetail")]
    public partial class vw_film_transactiondetail
    {
        public int Id { get; set; }
        public int? transid { get; set; }
        public string txtid_transactions { get; set; }
        public string address { get; set; }
        public double? amount1 { get; set; }
        public double? amount2 { get; set; }
        public string currency1 { get; set; }
        public string currency2 { get; set; }
        public string status_url { get; set; }
        public string qrcore_url { get; set; }
        public DateTime? datecreated { get; set; }
        public double? rateWS { get; set; }
        public int? statusWS { get; set; }
        public string hashWS { get; set; }
        public string idRef { get; set; }
        public string keyRef { get; set; }
        public string typeTrans { get; set; }
        public double? rateUSD1 { get; set; }
        public double? rateUSD2 { get; set; }
        public string addressWS { get; set; }
        public int? memberid { get; set; }
        public string txthash { get; set; }
        public int? statusWS1 { get; set; }
        public int? memberidTo { get; set; }
        public string txtHash1 { get; set; }
        public int? statusETH { get; set; }
        public string username { get; set; }
        public string note { get; set; }
        public string bankid { get; set; }
        public string statusBanking { get; set; }
        public string fullname { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string urlTemp { get; set; }
        public string statusBankingResult { get; set; }
        public string orderIdRef { get; set; }
    }

}
