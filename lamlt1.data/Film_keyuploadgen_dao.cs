﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_keyupload")]
    public partial class film_keyupload : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string description { get; set; }
        public string key_upload { get; set; }
        public int? channel_id { get; set; }
        public string note { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
    }

    [Alias("vw_film_keyupload")]
    public partial class vw_film_keyupload
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string key_upload { get; set; }
        public int? channel_id { get; set; }
        public string note { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
        public string channel_title { get; set; }
    }
}