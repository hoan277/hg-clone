﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{

    [Alias("model")]
    public partial class model : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string decrypt { get; set; }
        public DateTime? datecreated { get; set; }

    }
    [Alias("vw_model")]
    public partial class vw_model
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string decrypt { get; set; }
        public DateTime? datecreated { get; set; }
    }

}
