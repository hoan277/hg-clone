﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_catalog_film")]
    public partial class film_catalog_film : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? catalogid { get; set; }
        public int? filmid { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
    }

    [Alias("vw_film_catalog_film")]
    public partial class vw_film_catalog_film
    {
        public int Id { get; set; }
        public int? catalogid { get; set; }
        public int? filmid { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid { get; set; }
        public string catalogid_title { get; set; }
        public string filmid_title { get; set; }
    }
}