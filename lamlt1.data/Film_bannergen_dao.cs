﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_banner")]
    public partial class film_banner : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public int? position { get; set; }
        public int? prior { get; set; }
        public string file { get; set; }
        public string html { get; set; }
        public string desc { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? typeid { get; set; }
        public int? type_client { get; set; }
        public int? film_id { get; set; }
        public int? show_home { get; set; }
        public int? display { get; set; }
    }

    [Alias("vw_film_banner")]
    public partial class vw_film_banner
    {
        public int Id { get; set; }
        public string title { get; set; }
        public int? position { get; set; }
        public int? prior { get; set; }
        public string file { get; set; }
        public string html { get; set; }
        public string desc { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int film_catalog_id { get; set; }
        public string film_catalog_title { get; set; }
        public int film_catalog_code { get; set; }
        public int? type_client { get; set; }
        public int? film_id { get; set; }
        public string film_title { get; set; }
        public int typekid { get; set; }
        public int? show_home { get; set; }
        public int? display { get; set; }
    }
}