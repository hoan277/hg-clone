﻿using System;

using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using ServiceStack;

namespace lamlt.data
{
    [Alias("film_video_processing_transcript")]
    public partial class film_video_processing_transcript : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? video_processing_id { get; set; }
        public int? transcript_id { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public string status { get; set; }
        public string file_thanh_pham { get; set; }
        public int? isview { get; set; }
        public int? userid { get; set; }
    }

    [Alias("vw_film_video_processing_transcript")]
    public partial class vw_film_video_processing_transcript
    {
        public int Id { get; set; }
        public int? video_processing_id { get; set; }
        public int? transcript_id { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? isview { get; set; }
        public string status { get; set; }
        public string file_thanh_pham { get; set; }
        public string video_processing_title { get; set; }
        public string transcript_title { get; set; }
        public string transcript_file { get; set; }
        public string transcript_code { get; set; }
        public string transcript_desc { get; set; }
        public string transcript_keywords { get; set; }
    }
}