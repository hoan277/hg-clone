﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_contract")]
    public partial class film_contract : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int film_id { get; set; }
        public string number { get; set; }
        public string censorship { get; set; }
        public string copyright { get; set; }
        public string appendix { get; set; }
        public string partner { get; set; }
        public string type { get; set; }
        public string file { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_sign { get; set; }
        public DateTime date_expired { get; set; }
    }

    [Alias("vw_film_contract")]
    public partial class vw_film_contract
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int film_id { get; set; }
        public string number { get; set; }
        public string censorship { get; set; }
        public string appendix { get; set; }
        public string copyright { get; set; }
        public string partner { get; set; }
        public string type { get; set; }
        public string file { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_sign { get; set; }
        public DateTime date_expired { get; set; }
    }
}