﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("totalviettel")]
    public partial class totalviettel : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public DateTime? date { get; set; }
        public int? total { get; set; }

    }
    [Alias("vw_totalviettel")]
    public partial class vw_totalviettel
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public DateTime? date { get; set; }
        public int? total { get; set; }
    }
}
