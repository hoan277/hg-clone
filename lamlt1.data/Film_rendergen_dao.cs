﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_render")]
    public partial class film_render : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string list_file { get; set; }
        public int effect { get; set; }
        public string thumb { get; set; }
        public int channel_id { get; set; }
        public string yt_title { get; set; }
        public string yt_desc { get; set; }
        public string yt_mode { get; set; }
        public DateTime? yt_schedule { get; set; }
        public string status { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_update { get; set; }
    }

    [Alias("vw_film_render")]
    public partial class vw_film_render
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string list_file { get; set; }
        public int effect { get; set; }
        public string thumb { get; set; }
        public int channel_id { get; set; }
        public string channel_id_title { get; set; }
        public string yt_title { get; set; }
        public string yt_desc { get; set; }
        public string yt_mode { get; set; }
        public DateTime? yt_schedule { get; set; }
        public string status { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_update { get; set; }
    }
}