﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_comments")]
    public partial class film_comments : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? userid { get; set; }
        public int? film_id { get; set; }
        public string full_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime? datecreated { get; set; }
        public int userid_created { get; set; }
        public int? like { get; set; }
        public int? dislike { get; set; }
        public int? status { get; set; }
    }

    [Alias("vw_film_comments")]
    public partial class vw_film_comments
    {
        public int Id { get; set; }
        public int? userid { get; set; }
        public int? film_id { get; set; }
        public string content { get; set; }
        public DateTime? datecreated { get; set; }
        public int? status { get; set; }
        public string avatar { get; set; }
        public string fullname { get; set; }
    }
}