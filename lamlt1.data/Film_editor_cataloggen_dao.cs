﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_editor_catalog")]
    public partial class film_editor_catalog : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? editor_id { get; set; }
        public int? catalog_id { get; set; }
        public int? catalog_time { get; set; }
        public int? catalog_position { get; set; }
        public int? change_scene_id { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
    }

    [Alias("vw_film_editor_catalog")]
    public partial class vw_film_editor_catalog
    {
        public int Id { get; set; }
        public int? editor_id { get; set; }
        public int? catalog_id { get; set; }
        public int? catalog_time { get; set; }
        public int? catalog_position { get; set; }
        public int? change_scene_id { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
        public string title { get; set; }
    }
}