﻿
using ServiceStack.DataAnnotations;
using ServiceStack.Model;

namespace lamlt.data
{
    [Alias("translate")]
    public partial class translate : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public string result_orc { get; set; }
        public string result { get; set; }
        public string status { get; set; }

    }
    [Alias("vw_translate")]
    public partial class vw_translate
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public string result_orc { get; set; }
        public string result { get; set; }
        public string status { get; set; }
    }
}