﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_product_processing")]
    public partial class film_product_processing : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public int? product_id { get; set; }
        public int? video_processing_id { get; set; }
        public int? video_processing_time { get; set; }
        public int? video_processing_position { get; set; }
        public int? catalog_id { get; set; }
        public int? catalog_time { get; set; }
        public int? catalog_position { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }

    }
    [Alias("vw_film_product_processing")]
    public partial class vw_film_product_processing
    {
        public int Id { get; set; }
        public int? product_id { get; set; }
        public string product_title { get; set; }
        public int? video_processing_id { get; set; }
        public int? video_processing_time { get; set; }
        public int? video_processing_position { get; set; }
        public int? catalog_id { get; set; }
        public string catalog_title { get; set; }
        public int? catalog_time { get; set; }
        public int? catalog_position { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
        public int? userid { get; set; }
    }
}