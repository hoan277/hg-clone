﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_video_trainer_trainer")]
    public partial class film_video_trainer : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string trainer_url { get; set; }
        public string trainer_thumbnail { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
    }

    [Alias("vw_film_video_trainer")]
    public partial class vw_film_video_trainer
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string trainer_url { get; set; }
        public string trainer_thumbnail { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? status { get; set; }
    }
}