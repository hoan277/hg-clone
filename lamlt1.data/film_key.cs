﻿using System;

namespace lamlt.data
{
    public class film_key
    {
        public int id { get; set; }
        public string title { get; set; }
        public string key { get; set; }
        public string allow { get; set; }
        public string status { get; set; }
        public string note { get; set; }
    }
    public class film_key_log
    {
        public int id { get; set; }
        public string key { get; set; }
        public int time { get; set; }
        public DateTime created { get; set; }
        public string note { get; set; }
    }
}
