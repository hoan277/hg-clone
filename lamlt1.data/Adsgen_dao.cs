﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("ads")]
    public partial class ads : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string type { get; set; }
        public string ads_id { get; set; }
        public string ads_type { get; set; }
        public string item_id { get; set; }
        public string item_type { get; set; }
        public string content { get; set; }
        public string size { get; set; }
        public string note { get; set; }
        public DateTime datecreated { get; set; }
        public string status { get; set; }
        public int? time_show { get; set; }
        public int? duration { get; set; }
        public string link_vast { get; set; }
        public string embedded { get; set; }
        public int? is_android { get; set; }
        public int? typekid { get; set; }
    }

    [Alias("vw_ads")]
    public partial class vw_ads
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string type { get; set; }
        public string ads_id { get; set; }
        public string ads_type { get; set; }
        public string item_id { get; set; }
        public string item_type { get; set; }
        public string content { get; set; }
        public string size { get; set; }
        public string note { get; set; }
        public DateTime datecreated { get; set; }
        public string status { get; set; }
        public int? time_show { get; set; }
        public int? duration { get; set; }
        public string link_vast { get; set; }
        public string embedded { get; set; }
        public int? is_android { get; set; }
        public int? typekid { get; set; }
    }
}