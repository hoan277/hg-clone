﻿using System;

using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using ServiceStack;

namespace lamlt.data
{
    [Alias("film_transcript")]
    public partial class film_transcript : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public string title { get; set; }
        public string file { get; set; }
        public string code { get; set; }
        public int? catalogid { get; set; }
        public string language { get; set; }
        public string keywords { get; set; }
        public string desc { get; set; }
        public int? quatity { get; set; }
        public int? position { get; set; }
        public int? status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? isview { get; set; }
    }

    [Alias("vw_film_transcript")]
    public partial class vw_film_transcript
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string file { get; set; }
        public string code { get; set; }
        public int? catalogid { get; set; }
        public string language { get; set; }
        public string keywords { get; set; }
        public string desc { get; set; }
        public int? quatity { get; set; }
        public int? position { get; set; }
        public int? status { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public int? userid { get; set; }
        public int? isview { get; set; }
    }
}