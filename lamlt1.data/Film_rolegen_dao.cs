﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_role")]
    public partial class film_role : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string code { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public string desc { get; set; }
        public int? userid { get; set; }
        public int? status { get; set; }
        public string menu { get; set; }


    }
    [Alias("vw_film_role")]
    public partial class vw_film_role
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string code { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? dateupdated { get; set; }
        public string desc { get; set; }
        public int? userid { get; set; }
        public string menu { get; set; }
        public int? status { get; set; }
    }
}