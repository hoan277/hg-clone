﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_media_stream")]
    public partial class film_media_stream : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int stt { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string bitrate { get; set; }
        public string audiobitrate { get; set; }
        public int? deinterface { get; set; }
        public string fbs { get; set; }
        public int? framerate { get; set; }
        public string input_dir { get; set; }
        public string input_origin { get; set; }
        public string outputFolder { get; set; }
        public string other { get; set; }
        public DateTime? datecreated { get; set; }
    }

    [Alias("vw_film_media_stream")]
    public partial class vw_film_media_stream
    {
        public int Id { get; set; }
        public int stt { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string bitrate { get; set; }
        public string audiobitrate { get; set; }
        public int? deinterface { get; set; }
        public string fbs { get; set; }
        public int? framerate { get; set; }
        public string input_dir { get; set; }
        public string input_origin { get; set; }
        public string outputFolder { get; set; }
        public string other { get; set; }
        public DateTime? datecreated { get; set; }
    }
}