﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_contact")]
    public partial class film_contact : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }

        public int? userid { get; set; }
        public string full_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid_created { get; set; }
    }

    [Alias("vw_film_contact")]
    public partial class vw_film_contact
    {
        public int Id { get; set; }
        public int? userid { get; set; }
        public string full_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime? datecreated { get; set; }
        public int? userid_created { get; set; }
    }
}