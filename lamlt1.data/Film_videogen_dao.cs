﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_video")]
    public partial class film_video : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        // chờ duyêt
        public DateTime? datecreated { get; set; }
        // nháp
        public DateTime? dateupdate1 { get; set; }
        // đã duyệt
        public DateTime? dateupdate2 { get; set; }
        // Từ chối
        public DateTime? dateupdate3 { get; set; }
        // Đã cập nhât
        public DateTime? dateupdate4 { get; set; }
        // Đang convert
        public DateTime? dateupdate5 { get; set; }
        // Cap nhat anh loi
        public DateTime? dateupdate6 { get; set; }
        public int? userid { get; set; }
        public int? seriid { get; set; }
        public string imdb { get; set; }
        public int? format { get; set; }
        public int? sub_type { get; set; }
        public int? publish_year { get; set; }
        public int? publish_countryid { get; set; }
        public string duration { get; set; }
        public string actor { get; set; }
        public string director { get; set; }
        public int? film_type { get; set; }
        public int? episode { get; set; }
        public int? episode_current { get; set; }
        public string contract_copyright { get; set; }
        public DateTime? contract_exprired { get; set; }
        public string contract_appendix { get; set; }
        public string copyright_appendix { get; set; }
        public DateTime? copyright_expired { get; set; }
        public int? catalog_id { get; set; }
        public string upload_file { get; set; }
        public string thumb_file { get; set; }
        public int? exclusive { get; set; }
        public int? showhome { get; set; }
        public float? price { get; set; }
        public int? status { get; set; }
        public int? cpid { get; set; }
        public string code { get; set; }
        public string tags { get; set; }
        public int? episode_id { get; set; }
        public string catalog { get; set; }
        public int? trainer_id { get; set; }
        public int? isview { get; set; }
        public string trainer_url { get; set; }
        public string path { get; set; }
        public int? count_share { get; set; }
        public int? filter { get; set; }
        public int? view { get; set; }
    }

    [Alias("vw_film_video")]
    public partial class vw_film_video
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        // chờ duyêt
        public DateTime? datecreated { get; set; }
        // nháp
        public DateTime? dateupdate1 { get; set; }
        // đã duyệt
        public DateTime? dateupdate2 { get; set; }
        // Từ chối
        public DateTime? dateupdate3 { get; set; }
        // Đã cập nhât
        public DateTime? dateupdate4 { get; set; }
        // Đang convert
        public DateTime? dateupdate5 { get; set; }
        // Cap nhat anh loi
        public DateTime? dateupdate6 { get; set; }
        public int? userid { get; set; }
        public int? seriid { get; set; }
        public string imdb { get; set; }
        public int? format { get; set; }
        public int? sub_type { get; set; }
        public int publish_year { get; set; }
        public int? publish_countryid { get; set; }
        public string duration { get; set; }
        public string actor { get; set; }
        public string director { get; set; }
        public int? film_type { get; set; }
        public int episode { get; set; }
        public int episode_current { get; set; }
        public string contract_copyright { get; set; }
        public DateTime? contract_exprired { get; set; }
        public string contract_appendix { get; set; }
        public string copyright_appendix { get; set; }
        public DateTime? copyright_expired { get; set; }
        public int? catalog_id { get; set; }
        public int catalog_id_code { get; set; }
        public string upload_file { get; set; }
        public string thumb_file { get; set; }
        public int? exclusive { get; set; }
        public int? showhome { get; set; }
        public float? price { get; set; }
        public int? status { get; set; }
        public int? cpid { get; set; }
        public string code { get; set; }
        public string tags { get; set; }
        public int like { get; set; }
        public int dislike { get; set; }
        public string publish_countryid_title { get; set; }
        public string catalog_id_title { get; set; }
        public string cpid_title { get; set; }
        public int? episode_id { get; set; }
        public string episode_title { get; set; }
        public string catalog { get; set; }
        public int? trainer_id { get; set; }
        public string trainer_title { get; set; }
        public string trainer_desc { get; set; }
        public string trainer_url { get; set; }
        public string trainer_thumbnail { get; set; }
        public string path { get; set; }
        public int? isview { get; set; }
        public int? count_share { get; set; }
        public int? filter { get; set; }
        public int? view { get; set; }
    }

    [Alias("vw_film_video_cms")]
    public partial class vw_film_video_cms
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        // chờ duyêt
        public DateTime? datecreated { get; set; }
        // nháp
        public DateTime? dateupdate1 { get; set; }
        // đã duyệt
        public DateTime? dateupdate2 { get; set; }
        // Từ chối
        public DateTime? dateupdate3 { get; set; }
        // Đã cập nhât
        public DateTime? dateupdate4 { get; set; }
        // Đang convert
        public DateTime? dateupdate5 { get; set; }
        // Cap nhat anh loi
        public DateTime? dateupdate6 { get; set; }
        public int? userid { get; set; }
        public int? seriid { get; set; }
        public string imdb { get; set; }
        public int? format { get; set; }
        public int? sub_type { get; set; }
        public int publish_year { get; set; }
        public int? publish_countryid { get; set; }
        public string duration { get; set; }
        public string actor { get; set; }
        public string director { get; set; }
        public int? film_type { get; set; }
        public int episode { get; set; }
        public int episode_current { get; set; }
        public string contract_copyright { get; set; }
        public DateTime? contract_exprired { get; set; }
        public string contract_appendix { get; set; }
        public string copyright_appendix { get; set; }
        public DateTime? copyright_expired { get; set; }
        public int? catalog_id { get; set; }
        public int catalog_id_code { get; set; }
        public string upload_file { get; set; }
        public string thumb_file { get; set; }
        public int? exclusive { get; set; }
        public int? showhome { get; set; }
        public float? price { get; set; }
        public int? status { get; set; }
        public int? cpid { get; set; }
        public string code { get; set; }
        public string tags { get; set; }
        public int like { get; set; }
        public int dislike { get; set; }
        public string publish_countryid_title { get; set; }
        public string catalog_id_title { get; set; }
        public string cpid_title { get; set; }
        public int? episode_id { get; set; }
        public string episode_title { get; set; }
        public string catalog { get; set; }
        public int? trainer_id { get; set; }
        public string trainer_title { get; set; }
        public string trainer_desc { get; set; }
        public string trainer_url { get; set; }
        public string trainer_thumbnail { get; set; }
        public string path { get; set; }
        public int? isview { get; set; }
        public int? count_share { get; set; }
        public int? filter { get; set; }
        public int? view { get; set; }
    }

    [Alias("vw_film_video_report")]
    public partial class vw_film_video_report
    {
        public int video_id { get; set; }
        public string film_title { get; set; }
        public int? cp_id { get; set; }
        public string cp_title { get; set; }
        public DateTime? dateview { get; set; }
        public int? count_share { get; set; }
        public int? filter { get; set; }
    }


    [Alias("vw_film_video_statistic")]
    public partial class vw_film_video_statistic
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        // chờ duyêt
        public DateTime? datecreated { get; set; }
        // nháp
        public DateTime? dateupdate1 { get; set; }
        // đã duyệt
        public DateTime? dateupdate2 { get; set; }
        // Từ chối
        public DateTime? dateupdate3 { get; set; }
        // Đã cập nhât
        public DateTime? dateupdate4 { get; set; }
        // Đang convert
        public DateTime? dateupdate5 { get; set; }
        // Cap nhat anh loi
        public DateTime? dateupdate6 { get; set; }
        public int? userid { get; set; }
        public int? seriid { get; set; }
        public string imdb { get; set; }
        public int? format { get; set; }
        public int? sub_type { get; set; }
        public int? publish_year { get; set; }
        public int? publish_countryid { get; set; }
        public string duration { get; set; }
        public string actor { get; set; }
        public string director { get; set; }
        public int? film_type { get; set; }
        public int? episode { get; set; }
        public int? episode_current { get; set; }
        public string contract_copyright { get; set; }
        public DateTime? contract_exprired { get; set; }
        public string contract_appendix { get; set; }
        public string copyright_appendix { get; set; }
        public DateTime? copyright_expired { get; set; }
        public int? catalog_id { get; set; }
        public string upload_file { get; set; }
        public string thumb_file { get; set; }
        public int? exclusive { get; set; }
        //public int? showhome { get; set; }

        public float? price { get; set; }
        public int? status { get; set; }
        public int? cpid { get; set; }
        public string code { get; set; }
        public string tags { get; set; }
        public string publish_countryid_title { get; set; }
        public string catalog_id_title { get; set; }
        public string cpid_title { get; set; }
        public int? count_share { get; set; }
        public int? filter { get; set; }
    }

}