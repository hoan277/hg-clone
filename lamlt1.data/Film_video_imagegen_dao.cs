﻿using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using System;

namespace lamlt.data
{
    [Alias("film_video_image")]
    public partial class film_video_image : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
        public string url { get; set; }
        public int? filmid { get; set; }
        public DateTime? datecreated { get; set; }

    }
    [Alias("vw_film_video_image")]
    public partial class vw_film_video_image
    {
        public int Id { get; set; }
        public string url { get; set; }
        public int? filmid { get; set; }
        public DateTime? datecreated { get; set; }
        public string filmid_title { get; set; }
    }
}
