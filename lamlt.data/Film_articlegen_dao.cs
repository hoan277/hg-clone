﻿using System;

using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using ServiceStack;

namespace lamlt.data
{
	[Alias("film_article")]
    public partial class film_article : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
public string url { get; set; }
public int? ordinal { get; set; }
public string target { get; set; }
public int? status { get; set; }
public DateTime? created_time { get; set; }
public string title { get; set; }
public string summary { get; set; }
public string content { get; set; }

    }
}