﻿using System;

using ServiceStack.DataAnnotations;
using ServiceStack.Model;
using ServiceStack;

namespace lamlt.data
{
	[Alias("film_video_image")]
    public partial class film_video_image : IHasId<int>
    {
        [Alias("id")]
        [AutoIncrement]
        public int Id { get; set; }
public int? filmid { get; set; }
public string url { get; set; }
public DateTime? datecreated { get; set; }

    }
}